package tw.org.ctworld.meditation;

import org.aspectj.apache.bcel.classfile.Code;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.*;
import tw.org.ctworld.meditation.services.*;

import java.util.ArrayList;
import java.util.List;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    @Value("${ad.domain}")
    private String domain;

    @Autowired
    private UnitInfoRepo unitInfoRepo;

    @Autowired
    private UserInfoRepo userInfoRepo;

    @Autowired
    private CodeDefRepo codeDefRepo;

    @Autowired
    private ExcelProcessService excelProcessService;

    @Autowired
    private UserLoginLogService userLoginLogService;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private OldClassMonthStatsService oldClassMonthStatsService;

    @Autowired
    private SysCodeService sysCodeService;

    private static final Logger logger = LoggerFactory.getLogger(ApplicationStartup.class);


    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        logger.info("System Preparing ...");

        userLoginLogService.logoutAll();
        fileStorageService.cleanTempFiles(false);

        // publish required data start, DO NOT REMOVE!
        AddUnitInfoFromExcel();
        PublishOldClassStats();
        // publish required data end, DO NOT REMOVE!

//        AddAuthInfo();
//        AddAccountCategoryInfo();

        AddLoginUserInfo();
        //AddClassInfo();
//        AddClassCodeDef();
        AddCodeDef();

        if (domain.equals("box7000.com")) {
            BypassAllIP();
        }
//        TestYubikey();

//        setTwIdToMembers();

        AddSysCodeIfEmpty();
        CreateFilePath();

        logger.info("System Ready");
    }

    // publish unit data, DO NOT REMOVE!
    private void AddUnitInfoFromExcel() {
        if (unitInfoRepo.findAll().isEmpty()) {

            logger.info("Inserting Unit Info ...");

            excelProcessService.processUnitInfoData();
        }
    }

    // publish old class Statistics
    private void PublishOldClassStats() {
        oldClassMonthStatsService.importOldClassStats();
    }

    // set not ip check to all units for testing purpose
    private void BypassAllIP() {
        unitInfoRepo.BypassAllIp();
    }

    private void AddLoginUserInfo() {
        if (userInfoRepo.findAll().isEmpty()) {

            String[][] data = new String[][]{
                    {"UNIT02002", "法務組", UserInfo.AccountCategory.ADMIN.getValue(), "Atest", "atest@test.com", "JobTitle A", "0910001001", "0", null, "ccccccectlfh"},

                    {"UNIT02006", "專案開發組", UserInfo.AccountCategory.PERMANENT.getValue(), "Btest", "btest@test.com", "JobTitle B", "0913001002", "0", null, "ccccccectlfh"},

                    {"UNIT01001", "靈泉", UserInfo.AccountCategory.ABODE.getValue(), "Btest1", "btest@test.com", "JobTitle B", "0913001002", "0", null, "ccccccectlfh"},
                    {"UNIT01003", "天祥", UserInfo.AccountCategory.ABODE.getValue(), "Btest2", "btest@test.com", "JobTitle B", "0913001002", "0", null, "ccccccectlfh"},
                    {"UNIT01004", "蓮社", UserInfo.AccountCategory.ABODE.getValue(), "Btest3", "btest@test.com", "JobTitle B", "0913001002", "0", null, "ccccccectlfh"},

                    {"UNIT01047", "普恩", UserInfo.AccountCategory.ABODE.getValue(), "Utest", "utest@test.com", "JobTitle C", "0913001002", "0", null, "ccccccectlfh"},

                    {"UNIT01001", "靈泉", UserInfo.AccountCategory.ABODE.getValue(), "Utest2", "utest2@test.com", "JobTitle C", "0913001002", "0", "107000001", "ccccccectlfh"},
                    {"UNIT01003", "天祥", UserInfo.AccountCategory.ABODE.getValue(), "Utest3", "utest3@test.com", "JobTitle C", "0913001002", "0", "107000002", "ccccccectlfh"},
                    {"UNIT01004", "蓮社", UserInfo.AccountCategory.ABODE.getValue(), "Utest4", "utest4@test.com", "JobTitle C", "0913001002", "0", "107000003", "ccccccectlfh"},
            };

            for (int i = 0; i < data.length; i++) {
                UserInfo info = new UserInfo(data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5], data[i][6], (data[i][7].equals("0") ? false : true), data[i][8], data[i][9]);

                info.setAllPermissions(true);

                userInfoRepo.save(info);
            }
        }
    }

//    private void AddClassInfo() {
//        if (classInfoRepo.findAll().isEmpty()) {
//
//            String[][] classData = new String[][]{
//                    {"2", "日間", "1", "初級班"},
//                    {"2", "夜間", "1", "初級班"},
//                    {"2", "日間", "2", "中級班"},
//                    {"2", "夜間", "2", "中級班"},
//                    {"2", "日間", "3", "高級班"},
//                    {"2", "夜間", "3", "高級班"},
//                    {"2", "日間", "4", "研一班"},
//                    {"2", "夜間", "4", "研一班"},
//                    {"2", "日間", "5", "研二班"},
//                    {"2", "夜間", "5", "研二班"},
//                    {"2", "日間", "10", "青少年班"},
//                    {"2", "夜間", "10", "青少年班"},
//                    {"2", "日間", "11", "長青班"},
//                    {"2", "夜間", "11", "長青班"},
//                    {"2", "日間", "12", "兒童班"},
//                    {"2", "夜間", "12", "兒童班"},
//                    {"2", "日間", "13", "念佛班"},
//                    {"2", "夜間", "13", "念佛班"},
//                    {"2", "日間", "14", "梵唄班"},
//                    {"2", "夜間", "14", "梵唄班"},
//                    {"2", "日間", "15", "才藝班"},
//                    {"2", "夜間", "15", "才藝班"},
//                    {"2", "日間", "16", "共修班"},
//                    {"2", "夜間", "16", "共修班"}
//            };
//
//            for (int i = 0; i < classData.length; i++) {
//
//                Date date = java.sql.Date.valueOf("2018-06-23");
//
//                String classId = classIdGenerator();
//
//                ClassInfo classInfo = new ClassInfo();
//                classInfo.setClassId(classId);
//                classInfo.setClassStartDate(date);
//
//                classInfo.setClassStatus(classData[i][0]);
//                classInfo.setClassDayOrNight(classData[i][1]);
//                classInfo.setClassTypeNum(Integer.parseInt(classData[i][2]));
//                classInfo.setClassTypeName(classData[i][3]);
//
//                classInfoRepo.save(classInfo);
//
//                String[][] studentData = new String[][]{
//                        {"1", "V"},
//                        {"2", "V"},
//                        {"3", "V"},
//                        {"4", "V"},
//                        {"5", "V"},
//                        {"6", "X"},
//                        {"7", "X"},
//                        {"8", "X"},
//                        {"9", "X"},
//                        {"10", "X"}
//                };
//
//                for (int x = 0; x < studentData.length; x++) {
//
//                    ClassAttendRecord classAttendRecord = new ClassAttendRecord();
//                    classAttendRecord.setClassId(classId);
//                    classAttendRecord.setMemberId(studentData[x][0]);
//                    classAttendRecord.setAttendMark(studentData[x][1]);
//
//                    classAttendRecordRepo.save(classAttendRecord);
//                }
//            }
//        }
//
//    }

    private void AddCodeDef() {
        if (codeDefRepo.findAll().isEmpty()) {
            List<CodeDef> codeDefList = excelProcessService.processCodeDefExcel();
            codeDefRepo.saveAll(codeDefList);
        }
    }

//    private void AddClassCodeDef() {
//        if (codeDefRepo.findAll().isEmpty()) {
//
//            String[][] classCodeDefData = new String[][]{
//                    {"課程狀態", null, null, "未啟動", "0", "1"},
//                    {"課程狀態", null, null, "可報名", "1", "2"},
//                    {"課程狀態", null, null, "開課中", "2", "3"},
//                    {"課程狀態", null, null, "待結業", "3", "4"},
//                    {"課程狀態", null, null, "已結業", "4", "5"},
//
//                    {"禪修班", null, null, "初級班", "1", "1"},
//                    {"禪修班", null, null, "中級班", "2", "2"},
//                    {"禪修班", null, null, "高級班", "3", "3"},
//                    {"禪修班", null, null, "研一班", "4", "4"},
//                    {"禪修班", null, null, "研二班", "5", "5"},
//                    {"禪修班", null, null, "青少年班", "10", "6"},
//                    {"禪修班", null, null, "長青班", "11", "7"},
//                    {"禪修班", null, null, "兒童班", "12", "8"},
//                    {"禪修班", null, null, "念佛班", "13", "9"},
//                    {"禪修班", null, null, "梵唄班", "14", "10"},
//                    {"禪修班", null, null, "才藝班", "15", "11"},
//                    {"禪修班", null, null, "共修班", "16", "12"},
//                    {"語言別", null, null, "國語", "1", "1"},
//                    {"語言別", null, null, "台語", "2", "2"},
//                    {"語言別", null, null, "客語", "3", "3"},
//                    {"語言別", null, null, "英語", "4", "4"},
//                    {"語言別", null, null, "日語", "5", "5"},
//                    {"語言別", null, null, "德語", "6", "6"},
//                    {"語言別", null, null, "粵語", "7", "7"},
//                    {"語言別", null, null, "義大利語", "8", "8"},
//
//                    {"學員長職事", null, null, "總學員長", "1", "1"},
//                    {"學員長職事", null, null, "顧問副總學員長", "2", "2"},
//                    {"學員長職事", null, null, "榮譽副總學員長", "3", "3"},
//                    {"學員長職事", null, null, "常務副總學員長", "4", "4"},
//                    {"學員長職事", null, null, "副總學員長", "5", "5"},
//                    {"學員長職事", null, null, "課務副總學員長", "6", "6"},
//                    {"學員長職事", null, null, "法務副總學員長", "7", "7"},
//                    {"學員長職事", null, null, "活動副總學員長", "8", "8"},
//                    {"學員長職事", null, null, "人力副總學員長", "9", "9"},
//                    {"學員長職事", null, null, "福田副總學員長", "10", "10"},
//                    {"學員長職事", null, null, "課務組長", "11", "11"},
//                    {"學員長職事", null, null, "法務組長", "12", "12"},
//                    {"學員長職事", null, null, "活動組長", "13", "13"},
//                    {"學員長職事", null, null, "人力組長", "14", "14"},
//                    {"學員長職事", null, null, "福田組長", "15", "15"},
//                    {"學員長職事", null, null, "學員長", "16", "16"},
//                    {"學員長職事", null, null, "秘書", "17", "17"},
//                    {"學員長職事", null, null, "組長", "18", "18"},
//                    {"學員長職事", null, null, "副組長", "19", "19"},
//
//                    {"工作內容", null, null, "維那", "1", "1"},
//                    {"工作內容", null, null, "悅眾", "2", "2"},
//                    {"工作內容", null, null, "寶鐘鼓", "3", "3"},
//                    {"工作內容", null, null, "輔導老師", "4", "4"},
//                    {"工作內容", null, null, "值星官", "5", "5"},
//                    {"工作內容", null, null, "尊師重道", "6", "6"},
//                    {"工作內容", null, null, "主藏", "7", "7"},
//                    {"工作內容", null, null, "內謢", "8", "8"},
//                    {"工作內容", null, null, "外護", "9", "9"},
//                    {"工作內容", null, null, "結緣", "10", "10"},
//                    {"工作內容", null, null, "點心", "11", "11"},
//                    {"工作內容", null, null, "歡喜", "12", "12"},
//                    {"工作內容", null, null, "齋務", "13", "13"},
//                    {"工作內容", null, null, "報到處", "14", "14"},
//                    {"工作內容", null, null, "小組報到", "15", "15"},
//                    {"工作內容", null, null, "大雄佈場", "16", "16"},
//                    {"工作內容", null, null, "禪堂佈場", "17", "17"},
//                    {"工作內容", null, null, "冷氣主機", "18", "18"},
//                    {"工作內容", null, null, "音響視訊", "19", "19"},
//                    {"工作內容", null, null, "前燈控", "20", "20"},
//                    {"工作內容", null, null, "西燈控", "21", "21"},
//                    {"工作內容", null, null, "東燈控", "22", "22"},
//                    {"工作內容", null, null, "後燈控", "23", "23"},
//                    {"工作內容", null, null, "除濕機", "24", "24"},
//                    {"工作內容", null, null, "空氣清淨機", "25", "25"},
//
//                    {"禪修班別", null, null, "日初", "1", "1"},
//                    {"禪修班別", null, null, "夜初", "2", "2"},
//                    {"禪修班別", null, null, "日中", "3", "3"},
//                    {"禪修班別", null, null, "夜中", "4", "4"},
//                    {"禪修班別", null, null, "日高", "5", "5"},
//                    {"禪修班別", null, null, "夜高", "6", "6"},
//                    {"禪修班別", null, null, "日研一", "7", "7"},
//                    {"禪修班別", null, null, "夜研一", "8", "8"},
//                    {"禪修班別", null, null, "日研二", "9", "9"},
//                    {"禪修班別", null, null, "夜研二", "10", "10"},
//                    {"禪修班別", null, null, "青少年班", "11", "11"},
//                    {"禪修班別", null, null, "長青班", "12", "12"},
//                    {"禪修班別", null, null, "兒童班", "13", "13"},
//                    {"禪修班別", null, null, "念佛班", "14", "14"},
//                    {"禪修班別", null, null, "梵唄班", "15", "15"},
//                    {"禪修班別", null, null, "才藝班", "16", "16"},
//                    {"禪修班別", null, null, "共修班", "17", "17"},
//
//                    {"護法會", null, null, "會長", "1", "1"},
//                    {"護法會", null, null, "總監", "2", "2"},
//                    {"護法會", null, null, "督導", "3", "3"},
//                    {"護法會", null, null, "榮譽會長", "4", "4"},
//                    {"護法會", null, null, "副會長", "5", "5"},
//                    {"護法會", null, null, "總幹事", "6", "6"},
//                    {"護法會", null, null, "副總幹事", "7", "7"},
//                    {"護法會", null, null, "執行秘書", "8", "8"},
//                    {"護法會", null, null, "顧問", "9", "9"},
//                    {"護法會", null, null, "諮詢委員", "10", "10"},
//                    {"護法會", null, null, "心燈組組長", "11", "11"},
//                    {"護法會", null, null, "心燈組副組長", "12", "12"},
//                    {"護法會", null, null, "心燈組幹事", "13", "13"},
//                    {"護法會", null, null, "心燈組秘書", "14", "14"},
//                    {"護法會", null, null, "文宣組組長", "15", "15"},
//                    {"護法會", null, null, "文宣組副組長", "16", "16"},
//                    {"護法會", null, null, "文宣組幹事", "17", "17"},
//                    {"護法會", null, null, "文宣組秘書", "18", "18"},
//                    {"護法會", null, null, "司儀組組長", "19", "19"},
//                    {"護法會", null, null, "司儀組副組長", "20", "20"},
//                    {"護法會", null, null, "司儀組幹事", "21", "21"},
//                    {"護法會", null, null, "司儀組秘書", "22", "22"},
//                    {"護法會", null, null, "交通組組長", "23", "23"},
//                    {"護法會", null, null, "交通組副組長", "24", "24"},
//                    {"護法會", null, null, "交通組幹事", "25", "25"},
//                    {"護法會", null, null, "交通組秘書", "26", "26"},
//                    {"護法會", null, null, "助念組組長", "27", "27"},
//                    {"護法會", null, null, "助念組副組長", "28", "28"},
//                    {"護法會", null, null, "助念組幹事", "29", "29"},
//                    {"護法會", null, null, "助念組秘書", "30", "30"},
//                    {"護法會", null, null, "社會發展組組長", "31", "31"},
//                    {"護法會", null, null, "社會發展組副組長", "32", "32"},
//                    {"護法會", null, null, "社會發展組幹事", "33", "33"},
//                    {"護法會", null, null, "社會發展組秘書", "34", "34"},
//                    {"護法會", null, null, "花藝組組長", "35", "35"},
//                    {"護法會", null, null, "花藝組副組長", "36", "36"},
//                    {"護法會", null, null, "花藝組幹事", "37", "37"},
//                    {"護法會", null, null, "花藝組秘書", "38", "38"},
//                    {"護法會", null, null, "金剛護法組組長", "39", "39"},
//                    {"護法會", null, null, "金剛護法組副組長", "40", "40"},
//                    {"護法會", null, null, "金剛護法組幹事", "41", "41"},
//                    {"護法會", null, null, "金剛護法組秘書", "42", "42"},
//                    {"護法會", null, null, "活動組組長", "43", "43"},
//                    {"護法會", null, null, "活動組副組長", "44", "44"},
//                    {"護法會", null, null, "活動組幹事", "45", "45"},
//                    {"護法會", null, null, "活動組秘書", "46", "46"},
//                    {"護法會", null, null, "美工佈置組組長", "47", "47"},
//                    {"護法會", null, null, "美工佈置組副組長", "48", "48"},
//                    {"護法會", null, null, "美工佈置組幹事", "49", "49"},
//                    {"護法會", null, null, "美工佈置組秘書", "50", "50"},
//                    {"護法會", null, null, "音響組組長", "51", "51"},
//                    {"護法會", null, null, "音響組副組長", "52", "52"},
//                    {"護法會", null, null, "音響組幹事", "53", "53"},
//                    {"護法會", null, null, "音響組秘書", "54", "54"},
//                    {"護法會", null, null, "香積組組長", "55", "55"},
//                    {"護法會", null, null, "香積組副組長", "56", "56"},
//                    {"護法會", null, null, "香積組幹事", "57", "57"},
//                    {"護法會", null, null, "香積組秘書", "58", "58"},
//                    {"護法會", null, null, "書記組組長", "59", "59"},
//                    {"護法會", null, null, "書記組副組長", "60", "60"},
//                    {"護法會", null, null, "書記組幹事", "61", "61"},
//                    {"護法會", null, null, "書記組秘書", "62", "62"},
//                    {"護法會", null, null, "庶務組組長", "63", "63"},
//                    {"護法會", null, null, "庶務組副組長", "64", "64"},
//                    {"護法會", null, null, "庶務組幹事", "65", "65"},
//                    {"護法會", null, null, "庶務組秘書", "66", "66"},
//                    {"護法會", null, null, "梵唄組組長", "67", "67"},
//                    {"護法會", null, null, "梵唄組副組長", "68", "68"},
//                    {"護法會", null, null, "梵唄組幹事", "69", "69"},
//                    {"護法會", null, null, "梵唄組秘書", "70", "70"},
//                    {"護法會", null, null, "莊嚴組組長", "71", "71"},
//                    {"護法會", null, null, "莊嚴組副組長", "72", "72"},
//                    {"護法會", null, null, "莊嚴組幹事", "73", "73"},
//                    {"護法會", null, null, "莊嚴組秘書", "74", "74"},
//                    {"護法會", null, null, "照客組組長", "75", "75"},
//                    {"護法會", null, null, "照客組副組長", "76", "76"},
//                    {"護法會", null, null, "照客組幹事", "77", "77"},
//                    {"護法會", null, null, "照客組秘書", "78", "78"},
//                    {"護法會", null, null, "資訊組組長", "79", "79"},
//                    {"護法會", null, null, "資訊組副組長", "80", "80"},
//                    {"護法會", null, null, "資訊組幹事", "81", "81"},
//                    {"護法會", null, null, "資訊組秘書", "82", "82"},
//                    {"護法會", null, null, "圖書館組組長", "83", "83"},
//                    {"護法會", null, null, "圖書館組副組長", "84", "84"},
//                    {"護法會", null, null, "圖書館組幹事", "85", "85"},
//                    {"護法會", null, null, "圖書館組秘書", "86", "86"},
//                    {"護法會", null, null, "福田組組長", "87", "87"},
//                    {"護法會", null, null, "福田組副組長", "88", "88"},
//                    {"護法會", null, null, "福田組幹事", "89", "89"},
//                    {"護法會", null, null, "福田組秘書", "90", "90"},
//                    {"護法會", null, null, "課務組組長", "95", "95"},
//                    {"護法會", null, null, "課務組副組長", "96", "96"},
//                    {"護法會", null, null, "課務組幹事", "97", "97"},
//                    {"護法會", null, null, "課務組秘書", "98", "98"},
//                    {"護法會", null, null, "機動組組長", "99", "99"},
//                    {"護法會", null, null, "機動組副組長", "100", "100"},
//                    {"護法會", null, null, "機動組幹事", "101", "101"},
//                    {"護法會", null, null, "機動組秘書", "102", "102"},
//                    {"護法會", null, null, "環保組組長", "103", "103"},
//                    {"護法會", null, null, "環保組副組長", "104", "104"},
//                    {"護法會", null, null, "環保組幹事", "105", "105"},
//                    {"護法會", null, null, "環保組秘書", "106", "106"},
//                    {"護法會", null, null, "醫護組組長", "107", "107"},
//                    {"護法會", null, null, "醫護組副組長", "108", "108"},
//                    {"護法會", null, null, "醫護組幹事", "109", "109"},
//                    {"護法會", null, null, "醫護組秘書", "110", "110"},
//                    {"護法會", null, null, "藝文組組長", "111", "111"},
//                    {"護法會", null, null, "藝文組副組長", "112", "112"},
//                    {"護法會", null, null, "藝文組幹事", "113", "113"},
//                    {"護法會", null, null, "藝文組秘書", "114", "114"},
//                    {"護法會", null, null, "攝影組組長", "115", "115"},
//                    {"護法會", null, null, "攝影組副組長", "116", "116"},
//                    {"護法會", null, null, "攝影組幹事", "117", "117"},
//                    {"護法會", null, null, "攝影組秘書", "118", "118"},
//                    {"護法會", null, null, "福慧出坡組組長", "120", "120"},
//                    {"護法會", null, null, "福慧出坡組副組長", "121", "123"},
//                    {"護法會", null, null, "福慧出坡組幹事", "122", "126"},
//                    {"護法會", null, null, "福慧出坡組秘書", "119", "129"},
//                    {"護法會", null, null, "安全組組長", "123", "131"},
//                    {"護法會", null, null, "安全組副組長", "124", "134"},
//                    {"護法會", null, null, "安全組幹事", "125", "137"},
//                    {"護法會", null, null, "安全組秘書", "126", "140"},
//                    {"護法會", null, null, "視訊組組長", "127", "143"},
//                    {"護法會", null, null, "視訊組副組長", "128", "146"},
//                    {"護法會", null, null, "視訊組幹事", "129", "149"},
//                    {"護法會", null, null, "視訊組秘書", "130", "152"},
//                    {"護法會", null, null, "資料組組長", "131", "155"},
//                    {"護法會", null, null, "資料組副組長", "132", "158"},
//                    {"護法會", null, null, "資料組幹事", "133", "161"},
//                    {"護法會", null, null, "資料組秘書", "134", "164"},
//
//                    {"本山發心", "中台項目", "1", "圓", "1", "1"},
//                    {"本山發心", "中台項目", "1", "全", "2", "2"},
//                    {"本山發心", "中台項目", "1", "上", "3", "3"},
//                    {"本山發心", "中台項目", "1", "建", "4", "4"},
//                    {"本山發心", "中台項目", "1", "開", "5", "5"},
//                    {"本山發心", "中台項目", "1", "發", "6", "6"},
//                    {"本山發心", "中台項目", "1", "籌", "7", "7"},
//                    {"本山發心", "中台項目", "1", "增圓", "8", "8"},
//                    {"本山發心", "中台項目", "1", "增全", "9", "9"},
//                    {"本山發心", "中台項目", "1", "增上", "10", "10"},
//                    {"本山發心", "中台項目", "1", "增建", "11", "11"},
//                    {"本山發心", "中台項目", "1", "增開", "12", "12"},
//                    {"本山發心", "中台項目", "1", "增發", "13", "13"},
//                    {"本山發心", "中台項目", "1", "增籌", "14", "14"},
//                    {"本山發心", "博物管項目", "2", "博圓", "15", "1"},
//                    {"本山發心", "博物管項目", "2", "博全", "16", "2"},
//                    {"本山發心", "博物管項目", "2", "博上", "17", "3"},
//                    {"本山發心", "博物管項目", "2", "博建", "18", "4"},
//                    {"本山發心", "博物管項目", "2", "博開", "19", "5"},
//                    {"本山發心", "博物管項目", "2", "博發", "20", "6"},
//                    {"本山發心", "博物管項目", "2", "博籌", "21", "7"},
//                    {"本山發心", "普台項目", "3", "常董", "22", "1"},
//                    {"本山發心", "普台項目", "3", "榮董", "23", "2"},
//                    {"本山發心", "普台項目", "3", "興學", "24", "3"},
//                    {"本山發心", "普台項目", "3", "助學", "25", "4"},
//
//                    {"結夏發心", null, null, "供總", "1", "1"},
//                    {"結夏發心", null, null, "供副總", "2", "2"},
//                    {"結夏發心", null, null, "供精總", "3", "3"},
//                    {"結夏發心", null, null, "供菩提", "4", "4"},
//                    {"結夏發心", null, null, "供福慧", "5", "5"},
//                    {"結夏發心", null, null, "供莊嚴", "6", "6"},
//                    {"結夏發心", null, null, "供吉祥", "7", "7"},
//                    {"結夏發心", null, null, "供如意", "8", "8"},
//                    {"結夏發心", null, null, "藥總", "9", "9"},
//                    {"結夏發心", null, null, "藥副總", "10", "10"},
//                    {"結夏發心", null, null, "藥精總", "11", "11"},
//                    {"結夏發心", null, null, "藥菩提", "12", "12"},
//                    {"結夏發心", null, null, "藥福慧", "13", "13"},
//                    {"結夏發心", null, null, "藥莊嚴", "14", "14"},
//                    {"結夏發心", null, null, "藥吉祥", "15", "15"},
//                    {"結夏發心", null, null, "藥如意", "16", "16"},
//                    {"結夏發心", null, null, "蘭總", "17", "17"},
//                    {"結夏發心", null, null, "蘭副總", "18", "18"},
//                    {"結夏發心", null, null, "蘭精總", "19", "19"},
//                    {"結夏發心", null, null, "蘭菩提", "20", "20"},
//                    {"結夏發心", null, null, "蘭福慧", "21", "21"},
//                    {"結夏發心", null, null, "蘭莊嚴", "22", "22"},
//                    {"結夏發心", null, null, "蘭吉祥", "23", "23"},
//                    {"結夏發心", null, null, "蘭如意", "24", "24"},
//                    {"結夏發心", null, null, "普總", "25", "25"},
//                    {"結夏發心", null, null, "普副總", "26", "26"},
//                    {"結夏發心", null, null, "祿總", "27", "27"},
//                    {"結夏發心", null, null, "祿副總", "28", "28"},
//                    {"結夏發心", null, null, "蓮總", "29", "29"},
//                    {"結夏發心", null, null, "蓮副總", "30", "30"},
//
//                    {"親屬關係", "01", "01", "同修", "0101", "1"},
//                    {"親屬關係", "01", "01", "夫", "0102", "2"},
//                    {"親屬關係", "01", "01", "妻", "0103", "3"},
//                    {"親屬關係", "02", "02", "父", "0201", "1"},
//                    {"親屬關係", "02", "02", "母", "0202", "2"},
//                    {"親屬關係", "02", "02", "子", "0203", "3"},
//                    {"親屬關係", "02", "02", "女", "0204", "4"},
//                    {"親屬關係", "03", "03", "親眷", "0301", "1"},
//                    {"親屬關係", "03", "03", "兄", "0302", "2"},
//                    {"親屬關係", "03", "03", "弟", "0303", "3"},
//                    {"親屬關係", "03", "03", "姊", "0305", "4"},
//                    {"親屬關係", "03", "03", "妹", "0305", "5"},
//                    {"親屬關係", "03", "03", "孫子", "0306", "6"},
//                    {"親屬關係", "03", "03", "孫女", "0307", "7"},
//                    {"親屬關係", "03", "03", "岳父", "0308", "8"},
//                    {"親屬關係", "03", "03", "岳母", "0309", "9"},
//                    {"親屬關係", "03", "03", "公公", "0310", "10"},
//                    {"親屬關係", "03", "03", "婆婆", "0311", "11"},
////                    {"親屬關係", "03", "03", "弟媳", "0312", "12"},
////                    {"親屬關係", "03", "03", "弟妹", "0313", "13"},
//                    {"親屬關係", "03", "03", "爺爺", "0314", "14"},
//                    {"親屬關係", "03", "03", "奶奶", "0315", "15"},
//                    {"親屬關係", "03", "03", "外公", "0316", "16"},
//                    {"親屬關係", "03", "03", "外婆", "0317", "17"},
//                    {"親屬關係", "04", "04", "其他", "0401", "1"},
//
//                    {"活動狀態", null, null, "建置中", "0", "1"},
//                    {"活動狀態", null, null, "可增修取消報名", "1", "2"},
//                    {"活動狀態", null, null, "可修改取消報名", "2", "3"},
//                    {"活動狀態", null, null, "可取消報名", "3", "4"},
//                    {"活動狀態", null, null, "正常結束", "7", "5"},
//                    {"活動狀態", null, null, "取消活動", "9", "6"},
//
//                    {"活動身分別", null, null, "貴賓", "0", "1"},
//                    {"活動身分別", null, null, "信眾", "1", "2"},
//                    {"活動身分別", null, null, "義工", "2", "3"},
//                    {"活動身分別", null, null, "法師親眷", "3", "4"},
//                    {"活動身分別", null, null, "親眷", "4", "5"},
//                    {"活動身分別", null, null, "法師", "5", "6"},
//                    {"活動身分別", null, null, "打七", "6", "7"},
//                    {"活動身分別", null, null, "護七", "7", "8"},
//                    {"活動身分別", null, null, "戒子", "8", "9"},
//                    {"活動身分別", null, null, "護戒", "9", "10"},
//
//                    {"精舍活動行程類別", null, null, "法會", "PGM30001", "0"},
//                    {"精舍活動行程類別", null, null, "其他", "PGM30002", "1"},
//
//                    {"PGM項目類別", null, null, "總體項目", "PGM00", "1"},
//                    {"PGM項目類別", null, null, "一支香", "PGM01", "2"},
//                    {"PGM項目類別", null, null, "福慧出坡", "PGM02", "3"},
//                    {"PGM項目類別", null, null, "海眾留單", "PGM03", "4"},
//                    {"PGM項目類別", null, null, "一般通用", "PGM10", "5"},
//                    {"PGM項目類別", null, null, "巡禮", "PGM11", "6"},
//                    {"PGM項目類別", null, null, "法會", "PGM12", "7"},
//                    {"PGM項目類別", null, null, "禪七", "PGM17", "8"},
//                    {"PGM項目類別", null, null, "其它", "PGM99", "9"},
//            };
//
//            List<CodeDef> list = new ArrayList<>();
//
//            for (int i = 0; i < classCodeDefData.length; i++) {
//                CodeDef codeDef = new CodeDef();
//                codeDef.setCodeCategory(classCodeDefData[i][0]);
//                codeDef.setCodeGroup(classCodeDefData[i][1]);
//                codeDef.setCodeGroupId(classCodeDefData[i][2]);
//                codeDef.setCodeName(classCodeDefData[i][3]);
//                codeDef.setCodeId(classCodeDefData[i][4]);
//                codeDef.setOrderNum(Integer.parseInt(classCodeDefData[i][5]));
//                list.add(codeDef);
//            }
//
//            codeDefRepo.saveAll(list);
//        }
//    }

    // generate class id
//    public String classIdGenerator() {
//        Date date = new Date();
//        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//        int mingoYear = localDate.getYear() - 1911;
//
//        String criteria = "CLS" + String.valueOf(mingoYear);
//
//        String currentMaxValue = classInfoRepo.GetMaxAssetId(criteria + '%');
//
//        logger.debug("criteria: " + criteria);
//        logger.debug("current max: " + currentMaxValue);
//
//        long tempId = 0;
//
//        if (currentMaxValue != null) {
//
//            currentMaxValue = currentMaxValue.substring(3);
//
//            tempId = Long.parseLong(currentMaxValue);
//        } else {
//            tempId = Long.parseLong(criteria.substring(3)) * 100000;
//        }
//
//        tempId++;
//
//        return "CLS" + Long.toString(tempId);
//    }

//    private void setTwIdToMembers() {
//        logger.debug("set tw id num");
//        classCtMemberInfoService.setMemberTwId();
//    }

//    private void TestYubikey() {
//        logger.debug("keyid: " + yubicoService.getIdentity("ccccccectlfgvggbindindvfjhhrkubcfnkjljjrcvgv"));
//
//        boolean result = yubicoService.check("ccccccectlfgvggbindindvfjhhrkubcfnkjljjrcvgv");
//
//        if (result) {
//            logger.debug("yubikey ok");
//        } else {
//            logger.debug("yubikey not ok");
//        }
//    }

    private void AddSysCodeIfEmpty() {
        int count = sysCodeService.getAllSysCode().size();

        if (count == 0) {
            logger.debug("inserting sys code");

            List<SysCode> sysCodeLis = new ArrayList();

            switch (domain) {
                case "ctcm.org.tw":
                    // ctword
                    sysCodeLis.add(new SysCode("training_videos_path", "/opt/class_training_videos", "禪修班", "教育訓練影片路徑"));
                    sysCodeLis.add(new SysCode("training_docs_path", "/opt/class_training_docs", "禪修班", "教育訓練文件路徑"));
                    sysCodeLis.add(new SysCode("class_member_image_path", "/opt/class_member_images", "禪修班", "學員影像檔路徑"));
                    sysCodeLis.add(new SysCode("class_original_makeupfiles_path", "/opt/class_original_makeupfiles", "禪修班", "補課影音原始檔路徑"));
                    sysCodeLis.add(new SysCode("class_converted_makeupfiles_path", "/opt/class_converted_makeupfiles", "禪修班", "轉化後補課影音檔路徑"));
                    sysCodeLis.add(new SysCode("class_user_download_files_path", "/opt/class_download_files", "禪修班", "程式中給使用者下載的檔案"));
                    sysCodeLis.add(new SysCode("class_contact_info_on_login_page", "業務服務窗口: 法務組 見芃法師 /分機31203簡碼21037\n技術服務窗口: 傳千居士 / 分機31725 簡碼21248", "禪修班", "登入頁面業務窗口與技術窗口資訊"));
                    sysCodeLis.add(new SysCode("class_update_age_hour", "2", "禪修班", "每日更新年紀時間"));

                    break;
                case "box7000.com":
                    // box 200
                    sysCodeLis.add(new SysCode("training_videos_path", "/home/jack/temp/meditation/class_training_videos", "禪修班", "教育訓練影片路徑"));
                    sysCodeLis.add(new SysCode("training_docs_path", "/home/jack/temp/meditation/class_training_docs", "禪修班", "教育訓練文件路徑"));
                    sysCodeLis.add(new SysCode("class_member_image_path", "/home/jack/temp/meditation/class_member_images", "禪修班", "學員影像檔路徑"));
                    sysCodeLis.add(new SysCode("class_original_makeupfiles_path", "/home/jack/temp/meditation/class_original_makeupfiles", "禪修班", "補課影音原始檔路徑"));
                    sysCodeLis.add(new SysCode("class_converted_makeupfiles_path", "/home/jack/temp/meditation/class_converted_makeupfiles", "禪修班", "轉化後補課影音檔路徑"));
                    sysCodeLis.add(new SysCode("class_user_download_files_path", "/home/jack/temp/meditation/class_download_files", "禪修班", "程式中給使用者下載的檔案"));
                    sysCodeLis.add(new SysCode("class_contact_info_on_login_page", "業務服務窗口: 法務組 見芃法師 /分機31203簡碼21037\n技術服務窗口: 傳千居士 / 分機31725 簡碼21248", "禪修班", "登入頁面業務窗口與技術窗口資訊"));
                    sysCodeLis.add(new SysCode("class_update_age_hour", "10", "禪修班", "每日更新年紀時間"));

                    break;
            }

            sysCodeService.saveSysCode(sysCodeLis);
        }
    }

    private void CreateFilePath() {
        logger.debug("create file paths");

        String trainingDocs = sysCodeService.getTrainingDocsPath();
        String trainingVideos = sysCodeService.getTrainingVideosPath();
        String makeupOriginal = sysCodeService.getOriginalMakeupFilePath();
        String makeup = sysCodeService.getMakeupFilePath();
        String member = sysCodeService.getMemberImagePath();

        fileStorageService.createPath(trainingDocs);
        fileStorageService.createPath(trainingVideos);
        fileStorageService.createPath(makeupOriginal);
        fileStorageService.createPath(makeup);
        fileStorageService.createPath(member);
    }
}
