package tw.org.ctworld.meditation.views;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.ClassReport.ClassMemberRecord;
import tw.org.ctworld.meditation.services.FileStorageService;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class StudentInfoCardWordView {

    @Autowired
    private FileStorageService fileStorageService;


    private static final Logger logger = LoggerFactory.getLogger(StudentInfoCardWordView.class);


    public XWPFDocument generateDocument(List<ClassMemberRecord> results) {
        int size = results.size();

        XWPFDocument document = new XWPFDocument();

        // setup page
        CTBody body = document.getDocument().getBody();

        if (!body.isSetSectPr()) {
            body.addNewSectPr();
        }

        CTSectPr section = body.getSectPr();

        CTPageMar pageMar = section.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(720L));
        pageMar.setTop(BigInteger.valueOf(720L));
        pageMar.setRight(BigInteger.valueOf(720L));
        pageMar.setBottom(BigInteger.valueOf(720L));

        if (!section.isSetPgSz()) {
            section.addNewPgSz();
        }

        CTPageSz pageSize = section.getPgSz();
        pageSize.setOrient(STPageOrientation.LANDSCAPE);
        pageSize.setH(BigInteger.valueOf(11907));
        pageSize.setW(BigInteger.valueOf(16840));

        int pageCount = size / 10 + 1;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date now = Calendar.getInstance().getTime();

        for (int i = 0; i < pageCount; i++) {
            createPage(document, results, i, formatter.format(now));
        }

        return document;
    }

    private void createPage(XWPFDocument document, List<ClassMemberRecord> results, int index, String dateStr) {

        XWPFParagraph title = document.createParagraph();
        title.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun titleRun = title.createRun();
        titleRun.setFontSize(20);
        titleRun.setText(results.get(0).getClassName() + " - 學員資料卡");

        XWPFParagraph date = document.createParagraph();
        date.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun dateRun = date.createRun();
        dateRun.setFontSize(12);
        dateRun.setText("列印日期: " + dateStr);

        int itemCount = results.size() - index * 10;
        if (itemCount > 10) {
            itemCount = 10;
        }
        XWPFTable table = document.createTable(itemCount * 2 + 1, 17);
        table.setCellMargins(0, 0, 0, 0);

        // set cell width
        long widthS = (long) (1440 * 0.42);
        long widthM = (long) (1440 * 0.64);
        long widthM2 = (long) (1440 * 0.72);
        long widthL = (long) (1440 * 1.1);
        table.getCTTbl().addNewTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthS)); //0
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthM)); //1
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthS)); //2
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthL)); //3
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthM)); //4
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthS)); //5
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthS)); //6
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthM)); //7
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthS)); //8
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthS)); //9
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthL)); //10
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthM2)); //11
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthL)); //12
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthL)); //13
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthS)); //14
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthS)); //15
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(widthS)); //16

        CTTblWidth tblWidth = table.getRow(0).getCell(0).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthS));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(1).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthM));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(2).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthS));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(3).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthL));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(4).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthM));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(5).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthS));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(6).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthS));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(7).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthM));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(8).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthS));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(9).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthS));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(10).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthL));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(11).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthM2));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(12).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthL));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(13).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthL));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(14).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthS));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(15).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthS));
        tblWidth.setType(STTblWidth.DXA);
        tblWidth = table.getRow(0).getCell(16).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(widthS));
        tblWidth.setType(STTblWidth.DXA);

        // set titles
        String titleStrs[] = {"No.", "照片", "性別", "姓名", "法名", "年齡", "期別", "班別", "組別", "組號", "禪修班職事", "前期班別", "住家電話", "行動電話", "應上週數", "實上週數", "出席率"};

        for (int i = 0; i < titleStrs.length; i++) {
            XWPFTableRow row = table.getRow(0);
            XWPFTableCell cell = row.getCell(i);

            cell.removeParagraph(0);
            cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            XWPFParagraph paragraph = cell.addParagraph();
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun run = paragraph.createRun();

            run.setFontFamily("標楷體");
            run.setFontSize(12);
            run.setText(titleStrs[i]);
        }

        for (int i = 0; i < itemCount; i++) {
            int startRow = i * 2 + 1;
            int endRow = startRow + 1;
            // merge cells
            //no.
            mergeCellsVertically(table, 0, startRow, endRow);
            //photo
            mergeCellsVertically(table, 1, startRow, endRow);
            //address
            mergeCellsHorizontal(table, endRow, 2, 9);
            //company
            mergeCellsHorizontal(table, endRow, 10, 16);

            //publish data

            int dataIndex = index * 10 + i;

            if (dataIndex < results.size()) {
                ClassMemberRecord record = results.get(dataIndex);
                XWPFTableRow row1 = table.getRow(startRow);
                XWPFTableRow row2 = table.getRow(endRow);
                formatCell(row1.getCell(0), String.valueOf(dataIndex + 1), true, false);
                setPhoto(row1.getCell(1), record);
                formatCell(row1.getCell(2), record.getGender(), true, false);
                if (record.isDroppedClass()) {
                    formatCell(row1.getCell(3), record.getAliasName(), true, true);
                }else {
                    formatCell(row1.getCell(3), record.getAliasName(), true, false);
                }
                formatCell(row1.getCell(4), record.getCtDharmaName(), true, false);
                formatCell(row1.getCell(5), String.valueOf(record.getAge()), true, false);
                formatCell(row1.getCell(6), String.valueOf(record.getClassPeriodNum()), true, false);
                formatCell(row1.getCell(7), record.getClassName(), true, false);
                formatCell(row1.getCell(8), record.getClassGroupId(), true, false);
                formatCell(row1.getCell(9), record.getMemberGroupNum(), true, false);
                String titleStr = "";
                if (record.getClassJobTitleList() != null) {
                    String jobStr[] = record.getClassJobTitleList().split("/");
                    titleStr += jobStr[0];
                }
                formatCell(row1.getCell(10), titleStr, true, false);
                formatCell(row1.getCell(11), record.getPreviousClassName(), true, false);
                formatCell(row1.getCell(12), record.getHomePhoneNum1(), true, false);
                formatCell(row1.getCell(13), record.getMobileNum1(), true, false);
                formatCell(row1.getCell(14), String.valueOf(record.getAttendRecordList().size()), true, false);
                formatCell(row1.getCell(15), String.valueOf(record.getWeek()), true, false);
                formatCell(row1.getCell(16), String.valueOf(record.getPercent()), true, false);

                formatCell(row2.getCell(2), " 住址：" + (record.getAddress() == null ? "" : record.getAddress()), false, false);
                String companyStr = "";
                if (record.getCompanyNameAndJobTitle() != null) {
                    companyStr += record.getCompanyNameAndJobTitle();
                }
                formatCell(row2.getCell(10), " 公司職稱：" + companyStr, false, false);
            }
        }

        XWPFParagraph end = document.createParagraph();
        end.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun endRun = end.createRun();
//        endRun.setFontFamily("標楷體");
//        endRun.setFontSize(8);
//        endRun.setText( "\n" + String.valueOf(index + 1) + " - " + String.valueOf(results.size() / 10 + 1));

        endRun.addBreak();
    }


    private void formatCell(XWPFTableCell cell, String text, boolean center, boolean cancel) {
        if (text != null && !text.isEmpty()) {

            cell.removeParagraph(0);

            XWPFParagraph paragraph = cell.addParagraph();

            XWPFRun run = paragraph.createRun();

            run.setFontFamily("標楷體");
            run.setFontSize(12);

            run.setText(text);

            if (cancel) {
                XWPFRun run2 = paragraph.createRun();
                run2.setFontFamily("標楷體");
                run2.setFontSize(12);
                run2.setColor("ff0000");
                run2.setText("(取消)");
            }

            if (center) paragraph.setAlignment(ParagraphAlignment.CENTER);

            cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
        }
    }

    private void setPhoto(XWPFTableCell cell, ClassMemberRecord record) {
        cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
        XWPFParagraph paragraph = cell.addParagraph();

        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();

        String imgFile;

        try {
            logger.debug("photo: " + record.getPhoto());

            if (record.getPhoto() == null || record.getPhoto().isEmpty()) {
                imgFile = "no_avatar.jpg";
                run.addPicture(new ClassPathResource(imgFile).getInputStream(), XWPFDocument.PICTURE_TYPE_JPEG, imgFile, Units.toEMU(40), Units.toEMU(40));

            } else {
                imgFile = record.getPhoto();
                Resource resource = fileStorageService.loadFile(0, imgFile);
                run.addPicture(resource.getInputStream(), XWPFDocument.PICTURE_TYPE_JPEG, imgFile, Units.toEMU(40), Units.toEMU(40));
            }

            cell.removeParagraph(0);
        } catch (Exception e) {
            logger.error("image not found");
        }
    }

    private void mergeCellsVertically(XWPFTable table, int col, int fromRow, int toRow) {
        for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
            XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
            if (rowIndex == fromRow) {
                // The first merged cell is set with RESTART merge value
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.RESTART);
            } else {
                // Cells which join (merge) the first one, are set with CONTINUE
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.CONTINUE);
            }
        }
    }

    private void mergeCellsHorizontal(XWPFTable table, int row, int fromCell, int toCell) {
        for (int cellIndex = fromCell; cellIndex <= toCell; cellIndex++) {
            XWPFTableCell cell = table.getRow(row).getCell(cellIndex);
            if (cellIndex == fromCell) {
                cell.getCTTc().addNewTcPr().addNewHMerge()
                        .setVal(STMerge.RESTART);
            } else {
                cell.getCTTc().addNewTcPr().addNewHMerge()
                        .setVal(STMerge.CONTINUE);
            }
        }
    }
}
