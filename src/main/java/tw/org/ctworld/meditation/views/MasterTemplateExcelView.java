package tw.org.ctworld.meditation.views;

import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class MasterTemplateExcelView extends AbstractXlsxView {
    private HttpSession session;

    public MasterTemplateExcelView(HttpSession session){
        super();
        this.session = session;
    }

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        String[][] items=new String[][]{ {"",""},{"",""}};
        String titleStr = "匯入法師名單格式檔";
        String[][] results=(String[][])model.get("results");
        results=new String[][]{ {"",""},{"",""}};
        String[] headStrings = {"衣編", "法名"};
        String   filename = "";
        filename=java.net.URLEncoder.encode(filename, StandardCharsets.UTF_8.name());
        response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + filename);
        // create excel xls sheet
        Sheet sheet = workbook.createSheet(titleStr);

        // create style for title cells
        CellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HorizontalAlignment.CENTER);

        Font font = workbook.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeight((short) (18*20));
        titleStyle.setFont(font);

        // create style for header cells
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.CENTER);
        headStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());

        Font headFont = workbook.createFont();
        headFont.setFontName("Arial");
        headFont.setBold(true);
        headStyle.setFont(headFont);

        // create style for other cells
        CellStyle otherStyle = workbook.createCellStyle();
        otherStyle.setAlignment(HorizontalAlignment.CENTER);

        Font otherFont = workbook.createFont();
        font.setBold(true);
        otherFont.setColor(IndexedColors.BLACK.getIndex());
        otherStyle.setFont(otherFont);

        int rowCount = 0;

//        Row title = sheet.createRow(rowCount);
//        title.setHeight((short) (25*20));
//        Cell titleCell = title.createCell(0);
//        titleCell.setCellValue(titleStr);
//        titleCell.setCellStyle(titleStyle);
//
//        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, headStrings.length - 1));
//        rowCount++;

        // create header row
        Row header = sheet.createRow(rowCount);
        header.setHeight((short) (25*20));

        int cellIndex = 0;
        for (int i = 0; i < headStrings.length; i++) {
            Cell cell = header.createCell(cellIndex++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headStrings[i]);
        }

        rowCount++;

        int dataIndex = 1;

        for(String[] ele:results ){
            int columnIndex = 0;
            Row dataRow = sheet.createRow(rowCount++);
            dataRow.createCell(columnIndex++).setCellValue(ele[0]);
            dataRow.createCell(columnIndex++).setCellValue(ele[1]);
            dataRow.setRowStyle(otherStyle);
        }

        for (int i = 0 ; i < headStrings.length ; i++) {
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, (int)(sheet.getColumnWidth(i) * 2));
        }
    }
}
