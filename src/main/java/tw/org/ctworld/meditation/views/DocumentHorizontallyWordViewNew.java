package tw.org.ctworld.meditation.views;



import com.microsoft.schemas.office.word.STWrapType;
import com.microsoft.schemas.vml.CTGroup;
import com.microsoft.schemas.vml.CTShape;
import com.microsoft.schemas.vml.CTTextbox;
import com.microsoft.schemas.vml.STTrueFalse;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;
import tw.org.ctworld.meditation.beans.Printer.Printer007Object;
import tw.org.ctworld.meditation.beans.Printer.Printer011Object;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DocumentHorizontallyWordViewNew {

    private static final Logger logger = LoggerFactory.getLogger(DocumentHorizontallyWordViewNew.class);

    public XWPFDocument generateDocument(int type, List<Printer007Object> classCertFieldSettingList,
                                         List<Printer011Object> memberNameAndAgeList, String certContent,
                                         String certMaster, String certDate) {

        XWPFDocument document = new XWPFDocument();

        // start setup page
        CTBody body = document.getDocument().getBody();

        if(!body.isSetSectPr()){
            body.addNewSectPr();
        }

        CTSectPr section = body.getSectPr();

        CTPageMar pageMar = section.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(720L));
        pageMar.setTop(BigInteger.valueOf(720L));
        pageMar.setRight(BigInteger.valueOf(720L));
        pageMar.setBottom(BigInteger.valueOf(720L));

        if (!section.isSetPgSz()) {
            section.addNewPgSz();
            section.addNewPgSz();
        }

        // 1cm = 567twip
        CTPageSz pageSize = section.getPgSz();
        pageSize.setOrient(STPageOrientation.LANDSCAPE);
        pageSize.setH(BigInteger.valueOf(11113));
        pageSize.setW(BigInteger.valueOf(15366));
        // end setup page

        for (int x = 0 ; x < memberNameAndAgeList.size() ; x++) {

            XWPFParagraph paragraph = document.createParagraph();
            XWPFRun run = paragraph.createRun();

            String name = memberNameAndAgeList.get(x).getName();
            String age = memberNameAndAgeList.get(x).getAge();
            String[] certContentList = certContent.split(";");
            String[] certMasterList = certMaster.split(";");
            String[] certDateList = certDate.split(";");
            Printer007Object classCertFieldSetting = null;

            switch (type) {
                case 1: // 結業證書

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("1")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),name, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),age, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    if (certContentList.length == 2) {
                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3-1")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3-3")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                    }else {
                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3-1")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3-2")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3-3")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[2].replace("～", "~"), classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                    }

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("4")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("5")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("6")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("7")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("8")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                    break;
                case 2: // 全勤證書

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("1")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),name, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    if (certContentList.length == 2) {
                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2-1")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2-3")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                    }else {
                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2-1")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2-2")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2-3")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[2].replace("～", "~"), classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                    }

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("4")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("5")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("6")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("7")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getVerticalAlignType(), classCertFieldSetting.getIsItalic());
                    break;
            }

            if (x != memberNameAndAgeList.size() - 1) {
                run.addBreak(BreakType.PAGE);
            }

        }


        return  document;
    }

    private void addTextBox(XWPFDocument document, XWPFParagraph paragraph, XWPFRun run, int width, int height,
                            String text, int marginLeft, int marginTop, int fontSize, String fontFamily, double spacing,
                            boolean isBold, String align, boolean isItalic) {
        CTGroup ctGroup = CTGroup.Factory.newInstance();

        CTShape ctShape = ctGroup.addNewShape();
        ctShape.setStroked(STTrueFalse.F);
        ctShape.addNewWrap().setType(STWrapType.SQUARE);

        ctShape.setStyle("position:absolute;margin-top:" + marginTop + "pt;margin-left:" + marginLeft + "pt;width:" + width + "px;height:" + height + "px;border-top:0px;border-left:0px;border-bottom:0px;border-right:0px;");

        CTTextbox ctTextbox = ctShape.addNewTextbox();
        ctTextbox.setStyle("layout-flow:vertical-ideographic");

        CTTxbxContent ctTxbxContent = ctShape.addNewTextbox().addNewTxbxContent();
        XWPFParagraph textBoxParagraph = new XWPFParagraph(ctTxbxContent.addNewP(), (IBody)document);
        CTParaRPr ctParaRPr = textBoxParagraph.getCTP().addNewPPr().addNewRPr();
        ctParaRPr.addNewEastAsianLayout().setVert(STOnOff.X_1);
        textBoxParagraph.setSpacingBetween(spacing);
        switch (align) {
            case "上":
                textBoxParagraph.setAlignment(ParagraphAlignment.LEFT);
                break;
            case "中":
                textBoxParagraph.setAlignment(ParagraphAlignment.CENTER);
                break;
            case "下":
                textBoxParagraph.setAlignment(ParagraphAlignment.RIGHT);
                break;
            case "文字等分":
                textBoxParagraph.setAlignment(ParagraphAlignment.DISTRIBUTE);
                break;
        }

        XWPFRun textBoxRun = textBoxParagraph.createRun();
        textBoxRun.setFontFamily(fontFamily);
        textBoxRun.setText(text);
        textBoxRun.setFontSize(fontSize);
        textBoxRun.setBold(isBold);
        textBoxRun.setItalic(isItalic);

        try {
            Node ctGroupNode = ctGroup.getDomNode();
            CTPicture ctPicture = CTPicture.Factory.parse(ctGroupNode);
            run = paragraph.createRun();
            CTR cTR = run.getCTR();
            cTR.addNewPict();
            cTR.setPictArray(0, ctPicture);
        } catch (XmlException e) {
            e.printStackTrace();
        }
    }

}
