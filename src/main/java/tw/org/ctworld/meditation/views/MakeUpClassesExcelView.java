package tw.org.ctworld.meditation.views;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import tw.org.ctworld.meditation.beans.ClassReport.ClassAttendance;
import tw.org.ctworld.meditation.beans.ClassReport.ClassMemberRecord;
import tw.org.ctworld.meditation.beans.ClassStats.ClassStats008Response;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class MakeUpClassesExcelView extends AbstractXlsxView {

    private HttpSession session;

    public MakeUpClassesExcelView(HttpSession session) {
        super();
        this.session = session;
    }

    private static final Logger logger = LoggerFactory.getLogger(MakeUpClassesExcelView.class);

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<ClassStats008Response.ClassStats008_1Object> results =
                (List<ClassStats008Response.ClassStats008_1Object>) model.get("results");
        ClassStats008Response.ClassStats008_1Object sample = null;

        String titleStr = "補課名冊";
        String[] headStrings = {"No.", "學員編號", "期別", "班別", "性別", "姓名", "法名", "組別", "組號", "起日", "迄日",
                "全勤", "修業狀況"};

        // change the file name
        String filename = "";

        if (results.size() == 0) {

            filename = titleStr + "_無資料匯出";
        }else {

            sample = results.get(0);
            String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());
            String userName = (String) session.getAttribute(UserSession.Keys.UserName.getValue());

            filename = unitName + "_" + sample.getClassPeriodNum() + "_" + sample.getClassName() + "_" + titleStr +
                    "_" + userName + "_" + CommonUtils.GetCurrentTimeString() + ".xls";
        }

        filename = URLEncoder.encode(filename, StandardCharsets.UTF_8.name());

        response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + filename);

        // create excel xls sheet
        Sheet sheet = workbook.createSheet(titleStr);

        // create style for title cells
        CellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HorizontalAlignment.CENTER);

        Font font = workbook.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeight((short) (18*20));
        titleStyle.setFont(font);

        // create style for header cells
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.CENTER);

        Font headFont = workbook.createFont();
        headFont.setFontName("Arial");
        headFont.setBold(true);
        headStyle.setFont(headFont);

        // create style for other cells
        CellStyle otherStyle = workbook.createCellStyle();
        otherStyle.setAlignment(HorizontalAlignment.CENTER);

        Font otherFont = workbook.createFont();
        font.setBold(true);
        otherFont.setColor(IndexedColors.BLACK.getIndex());
        otherStyle.setFont(otherFont);

        int rowCount = 0;

        Row title = sheet.createRow(rowCount);
        title.setHeight((short) (25*20));
        Cell titleCell = title.createCell(0);
        titleCell.setCellValue(titleStr);
        titleCell.setCellStyle(titleStyle);

        if (sample != null) {
            sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, headStrings.length + sample.getAttendRecordList().size() - 1));
        }else {
            sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, headStrings.length - 1));
        }

        rowCount++;

        // create header row
        Row header = sheet.createRow(rowCount);
        header.setHeight((short) (25*20));

        int cellIndex = 0;
        for (int i = 0; i < headStrings.length; i++) {
            Cell cell = header.createCell(cellIndex++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headStrings[i]);

            if (i == 12) {
                for (int j = 0; j < sample.getAttendRecordList().size(); j++) {
                    Cell cellD = header.createCell(cellIndex++);
                    cellD.setCellStyle(headStyle);
                    cellD.setCellValue(j + 1);
                }
            }
        }

        rowCount++;

        int dataIndex = 1;

        for (ClassStats008Response.ClassStats008_1Object record: results) {
            int columnIndex = 0;
            Row dataRow = sheet.createRow(rowCount++);
            dataRow.createCell(columnIndex++).setCellValue(dataIndex++);
            dataRow.createCell(columnIndex++).setCellValue(record.getMemberId());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassPeriodNum());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassName());
            dataRow.createCell(columnIndex++).setCellValue(record.getGender());
            dataRow.createCell(columnIndex++).setCellValue(record.getAliasName());
            dataRow.createCell(columnIndex++).setCellValue(record.getCtDharmaName());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassGroupId());
            dataRow.createCell(columnIndex++).setCellValue(record.getMemberGroupNum());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassStartDate().toString().split(" ")[0]);
            dataRow.createCell(columnIndex++).setCellValue(record.getClassEndDate().toString().split(" ")[0]);
            dataRow.createCell(columnIndex++).setCellValue(record.getIsFullAttended() ? "V" : "");
            dataRow.createCell(columnIndex++).setCellValue(record.getClassAttendedResult());

            for (ClassStats008Response.ClassStats008_2Object attend: record.getAttendRecordList()){

                Cell cell = dataRow.createCell(columnIndex++);

                if (attend.getColor().equals("R")) {

                    otherFont.setColor(IndexedColors.RED.getIndex());
                    otherStyle.setFont(otherFont);
                    cell.setCellStyle(otherStyle);
                }

                cell.setCellValue(attend.getAttendStatus());
            }
        }

        for (int i = 0; i < headStrings.length + sample.getAttendRecordList().size(); i++) {
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, (int)(sheet.getColumnWidth(i) * 2));
        }
    }
}
