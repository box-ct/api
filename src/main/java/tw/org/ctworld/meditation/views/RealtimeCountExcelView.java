package tw.org.ctworld.meditation.views;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import tw.org.ctworld.meditation.beans.DataReport.DataReport003Response;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class RealtimeCountExcelView extends AbstractXlsxView {

    private HttpSession session;

    public RealtimeCountExcelView(HttpSession session) {
        super();
        this.session = session;
    }

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<DataReport003Response.DataReport003_1Object> results =
                (List<DataReport003Response.DataReport003_1Object>) model.get("results");
//        DataReport003Response.DataReport003_1Object sample = results.get(0);

        String titleStr = "禪修班即時報到統計表(當日)";
        String[] headStrings = {"No.", "單位", "日間初級班", "夜間初級班", "日間中級班", "夜間中級班", "日間高級班",
                "夜間高級班", "日間研一班", "夜間研一班", "日間研二班", "夜間研二班", "其他"};

        // change the file name
        String filename = "";

        if (results.size() == 0) {

            filename = titleStr + "_無資料匯出";
        }else {

            DataReport003Response.DataReport003_1Object sample = results.get(0);
            String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());
            String userName = (String) session.getAttribute(UserSession.Keys.UserName.getValue());

            filename = unitName + "_" + titleStr + "_" + userName + "_" + CommonUtils.GetCurrentTimeString() + ".xls";
        }

        filename = java.net.URLEncoder.encode(filename, StandardCharsets.UTF_8.name());

        response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + filename);

        // create excel xls sheet
        Sheet sheet = workbook.createSheet(titleStr);

        // create style for title cells
        CellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HorizontalAlignment.CENTER);

        Font font = workbook.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeight((short) (18*20));
        titleStyle.setFont(font);

        // create style for header cells
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.CENTER);

        Font headFont = workbook.createFont();
        headFont.setFontName("Arial");
        headFont.setBold(true);
        headStyle.setFont(headFont);

        // create style for other cells
        CellStyle otherStyle = workbook.createCellStyle();
        otherStyle.setAlignment(HorizontalAlignment.CENTER);

        int rowCount = 0;

        Row title = sheet.createRow(rowCount);
        title.setHeight((short) (25*20));
        Cell titleCell = title.createCell(0);
        titleCell.setCellValue(titleStr);
        titleCell.setCellStyle(titleStyle);

        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, headStrings.length - 1));
        rowCount++;

        // create header row
        Row header = sheet.createRow(rowCount);
        header.setHeight((short) (25*20));

        int cellIndex = 0;
        for (int i = 0; i < headStrings.length; i++) {
            Cell cell = header.createCell(cellIndex++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headStrings[i]);
        }

        rowCount++;

        int dataIndex = 1;

        for (DataReport003Response.DataReport003_1Object record: results) {
            int columnIndex = 0;
            Row dataRow = sheet.createRow(rowCount++);
            dataRow.createCell(columnIndex++).setCellValue(dataIndex++);
            dataRow.createCell(columnIndex++).setCellValue(record.getUnitName());
            for (DataReport003Response.DataReport003_2Object record2 : record.getClasses()) {
                dataRow.createCell(columnIndex++).setCellValue(record2.getNumber());
            }

            dataRow.setRowStyle(otherStyle);
        }

        for (int i = 0 ; i < headStrings.length ; i++) {
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, (int)(sheet.getColumnWidth(i) * 2));
        }
    }
}
