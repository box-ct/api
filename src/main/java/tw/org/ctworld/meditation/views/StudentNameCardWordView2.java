package tw.org.ctworld.meditation.views;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.ClassReport.ClassMemberRecord;
import tw.org.ctworld.meditation.services.FileStorageService;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Service
public class StudentNameCardWordView2 {
    @Autowired
    private FileStorageService fileStorageService;

    private static final Logger logger = LoggerFactory.getLogger(StudentNameCardWordView2.class);


    public XWPFDocument generateDocument(List<ClassMemberRecord> results, int type, String unitName) {

        int size = results.size();

        XWPFDocument document = new XWPFDocument();

        // setup page
        CTBody body = document.getDocument().getBody();

        if(!body.isSetSectPr()){
            body.addNewSectPr();
        }

        CTSectPr section = body.getSectPr();

        CTPageMar pageMar = section.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(720L));
        pageMar.setTop(BigInteger.valueOf(720L));
        pageMar.setRight(BigInteger.valueOf(720L));
        pageMar.setBottom(BigInteger.valueOf(720L));

        if (!section.isSetPgSz()) {
            section.addNewPgSz();
        }

        CTPageSz pageSize = section.getPgSz();
        pageSize.setOrient(STPageOrientation.PORTRAIT);
        pageSize.setW(BigInteger.valueOf(11907));
        pageSize.setH(BigInteger.valueOf(16840));

        // create table
//        XWPFTable table;
//        XWPFParagraph paragraph;
//        XWPFRun run;

        // cal number of rows
        int numRows = size / 2 + 1;
        int numCols = 2;

//        document.createParagraph();

        XWPFTable table = document.createTable(numRows, numCols);
        table.getCTTbl().getTblPr().unsetTblBorders();

        int tableWidth = 10467;
        table.setWidth(tableWidth);

        table.getCTTbl().addNewTblGrid().addNewGridCol().setW(BigInteger.valueOf(tableWidth/numCols));
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(tableWidth/numCols));

        CTTblWidth tblWidth = table.getRow(0).getCell(0).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(tableWidth/numCols));
        tblWidth.setType(STTblWidth.DXA);

        tblWidth = table.getRow(0).getCell(1).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(tableWidth/numCols));
        tblWidth.setType(STTblWidth.DXA);

        for (int i = 0; i < size; i++) {
            int rowIndex = i / 2;
            int colIndex = i % 2;

            XWPFTableRow row = table.getRow(rowIndex);
            XWPFTableCell cell = row.getCell(colIndex);

            row.setCantSplitRow(true);

            XmlCursor cursor = cell.getParagraphs().get(0).getCTP().newCursor();

            // create table
            XWPFTable innerTable = cell.insertNewTbl(cursor);

            innerTable.removeRow(0);

            XWPFTableRow row1 = innerTable.createRow();

            row1.createCell();
            row1.createCell();
            row1.createCell();
            row1.createCell();

            innerTable.createRow();
            innerTable.createRow();
            innerTable.createRow();
            innerTable.createRow();
            innerTable.createRow();

            mergeCellsHorizontal(innerTable,0, 1, 3);
            mergeCellsHorizontal(innerTable,1, 0, 1);
            mergeCellsHorizontal(innerTable,1, 2, 3);
            mergeCellsHorizontal(innerTable,2, 0, 1);
            mergeCellsHorizontal(innerTable,2, 2, 3);
            mergeCellsHorizontal(innerTable,3, 0, 1);
            mergeCellsHorizontal(innerTable,4, 0, 1);

            mergeCellsVertically(innerTable, 0, 1, 4);
            mergeCellsVertically(innerTable, 3, 3, 4);

            ClassMemberRecord record = results.get(i);

            // 0: cht, 1: eng
            String classStr;
            String nameStr;
            String titleStr;
            String dNameStr;


            if (type == 0) {
                classStr = "週" + record.getDayOfWeek() + record.getDayOrNight() + " ";
                nameStr = "";
                titleStr = "";
                dNameStr = "";
            } else {
                classStr = "";
                switch (record.getDayOfWeek()) {
                    case "一":
                        classStr += "Mon.";
                        break;
                    case "二":
                        classStr += "Tue.";
                        break;
                    case "三":
                        classStr += "Wen.";
                        break;
                    case "四":
                        classStr += "Thu.";
                        break;
                    case "五":
                        classStr += "Fri.";
                        break;
                    case "六":
                        classStr += "Sat.";
                        break;
                    case "日":
                        classStr += "Sun.";
                        break;
                }

                classStr += " ";

                switch (record.getDayOrNight()) {
                    case "日間":
                        classStr += "Day ";
                        break;
                    case "夜間":
                        classStr += "Night ";
                        break;
                }

                nameStr = "";
                titleStr = "";
                dNameStr = "";
            }

            classStr += record.getClassName();

            if (record.getAliasName() != null) {
                nameStr += record.getAliasName();
            }

            if (record.getClassJobTitleList() != null) {
                String jobStr[] = record.getClassJobTitleList().split("/");
                titleStr += jobStr[0];
            }

            if (record.getCtDharmaName() == null) {
                dNameStr += "";
            } else {
                dNameStr += record.getCtDharmaName();
            }

            String titleFooterColor = "";
            switch (record.getDayOrNight()) {
                case "日間":
                    titleFooterColor = "FFFFFF";
                    break;
                case "夜間":
                    titleFooterColor = "000000";
                    break;
            }
            formatCell(innerTable.getRow(0).getCell(1), classStr, true, 14, true, titleFooterColor, type);
            formatCell(innerTable.getRow(1).getCell(2), titleStr, false, 11, false,"e50000", type);
            formatCell(innerTable.getRow(2).getCell(2), nameStr, false, 18, true, "000000", type);
            formatCell(innerTable.getRow(3).getCell(2), dNameStr, false, 18, true, "000000", type);

            if (type == 0) {
                formatCell(innerTable.getRow(5).getCell(2), unitName + "精舍", true, 14, true, titleFooterColor, type);
            }else {
                formatCell(innerTable.getRow(5).getCell(2), "Center of "+ unitName, true, 12, true, titleFooterColor, type);
            }


            String genderStr;
            if (type == 0) {
                genderStr = record.getGender();
            } else {
                if (record.getGender().equals("男")){
                    genderStr = "Male";
                } else {
                    genderStr = "Female";
                }
            }


            if (type == 0) {
                formatCell(innerTable.getRow(4).getCell(2), genderStr + record.getClassGroupId() + "組#" + record.getMemberId(),
                        false, 11, false, "000000", type);
            }else {
                formatCell(innerTable.getRow(4).getCell(2), genderStr + " Group " + record.getClassGroupId() + "#" + record.getMemberId(),
                        false, 11, false, "000000", type);
            }

            setRowHeight(innerTable);

            setPhoto(innerTable.getRow(1).getCell(0), record);
            setQRCode(innerTable.getRow(3).getCell(3), record);
            setIcon(innerTable.getRow(0).getCell(0),false);
            setIcon(innerTable.getRow(5).getCell(1),true);
            setCellColor(innerTable, record.getDayOrNight(), record.getClassTypeName());

            XWPFRun run = cell.getParagraphs().get(0).createRun();

            run.setFontSize(7);
            run.setText(" ");

//            cell.addParagraph();
        }

        return document;
    }


    // set height and borders
    private void setRowHeight(XWPFTable table) {

        //1 in = 1440
        for(int i = 0; i < table.getRows().size(); i++) {
            switch (i) {
                case 0:
                    table.getRow(i).setHeight(454);
                    break;
                case 5:
                    table.getRow(i).setHeight(454);
                    break;
            }
        }

        int cw1 = 720;
        int cw2 = 720;
        int cw3 = 1944;
        int cw4 = 1440;

        int tableWidth = cw1 + cw2 + cw3 + cw4;
        table.setWidth(tableWidth);

        table.getCTTbl().addNewTblGrid().addNewGridCol().setW(BigInteger.valueOf(cw1));
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(cw2));
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(cw3));
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(cw4));

        CTTblWidth tblWidth = table.getRow(3).getCell(0).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(cw1));
        tblWidth.setType(STTblWidth.DXA);

        tblWidth = table.getRow(3).getCell(1).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(cw2));
        tblWidth.setType(STTblWidth.DXA);

        tblWidth = table.getRow(3).getCell(2).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(cw3));
        tblWidth.setType(STTblWidth.DXA);

        tblWidth = table.getRow(3).getCell(3).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(cw4));
        tblWidth.setType(STTblWidth.DXA);

        List<CTBorder> borders = new ArrayList<>();

//        for(XWPFTableRow row: table.getRows()) {
//            for (XWPFTableCell cell: row.getTableCells()) {
//                CTTcBorders borderC = cell.getCTTc().addNewTcPr().addNewTcBorders();
//                CTBorder borderT = borderC.addNewTop();
//                borderT.setVal(STBorder.SINGLE);
//                borderT.setSz(BigInteger.valueOf(5));
//                borderT.setColor("000000");
//                CTBorder borderR = borderC.addNewRight();
//                borderR.setVal(STBorder.SINGLE);
//                borderR.setSz(BigInteger.valueOf(5));
//                borderR.setColor("000000");
//                CTBorder borderB = borderC.addNewBottom();
//                borderB.setVal(STBorder.SINGLE);
//                borderB.setSz(BigInteger.valueOf(5));
//                borderB.setColor("000000");
//                CTBorder borderL = borderC.addNewLeft();
//                borderL.setVal(STBorder.SINGLE);
//                borderL.setSz(BigInteger.valueOf(5));
//                borderL.setColor("000000");
//            }
//        }

        for(CTBorder border: borders) {
            border.setColor("000000");
            border.setVal(STBorder.BASIC_WIDE_MIDLINE);
        }
    }

    private void setPhoto(XWPFTableCell cell, ClassMemberRecord record) {
        cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
        XWPFParagraph paragraph = cell.addParagraph();

        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();

        String imgFile;

        try {
            if (record.getPhoto() == null || record.getPhoto().isEmpty()) {
                imgFile = "no_avatar.jpg";
                run.addPicture(new ClassPathResource(imgFile).getInputStream(), XWPFDocument.PICTURE_TYPE_JPEG, imgFile, Units.toEMU(60), Units.toEMU(72));
            } else {
                imgFile = record.getPhoto();
                Resource resource = fileStorageService.loadFile(0, imgFile);
                run.addPicture(resource.getInputStream(), XWPFDocument.PICTURE_TYPE_JPEG, imgFile, Units.toEMU(60), Units.toEMU(72));
            }

            cell.removeParagraph(0);
        } catch (Exception e) {
            logger.error("image not found");
        }
    }

    private void setIcon(XWPFTableCell cell, boolean alpha) {
        cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
        XWPFParagraph paragraph = cell.addParagraph();

        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();

        String imgFile;

        try {

            if (alpha) {
                imgFile = "logo_alpha0.png";
                run.addPicture(new ClassPathResource(imgFile).getInputStream(), XWPFDocument.PICTURE_TYPE_PNG, imgFile, Units.toEMU(15), Units.toEMU(21));
            }else {
                imgFile = "logo.png";
                run.addPicture(new ClassPathResource(imgFile).getInputStream(), XWPFDocument.PICTURE_TYPE_PNG, imgFile, Units.toEMU(15), Units.toEMU(21));
            }

            cell.removeParagraph(0);
        } catch (Exception e) {
            logger.error("image not found");
        }
    }

    private void setQRCode(XWPFTableCell cell, ClassMemberRecord record) {
        cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

        try {
            XWPFParagraph paragraph = cell.addParagraph();

            paragraph.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun run = paragraph.createRun();

            QRCodeWriter qrCodeWriter = new QRCodeWriter();

            Hashtable hints = new Hashtable();
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hints.put(EncodeHintType.MARGIN, 0);

            BitMatrix bitMatrix = qrCodeWriter.encode(String.valueOf(record.getMemberId()), BarcodeFormat.QR_CODE, 100, 100, hints);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "PNG", out);

            run.addPicture(new ByteArrayInputStream(out.toByteArray()), XWPFDocument.PICTURE_TYPE_PNG, "", Units.toEMU(60), Units.toEMU(60));

            cell.removeParagraph(0);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    private void setCellColor(XWPFTable innerTable, String ClassDayOrNight, String classTypeName) {

        String color = "";

        XWPFTableCell cell1 = innerTable.getRow(0).getCell(0);
        XWPFTableCell cell2 = innerTable.getRow(0).getCell(1);
        XWPFTableCell cell3 = innerTable.getRow(5).getCell(0);
        XWPFTableCell cell4 = innerTable.getRow(5).getCell(1);
        XWPFTableCell cell5 = innerTable.getRow(5).getCell(2);
        XWPFTableCell cell6 = innerTable.getRow(5).getCell(3);

        switch (ClassDayOrNight) {
            case "日間":

                switch (classTypeName) {
                    case "初級班":
                        color = "DD570A";
                        break;
                    case "中級班":
                        color =  "247328";
                        break;
                    case "高級班":
                        color = "5A113B";
                        break;
                    case "研一班":
                        color = "133396";
                        break;
                    case "研二班":
                        color = "EC155E";
                        break;
                    case "青少年班":
                        color = "D8930A";
                        break;
                    case "長青班":
                        color = "D8930A";
                        break;
                    case "兒童班":
                        color = "D8930A";
                        break;
                    case "念佛班":
                        color = "D8930A";
                        break;
                    case "梵唄班":
                        color = "D8930A";
                        break;
                    case "才藝班":
                        color = "D8930A";
                        break;
                    case "共修班":
                        color = "D8930A";
                        break;
                }
                break;
            case "夜間":

                switch (classTypeName) {
                    case "初級班":
                        color = "FFA14D";
                        break;
                    case "中級班":
                        color = "9ADA4D";
                        break;
                    case "高級班":
                        color = "DEC0E4";
                        break;
                    case "研一班":
                        color = "91C3F1";
                        break;
                    case "研二班":
                        color = "FFB6DE";
                        break;
                    case "青少年班":
                        color = "FDCE32";
                        break;
                    case "長青班":
                        color = "FDCE32";
                        break;
                    case "兒童班":
                        color = "FDCE32";
                        break;
                    case "念佛班":
                        color = "FDCE32";
                        break;
                    case "梵唄班":
                        color = "FDCE32";
                        break;
                    case "才藝班":
                        color = "FDCE32";
                        break;
                    case "共修班":
                        color = "FDCE32";
                        break;
                }
                break;
        }

        cell1.setColor(color);
        cell2.setColor(color);
        cell3.setColor(color);
        cell4.setColor(color);
        cell5.setColor(color);
        cell6.setColor(color);

        innerTable.getRow(1).getCell(0).setColor("FAF6EE");
        innerTable.getRow(1).getCell(2).setColor("FAF6EE");
        innerTable.getRow(2).getCell(0).setColor("FAF6EE");
        innerTable.getRow(2).getCell(2).setColor("FAF6EE");
        innerTable.getRow(3).getCell(0).setColor("FAF6EE");
        innerTable.getRow(3).getCell(2).setColor("FAF6EE");
        innerTable.getRow(3).getCell(3).setColor("FAF6EE");
        innerTable.getRow(4).getCell(0).setColor("FAF6EE");
        innerTable.getRow(4).getCell(2).setColor("FAF6EE");
        innerTable.getRow(4).getCell(3).setColor("FAF6EE");
    }

    private void formatCell(XWPFTableCell cell, String text, boolean center, int fontSize, boolean bold, String fontColor, int type) {
        if (text != null && !text.isEmpty()) {

            cell.removeParagraph(0);

            XWPFParagraph paragraph = cell.addParagraph();

            XWPFRun run = paragraph.createRun();

            run.setFontFamily("微軟正黑體");
            run.setFontSize(fontSize);
            run.setBold(bold);
            run.setColor(fontColor);

            String str[] = text.split("#");

            if (str.length > 1) {
                paragraph.setSpacingBetween(0.8);

                for (int i = 0; i < str.length; i++) {

                    run.setText(str[i]);

                    if (i < str.length - 1) {
                        run.addBreak();
                    }
                }
            } else {
                run.setText(text);
            }

            if (fontSize == 18) {
                paragraph.setSpacingBetween(0.8);
            }

            if (center) paragraph.setAlignment(ParagraphAlignment.CENTER);

            cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
        }
    }

    private void mergeCellsHorizontal(XWPFTable table, int row, int fromCell, int toCell) {
        for (int cellIndex = fromCell; cellIndex <= toCell; cellIndex++) {
            XWPFTableCell cell = table.getRow(row).getCell(cellIndex);
            if (cellIndex == fromCell) {
                cell.getCTTc().addNewTcPr().addNewHMerge()
                        .setVal(STMerge.RESTART);
            } else {
                cell.getCTTc().addNewTcPr().addNewHMerge()
                        .setVal(STMerge.CONTINUE);
            }
        }
    }

    private  void mergeCellsVertically(XWPFTable table, int col, int fromRow, int toRow) {
        for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
            XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
            if ( rowIndex == fromRow ) {
                // The first merged cell is set with RESTART merge value
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.RESTART);
            } else {
                // Cells which join (merge) the first one, are set with CONTINUE
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.CONTINUE);
            }
        }
    }
}
