package tw.org.ctworld.meditation.views;



import com.microsoft.schemas.office.word.STWrapType;
import com.microsoft.schemas.vml.CTGroup;
import com.microsoft.schemas.vml.CTShape;
import com.microsoft.schemas.vml.CTTextbox;
import com.microsoft.schemas.vml.STTrueFalse;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;

import java.math.BigInteger;

@Service
public class DocumentHorizontallyWordView {

    private static final Logger logger = LoggerFactory.getLogger(DocumentHorizontallyWordView.class);

    public XWPFDocument generateDocument(int marginTop, int marginLeft, int type) {

        XWPFDocument document = new XWPFDocument();

        // start setup page
        CTBody body = document.getDocument().getBody();

        if(!body.isSetSectPr()){
            body.addNewSectPr();
        }

        CTSectPr section = body.getSectPr();

        CTPageMar pageMar = section.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(720L));
        pageMar.setTop(BigInteger.valueOf(720L));
        pageMar.setRight(BigInteger.valueOf(720L));
        pageMar.setBottom(BigInteger.valueOf(720L));

        if (!section.isSetPgSz()) {
            section.addNewPgSz();
        }

        // 1cm = 567twip
        CTPageSz pageSize = section.getPgSz();
        pageSize.setOrient(STPageOrientation.LANDSCAPE);
        pageSize.setH(BigInteger.valueOf(11113));
        pageSize.setW(BigInteger.valueOf(15366));
        // end setup page

        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();

        switch (type) {
            case 1: // 結業證書

                addTextBox(document, paragraph, run, 58, 160,"張簡大同", 509 + marginLeft, 180 + marginTop, 22, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);

                addTextBox(document, paragraph, run, 42, 90,"一百零九", 516 + marginLeft, 356 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);

//                addTextBox(document, paragraph, run, 42, 365,"普因精舍第六十六期", 474 + marginLeft, 202 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
//                addTextBox(document, paragraph, run, 42, 365,"夜間研經一禪修班", 454 + marginLeft, 202 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
//                addTextBox(document, paragraph, run, 42, 365,"佛說四十二章經（經序～第十章）".replace("～", "~"), 434 + marginLeft, 202 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
                addTextBox(document, paragraph, run, 44, 365,"普因精舍第六十六期", 472 + marginLeft, 202 + marginTop, 16, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
                addTextBox(document, paragraph, run, 44, 365,"夜間研經一禪修班", 452 + marginLeft, 202 + marginTop, 16, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
                addTextBox(document, paragraph, run, 44, 365,"佛說四十二章經（經序～第十章）".replace("～", "~"), 432 + marginLeft, 202 + marginTop, 16, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);

//                addTextBox(document, paragraph, run, 40, 120,"普因普因精舍", 206 + marginLeft, 201 + marginTop, 12, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE);
//                addTextBox(document, paragraph, run, 40, 120,"住持", 193 + marginLeft, 201 + marginTop, 12, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE);
//                addTextBox(document, paragraph, run, 40, 180,"普因普因精舍住持", 201 + marginLeft, 157 + marginTop, 14, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE);
                addTextBox(document, paragraph, run, 52, 180,"普因精舍住持", 196 + marginLeft, 157 + marginTop, 20, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE);

                addTextBox(document, paragraph, run, 80, 205,"釋見因", 190 + marginLeft, 301 + marginTop, 42, "超世紀細毛楷", 1, false, ParagraphAlignment.DISTRIBUTE);

                addTextBox(document, paragraph, run, 39, 100,"一百九十九", 53 + marginLeft, 196 + marginTop, 12, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);

                addTextBox(document, paragraph, run, 40, 60,"十二", 53 + marginLeft, 291 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);

                addTextBox(document, paragraph, run, 40, 80,"三十一", 53 + marginLeft, 365 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
                break;
            case 2: // 全勤證書

                // 舊版證書
//                addTextBox(document, paragraph, run, 56, 160,"張簡大同", 532 + marginLeft, 215 + marginTop, 22, "標楷體", 1, true, ParagraphAlignment.DISTRIBUTE);
//
//                addTextBox(document, paragraph, run, 42, 365,"普因精舍第六十六期", 501 + marginLeft, 190 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
//                addTextBox(document, paragraph, run, 42, 365,"夜間研經一禪修班", 481 + marginLeft, 190 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
//                addTextBox(document, paragraph, run, 42, 365,"佛說四十二章經（經序～第十章）".replace("～", "~"), 461 + marginLeft, 190 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
//
//                addTextBox(document, paragraph, run, 40, 90,"普因普因精舍", 213 + marginLeft, 206 + marginTop, 10, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE);
//                addTextBox(document, paragraph, run, 40, 90,"住持", 200 + marginLeft, 206 + marginTop, 10, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE);
//
//                addTextBox(document, paragraph, run, 39, 100,"一百九十九", 52 + marginLeft, 192 + marginTop, 12, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
//
//                addTextBox(document, paragraph, run, 40, 60,"十二", 53 + marginLeft, 286 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
//
//                addTextBox(document, paragraph, run, 40, 80,"三十一", 53 + marginLeft, 360 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);


                // 新版證書
                addTextBox(document, paragraph, run, 56, 160,"張簡大同", 536 + marginLeft, 215 + marginTop, 22, "標楷體", 1, true, ParagraphAlignment.DISTRIBUTE);

//                addTextBox(document, paragraph, run, 42, 365,"普因精舍第六十六期", 504 + marginLeft, 190 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
//                addTextBox(document, paragraph, run, 42, 365,"夜間研經一禪修班", 484 + marginLeft, 190 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
//                addTextBox(document, paragraph, run, 42, 365,"佛說四十二章經（經序～第十章）".replace("～", "~"), 464 + marginLeft, 190 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
                addTextBox(document, paragraph, run, 44, 365,"普因精舍第六十六期", 504 + marginLeft, 190 + marginTop, 16, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
                addTextBox(document, paragraph, run, 44, 365,"夜間研經一禪修班", 484 + marginLeft, 190 + marginTop, 16, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
                addTextBox(document, paragraph, run, 44, 365,"佛說四十二章經（經序～第十章）".replace("～", "~"), 464 + marginLeft, 190 + marginTop, 16, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);

//                addTextBox(document, paragraph, run, 40, 180,"普因普因精舍住持", 198 + marginLeft, 143 + marginTop, 14, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE);
                addTextBox(document, paragraph, run, 52, 180,"普因精舍住持", 193 + marginLeft, 143 + marginTop, 20, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE);

                addTextBox(document, paragraph, run, 80, 205,"釋見因", 187 + marginLeft, 286 + marginTop, 42, "超世紀細毛楷", 1, false, ParagraphAlignment.DISTRIBUTE);

                addTextBox(document, paragraph, run, 39, 100,"一百九十九", 56 + marginLeft, 192 + marginTop, 12, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);

                addTextBox(document, paragraph, run, 40, 60,"十二", 57 + marginLeft, 286 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);

                addTextBox(document, paragraph, run, 40, 80,"三十一", 57 + marginLeft, 360 + marginTop, 14, "標楷體", 1, false, ParagraphAlignment.DISTRIBUTE);
                    break;
        }


        return  document;
    }

    private void addTextBox(XWPFDocument document, XWPFParagraph paragraph, XWPFRun run, int width, int height,
                            String text, int marginLeft, int marginTop, int fontSize, String fontFamily, double spacing,
                            boolean isBold, ParagraphAlignment align) {
        CTGroup ctGroup = CTGroup.Factory.newInstance();

        CTShape ctShape = ctGroup.addNewShape();
        ctShape.setStroked(STTrueFalse.F);
        ctShape.addNewWrap().setType(STWrapType.SQUARE);

        ctShape.setStyle("position:absolute;margin-top:" + marginTop + "pt;margin-left:" + marginLeft + "pt;width:" + width + "px;height:" + height + "px;border-top:0px;border-left:0px;border-bottom:0px;border-right:0px;");

        CTTextbox ctTextbox = ctShape.addNewTextbox();
        ctTextbox.setStyle("layout-flow:vertical-ideographic");

        CTTxbxContent ctTxbxContent = ctShape.addNewTextbox().addNewTxbxContent();
        XWPFParagraph textBoxParagraph = new XWPFParagraph(ctTxbxContent.addNewP(), (IBody)document);
        CTParaRPr ctParaRPr = textBoxParagraph.getCTP().addNewPPr().addNewRPr();
        ctParaRPr.addNewEastAsianLayout().setVert(STOnOff.X_1);
        textBoxParagraph.setSpacingBetween(spacing);
        textBoxParagraph.setAlignment(align);

        XWPFRun textBoxRun = textBoxParagraph.createRun();
        textBoxRun.setFontFamily(fontFamily);
        textBoxRun.setText(text);
        textBoxRun.setFontSize(fontSize);
        textBoxRun.setBold(isBold);


        try {
            Node ctGroupNode = ctGroup.getDomNode();
            CTPicture ctPicture = CTPicture.Factory.parse(ctGroupNode);
            run = paragraph.createRun();
            CTR cTR = run.getCTR();
            cTR.addNewPict();
            cTR.setPictArray(0, ctPicture);
        } catch (XmlException e) {
            e.printStackTrace();
        }
    }

}
