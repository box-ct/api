package tw.org.ctworld.meditation.views;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import tw.org.ctworld.meditation.beans.ClassReport.ClassAttendance;
import tw.org.ctworld.meditation.beans.ClassReport.ClassMemberRecord;
import tw.org.ctworld.meditation.beans.ClassStats.ClassStats005Object;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class CertExcelView extends AbstractXlsxView {

    private HttpSession session;

    public CertExcelView(HttpSession session) {
        super();
        this.session = session;
    }

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<ClassStats005Object> results = (List<ClassStats005Object>) model.get("results");
//        ClassStats005Object sample = results.get(0);

        String titleStr = "證書套印資料";
        String[] headStrings = {"No.", "精舍代碼", "精舍簡稱", "性別", "姓名", "法名", "年齡", "時段", "課程類別", "期別",
                "班別", "組別", "組號", "星期", "課程說明", "課程全名", "證書印名", "全勤", "結業"};

        // change the file name
        String filename = "";

        if (results.size() == 0) {

            filename = titleStr + "_無資料匯出";
        }else {

            ClassStats005Object sample = results.get(0);
            String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());
            String userName = (String) session.getAttribute(UserSession.Keys.UserName.getValue());

            filename = unitName + "_" + sample.getClassPeriodNum() + "_" + sample.getClassName() + "_" + titleStr +
                    "_" + userName + "_" + CommonUtils.GetCurrentTimeString() + ".xls";
        }

        filename = URLEncoder.encode(filename, StandardCharsets.UTF_8.name());

        response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + filename);

        // create excel xls sheet
        Sheet sheet = workbook.createSheet(titleStr);

        // create style for title cells
        CellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HorizontalAlignment.CENTER);

        Font font = workbook.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeight((short) (18*20));
        titleStyle.setFont(font);

        // create style for header cells
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.CENTER);

        Font headFont = workbook.createFont();
        headFont.setFontName("Arial");
        headFont.setBold(true);
        headStyle.setFont(headFont);

        // create style for other cells
        CellStyle otherStyle = workbook.createCellStyle();
        otherStyle.setAlignment(HorizontalAlignment.CENTER);

        int rowCount = 0;

        Row title = sheet.createRow(rowCount);
        title.setHeight((short) (25*20));
        Cell titleCell = title.createCell(0);
        titleCell.setCellValue(titleStr);
        titleCell.setCellStyle(titleStyle);

        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, headStrings.length - 1));
        rowCount++;

        // create header row
        Row header = sheet.createRow(rowCount);
        header.setHeight((short) (25*20));

        int cellIndex = 0;
        for (int i = 0; i < headStrings.length; i++) {
            Cell cell = header.createCell(cellIndex++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headStrings[i]);
        }

        rowCount++;

        int dataIndex = 1;

        for (ClassStats005Object record: results) {
            int columnIndex = 0;
            Row dataRow = sheet.createRow(rowCount++);
            dataRow.createCell(columnIndex++).setCellValue(dataIndex++);
            dataRow.createCell(columnIndex++).setCellValue(record.getUnitId());
            dataRow.createCell(columnIndex++).setCellValue(record.getUnitName());
            dataRow.createCell(columnIndex++).setCellValue(record.getGender());
            dataRow.createCell(columnIndex++).setCellValue(record.getAliasName());
            dataRow.createCell(columnIndex++).setCellValue(record.getCtDharmaName());
            dataRow.createCell(columnIndex++).setCellValue(record.getAge());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassDayOrNight());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassTypeName());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassPeriodNum());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassName());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassGroupId());
            dataRow.createCell(columnIndex++).setCellValue(record.getMemberGroupNum());
            dataRow.createCell(columnIndex++).setCellValue(record.getDayOfWeek());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassDesc());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassFullName());
            dataRow.createCell(columnIndex++).setCellValue(record.getCertificateName());
            dataRow.createCell(columnIndex++).setCellValue(record.getIsFullAttended() ? "V" : "");
            dataRow.createCell(columnIndex++).setCellValue(record.getIsGraduated() ? "V" : "");

            dataRow.setRowStyle(otherStyle);
        }

        for (int i = 0 ; i < headStrings.length ; i++) {
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, (int)(sheet.getColumnWidth(i) * 2));
        }
    }
}
