package tw.org.ctworld.meditation.views;

import com.microsoft.schemas.office.word.STWrapType;
import com.microsoft.schemas.vml.CTGroup;
import com.microsoft.schemas.vml.CTShape;
import com.microsoft.schemas.vml.STTrueFalse;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;

import java.math.BigInteger;

@Service
public class DocumentVerticallyWordView {

    private static final Logger logger = LoggerFactory.getLogger(DocumentVerticallyWordView.class);

    public XWPFDocument generateDocument(int marginTop, int marginLeft, int type) {

        XWPFDocument document = new XWPFDocument();

        // start setup page
        CTBody body = document.getDocument().getBody();

        if(!body.isSetSectPr()){
            body.addNewSectPr();
        }

        CTSectPr section = body.getSectPr();

        CTPageMar pageMar = section.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(720L));
        pageMar.setTop(BigInteger.valueOf(720L));
        pageMar.setRight(BigInteger.valueOf(720L));
        pageMar.setBottom(BigInteger.valueOf(720L));

        if (!section.isSetPgSz()) {
            section.addNewPgSz();
        }

        // 1cm = 567twip
        CTPageSz pageSize = section.getPgSz();
        pageSize.setOrient(STPageOrientation.PORTRAIT);

        switch (type) {
            case 3: // 英文-結業證書
                pageSize.setH(BigInteger.valueOf(15082));
                pageSize.setW(BigInteger.valueOf(10773));
                break;
            case 4: // 英文-全勤證書
                pageSize.setH(BigInteger.valueOf(15082));
                pageSize.setW(BigInteger.valueOf(10773));
                break;
            case 5: // 中英文-結業證書
                pageSize.setH(BigInteger.valueOf(16840));
                pageSize.setW(BigInteger.valueOf(11964));
                break;
            case 6: // 中英文-全勤證書
                pageSize.setH(BigInteger.valueOf(16840));
                pageSize.setW(BigInteger.valueOf(11964));
                break;
            case 7: // 日文-結業證書
                pageSize.setH(BigInteger.valueOf(15082));
                pageSize.setW(BigInteger.valueOf(10773));
                break;
            case 8: // 日文-全勤證書
                pageSize.setH(BigInteger.valueOf(15082));
                pageSize.setW(BigInteger.valueOf(10773));
                break;
        }
        // end setup page

        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();

        switch (type) {
            case 3: // 英文-結業證書

                addTextBox(document, paragraph, run, 320, 43,"Name or Recipient", 116 + marginLeft, 216 + marginTop, 18, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 400, 38,"Zen Buddhism Class-Level 1", 85 + marginLeft, 302 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 320, 38,"Name of Zen Center or Monastery", 25 + marginLeft, 330 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 250, 38,"City, State, Country", 275 + marginLeft, 330 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

//                addTextBox(document, paragraph, run, 220, 46,"Shi Jian Deng", 12 + marginLeft, 400 + marginTop, 20, "Times New Roman", 1, false, ParagraphAlignment.LEFT, true);

//                addTextBox(document, paragraph, run, 350, 35,"Abbot of Chung Tai Chan Monastery", 176 + marginLeft, 405 + marginTop, 12, "Times New Roman", 1, false, ParagraphAlignment.LEFT, false);

                addTextBox(document, paragraph, run, 200, 46,"Shi Jian Deng", 0 + marginLeft, 400 + marginTop, 22, "Times New Roman", 1, false, ParagraphAlignment.LEFT, true);

                addTextBox(document, paragraph, run, 350, 35,"Abbot of Chung Tai Chan Monastery", 168 + marginLeft, 408 + marginTop, 12, "Times New Roman", 1, false, ParagraphAlignment.LEFT, false);
                break;
            case 4: // 英文-全勤證書

                addTextBox(document, paragraph, run, 320, 43,"Name or Recipient", 116 + marginLeft, 216 + marginTop, 18, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 400, 38,"Zen Buddhism Class-Level 1", 85 + marginLeft, 302 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 320, 38,"Name of Zen Center or Monastery", 25 + marginLeft, 330 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 250, 38,"City, State, Country", 275 + marginLeft, 330 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

//                addTextBox(document, paragraph, run, 220, 46,"Shi Jian Deng", 12 + marginLeft, 400 + marginTop, 20, "Times New Roman", 1, false, ParagraphAlignment.LEFT, true);

//                addTextBox(document, paragraph, run, 350, 35,"Abbot of Chung Tai Chan Monastery", 176 + marginLeft, 405 + marginTop, 12, "Times New Roman", 1, false, ParagraphAlignment.LEFT, false);

                addTextBox(document, paragraph, run, 200, 46,"Shi Jian Deng", 0 + marginLeft, 400 + marginTop, 22, "Times New Roman", 1, false, ParagraphAlignment.LEFT, true);

                addTextBox(document, paragraph, run, 350, 35,"Abbot of Chung Tai Chan Monastery", 168 + marginLeft, 408 + marginTop, 12, "Times New Roman", 1, false, ParagraphAlignment.LEFT, false);
                break;
            case 5: // 中英文-結業證書

                addTextBox(document, paragraph, run, 320, 43,"Name or Recipient", 147 + marginLeft, 235 + marginTop, 18, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 400, 38,"Zen Buddhism Class-Level 1", 115 + marginLeft, 293 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 337, 38,"Name of Zen Center or Monastery", 39 + marginLeft, 321 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 267, 38,"City, State, Country", 304 + marginLeft, 321 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 220, 46,"Shi Jian Deng", 13 + marginLeft, 378 + marginTop, 22, "Times New Roman", 1, false, ParagraphAlignment.LEFT, true);

                addTextBox(document, paragraph, run, 350, 35,"Abbot of Chung Tai Chan Monastery", 182 + marginLeft, 387 + marginTop, 12, "Times New Roman", 1, false, ParagraphAlignment.LEFT, false);

                addTextBox(document, paragraph, run, 210, 45,"學員姓名", 185 + marginLeft, 486 + marginTop, 20, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 210, 45,"結業禪修班別", 183 + marginLeft, 517 + marginTop, 20, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

//                addTextBox(document, paragraph, run, 250, 40,"某某某某精舍住持", 355 + marginLeft, 645 + marginTop, 18, "華康魏碑體", 1, false, ParagraphAlignment.LEFT, false);

//                addTextBox(document, paragraph, run, 180, 80,"釋見因", 410 + marginLeft, 665 + marginTop, 38, "超世紀細毛楷", 1, false, ParagraphAlignment.LEFT, false);

//                addTextBox(document, paragraph, run, 110, 40,"某某某某精舍", 320 + marginLeft, 645 + marginTop, 10, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE, false);
//                addTextBox(document, paragraph, run, 110, 40,"住持", 320 + marginLeft, 665 + marginTop, 18, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE, false);
                addTextBox(document, paragraph, run, 116, 40,"某某精舍", 317 + marginLeft, 645 + marginTop, 18, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE, false);
                addTextBox(document, paragraph, run, 116, 40,"住持", 317 + marginLeft, 665 + marginTop, 18, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE, false);

                addTextBox(document, paragraph, run, 200, 80,"釋見因", 392 + marginLeft, 640 + marginTop, 44, "超世紀細毛楷", 1, false, ParagraphAlignment.LEFT, false);

                addTextBox(document, paragraph, run, 68, 40,"2016", 160 + marginLeft, 709 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 60, 40,"12", 320 + marginLeft, 709 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 60, 40,"31", 430 + marginLeft, 709 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);
                break;
            case 6: // 中英文-全勤證書

                addTextBox(document, paragraph, run, 320, 43,"Name or Recipient", 147 + marginLeft, 235 + marginTop, 18, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 400, 38,"Zen Buddhism Class-Level 1", 115 + marginLeft, 293 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 363, 38,"Name of Zen Center or Monastery", 11 + marginLeft, 321 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 295, 38,"City, State, Country", 298 + marginLeft, 321 + marginTop, 16, "Times New Roman", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 220, 46,"Shi Jian Deng", -12 + marginLeft, 376 + marginTop, 22, "Times New Roman", 1, false, ParagraphAlignment.LEFT, true);

                addTextBox(document, paragraph, run, 350, 35,"Abbot of Chung Tai Chan Monastery", 156 + marginLeft, 385 + marginTop, 12, "Times New Roman", 1, false, ParagraphAlignment.LEFT, false);

                addTextBox(document, paragraph, run, 210, 45,"學員姓名", 180 + marginLeft, 481 + marginTop, 20, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 180, 45,"結業禪修班別", 123 + marginLeft, 518 + marginTop, 20, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

//                addTextBox(document, paragraph, run, 250, 40,"某某某某精舍住持", 350 + marginLeft, 645 + marginTop, 18, "華康魏碑體", 1, false, ParagraphAlignment.LEFT, false);

//                addTextBox(document, paragraph, run, 180, 80,"釋見因", 410 + marginLeft, 665 + marginTop, 38, "超世紀細毛楷", 1, false, ParagraphAlignment.LEFT, false);

//                addTextBox(document, paragraph, run, 110, 40,"某某某某精舍", 320 + marginLeft, 645 + marginTop, 10, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE, false);
//                addTextBox(document, paragraph, run, 110, 40,"住持", 320 + marginLeft, 665 + marginTop, 18, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE, false);
                addTextBox(document, paragraph, run, 116, 40,"某某某某精舍", 317 + marginLeft, 645 + marginTop, 18, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE, false);
                addTextBox(document, paragraph, run, 116, 40,"住持", 317 + marginLeft, 665 + marginTop, 18, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE, false);

                addTextBox(document, paragraph, run, 200, 80,"釋見因", 392 + marginLeft, 640 + marginTop, 44, "超世紀細毛楷", 1, false, ParagraphAlignment.LEFT, false);

                addTextBox(document, paragraph, run, 68, 40,"2016", 160 + marginLeft, 709 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 60, 40,"12", 320 + marginLeft, 709 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 60, 40,"31", 430 + marginLeft, 709 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);
                break;
            case 7: // 日文-結業證書

                addTextBox(document, paragraph, run, 320, 43,"Name or Recipient", 115 + marginLeft, 154 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 250, 38,"Zen Buddhism Class-Level 1", 255 + marginLeft, 222 + marginTop, 16, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 175, 40,"某某某某精舍住持", 196 + marginLeft, 426 + marginTop, 14, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE, false);

                addTextBox(document, paragraph, run, 180, 80,"釋見因", 326 + marginLeft, 410 + marginTop, 40, "超世紀細毛楷", 1, false, ParagraphAlignment.LEFT, false);

                addTextBox(document, paragraph, run, 68, 40,"2016", 145 + marginLeft, 625 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 60, 40,"12", 285 + marginLeft, 625 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 60, 40,"31", 395 + marginLeft, 625 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);
                break;
            case 8: // 日文-全勤證書

                addTextBox(document, paragraph, run, 320, 43,"Name or Recipient", 115 + marginLeft, 154 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 210, 38,"Zen Buddhism Class-Level 1", 275 + marginLeft, 201 + marginTop, 16, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 175, 40,"某某某某精舍住持", 196 + marginLeft, 426 + marginTop, 14, "華康魏碑體", 1, false, ParagraphAlignment.DISTRIBUTE, false);

                addTextBox(document, paragraph, run, 180, 80,"釋見因", 326 + marginLeft, 410 + marginTop, 40, "超世紀細毛楷", 1, false, ParagraphAlignment.LEFT, false);

                addTextBox(document, paragraph, run, 68, 40,"2016", 145 + marginLeft, 625 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 60, 40,"12", 285 + marginLeft, 625 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);

                addTextBox(document, paragraph, run, 60, 40,"31", 395 + marginLeft, 625 + marginTop, 18, "標楷體", 1, false, ParagraphAlignment.CENTER, false);
                break;
        }

        return  document;
    }

    private void addTextBox(XWPFDocument document, XWPFParagraph paragraph, XWPFRun run, int width, int height,
                            String text, int marginLeft, int marginTop, int fontSize, String fontFamily, double spacing,
                            boolean isBold, ParagraphAlignment align, boolean isItalic) {
        CTGroup ctGroup = CTGroup.Factory.newInstance();

        CTShape ctShape = ctGroup.addNewShape();
        ctShape.setStroked(STTrueFalse.F);
        ctShape.addNewWrap().setType(STWrapType.SQUARE);

        ctShape.setStyle("position:absolute;margin-top:" + marginTop + "pt;margin-left:" + marginLeft + "pt;width:" + width + "px;height:" + height + "px;border-top:0px;border-left:0px;border-bottom:0px;border-right:0px;");

        CTTxbxContent ctTxbxContent = ctShape.addNewTextbox().addNewTxbxContent();
        XWPFParagraph textBoxParagraph = new XWPFParagraph(ctTxbxContent.addNewP(), (IBody)document);
        textBoxParagraph.setSpacingBetween(spacing);
        textBoxParagraph.setAlignment(align); // LEFT, CENTER, RIGHT, BOTH, DISTRIBUTE
        XWPFRun textBoxrun = textBoxParagraph.createRun();
        textBoxrun.setFontFamily(fontFamily);
        textBoxrun.setText(text);
        textBoxrun.setFontSize(fontSize);
        textBoxrun.setBold(isBold);
        textBoxrun.setItalic(isItalic);

        try {
            Node ctGroupNode = ctGroup.getDomNode();
            CTPicture ctPicture = CTPicture.Factory.parse(ctGroupNode);
            run = paragraph.createRun();
            CTR cTR = run.getCTR();
            cTR.addNewPict();
            cTR.setPictArray(0, ctPicture);
        } catch (XmlException e) {
            e.printStackTrace();
        }
    }

}
