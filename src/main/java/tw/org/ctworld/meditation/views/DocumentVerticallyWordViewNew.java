package tw.org.ctworld.meditation.views;

import com.microsoft.schemas.office.word.STWrapType;
import com.microsoft.schemas.vml.CTGroup;
import com.microsoft.schemas.vml.CTShape;
import com.microsoft.schemas.vml.STTrueFalse;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;
import tw.org.ctworld.meditation.beans.Printer.Printer007Object;
import tw.org.ctworld.meditation.beans.Printer.Printer011Object;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.libs.CommonUtils.print2JSON;

@Service
public class DocumentVerticallyWordViewNew {

    private static final Logger logger = LoggerFactory.getLogger(DocumentVerticallyWordViewNew.class);

    public XWPFDocument generateDocument(int type, List<Printer007Object> classCertFieldSettingList,
                                         List<Printer011Object> memberNameAndAgeList, String certContent,
                                         String certMaster, String certDate, String printerType) {

        XWPFDocument document = new XWPFDocument();

        // start setup page
        CTBody body = document.getDocument().getBody();

        if(!body.isSetSectPr()){
            body.addNewSectPr();
        }

        CTSectPr section = body.getSectPr();

        CTPageMar pageMar = section.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(720L));
        pageMar.setTop(BigInteger.valueOf(720L));
        pageMar.setRight(BigInteger.valueOf(720L));
        pageMar.setBottom(BigInteger.valueOf(720L));

        if (!section.isSetPgSz()) {
            section.addNewPgSz();
        }

        // 1cm = 567twip
        CTPageSz pageSize = section.getPgSz();
        pageSize.setOrient(STPageOrientation.PORTRAIT);

        switch (type) {
            case 3: // 英文-結業證書
                pageSize.setH(BigInteger.valueOf(15082));
                pageSize.setW(BigInteger.valueOf(10773));
                break;
            case 4: // 英文-全勤證書
                pageSize.setH(BigInteger.valueOf(15082));
                pageSize.setW(BigInteger.valueOf(10773));
                break;
            case 5: // 中英文-結業證書
                pageSize.setH(BigInteger.valueOf(16840));
                pageSize.setW(BigInteger.valueOf(11964));
                break;
            case 6: // 中英文-全勤證書
                pageSize.setH(BigInteger.valueOf(16840));
                pageSize.setW(BigInteger.valueOf(11964));
                break;
            case 7: // 日文-結業證書
                pageSize.setH(BigInteger.valueOf(15082));
                pageSize.setW(BigInteger.valueOf(10773));
                break;
            case 8: // 日文-全勤證書
                pageSize.setH(BigInteger.valueOf(15082));
                pageSize.setW(BigInteger.valueOf(10773));
                break;
        }
        // end setup page

        for (int x = 0 ; x < memberNameAndAgeList.size() ; x++) {

            XWPFParagraph paragraph = document.createParagraph();
            XWPFRun run = paragraph.createRun();

            String name = memberNameAndAgeList.get(x).getName();
            String age = memberNameAndAgeList.get(x).getAge();
            String[] certContentList = certContent.split(";");
            String[] certMasterList = certMaster.split(";");
            String[] certDateList = certDate.split(";");
            Printer007Object classCertFieldSetting = null;

            switch (type) {
                case 3: // 英文-結業證書

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("1")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),name, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("4")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("5")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("6")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());
                    break;
                case 4: // 英文-全勤證書

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("1")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),name, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("4")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("5")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("6")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());
                    break;
                case 5: // 中英文-結業證書

                    if (printerType.equals("英文")) {

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("1")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),name, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("4")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("5")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("6")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());
                    }else {

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("7")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),name, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("8")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("9-1")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("9-2")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("10")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());
                    }

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("11")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("12")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("13")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());
                    break;
                case 6: // 中英文-全勤證書

                    if (printerType.equals("英文")) {

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("1")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),name, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("4")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("5")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("6")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());
                    }else {

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("7")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),name, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("8")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("9-1")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("9-2")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                        classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("10")).collect(Collectors.toList()).get(0);
                        addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());
                    }

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("11")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("12")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("13")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());
                    break;
                case 7: // 日文-結業證書

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("1")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),name, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("4")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("5")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("6")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("7")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());
                    break;
                case 8: // 日文-全勤證書

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("1")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),name, classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("2")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certContentList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("3")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("4")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certMasterList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("5")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[0], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("6")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[1], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());

                    classCertFieldSetting = classCertFieldSettingList.stream().filter(s -> s.getFieldPositionNum().equals("7")).collect(Collectors.toList()).get(0);
                    addTextBox(document, paragraph, run, classCertFieldSetting.getWidth(), classCertFieldSetting.getHeight(),certDateList[2], classCertFieldSetting.getxDistance(), classCertFieldSetting.getyDistance(), classCertFieldSetting.getFontSize(), classCertFieldSetting.getFontName(), 1, classCertFieldSetting.getIsBold(), classCertFieldSetting.getHorizontalAlignType(), classCertFieldSetting.getIsItalic());
                    break;
            }

            if (x != memberNameAndAgeList.size() - 1) {
                run.addBreak(BreakType.PAGE);
            }

        }

        return  document;
    }

    private void addTextBox(XWPFDocument document, XWPFParagraph paragraph, XWPFRun run, int width, int height,
                            String text, int marginLeft, int marginTop, int fontSize, String fontFamily, double spacing,
                            boolean isBold, String align, boolean isItalic) {
        CTGroup ctGroup = CTGroup.Factory.newInstance();

        CTShape ctShape = ctGroup.addNewShape();
        ctShape.setStroked(STTrueFalse.F);
        ctShape.addNewWrap().setType(STWrapType.SQUARE);

        ctShape.setStyle("position:absolute;margin-top:" + marginTop + "pt;margin-left:" + marginLeft + "pt;width:" + width + "px;height:" + height + "px;border-top:0px;border-left:0px;border-bottom:0px;border-right:0px;");

        CTTxbxContent ctTxbxContent = ctShape.addNewTextbox().addNewTxbxContent();
        XWPFParagraph textBoxParagraph = new XWPFParagraph(ctTxbxContent.addNewP(), (IBody)document);
        textBoxParagraph.setSpacingBetween(spacing);
        switch (align) {
            case "靠左":
                textBoxParagraph.setAlignment(ParagraphAlignment.LEFT);
                break;
            case "置中":
                textBoxParagraph.setAlignment(ParagraphAlignment.CENTER);
                break;
            case "靠右":
                textBoxParagraph.setAlignment(ParagraphAlignment.RIGHT);
                break;
            case "文字等分":
                textBoxParagraph.setAlignment(ParagraphAlignment.DISTRIBUTE);
                break;
            case "左右對齊":
                textBoxParagraph.setAlignment(ParagraphAlignment.BOTH);
                break;
        }
        XWPFRun textBoxrun = textBoxParagraph.createRun();
        textBoxrun.setFontFamily(fontFamily);
        textBoxrun.setText(text);
        textBoxrun.setFontSize(fontSize);
        textBoxrun.setBold(isBold);
        textBoxrun.setItalic(isItalic);

        try {
            Node ctGroupNode = ctGroup.getDomNode();
            CTPicture ctPicture = CTPicture.Factory.parse(ctGroupNode);
            run = paragraph.createRun();
            CTR cTR = run.getCTR();
            cTR.addNewPict();
            cTR.setPictArray(0, ctPicture);
        } catch (XmlException e) {
            e.printStackTrace();
        }
    }

}
