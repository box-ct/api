package tw.org.ctworld.meditation.views;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.ClassReport.ClassMemberRecord;
import tw.org.ctworld.meditation.services.FileStorageService;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Service
public class StudentNameCardWordView1 {
    @Autowired
    private FileStorageService fileStorageService;

    private static final Logger logger = LoggerFactory.getLogger(StudentNameCardWordView1.class);


    public XWPFDocument generateDocument(List<ClassMemberRecord> results, int type) {

        int size = results.size();

        XWPFDocument document = new XWPFDocument();

        // setup page
        CTBody body = document.getDocument().getBody();

        if(!body.isSetSectPr()){
            body.addNewSectPr();
        }

        CTSectPr section = body.getSectPr();

        CTPageMar pageMar = section.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(720L));
        pageMar.setTop(BigInteger.valueOf(720L));
        pageMar.setRight(BigInteger.valueOf(720L));
        pageMar.setBottom(BigInteger.valueOf(720L));

        if (!section.isSetPgSz()) {
            section.addNewPgSz();
        }

        CTPageSz pageSize = section.getPgSz();
        pageSize.setOrient(STPageOrientation.PORTRAIT);
        pageSize.setW(BigInteger.valueOf(11907));
        pageSize.setH(BigInteger.valueOf(16840));

        // create table
//        XWPFTable table;
//        XWPFParagraph paragraph;
//        XWPFRun run;

        // cal number of rows
        int numRows = size / 2 + 1;
        int numCols = 2;

//        document.createParagraph();

        XWPFTable table = document.createTable(numRows, numCols);
        table.getCTTbl().getTblPr().unsetTblBorders();

        int tableWidth = 10467;
        table.setWidth(tableWidth);

        table.getCTTbl().addNewTblGrid().addNewGridCol().setW(BigInteger.valueOf(tableWidth/numCols));
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(tableWidth/numCols));

        CTTblWidth tblWidth = table.getRow(0).getCell(0).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(tableWidth/numCols));
        tblWidth.setType(STTblWidth.DXA);

        tblWidth = table.getRow(0).getCell(1).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(tableWidth/numCols));
        tblWidth.setType(STTblWidth.DXA);

        for (int i = 0; i < size; i++) {
            int rowIndex = i / 2;
            int colIndex = i % 2;

            XWPFTableRow row = table.getRow(rowIndex);
            XWPFTableCell cell = row.getCell(colIndex);

            row.setCantSplitRow(true);

            XmlCursor cursor = cell.getParagraphs().get(0).getCTP().newCursor();

            // create table
            XWPFTable innerTable = cell.insertNewTbl(cursor);

            innerTable.removeRow(0);

            XWPFTableRow row1 = innerTable.createRow();
            row1.createCell();
//
            row1.createCell();
            innerTable.createRow();
            innerTable.createRow();
            innerTable.createRow();

//            logger.debug(innerTable.getRows().size() + "/" + innerTable.getRow(0).getTableCells().size());

            mergeCellsVertically(innerTable, 0, 0, 2);

            ClassMemberRecord record = results.get(i);

            // 0: cht, 1: eng
            String classStr = record.getClassName();
            String nameStr;
            String titleStr;

            if (type == 0) {
                classStr += "(週" + record.getDayOfWeek() + " " + record.getDayOrNight() + ")";
                nameStr = "姓名: ";
                titleStr = "職事: ";
            } else {
                classStr += " (";
                switch (record.getDayOfWeek()) {
                    case "一":
                        classStr += "Mon.";
                        break;
                    case "二":
                        classStr += "Tue.";
                        break;
                    case "三":
                        classStr += "Wen.";
                        break;
                    case "四":
                        classStr += "Thu.";
                        break;
                    case "五":
                        classStr += "Fri.";
                        break;
                    case "六":
                        classStr += "Sat.";
                        break;
                    case "日":
                        classStr += "Sun.";
                        break;
                }

                classStr += " ";

                switch (record.getDayOrNight()) {
                    case "日間":
                        classStr += "Day";
                        break;
                    case "夜間":
                        classStr += "Night";
                        break;
                }

                classStr += ")";

                nameStr = "Name: ";
                titleStr = "Title: ";
            }

            if (record.getAliasName() != null) {
                nameStr += record.getAliasName();
            }

            if (record.getClassJobTitleList() != null) {
                String jobStr[] = record.getClassJobTitleList().split("/");
                titleStr += jobStr[0];
            }

            formatCell(innerTable.getRow(0).getCell(1), classStr, true, type);
            formatCell(innerTable.getRow(1).getCell(1), nameStr, false, type);
            formatCell(innerTable.getRow(2).getCell(1), titleStr, false, type);

            String genderStr;
            if (type == 0) {
                genderStr = record.getGender();
            } else {
                if (record.getGender().equals("男")){
                    genderStr = "Male";
                } else {
                    genderStr = "Female";
                }
            }

            String dNameStr;
            if (record.getCtDharmaName() == null) {
                dNameStr = "";
            } else {
                dNameStr = record.getCtDharmaName();
            }
            formatCell(innerTable.getRow(3).getCell(1), dNameStr + "#" + genderStr + " " + record.getClassGroupId() + "#" + record.getMemberId(), false, type);

            setRowHeight(innerTable);

            setPhoto(innerTable.getRow(0).getCell(0), record);
            setQRCode(innerTable.getRow(3).getCell(0), record);

            XWPFRun run = cell.getParagraphs().get(0).createRun();

            run.setFontSize(7);
            run.setText(" ");

//            cell.addParagraph();
        }

        return document;
    }


    // set height and borders
    private void setRowHeight(XWPFTable table) {
        //1 in = 1440
        for(int i = 0; i < table.getRows().size(); i++) {
            if (i < 3) {
                table.getRow(i).setHeight(576);
            } else {
                table.getRow(i).setHeight(1440);
            }
        }

        int cw1 = 1440;
        int cw2 = 3384;

        int tableWidth = cw1 + cw2;
        table.setWidth(tableWidth);

        table.getCTTbl().addNewTblGrid().addNewGridCol().setW(BigInteger.valueOf(cw1));
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(cw2));

        CTTblWidth tblWidth = table.getRow(0).getCell(0).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(cw1));
        tblWidth.setType(STTblWidth.DXA);

        tblWidth = table.getRow(0).getCell(1).getCTTc().addNewTcPr().addNewTcW();
        tblWidth.setW(BigInteger.valueOf(cw2));
        tblWidth.setType(STTblWidth.DXA);

        List<CTBorder> borders = new ArrayList<>();

        for(XWPFTableRow row: table.getRows()) {
            for (XWPFTableCell cell: row.getTableCells()) {
                CTTcBorders borderC = cell.getCTTc().addNewTcPr().addNewTcBorders();
                CTBorder borderT = borderC.addNewTop();
                borderT.setVal(STBorder.SINGLE);
                borderT.setSz(BigInteger.valueOf(20));
                borderT.setColor("0000FF");
                CTBorder borderR = borderC.addNewRight();
                borderR.setVal(STBorder.SINGLE);
                borderR.setSz(BigInteger.valueOf(20));
                borderR.setColor("0000FF");
                CTBorder borderB = borderC.addNewBottom();
                borderB.setVal(STBorder.SINGLE);
                borderB.setSz(BigInteger.valueOf(20));
                borderB.setColor("0000FF");
                CTBorder borderL = borderC.addNewLeft();
                borderL.setVal(STBorder.SINGLE);
                borderL.setSz(BigInteger.valueOf(20));
                borderL.setColor("0000FF");
            }
        }

        for(CTBorder border: borders) {
            border.setColor("0000FF");
            border.setVal(STBorder.BASIC_WIDE_MIDLINE);
        }
    }

    private void setPhoto(XWPFTableCell cell, ClassMemberRecord record) {
        cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
        XWPFParagraph paragraph = cell.addParagraph();

        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();

        String imgFile;

        try {
            if (record.getPhoto() == null || record.getPhoto().isEmpty()) {
                imgFile = "no_avatar.jpg";
                run.addPicture(new ClassPathResource(imgFile).getInputStream(), XWPFDocument.PICTURE_TYPE_JPEG, imgFile, Units.toEMU(60), Units.toEMU(72));
            } else {
                imgFile = record.getPhoto();
                Resource resource = fileStorageService.loadFile(0, imgFile);
                run.addPicture(resource.getInputStream(), XWPFDocument.PICTURE_TYPE_JPEG, imgFile, Units.toEMU(60), Units.toEMU(72));
            }

            cell.removeParagraph(0);
        } catch (Exception e) {
            logger.error("image not found");
        }
    }

    private void setQRCode(XWPFTableCell cell, ClassMemberRecord record) {
        cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

        try {
            XWPFParagraph paragraph = cell.addParagraph();

            paragraph.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun run = paragraph.createRun();

            QRCodeWriter qrCodeWriter = new QRCodeWriter();

            Hashtable hints = new Hashtable();
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hints.put(EncodeHintType.MARGIN, 0);

            BitMatrix bitMatrix = qrCodeWriter.encode(String.valueOf(record.getMemberId()), BarcodeFormat.QR_CODE, 150, 150, hints);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "PNG", out);

            run.addPicture(new ByteArrayInputStream(out.toByteArray()), XWPFDocument.PICTURE_TYPE_PNG, "", Units.toEMU(60), Units.toEMU(60));

            cell.removeParagraph(0);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    private void formatCell(XWPFTableCell cell, String text, boolean center, int type) {
        if (text != null && !text.isEmpty()) {

            cell.removeParagraph(0);

            XWPFParagraph paragraph = cell.addParagraph();

            XWPFRun run = paragraph.createRun();

            run.setFontFamily("標楷體");
            run.setFontSize(12);

            String str[] = text.split("#");

            if (str.length > 1) {
                paragraph.setSpacingBetween(1.35);

                if (type == 1) {
                    run.setFontSize(10);
                }

                for (int i = 0; i < str.length; i++) {
                    switch (i) {
                        case 0:
                            if (type == 0) {
                                run.setText("法名: ");
                            } else {
                                run.setText("Dharma Name: ");
                            }
                            break;
                        case 1:
                            if (type == 0) {
                                run.setText("組別: ");
                            } else {
                                run.setText("Group: ");
                            }
                            break;
                        case 2:
                            if (type == 0) {
                                run.setText("學員編號: ");
                            } else {
                                run.setText("Member ID: ");
                            }
                            break;
                    }

                    run.setText(str[i]);

                    if (i < str.length - 1) {
                        run.addBreak();
                    }
                }
            } else {
                run.setText(text);
            }

            if (center) paragraph.setAlignment(ParagraphAlignment.CENTER);

            cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
        }
    }


    private  void mergeCellsVertically(XWPFTable table, int col, int fromRow, int toRow) {
        for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
            XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
            if ( rowIndex == fromRow ) {
                // The first merged cell is set with RESTART merge value
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.RESTART);
            } else {
                // Cells which join (merge) the first one, are set with CONTINUE
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.CONTINUE);
            }
        }
    }
}
