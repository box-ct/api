package tw.org.ctworld.meditation.views;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import tw.org.ctworld.meditation.beans.MemberImport.MemberExportInfo;
import tw.org.ctworld.meditation.beans.MemberImport.MemberTemplateItem;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.services.MemberImportService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static tw.org.ctworld.meditation.libs.CommonUtils.GetCurrentTimeString;

public class MemberTemplateExcelView extends AbstractXlsxView {
    private HttpSession session;

    public MemberTemplateExcelView(HttpSession session){
        super();
        this.session = session;
    }

    @Autowired
    private MemberImportService memberImportService;

    private List<String> fieldNames= memberImportService.fieldNames;

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        MemberExportInfo results=(MemberExportInfo)model.get("results");
        // “精舍代碼(後三碼)_精舍名稱_信眾基本資料_西元月年日_時分秒.xls”
        String titleStr ="信眾基本資料";
        String TimeZone= (String) request.getSession().getAttribute(UserSession.Keys.TimeZone.getValue());
        int hh=0;
        try{
            hh=Integer.parseInt(TimeZone);
            logger.debug("timezone:"+hh);
        }
        catch (Exception ex){

        }
        Date now = Calendar.getInstance().getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
        Date newDate =java.util.Date.from( now.toInstant().plus(Duration.ofHours(hh)));
        String datestr=format.format(newDate);
        //
        String[] headStrings =results.getSelectedField().stream().toArray(String[]::new);
        String filename = results.getUnitId().substring(results.getUnitId().length()-3)+"_"+ results.getUnitName()+"_信眾基本資料_"+datestr+".xls";
        filename=java.net.URLEncoder.encode(filename, StandardCharsets.UTF_8.name());
        response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + filename);
        // create excel xls sheet
        Sheet sheet = workbook.createSheet(titleStr);

        // create style for title cells
        CellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HorizontalAlignment.CENTER);

        Font font = workbook.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeight((short) (18*20));
        titleStyle.setFont(font);

        // create style for header cells
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.CENTER);
        headStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());

        Font headFont = workbook.createFont();
        headFont.setFontName("Arial");
        headFont.setBold(true);
        headStyle.setFont(headFont);

        // create style for other cells
        CellStyle otherStyle = workbook.createCellStyle();
        otherStyle.setAlignment(HorizontalAlignment.CENTER);

        Font otherFont = workbook.createFont();
        font.setBold(true);
        otherFont.setColor(IndexedColors.BLACK.getIndex());
        otherStyle.setFont(otherFont);

        int rowCount = 0;

//        Row title = sheet.createRow(rowCount);
//        title.setHeight((short) (25*20));
//        Cell titleCell = title.createCell(0);
//        titleCell.setCellValue(titleStr);
//        titleCell.setCellStyle(titleStyle);
//
//        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, headStrings.length - 1));
//        rowCount++;

        // create header row
        Row header = sheet.createRow(rowCount);
        header.setHeight((short) (25*20));

        int cellIndex = 0;
        for (int i = 0; i < headStrings.length; i++) {
            Cell cell = header.createCell(cellIndex++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headStrings[i]);
        }

        rowCount++;

//        int dataIndex = 1;

        for(MemberTemplateItem ele:results.getMemberTemplateItems() ){
            int columnIndex = 0;
            Row dataRow = sheet.createRow(rowCount++);
            for (String fld:results.getSelectedField()){
                dataRow.createCell(columnIndex++).setCellValue(GetFieldValue(fld,ele));
            }
            dataRow.setRowStyle(otherStyle);
        }

        for (int i = 0 ; i < headStrings.length ; i++) {
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, (int)(sheet.getColumnWidth(i) * 2));
        }
    }

    private String GetFieldValue(String fieldName,MemberTemplateItem ele){
        String val="";
        int idx=fieldNames.indexOf(fieldName);
        if (idx>-1){
            switch (idx){
                //String, String, String, String, String, String, String, java.util.Date, String, String
                //memberId,gender,aliasName,engLastName,engFirstName,ctDharmaName,ctRefugeMaster,birthDate,twIdNum,passportNum,
                case 0:val= ele.getMemberId();break;
                case 1:val=ele.getGender();break;
                case 2:val=ele.getAliasName();break;
                case 3:val=ele.getEngLastName();break;
                case 4:val=ele.getEngFirstName();break;
                case 5:val=ele.getCtDharmaName();break;
                case 6:val=ele.getCtRefugeMaster();break;
                case 7:val=ele.getBirthDate();break;
                case 8:val=ele.getTwIdNum();break;
                case 9:val=ele.getPassportNum();break;
                // String, String, String, String, String, String, String, String, String, String
                // homePhoneNum1,mobileNum1,officePhoneNum1,email,skillList,healthList,mailingZip,mailingCountry,mailingState,mailingCity
                case 10:val=ele.getHomePhoneNum1();break;
                case 11:val=ele.getMobileNum1();break;
                case 12:val=ele.getOfficePhoneNum1();break;
                case 13:val=ele.getEmail();break;
                case 14:val=ele.getSkillList();break;
                case 15:val=ele.getHealthList();break;
                case 16:val=ele.getMailingZip();break;
                case 17:val=ele.getMailingCountry();break;
                case 18:val=ele.getMailingState();break;
                case 19:val=ele.getMailingCity();break;
                // String, String, String, String, String, String, String, String, String, boolean
                // mailingStreet,birthCountry,nationality,urgentContactPersonName1,urgentContactPersonRelationship1,urgentContactPersonPhoneNum1,introducerName,introducerRelationship,introducerPhoneNum,okSendEletter
                case 20:val=ele.getMailingStreet();break;
                case 21:val=ele.getBirthCountry();break;
                case 22:val=ele.getNationality();break;
                case 23:val=ele.getUrgentContactPersonName1();break;
                case 24:val=ele.getUrgentContactPersonRelationship1();break;
                case 25:val=ele.getUrgentContactPersonPhoneNum1();break;
                case 26:val=ele.getIntroducerName();break;
                case 27:val=ele.getIntroducerRelationship();break;
                case 28:val=ele.getIntroducerPhoneNum();break;
                case 29:val=ele.getOkSendEletter();break;
                // boolean, boolean, String, String, String, String, String, String, String, String
                // okSendMagazine,okSendMessage,companyName,companyJobTitle,schoolDegree,schoolName,schoolMajor,clubName,clubJobTitleNow,clubJobTitlePrev
                case 30:val=ele.getOkSendMagazine();break;
                case 31:val=ele.getOkSendMessage();break;
                case 32:val=ele.getCompanyName();break;
                case 33:val=ele.getCompanyJobTitle();break;
                case 34:val=ele.getSchoolDegree();break;
                case 35:val=ele.getSchoolName();break;
                case 36:val=ele.getSchoolMajor();break;
                case 37:val=ele.getClubName();break;
                case 38:val=ele.getClubJobTitleNow();break;
                case 39:val=ele.getClubJobTitlePrev();break;
                // String, String, String, String, String, java.util.Date, String, boolean, String, boolean
                // parentsName,parentsPhoneNum,parentsJobTitle,parentsEmployer,dataUnitMemberId,dataSentToCTDate,classList,isTakeRefuge,refugeDate,isTakePrecept5
                case 40:val=ele.getParentsName();break;
                case 41:val=ele.getParentsPhoneNum();break;
                case 42:val=ele.getParentsJobTitle();break;
                case 43:val=ele.getParentsEmployer();break;
                case 44:val=ele.getDataUnitMemberId();break;
                case 45:val=ele.getDataSentToCTDate();break;
                case 46:val=ele.getClassList();break;
                case 47:val=ele.getIsTakeRefuge();break;
                case 48:val=ele.getRefugeDate();break;
                case 49:val=ele.getIsTakePrecept5();break;
                // String, boolean, String, String, String
                // precept5Date,isTakeBodhiPrecept,bodhiPreceptDate,donationNameList,dsaJobNameList
                case 50:val=ele.getPrecept5Date();break;
                case 51:val=ele.getIsTakeBodhiPrecept();break;
                case 52:val=ele.getBodhiPreceptDate();break;
                case 53:val=ele.getDonationNameList();break;
                case 54:val=ele.getDsaJobNameList();break;
            }
        }
        return val;
    }
}
