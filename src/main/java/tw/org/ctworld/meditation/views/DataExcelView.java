package tw.org.ctworld.meditation.views;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import tw.org.ctworld.meditation.beans.ClassStats.ClassStats005Object;
import tw.org.ctworld.meditation.beans.ClassStats.ClassStats007Object;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class DataExcelView extends AbstractXlsxView {

    private HttpSession session;

    public DataExcelView(HttpSession session) {
        super();
        this.session = session;
    }

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<ClassStats007Object> results = (List<ClassStats007Object>) model.get("results");
//        ClassStats007Object sample = results.get(0);

        String titleStr = "資料組";
        String[] headStrings = {"No.", "精舍代碼", "精舍簡稱", "學員編號", "性別", "姓名", "法名", "副名", "身分證號", "生日",
                "出生地", "地址", "住家電話", "行動電話", "畢業學校及科系", "工作單位及職稱", "期別", "班別", "組別", "組號",
                "上課起日", "上課迄日", "星期", "全勤", "補後全勤", "結業", "精進獎", "特殊專長", "身心狀況", "本期介紹人",
                "緊急聯絡人", "緊急連絡人電話", "中台信眾編號", "通啟", "月刊", "禪修班職稱", "有帶小組學員"};

        // change the file name
        String filename = "";

        if (results.size() == 0) {

            filename = titleStr + "_無資料匯出";
        }else {

            ClassStats007Object sample = results.get(0);
            String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());
            String userName = (String) session.getAttribute(UserSession.Keys.UserName.getValue());

            filename = unitName + "_" + sample.getClassPeriodNum() + "_" + sample.getClassName() + "_" + titleStr +
                    "_" + userName + "_" + CommonUtils.GetCurrentTimeString() + ".xls";
        }

        filename = URLEncoder.encode(filename, StandardCharsets.UTF_8.name());

        response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + filename);

        // create excel xls sheet
        Sheet sheet = workbook.createSheet(titleStr);

        // create style for title cells
        CellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HorizontalAlignment.CENTER);

        Font font = workbook.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeight((short) (18*20));
        titleStyle.setFont(font);

        // create style for header cells
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.CENTER);

        Font headFont = workbook.createFont();
        headFont.setFontName("Arial");
        headFont.setBold(true);
        headStyle.setFont(headFont);

        // create style for other cells
        CellStyle otherStyle = workbook.createCellStyle();
        otherStyle.setAlignment(HorizontalAlignment.CENTER);

        int rowCount = 0;

        Row title = sheet.createRow(rowCount);
        title.setHeight((short) (25*20));
        Cell titleCell = title.createCell(0);
        titleCell.setCellValue(titleStr);
        titleCell.setCellStyle(titleStyle);

        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, headStrings.length - 1));
        rowCount++;

        // create header row
        Row header = sheet.createRow(rowCount);
        header.setHeight((short) (25*20));

        int cellIndex = 0;
        for (int i = 0; i < headStrings.length; i++) {
            Cell cell = header.createCell(cellIndex++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headStrings[i]);
        }

        rowCount++;

        int dataIndex = 1;

        for (ClassStats007Object record: results) {
            int columnIndex = 0;
            Row dataRow = sheet.createRow(rowCount++);
            dataRow.createCell(columnIndex++).setCellValue(dataIndex++);
            dataRow.createCell(columnIndex++).setCellValue(record.getUnitId());
            dataRow.createCell(columnIndex++).setCellValue(record.getUnitName());
            dataRow.createCell(columnIndex++).setCellValue(record.getMemberId());
            dataRow.createCell(columnIndex++).setCellValue(record.getGender());
            dataRow.createCell(columnIndex++).setCellValue(record.getAliasName());
            dataRow.createCell(columnIndex++).setCellValue(record.getCtDharmaName());
            dataRow.createCell(columnIndex++).setCellValue(record.getNameTail());
            dataRow.createCell(columnIndex++).setCellValue(record.getTwIdNum());
            dataRow.createCell(columnIndex++).setCellValue(record.getBirthDate());
            dataRow.createCell(columnIndex++).setCellValue(record.getBirthCity());
            dataRow.createCell(columnIndex++).setCellValue(record.getAddress());
            dataRow.createCell(columnIndex++).setCellValue(record.getHomePhoneNum1());
            dataRow.createCell(columnIndex++).setCellValue(record.getMobileNum1());
            dataRow.createCell(columnIndex++).setCellValue(record.getSchoolNameaAndMajor());
            dataRow.createCell(columnIndex++).setCellValue(record.getCompanyNameAndJobTitle());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassPeriodNum());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassName());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassGroupId());
            dataRow.createCell(columnIndex++).setCellValue(record.getMemberGroupNum());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassStartDate().toString().split(" ")[0]);
            dataRow.createCell(columnIndex++).setCellValue(record.getClassEndDate().toString().split(" ")[0]);
            dataRow.createCell(columnIndex++).setCellValue(record.getDayOfWeek());
            dataRow.createCell(columnIndex++).setCellValue(record.getIsFullAttended() ? "V" : "");
            dataRow.createCell(columnIndex++).setCellValue(record.getIsMakeupToFullAttended() ? "V" : "");
            dataRow.createCell(columnIndex++).setCellValue(record.getIsGraduated() ? "V" : "");
            dataRow.createCell(columnIndex++).setCellValue(record.getIsDiligentAward() ? "V" : "");
            dataRow.createCell(columnIndex++).setCellValue(record.getSkillList());
            dataRow.createCell(columnIndex++).setCellValue(record.getHealthList());
            dataRow.createCell(columnIndex++).setCellValue(record.getCurrentClassIntroducerName());
            dataRow.createCell(columnIndex++).setCellValue(record.getUrgentContactPersonName1());
            dataRow.createCell(columnIndex++).setCellValue(record.getUrgentContactPersonPhoneNum1());
            dataRow.createCell(columnIndex++).setCellValue(record.getDataUnitMemberId());
            dataRow.createCell(columnIndex++).setCellValue(record.getOkSendMessage() ? "V" : "");
            dataRow.createCell(columnIndex++).setCellValue(record.getOkSendMagazine() ? "V" : "");

            dataRow.createCell(columnIndex++).setCellValue(record.getClassJobTitleList());
            dataRow.createCell(columnIndex++).setCellValue(record.getLeadGroup() ? "V" : "");

            dataRow.setRowStyle(otherStyle);
        }

        for (int i = 0 ; i < headStrings.length ; i++) {
            sheet.autoSizeColumn(i, true);
            int width = (int)(sheet.getColumnWidth(i) * 2);
            try {
                sheet.setColumnWidth(i, width);
            } catch (Exception e) {
                logger.debug(e.getMessage());
            }
        }
    }
}
