package tw.org.ctworld.meditation.views;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import tw.org.ctworld.meditation.beans.ClassReport.ClassAttendance;
import tw.org.ctworld.meditation.beans.ClassReport.ClassMemberRecord;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class AttendanceExcelView extends AbstractXlsxView {

    private static final Logger logger = LoggerFactory.getLogger(AttendanceExcelView.class);

    private HttpSession session;

    public AttendanceExcelView(HttpSession session) {
        super();
        this.session = session;
    }

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<ClassMemberRecord> results = (List<ClassMemberRecord>) model.get("results");
        ClassMemberRecord sample = results.get(0);

        String titleStr =  sample.getClassName() + "_禪修簽到表";
        String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());
        String userName = (String) session.getAttribute(UserSession.Keys.UserName.getValue());
        String filename= unitName + "_" + titleStr + "_" + userName + "_" + CommonUtils.GetCurrentTimeString() + ".xls";
        filename = URLEncoder.encode(filename, StandardCharsets.UTF_8.name());
        response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + filename);

        String[] headStrings = {"No.", "學員編號","姓名","法名","性別","禪修班類別","期別","班別","組別","組號","禪修班職稱","取消", "本期介紹人", "本期介紹人關係", "行動電話", "住家電話"};

        // create excel xls sheet
        Sheet sheet = workbook.createSheet(titleStr);

        // create style for title cells
        CellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HorizontalAlignment.CENTER);

        Font font = workbook.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeight((short) (18*20));
        titleStyle.setFont(font);

        // create style for header cells
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.CENTER);

        Font headFont = workbook.createFont();
        headFont.setFontName("Arial");
        headFont.setBold(true);
        headStyle.setFont(headFont);

        // create style for other cells
        CellStyle otherStyle = workbook.createCellStyle();
        otherStyle.setAlignment(HorizontalAlignment.CENTER);

        // create style for other date cells
        CellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setAlignment(HorizontalAlignment.CENTER);

        Font dateFont = workbook.createFont();
        dateFont.setFontName("Arial");
        dateFont.setBold(true);
        dateStyle.setFont(dateFont);
        dateStyle.setWrapText(true);

        int rowCount = 0;

        Row title = sheet.createRow(rowCount);
        title.setHeight((short) (25*20));
        Cell titleCell = title.createCell(0);
        titleCell.setCellValue(titleStr);
        titleCell.setCellStyle(titleStyle);

        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, headStrings.length + sample.getAttendRecordList().size() - 1));
        rowCount++;

        // create header row
        Row header = sheet.createRow(rowCount);
        header.setHeight((short) (30*20));

        int cellIndex = 0;
        for (int i = 0; i < headStrings.length; i++) {
            Cell cell = header.createCell(cellIndex++);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headStrings[i]);

            if (i == 11) {
                for (int j = 0; j < sample.getAttendRecordList().size(); j++) {
                    Cell cellD = header.createCell(cellIndex++);
                    cellD.setCellStyle(dateStyle);
                    cellD.setCellValue(sample.getAttendRecordList().get(j).getClassWeeksNum() + "\n" + sample.getAttendRecordList().get(j).getClassDate());
                }
            }
        }

        rowCount++;

        int dataIndex = 1;

        for (ClassMemberRecord record: results) {
            int columnIndex = 0;
            Row dataRow = sheet.createRow(rowCount++);
            dataRow.createCell(columnIndex++).setCellValue(dataIndex++);
            dataRow.createCell(columnIndex++).setCellValue(record.getMemberId());
            dataRow.createCell(columnIndex++).setCellValue(record.getAliasName());
            dataRow.createCell(columnIndex++).setCellValue(record.getCtDharmaName());
            dataRow.createCell(columnIndex++).setCellValue(record.getGender());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassTypeName());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassPeriodNum());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassName());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassGroupId());
            dataRow.createCell(columnIndex++).setCellValue(record.getMemberGroupNum());
            dataRow.createCell(columnIndex++).setCellValue(record.getClassJobTitleList());
            dataRow.createCell(columnIndex++).setCellValue(record.isDroppedClass() ? "V" : "");

            for (ClassAttendance attend: record.getAttendRecordList()){
                dataRow.createCell(columnIndex++).setCellValue(attend.getAttendMark());
            }

            dataRow.createCell(columnIndex++).setCellValue(record.getCurrentClassIntroducerName());
            dataRow.createCell(columnIndex++).setCellValue(record.getCurrentClassIntroducerRelationship());
            dataRow.createCell(columnIndex++).setCellValue(record.getMobileNum1());
            dataRow.createCell(columnIndex++).setCellValue(record.getHomePhoneNum1());

            dataRow.setRowStyle(otherStyle);
        }

        for (int i = 0; i < headStrings.length + sample.getAttendRecordList().size(); i++) {
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, (int)(sheet.getColumnWidth(i) * 2));
        }


    }
}
