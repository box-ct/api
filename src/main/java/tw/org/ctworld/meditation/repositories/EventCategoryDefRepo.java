/*
* Auto Generated
* Based on name: event_category_def
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef002Object;
import tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef005Object;
import tw.org.ctworld.meditation.models.EventCategoryDef;

import javax.transaction.Transactional;
import java.util.List;

public interface EventCategoryDefRepo extends JpaRepository<EventCategoryDef, Long> {

    @Query("select max(e.eventCategoryId) from EventCategoryDef e")
    String GetMaxCategoryId();

    @Query("select new tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef002Object(" +
            "e.id, e.eventCategoryId, e.eventCategoryName, e.isCenterAvailable) " +
            "from EventCategoryDef e order by e.eventCategoryId")
    List<EventCategoryDef002Object> GetCategoryList();

    @Query("select new tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef002Object(" +
            "e.id, e.eventCategoryId, e.eventCategoryName, e.isCenterAvailable) " +
            "from EventCategoryDef e where e.isCenterAvailable = false order by e.eventCategoryId")
    List<EventCategoryDef002Object> GetCtEventsMainCategoryList();

    @Query("select new tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef002Object(" +
            "e.id, e.eventCategoryId, e.eventCategoryName, e.isCenterAvailable) " +
            "from EventCategoryDef e where e.isCenterAvailable = true order by e.eventCategoryId")
    List<EventCategoryDef002Object> GetUnitEventsMainCategoryList();

    @Query("select e from EventCategoryDef e where e.id = ?1")
    EventCategoryDef GetEventCategoryDefById(long id);

    @Query("select e from EventCategoryDef e where e.eventCategoryId = ?1")
    EventCategoryDef GetEventCategoryDefByEventCategoryId(String eventCategoryId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventCategoryDef e where e.eventCategoryId = ?1")
    void DeleteEventCategoryDefByEventCategoryId(String eventCategoryId);

    @Query("select count(e) from EventCategoryDef e where e.eventCategoryName = ?1")
    int GetEventCategoryDefCountByEventCategoryName(String eventCategoryName);

    @Query("select new tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef005Object(" +
            "e.eventCategoryId, e.eventCategoryName) " +
            "from EventCategoryDef e where e.eventCategoryName = ?1")
    List<EventCategoryDef005Object> GetCategoryListByEventCategoryName(String eventCategoryName);
}
