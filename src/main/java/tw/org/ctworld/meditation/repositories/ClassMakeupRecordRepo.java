package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.ClassMakeupRecord;

import java.util.List;

public interface ClassMakeupRecordRepo extends JpaRepository<ClassMakeupRecord, Long> {

    // makeup
    @Query("select c from ClassMakeupRecord c where c.classId in ?1")
    List<ClassMakeupRecord> GetRecordByClassIdList(List<String> classIdList);

    // makeup
    @Query("select c from ClassMakeupRecord c where c.classId in ?1 and c.memberId = ?2")
    List<ClassMakeupRecord> GetRecordByClassIdListMemberId(List<String> classIdList, String memberId);

    @Query("select c from ClassMakeupRecord c where c.classId = ?1 and c.memberId = ?2 and c.classWeeksNum = ?3")
    List<ClassMakeupRecord> GetRecordByClassIdMemberIdWeekNum(String classId, String memberId, int classWeekNum);
}
