package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.SysCode;

import java.util.List;

public interface SysCodeRepo extends JpaRepository<SysCode, Long> {

    @Query("select s from SysCode s where s.code_key = ?1")
    List<SysCode> GetSysCodeByKey(String key);
}
