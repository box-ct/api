/*
* Auto Generated
* Based on name: event_summer_donation_record
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.EventSummerDonationRecord;

import javax.transaction.Transactional;
import java.util.List;

public interface EventSummerDonationRecordRepo extends JpaRepository<EventSummerDonationRecord, Long> {

    @Query("select d from EventSummerDonationRecord d where d.enrollUserId = ?1 order by d.summerDonationYear desc")
    List<EventSummerDonationRecord> GetByMemberId(String memberId);

    @Query("select d from EventSummerDonationRecord d where d.enrollUserId = ?1 and d.summerDonationYear = ?2")
    List<EventSummerDonationRecord> GetByMemberIdYear(String memberId, String year);

    @Query("select d.enrollUserId from EventSummerDonationRecord d where d.enrollUserId in ?1 and d.summerDonationYear = ?2")
    List<String> GetByMemberIdsYear(List<String> memberIds, String year);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventSummerDonationRecord d where d.enrollUserId = ?1 and d.summerDonationYear = ?2")
    void DelByMemberIdYear(String memberId, String year);
}
