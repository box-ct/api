package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.MemberInfos.DsaMember;
import tw.org.ctworld.meditation.models.UnitDsaMemberRecord;

import java.util.List;

public interface UnitDsaMemberRecordRepo extends JpaRepository<UnitDsaMemberRecord, Integer> {

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.DsaMember(d.unitName, d.ctDsaYear, d.centerDsaYear, d.dsaJobTitleNew) from UnitDsaMemberRecord d where d.memberId = ?1 and (d.isCancelled is null or d.isCancelled = 0)")
    List<DsaMember> GetRecordsByMemberId(String memberId);
}
