/*
* Auto Generated
* Based on name: event_unit_template
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.EventEnrollMember.TemplateShort;
import tw.org.ctworld.meditation.models.EventUnitTemplate;

import javax.transaction.Transactional;
import java.util.List;

public interface EventUnitTemplateRepo extends JpaRepository<EventUnitTemplate, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitTemplate e where e.eventId = ?1 and e.unitId in ?2")
    void DeleteEventUnitTemplateByEventIdAndUnitIdList(String eventId, List<String> unitIdList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitTemplate e where e.eventId = ?1")
    void DeleteEventUnitTemplateByEventId(String eventId);

//    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.TemplateShort(e.templateName, e.templateUniqueId) " +
//            "from EventUnitTemplate e where e.eventId = ?1")
//    List<TemplateShort> GetByEventId(String eventId);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.TemplateShort(e.templateName, e.templateUniqueId) " +
            "from EventUnitTemplate e where e.eventId = ?1 and e.unitId = ?2 and e.isForMaster = ?3 order by e.createDtTm")
    List<TemplateShort> GetByEventIdUnitId(String eventId, String unitId, boolean isMaster);

    @Query("select count(e) from EventUnitTemplate e where e.eventId = ?1 and e.unitId = ?2 and e.templateName = ?3")
    int CheckTemplateName(String eventId, String unitId, String templateName);

    @Query("select count(e) from EventUnitTemplate e where e.eventId = ?1 and e.unitId = ?2 and e.templateName = ?3 and e.id <> ?4")
    int CheckTemplateNameExclude(String eventId, String unitId, String templateName, long excludeId);

    @Query("select max(e.templateUniqueId) from EventUnitTemplate e where e.templateUniqueId like ?1%")
    String GetMaxTemplateUniqueId(String criteria);

    @Query("select t from EventUnitTemplate t where t.eventId = ?1 and t.templateUniqueId = ?2")
    List<EventUnitTemplate> GetByEventIdTemplateUniqueId(String eventId, String templateUniqueId);
}
