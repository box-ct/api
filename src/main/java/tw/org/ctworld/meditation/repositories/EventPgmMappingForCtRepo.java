/*
* Auto Generated
* Based on name: event_pgm_mapping_for_ct
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.EventPgmMappingForCt;

import javax.transaction.Transactional;

public interface EventPgmMappingForCtRepo extends JpaRepository<EventPgmMappingForCt, Long> {

    @Query("select e from EventPgmMappingForCt e where e.eventId = ?1")
    EventPgmMappingForCt GetEventPgmMappingForCtByEventId(String eventId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventPgmMappingForCt e where e.eventId = ?1")
    void DeleteEventPgmMappingForCtByEventId(String eventId);
}
