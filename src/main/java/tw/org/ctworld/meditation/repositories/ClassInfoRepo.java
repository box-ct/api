package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.Anonymous.AnonymousMakeupClass;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup001Object;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002object;
import tw.org.ctworld.meditation.beans.ClassStats.ClassStats003_2Object;
import tw.org.ctworld.meditation.beans.Common.Common011Object;
import tw.org.ctworld.meditation.beans.Makeup.MakeupClassInfo;
import tw.org.ctworld.meditation.beans.Makeup.Unit;
import tw.org.ctworld.meditation.beans.Member.Members008Object;
import tw.org.ctworld.meditation.beans.DataSearch.DataSearchClassInfo;
import tw.org.ctworld.meditation.models.ClassInfo;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface ClassInfoRepo extends JpaRepository<ClassInfo, Long> {

    @Query("select distinct c.year from ClassInfo c")
    List<String> GetClassYearComboBox();

    @Query("select distinct c.year from ClassInfo c where c.unitId = ?1")
    List<String> GetClassYearComboBox(String unitId);

    @Query("select distinct c.classPeriodNum from ClassInfo c where c.classStatus = ?1 and c.unitId = ?2 order by cast(c.classPeriodNum as int)")
    List<String> GetClassInfoComboBox(String classStatus, String unitId);

    @Query("select distinct c.className from ClassInfo c where c.classStatus = ?1 and c.classPeriodNum = ?2 and c.unitId = ?3")
    List<String> GetClassNameComboBox(String classStatus, String classPeriodNum, String unitId);

    @Query("select c.classId from ClassInfo c where c.classStatus = ?1 and c.classPeriodNum = ?2 and c.className = ?3 and c.unitId = ?4")
    List<String> GetClassId(String classStatus, String classPeriodNum, String className, String unitId);

    @Query("select c.classId from ClassInfo c where c.unitName = ?1")
    List<String> GetClassIdList(String unitName);

    @Query("select c from ClassInfo c where c.classStatus = ?1 order by c.classDayOrNight desc , c.classTypeNum")
    List<ClassInfo> GetClassInfoByClassStatus(String classStatus);

    @Query("select c.classId from ClassInfo c where c.classStatus = ?1 order by c.classDayOrNight desc , c.classTypeNum")
    List<String> GetClassIdByClassStatus2(String classStatus);

    @Query("select c from ClassInfo c where c.classId = ?1")
    ClassInfo GetAClassByClassId(String classId);

    @Query("select c.className from ClassInfo c where c.classId = ?1")
    String GetClassNameByClassID(String classId);

    @Query("select max(c.classId) from ClassInfo c where c.classId like ?1")
    String GetMaxAssetId(String criteria);

    @Query("select c.classId from ClassInfo c where c.classStatus = ?1")
    List<String> GetClassIdByClassStatus(String classStatus);

    @Query("select new tw.org.ctworld.meditation.beans.Anonymous.AnonymousMakeupClass(c.classId, c.className, c.dayOfWeek) from ClassInfo c where c.classStatus = ?1 and c.unitId = ?2")
    List<AnonymousMakeupClass> GetOpeningClassListByClassStatusUnitId(String classStatus, String unitId);

    @Query("select max(c.classPeriodNum) from ClassInfo c where c.classDayOrNight = ?1 and c.classTypeNum = ?2")
    Integer GetMaxClassPeriodNum(String classDayOrNight, int classTypeNum);

    @Query("select max(c.classPeriodNum) from ClassInfo c where c.classDayOrNight = ?1 and c.classTypeNum = ?2 and c.unitId = ?3")
    Integer GetMaxClassPeriodNum(String classDayOrNight, int classTypeNum, String unitId);

    @Query("select count(c) from ClassInfo c where c.classDayOrNight = ?1")
    int GetClassInfoNumByClassDayOrNight(String classDayOrNight);

    @Query("select count(c) from ClassInfo c where c.classTypeNum = ?1")
    int GetClassInfoNumByClassTypeNum(int classTypeNum);

    @Query("select count(c) from ClassInfo c where c.classDayOrNight = ?1 and c.unitId = ?2")
    int GetClassInfoNumByClassDayOrNight(String classDayOrNight, String unitId);

    @Query("select count(c) from ClassInfo c where c.classTypeNum = ?1 and c.unitId = ?2")
    int GetClassInfoNumByClassTypeNum(int classTypeNum, String unitId);

    @Query("select c from ClassInfo c where c.year = ?1 and c.unitId = ?2 order by c.year desc , c.classPeriodNum desc , c.classTypeNum asc ")
    List<ClassInfo> GetClassInfoList1000(String year, String unitId);

    @Query("select c from ClassInfo c where c.classStatus = ?1 and c.unitId = ?2 order by c.year desc , c.classPeriodNum desc , c.classTypeNum asc ")
    List<ClassInfo> GetClassInfoList0100(String classStatus, String unitId);

    @Query("select c from ClassInfo c where c.year = ?1 and c.classStatus = ?2 and c.unitId = ?3 order by c.year desc , c.classPeriodNum desc , c.classTypeNum asc ")
    List<ClassInfo> GetClassInfoList1100(String year, String classStatus, String unitId);

    @Query("select c from ClassInfo c where c.year = ?1 and c.classStatus = ?2 and c.classPeriodNum = ?3 and c.unitId = ?4 order by c.year desc , c.classPeriodNum desc , c.classTypeNum asc ")
    List<ClassInfo> GetClassInfoList1110(String year, String classStatus, String classPeriodNum, String unitId);

    @Query("select c from ClassInfo c where c.year = ?1 and c.classStatus = ?2 and c.classPeriodNum = ?3 and c.className = ?4 and c.unitId = ?5 order by c.year desc , c.classPeriodNum desc , c.classTypeNum asc ")
    List<ClassInfo> GetClassInfoList1111(String year, String classStatus, String classPeriodNum, String className, String unitId);

    @Query("select c from ClassInfo c where c.classStatus = ?1 and c.classPeriodNum = ?2 and c.unitId = ?3 order by c.year desc , c.classPeriodNum desc , c.classTypeNum asc ")
    List<ClassInfo> GetClassInfoList0110(String classStatus, String classPeriodNum, String unitId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassInfo c where c.classId = ?1")
    void DeleteClassInfoByClassId(String classId);

    @Query("select c from ClassInfo c where c.classId = ?1")
    ClassInfo GetClassInfoByClassId(String classId);

    @Query("select c.minutesBeLate from ClassInfo c where c.classId = ?1")
    int GetMinutesBeLateByClassId(String classId);

    @Query("select new tw.org.ctworld.meditation.beans.Member.Members008Object(c.classStatus, c.className, c.classPeriodNum, c.classId) " +
            "from ClassInfo c where (c.classStatus = '可報名' or c.classStatus = '開課中') and c.unitId = ?1 " +
            "order by c.classStatus, c.classDayOrNight desc, c.classTypeNum")
    List<Members008Object> GetClassDataList(String unitId);

    @Query("select c from ClassInfo c where c.unitId = ?1 and c.classId = ?2 and c.classPeriodNum = ?3")
    List<ClassInfo> GetClassInfoByUnitIdClassIdClassPeriodNum(String unitId, String classId, String classPeriodNum);

    // DataSearch Class/Group Sub-total
    @Query("select new tw.org.ctworld.meditation.beans.DataSearch.DataSearchClassInfo(c.classId, c.className, c.classTypeNum) from ClassInfo c where c.unitId = ?1 and c.classStatus = ?2 order by c.classTypeNum, substr(c.className, 0, 1)")
    List<DataSearchClassInfo> GetClassInfoByUnitIdClassStatus(String unitId, String classStatus);

    // DataSearch get history periods
    @Query("select distinct c.classPeriodNum from ClassInfo c where c.unitId = ?1 and c.classStatus <> '可報名' and c.classStatus <> '未啟動' order by cast(c.classPeriodNum as int) desc")
    List<String> GetClassPeriods(String unitId);

    // DataSearch class count by unit id and class status
    @Query("select count(c.classId) from ClassInfo c where c.unitId = ?1 and c.classStatus = ?2")
    Long GetClassCountByUnitStatus(String unitId, String classStatus);

    @Query("select new tw.org.ctworld.meditation.beans.ClassStats.ClassStats003_2Object(c.absenceCountToFullAttend, " +
            "c.absenceCountToGraduate, c.makeupCountToGraduate, c.absenceCountToDiligentAward, c.minMakeupCountToDiligentAward, c.maxMakeupCountToDiligentAward) " +
            "from ClassInfo c where c.classId = ?1")
    ClassStats003_2Object GetClassStats003_2ObjectByClassId(String classId);

    @Query("select distinct c.classPeriodNum from ClassInfo c where c.year = ?1 and c.classStatus = ?2 and c.unitId = ?3")
    List<String> GetClassPeriodNumList1(String year, String classStatus, String unitId);

//    @Query("select distinct c.classPeriodNum from ClassInfo c inner join ClassEnrollForm e on c.classId = e.classId " +
//            "where c.year = ?1 and c.classStatus = ?2 and c.unitId = ?3 and e.memberId = ?4 and " +
//            "e.classJobTitleList <> null and e.isExecuteClassAttendRecord = 1")
//    List<String> GetClassPeriodNumList2(String year, String classStatus, String unitId, String memberId);

    @Query("select distinct c.classPeriodNum from ClassInfo c where c.classStatus = ?1 and c.unitId = ?2")
    List<String> GetClassPeriodNumList3(String classStatus, String unitId);

    @Query("select distinct c.classPeriodNum from ClassInfo c inner join ClassEnrollForm e on c.classId = e.classId " +
            "where c.classStatus = ?1 and c.unitId = ?2 and e.memberId = ?3 and e.classJobTitleList <> null ")
    List<String> GetClassPeriodNumList4(String classStatus, String unitId, String memberId);

//    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002object(c.classId, c.className) " +
//            "from ClassInfo c where c.year = ?1 and c.classStatus = ?2 and c.classPeriodNum = ?3 and  c.unitId = ?4")
//    List<ClassRecord002object> GetClassNameList1(String year, String classStatus, String classPeriodNum, String unitId);

    // for master
    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002object(c.classId, c.className, c.classPeriodNum, c.classStatus, c.classDayOrNight, c.classTypeNum) " +
            "from ClassInfo c where (?1 is null or c.year = ?1) and (?2 is null or c.classStatus = ?2) and (?2 is not null or c.classStatus = '開課中' or c.classStatus = '待結業') and c.unitId = ?3")
    List<ClassRecord002object> GetClassNameList(String year, String classStatus, String unitId);

    // for leader
    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002object(c.classId, c.className, c.classPeriodNum, c.classStatus, c.classDayOrNight, c.classTypeNum) " +
            "from ClassInfo c where (?1 is null or c.year = ?1) and (?2 is null or c.classStatus = ?2) and (?2 is not null or c.classStatus = '開課中' or c.classStatus = '待結業') and c.classId in ?3 order by c.classStatus")
    List<ClassRecord002object> GetClassNameList(String year, String classStatus, List<String> classIds);

//    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002object(c.classId, c.className) " +
//            "from ClassInfo c inner join ClassEnrollForm e on c.classId = e.classId " +
//            "where c.year = ?1 and c.classStatus = ?2 and c.classPeriodNum = ?3 and c.unitId = ?4 and " +
//            "e.classJobTitleList <> null and e.memberId = ?5 and e.isExecuteClassAttendRecord = 1")
//    List<ClassRecord002object> GetClassNameList2(String year, String classStatus, String classPeriodNum, String unitId, String memberId);

//    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002object(c.classId, c.className) " +
//            "from ClassInfo c where c.classStatus = ?1 and c.classPeriodNum = ?2 and  c.unitId = ?3")
//    List<ClassRecord002object> GetClassNameList3(String classStatus, String classPeriodNum, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002object(c.classId, c.className, c.classPeriodNum) " +
            "from ClassInfo c where c.classStatus = ?1 and c.unitId = ?2")
    List<ClassRecord002object> GetClassNameList3(String classStatus, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002object(c.classId, c.className) " +
            "from ClassInfo c inner join ClassEnrollForm e on c.classId = e.classId " +
            "where c.classStatus = ?1 and c.classPeriodNum = ?2 and c.unitId = ?3 and e.classJobTitleList <> null and e.memberId = ?4")
    List<ClassRecord002object> GetClassNameList4(String classStatus, String classPeriodNum, String unitId, String memberId);

    @Query("select c.className from ClassInfo c where c.classPeriodNum = ?1 and c.unitId = ?2")
    List<String> GetClassListByClassPeriodNumAndUnitId(String classPeriodNum, String unitId);

    @Query("select c.classId from ClassInfo c where c.unitId = ?1 and c.classDayOrNight = ?2 and c.classTypeName = ?3 and c.classPeriodNum = ?4 and c.className like %?5%")
    List<String> FindClasses(String unitId, String classDayOrNight, String classTypeName, String classPeriodNum, String className);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassInfo i set i.classStatus = '開課中' where i.classStatus = '可報名' and i.classStartDate = ?1")
    void UpdateClassStatusOpen(Date classStartDate);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassInfo i set i.classStatus = '待結業' where i.classStatus = '開課中' and i.classEndDate <= ?1")
    void UpdateClassStatusClose(Date classEndDate);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassInfo i set i.classStatus = '已結業' where i.classStatus = '待結業' and i.classEndDate <= ?1")
    void UpdateClassStatusDone(Date classEndDate);

    @Query("select i from ClassInfo i where i.classStartDate = ?1")
    List<ClassInfo> GetClassStatus(Date classStartDate);

    @Query("select classId from ClassInfo where classId in ?1 " +
            "order by classPeriodNum desc, classTypeNum asc, classDayOrNight desc")
    List<String> GetClassIdListByClassIdList(List<String> classIdList);

    @Query("select c from ClassInfo c where classId in ?1")
    List<ClassInfo> GetClassListByClassIdList(List<String> classIdList);

    // 資料查詢 - 證件檢核 - 列表
    @Query("select c.classId from ClassInfo c where c.unitId = ?1 and c.classStatus = '開課中' order by c.classId")
    List<String> GetOpeningClassIdsByUnitId(String unitId);

    // 資料查詢 - 證件檢核 - 新/舊生
    @Query("select c.classId from ClassInfo c where c.unitId = ?1 and (c.classStatus = '待結業' or c.classStatus = '已結業') order by c.classId")
    List<String> GetClosedClassIdsByUnitId(String unitId);

    // 取得課程列表
    @Query("select new tw.org.ctworld.meditation.beans.Common.Common011Object(" +
            "c.year, c.classStatus, c.classPeriodNum, c.classId, c.className) " +
            "from ClassInfo c where c.unitId = ?1 order by c.classTypeNum desc, cast(c.classPeriodNum as int) desc")
    List<Common011Object> GetClassListByUnitId(String unitId);

    @Query("select new tw.org.ctworld.meditation.models.ClassInfo(c.classId, c.className, c.dayOfWeek, c.classPeriodNum, c.classStatus) " +
            "from ClassInfo c where classId in ?1")
    List<ClassInfo> GetClassListByClassIdList2(List<String> classIdList);

    @Query("select c.classId from ClassInfo c where classId in ?1 and c.classStatus = '開課中'")
    List<String> GetClassIdListByClassIdList2(List<String> classIdList);

    // makeup
    @Query("select new tw.org.ctworld.meditation.beans.Makeup.Unit(c.unitId, c.unitName) from ClassInfo c where c.year = ?1 group by c.unitId")
    List<Unit> GetDistinctUnitByYear(String year);

    // makeup
    @Query("select c.classStatus from ClassInfo c where c.year = ?1 and c.unitId = ?2 group by c.classStatus")
    List<String> GetDistinctStatusByYearUnit(String year, String unitId);

    // makeup
    @Query("select new tw.org.ctworld.meditation.beans.Makeup.MakeupClassInfo(c.year, c.unitName, c.classId, c.classStatus, c.classStartDate, c.classEndDate, c.dayOfWeek, c.classDayOrNight, c.classTypeName, c.classPeriodNum, c.className, c.classDesc, c.classTotalNum) from ClassInfo c where c.year = ?1 and c.unitId = ?2 and c.classStatus = ?3")
    List<MakeupClassInfo> GetClassesByYearUnitStatus(String year, String unitId, String classStatus);

    // makeup
    @Query("select new tw.org.ctworld.meditation.beans.Makeup.MakeupClassInfo(c.year, c.unitName, c.classId, c.classStatus, c.classStartDate, c.classEndDate, c.dayOfWeek, c.classDayOrNight, c.classTypeName, c.classPeriodNum, c.className, c.classDesc, c.classTotalNum) from ClassInfo c where c.classId = ?1")
    List<MakeupClassInfo> GetClassByClassId(String classId);

    // makeup
    @Query("select new tw.org.ctworld.meditation.beans.Makeup.MakeupClassInfo(c.year, c.unitName, c.classId, c.classStatus, c.classStartDate, c.classEndDate, c.dayOfWeek, c.classDayOrNight, c.classTypeName, c.classPeriodNum, c.className, c.classDesc, c.classTotalNum) " +
            "from ClassInfo c where (?1 is null or c.unitId = ?1) and (?2 is null or c.year = ?2) and (?3 is null or c.classStatus = ?3) and (?4 is null or c.classId = ?4)")
    List<MakeupClassInfo> GetClassByUnitIdYearStatusClassIdNullable(String unitId, String year, String classStatus, String classId);

    @Query("select c.className from ClassInfo c where c.classId = ?1")
    List<String> GetClassName(String classId);

    @Query("select new tw.org.ctworld.meditation.models.ClassInfo(" +
            "c.unitName, c.classDayOrNight, c.classTypeName, c.classTypeNum, c.classPeriodNum, c.certificateName, c.certificateEngName, c.classDesc) " +
            "from ClassInfo c where c.classId = ?1")
    ClassInfo GetClassInfoByClassId2(String classId);

    // classes
    @Query("select c from ClassInfo c where c.classId = ?1")
    List<ClassInfo> FindClassByClassId(String classId);

    // get years unitId is nullable
    @Query("select c.year from ClassInfo c where (?1 is null or c.unitId = ?1) group by c.year order by c.year")
    List<String> GetYears(String unitId);

    // class record
    @Query("select c.classStatus from ClassInfo c where c.unitId = ?1 and (?2 is null or c.year = ?2) group by c.classStatus order by c.classStatus")
    List<String> GetStatusByUnitYear(String unit, String year);

    // class record
    @Query("select c.classStatus from ClassInfo c where c.classId in ?1 and (?2 is null or c.year = ?2) group by c.classStatus order by c.classStatus")
    List<String> GetStatusByClassIds(List<String> classIds, String year);

    // class record
    @Query("select new tw.org.ctworld.meditation.models.ClassInfo(" +
            "c.classStatus, c.classId, c.year, c.classStartDate, c.classEndDate, c.dayOfWeek, c.classDayOrNight, " +
            "c.classTypeName, c.classPeriodNum, c.className, c.classTotalNum, c.classDesc) " +
            "from ClassInfo c where c.unitId = ?1 and (?2 is null or c.year = ?2) and (?3 is null or c.classStatus = ?3) order by c.classStatus")
    List<ClassInfo> GetClassInfoListByYearAndClassStatus(String unitId, String year, String classStatus);

    // class record
    @Query("select new tw.org.ctworld.meditation.models.ClassInfo(" +
            "c.classStatus, c.classId, c.year, c.classStartDate, c.classEndDate, c.dayOfWeek, c.classDayOrNight, " +
            "c.classTypeName, c.classPeriodNum, c.className, c.classTotalNum, c.classDesc) " +
            "from ClassInfo c where c.unitId = ?1 and c.classStatus in ('開課中', '待結業') order by c.year, c.classStatus desc ")
    List<ClassInfo> GetClassInfoList(String unitId);

    // class upgrade
    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002object(c.classId, c.className, c.classPeriodNum, c.classStatus, c.classDayOrNight, c.classTypeNum) " +
            "from ClassInfo c where c.unitId = ?1 and (?2 is null or c.classId <> ?2) and (c.classStatus = '可報名' or c.classStatus = '開課中')")
    List<ClassRecord002object> GetUpgradeClassList(String unitId, String classId);

    @Query("select c from ClassInfo c where c.classId in ?1")
    List<ClassInfo> GetClassesByClassIdList(List<String> classIds);

    // makeup
    @Query("select new tw.org.ctworld.meditation.beans.Makeup.MakeupClassInfo(c.year, c.unitName, c.classId, c.classStatus, c.classStartDate, c.classEndDate, c.dayOfWeek, c.classDayOrNight, c.classTypeName, c.classPeriodNum, c.className, c.classDesc, c.classTotalNum) " +
            "from ClassInfo c where (?1 is null or c.unitId = ?1) and (?2 is null or c.year = ?2) and (?3 is null or c.classStatus = ?3) and (?3 is not null or c.classStatus = '開課中' or c.classStatus = '待結業') and c.classId in ?4 order by c.year, c.classStatus desc ")
    List<MakeupClassInfo> GetClassByUnitIdYearStatusClassIdNullable(String unitId, String year, String classStatus, List<String> classIds);

    @Query("select new tw.org.ctworld.meditation.beans.Makeup.MakeupClassInfo(c.year, c.unitName, c.classId, c.classStatus, c.classStartDate, c.classEndDate, c.dayOfWeek, c.classDayOrNight, c.classTypeName, c.classPeriodNum, c.className, c.classDesc, c.classTotalNum) " +
            "from ClassInfo c where (?1 is null or c.unitId = ?1) and (?2 is null or c.year = ?2) and (?3 is null or c.classStatus = ?3) and (?3 is not null or c.classStatus = '開課中' or c.classStatus = '待結業') order by c.year, c.classStatus desc ")
    List<MakeupClassInfo> GetClassByUnitIdYearStatusClassIdNullable(String unitId, String year, String classStatus);

    @Query("select new tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup001Object(c.classId, c.classPeriodNum, c.className) " +
            "from ClassInfo c where c.unitId = ?1 and (c.classStatus = '開課中' or c.classStatus = '待結業') " +
            "order by c.classPeriodNum desc, c.classTypeNum desc")
    List<ClassGroup001Object> GetClassesByUnitId(String unitId);
}
