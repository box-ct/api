package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain003_1Object;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain002Object;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp002_1Object;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp003_1Object;
import tw.org.ctworld.meditation.models.EventMain;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface EventMainRepo extends JpaRepository<EventMain, Long> {

    @Query("select e.eventId from EventMain e where e.eventStartDate >= ?1 and e.eventEndDate <= ?2")
    List<String> GetEventsByUnitDates(String startDate, String endDate);

    @Query("select new tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain003_1Object(" +
            "c.eventYear, c.sponsorUnitName, c.eventCategoryName, c.eventName, c.eventStartDate, c.eventEndDate, " +
            "c.status, c.eventNote, c.eventId, c.updatorName, c.updateDtTm, c.updateDtTm) " +
            "from EventMain c where c.isCreatedByCt = true order by c.eventStartDate desc")
    List<CtEventMaintain003_1Object> GetCtEventsMainList();

    @Query("select max(e.eventId) from EventMain e where e.eventId like ?1")
    String GetMaxEventId(String eventId);

    @Query("select e from EventMain e where e.eventId = ?1")
    EventMain GetClassEventMainByEventId(String eventId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventMain e where e.eventId = ?1")
    void DeleteEventMainByEventId(String eventId);

    @Query("select new tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain002Object(" +
            "e.eventYear, e.sponsorUnitName, e.eventCategoryName, e.eventCategoryColorCode, e.eventName, e.eventStartDate, e.eventEndDate, " +
            "e.status, e.eventNote, e.eventId, e.updatorName, e.updateDtTm) " +
            "from EventMain e where e.eventCreatorUnitId = ?1 " +
            "order by case when e.eventStartDate is null then 1 else 0 end, e.eventStartDate desc")
    List<UnitEventsMaintain002Object> GetEventMainByUnitId(String unitId);

    @Query("select e.eventName from EventMain e where e.eventId = ?1")
    String GetEventNameByEventId(String eventId);

    @Query("select new tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp002_1Object(" +
            "e.eventYear, e.sponsorUnitName, e.eventName, e.eventStartDate, e.eventEndDate, e.isNeedAbbotSigned, " +
            "e.status, e.eventNote, e.eventId) from EventMain e " +
            "where e.status in (1, 2, 3) and e.attendUnitList like %?1% order by e.eventStartDate desc")
    List<UnitStartUp002_1Object> GetEventNameListByUnitId(String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp003_1Object(" +
            "e.eventYear, e.sponsorUnitName, e.eventName, e.eventStartDate, e.eventEndDate, e.isNeedAbbotSigned, " +
            "e.status, e.eventNote, e.eventId, e.eventStartTime, eventEndTime) from EventMain e where e.eventId = ?1")
    UnitStartUp003_1Object GetEventNameListByEventIdList(String eventId);


    @Query("select new tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain002Object(" +
            "e.eventYear, e.sponsorUnitName, e.eventCategoryName, e.eventCategoryColorCode, e.eventName, e.eventStartDate, e.eventEndDate, " +
            "e.status, e.eventNote, e.eventId, e.updatorName, e.updateDtTm, e.isNeedAbbotSigned, e.isOnlyForMaster) " +
            "from EventMain e where (?1 is null or e.attendUnitList like %?1%) " +
            "order by case when e.eventStartDate is null then 1 else 0 end, e.eventStartDate desc, e.eventStartTime")
    List<UnitEventsMaintain002Object> GetEventMainAttendByUnitId(String unitId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update EventMain e set e.status = 2, e.updatorId = 'cron', e.updatorName = '活動狀態更改排程(每日AM00:00執行)', " +
            "e.updateDtTm = ?1, e.updatorIp = '127.0.0.1', e.updatorUnitId = 'UNIT02005', " +
            "e.updatorUnitName = '資訊中心' where e.enrollEndDate < ?1 and e.status < 2")
    void setEnrollEndDayDue(Date now);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update EventMain e set e.status = 3, e.updatorId = 'cron', e.updatorName = '活動狀態更改排程(每日AM00:00執行)', " +
            "e.updateDtTm = ?1, e.updatorIp = '127.0.0.1', e.updatorUnitId = 'UNIT02005', " +
            "e.updatorUnitName = '資訊中心' where e.changeEndDate < ?1 and e.status < 3")
    void setChangeEndDayDue(Date now);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update EventMain e set e.status = 7, e.updatorId = 'cron', e.updatorName = '活動狀態更改排程(每日AM00:00執行)', " +
            "e.updateDtTm = ?1, e.updatorIp = '127.0.0.1', e.updatorUnitId = 'UNIT02005', " +
            "e.updatorUnitName = '資訊中心' where e.finalDate < ?1 and e.status < 7")
    void setFinalDayDue(Date now);

    @Query("select e.eventId from EventMain e where e.status >= 1 and e.status <= 3 and e.isSyncCtTable = 1")
    List<String> GetEventIdsForSync();

}
