/*
* Auto Generated
* Based on name: ct_member_import_log
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.MemberImport.MemberImportLog;
import tw.org.ctworld.meditation.models.CtMemberImportLog;

import java.util.Date;
import java.util.List;

public interface CtMemberImportLogRepo extends JpaRepository<CtMemberImportLog, Long> {

    @Query("select new tw.org.ctworld.meditation.beans.MemberImport.MemberImportLog(" +
            "m.memberId,m.aliasName,m.ctDharmaName,m.importResultType,m.importContent,m.creatorName,m.createDtTm,c.timezone) " +
            "from CtMemberImportLog m inner join UnitInfo c on m.unitId=c.unitId " +
            "where m.unitId=?1 and m.createDtTm between ?2 and ?3  ")
    List<MemberImportLog> GetMemberImportLog(String unitId, Date startdate, Date enddate);

}
