package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.Member.Members002Object;
import tw.org.ctworld.meditation.models.ClassCommonFilepath;

import java.util.List;

public interface ClassCommonFilepathRepo extends JpaRepository<ClassCommonFilepath, Long> {

    @Query("Select p.filePath from ClassCommonFilepath p where p.fileName like ?1%")
    List<String> GetAllFilePathByMemberId(String memberId);

//    @Query("Select p from ClassCommonFilepath p where p.filePathKind = 'PHOTO' and substring(p.fileName, 1, 9) in ?1")
//    List<ClassCommonFilepath> GetAllFilePathByMemberId(List<String> memberIds);

    @Query("Select new tw.org.ctworld.meditation.beans.Member.Members002Object(p.filePathKind, p.filePathRoot, " +
            "p.filePath, p.fileName) " +
            "from ClassCommonFilepath p where p.fileName like ?1% " +
            "order by p.filePathKind asc, p.updateDtTm")
    List<Members002Object> GetFilePathByMemberId(String memberId);

    @Query("Select new tw.org.ctworld.meditation.beans.Member.Members002Object(p.filePathKind, p.filePathRoot, " +
            "p.filePath, p.fileName) " +
            "from ClassCommonFilepath p where p.filePath in ?1 " +
            "order by p.filePathKind asc, p.updateDtTm desc")
    List<Members002Object> GetFilePathByFilePathRootList(List<String> filePathRootList);

    @Query("Select new tw.org.ctworld.meditation.models.ClassCommonFilepath(p.filePathRoot, p.filePath, p.fileName) " +
            "from ClassCommonFilepath p")
    List<ClassCommonFilepath> GetAllFilePathByMemberId();
}
