/*
* Auto Generated
* Based on name: event_unit_template_pgm
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.EventEnrollPgm;
import tw.org.ctworld.meditation.models.EventUnitTemplatePgm;

import javax.transaction.Transactional;
import java.util.List;

public interface EventUnitTemplatePgmRepo extends JpaRepository<EventUnitTemplatePgm, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitTemplatePgm e where e.eventId = ?1 and e.unitId in ?2")
    void DeleteEventUnitTemplatePgmByEventIdAndUnitIdList(String eventId, List<String> unitIdList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitTemplatePgm e where e.eventId = ?1")
    void DeleteEventUnitTemplatePgmByEventId(String eventId);

    @Query("select e from EventUnitTemplatePgm e where e.pgmUniqueId = ?1")
    List<EventUnitTemplatePgm> GetEventUnitTemplatePgmListByPgmUniqueId(String pgmUniqueId);

    @Query("select e from EventUnitTemplatePgm e where e.eventId = ?1 and e.templateUniqueId = ?2")
    List<EventUnitTemplatePgm> GetByEventIdTemplateUniqueId(String eventId, String templateUniqueId);
}
