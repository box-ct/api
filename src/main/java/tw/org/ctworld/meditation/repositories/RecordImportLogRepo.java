package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tw.org.ctworld.meditation.models.RecordImportLog;

public interface RecordImportLogRepo extends JpaRepository<RecordImportLog, Long> {
}
