package tw.org.ctworld.meditation.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.DataReport.DataReport007Object;
import tw.org.ctworld.meditation.models.UnitInfo;

import javax.transaction.Transactional;
import java.util.List;

public interface UnitInfoRepo extends JpaRepository<UnitInfo, Long> {

    @Cacheable(value = "group")
    @Query("select c.groupName from UnitInfo c group by c.groupName order by MIN(id)")
    List<String> GetGroups();

    @Query("select u from UnitInfo u order by u.sortNum")
    List<UnitInfo> GetUnits();

    @Cacheable(value = "groupUnits", key = "#p0")
    @Query("select u from UnitInfo u where u.groupName = ?1 order by u.sortNum")
    List<UnitInfo> GetUnitsByGroup(String group);

    @Query("select district from UnitInfo where district <> null group by district order by MIN(sortNum) ASC ")
    List<String> GetDistricts();

    @Query("select u from UnitInfo u where u.district = ?1 order by u.sortNum")
    List<UnitInfo> GetUnitsByDistrict(String district);

    @Query("select u.groupName from UnitInfo u where u.unitName = ?1")
    String FindUnitGroupByName(String unitName);

    @Query("select u from UnitInfo u where u.unitId <> ?1 order by u.sortNum")
    List<UnitInfo> GetUnitList(String unitId);

    @Query("select u from UnitInfo u where  u.unitId = ?1")
    List<UnitInfo> FindUnitInfoByUnitId(String unitId);

    // check if unit id exists
    @Query("select case when count(u) > 0 then true else false end from UnitInfo u where u.unitId = ?1")
    boolean IsUnitIdExist(String unitId);

    @Query("select u.unitName from UnitInfo u where u.unitId = ?1")
    String GetUnitNameById(String unitId);

    @Query("select u from UnitInfo u order by u.sortNum")
    List<UnitInfo> GetLoginUnitList();

    @Query("select distinct u.district from UnitInfo u where u.district <> null")
    List<String> GetUnitDistrictList();

    @Query("select distinct u.groupName from UnitInfo u")
    List<String> GetUnitGroupNameList();

    @Query("select u from UnitInfo u where u.unitId = ?1")
    UnitInfo GetClassUnitInfoByUnitId(String unitId);

//    @Query("select u.timezone from UnitInfo u where u.unitId = ?1")
//    String GetClassUnitTimeZoneByUnitId(String unitId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update UnitInfo u set u.isNoIpLimit = 1 where u.isNoIpLimit = 0")
    void BypassAllIp();

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport007Object(u.unitId, u.isOverseasUnit) " +
            "from UnitInfo u where u.groupName = '精舍'")
    List<DataReport007Object> GetUnitObjectList();

    @Query("select u.unitId from UnitInfo u where u.groupName = '精舍' order by u.sortNum")
    List<String> GetAllAbodeId();

    @Query("select u.unitId from UnitInfo u where u.groupName = '精舍' and u.isOverseasUnit = 0 order by u.sortNum")
    List<String> GetDomesticAbodeId();

    @Query("select u.unitId from UnitInfo u where u.groupName = '精舍' and u.isOverseasUnit = 1 order by u.sortNum")
    List<String> GetOverseaAbodeId();

    @Query("select new tw.org.ctworld.meditation.models.UnitInfo(u.unitId, u.unitName, u.unitStartIp, u.unitEndIp, u.timezone) " +
            "from UnitInfo u where u.unitStartIp like ?1% or u.unitEndIp like ?1%")
    List<UnitInfo> GetClassUnitInfoLikeIp(String ip);

    @Query("select new tw.org.ctworld.meditation.models.UnitInfo" +
            "(u.unitId, u.abbotName, u.abbotEngName, u.certUnitEngName, u.stateName, u.certUnitName) " +
            "from UnitInfo u where u.unitId = ?1")
    UnitInfo GetCertUnitNameByUnitId(String unitId);
}
