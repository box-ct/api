package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain003_2Object;
import tw.org.ctworld.meditation.beans.DataSearch.DataSearchEventEnrollObject;
import tw.org.ctworld.meditation.beans.EventEnrollMember.EnrollSigned;
import tw.org.ctworld.meditation.beans.MemberInfos.EventItem;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp007Object;
import tw.org.ctworld.meditation.models.EventEnrollMain;

import javax.transaction.Transactional;
import java.util.List;

public interface EventEnrollMainRepo extends JpaRepository<EventEnrollMain, Long> {

    @Query("select e from EventEnrollMain e where e.enrollUserId = ?1 and e.unitId = ?2 order by e.arriveCtDate desc ")
    List<EventEnrollMain> GetClassEventEnrollMainActivityRecord(String memberId, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.EventItem(e.enrollUserId, e.enrollUserName, e.enrollUserCtDharmaName, e.classPeriodNum, e.className, e.classGroupId, e.eventName, e.arriveCtDate, e.leaveCtDate, e.idType1, e.idType2, e.creatorUnitName) " +
            "from EventEnrollMain e " +
            "where e.enrollUserId = ?1 and (?2 is null or e.unitId = ?2) order by e.arriveCtDate desc ")
    List<EventItem> GetClassEventEnrollMainRecord(String memberId, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.DataSearch.DataSearchEventEnrollObject(" +
            "e.enrollUserId, e.classPeriodNum, e.className, e.gender, e.classGroupId, " +
            "e.enrollUserName, e.enrollUserCtDharmaName, e.eventId, e.eventName, e.idType1, e.idType2) " +
            "from EventEnrollMain e inner join EventMain m on e.eventId = m.eventId " +
            "where e.unitId = ?1 and e.eventId in ?2 order by e.classPeriodNum, e.classId, e.classGroupId")
    List<DataSearchEventEnrollObject> GetEventEnrollMainByUnitEvents(String unitId, List<String> eventIds);

    @Query("select e from EventEnrollMain e where e.eventId = ?1 and e.isCanceled = false ")
    List<EventEnrollMain> GetClassEventEnrollMainListByEventId(String eventId);

    @Query("select new tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain003_2Object(" +
            "e.eventId, e.isMaster, e.isAbbotSigned) " +
            "from EventEnrollMain e where e.eventId in ?1 and e.isCanceled = false ")
    List<CtEventMaintain003_2Object> GetRecordsByEventIdList(List<String> eventIdList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventEnrollMain e where e.eventId = ?1 and e.unitId in ?2")
    void DeleteEventEnrollMainByEventIdAndUnitIdList(String eventId, List<String> unitIdList);

    @Query("select e from EventEnrollMain e where e.eventId = ?1 and e.unitId in ?2")
    List<EventEnrollMain> GetEventEnrollMainListByEventIdAndUntIdList(String eventId, List<String> unitIdList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventEnrollMain e where e.eventId = ?1")
    void DeleteEventEnrollMainByEventId(String eventId);

    @Query("select new tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp007Object(e.enrollUserId, " +
            "e.enrollUserName, e.enrollUserCtDharmaName, e.gender, e.birthDate, e.dsaJobNameList, e.updateDtTm, e.mobileNum1) " +
            "from EventEnrollMain e where e.eventId = ?1 and e.isCanceled = false ")
    List<UnitStartUp007Object> GetClassEventEnrollMainListByEventId2(String eventId);

    @Query("select e.enrollUserId from EventEnrollMain e where e.eventId = ?1 and e.enrollUserId in ?2 and e.isCanceled = false ")
    List<String> GetEnrollUserIdListByEventIdAndEnrollUserIdList(String eventId, List<String> enrollUserIdList);

    @Query("select e.enrollUserId from EventEnrollMain e where e.eventId = ?1 and e.enrollUserId = ?2 and e.isCanceled = false ")
    String GetEnrollUserIdByEventIdAndEnrollUserId(String eventId, String enrollUserId);

    @Query("select e from EventEnrollMain e where (?1 is null or e.unitId = ?1) and e.eventId = ?2 and e.isMaster <> 1 order by e.updateDtTm desc")
    List<EventEnrollMain> GetEnrolledMembersByEventId(String unitId, String eventId);

    @Query("select e from EventEnrollMain e where (?1 is null or e.unitId = ?1) and e.eventId = ?2 and e.isMaster = 1 order by e.enrollUnitId")
    List<EventEnrollMain> GetEnrolledMastersByEventId(String unitId, String eventId);

    @Query("select distinct e.enrollUserId from EventEnrollMain e where (?1 is null or e.unitId = ?1) and e.eventId = ?2")
    List<String> GetEnrolledMemberIdsByEventId(String unitId, String eventId);

    @Query("select max(e.formAutoAddNum) from EventEnrollMain e where e.formAutoAddNum like ?1%")
    String GetMaxFormAutoAddNum(String criteria);

    @Query("select max(e.eventFormUid) from EventEnrollMain e where e.eventFormUid like ?1%")
    String GetMaxEventFormUid(String criteria);

    @Query("select count(e) from EventEnrollMain e where e.eventId = ?1 and e.enrollUserId = ?2")
    long CheckEnrollByMemberIdEventId(String eventId, String memberId);

    @Query("select count(e) from EventEnrollMain e where e.eventId = ?1 and e.enrollUserId in ?2")
    long CheckEnrollByMemberIdEventIds(String eventId, List<String> memberIds);

    @Query("select e from EventEnrollMain e where e.eventId = ?1 and e.formAutoAddNum = ?2")
    List<EventEnrollMain> GetEnrolledByEventIdFormAutoAddNum(String eventId, String formAutoAddNum);

    @Query("select e from EventEnrollMain e where e.formAutoAddNum = ?1")
    List<EventEnrollMain> GetEnrolledByFormAutoAddNum(String formAutoAddNum);

    @Query("select e from EventEnrollMain e where e.eventId = ?1 and e.formAutoAddNum in ?2")
    List<EventEnrollMain> GetEnrolledByEventIdFormAutoAddNums(String eventId, List<String> formAutoAddNums);

    @Query("select e from EventEnrollMain e where e.eventId = ?1 and e.templateUniqueId = ?2")
    List<EventEnrollMain> GetEnrolledByApplyTemplateId(String eventId, String templateId);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.EnrollSigned(e.eventId, e.unitId, e.isAbbotSigned, e.isCtApproved, e.isMaster) " +
            "from EventEnrollMain e where e.eventId in ?1 and e.isCanceled <> 1 and (?2 is null or e.enrollUnitId = ?2)")
    List<EnrollSigned> GetSigned(List<String> eventIds, String unitId);

    @Query("select e from EventEnrollMain e where e.formAutoAddNum in ?1")
    List<EventEnrollMain> GetEnrollsByFormAutoAddNums(List<String> formAutoAddNums);

    @Query("select e from EventEnrollMain e where e.eventId in ?1 and (?2 = true or e.isWaitForCtSync = 1)")
    List<EventEnrollMain> GetWaitForSync(List<String> eventIds, boolean isAll);
}
