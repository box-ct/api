/*
* Auto Generated
* Based on name: event_unit_vlntr_info
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.EventUnitVlntrInfo;

public interface EventUnitVlntrInfoRepo extends JpaRepository<EventUnitVlntrInfo, Long> {
}
