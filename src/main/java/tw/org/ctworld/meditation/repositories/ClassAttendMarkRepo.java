package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord007Object;
import tw.org.ctworld.meditation.models.ClassAttendMark;

import java.util.List;

public interface ClassAttendMarkRepo extends JpaRepository<ClassAttendMark, Long> {

    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord007Object(a.attendMark, a.isFullAttended, " +
            "a.isGraduated, a.isDiligentAward, a.isMakeupClass) " +
            "from ClassAttendMark a where a.classId = ?1 and a.unitId = ?2")
    List<ClassRecord007Object> GetClassAttendMarkList(String classId, String unitId);

    @Query("select a from ClassAttendMark a where a.classId = ?1 and a.attendMark = ?2 and a.unitId = ?3")
    ClassAttendMark GetClassAttendMark(String classId, String attendMark, String unitId);

    @Query("select a.attendMark from ClassAttendMark a where a.classId = ?1 and a.isGraduated = 1")
    List<String> GetIsGraduatedList(String classId);

    @Query("select a.attendMark from ClassAttendMark a where a.classId = ?1 and a.isMakeupClass = 1")
    List<String> GetIsMakeupClassList(String classId);

    @Query("select a.attendMark from ClassAttendMark a where a.classId = ?1 and a.isFullAttended = 1")
    List<String> GetFullAttendMarkByClassId(String classId);


    @Query("select a from ClassAttendMark a where a.classId in ?1 and a.isGraduated = 1")
    List<ClassAttendMark> GetIsGraduatedListByClassIds(List<String> classIds);
}
