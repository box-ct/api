/*
* Auto Generated
* Based on name: unit_master_info
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort;
import tw.org.ctworld.meditation.beans.EventEnrollMember.ExcelMember;
import tw.org.ctworld.meditation.beans.UnitMaster.MasterInfo;
import tw.org.ctworld.meditation.beans.UnitMaster.UnitMaster001Object;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp005Object;
import tw.org.ctworld.meditation.models.UnitMasterInfo;
import tw.org.ctworld.meditation.models.CtMasterInfo;

import javax.transaction.Transactional;
import java.util.List;

public interface UnitMasterInfoRepo extends JpaRepository<UnitMasterInfo, Long> {

    @Query("select  m " +
            " from CtMasterInfo m where m.masterId=case when ?1<>''  then ?1 else m.masterId end " +
            " and m.masterName=case when ?2<>'' then ?2 else m.masterName end order by m.masterId desc ")
    //            " and (select count (un) from UnitMasterInfo un where m.masterId=un.masterId and un.unitId=?3)=case when ?1<>'' then 0 else -1 end"+
//            " and (select count (un) from UnitMasterInfo un where m.masterName=un.masterName and un.unitId=?3)=case when ?1<>'' then 0 else -1 end")
    List<CtMasterInfo> GetCtMasterByCond(String masterId,String masterName,String unitid);

    @Query("select m " +
            "from UnitMasterInfo m where (?1 is null or m.unitId = ?1)")
    List<UnitMasterInfo> GetUnitMasterByUnitId(String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort(" +
            "m.isAbbot, false, m.isTreasurer, m.jobOrder, m.jobTitle, m.masterId, m.masterName, m.masterPreceptTypeName, " +
            "m.mobileNum, m.preceptOrder, m.unitId, m.unitName, m.updateDtTm, m.updatorName) " +
            "from UnitMasterInfo m where (?1 is null or m.unitId = ?1) " +
            "and (?2 is null or upper(m.masterId) like ?2%) " +
            "and (?3 is null or m.masterName like %?3% or m.masterId like %?3%) order by m.unitId, m.jobOrder, m.preceptOrder")
    List<MasterInfoShort> GetUnitMasterShort(String unitId, String masterIdPrefix, String keyword);

//    @Query("select new tw.org.ctworld.meditation.beans.UnitMaster.UnitMaster001Object(m) " +
//            "from UnitMasterInfo m where m.masterName like ?1% ")
//    UnitMaster001Object GetUnitMasterByAliasname(String aliasName);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from UnitMasterInfo e where e.unitId = ?1")
    void DeleteUnitMastersByUnitId(String unitId);

//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("update ClassUnitInfo u set u.certUnitEngName=?2,u.unitPhoneNum=?3,u.unitFaxNum=?4,u.unitPhoneNumCode=?5,u.unitMobileNum=?6,u.unitEmail=?7,u.unitMailingAddress=?8, " +
//            "u.stateName=?9,u.abbotEngName=?10  where u.unitId = ?1")
//    void UpdateUnitInfo(String unitid,String certUnitEngName,String unitPhoneNum,String unitFaxNum,String unitPhoneNumCode,String unitMobileNum,String unitEmail,String unitMailingAddress,
//                        String stateName,String abbotEngName);

    //models及資料庫尚未更新
//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("update UnitInfo u set u.certUnitEngName=?2, " +
//            "u.stateName=?3,u.abbotEngName=?4  where u.unitId = ?1")
//    void UpdateUnitInfo(String unitid,String certUnitEngName,
//                        String stateName,String abbotEngName);

    @Query("select new tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp005Object(" +
            "m.masterId, m.masterName, m.jobTitle, m.isAbbot, m.isTreasurer, m.mobileNum) " +
            "from UnitMasterInfo m where m.unitId = ?1 ")
    List<UnitStartUp005Object> GetUnitMasterInfoListByUnitId(String unitId);

    @Query("select m from UnitMasterInfo m where m.unitId = ?1 and m.masterId = ?2")
    UnitMasterInfo GetUnitMasterInfoByUnitIdAndMasterId(String unitId, String masterId);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.ExcelMember(" +
            "m.masterId, m.masterName) " +
            "from UnitMasterInfo m where (?1 is null or m.unitId = ?1)")
    List<ExcelMember> GetAllMasterInfoByUnitId(String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort(" +
            "m.isAbbot, false, m.isTreasurer, m.jobOrder, m.jobTitle, m.masterId, m.masterName, m.masterPreceptTypeName, " +
            "m.mobileNum, m.preceptOrder, m.unitId, m.unitName, m.updateDtTm, m.updatorName) " +
            "from UnitMasterInfo m where m.unitId = ?1 and m.masterId in ?2 " +
            "order by m.jobOrder, m.preceptOrder")
    List<MasterInfoShort> GetAllMasterInfoByMasterIds(String unit, List<String> masterIds);


    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort(" +
            "m.isAbbot, false, m.isTreasurer, m.jobOrder, m.jobTitle, m.masterId, m.masterName, m.masterPreceptTypeName, " +
            "m.mobileNum, m.preceptOrder, m.unitId, m.unitName, m.updateDtTm, m.updatorName) " +
            "from UnitMasterInfo m where m.unitId = ?1 and m.masterId = ?2 " +
            "order by m.jobOrder, m.preceptOrder")
    List<MasterInfoShort> GetUnitMasterByMasterId(String initId, String masterId);

    @Query("select m from UnitMasterInfo m where m.unitId = ?1 and m.masterId = ?2 " +
            "order by m.jobOrder, m.preceptOrder")
    List<UnitMasterInfo> GetByMasterId(String initId, String masterId);

//    @Query("select m from UnitMasterInfo m where m.masterId in ?1")
//    List<UnitMasterInfo> GetAllMasterInfoByMasterIds(List<String> masterIds);
}
