package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.Printer.Printer007Object;
import tw.org.ctworld.meditation.models.ClassCertFieldSetting;

import java.util.List;

public interface ClassCertFieldSettingRepo extends JpaRepository<ClassCertFieldSetting, Long> {

    @Query("select c from ClassCertFieldSetting c where c.certTemplateId in ?1 order by c.id")
    List<ClassCertFieldSetting> GetCertTemplateIdListByCertTemplateId(List<String> certTemplateIdList);

    @Query("select new tw.org.ctworld.meditation.beans.Printer.Printer007Object(" +
            "c.id, c.certTypeName, c.fieldName, c.fieldPositionNum, c.fontName, c.fontSize, c.isBold, c.isItalic, " +
            "c.horizontalAlignType, c.verticalAlignType, c.textDirection, c.width, c.height, c.yDistance, c.xDistance, " +
            "c.isEnabled) " +
            "from ClassCertFieldSetting c where c.certTemplateId = ?1 order by c.id")
    List<Printer007Object> GetCertTemplateIdListByCertTemplateId2(String certTemplateId);
}
