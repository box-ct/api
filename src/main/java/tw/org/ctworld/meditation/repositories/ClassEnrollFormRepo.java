package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup001Object;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup002Object;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord000Object;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord003_1Object;
import tw.org.ctworld.meditation.beans.ClassStats.*;
import tw.org.ctworld.meditation.beans.ClassUpgrade.ClassUpgrade000Object;
import tw.org.ctworld.meditation.beans.ClassUpgrade.ClassUpgrade003Object;
import tw.org.ctworld.meditation.beans.Classes.Classes006Object;
import tw.org.ctworld.meditation.beans.Classes.Classes007Object;
import tw.org.ctworld.meditation.beans.Classes.Classes009Response;
import tw.org.ctworld.meditation.beans.Common.Common012_2Object;
import tw.org.ctworld.meditation.beans.DataReport.DataReport001Object;
import tw.org.ctworld.meditation.beans.DataReport.DataReport003Object;
import tw.org.ctworld.meditation.beans.DataReport.DataReport005Object;
import tw.org.ctworld.meditation.beans.EventEnrollMember.ClassEnrollFormShort;
import tw.org.ctworld.meditation.beans.Member.*;
import tw.org.ctworld.meditation.beans.DataSearch.DataSearchClassEnrollForm;
import tw.org.ctworld.meditation.beans.DataSearch.DataSearchHistoryClass;
import tw.org.ctworld.meditation.beans.MemberImport.MemberEnrollInfo;
import tw.org.ctworld.meditation.beans.MemberInfos.MemberInfos001_1Object;
import tw.org.ctworld.meditation.beans.MemberInfos.SearchMemberInfoEnrollForm;
import tw.org.ctworld.meditation.models.ClassEnrollForm;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface ClassEnrollFormRepo extends JpaRepository<ClassEnrollForm, Long> {

    @Query("select e from ClassEnrollForm e " +
            "where e.classId in ?1 and e.classPeriodNum = ?2 and e.className = ?3 and e.classGroupId = ?4 and e.unitId = ?5 " +
            "order by e.classPeriodNum desc ,e.classTypeNum asc ,e.gender desc ")
    List<ClassEnrollForm> GetClassEnrollFormListByRequestParam(
            List<String> classIdList, String classPeriodNum, String className, String classGroupId, String unitId);

    @Query("select e from ClassEnrollForm e " +
            "where e.classId in ?1 and e.classPeriodNum = ?2 and e.className = ?3 and e.unitId = ?4 " +
            "order by e.classPeriodNum desc ,e.classTypeNum asc ,e.gender desc ")
    List<ClassEnrollForm> GetClassEnrollFormListByRequestParam(
            List<String> classIdList, String classPeriodNum, String className, String unitId);

    @Query("select e from ClassEnrollForm e " +
            "where e.classId in ?1 and e.classPeriodNum like %?2% and e.className like %?3% and e.classGroupId like %?4% and e.unitId like %?5% and e.aliasName like %?6% " +
            "order by e.classPeriodNum desc ,e.classTypeNum asc ,e.gender desc ")
    List<ClassEnrollForm> GetClassEnrollFormListByRequestParam(
            List<String> classIdList, String classPeriodNum, String className, String classGroupId, String unitId, String aliasName);

    @Query("select e from ClassEnrollForm e " +
            "where e.unitId = ?1 and e.aliasName like %?2% " +
            "order by e.classPeriodNum desc ,e.classTypeNum asc ,e.gender desc ")
    List<ClassEnrollForm> GetClassEnrollFormListByRequestParam(String unitId, String aliasName);

    @Query("select new tw.org.ctworld.meditation.beans.Member.Members001Object(" +
            "e.memberId, e.gender, e.aliasName, e.ctDharmaName, e.classPeriodNum, e.className, e.age, e.classGroupId, " +
            "e.homePhoneNum1, e.mobileNum1, e.dsaJobNameList, e.isDroppedClass) " +
            "from ClassEnrollForm e " +
            "where e.unitId = ?1 " +
            "order by e.classPeriodNum desc ,e.classTypeNum asc ,e.gender desc ")
    List<Members001Object> GetClassEnrollFormListByRequestParam(String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.Member.Members001Object(" +
            "e.memberId, e.gender, e.aliasName, e.ctDharmaName, e.classPeriodNum, e.className, e.age, e.classGroupId, " +
            "e.homePhoneNum1, e.mobileNum1, e.dsaJobNameList, e.isDroppedClass) " +
            "from ClassEnrollForm e " +
            "where e.unitId = ?1 and  e.classId in ?2 " +
            "order by e.classPeriodNum desc ,e.classTypeNum asc ,e.gender desc ")
    List<Members001Object> GetClassEnrollFormListByRequestParam(String unitId, List<String> classIdList);

    @Query("select distinct e.classGroupId from ClassEnrollForm e where e.classId = ?1 order by cast(e.classGroupId as int)")
    List<String> GetClassGroupComboBox(String classId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassEnrollForm e where e.classId = ?1")
    void DeleteClassEnrollFormByClassId(String classId);

    @Query("select e from ClassEnrollForm e where e.memberId = ?1 and e.unitId = ?2 order by e.id asc ")
    List<ClassEnrollForm> GetClassEnrollFormListByMemberIdAndUnitId(String memberId, String unitId);

    @Query("select e.classEnrollFormId from ClassEnrollForm e where e.unitId = ?1 and e.memberId = ?2 order by e.id asc ")
    List<String> GetClassEnrollFormIdByUnitIdAndMemberId(String unitId, String memberId);

//    @Query("select e.classId from ClassEnrollForm e where e.memberId = ?1 and e.unitId = ?2")
//    List<String> GetClassEnrollFormClassIdByMemberIdAndUnitId(String memberId, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.Member.Members002MeditationRecordObject(e.memberId, e.classPeriodNum, e.className, e.gender, e.classGroupId, e.aliasName, e.classJobTitleList, false ,e.isDroppedClass, e.isFullAttended, e.isGraduated, e.isNotComeBack, e.isEnrollToNewUpperClass, e.nextNewClassName, i.classStatus, e.unitName) " +
            "from ClassEnrollForm e " +
            "inner join ClassInfo i on e.classId = i.classId " +
            "where e.memberId = ?1 and (?2 is null or e.unitId = ?2) " +
            "order by " +
            "case when i.classStatus = '開課中' then 1 else " +
            "case when i.classStatus = '可報名' then 2 else " +
            "case when i.classStatus = '待結業' then 3 else " +
            "case when i.classStatus = '已結業' then 4 else 9 end end end end asc, " +
            "e.classTypeNum asc, e.classPeriodNum desc ")
    List<Members002MeditationRecordObject> GetClassCtMemberInfoMeditationRecord1(String memberId, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.Member.Members002RegistrationRecordObject(e.memberId, e.aliasName, e.ctDharmaName, e.classPeriodNum, e.className, e.classGroupId, e.memberGroupNum, a.classDate, a.attendCheckinDtTm, a.classWeeksNum, a.attendMark) " +
            "from ClassEnrollForm e " +
            "inner join ClassAttendRecord a on e.classEnrollFormId = a.classEnrollFormId " +
            "inner join ClassInfo i on e.classId = i.classId " +
            "where e.memberId = ?1 and (?2 is null or e.unitId = ?2) and i.classStatus = ?3 order by e.classTypeNum asc , a.classWeeksNum asc ")
    List<Members002RegistrationRecordObject> GetClassEventEnrollMainRegistrationRecord(String memberId, String unitId, String classStatus);

    @Query("select e from ClassEnrollForm e where e.memberId = ?1 and e.classId = ?2 and e.unitId = ?3")
    List<ClassEnrollForm> GetClassRegisterStatus(String memberId, String classId, String unitId);

    @Query("select max(e.classEnrollFormId) from ClassEnrollForm e where e.classEnrollFormId like ?1")
    String GetMaxAssetId(String classEnrollFormId);

    @Query("select max(cast(e.memberGroupNum as int)) from ClassEnrollForm e where e.classId = ?1 and  e.classGroupId = ?2")
    String GetMaxMemberGroupNum(String classId, String classGroupId);

    @Query("select e.memberGroupNum from ClassEnrollForm e where e.classId = ?1 and  e.classGroupId = ?2 order by cast(e.memberGroupNum as int) asc")
    List<String> GetMemberGroupNumList(String classId, String classGroupId);

    @Query("select e from ClassEnrollForm e where e.classEnrollFormId = ?1")
    ClassEnrollForm GetClassEnrollFormByClassEnrollFormId(String classEnrollFormId);

    @Query("select new tw.org.ctworld.meditation.beans.Member.Members004Object(i.className, i.dayOfWeek," +
            " i.classDayOrNight, e.aliasName, e.classJobTitleList, e.ctDharmaName, e.gender, e.classGroupId, e.memberId, i.classTypeName, i.unitName) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId where e.classEnrollFormId = ?1")
    Members004Object GetCardInfoByMemberId(String classEnrollFormId);

    @Query("select new tw.org.ctworld.meditation.beans.Classes.Classes006Object(e.classEnrollFormId, e.classId, " +
            "e.memberId, e.gender, e.aliasName, e.ctDharmaName, e.classPeriodNum, e.className, e.classGroupId, " +
            "e.memberGroupNum, e.classJobTitleList, e.isNotComeBack, e.isCompleteNextNewClassEnroll, " +
            "e.isEnrollToNewUpperClass, e.isFullAttended, e.isGraduated, e.previousClassName, e.previousClassGroupName, " +
            "e.nextNewClassName, e.nextNewClassNote, e.age, e.mobileNum1, true ) " +
            "from ClassEnrollForm e where e.classId = ?1 and (e.isDroppedClass = ?2 or (?2 = false and e.isDroppedClass is null)) and e.unitId = ?3")
    List<Classes006Object> GetClassRegisterList(String classId, boolean isDroppedClass, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.Classes.Classes007Object(e.classEnrollFormId, e.memberId, " +
            "e.gender, e.aliasName, e.ctDharmaName, e.classGroupId, e.memberGroupNum, e.classJobTitleList, " +
            "e.classJobContentList, e.isLeadGroup, e.age, e.mobileNum1) " +
            "from ClassEnrollForm e where e.classId = ?1 and e.memberGroupNum in ?2 " +
            "order by e.gender desc , cast(e.classGroupId as int) asc , cast(e.memberGroupNum as int) asc ")
    List<Classes007Object> GetClassLeaderListByClassId(String classId, List<String> memberGroupNumList);

    @Query("select new tw.org.ctworld.meditation.beans.Classes.Classes007Object(e.classEnrollFormId, e.memberId, " +
            "e.gender, e.aliasName, e.ctDharmaName, e.classGroupId, e.memberGroupNum, e.classJobTitleList, " +
            "e.classJobContentList, e.isLeadGroup, e.age, e.mobileNum1) " +
            "from ClassEnrollForm e where e.classId = ?1 " +
            "order by e.gender desc , cast(e.classGroupId as int) asc , cast(e.memberGroupNum as int) asc ")
    List<Classes007Object> GetClassLeaderListByClassId(String classId);

    // 本班註記
    @Query("select new tw.org.ctworld.meditation.beans.Member.MembersObject(e.memberId, e.gender, e.aliasName, e.ctDharmaName, e.classPeriodNum, e.className, e.classGroupId, e.memberGroupNum, e.isHomeClass, e.homeClassNote, e.classId)" +
            " from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId where i.unitId = ?1 and i.classStatus = ?2" +
            " and e.memberId in (select de.memberId from ClassEnrollForm de inner join ClassInfo di on de.classId = di.classId where di.unitId = ?1 and di.classStatus = ?2 group by de.memberId having count(de.memberId) > 1)" +
            " order by e.gender desc , e.classPeriodNum, e.classTypeNum, e.classGroupId, e.memberGroupNum")
    List<MembersObject> GetSpanClassMemberIdsByClassStatusUnitId(String unitId, String classStatus);

    // 本班註記
    @Query("select e from ClassEnrollForm e where e.memberId = ?1 and e.classPeriodNum = ?2")
    List<ClassEnrollForm> GetSpanClassByMemberIdClassPeriodNumClassName(String memberId, String classPeriodNum);

    // 資料查詢 - 證件檢核
    @Query("select new tw.org.ctworld.meditation.beans.Member.MembersObject(e.memberId, e.classPeriodNum, e.className, e.classGroupId, e.memberGroupNum, e.gender, e.aliasName, e.ctDharmaName, e.isDroppedClass, e.twIdNum, e.currentClassIntroducerName, e.currentClassIntroducerRelationship, e.age)" +
            " from ClassEnrollForm e where e.unitId = ?1 and e.classId in ?2 order by e.classPeriodNum, e.classTypeNum, substr(e.className, 0, 1), e.gender, e.memberId")
    List<MembersObject> GetUniqueMemberEnrollFormByUnitIdClassIds(String unitId, List<String> classIds);

    // 資料查詢 - 證件檢核 - 新/舊生
    @Query("select f.memberId from ClassEnrollForm f where f.unitId = ?1 and f.classId in ?2 and f.memberId in ?3")
    List<String> GetMemberEnrollFormByUnitIdClassIdsMemberIds(String unitId, List<String> classIds, List<String> memberIds);

    // DataSearch Class/Group Sub-total
    @Query("select new tw.org.ctworld.meditation.beans.DataSearch.DataSearchClassEnrollForm(e.classId, e.gender, e.classGroupId, e.classJobTitleList) from ClassEnrollForm e where e.classId in ?1 and e.isDroppedClass = 0 order by e.classId")
    List<DataSearchClassEnrollForm> GetClassEnrollFormByClassIds(List<String> classIds);

    // DataSearch period/class member count
    @Query("Select new tw.org.ctworld.meditation.beans.DataSearch.DataSearchHistoryClass(e.className, count(e)) from ClassEnrollForm e where e.unitId = ?1 and e.classPeriodNum = ?2 group by e.classId, e.className")
    List<DataSearchHistoryClass> GetClassMemberCountByUnitPeriod(String unitId, String period);

    // DataSearch get leaders
    @Query("Select new tw.org.ctworld.meditation.beans.Member.MembersObject(e.classPeriodNum, e.className, e.classGroupId, e.memberGroupNum, e.gender, e.aliasName, e.ctDharmaName, e.classJobTitleList, e.homePhoneNum1, e.mobileNum1, e.asistantMasterName, e.dayOfWeek)" +
            " from ClassEnrollForm e where e.unitId = ?1 and e.classId in ?2 and e.classJobTitleList like '%學員長%'" +
            " and (?3 is null or e.classPeriodNum = ?3)" +
            " and (?4 is null or e.className = ?4)" +
            " and (?5 is null or e.classGroupId = ?5)" +
            " order by e.classJobHighestNum, e.gender, e.memberGroupNum")
    List<MembersObject> GetLeaders(String unitId, List<String> classIds, String classPeriodNum, String className, String classGroupId);

    // DataSearch Span Count
    @Query("select new tw.org.ctworld.meditation.beans.Member.MembersObject(e.memberId, e.gender, e.classJobTitleList)" +
            " from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId where i.unitId = ?1 and i.classStatus = ?2" +
            " and e.memberId in (select de.memberId from ClassEnrollForm de inner join ClassInfo di on de.classId = di.classId where di.unitId = ?1 and di.classStatus = ?2 group by de.memberId having count(de.memberId) = ?3)" +
            " order by e.gender, e.memberId, e.classPeriodNum, e.classTypeNum, e.classGroupId")
    List<MembersObject> GetSpanClassMemberList(String unitId, String classStatus, Long count);

    @Query("Select new tw.org.ctworld.meditation.beans.Member.Members006Object(e.classId, e.className, e.classGroupId) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.memberId = ?1 and i.classStatus = ?2 and e.unitId = ?3")
    List<Members006Object> GetMemberClassName(String memberId, String classStatus, String unitId);

    @Query("select e from ClassEnrollForm e where e.memberId = ?1 and e.classId = ?2")
    ClassEnrollForm GetClassEnrollFormByMemberIdAndClassId(String memberId, String classId);

//    @Query("select new tw. from ClassEnrollForm e where e.memberId = ?1 and e.classId = ?2")
//    ClassEnrollForm GetClassEnrollFormByMemberIdAndClassId(String memberId, String classId);

    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord003_1Object(e.memberId, e.aliasName, " +
            "e.ctDharmaName, e.gender, e.className, e.classGroupId, e.memberGroupNum, e.classJobTitleList, " +
            "e.isDroppedClass, e.changeAndWaitPrintMark, e.classDroppedDate, e.userDefinedLabel1, e.userDefinedLabel2, " +
            "e.newReturnChangeMark, e.newReturnChangeDate, e.classEnrollFormNote) " +
            "from ClassEnrollForm e " +
            "where e.classId = ?1 " +
            "order by cast(e.memberGroupNum as int) asc, e.classJobHighestNum asc, e.gender desc, cast(e.classGroupId as int) asc")
    List<ClassRecord003_1Object> GetClassEnrollFormAndAttendRecordByClassId(String classId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassEnrollForm e where e.memberId = ?1 and e.classId = ?2")
    void DeleteClassEnrollFormByMemberIdAndClassId(String memberId, String classId);

    @Query("select e from ClassEnrollForm e where e.classId = ?1 and e.aliasName = ?2 and e.twIdNum = ?3")
    List<ClassEnrollForm> GetEnrollFormByNameTwIdClassId(String classId, String name, String twIdNum);

    @Query("select e from ClassEnrollForm e where e.classId = ?1 and e.aliasName = ?2 and e.birthDate = ?3")
    List<ClassEnrollForm> GetEnrollFormByNameBirthdayClassId(String classId, String name, Date birthday);

    @Query("select new tw.org.ctworld.meditation.beans.ClassStats.ClassStats003_1Object(" +
            "e.classEnrollFormId, e.memberId, e.aliasName, e.ctDharmaName, e.gender, e.classPeriodNum, e.className, " +
            "e.classGroupId, e.memberGroupNum, e.classJobTitleList, e.isFullAttended, e.isGraduated, " +
            "e.classAttendedResult, e.age) " +
            "from ClassEnrollForm e where e.classId = ?1 and e.unitId = ?2 and e.isDroppedClass = 0" +
            "order by cast(e.memberGroupNum as int) asc, e.classJobHighestNum asc, e.gender desc, cast(e.classGroupId as int) asc ")
    List<ClassStats003_1Object> GetEnrollFormByClassIdAndUnitId(String classId, String unitId);

    // 留原班
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassEnrollForm e set e.isStayInOriginalClass = 1 where e.classId = ?1 and e.memberId = ?2")
    void UpdateIsStayInOriginalClass(@Param("classId") String classId, @Param("memberId") String memberId);

    // 留原班
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassEnrollForm e set e.isStayInOriginalClass = 1 where e.classId = ?1 and e.memberId in ?2")
    void UpdateIsStayInOriginalClass(@Param("classId") String classId, @Param("memberIds") List<String> memberId);

    // 最後是否全勤
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassEnrollForm e set e.isFullAttended = 1 where e.classId = ?1 and e.memberId = ?2")
    void UpdateIsFullAttended(@Param("classId") String classId, @Param("memberId") String memberId);

    // 最後是否結業
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassEnrollForm e set e.isGraduated = 1 where e.classId = ?1 and e.memberId = ?2")
    void UpdateIsGraduated(@Param("classId") String classId, @Param("memberId") String memberId);

    // 最後是否結業
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassEnrollForm e set e.isGraduated = 1 where e.classId = ?1 and e.memberId in ?2")
    void UpdateIsGraduated(@Param("classId") String classId, @Param("memberIds") List<String> memberId);

    // 補後全勤
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassEnrollForm e set e.isMakeupToFullAttended = 1 where e.classId = ?1 and e.memberId = ?2")
    void UpdateIsMakeupToFullAttended(@Param("classId") String classId, @Param("memberId") String memberId);

    // 精進獎
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassEnrollForm e set e.isDiligentAward = 1 where e.classId = ?1 and e.memberId = ?2")
    void UpdateIsDiligentAward(@Param("classId") String classId, @Param("memberId") String memberId);

    @Query("select new tw.org.ctworld.meditation.beans.ClassStats.ClassStats005Object(e.unitId, e.unitName, e.gender, " +
            "e.aliasName, e.ctDharmaName, e.age, i.classDayOrNight, e.classTypeName, e.classPeriodNum, e.className, " +
            "e.classGroupId, e.memberGroupNum, e.dayOfWeek, e.classDesc, e.classFullName, i.certificateName, " +
            "e.isFullAttended, e.isGraduated) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.classEnrollFormId in ?1 order by e.gender desc, cast(e.classGroupId as int) asc, cast(e.memberGroupNum as int) asc ")
    List<ClassStats005Object> GetExcelCertListByIds(List<String> ids);

    @Query("select new tw.org.ctworld.meditation.beans.ClassStats.ClassStats007Object(e.unitId, e.unitName, e.memberId, " +
            "e.gender, e.aliasName, e.ctDharmaName, c.nameTail, c.twIdNum, c.birthDate, c.birthCity, e.address, " +
            "e.homePhoneNum1, e.mobileNum1, e.schoolNameAndMajor, e.companyNameAndJobTitle, e.classPeriodNum, e.className, " +
            "e.classGroupId, e.memberGroupNum, e.classStartDate, e.classEndDate, e.dayOfWeek, e.isFullAttended, " +
            "e.isMakeupToFullAttended, e.isGraduated, e.isDiligentAward, c.skillList, c.healthList, " +
            "e.currentClassIntroducerName, c.urgentContactPersonName1, c.urgentContactPersonPhoneNum1, c.dataUnitMemberId, " +
            "c.okSendMessage, c.okSendMagazine, e.classJobTitleList, e.isLeadGroup) " +
            "from ClassEnrollForm e inner join CtMemberInfo c on e.memberId = c.memberId " +
            "where e.classId = ?1")
    List<ClassStats007Object> GetExcelDataListByClassId(String classId);

    @Query("select new tw.org.ctworld.meditation.beans.ClassStats.ClassStats008_1Object(e.memberId, e.classPeriodNum, " +
            "e.className, e.gender, e.aliasName, e.ctDharmaName, e.classGroupId, e.memberGroupNum, e.isFullAttended, " +
            "e.classAttendedResult) " +
            "from ClassEnrollForm e where e.classId = ?1")
    List<ClassStats008_1Object> GetExcelMakeUpClassesListByClassId(String classId);

    // class - report
    @Query("select e from ClassEnrollForm e where e.classId = ?1 order by e.classJobHighestNum desc, e.gender desc, e.classGroupId asc, cast(e.memberGroupNum as int) asc")
    List<ClassEnrollForm> GetClassEnrollFormByClassId(String classId);

    @Query("select new tw.org.ctworld.meditation.models.ClassEnrollForm(e.classEnrollFormId, e.memberId, e.gender, " +
            "e.aliasName, e.ctDharmaName, e.classId, e.classPeriodNum, e.className, e.classGroupId) " +
            "from ClassEnrollForm e where e.classId = ?1")
    List<ClassEnrollForm> GetClassEnrollFormListByClassId(String classId);

    // class - report
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update ClassEnrollForm f set f.changeAndWaitPrintMark = ?3 where f.classId = ?1 and f.memberId in ?2")
    void SetPrintMark(String classId, List<String> memberIds, boolean onOff);

    @Query("select new tw.org.ctworld.meditation.beans.ClassUpgrade.ClassUpgrade003Object(e.classEnrollFormId, " +
            "e.memberId, e.gender, e.aliasName, e.ctDharmaName, e.classPeriodNum, e.className, e.classGroupId, " +
            "e.memberGroupNum, e.classJobTitleList, e.isNotComeBack, e.isCompleteNextNewClassEnroll, " +
            "e.isEnrollToNewUpperClass, e.isFullAttended, e.isGraduated, e.previousClassName, e.previousClassGroupName, " +
            "e.nextNewClassName, e.nextNewClassNote, e.classJobContentList, e.isLeadGroup) " +
            "from ClassEnrollForm e where e.classId = ?1 and e.isDroppedClass = 0 and e.unitId = ?2 " +
            "order by cast(e.memberGroupNum as int) asc, e.gender desc, cast(e.classGroupId as int) asc ")
    List<ClassUpgrade003Object> GetClassEnrollFormListByClassId(String classId, String unitId);

    @Query("select e from ClassEnrollForm e where e.memberId = ?1 and e.nextNewClassId = ?2")
    ClassEnrollForm GetPrevClassEnrollFormByMemberIdAndClassId(String memberId, String classId);

    @Query("select e from ClassEnrollForm e where e.memberId in ?1 and e.classId = ?2")
    List<ClassEnrollForm> GetClassEnrollFormByMemberIdsAndClassId(List<String> memberIds, String classId);

    @Query("select false from ClassEnrollForm e where e.memberId = ?1")
    List<Boolean> GetIsExecuteClassAttendRecord(String memberId);

    @Query("select false from ClassEnrollForm e where e.memberId = ?1")
    List<Boolean> GetIsExecuteClassGraduateStats(String memberId);

    @Query("select false from ClassEnrollForm e where e.memberId = ?1")
    List<Boolean> GetIsExecuteClassUpGrade(String memberId);

    @Query("select distinct e.year from ClassEnrollForm e")
    List<String> GetYearList();

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport001Object(e.unitName, e.year, " +
            "e.classPeriodNum, e.className, e.classId, e.gender, e.isDroppedClass, e.isGraduated, i.classDayOrNight, " +
            "i.classTypeName) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.year in ?1 and i.classStatus = '開課中' " +
            "order by e.classPeriodNum desc, e.classTypeNum asc, i.classDayOrNight desc ")
    List<DataReport001Object> GetDataReportByYear1(List<String> year);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport001Object(e.unitName, e.year, " +
            "e.classPeriodNum, e.className, e.classId, e.gender, e.isDroppedClass, e.isGraduated, i.classDayOrNight, " +
            "i.classTypeName) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.year in ?1 and i.classStatus = '開課中' and e.classTypeNum in (1,2,3,4,5) " +
            "order by e.classPeriodNum desc, e.classTypeNum asc, i.classDayOrNight desc ")
    List<DataReport001Object> GetDataReportByYear2(List<String> year);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport001Object(e.unitName, e.year, " +
            "e.classPeriodNum, e.className, e.classId, e.gender, e.isDroppedClass, e.isGraduated, i.classDayOrNight, " +
            "i.classTypeName) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.year in ?1 and i.classStatus in ('開課中','可報名','已結業') " +
            "order by e.classPeriodNum desc, e.classTypeNum asc, i.classDayOrNight desc ")
    List<DataReport001Object> GetDataReportByYear3(List<String> year);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport001Object(e.unitName, e.year, " +
            "e.classPeriodNum, e.className, e.classId, e.gender, e.isDroppedClass, e.isGraduated, i.classDayOrNight, " +
            "i.classTypeName) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.year in ?1 and i.classStatus in ('開課中','可報名','已結業') and e.classTypeNum in (1,2,3,4,5) " +
            "order by e.classPeriodNum desc, e.classTypeNum asc, i.classDayOrNight desc ")
    List<DataReport001Object> GetDataReportByYear4(List<String> year);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport003Object(e.unitName, e.classId, " +
            "e.className, e.classTypeName, e.memberId, a.attendMark, e.isDroppedClass) " +
            "from ClassEnrollForm e inner join ClassAttendRecord a on e.classEnrollFormId = a.classEnrollFormId " +
            "where a.classDate > ?1 and a.classDate <= ?2")
    List<DataReport003Object> getRealTimeCount(Date startDate, Date endDate);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport005Object(e.unitName, e.classId, " +
            "e.classPeriodNum, e.className, e.classTypeName, i.classDayOrNight, a.attendMark, e.isDroppedClass, " +
            "a.classDate, e.classTypeNum) " +
            "from ClassEnrollForm e inner join ClassAttendRecord a on e.classEnrollFormId = a.classEnrollFormId " +
            "inner join ClassInfo i on e.classId = i.classId " +
            "where a.classDate > ?1 and a.classDate <= ?2 and e.isDroppedClass = 0 and a.attendMark = 'V' " +
//            "where a.classDate > ?1 and a.classDate <= ?2 and e.isDroppedClass = 0 " +
            "order by e.classPeriodNum desc, e.classTypeNum asc, i.classDayOrNight desc")
    List<DataReport005Object> getCheckinCountByRange(Date startDate, Date endDate);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport005Object(e.unitName, e.classId, " +
            "e.classPeriodNum, e.className, a.attendMark, a.classDate, e.classTypeNum) " +
            "from ClassEnrollForm e inner join ClassAttendRecord a on e.classEnrollFormId = a.classEnrollFormId " +
            "inner join ClassInfo i on e.classId = i.classId " +
            "where a.classDate > ?1 and a.classDate <= ?2 and e.isDroppedClass = 0 and a.attendMark = 'V' " +
            "order by e.classPeriodNum desc, e.classTypeNum asc, i.classDayOrNight desc")
    List<DataReport005Object> GetCheckinCountByRange(Date startDate, Date endDate);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport005Object(e.unitName, e.classId, " +
            "e.classPeriodNum, e.className, e.classTypeNum, i.classDayOrNight, e.classEnrollFormId, e.isDroppedClass) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.classId in ?1 order by e.classPeriodNum desc, e.classTypeNum asc, i.classDayOrNight desc")
    List<DataReport005Object> GetClassEnrollFormListByClassIdList(List<String> classId);

    @Query("select new tw.org.ctworld.meditation.models.ClassEnrollForm(e.classId, e.className, e.dayOfWeek) " +
            "from ClassEnrollForm e where e.classId in ?1 order by e.classPeriodNum desc, e.classTypeNum asc")
    List<ClassEnrollForm> GetClassEnrollFormListByClassIdList2(List<String> classId);

    @Query("select e.classId " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.isDroppedClass = 0 and e.classId in ?1 " +
            "order by e.classPeriodNum desc, e.classTypeNum asc, i.classDayOrNight desc")
    List<String> GetClassEnrollFormClassIdListByClassIdList(List<String> classIdList);

    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord000Object(e.classId, i.classStatus, " +
            "e.year, e.classPeriodNum, e.className, e.memberId, e.classJobTitleList) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.memberId = ?1")
    List<ClassRecord000Object> GetClassRecordList(String memberId);

    @Query("select new tw.org.ctworld.meditation.beans.ClassStats.ClassStats000Object(e.classId, i.classStatus, " +
            "e.year, e.classPeriodNum, e.className, e.memberId, e.classJobTitleList, e.unitId) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.memberId = ?1")
    List<ClassStats000Object> getClassStatsList(String memberId);

    @Query("select new tw.org.ctworld.meditation.beans.ClassUpgrade.ClassUpgrade000Object(e.classId, i.classStatus, " +
            "e.year, e.classPeriodNum, e.className, e.memberId, e.classJobTitleList, e.unitId) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.memberId = ?1")
    List<ClassUpgrade000Object> getClassUpgradeList(String memberId);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.MemberInfos001_1Object(e.memberId, e.className) " +
            "from ClassEnrollForm e where e.memberId in ?1 order by e.classPeriodNum")
    List<MemberInfos001_1Object> getMemberClassListByMemberIdList(List<String> memberIdList);

    @Query("select new tw.org.ctworld.meditation.beans.Common.Common012_2Object(" +
            "e.memberId, e.gender, e.classGroupId, e.classId) " +
            "from ClassEnrollForm e where e.classId = ?1 order by cast(e.classGroupId as int)")
    List<Common012_2Object> GetClassGroupListByClassId(String classId);

    @Query("select new tw.org.ctworld.meditation.models.ClassEnrollForm(" +
            "e.memberId, e.classEnrollFormId, e.aliasName, e.ctDharmaName, e.mobileNum1) " +
            "from ClassEnrollForm e where e.classEnrollFormId in ?1 and e.isDroppedClass = 0 order by e.memberId")
    List<ClassEnrollForm> getMemberClassListByClassEnrollFormIdList(List<String> classEnrollFormIdList);

    @Query("select e.memberId from ClassEnrollForm e where e.classId = ?1 and e.isDroppedClass = 0")
    List<String> GetMemberIdByClassId(String classId);

    @Query("select new tw.org.ctworld.meditation.models.ClassEnrollForm(e.classId, e.className, e.dayOfWeek) " +
            "from ClassEnrollForm e where e.memberId = ?1 and e.isDroppedClass = 0 order by e.classId")
    List<ClassEnrollForm> GetClassEnrollFormListByMemberMemberId(String memberId);

    @Query("select new tw.org.ctworld.meditation.models.ClassEnrollForm(" +
            "e.memberId, e.classEnrollFormId, e.gender, e.aliasName, e.ctDharmaName, e.classPeriodNum, e.className, " +
            "e.classGroupId, e.memberGroupNum, e.classJobContentList, e.isFullAttended, e.isGraduated, " +
            "e.isFullAttendedPrinted, e.isFullAttendedCertPrintedDtTm, e.isGraduatedPrinted, " +
            "e.isGraduatedCertPrintedDtTm, e.certPrintUserName) " +
            "from ClassEnrollForm e where e.classId = ?1")
    List<ClassEnrollForm> GetClassEnrollFormListByClassId2(String classId);

    @Query("select e from ClassEnrollForm e where e.classEnrollFormId in ?1")
    List<ClassEnrollForm> GetClassEnrollFormListByClassEnrollFormIdList(List<String> classEnrollFormIdList);

    @Query("select new tw.org.ctworld.meditation.beans.Classes.Classes009Response(" +
            "e.id, e.classEnrollFormId, e.memberId, e.gender, e.aliasName, e.ctDharmaName, e.classGroupId, " +
            "e.memberGroupNum, e.classJobTitleList, e.classJobContentList, e.isLeadGroup) " +
            "from ClassEnrollForm e where e.classEnrollFormId = ?1")
    Classes009Response GetLeaderInfoByClassEnrollFormId(String classEnrollFormId);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.SearchMemberInfoEnrollForm(" +
            "e.memberId, e.classId, e.className, e.classPeriodNum, e.isDroppedClass) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.memberId in ?1 and i.classStatus = '開課中'")
    List<SearchMemberInfoEnrollForm> GetCurrentEnrollFormByMemberIds(List<String> memberIds);

    @Query("select new tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup002Object(" +
            "e.aliasName, e.ctDharmaName, e.gender, e.className, e.classPeriodNum, e.classGroupId, e.memberGroupNum, " +
            "e.dsaJobNameList, e.memberId, e.classEnrollFormId) " +
            "from ClassEnrollForm e where e.classId = ?1 and e.isDroppedClass = false order by e.classJobHighestNum desc, e.gender desc, e.classGroupId asc, cast(e.memberGroupNum as int) asc")
    List<ClassGroup002Object> GetClassEnrollFormListByClassId3(String classId);

    //add by ralph
    @Query("select new tw.org.ctworld.meditation.beans.MemberImport.MemberEnrollInfo(" +
            "e.memberId, e.classId, e.className, e.classTypeNum,e.classTypeName,e.classJobTitleList,e.homeClassNote) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.memberId in ?1 and i.classStatus = '開課中'")
    List<MemberEnrollInfo> GetCurrentEnrollInfosByMemberIds(List<String> memberIds);

    @Query("select new tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup001Object(e.classId, e.classPeriodNum, e.className) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where i.unitId = ?1 and e.memberId = ?2 and (i.classStatus = '開課中') " +
            "order by e.classPeriodNum desc, e.classTypeNum desc")
    List<ClassGroup001Object> GetCurrentOpeningClassEnrolled(String unitId, String memberId);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.ClassEnrollFormShort(" +
            "e.classId, e.className, e.classPeriodNum, e.classGroupId, e.classTypeNum, e.classTypeName, e.memberId) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where e.memberId in ?1 and (i.classStatus = '開課中' or i.classStatus ='待結業')")
    List<ClassEnrollFormShort> GetCurrentOpeningClassEnrolled(List<String> memberIds);


    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.ClassEnrollFormShort(" +
            "e.className, e.classJobTitleList, e.memberId, e.homeClassNote, i.classTypeNum) " +
            "from ClassEnrollForm e inner join ClassInfo i on e.classId = i.classId " +
            "where (?1 is null or e.unitId = ?1) and i.classStatus = '開課中' and e.memberId in ?2 order by i.classTypeNum")
    List<ClassEnrollFormShort> GetClassNameAndJobList(String unitId, List<String> memberIds);

    @Query("select e.memberId from ClassEnrollForm e where e.unitId = ?1 and e.classId = ?2")
    List<String> GetEnrolledMemberId(String unitId, String classId);
}
