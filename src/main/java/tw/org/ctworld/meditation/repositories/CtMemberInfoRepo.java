package tw.org.ctworld.meditation.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.Anonymous.Anonymous005_1Object;
import tw.org.ctworld.meditation.beans.Anonymous.Anonymous005_2Object;
import tw.org.ctworld.meditation.beans.EventEnrollMember.ExcelMember;
import tw.org.ctworld.meditation.beans.EventEnrollMember.MemberInfoNotEnrolled;
import tw.org.ctworld.meditation.beans.EventEnrollMember.MemberShort;
import tw.org.ctworld.meditation.beans.Member.MembersObject;
import tw.org.ctworld.meditation.beans.MemberImport.MemberCompareItem;
import tw.org.ctworld.meditation.beans.MemberImport.MemberListItem;
import tw.org.ctworld.meditation.beans.MemberImport.MemberTemplateItem;
import tw.org.ctworld.meditation.beans.MemberInfos.DuplicateMember;
import tw.org.ctworld.meditation.beans.MemberInfos.MemberInfoItem;
import tw.org.ctworld.meditation.beans.MemberInfos.MemberInfos001_1Object;
import tw.org.ctworld.meditation.beans.MemberInfos.SearchMemberInfo;
import tw.org.ctworld.meditation.beans.Sys.SearchMember;
import tw.org.ctworld.meditation.models.CtMemberInfo;

import java.util.List;

public interface CtMemberInfoRepo extends JpaRepository<CtMemberInfo, Long> {

    @Query("select m from CtMemberInfo m where m.aliasName = ?1 and m.unitId = ?2 and m.isDeleteByUnit = ?3")
    List<CtMemberInfo> GetClassCtMemberInfoListByRequestParam(String aliasName, String unitId, boolean isDeleteByUnit);

    @Query("select m from CtMemberInfo m where m.memberId = ?1")
    CtMemberInfo GetClassCtMemberInfoByMemberId(String memberId);

    @Query("select new tw.org.ctworld.meditation.beans.Member.MembersObject(m.memberId, m.gender, m.aliasName, " +
            "m.ctDharmaName, m.homePhoneNum1, m.mobileNum1, m.dsaJobNameList, m.twIdNum, m.mailingCountry, " +
            "m.mailingState, m.mailingCity, m.mailingStreet, m.companyJobTitle, m.schoolName, m.schoolMajor, " +
            "m.birthDate, m.age) " +
            "from CtMemberInfo m where m.memberId = ?1")
    MembersObject GetClassCtMemberInfoByMemberId2(String memberId);

    @Query("select m from CtMemberInfo m where (m.aliasName = ?1 or m.fullName = ?2) and m.unitId = ?3 and m.isDeleteByUnit = ?4")
    List<CtMemberInfo> GetClassCtMemberInfoDuplicateName(String aliasName, String fullName, String unitId, boolean isDeleteByUnit);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.DuplicateMember(m.memberId, m.twIdNum, m.fullName, m.aliasName, m.ctDharmaName, m.gender, m.birthDate, m.mailingCountry, m.mailingState, m.mailingCity, m.mailingStreet) from CtMemberInfo m where (m.aliasName = ?1 or m.fullName = ?2) and m.unitId = ?3 and m.isDeleteByUnit = ?4")
    List<DuplicateMember> GetClassCtMemberInfoDuplicateNameV2(String aliasName, String fullName, String unitId, boolean isDeleteByUnit);

    @Query("select m from CtMemberInfo m where m.mailingCity = ?1 and m.mailingStreet = ?2 and m.unitId = ?3 and m.isDeleteByUnit = ?4")
    List<CtMemberInfo> GetClassCtMemberInfoDuplicateAddress1(String mailingCity, String mailingStreet, String unitId, boolean isDeleteByUnit);

    @Query("select m from CtMemberInfo m where m.mailingCity = ?1 and m.unitId = ?2 and m.isDeleteByUnit = ?3")
    List<CtMemberInfo> GetClassCtMemberInfoDuplicateAddress2(String mailingCity, String unitId, boolean isDeleteByUnit);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.DuplicateMember(m.memberId, m.twIdNum, m.fullName, m.aliasName, m.ctDharmaName, m.gender, m.birthDate, m.mailingCountry, m.mailingState, m.mailingCity, m.mailingStreet) from CtMemberInfo m where m.mailingCity = ?1 and (?2 is null or m.mailingStreet = ?2) and m.unitId = ?3 and m.isDeleteByUnit = ?4")
    List<DuplicateMember> GetClassCtMemberInfoDuplicateAddressV2(String mailingCity, String mailingStreet, String unitId, boolean isDeleteByUnit);

    @Query("select m from CtMemberInfo m where m.familyLeaderMemberId = ?1 and m.unitId = ?2 and m.isDeleteByUnit = ?3")
    List<CtMemberInfo> GetClassCtMemberInfoDuplicateFamily(String familyLeaderMemberId, String unitId, boolean isDeleteByUnit);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.DuplicateMember(m.memberId, m.aliasName, m.ctDharmaName, m.gender, m.birthDate, m.familyLeaderName, m.familyLeaderMemberId, m.familyLeaderRelationship, m.homePhoneNum1, m.mobileNum1) from CtMemberInfo m where m.familyLeaderMemberId = ?1 and m.unitId = ?2 and m.isDeleteByUnit = ?3")
    List<DuplicateMember> GetClassCtMemberInfoDuplicateFamilyV2(String familyLeaderMemberId, String unitId, boolean isDeleteByUnit);

    @Query("select m from CtMemberInfo m where m.familyLeaderMemberId = ?1 and m.unitId = ?2 and m.isDeleteByUnit = ?3")
    List<CtMemberInfo> GetClassCtMemberInfoClassEnrollForm(String familyLeaderMemberId, String unitId, boolean isDeleteByUnit);

    /*
     excel import search member
      */
//    @Query("select m.memberId from ClassCtMemberInfo m where m.twIdNum = ?2")
//    List<String> GetMemberIdByTwid(String twId);

    @Query("select new tw.org.ctworld.meditation.beans.Sys.SearchMember(m.memberId, m.aliasName, m.ctDharmaName, m.gender, m.unitName, m.birthDate, m.mobileNum1) from CtMemberInfo m where m.aliasName like %?1%")
    List<SearchMember> SearchByAliasName(String aliasNAme);

    @Query("select m from CtMemberInfo m where m.aliasName = ?1 and m.twIdNum = ?2 and m.unitId = ?3")
    List<CtMemberInfo> GetMemberIdByNameTwId(String name, String twId, String unitId);

    @Query("select m from CtMemberInfo m where m.aliasName = ?1 and m.birthDate = ?2 and m.unitId = ?3")
    List<CtMemberInfo> GetMemberIdByNameBirthday(String name, String birthday, String unitId);

//    @Query("select m.memberId from ClassCtMemberInfo m where m.aliasName = ?1")
//    List<String> GetMemberIdByName(String name);
//
//    @Query("select m.memberId from ClassCtMemberInfo m where m.aliasName like %?1%")
//    List<String> GetMemberIdLikeName(String name);

    @Query("select m from CtMemberInfo m where m.twIdNum is null or m.twIdNum = ''")
    List<CtMemberInfo> GetMemberNoTwId(Pageable pageable);

    @Query("select m from CtMemberInfo m where m.memberId in ?1")
    List<CtMemberInfo> GetMemberInfoByMemberIds(List<String> memberIds);

    @Query("select m.memberId from CtMemberInfo m order by rand()")
    List<String> GetMemberIdList();

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.MemberInfos001_1Object(" +
            "m.memberId, m.gender, m.aliasName, m.ctDharmaName, m.age, m.homePhoneNum1, m.mobileNum1, m.dsaJobNameList) " +
            "from CtMemberInfo m where m.unitId = ?1 and m.isDeleteByUnit <> 1")
    List<MemberInfos001_1Object> GetAllMemberInfoListByUnitId(String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.Anonymous.Anonymous005_1Object(" +
            "m.memberId, m.aliasName, m.ctDharmaName, m.twIdNum, m.birthDate, m.mobileNum1, m.mobileNum2, m.homePhoneNum1, m.homePhoneNum2) " +
            "from CtMemberInfo m where m.unitId = ?1 and (m.twIdNum like %?2 or m.mobileNum1 like %?2 or m.mobileNum2 like %?2 or " +
            "m.homePhoneNum1 like %?2 or m.homePhoneNum2 like %?2 or (Month(m.birthDate) = ?3 and Day(m.birthDate) = ?4))")
    List<Anonymous005_1Object> GetMemberInfoLikeKeyword(String unitId, String keyword, int month, int day);

    @Query("select new tw.org.ctworld.meditation.beans.Anonymous.Anonymous005_1Object(" +
            "m.memberId, m.aliasName, m.ctDharmaName, m.twIdNum, m.birthDate, m.mobileNum1, m.mobileNum2, m.homePhoneNum1, m.homePhoneNum2) " +
            "from CtMemberInfo m where m.unitId = ?1 and (m.aliasName like %?2% or m.fullName like %?2%)")
    List<Anonymous005_1Object> GetMemberInfoLikeKeyword(String unitId, String keyword);

    @Query("select (count(m) > 0) from CtMemberInfo m where m.memberId = ?1 and m.twIdNum like %?2")
    boolean GetClassCtMemberInfoByTwIdNum(String memberId, String twIdNum);

    @Query("select (count(m) > 0) from CtMemberInfo m where m.memberId = ?1 and (Month(m.birthDate) = ?2 and Day(m.birthDate) = ?3)")
    boolean GetClassCtMemberInfoByBirthDate(String memberId, int month, int day);

    @Query("select (count(m) > 0) from CtMemberInfo m where m.memberId = ?1 and m.mobileNum1 like %?2")
    boolean GetClassCtMemberInfoByMobileNum1(String memberId, String mobileNum1);

    @Query("select (count(m) > 0) from CtMemberInfo m where m.memberId = ?1 and m.mobileNum2 like %?2")
    boolean GetClassCtMemberInfoByMobileNum2(String memberId, String mobileNum2);

//    @Query("select (count(m) > 0) from ClassCtMemberInfo m where m.memberId = ?1 and m.mobileNum3 like %?2")
//    boolean GetClassCtMemberInfoByMobileNum3(String memberId, String mobileNum3);

    @Query("select (count(m) > 0) from CtMemberInfo m where m.memberId = ?1 and m.homePhoneNum1 like %?2")
    boolean GetClassCtMemberInfoByHomePhoneNum1(String memberId, String homePhoneNum1);

    @Query("select (count(m) > 0) from CtMemberInfo m where m.memberId = ?1 and m.homePhoneNum2 like %?2")
    boolean GetClassCtMemberInfoByHomePhoneNum2(String memberId, String homePhoneNum2);

    @Query("select new tw.org.ctworld.meditation.beans.Anonymous.Anonymous005_2Object(" +
            "m.memberId, m.aliasName, m.ctDharmaName) " +
            "from CtMemberInfo m where m.unitId = ?1 and m.memberId = ?2")
    List<Anonymous005_2Object> GetCtMemberInfoListByUnitIdAndMemberId(String unitId, String memberId);

    @Query("select m.aliasName from CtMemberInfo m where m.memberId = ?1")
    List<String> GetAliasName(String memberId);

    @Query("select m from CtMemberInfo m where Month(m.birthDate) = ?1 and Day(m.birthDate) = ?2")
    List<CtMemberInfo> GetMemberByBirthDate(int month, int day);

//    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.MemberInfoItem(" +
//            "m.aliasName, m.memberId, m.ctDharmaName, m.gender, m.birthDate, m.dsaJobNameList, m.updateDtTm, m.twIdNum, " +
//            "m.isDeleteByUnit, m.updatorName, m.unitId, m.unitName) " +
//            "from CtMemberInfo m where (m.aliasName = ?1 or m.fullName = ?1 or m.engFirstName = ?1 or m.engLastName = ?1) and m.isDeleteByUnit <> 1")
//    List<MemberInfoItem> FindSameName(String name);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.MemberInfoItem(" +
            "m.aliasName, m.memberId, m.ctDharmaName, m.gender, m.birthDate, m.dsaJobNameList, m.updateDtTm, m.twIdNum, " +
            "m.isDeleteByUnit, m.updatorName, m.unitId, m.unitName) " +
            "from CtMemberInfo m where (?2 is null or m.unitId = ?2) and " +
            "(m.aliasName like %?1% or m.fullName like %?1% or m.ctDharmaName like %?1%) and m.isDeleteByUnit <> 1")
    List<MemberInfoItem> SearchName(String name, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.MemberInfoItem(" +
            "m.aliasName, m.memberId, m.ctDharmaName, m.gender, m.birthDate, m.dsaJobNameList, m.updateDtTm, m.twIdNum, " +
            "m.isDeleteByUnit, m.updatorName, m.unitId, m.unitName) " +
            "from CtMemberInfo m where (?2 is null or m.unitId = ?2) and " +
            "(m.aliasName = ?1 or m.fullName = ?1 or m.engFirstName = ?1 or m.engLastName = ?1) order by m.memberId")
    List<MemberInfoItem> FindSameName(String name, String unitId);

    @Query("select max(m.id) from CtMemberInfo m")
    int GetMaxId();

    @Query("select max(m.memberId) from CtMemberInfo m where m.memberId like ?1")
    String GetMaxMemberId(String criteria);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.SearchMemberInfo(" +
            "m.memberId, m.gender, m.aliasName, m.ctDharmaName, m.age, m.homePhoneNum1, m.mobileNum1, m.dsaJobNameList, m.unitId, m.unitName, m.birthDate, m.updateDtTm) " +
            "from CtMemberInfo m " +
            "where (?10 is null or m.unitId = ?10) and (?1 is null or m.aliasName like %?1% or m.fullName like %?1% or m.engLastName like %?1% or m.engFirstName like %?1%) and (?2 is null or m.ctDharmaName like %?2%) and (?3 is null or m.twIdNum like %?3%) and (?4 is null or m.memberId like %?4%) and (?5 is null or m.dsaJobNameList like %?5%) and (?6 is null or m.email1 like %?6% or m.email2 like %?6%) and (?7 is null or m.homePhoneNum1 like %?7% or m.homePhoneNum2 like %?7% or m.officePhoneNum1 like %?7% or m.mobileNum1 like %?7% or m.mobileNum2 like %?7%) and (?8 is null or m.mailingStreet like %?8% or m.permanentStreet like %?8% or m.mailingCity like %?8% or m.permanentCity like %?8%) and (?9 is null or m.skillList like %?9%) and m.isDeleteByUnit = 0 order by m.memberId")
    List<SearchMemberInfo> SearchMember(String name, String ctDharmaName, String twIdNum, String memberId, String dsaJobNameList, String email, String phone, String address, String skill, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.SearchMemberInfo(" +
            "m.memberId, m.gender, m.aliasName, m.ctDharmaName, m.age, m.homePhoneNum1, m.mobileNum1, m.dsaJobNameList, m.unitId, m.unitName, m.birthDate, m.updateDtTm) " +
            "from CtMemberInfo m " +
            "where (?10 is null or m.unitId = ?10) and (?1 is null or m.aliasName like %?1% or m.fullName like %?1% or m.engLastName like %?1% or m.engFirstName like %?1%) and (?2 is null or m.ctDharmaName like %?2%) and (?3 is null or m.twIdNum like %?3%) and (?4 is null or m.memberId like %?4%) and (?5 is null or m.dsaJobNameList like %?5%) and (?6 is null or m.email1 like %?6% or m.email2 like %?6%) and (?7 is null or m.homePhoneNum1 like %?7% or m.homePhoneNum2 like %?7% or m.officePhoneNum1 like %?7% or m.mobileNum1 like %?7% or m.mobileNum2 like %?7%) and (?8 is null or m.mailingStreet like %?8% or m.permanentStreet like %?8% or m.mailingCity like %?8% or m.permanentCity like %?8%) and (?9 is null or m.skillList like %?9%) and Year(m.birthDate) = ?11 and Month(m.birthDate) = ?12 and Day(m.birthDate) = ?13 and m.isDeleteByUnit = 0 order by m.memberId")
    List<SearchMemberInfo> SearchMember(String name, String ctDharmaName, String twIdNum, String memberId, String dsaJobNameList, String email, String phone, String address, String skill, String unitId, int year, int month, int day);

    @Query("select m.memberId from CtMemberInfo m order by m.memberId")
    List<String> GetAllMemberIdList();

    // added by ralph
    @Query("select new tw.org.ctworld.meditation.beans.MemberImport.MemberListItem("+
            "m.memberId,m.gender,m.aliasName,m.ctDharmaName,m.dsaJobNameList,m.classList,m.donationNameList,m.healthList,m.birthDate,m.twIdNum,m.passportNum,m.urgentContactPersonName1,m.urgentContactPersonRelationship1,m.urgentContactPersonPhoneNum1,m.age) "+
            "from CtMemberInfo m "+
            "where m.unitId=?1 and isDeleteByUnit <> 1 order by m.gender desc,m.classList asc ")
    List<MemberListItem> GetMemberListItem(String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.MemberImport.MemberCompareItem("+
            "m.memberId,m.aliasName,m.fullName,m.twIdNum, m.birthDate )"+
            "from CtMemberInfo m "+
            "where m.aliasName in ?1 or m.fullName in ?1 ")
    List<MemberCompareItem> GetAllMemberByNameList(List<String> names);

    @Query("select new tw.org.ctworld.meditation.beans.MemberImport.MemberTemplateItem("+
            "m.memberId,m.gender,m.aliasName,m.engLastName,m.engFirstName,m.ctDharmaName,m.ctRefugeMaster,m.birthDate,m.twIdNum,m.passportNum,m.homePhoneNum1,m.mobileNum1,m.officePhoneNum1,m.email1,m.skillList,m.healthList,m.mailingZip,m.mailingCountry,m.mailingState,m.mailingCity,m.mailingStreet,m.birthCountry,m.nationality,m.urgentContactPersonName1,m.urgentContactPersonRelationship1,m.urgentContactPersonPhoneNum1,m.introducerName,m.introducerRelationship,m.introducerPhoneNum,m.okSendEletter,m.okSendMagazine,m.okSendMessage,m.companyName,m.companyJobTitle,m.schoolDegree,m.schoolName,m.schoolMajor,m.clubName,m.clubJobTitleNow,m.clubJobTitlePrev,m.parentsName,m.parentsPhoneNum,m.parentsJobTitle,m.parentsEmployer,m.dataUnitMemberId,m.dataSentToCTDate,m.classList,m.isTakeRefuge,m.refugeDate,m.isTakePrecept5,m.precept5Date,m.isTakeBodhiPrecept,m.bodhiPreceptDate,m.donationNameList,m.dsaJobNameList)"+
            "from CtMemberInfo m "+
            "where m.memberId in ?1 and isDeleteByUnit <> 1 order by m.gender desc,m.classList asc ")
    List<MemberTemplateItem> GetMemberDataByMemberIds(List<String> memberIds);

    @Query("select max(m.id) from CtMemberInfo m ")
    Integer GetMaxDocId();

    @Query("select (count(m) > 0) "+
            "from CtMemberInfo m "+
            "where m.unitId=?1 and m.gender=?2 and m.aliasName=?3 and m.twIdNum=?4  ")
    boolean CountMemberByInfo(String unitId,String gender,String aliasName,String twIdNum);

    @Query("select (count(m) > 0) from CtMemberInfo m where m.memberId = ?1")
    boolean CheckMemberId(String memberId);


    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.MemberShort("+
            "m.memberId, m.aliasName, m.ctDharmaName, m.gender, m.birthDate) "+
            "from CtMemberInfo m "+
            "where m.memberId in ?1 order by m.memberId")
    List<MemberShort> GetMemberShortByMemberIds(List<String> memberIds);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.MemberInfoNotEnrolled("+
            "m.aliasName, m.classList, m.ctDharmaName, m.donationNameList, m.dsaJobNameList, m.healthList, " +
            "m.memberId, m.twIdNum, m.updatorName, m.urgentContactPersonName1, m.urgentContactPersonPhoneNum1, " +
            "m.urgentContactPersonRelationship1, m.birthDate, m.updateDtTm, m.age, m.familyLeaderName, m.unitId, " +
            "m.unitName, m.gender, m.passportNum) " +
            "from CtMemberInfo m "+
            "where (?1 is null or m.unitId = ?1) and (?3 = false or m.memberId not in ?2) and (?5 = false or m.memberId in ?4) and (?6 is null or m.aliasName like %?6%) order by m.memberId")
    List<MemberInfoNotEnrolled> GetMembersExcludeMemberIds(String unitId, List<String> enrolledMemberIds, boolean hasEnroll, List<String> inClassMemberIds, boolean hasInClass, String aliasName);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.MemberInfoNotEnrolled("+
            "m.aliasName, m.classList, m.ctDharmaName, m.donationNameList, m.dsaJobNameList, m.healthList, " +
            "m.memberId, m.twIdNum, m.updatorName, m.urgentContactPersonName1, m.urgentContactPersonPhoneNum1, " +
            "m.urgentContactPersonRelationship1, m.birthDate, m.updateDtTm, m.age, m.familyLeaderName, m.unitId, " +
            "m.unitName, m.gender, m.passportNum) " +
            "from CtMemberInfo m "+
            "where (?1 is null or m.unitId = ?1) and (?3 = false or m.memberId not in ?2) and (?4 is null or m.aliasName like %?4% or m.ctDharmaName like %?4%) and (?5 is null or (m.memberId like %?5% or m.aliasName like %?5% or m.ctDharmaName like %?5% or m.dsaJobNameList like %?5% or m.classList like %?5% or m.donationNameList like %?5% or m.healthList like %?5% or m.twIdNum like %?5% or m.urgentContactPersonName1 like %?5% or m.urgentContactPersonPhoneNum1 like %?5% or m.urgentContactPersonRelationship1 like %?5% or m.unitName like %?5% or m.updatorName like %?5%))")
    Page<MemberInfoNotEnrolled> GetMembersExcludeMemberIds(String unitId, List<String> enrolledMemberIds, boolean hasEnroll, String aliasName, String keyword, Pageable pageable);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.MemberInfoNotEnrolled("+
            "m.aliasName, m.classList, m.ctDharmaName, m.donationNameList, m.dsaJobNameList, m.healthList, " +
            "m.memberId, m.twIdNum, m.updatorName, m.urgentContactPersonName1, m.urgentContactPersonPhoneNum1, " +
            "m.urgentContactPersonRelationship1, m.birthDate, m.updateDtTm, m.age, m.familyLeaderName, m.unitId, " +
            "m.unitName, m.gender, m.passportNum) " +
            "from CtMemberInfo m "+
            "where (?1 is null or m.unitId = ?1) and m.memberId in ?2")
    List<MemberInfoNotEnrolled> GetMembersByMemberIds(String unitId, List<String> memberIds);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.MemberInfoNotEnrolled("+
            "m.aliasName, m.classList, m.ctDharmaName, m.donationNameList, m.healthList, " +
            "m.memberId, m.twIdNum, m.urgentContactPersonName1, m.urgentContactPersonPhoneNum1, " +
            "m.urgentContactPersonRelationship1, m.birthDate, m.age, m.carLicenseNum1, m.passportNum, " +
            "m.relativeMeritList, m.gender, m.mobileNum1) " +
            "from CtMemberInfo m "+
            "where m.memberId = ?1")
    List<MemberInfoNotEnrolled> Get4EventEnrollByMemberIds(String memberId);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.ExcelMember(" +
            "m.aliasName, m.ctDharmaName, m.twIdNum, m.memberId) " +
            "from CtMemberInfo m where (?1 is null or m.unitId = ?1) and m.isDeleteByUnit <> 1")
    List<ExcelMember> GetAllMemberInfoByUnitId(String unitId);

    @Query("select m.memberId from CtMemberInfo m where m.unitId = ?1 order by m.memberId")
    List<String> GetMemberIdListByUnitId(String unitId);
}
