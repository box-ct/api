package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.ClassBarCode.ClassBarCode001Object;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord003_2Object;
import tw.org.ctworld.meditation.beans.ClassStats.ClassStats008_2Object;
import tw.org.ctworld.meditation.beans.DataReport.DataReport005Object;
import tw.org.ctworld.meditation.beans.DataReport.DataReport007Object;
import tw.org.ctworld.meditation.beans.DataReport.DataReport011_1Object;
import tw.org.ctworld.meditation.models.ClassAttendRecord;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface ClassAttendRecordRepo extends JpaRepository<ClassAttendRecord, Long> {

    @Query("select count(c) from ClassAttendRecord c where c.classId = ?1 and c.classDate >= ?2 and c.classDate <= ?3 and c.attendMark in ?4")
    int GetEnrollCountByClassIdAndClassDate(String classId, Date classStartDate, Date classEndDate, List<String> attendMarkList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassAttendRecord c where c.classId = ?1")
    void DeleteClassAttendRecordByClassId(String classId);

    @Query("select distinct(c.classId) from ClassAttendRecord c where c.unitId = ?1 and c.classDate >= ?2 and c.classDate <= ?3")
    List<String> GetClassIdListByUnitIdAndClassDate(String unitId, Date classStartDate, Date classEndDate);

    @Query("select distinct(c.classId) from ClassAttendRecord c where c.classDate >= ?1 and c.classDate <= ?2")
    List<String> GetClassIdListByUnitIdAndClassDate(Date classStartDate, Date classEndDate);

    @Query("select count(c) from ClassAttendRecord c where c.classId = ?1 and c.classDate >= ?2 and c.classDate <= ?3")
    int GetTotalCountByClassIdAndClassDate(String classId, Date classStartDate, Date classEndDate);

    @Query("select c.classWeeksNum from ClassAttendRecord c where c.classId = ?1 and c.attendMark is not null order by c.classDate")
    List<Integer> GetAttendMarkLastWeekNumListByClassId(String classId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassAttendRecord c where c.id = ?1")
    void DeleteClassAttendRecordById(long id);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassAttendRecord c where c.classId = ?1 and c.classWeeksNum in ?2")
    void DeleteClassAttendRecordByClassIdAndClassWeeksNums(String classId, List<Integer> classWeeksNums);

    @Query("select c from ClassAttendRecord c where c.memberId = ?1 and c.classId = ?2 order by c.classWeeksNum asc ")
    List<ClassAttendRecord> GetClassAttendRecordListByMemberIdAndClassId(String memberId, String classId);

    @Query("select c from ClassAttendRecord c where c.memberId = ?1 and c.classId = ?2 and c.classWeeksNum in ?3 order by c.classWeeksNum asc ")
    List<ClassAttendRecord> GetClassAttendRecordListByMemberIdAndClassIdAndClassWeekNumList(String memberId, String classId, List<Integer> classWeekNumList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassAttendRecord c where c.memberId = ?1 and c.classId = ?2")
    void DeleteClassAttendRecordByMemberIdAndClassId(String memberId, String classId);

    @Query("select new tw.org.ctworld.meditation.beans.ClassBarCode.ClassBarCode001Object(" +
            "a.id, a.memberId, a.gender, a.aliasName, a.ctDharmaName, a.classPeriodNum, a.className, a.classGroupId, " +
            "e.memberGroupNum, a.classWeeksNum, a.attendCheckinDtTm, a.attendMark) " +
            "from ClassAttendRecord a inner join ClassEnrollForm e on a.classEnrollFormId = e.classEnrollFormId " +
            "where a.classId = ?1 and a.classDate >= ?2 and a.classDate <= ?3 and e.isDroppedClass = 0 " +
            "order by a.attendCheckinDtTm desc")
    List<ClassBarCode001Object> GetClassAttendRecordListByClassIdAndDate(String classId, Date classStartDate, Date classEndDate);

    @Query("select c from ClassAttendRecord c where c.id = ?1")
    ClassAttendRecord GetClassAttendRecordById(long id);

    @Query("select new tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord003_2Object(a.memberId, a.classWeeksNum, a.classDate, a.attendMark) " +
            "from ClassAttendRecord a where a.classId = ?1 order by a.classWeeksNum asc ")
    List<ClassRecord003_2Object> GetClassAttendRecordListByClassId(String classId);

    @Query("select a from ClassAttendRecord a where a.memberId = ?1 and a.classId = ?2 and a.classWeeksNum = ?3")
    ClassAttendRecord GetClassAttendRecordByMemberIdAndClassIdAndAttendMark(String memberId, String classId, int classWeeksNum);

    @Query("select c from ClassAttendRecord c where c.memberId = ?1 and c.classId = ?2 and c.classDate <= ?3")
    List<ClassAttendRecord> GetClassAttendRecordListByMemberIdAndClassIdAndEndDate(String memberId, String classId, Date endDate);

    @Query("select c from ClassAttendRecord c where c.classId = ?1 and c.classDate <= ?2")
    List<ClassAttendRecord> GetClassAttendRecordListByClassIdAndEndDate(String classId, Date endDate);

    @Query("select c from ClassAttendRecord c where c.memberId in ?1 and c.classId = ?2 and c.classDate <= ?3")
    List<ClassAttendRecord> GetClassAttendRecordListByMemberIdListAndClassIdAndEndDate(List<String> memberId, String classId, Date endDate);

    @Query("select new tw.org.ctworld.meditation.beans.ClassStats.ClassStats008_2Object(a.classDate, a.attendMark, a.updateDtTm) " +
            "from ClassAttendRecord a where a.memberId = ?1 and a.classId = ?2 and a.classDate <= ?3 order by a.classWeeksNum asc ")
    List<ClassStats008_2Object> GetExcelMakeUpClassesListByClassId(String memberId, String classId, Date classDate);

    // class - report
    @Query("select c from ClassAttendRecord c where c.classId = ?1 order by c.memberId, c.classWeeksNum")
    List<ClassAttendRecord> GetClassAttendRecordByClassId(String classId);

    @Query("select c.id from ClassAttendRecord c where c.classId = ?1 and c.classWeeksNum = ?2")
    List<Long> GetClassAttendRecordListByClassIdAndClassWeeksNum(String classId, int classWeeksNum);

    @Query("select c.attendMark from ClassAttendRecord c where c.memberId = ?1 and c.classId = ?2 order by c.classWeeksNum asc ")
    List<String> GetAttendMarkListByMemberIdAndClassId(String memberId, String classId);

    @Query("select a.classDate from ClassAttendRecord a where a.classDate >= ?1 and a.classDate <= ?2 order by a.classDate asc ")
    List<Date> GetClassAttendRecordClassDateListByClassDate(Date startDate, Date endDate);

    @Query("select a.classDate from ClassAttendRecord a where a.classDate >= ?1 and a.classDate <= ?2 and a.classId in ?3 order by a.classDate asc ")
    List<Date> GetClassAttendRecordClassDateListByClassDateAndClassIdList(Date startDate, Date endDate, List<String> classIdList);

    @Query("select a.classId from ClassAttendRecord a where a.classDate >= ?1 and a.classDate <= ?2 order by a.classDate asc ")
    List<String> GetClassAttendRecordClassIdListByClassDate(Date startDate, Date endDate);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport005Object(a.classId, a.attendMark, a.classDate, a.classEnrollFormId) " +
            "from ClassAttendRecord a " +
            "where a.classDate >= ?1 and a.classDate <= ?2")
    List<DataReport005Object> GetClassAttendRecordListByClassDate(Date startDate, Date endDate);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport007Object(a.unitId, a.classDate, a.memberId, " +
            "a.classId, a.classEnrollFormId, a.attendMark) " +
            "from ClassAttendRecord a " +
            "where a.classDate >= ?1 and a.classDate <= ?2 and a.unitId in ?3")
    List<DataReport007Object> GetClassAttendRecordListByClassDateAndUnitIds(Date startDate, Date endDate, List<String> unitIds);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport007Object(a.unitId, a.classDate, a.memberId, " +
            "a.classId, a.classEnrollFormId, a.attendMark, i.classDayOrNight, i.classTypeName, i.classLanguage) " +
            "from ClassAttendRecord a inner join ClassInfo i on a.classId = i.classId " +
            "where a.classDate >= ?1 and a.classDate <= ?2 and a.unitId in ?3 and i.classLanguage in ?4")
    List<DataReport007Object> GetClassAttendRecordListByClassDateAndUnitIdsAndLanguages(Date startDate, Date endDate, List<String> unitIds, List<String> languages);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport011_1Object(a.unitId, a.classDate, a.memberId, " +
            "a.classId, a.classEnrollFormId, a.attendMark, a.classWeeksNum) " +
            "from ClassAttendRecord a " +
            "where a.classDate >= ?1 and a.classDate <= ?2 and a.unitId in ?3")
    List<DataReport011_1Object> GetClassAttendRecordListByClassDateAndUnitIds2(Date startDate, Date endDate, List<String> unitIds);

    @Query("select new tw.org.ctworld.meditation.models.ClassAttendRecord(a.classEnrollFormId, a.attendMark) " +
            "from ClassAttendRecord a where a.classId = ?1 and a.classWeeksNum = ?2 and a.memberId in ?3")
    List<ClassAttendRecord> GetClassAttendRecordListByClassIdAndClassWeeksNum2(String classId, int classWeeksNum, List<String> memberIdList);

    @Query("select c from ClassAttendRecord c where c.memberId = ?1 and c.classId = ?2 and c.classWeeksNum = ?3")
    ClassAttendRecord GetClassAttendRecordByMemberIdAndClassIdAndClassWeeksNum(String memberId, String classId, int classWeeksNum);

    @Query("select c from ClassAttendRecord c where c.classEnrollFormId = ?1 and c.classWeeksNum = ?2")
    ClassAttendRecord GetClassAttendRecordByClassEnrollFormIdAndClassWeeksNum(String classEnrollFormId, int classWeeksNum);

    @Query("select new tw.org.ctworld.meditation.models.ClassAttendRecord(c.classId, c.classWeeksNum, c.classDate, c.attendMark, c.note) " +
            "from ClassAttendRecord c where c.memberId = ?1 and c.classId in ?2 order by c.classId, c.classWeeksNum")
    List<ClassAttendRecord> GetClassAttendRecordListByMemberIdAndClassId(String memberId, List<String> classIdList);

    @Query("select c.attendMark from ClassAttendRecord c where c.classId = ?1 and c.memberId = ?2")
    List<String> GetClassAttendMarkListByClassIdAndMemberId(String classId, String memberId);

    @Query("select c from ClassAttendRecord c where c.classId in ?1 and c.memberId = ?2")
    List<ClassAttendRecord> GetRecordsByClassIdsMemberId(List<String> classIds, String memberId);
}
