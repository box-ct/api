package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.Printer.Printer001Object;
import tw.org.ctworld.meditation.models.ClassCertInfo;
import tw.org.ctworld.meditation.models.ClassUnitPrinter;

import java.util.List;

public interface ClassUnitPrinterRepo extends JpaRepository<ClassUnitPrinter, Long> {

    @Query("select new tw.org.ctworld.meditation.beans.Printer.Printer001Object(u.id, u.printerName, u.printerId, u.note)" +
            " from ClassUnitPrinter u where u.unitId = ?1")
    List<Printer001Object> GetPrinterList(String unitId);

    @Query("select max(e.printerId) from ClassUnitPrinter e")
    String GetMaxPrinterId();

    @Query("select c from ClassUnitPrinter c where c.printerId = ?1")
    ClassUnitPrinter GetClassUnitPrinterByPrinterID(String printerId);

    @Query("select e.printerName from ClassUnitPrinter e where e.printerId = ?1")
    String GetPrinterNameByPrinterId(String printerId);

    @Query("select e.printerName from ClassUnitPrinter e where e.unitId = ?1")
    List<String> GetPrinterNameListByUnitId(String unitId);

    @Query("select e from ClassUnitPrinter e where e.unitId = ?1 and e.printerName = ?2")
    List<ClassUnitPrinter> GetClassUnitPrinterListByUnitIdAndPrinterName(String unitId, String printerName);
}
