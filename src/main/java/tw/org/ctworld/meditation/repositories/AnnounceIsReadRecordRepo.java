package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.AnnounceIsReadRecord;

import java.util.List;

public interface AnnounceIsReadRecordRepo extends JpaRepository<AnnounceIsReadRecord, Long> {

    @Query("select a.messageId from AnnounceIsReadRecord a where a.userId = ?1")
    List<String> FindClassAnnounceIsReadRecordByUserId(String userId);

    @Query("select (count(a) > 0) from AnnounceIsReadRecord a where a.userId = ?1 and a.messageId = ?2")
    boolean IsRead(String userId, String messageId);
}
