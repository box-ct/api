package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tw.org.ctworld.meditation.models.ClassUnitMasterInfo;

public interface ClassUnitMasterInfoRepo extends JpaRepository<ClassUnitMasterInfo, Long> {


}
