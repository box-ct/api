/*
* Auto Generated
* Based on name: ct_member_note
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.MemberInfos.MemberNote;
import tw.org.ctworld.meditation.models.CtMemberNote;

import java.util.List;

public interface CtMemberNoteRepo extends JpaRepository<CtMemberNote, Long> {

    @Query("select c from CtMemberNote c where c.memberId = ?1 and c.unitId = ?2 order by c.updateDtTm desc ")
    List<CtMemberNote> GetClassCtMemberInfoClassEnrollForm(String memberId, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.MemberInfos.MemberNote(c.id, c.note, c.noteType, c.createDtTm, c.creatorName, c.updateDtTm, c.updatorName) from CtMemberNote c where c.memberId = ?1 order by c.updateDtTm desc ")
    List<MemberNote> GetClassCtMemberNote(String memberId);

    @Query("select max(n.id) from CtMemberNote n")
    Integer GetMaxId();
}
