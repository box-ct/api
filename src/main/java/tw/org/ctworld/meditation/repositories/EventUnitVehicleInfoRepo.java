/*
* Auto Generated
* Based on name: event_unit_vehicle_info
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.EventUnitVehicleInfo;

import javax.transaction.Transactional;
import java.util.List;

public interface EventUnitVehicleInfoRepo extends JpaRepository<EventUnitVehicleInfo, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitVehicleInfo e where e.eventId = ?1 and e.unitId in ?2")
    void DeleteEventUnitVehicleInfoByEventIdAndUnitIdList(String eventId, List<String> unitIdList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitVehicleInfo e where e.eventId = ?1")
    void DeleteEventUnitVehicleInfoByEventId(String eventId);


    @Query("select v from EventUnitVehicleInfo v where v.eventId = ?1 and v.unitId = ?2 order by v.vehicleType, v.vehicleNum")
    List<EventUnitVehicleInfo> GetByEventIdAndUnitId(String eventId, String unitId);
}
