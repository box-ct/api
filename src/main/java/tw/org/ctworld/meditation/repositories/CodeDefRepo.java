package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.CodeDef;

import java.util.List;

public interface CodeDefRepo extends JpaRepository<CodeDef, Long> {

    @Query("select c.codeName from CodeDef c where c.codeCategory = ?1")
    List<String> GetName(String codeCategory);

    @Query("select c from CodeDef c where c.codeCategory = ?1 order by c.orderNum")
    List<CodeDef> GetClassCodeDefByCodeCategory(String codeCategory);

    @Query("select c from CodeDef c")
    List<CodeDef> GetClassCodeDefList();

    @Query("select c.codeId from CodeDef c where c.codeCategory = ?1 and c.codeName = ?2")
    String GetClassCodeDefCodeId(String codeCategory, String codeName);

    @Query("select c from CodeDef c where c.codeCategory = '精舍活動行程類別' and c.codeId = ?1")
    List<CodeDef> GetUnitEventPgms(String pgmId);
}
