package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.Printer.Printer001Object;
import tw.org.ctworld.meditation.beans.Printer.Printer005Object;
import tw.org.ctworld.meditation.models.ClassCertInfo;

import java.util.List;

public interface ClassCertInfoRepo extends JpaRepository<ClassCertInfo, Long> {

    @Query("select max(c.certTemplateId) from ClassCertInfo c")
    String GetMaxCertTemplateId();

    @Query("select c from ClassCertInfo c where c.printerId = ?1 order by c.id")
    List<ClassCertInfo> GetClassCertInfoListByPrinterID(String printerId);

    @Query("select distinct c.certTypeName from ClassCertInfo c where c.printerId = ?1 order by c.id")
    List<String> GetClassCertTypeNameListByPrinterId(String printerId);

    @Query("select new tw.org.ctworld.meditation.beans.Printer.Printer005Object(" +
            "c.id, c.certTemplateId, c.certTypeName, c.certTemplateName, c.description) " +
            "from ClassCertInfo c where c.printerId = ?1 and c.certTypeName = ?2 order by c.id")
    List<Printer005Object> GetClassClassCertInfoListByPrinterIDAndCertTypeName(String printerId, String certTypeName);

    @Query("select c from ClassCertInfo c where c.certTemplateId = ?1")
    ClassCertInfo GetClassCertInfoByCertTemplateId(String certTemplateId);

    @Query("select new tw.org.ctworld.meditation.beans.Printer.Printer001Object(c.printerId, c.certTypeName) " +
            "from ClassCertInfo c where c.printerId in ?1 order by c.id")
    List<Printer001Object> GetClassCertTypeNameListByPrinterIdList(List<String> printerIdList);

    @Query("select c from ClassCertInfo c where c.printerId = ?1")
    List<ClassCertInfo> GetClassCertInfoByPrinterId(String printerId);

    @Query("select c.certTemplateName from ClassCertInfo c where c.printerId = ?1 order by c.id")
    List<String> GetClassCertTypeLastNameListByPrinterId(String printerId);
}
