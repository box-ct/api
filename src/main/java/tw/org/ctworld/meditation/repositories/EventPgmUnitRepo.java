/*
* Auto Generated
* Based on name: event_pgm_unit
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.EventPgmUnit;

import javax.transaction.Transactional;
import java.util.List;

public interface EventPgmUnitRepo extends JpaRepository<EventPgmUnit, Long> {

    @Query("select e from EventPgmUnit e where e.pgmUniqueId = ?1")
    List<EventPgmUnit> GetEventPgmUnitListByPgmUniqueId(String pgmUniqueId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventPgmUnit e where e.pgmUniqueId = ?1")
    void DeleteEventPgmUnitByPgmUniqueId(String pgmUniqueId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventPgmUnit e where e.eventId = ?1 and e.unitId in ?2")
    void DeleteEventPgmUnitByEventIdAndUnitIdList(String eventId, List<String> unitIdList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventPgmUnit e where e.eventId = ?1")
    void DeleteEventPgmUnitByEventId(String eventId);

    @Query("select e.eventId from EventPgmUnit e where e.unitId = ?1")
    List<String> GetEventIdListByUnitId(String unitId);

    @Query("select e from EventPgmUnit e where e.unitId = ?1 and e.pgmUniqueId in ?2")
    List<EventPgmUnit> GetByUnitIdInPgmUniqueIds(String unitId, List<String> pgmUniqueIds);

    @Query("select e from EventPgmUnit e where e.unitId = ?1 and e.eventId = ?2 and e.pgmId = 'PGM10014'")
    EventPgmUnit GetByUnitIdEventIdDorm(String unitId, String eventId);

    @Query("select e from EventPgmUnit e where e.unitId = ?1 and e.eventId = ?2 and e.pgmId = 'PGM10017'")
    EventPgmUnit GetByUnitIdEventIdTrans(String unitId, String eventId);
}
