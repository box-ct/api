package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.Classes.Classes007Object;
import tw.org.ctworld.meditation.models.UserInfo;

import java.util.List;

public interface UserInfoRepo extends JpaRepository<UserInfo, Long> {

    // get login user auth info
    @Query("select l from UserInfo l where l.userId = ?1 and l.unitId = ?2")
    UserInfo FindLoginUserInfo(String userId, String unitId);

    // get login user auth info
    @Query("select l from UserInfo l where l.id = ?1")
    UserInfo FindLoginUserInfo(Long id);

    // 權限管理-人員權限列表(中台複製)
    @Query("select l from UserInfo l order by l.unitId, l.roleType")
    List<UserInfo> findAuthInfoByAll();

    // 紀錄-側邊欄權限列表(中台複製)
    @Query("select l from UserInfo l where l.unitId = ?1")
    List<UserInfo> findAuthInfoByUnitIdForRecord(String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.Classes.Classes007Object(l.memberId, l.id, l.managedClassList) " +
            "from UserInfo l where l.memberId in ?1")
    List<Classes007Object> GetMemberIdAndManagedListByMemberIdList(List<String> memberIdList);

    @Query("select l from UserInfo l where l.memberId = ?1")
    UserInfo GetClassLoginUserInfoById(String memberId);

    @Query("select l.managedClassList from UserInfo l where l.memberId = ?1")
    String GetClassLoginUserInfoManagedClassListByMemberId(String memberId);

    @Query("select l from UserInfo l where l.userId = ?1")
    UserInfo FindLoginUserInfo(String userId);
}