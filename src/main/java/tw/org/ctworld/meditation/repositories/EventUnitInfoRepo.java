/*
* Auto Generated
* Based on name: event_unit_info
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain003_2Object;
import tw.org.ctworld.meditation.beans.UnitStartUp.Forecast;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp003_2Object;
import tw.org.ctworld.meditation.models.EventUnitInfo;

import javax.transaction.Transactional;
import java.util.List;

public interface EventUnitInfoRepo extends JpaRepository<EventUnitInfo, Long> {

    @Query("select new tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain003_2Object(e.eventId) " +
            "from EventUnitInfo e where e.eventId in ?1")
    List<CtEventMaintain003_2Object> GetRecordsByEventIdList(List<String> eventIdList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitInfo e where e.eventId = ?1 and e.unitId in ?2")
    void DeleteEventUnitInfoByEventIdAndUnitIdList(String eventId, List<String> unitIdList);

    @Query("select e from EventUnitInfo e where e.eventId = ?1 and e.unitId in ?2")
    List<EventUnitInfo> GetEventUnitInfoListByEventIdAndUnitIdList(String eventId, List<String> unitIdList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitInfo e where e.eventId = ?1")
    void DeleteEventUnitInfoByEventId(String eventId);

    @Query("select e.eventEnrollGroupNameList from EventUnitInfo e " +
            "where e.unitId = ?1 and e.eventEnrollGroupNameList is not null and e.eventEnrollGroupNameList <> '' " +
            " and e.eventId <> ?2 order by e.createDtTm desc")
    List<String> GetLastEventEnrollGroupNameListByUnitId(String unitId, String eventId);

    @Query("select new tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp003_2Object(" +
            "e.peopleForecast, e.leaveUnitDate, e.leaveUnitTime, e.arriveCtDate, e.arriveCtTime, e.leaveCtDate, " +
            "e.leaveCtTime, e.leaderMasterName, e.leaderMasterId, e.leaderMasterPhoneNum, e.eventLeader, " +
            "e.eventLeaderMemberId, e.eventLeaderPhone, e.viceEventLeader1, e.viceEventLeader1MemberId, " +
            "e.viceEventLeader1Phone, e.viceEventLeader2, e.viceEventLeader2MemberId, e.viceEventLeader2Phone, " +
            "e.eventEnrollGroupNameList, e.eventUnitNote) " +
            "from EventUnitInfo e where e.eventId = ?1 and e.unitId = ?2")
    UnitStartUp003_2Object GetEventUnitInfoByEventIdAndUnitId(String eventId, String unitId);

    @Query("select e from EventUnitInfo e where e.eventId = ?1 and e.unitId = ?2")
    EventUnitInfo GetEventUnitInfoDetailByEventIdAndUnitId(String eventId, String unitId);

    @Query("select e from EventUnitInfo e where e.eventId in ?1 and (?2 is null or e.unitId = ?2)")
    List<EventUnitInfo> GetByEventIdList(List<String> eventIdList, String unitId);

    @Query("select e from EventUnitInfo e where e.eventId = ?1 and e.unitId in ?2")
    List<EventUnitInfo> GetByEventIdUnitIds(String eventIdList, List<String> unitIds);

    @Query("select new tw.org.ctworld.meditation.beans.UnitStartUp.Forecast(e.peopleForecast, e.eventId) " +
            "from EventUnitInfo e where e.eventId in ?1 and e.unitId = ?2")
    List<Forecast> GetForecastByEventIdList(List<String> eventIdList, String unitId);

    @Query("select new tw.org.ctworld.meditation.beans.UnitStartUp.Forecast(sum(e.peopleForecast), e.eventId) " +
            "from EventUnitInfo e where e.eventId in ?1 group by e.eventId")
    List<Forecast> GetForecastByEventIdList(List<String> eventIdList);

    @Query("select e from EventUnitInfo e where e.eventId = ?1")
    List<EventUnitInfo> GetEventUnitInfoByEventId(String eventId);
}
