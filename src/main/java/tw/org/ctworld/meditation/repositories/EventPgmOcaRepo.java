package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain011Object;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain006Object;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain008Object;
import tw.org.ctworld.meditation.models.EventPgmOca;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface EventPgmOcaRepo extends JpaRepository<EventPgmOca, Long> {

    @Query("select max(e.pgmUniqueId) from EventPgmOca e where length(e.pgmUniqueId) = 14 and e.pgmUniqueId like ?1")
    String GetMaxPgmUniqueId(String pgmUniqueId);

    @Query("select e from EventPgmOca e where e.pgmUniqueId in ?1")
    List<EventPgmOca> GetEventPgmOcaListByPgmUniqueIdList(List<String> pgmUniqueIdList);

    @Query("select new tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain011Object(" +
            "o.pgmUniqueId, o.isDisabledByOCA, o.isRequired, o.pgmUiOrderNum, o.pgmDate, o.pgmStartTime, o.pgmEndTime, " +
            "o.pgmCtOrderNum, o.pgmName, o.pgmNote, o.pgmDesc, o.goTransDate, o.goTransType, o.returnTransDate, " +
            "o.returnTransType, o.pgmCategoryId, o.pgmCategoryName, o.pgmId, o.isGenPgm, p.genPgmUiType, " +
            "p.genPgmInputOption, p.isCenterAvailable) " +
            "from EventPgmOca o inner join EventPgmDef p on o.pgmId = p.pgmId where o.eventId = ?1 " +
            "order by o.pgmCategoryId, o.pgmDate")
    List<CtEventMaintain011Object> GetCtEventPgmOcaListByEventId(String eventId);

    @Query("select new tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain008Object(" +
            "o.pgmId, o.pgmName, o.pgmUniqueId, o.pgmCategoryName, o.pgmDate, o.pgmStartTime, o.pgmEndTime, " +
            "o.pgmNote, o.pgmUiOrderNum) " +
            "from EventPgmOca o where o.eventId = ?1 " +
            "order by o.pgmCategoryId, o.pgmDate")
    List<UnitEventsMaintain008Object> GetUnitEventPgmOcaListByEventId(String eventId);

    @Query("select e from EventPgmOca e where e.pgmUniqueId = ?1")
    EventPgmOca GetEventPgmOcaByEventIdAndPgmUniqueId(String pgmUniqueId);

    @Query("select count(1) from EventPgmOca e where e.eventId = ?1 and e.pgmUniqueId <> ?2 and e.pgmCtOrderNum = ?3")
    int CheckIfPgmCtOrderNumExist(String eventId, String pgmUniqueId, int pgmCtOrderNum);

    @Query("select count(1) from EventPgmOca e where e.eventId = ?1 and e.pgmUniqueId <> ?2 and e.pgmUiOrderNum = ?3 and (?4 is null or ?4 = e.pgmDate)")
    int CheckIfPgmUiOrderNumExist(String eventId, String pgmUniqueId, int pgmUiOrderNum, Date pgmDate);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventPgmOca e where e.pgmUniqueId = ?1")
    void DeleteEventPgmOcaByPgmUniqueId(String pgmUniqueId);

    @Query("select count(e) from EventPgmOca e where e.eventId = ?1 and e.pgmUiOrderNum = ?2")
    int GetEventPgmOcaCountByEventIdAndPgmUiOrderNum(String eventId, Integer pgmUiOrderNum);

    @Query("select count(e) from EventPgmOca e where e.eventId = ?1 and e.pgmCtOrderNum = ?2")
    int GetEventPgmOcaCountByEventIdAndPgmCtOrderNum(String eventId, Integer pgmCtOrderNum);

    @Query("select e from EventPgmOca e where e.eventId = ?1")
    List<EventPgmOca> GetEventPgmOcaByEventId(String eventId);
}
