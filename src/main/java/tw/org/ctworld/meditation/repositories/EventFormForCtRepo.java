/*
* Auto Generated
* Based on name: event_form for_ct
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.EventFormForCt;

import javax.transaction.Transactional;
import java.util.List;

public interface EventFormForCtRepo extends JpaRepository<EventFormForCt, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventFormForCt e where e.eventId = ?1")
    void DeleteEventFormForCtByEventId(String eventId);

    @Query("select e from EventFormForCt e where e.formAutoAddNum in ?1")
    List<EventFormForCt> GetEventFormForCtListByFormAutoAddNumList(List<String> formAutoAddNumList);

    @Query("select count(e) from EventFormForCt e where e.eventId = ?1")
    int GetEventFormForCtCountByEventId(String eventId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventFormForCt e where e.eventId = ?1 and e.unitId in ?2")
    void DeleteEventFormForCtByEventIdAndUnitIdList(String eventId, List<String> unitIdList);

    @Query("select e from EventFormForCt e where e.eventId = ?1")
    List<EventFormForCt> GetEventFormForCtListByEventId(String eventId);
}
