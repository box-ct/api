package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.UserLoginLog;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface UserLoginLogRepo extends JpaRepository<UserLoginLog, Long>{

    @Query("select u from UserLoginLog u")
    List<UserLoginLog> GetClassUserLoginLog();

    @Query("select u from UserLoginLog u where u.sessionId = ?1")
    UserLoginLog FindClassUserLoginLogBySessionId(String sessionId);

//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("update ClassUserLoginLog u set u.logoutTime = :logoutTime where u.sessionId = :sessionId")
//    void UpdateLogoutTime(@Param("logoutTime") Date logoutTime, @Param("sessionId") String sessionId);

    // called when app start/restart
    @Transactional
    @Modifying
    @Query("update UserLoginLog l set l.logoutTime = current_timestamp where l.logoutTime is null")
    void LogoutAll();

    @Query("select l from UserLoginLog l where l.logoutTime is null and l.loginTime > ?1 order by l.loginTime desc")
    List<UserLoginLog> GetCurrentlyLogin(Date before);



}
