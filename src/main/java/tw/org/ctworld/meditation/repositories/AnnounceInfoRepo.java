package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.AnnounceInfo;

import java.util.List;

public interface AnnounceInfoRepo extends JpaRepository<AnnounceInfo, Integer> {

    @Query("select a from AnnounceInfo a where a.isEnabled = 1 order by a.announceDt desc ")
    List<AnnounceInfo> GetAllMsg();

    @Query("select a from AnnounceInfo a where a.messageId not in ?1 and a.isEnabled = 1 order by a.announceDt desc ")
    List<AnnounceInfo> GetUnReadMsg(List<String> readList);

    @Query("select max(a.messageId) from AnnounceInfo a where a.messageId like ?1")
    String GetMaxAssetId(String criteria);

    @Query("select (case when count(a) > 0 then 'TRUE' else 'FALSE' end) from AnnounceInfo a where a.messageId = ?1")
    boolean HasMessageId(String messageId);

    @Query("select a from AnnounceInfo a where a.messageId = ?1")
    List<AnnounceInfo> GetInfoByMessageId(String messageId);
}
