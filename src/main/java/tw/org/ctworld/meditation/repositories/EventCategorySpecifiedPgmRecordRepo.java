/*
* Auto Generated
* Based on name: event_category_specified_pgm_record
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef007Object;
import tw.org.ctworld.meditation.models.EventCategorySpecifiedPgmRecord;

import javax.transaction.Transactional;
import java.util.List;

public interface EventCategorySpecifiedPgmRecordRepo extends JpaRepository<EventCategorySpecifiedPgmRecord, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventCategorySpecifiedPgmRecord e where e.eventCategoryId = ?1")
    void DeleteEventCategorySpecifiedPgmRecordByEventCategoryId(String eventCategoryId);

    @Query("select new tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef007Object(" +
            "c.id, c.pgmCategoryId, c.pgmCategoryName, c.pgmId, c.pgmName, p.pgmDesc, p.isGenPgm, p.genPgmUiType, " +
            "p.genPgmInputOption, p.pgmStartTime, p.pgmEndTime, p.isCenterAvailable, p.pgmNote, c.updatorName, c.updateDtTm) " +
            "from EventCategorySpecifiedPgmRecord c inner join EventPgmDef p on c.pgmId = p.pgmId " +
            "where c.eventCategoryId = ?1")
    List<EventCategoryDef007Object> GetEventCategorySpecifiedListByEventCategoryId(String eventCategoryId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventCategorySpecifiedPgmRecord e where e.id = ?1")
    void DeleteEventCategorySpecifiedPgmRecordById(long id);

    @Query("select new tw.org.ctworld.meditation.models.EventCategorySpecifiedPgmRecord(" +
            "c.eventCategoryId, c.pgmId, c.pgmName) " +
            "from EventCategorySpecifiedPgmRecord c where c.eventCategoryId = ?1")
    List<EventCategorySpecifiedPgmRecord> GetEventCategorySpecifiedPgmRecordListByEventCategoryId(String eventCategoryId);
}
