package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.DataReport.DataReport011_2Object;
import tw.org.ctworld.meditation.models.ClassDateInfo;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface ClassDateInfoRepo extends JpaRepository<ClassDateInfo, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassDateInfo c where c.classId = ?1")
    void DeleteClassDateInfoByClassId(String classId);

    @Query("select c from ClassDateInfo c where c.classId = ?1 order by c.classWeeksNum")
    List<ClassDateInfo> GetClassDateInfoByClassId(String classId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassDateInfo c where c.id = ?1")
    void DeleteClassDateInfoById(long id);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassDateInfo c where c.id in ?1")
    void DeleteClassDateInfoByIds(List<Long> ids);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from ClassDateInfo c where c.classId = ?1")
    void DeleteAllClassDateInfoByClassId(String classId);

    @Query("select c from ClassDateInfo c where c.id = ?1")
    ClassDateInfo GetClassDateInfoById(long id);

    @Query("select c.classWeeksNum from ClassDateInfo c where c.id in ?1")
    List<Integer> GetClassDateInfoWeeksNumsByIds(List<Long> ids);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport011_2Object(c.classId, c.classDayOfWeek, c.classWeeksNum, c.classDate) " +
            "from ClassDateInfo c where c.classDate >= ?1 and c.classDate <= ?2 and c.unitId in ?3 and c.classDayOfWeek in ?4")
    List<DataReport011_2Object> GetClassDateInfoByClassDateAndUnitIds(Date classStartDate, Date classEndDate, List<String> unitIds, List<String> weeks);

    @Query("select count(c) from ClassDateInfo c where c.classId = ?1")
    int GetClassDateInfoCountByClassId(String classId);

    @Query("select distinct c.classId from ClassDateInfo c where c.unitId = ?1 and c.classDate >= ?2 and c.classDate <= ?3")
    List<String> GetClassIdByDate(String unitId, Date startDate, Date endDate);

    @Query("select c from ClassDateInfo c where c.classId in ?1 and c.isCancelled = 0 order by c.classId, c.classWeeksNum")
    List<ClassDateInfo> GetClassByClassIdList(List<String> classIdList);

    // makeup
    @Query("select c from ClassDateInfo c where c.classId in ?1 and c.isCancelled = 0 order by c.classWeeksNum")
    List<ClassDateInfo> GetClassDateByClassIdList(List<String> classIdList);

    // makeup
    @Query("select c from ClassDateInfo c where c.classId = ?1 and c.isCancelled = 0 order by c.classWeeksNum")
    List<ClassDateInfo> GetClassDateByClassId(String classId);

//    @Query("select max(c.makeupFileId) from ClassDateInfo c where c.makeupFileId like ?1")
//    String GetMaxMakeupFileId(String criteria);

    @Query("select c from ClassDateInfo c where c.classId = ?1 and c.isCancelled = 0 and c.classWeeksNum = ?2")
    List<ClassDateInfo> GetClassDateByClassIdWeekNum(String classId, int weekNum);

    // makeup
    @Query("select c from ClassDateInfo c where c.classId in ?1 and c.isCancelled = 0")
    List<ClassDateInfo> GetAllClassDateByClassIdList(List<String> classIdList);

    // makeup
    @Query("select c from ClassDateInfo c where c.makeupFileName is not null and c.uploadDtTm < ?1")
    List<ClassDateInfo> GetFilesOverYear(Date before);
}
