package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tw.org.ctworld.meditation.beans.DataReport.DataReport009Object;
import tw.org.ctworld.meditation.beans.DataReport.MonthlyTotal;
import tw.org.ctworld.meditation.models.OldClassMonthStats;

import java.util.List;


public interface OldClassMonthStatsRepo extends JpaRepository<OldClassMonthStats, Long> {

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.MonthlyTotal(o.year, o.month," +
            "sum(o.dayClass1Stats + o.dayClass2Stats + o.dayClass3Stats + o.dayClass4Stats + o.dayClass5Stats" +
            " + o.nightClass1Stats + o.nightClass2Stats + o.nightClass3Stats + o.nightClass4Stats + o.nightClass5Stats" +
            " + o.childrenClass + o.seniorClass + o. otherClass" +
            " + o.engDayClass1Stats + o.engDayClass2Stats + o.engDayClass3Stats + o.engDayClass4Stats" +
            " + o.engNightClass1Stats + o.engNightClass2Stats + o.engNightClass3Stats + o.engNightClass4Stats" +
            " + o.japDayClass1Stats)) " +
            "from OldClassMonthStats o where o.unitId in ?1 and cast(concat(o.year, o.month) as int) >= ?2 and cast(concat(o.year, o.month) as int) <= ?3 group by o.year, o.month")
    List<MonthlyTotal> getMonthlyCountByStartEndUnits(List<String> units, int start, int end);

    @Query("select o from OldClassMonthStats o where o.unitId in ?1 and cast(concat(o.year, o.month) as int) >= ?2 and cast(concat(o.year, o.month) as int) <= ?3")
    List<OldClassMonthStats> getMonthlyCountByStartEndUnitsDebug(List<String> units, int start, int end);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport009Object(o.year, o.month, " +
            "o.dayClass1Stats, o.dayClass2Stats, o.dayClass3Stats, o.dayClass4Stats, o.dayClass5Stats, " +
            "o.nightClass1Stats, o.nightClass2Stats, o.nightClass3Stats, o.nightClass4Stats, o.nightClass5Stats, " +
            "o.childrenClass, o.seniorClass, o. otherClass) " +
            "from OldClassMonthStats o where o.unitId in ?1 and cast(concat(o.year, o.month) as int) >= ?2 and cast(concat(o.year, o.month) as int) <= ?3")
    List<DataReport009Object> getLang1ByStartEndUnits(List<String> units, int start, int end);

    @Query("select new tw.org.ctworld.meditation.beans.DataReport.DataReport009Object(o.year, o.month, " +
            "o.engDayClass1Stats, o.engDayClass2Stats, o.engDayClass3Stats, o.engDayClass4Stats, " +
            "o.engNightClass1Stats, o.engNightClass2Stats, o.engNightClass3Stats, o.engNightClass4Stats, " +
            "o.japDayClass1Stats, " +
            "o.childrenClass, o.seniorClass, o. otherClass) " +
            "from OldClassMonthStats o where o.unitId in ?1 and cast(concat(o.year, o.month) as int) >= ?2 and cast(concat(o.year, o.month) as int) <= ?3")
    List<DataReport009Object> getLang2ByStartEndUnits(List<String> units, int start, int end);
}
