package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain016Object;
import tw.org.ctworld.meditation.models.EventCtSpecialInput;

import javax.transaction.Transactional;
import java.util.List;

public interface EventCtSpecialInputRepo extends JpaRepository<EventCtSpecialInput, Long> {

    @Query("select c.sInputName from EventCtSpecialInput c where c.eventId = ?1")
    List<String> GetSpecialInputNameByEventId(String eventId);

    @Query("select new tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain016Object(" +
            "e.sInputId, e.sInputName, e.sInputExample, e.sInputCategory, e.sInputWriteType, e.sInputOrderNum, e.isRequired) " +
            "from EventCtSpecialInput e where e.eventId = ?1 order by case when e.sInputOrderNum is null then 1 else 0 end, e.sInputOrderNum")
    List<CtEventMaintain016Object> GetEventUnitSpecialInputListByEventId(String eventId);

    @Query("select count(c) from EventCtSpecialInput c where c.eventId = ?1 and c.sInputOrderNum = ?2")
    int GetEventCtSpecialInputCountByEventIdAndsInputOrderNum(String eventId, int sInputOrderNum);

    @Query("select max(e.sInputId) from EventCtSpecialInput e where e.sInputId like ?1")
    String GetMaxSInputId(String sInputId);

    @Query("select c from EventCtSpecialInput c where c.eventId = ?1 and c.sInputId = ?2")
    List<EventCtSpecialInput> GetEventCtSpecialInputByEventIdAndsInputId(String eventId, String sInputId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventCtSpecialInput c where c.eventId = ?1 and c.sInputId = ?2")
    void DeleteEventCtSpecialInputByEventIdAndsInputId(String eventId, String sInputId);

    @Query("select c from EventCtSpecialInput c where c.eventId = ?1")
    List<EventCtSpecialInput> GetByEventId(String eventId);
}
