/*
* Auto Generated
* Based on name: ct_master_info
* Date: 2019-06-18 15:46:19
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort;
import tw.org.ctworld.meditation.beans.EventEnrollMember.ExcelMember;
import tw.org.ctworld.meditation.models.CtMasterInfo;
import tw.org.ctworld.meditation.beans.UnitMaster.UnitMaster001Object;
import tw.org.ctworld.meditation.models.UnitMasterInfo;

import java.util.List;

public interface CtMasterInfoRepo extends JpaRepository<CtMasterInfo, Long> {

    @Query("select m " +
            "from CtMasterInfo m where m.masterName like %?1% ")
    List<CtMasterInfo> GetCtMasterByAliasname(String aliasName);

    @Query("select m from CtMasterInfo m where m.masterId = ?1")
    CtMasterInfo GetCtMasterInfoByMasterId(String masterId);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort(" +
            "m.isAbbot, true, m.isTreasurer, m.jobOrder, m.jobTitle, m.masterId, m.masterName, m.masterPreceptTypeName, " +
            "m.mobileNum, m.preceptOrder, m.unitId, m.unitName, m.updateDtTm, m.updatorName) " +
            "from CtMasterInfo m where (?1 is null or upper(m.masterId) like ?1%) " +
            "and (?2 is null or m.masterName like %?2% or m.masterId like %?2%) " +
            "order by m.jobOrder, m.preceptOrder")
    List<MasterInfoShort> GetCtMasterShort(String masterIdPrefix, String keyword);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort(" +
            "m.isAbbot, true, m.isTreasurer, m.jobOrder, m.jobTitle, m.masterId, m.masterName, m.masterPreceptTypeName, " +
            "m.mobileNum, m.preceptOrder, m.unitId, m.unitName, m.updateDtTm, m.updatorName) " +
            "from CtMasterInfo m where m.masterId = ?1")
    List<MasterInfoShort> GetCtMasterShortByMasterId(String masterId);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMember.ExcelMember(" +
            "m.masterId, m.masterName) " +
            "from CtMasterInfo m")
    List<ExcelMember> GetAllMasterInfo();

    @Query("select m from CtMasterInfo m where m.masterId in ?1")
    List<CtMasterInfo> findByMemberId(List<String> masterIds);

    @Query("select new tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort(" +
            "m.isAbbot, true, m.isTreasurer, m.jobOrder, m.jobTitle, m.masterId, m.masterName, m.masterPreceptTypeName, " +
            "m.mobileNum, m.preceptOrder, m.unitId, m.unitName, m.updateDtTm, m.updatorName) " +
            "from CtMasterInfo m where m.masterId in ?1 " +
            "order by m.jobOrder, m.preceptOrder")
    List<MasterInfoShort> GetAllMasterInfoByMasterIds(List<String> masterIds);

    @Query("select new tw.org.ctworld.meditation.models.UnitMasterInfo(m) from CtMasterInfo m where m.masterId in ?1")
    List<UnitMasterInfo> GetAsUnitMasterInfoByMasterIds(List<String> masterIds);
}
