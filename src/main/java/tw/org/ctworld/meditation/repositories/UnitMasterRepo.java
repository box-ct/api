package tw.org.ctworld.meditation.repositories;

import org.springframework.stereotype.Repository;
import tw.org.ctworld.meditation.beans.EventEnrollMaster.EnrollMaster;
import tw.org.ctworld.meditation.models.UnitMasterInfo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UnitMasterRepo {
    @PersistenceContext
    private EntityManager entityManager;

    public List<UnitMasterInfo> GetMatchedMasters(List<EnrollMaster> enrollMasterList) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<UnitMasterInfo> query = cb.createQuery(UnitMasterInfo.class);
        Root<UnitMasterInfo> root = query.from(UnitMasterInfo.class);

        List<Predicate> predicates = new ArrayList<>();

        for (EnrollMaster master : enrollMasterList) {
            javax.persistence.criteria.Predicate unitId = cb.equal(root.get("unitId"), master.getUnitId());
            javax.persistence.criteria.Predicate masterId = cb.equal(root.get("masterId"), master.getMasterId());
            predicates.add(cb.and(unitId, masterId));
        }

        query.where(cb.or(predicates.toArray(new Predicate[predicates.size()])));

        return entityManager.createQuery(query).getResultList();
    }

}
