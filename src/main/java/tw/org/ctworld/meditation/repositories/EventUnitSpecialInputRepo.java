/*
* Auto Generated
* Based on name: event_unit_secial_input
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp003_3Object;
import tw.org.ctworld.meditation.models.EventUnitSpecialInput;

import javax.transaction.Transactional;
import java.util.List;

public interface EventUnitSpecialInputRepo extends JpaRepository<EventUnitSpecialInput, Long> {

    @Query("select e from EventUnitSpecialInput e where e.eventId = ?1 and e.sInputId = ?2")
    List<EventUnitSpecialInput> GetEventUnitSpecialInputListByEventIdAndsInputId(String eventId, String sInputId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitSpecialInput e where e.eventId = ?1 and e.sInputId = ?2")
    void DeleteEventUnitSpecialInputListByEventIdAndsInputId(String eventId, String sInputId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitSpecialInput e where e.eventId = ?1 and e.unitId in ?2")
    void DeleteEventUnitSpecialInputByEventIdAndUnitIdList(String eventId, List<String> unitIdList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventUnitSpecialInput e where e.eventId = ?1")
    void DeleteEventUnitSpecialInputByEventId(String eventId);

    @Query("select new tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp003_3Object(" +
            "e.id, e.sInputId, e.sInputName, e.sInputValue, e.isRequired, e.sInputExample, e.sInputCategory, e.sInputWriteType) " +
            "from EventUnitSpecialInput e where e.eventId = ?1 and e.unitId = ?2 order by e.sInputOrderNum")
    List<UnitStartUp003_3Object> GetEventUnitSpecialInputListByEventIdAndUnitId(String eventId, String unitId);

    @Query("select e from EventUnitSpecialInput e where e.eventId = ?1 and e.id in ?2")
    List<EventUnitSpecialInput> GetEventUnitSpecialInputListByEventIdAndsInputIdList(String eventId, List<Long> ids);
}
