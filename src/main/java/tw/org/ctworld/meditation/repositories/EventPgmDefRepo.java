/*
* Auto Generated
* Based on name: event_pgm_def
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.beans.EventPgmDef.EventPgmDef001Object;
import tw.org.ctworld.meditation.models.EventPgmDef;

import javax.transaction.Transactional;
import java.util.List;

public interface EventPgmDefRepo extends JpaRepository<EventPgmDef, Long> {

    @Query("select max(e.pgmId) from EventPgmDef e where e.pgmId like ?1")
    String GetMaxPgmId(String pgmCategoryId);

    @Query("select new tw.org.ctworld.meditation.beans.EventPgmDef.EventPgmDef001Object(" +
            "e.id, e.pgmCategoryId, e.pgmCategoryName, e.pgmId, e.pgmName, e.pgmDesc, e.isGenPgm, e.genPgmUiType, " +
            "e.genPgmInputOption, e.pgmStartTime, e.pgmEndTime, e.isCenterAvailable, e.pgmNote, e.updatorName, e.updateDtTm, e.updateDtTm) " +
            "from EventPgmDef e order by e.pgmId")
    List<EventPgmDef001Object> GetPgmList();

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventPgmDef e where e.id = ?1")
    void DeletePgmById(Long id);

    @Query("select e from EventPgmDef e where e.id = ?1")
    EventPgmDef GetPgmDefById(Long id);

    @Query("select e from EventPgmDef e where e.pgmId in ?1")
    List<EventPgmDef> GetEventPgmDefListByPgmIdList(List<String> pgmId);

    @Query("select e from EventPgmDef e where e.pgmId = ?1")
    EventPgmDef GetEventPgmDefByPgmId(String pgmId);
}
