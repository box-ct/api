package tw.org.ctworld.meditation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import tw.org.ctworld.meditation.models.EventEnrollPgm;

import javax.transaction.Transactional;
import java.util.List;

public interface EventEnrollPgmRepo extends JpaRepository<EventEnrollPgm, Long> {


    @Query("select new tw.org.ctworld.meditation.models.EventEnrollPgm(e.formAutoAddNum, e.isGenPgm, e.pgmCtOrderNum) " +
            "from EventEnrollPgm e where e.eventId = ?1")
    List<EventEnrollPgm> GetClassEventEnrollPgmListByEventId(String eventId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventEnrollPgm e where e.eventId = ?1 and e.unitId in ?2")
    void DeleteEventEnrollPgmByEventIdAndUnitIdList(String eventId, List<String> unitIdList);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventEnrollPgm e where e.eventId = ?1")
    void DeleteEventEnrollPgmByEventId(String eventId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("delete from EventEnrollPgm e where e.pgmUniqueId = ?1")
    void DeleteEventEnrollPgmByPgmUniqueId(String pgmUniqueId);

    @Query("select e from EventEnrollPgm e where e.pgmUniqueId = ?1")
    List<EventEnrollPgm> GetClassEventEnrollPgmListByPgmUniqueId(String pgmUniqueId);

    @Query("select e from EventEnrollPgm e where e.eventId = ?1 and e.formAutoAddNum = ?2")
    List<EventEnrollPgm> GetByEventIdFormAutoAddNum(String eventId, String formAutoAddNum);

    @Query("select e from EventEnrollPgm e where e.eventId = ?1 and e.formAutoAddNum in ?2")
    List<EventEnrollPgm> GetByEventIdFormAutoAddNum(String eventId, List<String> formAutoAddNum);

    @Query("select e from EventEnrollPgm e where e.pgmId = 'PGM10017' and e.eventId = ?1 and e.formAutoAddNum in ?2")
    List<EventEnrollPgm> GetTransByEventIdFormAutoAddNum(String eventId, List<String> formAutoAddNum);

    @Query("select e from EventEnrollPgm e where e.formAutoAddNum in ?1")
    List<EventEnrollPgm> GetByFormAutoAddNums(List<String> formAutoAddNum);
}
