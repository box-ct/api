package tw.org.ctworld.meditation.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import tw.org.ctworld.meditation.models.UserLoginLog;
import tw.org.ctworld.meditation.repositories.UserLoginLogRepo;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@WebListener
public class SessionDestroyListener implements HttpSessionListener {

    private static final Logger logger = LoggerFactory.getLogger(SessionDestroyListener.class);

    @Autowired
    private UserLoginLogRepo userLoginLogRepo;


    @Override
    public void sessionCreated(HttpSessionEvent se) {

        //logger.debug("Create");

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

        //logger.debug("Destroy");

        UserLoginLog userLoginLog = userLoginLogRepo.FindClassUserLoginLogBySessionId(se.getSession().getId());
        if (userLoginLog != null) {
            userLoginLog.setLogoutTime(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()));
            userLoginLog.setUpdater(se.getSession());
            userLoginLogRepo.save(userLoginLog);
        }
    }
}
