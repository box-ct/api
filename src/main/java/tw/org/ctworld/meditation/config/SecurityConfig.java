package tw.org.ctworld.meditation.config;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.models.UserInfo;
import tw.org.ctworld.meditation.models.UnitInfo;
import tw.org.ctworld.meditation.models.UserLoginLog;
import tw.org.ctworld.meditation.repositories.UnitInfoRepo;
import tw.org.ctworld.meditation.repositories.UserLoginLogRepo;
import tw.org.ctworld.meditation.services.UserInfoService;
import tw.org.ctworld.meditation.services.YubicoService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);

    @Value("${ad.domain}")
    private String AD_DOMAIN;

    @Value("${ad.url}")
    private String AD_URL;

    @Value("${server.servlet.context-path}")
    private String ServerContext;

    @Value("${yubico.switch}")
    private boolean ybSwitch;

    @Autowired
    private UserLoginLogRepo userLoginLogRepo;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UnitInfoRepo unitInfoRepo;

    @Autowired
    private YubicoService yubicoService;


    @Override
    public void configure(WebSecurity web) {
        //super.configure(web);
        web.ignoring().antMatchers(
                "/v2/**",
                "/swagger-resources/**",
                "/assets/**",
                "/api/sys/units",
                "/api/sys/unit_district",
                "/api/sys/unit_group",
                "/api/anonymous/**",
                "/api/sys/image/**",
                "/api/sys/test/**",
                "/api/video/stream/**",
                "/api/video/**",
                "/api/makeup/**", //dev
                "/api/public/**",
                "/**/*.html",
                "/**/*.css",
                "/**/*.js",
                "/**/*.jpg",
                "/**/*.ico",
                "/**/*.svg",
                "/changelog",
                "/");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf()
                .disable()
                .anonymous()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(new AuthenticationEntryPoint() {
                    @Override
                    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
                        String json = new Gson().toJson(new BaseResponse(401, "access denied"));
                        response.setContentType("application/json");
                        response.getWriter().write(json);
                        response.setStatus(401);
                    }
                })
                .and()
                .authorizeRequests()
                .antMatchers("/assets/**",
                        "/api/auth/**",
                        "/api/anonymous/**",
                        "/api/sys/image/**",
                        "/api/video/**",
                        "/api/makeup/**", // dev
                        "/api/public/**",
                        "/favicon.ico",
                        "/**/*.png",
                        "/**/*.gif",
                        "/**/*.svg",
                        "/**/*.jpg",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/api/auth/login")
                .permitAll()
                .successHandler(this::loginSuccessHandler)
                .failureHandler(this::loginFailureHandler)
                .and()
                .logout()
                .logoutUrl("/api/auth/logout")
                .permitAll()
                .invalidateHttpSession(false)
                .addLogoutHandler(this::logoutHandler)
                .logoutSuccessHandler(this::logoutSuccessHandler);


        http
                .sessionManagement()
                .invalidSessionStrategy(new InvalidSessionStrategy() {
                    @Override
                    public void onInvalidSessionDetected(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
                        String json = new Gson().toJson(new BaseResponse(400, "session invalided"));
                        response.setContentType("application/json");
                        response.getWriter().write(json);
                        response.setStatus(400);
                    }
                })
                .maximumSessions(10000)
                .expiredSessionStrategy(new SessionInformationExpiredStrategy() {
                    @Override
                    public void onExpiredSessionDetected(SessionInformationExpiredEvent event) throws IOException, ServletException {
                        HttpServletResponse response = event.getResponse();

                        String json = new Gson().toJson(new BaseResponse(400, "session expired"));
                        response.setContentType("application/json");
                        response.getWriter().write(json);
                        response.setStatus(400);
                    }
                })
                .sessionRegistry(sessionRegistry());

//        http
//                .headers()
//                .addHeaderWriter(new XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.SAMEORIGIN));

        http
                .headers().disable();
        http
                .headers().defaultsDisabled().cacheControl();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
        authManagerBuilder.authenticationProvider(activeDirectoryLdapAuthenticationProvider()).userDetailsService(userDetailsService());
    }

    @Bean
    public AuthenticationManager authenticationManager() {
        return new ProviderManager(Arrays.asList(activeDirectoryLdapAuthenticationProvider()));
    }

    @Bean
    public AuthenticationProvider activeDirectoryLdapAuthenticationProvider() {
        ActiveDirectoryLdapAuthenticationProvider provider = new ActiveDirectoryLdapAuthenticationProvider(AD_DOMAIN, AD_URL);
        provider.setConvertSubErrorCodesToExceptions(true);
        provider.setUseAuthenticationRequestCredentials(true);

        return provider;
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }


    // triggered when ad auth success
    private void loginSuccessHandler(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication) throws IOException {

        final String userId = request.getParameter("username");
        final String department = request.getParameter("department");
        final String unitGroup = request.getParameter("unitGroup");
        final String unitId = request.getParameter("unitId");
        final String yubiKeyId = request.getParameter("yubiKeyId");

        logger.debug("userId: " + userId);
        logger.debug("department: " + department);
        logger.debug("ip: " + request.getRemoteAddr());
        logger.debug("unit group: " + unitGroup);
        logger.debug("unit id: " + unitId);
        logger.debug("yubiKey id: " + yubiKeyId);

        // check user privilege then redirect to login with error=privilege or index

        UserInfo loginUserInfo = userInfoService.findLoginUserInfo(userId, unitId);
        
        if (loginUserInfo != null) {

            if (loginUserInfo.getIsDisabled()) {

//                response.sendError(402, "error");
                String json = new Gson().toJson(new BaseResponse(402, "account disabled"));
                response.setContentType("application/json");
                response.getWriter().write(json);
                response.setStatus(400);

                return;
            }


            UnitInfo unitInfo = unitInfoRepo.GetClassUnitInfoByUnitId(unitId);

            if (unitInfo == null || !loginUserInfo.getUnitId().equals(unitId)) {
                String json = new Gson().toJson(new BaseResponse(404, "unit not found"));
                response.setContentType("application/json");
                response.getWriter().write(json);
                response.setStatus(400);

                return;
            }

            // 是否需要檢查IP
            if (!unitInfo.getIsNoIpLimit()) {

                String ip[] = request.getRemoteAddr().split("\\.");
                String[] unitStartIp = unitInfo.getUnitStartIp().toString().split("\\.");
                String[] unitEndIp = unitInfo.getUnitEndIp().split("\\.");

                if ((unitStartIp[0] + unitStartIp[1] + unitStartIp[2]).equals(ip[0] + ip[1] + ip[2])) {

                    if (Integer.parseInt(unitStartIp[3]) > Integer.parseInt(ip[3]) ||
                            Integer.parseInt(unitEndIp[3]) < Integer.parseInt(ip[3])) {

                        // ip未通過
                        loginIpFailed(request, response);
                        return;
                    }
                }else {

                    // ip未通過
                    loginIpFailed(request, response);
                    return;
                }
            }

            // YubiKey 是否通過 isUse = 開關
            if (!checkYBKey(ybSwitch, yubiKeyId, loginUserInfo.getYubiKeyId())) {

                yubiKeyFailed(request, response);
                return;
            }

            // get login user info store login user info in session
            HttpSession session = request.getSession();
            session.setAttribute(UserSession.Keys.UnitId.getValue(), loginUserInfo.getUnitId());
            session.setAttribute(UserSession.Keys.UnitName.getValue(), loginUserInfo.getUnitName());
            session.setAttribute(UserSession.Keys.RoleType.getValue(), loginUserInfo.getRoleType());
            session.setAttribute(UserSession.Keys.UserId.getValue(), loginUserInfo.getUserId());
            session.setAttribute(UserSession.Keys.UserName.getValue(), loginUserInfo.getUserName());
            session.setAttribute(UserSession.Keys.UserJobTitle.getValue(), loginUserInfo.getUserJobTitle());
            session.setAttribute(UserSession.Keys.MobileNum.getValue(), loginUserInfo.getMobileNum());
            session.setAttribute(UserSession.Keys.IsDisabled.getValue(), loginUserInfo.getIsDisabled());
            session.setAttribute(UserSession.Keys.Group.getValue(), unitGroup);
            session.setAttribute(UserSession.Keys.UserIP.getValue(), request.getRemoteAddr());
            session.setAttribute(UserSession.Keys.LogInTime.getValue(), LocalDateTime.now());
            session.setAttribute(UserSession.Keys.LoginUserInfo.getValue(), loginUserInfo);
            session.setAttribute(UserSession.Keys.TimeZone.getValue(), unitInfo.getTimezone());
            session.setAttribute(UserSession.Keys.MemberId.getValue(), loginUserInfo.getMemberId());
            session.setAttribute(UserSession.Keys.XUserName.getValue(), null);


            UserLoginLog userLoginLog = new UserLoginLog(session);
            userLoginLog.setLoginTime(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()));
            userLoginLog.setCreatorAndUpdater(session);
            //print2JSON("classUserLoginLog",classUserLoginLog);
            userLoginLogRepo.save(userLoginLog);

            //response.sendRedirect(ServerContext + "/api/auth/loginSuccess");

            String json = new Gson().toJson(new BaseResponse(200, "login success"));
            response.setContentType("application/json");
            response.getWriter().write(json);
            response.setStatus(200);

        } else {
            // login user info not found
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            if (auth != null) {
                new SecurityContextLogoutHandler().logout(request, response, auth);
            }

//            request.getSession().setAttribute("error", "privilege");

            //response.sendRedirect(ServerContext + "/api/auth/loginFailed");

//            response.sendError(400, "error");
            String json = new Gson().toJson(new BaseResponse(400, "no privilege found"));
            response.setContentType("application/json");
            response.getWriter().write(json);
            response.setStatus(400);
        }
    }

    // YubiKey failed
    private void yubiKeyFailed(HttpServletRequest request,
                               HttpServletResponse response)  throws IOException {

        String json = new Gson().toJson(new BaseResponse(405, "Yubikey failed"));
        response.setContentType("application/json");
        response.getWriter().write(json);
        response.setStatus(400);
    }

    // login user info not found
    private void loginIpFailed(HttpServletRequest request,
                             HttpServletResponse response)  throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        request.getSession().setAttribute("error", "privilege");

        //response.sendRedirect(ServerContext + "/api/auth/loginIpFailed");

        String json = new Gson().toJson(new BaseResponse(403, "ip not found"));
        response.setContentType("application/json");
        response.getWriter().write(json);
        response.setStatus(400);
    }

    // triggered when ad auth failed
    private void loginFailureHandler(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException e) throws IOException {

        logger.debug("auth failed " + e.getMessage());

//        response.sendRedirect(ServerContext + "/api/auth/loginFailed");
        String json = new Gson().toJson(new BaseResponse(401, "username/password not found"));
        response.setContentType("application/json");
        response.getWriter().write(json);
        response.setStatus(400);
    }

    // triggered when ad logout success
    private void logoutSuccessHandler(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication) throws IOException {

        logger.debug("logout success");

//        request.getSession();

//        if (session != null){
//            logger.debug("TEST");
//            session.removeAttribute("user");
//        }

//        response.sendRedirect(ServerContext + "/api/auth/logoutSuccess");
        String json = new Gson().toJson(new BaseResponse(0, "success"));
        response.setContentType("application/json");
        response.getWriter().write(json);
        response.setStatus(200);
    }

    private void logoutHandler (
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication) {

        logger.debug("logoutHandler");

        HttpSession session = request.getSession();

        String sessionId = session.getId();

        UserLoginLog userLoginLog = userLoginLogRepo.FindClassUserLoginLogBySessionId(sessionId);
        if (userLoginLog != null) {
            userLoginLog.setLogoutTime(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()));
            userLoginLog.setUpdater(session);
            userLoginLogRepo.save(userLoginLog);
        }
    }

    private boolean checkYBKey(boolean isUse, String yubiKeyId, String myYubiKeyId) {

        yubiKeyId = yubiKeyId.toLowerCase();

        if (isUse) {

            if (yubiKeyId.length() < 44) {

                logger.debug("YBK : yubikey not ok(less than 44)");
                return false;
            }else {

                boolean ybServiceCheck = yubicoService.check(yubiKeyId);

                boolean ybSQLCheck = myYubiKeyId.equals(yubicoService.getIdentity(yubiKeyId));

                if (ybServiceCheck == true && ybSQLCheck == true) {

                    logger.debug("YBK : yubikey ok");
                    return true;
                }else {

                    if (ybServiceCheck != true) {

                        logger.debug("YBK : yubikey not ok(ybServiceCheck failed)");
                    }

                    if (ybSQLCheck != true) {

                        logger.debug("YBK : yubikey not ok(ybSQLCheck failed)");
                    }

                    return false;
                }
            }

        }else {

            logger.debug("YBK : yubikey not check");
            return true;
        }
    }

}
