package tw.org.ctworld.meditation.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

@Component
public class LoggedUser implements HttpSessionBindingListener {

    private static final Logger logger = LoggerFactory.getLogger(LoggedUser.class);

    @Override
    public void valueBound(HttpSessionBindingEvent event) {

        logger.debug("add user");

    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {

        logger.debug("remove user");

    }
}
