package tw.org.ctworld.meditation.config;

import com.fasterxml.classmate.TypeResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tw.org.ctworld.meditation.models.*;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    private static final Logger logger = LoggerFactory.getLogger(SwaggerConfig.class);

    @Autowired
    private TypeResolver typeResolver;

    @Bean
    public Docket api() {

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .additionalModels(typeResolver.resolve(AnnounceInfo.class))
                .additionalModels(typeResolver.resolve(AnnounceIsReadRecord.class))
                .additionalModels(typeResolver.resolve(AnnounceInfo.class))
                .additionalModels(typeResolver.resolve(AnnounceIsReadRecord.class))
                .additionalModels(typeResolver.resolve(ClassAttendMark.class))
                .additionalModels(typeResolver.resolve(ClassAttendRecord.class))
                .additionalModels(typeResolver.resolve(ClassCertFieldSetting.class))
                .additionalModels(typeResolver.resolve(ClassCertInfo.class))
                .additionalModels(typeResolver.resolve(CodeDef.class))
                .additionalModels(typeResolver.resolve(ClassCommonFilepath.class))
                .additionalModels(typeResolver.resolve(CtMemberInfo.class))
                .additionalModels(typeResolver.resolve(CtMemberNote.class))
                .additionalModels(typeResolver.resolve(ClassDateInfo.class))
                .additionalModels(typeResolver.resolve(ClassEnrollForm.class))
                .additionalModels(typeResolver.resolve(EventCtSpecialInput.class))
                .additionalModels(typeResolver.resolve(EventEnrollMain.class))
                .additionalModels(typeResolver.resolve(EventEnrollPgm.class))
                .additionalModels(typeResolver.resolve(EventMain.class))
                .additionalModels(typeResolver.resolve(EventPgmOca.class))
                .additionalModels(typeResolver.resolve(EventUnitInfo.class))
                .additionalModels(typeResolver.resolve(EventUnitSpecialInput.class))
                .additionalModels(typeResolver.resolve(EventUnitVehicleInfo.class))
                .additionalModels(typeResolver.resolve(ClassInfo.class))
                .additionalModels(typeResolver.resolve(UserInfo.class))
                .additionalModels(typeResolver.resolve(ClassMakeupRecord.class))
                .additionalModels(typeResolver.resolve(RecordImportLog.class))
                .additionalModels(typeResolver.resolve(UnitInfo.class))
                .additionalModels(typeResolver.resolve(ClassUnitMasterInfo.class))
                .additionalModels(typeResolver.resolve(ClassUnitPrinter.class))
                .additionalModels(typeResolver.resolve(UserLoginLog.class))
                .additionalModels(typeResolver.resolve(OldClassMonthStats.class))
                .additionalModels(typeResolver.resolve(SysCode.class))
                .additionalModels(typeResolver.resolve(UnitDsaMemberRecord.class))
                ;
    }
}
