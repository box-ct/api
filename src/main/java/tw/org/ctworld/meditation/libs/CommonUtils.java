package tw.org.ctworld.meditation.libs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tw.org.ctworld.meditation.beans.UserSession;

import javax.servlet.http.HttpSession;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.sql.Time;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.time.LocalDate;
import java.time.ZoneId;

public class CommonUtils {

    private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);

    /*
        return date apart from input value in day(s)
        before today with negative value ex. -1 = yesterday
        after today with positive value ex. 1 = tomorrow
     */
    public static Date GetDateFromToday(int diff) {
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, diff);
        return c.getTime();
    }

    public static void print2JSON(String funName, Object value) {
        ObjectMapper mapper = new ObjectMapper();
        logger.debug(funName);
        try {
            String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(value);
            System.out.println(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    // return null if empty
    public static String Empty2Null(String code) {
        if (code == null || code.isEmpty()) {
            return null;
        } else {
            return code;
        }
    }

    // return empty if null
    public static String Null2Empty(String code) {
        if (code == null) {
            return "";
        } else {
            return code;
        }
    }

    // copy image for assets
    public static void CopyImage(String fromFile, String toFile) {
        try {
            FileChannel src = new FileInputStream(fromFile).getChannel();
            FileChannel dest = new FileOutputStream(toFile).getChannel();
            dest.transferFrom(src, 0, src.size());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public static String TwIdMaker() {
        String checkHead = "ABCDEFGHJKLMNPQRSTUVWXYZIO";
        Random r = new Random();
        String s = "";
        int checknum = 0;    // 產生前9碼的同時計算產生驗證碼

        // 產生第一個英文字母
        int t = (r.nextInt(26) + 65);
        s += (char) t;
        t = checkHead.indexOf((char) t) + 10;
        checknum += t / 10;
        checknum += t % 10 * 9;

        // 產生第2個數字 (1~2)
        s += Integer.toString(t = r.nextInt(2) + 1);
        checknum += t * 8;

        // 產生後8碼
        for (int i = 2; i < 9; i++) {
            s += Integer.toString(t = r.nextInt(10));
            checknum += t * (9 - i);
        }

        // 完成驗證碼計算
        checknum = (10 - ((checknum) % 10)) % 10;
        s += Integer.toString(checknum);

        return s;
    }

    public static boolean TwIdChecker(String twpid) {
        Pattern TWPID_PATTERN = Pattern
                .compile("[ABCDEFGHJKLMNPQRSTUVXYWZIO][12]\\d{8}");
        boolean result = false;
        String pattern = "ABCDEFGHJKLMNPQRSTUVXYWZIO";

        try {
            if (TWPID_PATTERN.matcher(twpid.toUpperCase()).matches()) {
                int code = pattern.indexOf(twpid.toUpperCase().charAt(0)) + 10;
                int sum = 0;
                sum = (int) (code / 10) + 9 * (code % 10) + 8 * (twpid.charAt(1) - '0')
                        + 7 * (twpid.charAt(2) - '0') + 6 * (twpid.charAt(3) - '0')
                        + 5 * (twpid.charAt(4) - '0') + 4 * (twpid.charAt(5) - '0')
                        + 3 * (twpid.charAt(6) - '0') + 2 * (twpid.charAt(7) - '0')
                        + 1 * (twpid.charAt(8) - '0') + (twpid.charAt(9) - '0');
                if ( (sum % 10) == 0) {
                    result = true;
                }
            }
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public static String GetCurrentTimeString() {
        Date now = Calendar.getInstance().getTime();

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");

        return format.format(now);
    }

    public static int PercentageInt(int count, int total) {
        return (int) (count * 100.0f / total);
    }

    public static String Percentage(int count, int total) {
        return String.valueOf((int) (count * 100.0f / total));
    }

    public static boolean compare(String str1, String str2) {
        if (str1 != null && str1.isEmpty()) str1 = null;
        if (str2 != null && str2.isEmpty()) str2 = null;
        return (str1 == null ? str2 == null : str1.equals(str2));
    }

    public static String FormatBirthday(String birthday) {
        if (birthday == null || birthday.length() != 8) {
            birthday = "";
        } else {
            birthday = birthday.substring(0, 4) + "-" + birthday.substring(4, 6) + "-" + birthday.substring(6, 8);
        }

        return birthday;
    }

    static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    static final SimpleDateFormat dateTimeFormatShort = new SimpleDateFormat("yyyy-M-d H:m");
    static final SimpleDateFormat dateTimeCompactFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//    static final SimpleDateFormat dateFormatShort = new SimpleDateFormat("yyyy-M-d");
    static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
//    static final SimpleDateFormat timeFormatShort = new SimpleDateFormat("H:m");

    public static String DateTime2DateString(Date input) {
        if (input != null) {
            return dateFormat.format(input);
        } else {
            return "";
        }
    }

//    public static String DateTime2DateStringShort(Date input) {
//        if (input != null) {
//            return dateFormatShort.format(input);
//        } else {
//            return "";
//        }
//    }

    public static String DateTime2DateTimeCompactString(Date input) {
        if (input != null) {
            return dateTimeCompactFormat.format(input);
        } else {
            return "";
        }
    }

    public static String DateTime2TimeString(Date input) {
        if (input != null) {
            return timeFormat.format(input);
        } else {
            return "";
        }
    }

//    public static String DateTime2TimeStringShort(Date input) {
//        if (input != null) {
//            return timeFormatShort.format(input);
//        } else {
//            return "";
//        }
//    }

    public static String DateTime2DateTimeString(Date input) {
        if (input != null) {
            return dateTimeFormat.format(input);
        } else {
            return "";
        }
    }

//    public static String DateTime2DateTimeStringShort(Date input) {
//        if (input != null) {
//            return dateTimeFormatShort.format(input);
//        } else {
//            return "";
//        }
//    }

    public static Time ParseTime(String input) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            long ms = sdf.parse(input).getTime();
            return new Time(ms);
        } catch (ParseException p) {
//            logger.debug(p.getMessage() + ":" + input);
            return null;
        } catch (NullPointerException n) {
            return null;
        }
    }

//    public static Date ParseDate(String input, String timeZone) {
//        try {
//            return new SimpleDateFormat("yyyy-MM-dd X").parse(input + " " + timeZone);
//        } catch (ParseException p) {
//            logger.debug(p.getMessage() + ":" + input);
//            return null;
//        } catch (Exception e) {
//            return null;
//        }
//    }

    public static Date ParseDate(String input) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
            return sdf.parse(input);
        } catch (ParseException p) {
//            logger.debug(p.getMessage() + ":" + input);
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public static String FormatIntWithComma(long input) {
        DecimalFormat formatter = new DecimalFormat("#");
        formatter.setGroupingUsed(true);
        formatter.setGroupingSize(3);

        return formatter.format(input);
    }

    public static String FormatLong2DateTime(long input) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(input);

        return dateTimeFormat.format(calendar.getTime());
    }

    private static final String DAY_OF_WEEK_CHT = "日一二三四五六";

    public static int CStringDayOfWeek2Int(String input) {

        return DAY_OF_WEEK_CHT.indexOf(input) + 1;
    }

    public static String ParseDateTime2TimeZoneDT(HttpSession session, Date dateTime) {

        String mDateTime = "";

        if (dateTime != null) {

            int loginUnitTimeZone = Integer.parseInt(
                    session.getAttribute(UserSession.Keys.TimeZone.getValue()).toString()
                            .replace(".0", ""));

            String timeZone = String.valueOf(loginUnitTimeZone <= 0 ? loginUnitTimeZone : "+" + loginUnitTimeZone);

            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT" + timeZone));
            calendar.setTime(dateTime);

            mDateTime = calendar.get(Calendar.YEAR) + "-" +
                    ((calendar.get(Calendar.MONTH) + 1) < 10 ? (calendar.get(Calendar.MONTH) + 1) : (calendar.get(Calendar.MONTH) + 1)) + "-" +
                    (calendar.get(Calendar.DAY_OF_MONTH) < 10 ? calendar.get(Calendar.DAY_OF_MONTH) : calendar.get(Calendar.DAY_OF_MONTH)) + " " +
                    (calendar.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + calendar.get(Calendar.HOUR_OF_DAY) : calendar.get(Calendar.HOUR_OF_DAY)) + ":" +
                    (calendar.get(Calendar.MINUTE) < 10 ? "0" + calendar.get(Calendar.MINUTE) : calendar.get(Calendar.MINUTE)) + ":" +
                    (calendar.get(Calendar.SECOND) < 10 ? "0" + calendar.get(Calendar.SECOND) : calendar.get(Calendar.SECOND));
        }

        return mDateTime;
    }

    public static String GetMinGoYearStr() {
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int minGoYear = localDate.getYear() - 1911;

        return String.valueOf(minGoYear);
    }

    public static String GetMinGoDateStr() {
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int minGoYear = localDate.getYear() - 1911;
        return String.format("%03d", minGoYear) + String.format("%02d", localDate.getMonthValue()) + String.format("%02d", localDate.getDayOfMonth());
    }

    public static Integer String2Integer(String input) {
        Integer result = null;
        try {
            result = Integer.parseInt(input);
        } catch (NumberFormatException e) {

        }

        return result;
    }
}
