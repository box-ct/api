package tw.org.ctworld.meditation.beans.Classes;

public class Classes006Object {

    private String classEnrollFormId, classId, memberId, gender, aliasName, ctDharmaName, classPeriodNum, className, classGroupId, memberGroupNum,
            classJobTitleList;
    private boolean isNotComeBack, isCompleteNextNewClassEnroll, isEnrollToNewUpperClass, isFullAttended, isGraduated;
    private String previousClassName, previousClassGroupName, nextNewClassName, nextNewClassNote;
    private int age;
    private String mobileNum1;
    private boolean isAttend;

    public Classes006Object(String classEnrollFormId, String classId, String memberId, String gender, String aliasName,
                            String ctDharmaName, String classPeriodNum, String className, String classGroupId,
                            String memberGroupNum, String classJobTitleList, Boolean isNotComeBack,
                            Boolean isCompleteNextNewClassEnroll, Boolean isEnrollToNewUpperClass,
                            Boolean isFullAttended, Boolean isGraduated, String previousClassName,
                            String previousClassGroupName, String nextNewClassName, String nextNewClassNote, Integer age,
                            String mobileNum1, Boolean isAttend) {
        this.classEnrollFormId = classEnrollFormId;
        this.classId = classId;
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classJobTitleList = classJobTitleList;
        this.isNotComeBack = isNotComeBack != null ? isNotComeBack : false;
        this.isCompleteNextNewClassEnroll = isCompleteNextNewClassEnroll != null ? isCompleteNextNewClassEnroll : false;
        this.isEnrollToNewUpperClass = isEnrollToNewUpperClass != null ? isEnrollToNewUpperClass : false;
        this.isFullAttended = isFullAttended != null ? isFullAttended : false;
        this.isGraduated = isGraduated != null ? isGraduated : false;
        this.previousClassName = previousClassName;
        this.previousClassGroupName = previousClassGroupName;
        this.nextNewClassName = nextNewClassName;
        this.nextNewClassNote = nextNewClassNote;
        this.age = age != null ? age : 0;
        this.mobileNum1 = mobileNum1;
        this.isAttend = isAttend != null ? isAttend : false;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public boolean getIsNotComeBack() {
        return isNotComeBack;
    }

    public void setIsNotComeBack(boolean notComeBack) {
        isNotComeBack = notComeBack;
    }

    public boolean getIsCompleteNextNewClassEnroll() {
        return isCompleteNextNewClassEnroll;
    }

    public void setIsCompleteNextNewClassEnroll(boolean completeNextNewClassEnroll) {
        isCompleteNextNewClassEnroll = completeNextNewClassEnroll;
    }

    public boolean getIsEnrollToNewUpperClass() {
        return isEnrollToNewUpperClass;
    }

    public void setIsEnrollToNewUpperClass(boolean enrollToNewUpperClass) {
        isEnrollToNewUpperClass = enrollToNewUpperClass;
    }

    public boolean getIsFullAttended() {
        return isFullAttended;
    }

    public void setIsFullAttended(boolean fullAttended) {
        isFullAttended = fullAttended;
    }

    public boolean getIsGraduated() {
        return isGraduated;
    }

    public void setIsGraduated(boolean graduated) {
        isGraduated = graduated;
    }

    public String getPreviousClassName() {
        return previousClassName;
    }

    public void setPreviousClassName(String previousClassName) {
        this.previousClassName = previousClassName;
    }

    public String getPreviousClassGroupName() {
        return previousClassGroupName;
    }

    public void setPreviousClassGroupName(String previousClassGroupName) {
        this.previousClassGroupName = previousClassGroupName;
    }

    public String getNextNewClassName() {
        return nextNewClassName;
    }

    public void setNextNewClassName(String nextNewClassName) {
        this.nextNewClassName = nextNewClassName;
    }

    public String getNextNewClassNote() {
        return nextNewClassNote;
    }

    public void setNextNewClassNote(String nextNewClassNote) {
        this.nextNewClassNote = nextNewClassNote;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public boolean getIsAttend() {
        return isAttend;
    }

    public void setIsAttend(boolean attend) {
        isAttend = attend;
    }
}
