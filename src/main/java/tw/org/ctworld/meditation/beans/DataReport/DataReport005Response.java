package tw.org.ctworld.meditation.beans.DataReport;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class DataReport005Response extends BaseResponse {

    private List<DataReport005_1Object> items;

    public DataReport005Response(int errCode, String errMsg, List<DataReport005_1Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<DataReport005_1Object> getItems() {
        return items;
    }

    public void setItems(List<DataReport005_1Object> items) {
        this.items = items;
    }


    public static class DataReport005_1Object {

        private String unitName, classPeriodNum, className;
        private List<DataReport005_2Object> classDates;

        public DataReport005_1Object(String unitName, String classPeriodNum, String className, List<DataReport005_2Object> classDates) {
            this.unitName = unitName;
            this.classPeriodNum = classPeriodNum;
            this.className = className;
            this.classDates = classDates;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }

        public String getClassPeriodNum() {
            return classPeriodNum;
        }

        public void setClassPeriodNum(String classPeriodNum) {
            this.classPeriodNum = classPeriodNum;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public List<DataReport005_2Object> getClassDates() {
            return classDates;
        }

        public void setClassDates(List<DataReport005_2Object> classDates) {
            this.classDates = classDates;
        }
    }

    public static class DataReport005_2Object {

        private String classDate, number;

        public DataReport005_2Object(String classDate, String number) {
            this.classDate = classDate;
            this.number = number;
        }

        public String getClassDate() {
            return classDate;
        }

        public void setClassDate(String classDate) {
            this.classDate = classDate;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }
    }
}
