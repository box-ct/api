package tw.org.ctworld.meditation.beans;

public class DebugResponse extends BaseResponse {
    private String errLog;

    public DebugResponse(int errCode, String errMsg, String errLog) {
        super(errCode, errMsg);
        this.errLog = errLog;
    }

    public String getErrLog() {
        return errLog;
    }

    public void setErrLog(String errLog) {
        this.errLog = errLog;
    }
}
