package tw.org.ctworld.meditation.beans.ClassReport;

public class ClassAttendance {
    private String classWeeksNum;
    private String classDate;
    private String attendMark;

    public ClassAttendance() {
    }

    public ClassAttendance(String classWeeksNum, String classDate, String attendMark) {
        this.classWeeksNum = classWeeksNum;
        this.classDate = classDate;
        this.attendMark = attendMark;
    }

    public String getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(String classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public String getClassDate() {
        return classDate;
    }

    public void setClassDate(String classDate) {
        this.classDate = classDate;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }
}
