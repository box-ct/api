package tw.org.ctworld.meditation.beans.MemberImport;

public class MemberEnrollInfo {
    private String memberId;
    private String classId;
    private String className;
    private Integer classTypeNum;
    private String classTypeName;
    private String classJobTitleList;
    private String homeClassNote;

    public MemberEnrollInfo(String memberId, String classId, String className, Integer classTypeNum, String classTypeName, String classJobTitleList, String homeClassNote) {
        this.memberId = memberId;
        this.classId = classId;
        this.className = className;
        this.classTypeNum = classTypeNum;
        this.classTypeName = classTypeName;
        this.classJobTitleList = classJobTitleList;
        this.homeClassNote = homeClassNote;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getClassTypeNum() {
        return classTypeNum;
    }

    public void setClassTypeNum(Integer classTypeNum) {
        this.classTypeNum = classTypeNum;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public String getHomeClassNote() {
        return homeClassNote;
    }

    public void setHomeClassNote(String homeClassNote) {
        this.homeClassNote = homeClassNote;
    }
}
