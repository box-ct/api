package tw.org.ctworld.meditation.beans.ClassRecord;

import java.util.List;

public class ClassRecord008Request {

    private List<ClassRecord006Object> markList;

    public List<ClassRecord006Object> getMarkList() {
        return markList;
    }

    public void setMarkList(List<ClassRecord006Object> markList) {
        this.markList = markList;
    }


    public static class ClassRecord006Object {

        private String attendMark;
        private boolean isFullAttended, isGraduated, isDiligentAward, isMakeupClass;

        public String getAttendMark() {
            return attendMark;
        }

        public void setAttendMark(String attendMark) {
            this.attendMark = attendMark;
        }

        public boolean getIsFullAttended() {
            return isFullAttended;
        }

        public void setIsFullAttended(boolean fullAttended) {
            isFullAttended = fullAttended;
        }

        public boolean getIsGraduated() {
            return isGraduated;
        }

        public void setIsGraduated(boolean graduated) {
            isGraduated = graduated;
        }

        public boolean getIsDiligentAward() {
            return isDiligentAward;
        }

        public void setIsDiligentAward(boolean diligentAward) {
            isDiligentAward = diligentAward;
        }

        public boolean getIsMakeupClass() {
            return isMakeupClass;
        }

        public void setIsMakeupClass(boolean makeupClass) {
            isMakeupClass = makeupClass;
        }
    }
}
