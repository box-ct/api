package tw.org.ctworld.meditation.beans.ClassGroup;

import tw.org.ctworld.meditation.libs.CommonUtils;

public class ClassGroup002Object {

    private String aliasName, ctDharmaName, gender, className, classPeriodNum, classGroupId, memberGroupNum,
            dsaJobNameList, memberId, classEnrollFormId;

    public ClassGroup002Object(String aliasName, String ctDharmaName, String gender, String className,
                               String classPeriodNum, String classGroupId, String memberGroupNum, String dsaJobNameList,
                               String memberId, String classEnrollFormId) {
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.className = className;
        this.classPeriodNum = classPeriodNum;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.dsaJobNameList = dsaJobNameList;
        this.memberId = memberId;
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return CommonUtils.Null2Empty(ctDharmaName);
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassGroupId() {
        return CommonUtils.Null2Empty(classGroupId);
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public String getDsaJobNameList() {
        return CommonUtils.Null2Empty(dsaJobNameList);
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }
}
