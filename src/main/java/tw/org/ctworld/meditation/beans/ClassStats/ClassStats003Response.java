package tw.org.ctworld.meditation.beans.ClassStats;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class ClassStats003Response extends BaseResponse {

    private List<ClassStats003Object> items;

    public ClassStats003Response(int errCode, String errMsg, List<ClassStats003Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassStats003Object> getItems() {
        return items;
    }

    public void setItems(List<ClassStats003Object> items) {
        this.items = items;
    }


    public static class ClassStats003Object {

        private String classEnrollFormId, memberId, aliasName, ctDharmaName, gender, classPeriodNum, className, classGroupId,
                memberGroupNum, classJobTitleList;
        private int weekTotal, week;
        private boolean calcFullAttended, isFullAttended, calcGraduated, isGraduated, isMakeupToFullAttended;
        private String alertFullAttended, alertGraduated;
        private boolean isDiligentAward;
        private String calcClassAttendedResult, classAttendedResult;
        private int age;

        public ClassStats003Object(String classEnrollFormId, String memberId, String aliasName, String ctDharmaName,
                                   String gender,
                                   String classPeriodNum, String className, String classGroupId, String memberGroupNum,
                                   String classJobTitleList, int weekTotal, int week, boolean calcFullAttended,
                                   boolean isFullAttended, boolean calcGraduated, boolean isGraduated,
                                   boolean isMakeupToFullAttended, String alertFullAttended, String alertGraduated,
                                   boolean isDiligentAward, String calcClassAttendedResult, String classAttendedResult,
                                   int age) {
            this.classEnrollFormId = classEnrollFormId;
            this.memberId = memberId;
            this.aliasName = aliasName;
            this.ctDharmaName = ctDharmaName;
            this.gender = gender;
            this.classPeriodNum = classPeriodNum;
            this.className = className;
            this.classGroupId = classGroupId;
            this.memberGroupNum = memberGroupNum;
            this.classJobTitleList = classJobTitleList;
            this.weekTotal = weekTotal;
            this.week = week;
            this.calcFullAttended = calcFullAttended;
            this.isFullAttended = isFullAttended;
            this.calcGraduated = calcGraduated;
            this.isGraduated = isGraduated;
            this.isMakeupToFullAttended = isMakeupToFullAttended;
            this.alertFullAttended = alertFullAttended;
            this.alertGraduated = alertGraduated;
            this.isDiligentAward = isDiligentAward;
            this.calcClassAttendedResult = calcClassAttendedResult;
            this.classAttendedResult = classAttendedResult;
            this.age = age;
        }

        public String getClassEnrollFormId() {
            return classEnrollFormId;
        }

        public void setClassEnrollFormId(String classEnrollFormId) {
            this.classEnrollFormId = classEnrollFormId;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getAliasName() {
            return aliasName;
        }

        public void setAliasName(String aliasName) {
            this.aliasName = aliasName;
        }

        public String getCtDharmaName() {
            return ctDharmaName;
        }

        public void setCtDharmaName(String ctDharmaName) {
            this.ctDharmaName = ctDharmaName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getClassPeriodNum() {
            return classPeriodNum;
        }

        public void setClassPeriodNum(String classPeriodNum) {
            this.classPeriodNum = classPeriodNum;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(String classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getMemberGroupNum() {
            return memberGroupNum;
        }

        public void setMemberGroupNum(String memberGroupNum) {
            this.memberGroupNum = memberGroupNum;
        }

        public String getClassJobTitleList() {
            return classJobTitleList;
        }

        public void setClassJobTitleList(String classJobTitleList) {
            this.classJobTitleList = classJobTitleList;
        }

        public int getWeekTotal() {
            return weekTotal;
        }

        public void setWeekTotal(int weekTotal) {
            this.weekTotal = weekTotal;
        }

        public int getWeek() {
            return week;
        }

        public void setWeek(int week) {
            this.week = week;
        }

        public boolean isCalcFullAttended() {
            return calcFullAttended;
        }

        public void setCalcFullAttended(boolean calcFullAttended) {
            this.calcFullAttended = calcFullAttended;
        }

        public boolean getIsFullAttended() {
            return isFullAttended;
        }

        public void setIsFullAttended(boolean fullAttended) {
            isFullAttended = fullAttended;
        }

        public boolean isCalcGraduated() {
            return calcGraduated;
        }

        public void setCalcGraduated(boolean calcGraduated) {
            this.calcGraduated = calcGraduated;
        }

        public boolean getIsGraduated() {
            return isGraduated;
        }

        public void setIsGraduated(boolean graduated) {
            isGraduated = graduated;
        }

        public boolean getIsMakeupToFullAttended() {
            return isMakeupToFullAttended;
        }

        public void setIsMakeupToFullAttended(boolean makeupToFullAttended) {
            isMakeupToFullAttended = makeupToFullAttended;
        }

        public String getAlertFullAttended() {
            return alertFullAttended;
        }

        public void setAlertFullAttended(String alertFullAttended) {
            this.alertFullAttended = alertFullAttended;
        }

        public String getAlertGraduated() {
            return alertGraduated;
        }

        public void setAlertGraduated(String alertGraduated) {
            this.alertGraduated = alertGraduated;
        }

        public boolean getIsDiligentAward() {
            return isDiligentAward;
        }

        public void setIsDiligentAward(boolean diligentAward) {
            isDiligentAward = diligentAward;
        }

        public String getCalcClassAttendedResult() {
            return calcClassAttendedResult;
        }

        public void setCalcClassAttendedResult(String calcClassAttendedResult) {
            this.calcClassAttendedResult = calcClassAttendedResult;
        }

        public String getClassAttendedResult() {
            return classAttendedResult;
        }

        public void setClassAttendedResult(String classAttendedResult) {
            this.classAttendedResult = classAttendedResult;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }
}
