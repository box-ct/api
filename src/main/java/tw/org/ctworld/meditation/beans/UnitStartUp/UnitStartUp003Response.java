package tw.org.ctworld.meditation.beans.UnitStartUp;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class UnitStartUp003Response extends BaseResponse {

    private UnitStartUp003_1Object eventMain;
    private UnitStartUp003_2Object eventUnitInfo;

    public UnitStartUp003Response(int errCode, String errMsg, UnitStartUp003_1Object eventMain,
                                  UnitStartUp003_2Object eventUnitInfo) {
        super(errCode, errMsg);
        this.eventMain = eventMain;
        this.eventUnitInfo = eventUnitInfo;
    }

    public UnitStartUp003_1Object getEventMain() {
        return eventMain;
    }

    public void setEventMain(UnitStartUp003_1Object eventMain) {
        this.eventMain = eventMain;
    }

    public UnitStartUp003_2Object getEventUnitInfo() {
        return eventUnitInfo;
    }

    public void setEventUnitInfo(UnitStartUp003_2Object eventUnitInfo) {
        this.eventUnitInfo = eventUnitInfo;
    }
}
