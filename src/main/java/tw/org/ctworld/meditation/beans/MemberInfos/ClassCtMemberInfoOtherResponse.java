package tw.org.ctworld.meditation.beans.MemberInfos;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.Member.Members002ImagesObject;
import tw.org.ctworld.meditation.beans.Member.Members002MeditationRecordObject;
import tw.org.ctworld.meditation.beans.Member.Members002RegistrationRecordObject;

import java.util.List;

public class ClassCtMemberInfoOtherResponse extends BaseResponse {
    private List<DuplicateMember> duplicateNameList;
    private List<DuplicateMember> duplicateAddressList;
    private List<DuplicateMember> duplicateFamilyList;
    private List<Members002MeditationRecordObject> enrollFormList;
    private List<EventItem> eventList;
    private List<Members002RegistrationRecordObject> barCodeList;
    private List<DsaMember> dsaMemberList;
    private Members002ImagesObject images;


    public List<DuplicateMember> getDuplicateNameList() {
        return duplicateNameList;
    }

    public void setDuplicateNameList(List<DuplicateMember> duplicateNameList) {
        this.duplicateNameList = duplicateNameList;
    }

    public List<DuplicateMember> getDuplicateAddressList() {
        return duplicateAddressList;
    }

    public void setDuplicateAddressList(List<DuplicateMember> duplicateAddressList) {
        this.duplicateAddressList = duplicateAddressList;
    }

    public List<DuplicateMember> getDuplicateFamilyList() {
        return duplicateFamilyList;
    }

    public void setDuplicateFamilyList(List<DuplicateMember> duplicateFamilyList) {
        this.duplicateFamilyList = duplicateFamilyList;
    }

    public List<Members002MeditationRecordObject> getEnrollFormList() {
        return enrollFormList;
    }

    public void setEnrollFormList(List<Members002MeditationRecordObject> enrollFormList) {
        this.enrollFormList = enrollFormList;
    }

    public List<EventItem> getEventList() {
        return eventList;
    }

    public void setEventList(List<EventItem> eventList) {
        this.eventList = eventList;
    }

    public List<Members002RegistrationRecordObject> getBarCodeList() {
        return barCodeList;
    }

    public void setBarCodeList(List<Members002RegistrationRecordObject> barCodeList) {
        this.barCodeList = barCodeList;
    }

    public List<DsaMember> getDsaMemberList() {
        return dsaMemberList;
    }

    public void setDsaMemberList(List<DsaMember> dsaMemberList) {
        this.dsaMemberList = dsaMemberList;
    }

    public Members002ImagesObject getImages() {
        return images;
    }

    public void setImages(Members002ImagesObject images) {
        this.images = images;
    }
}
