package tw.org.ctworld.meditation.beans.EventEnrollMember;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import java.util.Date;

public class MemberInfoNotEnrolled {

    private String aliasName;
    private String carLicenseNum1;
    private String classList;
    private String classNameAndJobList;
    private String ctDharmaName;
    private String donationNameList;
    private String dsaJobNameList;
    private String familyLeaderName;
    private String gender;
    private String healthList;
    private String homeClassNote;
    private String memberId;
    private String mobileNum1;
    private String passportNum;
    private String photo;
    private String relativeMeritList;
    private String twIdNum;
    private String unitId;
    private String unitName;
    private String updatorName;
    private String urgentContactPersonName1;
    private String urgentContactPersonPhoneNum1;
    private String urgentContactPersonRelationship1;
    private Boolean isThisYearSummerDonation;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date birthDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Taipei")
    private Date updateDtTm;
    private Integer age;

    public MemberInfoNotEnrolled() {
    }

    public MemberInfoNotEnrolled(String aliasName,
                                 String classList,
                                 String ctDharmaName,
                                 String donationNameList,
                                 String dsaJobNameList,
                                 String healthList,
                                 String memberId,
                                 String twIdNum,
                                 String updatorName,
                                 String urgentContactPersonName1,
                                 String urgentContactPersonPhoneNum1,
                                 String urgentContactPersonRelationship1,
                                 Date birthDate,
                                 Date updateDtTm,
                                 Integer age,
                                 String familyLeaderName,
                                 String unitId,
                                 String unitName,
                                 String gender,
                                 String passportNum) {
        this.aliasName = aliasName;
        this.classList = classList;
        this.ctDharmaName = ctDharmaName;
        this.donationNameList = donationNameList;
        this.dsaJobNameList = dsaJobNameList;
        this.healthList = healthList;
        this.memberId = memberId;
        this.twIdNum = twIdNum;
        this.updatorName = updatorName;
        this.urgentContactPersonName1 = urgentContactPersonName1;
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
        this.birthDate = birthDate;
        this.updateDtTm = updateDtTm;
        this.age = age != null ? age : 0;
        this.familyLeaderName = familyLeaderName;
        this.unitId = unitId;
        this.unitName = unitName;
        this.gender = gender;
        this.passportNum = passportNum;
    }

    public MemberInfoNotEnrolled(String aliasName,
                                 String classList,
                                 String ctDharmaName,
                                 String donationNameList,
                                 String healthList,
                                 String memberId,
                                 String twIdNum,
                                 String urgentContactPersonName1,
                                 String urgentContactPersonPhoneNum1,
                                 String urgentContactPersonRelationship1,
                                 Date birthDate,
                                 Integer age,
                                 String carLicenseNum1,
                                 String passportNum,
                                 String relativeMeritList,
                                 String gender,
                                 String mobileNum1) {
        this.aliasName = aliasName;
        this.classList = classList;
        this.ctDharmaName = ctDharmaName;
        this.donationNameList = donationNameList;
        this.healthList = healthList;
        this.memberId = memberId;
        this.twIdNum = twIdNum;
        this.urgentContactPersonName1 = urgentContactPersonName1;
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
        this.birthDate = birthDate;
        this.age = age;
        this.carLicenseNum1 = carLicenseNum1;
        this.passportNum = passportNum;
        this.relativeMeritList = relativeMeritList;
        this.gender = gender;
        this.mobileNum1 = mobileNum1;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getClassList() {
        return classList;
    }

    public void setClassList(String classList) {
        this.classList = classList;
    }

    public String getClassNameAndJobList() {
        return classNameAndJobList;
    }

    public void setClassNameAndJobList(String classNameAndJobList) {
        this.classNameAndJobList = classNameAndJobList;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getDonationNameList() {
        return donationNameList;
    }

    public void setDonationNameList(String donationNameList) {
        this.donationNameList = donationNameList;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getHealthList() {
        return healthList;
    }

    public void setHealthList(String healthList) {
        this.healthList = healthList;
    }

    public String getHomeClassNote() {
        return homeClassNote;
    }

    public void setHomeClassNote(String homeClassNote) {
        this.homeClassNote = homeClassNote;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

    public String getUrgentContactPersonName1() {
        return urgentContactPersonName1;
    }

    public void setUrgentContactPersonName1(String urgentContactPersonName1) {
        this.urgentContactPersonName1 = urgentContactPersonName1;
    }

    public String getUrgentContactPersonPhoneNum1() {
        return urgentContactPersonPhoneNum1;
    }

    public void setUrgentContactPersonPhoneNum1(String urgentContactPersonPhoneNum1) {
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
    }

    public String getUrgentContactPersonRelationship1() {
        return urgentContactPersonRelationship1;
    }

    public void setUrgentContactPersonRelationship1(String urgentContactPersonRelationship1) {
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
    }

    public Boolean getIsThisYearSummerDonation() {
        return isThisYearSummerDonation;
    }

    public void setIsThisYearSummerDonation(Boolean thisYearSummerDonation) {
        isThisYearSummerDonation = thisYearSummerDonation;
    }

    public Date getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(Date updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCarLicenseNum1() {
        return carLicenseNum1;
    }

    public void setCarLicenseNum1(String carLicenseNum1) {
        this.carLicenseNum1 = carLicenseNum1;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public String getRelativeMeritList() {
        return relativeMeritList;
    }

    public void setRelativeMeritList(String relativeMeritList) {
        this.relativeMeritList = relativeMeritList;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFamilyLeaderName() {
        return familyLeaderName;
    }

    public void setFamilyLeaderName(String familyLeaderName) {
        this.familyLeaderName = familyLeaderName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }
}
