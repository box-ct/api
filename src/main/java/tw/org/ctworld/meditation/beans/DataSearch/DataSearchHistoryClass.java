package tw.org.ctworld.meditation.beans.DataSearch;

public class DataSearchHistoryClass {
    private String className;
    private int number;

    public DataSearchHistoryClass() {
    }

    public DataSearchHistoryClass(String className, Long number) {
        this.className = className;
        this.number = number.intValue();
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
