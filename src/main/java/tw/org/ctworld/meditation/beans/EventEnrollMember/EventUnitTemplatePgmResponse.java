package tw.org.ctworld.meditation.beans.EventEnrollMember;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.EventEnrollMain;
import tw.org.ctworld.meditation.models.EventUnitTemplate;

import java.util.List;

public class EventUnitTemplatePgmResponse extends BaseResponse {

    private EventUnitTemplate result;
    private List<EventPgmShort> genPgmList;
    private List<EventPgmShort> pgmList;

    public EventUnitTemplate getResult() {
        return result;
    }

    public void setResult(EventUnitTemplate result) {
        this.result = result;
    }

    public List<EventPgmShort> getGenPgmList() {
        return genPgmList;
    }

    public void setGenPgmList(List<EventPgmShort> genPgmList) {
        this.genPgmList = genPgmList;
    }

    public List<EventPgmShort> getPgmList() {
        return pgmList;
    }

    public void setPgmList(List<EventPgmShort> pgmList) {
        this.pgmList = pgmList;
    }
}
