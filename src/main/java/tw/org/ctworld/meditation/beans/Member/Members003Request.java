package tw.org.ctworld.meditation.beans.Member;

public class Members003Request {

    private String classEnrollFormId, classId, classGroupId, sameGroupMemberName, currentClassIntroducerName,
    currentClassIntroducerRelationship, childContactPersonName, childContactPersonPhoneNum;

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getSameGroupMemberName() {
        return sameGroupMemberName;
    }

    public void setSameGroupMemberName(String sameGroupMemberName) {
        this.sameGroupMemberName = sameGroupMemberName;
    }

    public String getCurrentClassIntroducerName() {
        return currentClassIntroducerName;
    }

    public void setCurrentClassIntroducerName(String currentClassIntroducerName) {
        this.currentClassIntroducerName = currentClassIntroducerName;
    }

    public String getCurrentClassIntroducerRelationship() {
        return currentClassIntroducerRelationship;
    }

    public void setCurrentClassIntroducerRelationship(String currentClassIntroducerRelationship) {
        this.currentClassIntroducerRelationship = currentClassIntroducerRelationship;
    }

    public String getChildContactPersonName() {
        return childContactPersonName;
    }

    public void setChildContactPersonName(String childContactPersonName) {
        this.childContactPersonName = childContactPersonName;
    }

    public String getChildContactPersonPhoneNum() {
        return childContactPersonPhoneNum;
    }

    public void setChildContactPersonPhoneNum(String childContactPersonPhoneNum) {
        this.childContactPersonPhoneNum = childContactPersonPhoneNum;
    }
}
