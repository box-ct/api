package tw.org.ctworld.meditation.beans.UnitMaster;

public class MasterInfo {
    private String masterId;
    private String masterName;
    private String masterPreceptTypeName;
    private boolean isAbbot;
    private boolean isTreasurer;
    private String jobTitle;
    private String mobileNum;
    private String preceptOrder;
    private String jobOrder;

    public MasterInfo(){}

    public MasterInfo(String masterId, String masterName, String masterPreceptTypeName, boolean isAbbot,
                      boolean isTreasurer, String jobTitle, String mobileNum, String preceptOrder, String jobOrder) {
        this.masterId = masterId;
        this.masterName = masterName;
        this.masterPreceptTypeName = masterPreceptTypeName;
        this.isAbbot = isAbbot;
        this.isTreasurer = isTreasurer;
        this.jobTitle = jobTitle;
        this.mobileNum = mobileNum;
        this.preceptOrder = preceptOrder;
        this.jobOrder = jobOrder;
    }


    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getMasterPreceptTypeName() {
        return masterPreceptTypeName;
    }

    public void setMasterPreceptTypeName(String masterPreceptTypeName) {
        this.masterPreceptTypeName = masterPreceptTypeName;
    }

    public boolean getIsAbbot() {
        return isAbbot;
    }

    public void setIsAbbot(boolean abbot) {
        isAbbot = abbot;
    }

    public boolean getIsTreasurer() {
        return isTreasurer;
    }

    public void setIsTreasurer(boolean treasurer) {
        isTreasurer = treasurer;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getPreceptOrder() {
        return preceptOrder;
    }

    public void setPreceptOrder(String preceptOrder) {
        this.preceptOrder = preceptOrder;
    }

    public String getJobOrder() {
        return jobOrder;
    }

    public void setJobOrder(String jobOrder) {
        this.jobOrder = jobOrder;
    }
}
