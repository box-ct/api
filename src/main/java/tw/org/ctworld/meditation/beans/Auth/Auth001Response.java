package tw.org.ctworld.meditation.beans.Auth;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.UnitInfo;

public class Auth001Response extends BaseResponse {

    private String userName, userId;
    private UnitInfo unit;
    private String ip;
    private Auth001Object role;
    private boolean isProxy;
    private Auth001Object2 proxy;

    public Auth001Response(int errCode, String errMsg, String userName, String userId, UnitInfo unit, String ip,
                           Auth001Object role, boolean isProxy, Auth001Object2 proxy) {
        super(errCode, errMsg);
        this.userName = userName;
        this.userId = userId;
        this.unit = unit;
        this.ip = ip;
        this.role = role;
        this.isProxy = isProxy;
        this.proxy = proxy;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UnitInfo getUnit() {
        return unit;
    }

    public void setUnit(UnitInfo unit) {
        this.unit = unit;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Auth001Object getRole() {
        return role;
    }

    public void setRole(Auth001Object role) {
        this.role = role;
    }

    public boolean getIsProxy() {
        return isProxy;
    }

    public void setIsProxy(boolean proxy) {
        isProxy = proxy;
    }

    public Auth001Object2 getProxy() {
        return proxy;
    }

    public void setProxy(Auth001Object2 proxy) {
        this.proxy = proxy;
    }


    public static class Unit {

        private String unitId, unitName;

        public Unit(String unitId, String unitName) {
            this.unitId = unitId;
            this.unitName = unitName;
        }

        public String getUnitId() {
            return unitId;
        }

        public void setUnitId(String unitId) {
            this.unitId = unitId;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }
    }

    public static class Auth001Object {

        private String roleType;
        private Boolean isSeeMemberData, isSeeCreateClass, isSeeBarcodeCheckIn, isSeeClassAttendRecord,
                isSeeClassGraduateStats, isSeeClassUpGrade, isSeePrintReport, isSeeHomeClass, isSeeIDCheck,
                isSeeCurrentClassMembers, isSeePreviousClassMembers, isSeeCurrentClassLeaders,
                isSeeMultpleClassMembersAndLeaders, isSeeEventRecord, isSeeClassStats, isSeeClassCurrentCheckInStats,
                isSeeClassPeriodCheckInStats, isSeeClassStatsByClassName, isSeeClassStatsWithLinechart, isSeeOnlineUser,
                isSeeNews, isSeeAuthorize, isSeeUnitInfo, isSeeImportClassRecord, isClassLeaderSetting,
                isClassMemberGroupSetting, isCertPrint, isMakeupFilesManage, isSysCode, isMemberManageByCT,
                isMakeupFileManageByCT;

        public Auth001Object(String roleType, Boolean isSeeMemberData, Boolean isSeeCreateClass,
                             Boolean isSeeBarcodeCheckIn, Boolean isSeeClassAttendRecord,
                             Boolean isSeeClassGraduateStats, Boolean isSeeClassUpGrade,
                             Boolean isSeePrintReport, Boolean isSeeHomeClass, Boolean isSeeIDCheck,
                             Boolean isSeeCurrentClassMembers, Boolean isSeePreviousClassMembers,
                             Boolean isSeeCurrentClassLeaders, Boolean isSeeMultpleClassMembersAndLeaders,
                             Boolean isSeeEventRecord, Boolean isSeeClassStats, Boolean isSeeClassCurrentCheckInStats,
                             Boolean isSeeClassPeriodCheckInStats, Boolean isSeeClassStatsByClassName,
                             Boolean isSeeClassStatsWithLinechart, Boolean isSeeOnlineUser, Boolean isSeeNews,
                             Boolean isSeeAuthorize, Boolean isSeeUnitInfo, Boolean isSeeImportClassRecord,
                             Boolean isClassLeaderSetting, Boolean isClassMemberGroupSetting, Boolean isCertPrint,
                             Boolean isMakeupFilesManage, Boolean isSysCode, Boolean isMemberManageByCT,
                             Boolean isMakeupFileManageByCT) {
            this.roleType = roleType;
            this.isSeeMemberData = isSeeMemberData;
            this.isSeeCreateClass = isSeeCreateClass;
            this.isSeeBarcodeCheckIn = isSeeBarcodeCheckIn;
            this.isSeeClassAttendRecord = isSeeClassAttendRecord;
            this.isSeeClassGraduateStats = isSeeClassGraduateStats;
            this.isSeeClassUpGrade = isSeeClassUpGrade;
            this.isSeePrintReport = isSeePrintReport;
            this.isSeeHomeClass = isSeeHomeClass;
            this.isSeeIDCheck = isSeeIDCheck;
            this.isSeeCurrentClassMembers = isSeeCurrentClassMembers;
            this.isSeePreviousClassMembers = isSeePreviousClassMembers;
            this.isSeeCurrentClassLeaders = isSeeCurrentClassLeaders;
            this.isSeeMultpleClassMembersAndLeaders = isSeeMultpleClassMembersAndLeaders;
            this.isSeeEventRecord = isSeeEventRecord;
            this.isSeeClassStats = isSeeClassStats;
            this.isSeeClassCurrentCheckInStats = isSeeClassCurrentCheckInStats;
            this.isSeeClassPeriodCheckInStats = isSeeClassPeriodCheckInStats;
            this.isSeeClassStatsByClassName = isSeeClassStatsByClassName;
            this.isSeeClassStatsWithLinechart = isSeeClassStatsWithLinechart;
            this.isSeeOnlineUser = isSeeOnlineUser;
            this.isSeeNews = isSeeNews;
            this.isSeeAuthorize = isSeeAuthorize;
            this.isSeeUnitInfo = isSeeUnitInfo;
            this.isClassLeaderSetting = isClassLeaderSetting;
            this.isClassMemberGroupSetting = isClassMemberGroupSetting;
            this.isCertPrint = isCertPrint;
            this.isMakeupFilesManage = isMakeupFilesManage;
            this.isSysCode = isSysCode;
            this.isMemberManageByCT = isMemberManageByCT;
            this.isMakeupFileManageByCT = isMakeupFileManageByCT;
        }

        public String getRoleType() {
            return roleType;
        }

        public void setRoleType(String roleType) {
            this.roleType = roleType;
        }

        public Boolean getIsSeeMemberData() {
            return isSeeMemberData;
        }

        public void setIsSeeMemberData(Boolean seeMemberData) {
            isSeeMemberData = seeMemberData;
        }

        public Boolean getIsSeeCreateClass() {
            return isSeeCreateClass;
        }

        public void setIsSeeCreateClass(Boolean seeCreateClass) {
            isSeeCreateClass = seeCreateClass;
        }

        public Boolean getIsSeeBarcodeCheckIn() {
            return isSeeBarcodeCheckIn;
        }

        public void setIsSeeBarcodeCheckIn(Boolean seeBarcodeCheckIn) {
            isSeeBarcodeCheckIn = seeBarcodeCheckIn;
        }

        public Boolean getIsSeeClassAttendRecord() {
            return isSeeClassAttendRecord;
        }

        public void setIsSeeClassAttendRecord(Boolean seeClassAttendRecord) {
            isSeeClassAttendRecord = seeClassAttendRecord;
        }

        public Boolean getIsSeeClassGraduateStats() {
            return isSeeClassGraduateStats;
        }

        public void setIsSeeClassGraduateStats(Boolean seeClassGraduateStats) {
            isSeeClassGraduateStats = seeClassGraduateStats;
        }

        public Boolean getIsSeeClassUpGrade() {
            return isSeeClassUpGrade;
        }

        public void setIsSeeClassUpGrade(Boolean seeClassUpGrade) {
            isSeeClassUpGrade = seeClassUpGrade;
        }

        public Boolean getIsSeePrintReport() {
            return isSeePrintReport;
        }

        public void setIsSeePrintReport(Boolean seePrintReport) {
            isSeePrintReport = seePrintReport;
        }

        public Boolean getIsSeeHomeClass() {
            return isSeeHomeClass;
        }

        public void setIsSeeHomeClass(Boolean seeHomeClass) {
            isSeeHomeClass = seeHomeClass;
        }

        public Boolean getIsSeeIDCheck() {
            return isSeeIDCheck;
        }

        public void setIsSeeIDCheck(Boolean seeIDCheck) {
            isSeeIDCheck = seeIDCheck;
        }

        public Boolean getIsSeeCurrentClassMembers() {
            return isSeeCurrentClassMembers;
        }

        public void setIsSeeCurrentClassMembers(Boolean seeCurrentClassMembers) {
            isSeeCurrentClassMembers = seeCurrentClassMembers;
        }

        public Boolean getIsSeePreviousClassMembers() {
            return isSeePreviousClassMembers;
        }

        public void setIsSeePreviousClassMembers(Boolean seePreviousClassMembers) {
            isSeePreviousClassMembers = seePreviousClassMembers;
        }

        public Boolean getIsSeeCurrentClassLeaders() {
            return isSeeCurrentClassLeaders;
        }

        public void setIsSeeCurrentClassLeaders(Boolean seeCurrentClassLeaders) {
            isSeeCurrentClassLeaders = seeCurrentClassLeaders;
        }

        public Boolean getIsSeeMultpleClassMembersAndLeaders() {
            return isSeeMultpleClassMembersAndLeaders;
        }

        public void setIsSeeMultpleClassMembersAndLeaders(Boolean seeMultpleClassMembersAndLeaders) {
            isSeeMultpleClassMembersAndLeaders = seeMultpleClassMembersAndLeaders;
        }

        public Boolean getIsSeeEventRecord() {
            return isSeeEventRecord;
        }

        public void setIsSeeEventRecord(Boolean seeEventRecord) {
            isSeeEventRecord = seeEventRecord;
        }

        public Boolean getIsSeeClassStats() {
            return isSeeClassStats;
        }

        public void setIsSeeClassStats(Boolean seeClassStats) {
            isSeeClassStats = seeClassStats;
        }

        public Boolean getIsSeeClassCurrentCheckInStats() {
            return isSeeClassCurrentCheckInStats;
        }

        public void setIsSeeClassCurrentCheckInStats(Boolean seeClassCurrentCheckInStats) {
            isSeeClassCurrentCheckInStats = seeClassCurrentCheckInStats;
        }

        public Boolean getIsSeeClassPeriodCheckInStats() {
            return isSeeClassPeriodCheckInStats;
        }

        public void setIsSeeClassPeriodCheckInStats(Boolean seeClassPeriodCheckInStats) {
            isSeeClassPeriodCheckInStats = seeClassPeriodCheckInStats;
        }

        public Boolean getIsSeeClassStatsByClassName() {
            return isSeeClassStatsByClassName;
        }

        public void setIsSeeClassStatsByClassName(Boolean seeClassStatsByClassName) {
            isSeeClassStatsByClassName = seeClassStatsByClassName;
        }

        public Boolean getIsSeeClassStatsWithLinechart() {
            return isSeeClassStatsWithLinechart;
        }

        public void setIsSeeClassStatsWithLinechart(Boolean seeClassStatsWithLinechart) {
            isSeeClassStatsWithLinechart = seeClassStatsWithLinechart;
        }

        public Boolean getIsSeeOnlineUser() {
            return isSeeOnlineUser;
        }

        public void setIsSeeOnlineUser(Boolean seeOnlineUser) {
            isSeeOnlineUser = seeOnlineUser;
        }

        public Boolean getIsSeeNews() {
            return isSeeNews;
        }

        public void setIsSeeNews(Boolean seeNews) {
            isSeeNews = seeNews;
        }

        public Boolean getIsSeeAuthorize() {
            return isSeeAuthorize;
        }

        public void setIsSeeAuthorize(Boolean seeAuthorize) {
            isSeeAuthorize = seeAuthorize;
        }

        public Boolean getIsSeeUnitInfo() {
            return isSeeUnitInfo;
        }

        public void setIsSeeUnitInfo(Boolean seeUnitInfo) {
            isSeeUnitInfo = seeUnitInfo;
        }

        public Boolean getIsSeeImportClassRecord() {
            return isSeeImportClassRecord;
        }

        public void setIsSeeImportClassRecord(Boolean seeImportClassRecord) {
            isSeeImportClassRecord = seeImportClassRecord;
        }

        public Boolean getIsClassLeaderSetting() {
            return isClassLeaderSetting;
        }

        public void setIsClassLeaderSetting(Boolean classLeaderSetting) {
            isClassLeaderSetting = classLeaderSetting;
        }

        public Boolean getIsClassMemberGroupSetting() {
            return isClassMemberGroupSetting;
        }

        public void setIsClassMemberGroupSetting(Boolean classMemberGroupSetting) {
            isClassMemberGroupSetting = classMemberGroupSetting;
        }

        public Boolean getIsCertPrint() {
            return isCertPrint;
        }

        public void setIsCertPrint(Boolean certPrint) {
            isCertPrint = certPrint;
        }

        public Boolean getIsMakeupFilesManage() {
            return isMakeupFilesManage;
        }

        public void setIsMakeupFilesManage(Boolean makeupFilesManage) {
            isMakeupFilesManage = makeupFilesManage;
        }

        public Boolean getIsSysCode() {
            return isSysCode;
        }

        public void setIsSysCode(Boolean sysCode) {
            isSysCode = sysCode;
        }

        public Boolean getIsMemberManageByCT() {
            return isMemberManageByCT;
        }

        public void setIsMemberManageByCT(Boolean memberManageByCT) {
            isMemberManageByCT = memberManageByCT;
        }

        public Boolean getIsMakeupFileManageByCT() {
            return isMakeupFileManageByCT;
        }

        public void setIsMakeupFileManageByCT(Boolean makeupFileManageByCT) {
            isMakeupFileManageByCT = makeupFileManageByCT;
        }
    }

    public static class Auth001Object2 {

        private String UserName;

        public Auth001Object2(String userName) {
            UserName = userName;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }
    }
}
