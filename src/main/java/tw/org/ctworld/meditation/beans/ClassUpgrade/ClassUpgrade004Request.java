package tw.org.ctworld.meditation.beans.ClassUpgrade;

public class ClassUpgrade004Request {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
