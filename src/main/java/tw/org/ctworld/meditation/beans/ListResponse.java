package tw.org.ctworld.meditation.beans;

import java.util.List;

public class ListResponse<T> extends BaseResponse {
    private List<T> items;

    public ListResponse() {
    }

    public ListResponse(int errCode, String errMsg, List<T> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
