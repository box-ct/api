package tw.org.ctworld.meditation.beans.EventPgmDef;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateTimeCompactString;
import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateTimeString;
import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2TimeString;

public class EventPgmDef001Object {

    private String id, pgmCategoryId, pgmCategoryName, pgmId, pgmName, pgmDesc;
    private Boolean isGenPgm;
    private String genPgmUiType, genPgmInputOption, pgmStartTime, pgmEndTime;
    private Boolean isCenterAvailable;
    private String pgmNote, updatorName, updateDtTm;
    private Date mUpdateDtTm;

    public EventPgmDef001Object(Long id, String pgmCategoryId, String pgmCategoryName, String pgmId, String pgmName,
                                String pgmDesc, Boolean isGenPgm, String genPgmUiType, String genPgmInputOption,
                                Date pgmStartTime, Date pgmEndTime, Boolean isCenterAvailable, String pgmNote,
                                String updatorName, Date updateDtTm, Date mUpdateDtTm) {
        this.id = String.valueOf(id);
        this.pgmCategoryId = pgmCategoryId;
        this.pgmCategoryName = pgmCategoryName;
        this.pgmId = pgmId;
        this.pgmName = pgmName;
        this.pgmDesc = pgmDesc;
        this.isGenPgm = isGenPgm;
        this.genPgmUiType = genPgmUiType;
        this.genPgmInputOption = genPgmInputOption;
        this.pgmStartTime = DateTime2TimeString(pgmStartTime);
        this.pgmEndTime = DateTime2TimeString(pgmEndTime);
        this.isCenterAvailable = isCenterAvailable;
        this.pgmNote = pgmNote;
        this.updatorName = updatorName;
        this.updateDtTm = null;
        this.mUpdateDtTm = mUpdateDtTm;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPgmCategoryId() {
        return pgmCategoryId;
    }

    public void setPgmCategoryId(String pgmCategoryId) {
        this.pgmCategoryId = pgmCategoryId;
    }

    public String getPgmCategoryName() {
        return pgmCategoryName;
    }

    public void setPgmCategoryName(String pgmCategoryName) {
        this.pgmCategoryName = pgmCategoryName;
    }

    public String getPgmId() {
        return pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }

    public String getPgmName() {
        return pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

    public String getPgmDesc() {
        return pgmDesc;
    }

    public void setPgmDesc(String pgmDesc) {
        this.pgmDesc = pgmDesc;
    }

    public Boolean getIsGenPgm() {
        return isGenPgm;
    }

    public void setIsGenPgm(Boolean genPgm) {
        isGenPgm = genPgm;
    }

    public String getGenPgmUiType() {
        return genPgmUiType;
    }

    public void setGenPgmUiType(String genPgmUiType) {
        this.genPgmUiType = genPgmUiType;
    }

    public String getGenPgmInputOption() {
        return genPgmInputOption;
    }

    public void setGenPgmInputOption(String genPgmInputOption) {
        this.genPgmInputOption = genPgmInputOption;
    }

    public String getPgmStartTime() {
        return pgmStartTime;
    }

    public void setPgmStartTime(String pgmStartTime) {
        this.pgmStartTime = pgmStartTime;
    }

    public String getPgmEndTime() {
        return pgmEndTime;
    }

    public void setPgmEndTime(String pgmEndTime) {
        this.pgmEndTime = pgmEndTime;
    }

    public Boolean getIsCenterAvailable() {
        return isCenterAvailable;
    }

    public void setIsCenterAvailable(Boolean centerAvailable) {
        isCenterAvailable = centerAvailable;
    }

    public String getPgmNote() {
        return pgmNote;
    }

    public void setPgmNote(String pgmNote) {
        this.pgmNote = pgmNote;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

    public String getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(String updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public Date getmUpdateDtTm() {
        return mUpdateDtTm;
    }

    public void setmUpdateDtTm(Date mUpdateDtTm) {
        this.mUpdateDtTm = mUpdateDtTm;
    }
}
