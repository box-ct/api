package tw.org.ctworld.meditation.beans.MemberImport;

public class MemberImportResult {
    private String status;
    private String errorResult;
    private String memberId;
    private String gender;
    private String fullName;
    private String aliasName;
    private String engFirstName;
    private String engLastName;
    private String ctDharmaName;
    private String birthDate;
    private String twIdNum;
    private String passportNum;
    private Boolean check;
    private int idx;

    public MemberImportResult(){}

    public MemberImportResult(String status, String errorResult, String memberId, String gender, String fullName, String aliasName, String engFirstName, String engLastName, String ctDharmaName, String birthDate, String twIdNum, String passportNum, Boolean check, int idx) {
        this.status = status;
        this.errorResult = errorResult;
        this.memberId = memberId;
        this.gender = gender;
        this.fullName = fullName;
        this.aliasName = aliasName;
        this.engFirstName = engFirstName;
        this.engLastName = engLastName;
        this.ctDharmaName = ctDharmaName;
        this.birthDate = birthDate;
        this.twIdNum = twIdNum;
        this.passportNum = passportNum;
        this.check = check;
        this.idx = idx;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorResult() {
        return errorResult;
    }

    public void setErrorResult(String errorResult) {
        this.errorResult = errorResult;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getEngFirstName() {
        return engFirstName;
    }

    public void setEngFirstName(String engFirstName) {
        this.engFirstName = engFirstName;
    }

    public String getEngLastName() {
        return engLastName;
    }

    public void setEngLastName(String engLastName) {
        this.engLastName = engLastName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }
}
