package tw.org.ctworld.meditation.beans.Printer;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.ArrayList;
import java.util.List;

public class Printer001Response extends BaseResponse {

    private List<Printer001Object> items;

    public Printer001Response(int errCode, String errMsg, List<Printer001Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Printer001Object> getItems() {
        return items;
    }

    public void setItems(List<Printer001Object> items) {
        this.items = items;
    }
}
