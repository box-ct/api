package tw.org.ctworld.meditation.beans.CtEventMaintain;

public class CtEventMaintain013Request {

    private String pgmUniqueId;
    private Boolean isDisabledByOCA, isRequired;
    private Integer pgmUiOrderNum;
    private String pgmDate, pgmStartTime, pgmEndTime;
    private Integer pgmCtOrderNum;
    private String pgmName, pgmNote, pgmDesc, goTransDate,
            goTransType, returnTransDate, returnTransType;

    public String getPgmUniqueId() {
        return pgmUniqueId;
    }

    public void setPgmUniqueId(String pgmUniqueId) {
        this.pgmUniqueId = pgmUniqueId;
    }

    public Boolean getIsDisabledByOCA() {
        return isDisabledByOCA;
    }

    public void setIsDisabledByOCA(Boolean disabledByOCA) {
        isDisabledByOCA = disabledByOCA;
    }

    public Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean required) {
        isRequired = required;
    }

    public Integer getPgmUiOrderNum() {
        return pgmUiOrderNum;
    }

    public void setPgmUiOrderNum(Integer pgmUiOrderNum) {
        this.pgmUiOrderNum = pgmUiOrderNum;
    }

    public String getPgmDate() {
        return pgmDate;
    }

    public void setPgmDate(String pgmDate) {
        this.pgmDate = pgmDate;
    }

    public String getPgmStartTime() {
        return pgmStartTime;
    }

    public void setPgmStartTime(String pgmStartTime) {
        this.pgmStartTime = pgmStartTime;
    }

    public String getPgmEndTime() {
        return pgmEndTime;
    }

    public void setPgmEndTime(String pgmEndTime) {
        this.pgmEndTime = pgmEndTime;
    }

    public Integer getPgmCtOrderNum() {
        return pgmCtOrderNum;
    }

    public void setPgmCtOrderNum(Integer pgmCtOrderNum) {
        this.pgmCtOrderNum = pgmCtOrderNum;
    }

    public String getPgmName() {
        return pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

    public String getPgmNote() {
        return pgmNote;
    }

    public void setPgmNote(String pgmNote) {
        this.pgmNote = pgmNote;
    }

    public String getPgmDesc() {
        return pgmDesc;
    }

    public void setPgmDesc(String pgmDesc) {
        this.pgmDesc = pgmDesc;
    }

    public String getGoTransDate() {
        return goTransDate;
    }

    public void setGoTransDate(String goTransDate) {
        this.goTransDate = goTransDate;
    }

    public String getGoTransType() {
        return goTransType;
    }

    public void setGoTransType(String goTransType) {
        this.goTransType = goTransType;
    }

    public String getReturnTransDate() {
        return returnTransDate;
    }

    public void setReturnTransDate(String returnTransDate) {
        this.returnTransDate = returnTransDate;
    }

    public String getReturnTransType() {
        return returnTransType;
    }

    public void setReturnTransType(String returnTransType) {
        this.returnTransType = returnTransType;
    }
}
