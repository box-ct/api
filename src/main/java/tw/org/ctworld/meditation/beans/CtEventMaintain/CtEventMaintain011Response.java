package tw.org.ctworld.meditation.beans.CtEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class CtEventMaintain011Response extends BaseResponse {

    private List<CtEventMaintain011Object> items;

    public CtEventMaintain011Response(int errCode, String errMsg, List<CtEventMaintain011Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<CtEventMaintain011Object> getItems() {
        return items;
    }

    public void setItems(List<CtEventMaintain011Object> items) {
        this.items = items;
    }
}
