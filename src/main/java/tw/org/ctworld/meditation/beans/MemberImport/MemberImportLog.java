package tw.org.ctworld.meditation.beans.MemberImport;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.time.Duration;
import java.util.Date;

public class MemberImportLog {
    private String memberId;
    private String aliasName;
    private String ctDharmaName;
    private String importResultType;
    private String importContent;
    private String creatorName;
    private String createDtTm;

    public MemberImportLog(){}
    public MemberImportLog(String memberId, String aliasName, String ctDharmaName, String importResultType, String importContent, String creatorName, Date createDtTm) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.importResultType = importResultType;
        this.importContent = importContent;
        this.creatorName = creatorName;
        this.createDtTm = CommonUtils.DateTime2DateTimeString(createDtTm);
    }

    public MemberImportLog(String memberId, String aliasName, String ctDharmaName, String importResultType, String importContent, String creatorName, Date createDtTm,String timezone) {
        int hh=0;
        try{
            hh=Integer.parseInt(timezone);
        }
        catch (Exception ex){

        }
        Date newDate =java.util.Date.from( createDtTm.toInstant().plus(Duration.ofHours(hh)));
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.importResultType = importResultType;
        this.importContent = importContent;
        this.creatorName = creatorName;
        this.createDtTm = CommonUtils.DateTime2DateTimeString(newDate);
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getImportResultType() {
        return importResultType;
    }

    public void setImportResultType(String importResultType) {
        this.importResultType = importResultType;
    }

    public String getImportContent() {
        return importContent;
    }

    public void setImportContent(String importContent) {
        this.importContent = importContent;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreateDtTm() {
        return createDtTm;
    }

    public void setCreateDtTm(String createDtTm) {
        this.createDtTm = createDtTm;
    }
}
