package tw.org.ctworld.meditation.beans.Common;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Common005Response extends BaseResponse {

    private List<String> items;

    public Common005Response(int errCode, String errMsg, List<String> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
