package tw.org.ctworld.meditation.beans.DataImport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExcelMemberEnroll {
    private String name;
    private String birthday;
    private String birthdayParsed;
    private Date birthDate;
    private String gender;
    private String genderParsed;
    private String twId;
    private String twIdParsed;
    private String ctName;
    private String groupNum;
    private int groupNumParsed;
    private String memberNum;
    private int memberNumParsed;
    private String jobList;
    private String canceled;
    private boolean canceledParsed;
    private String fullyAttended;
    private boolean fullyAttendedParsed;
    private String graduated;
    private boolean graduatedParsed;
    private boolean valid;
    private String memberId;
    private String formId;
    private String classId;
    private String unitId;
    private String unitName;

    private String importErrorContent;
    private String successUpdateContent;
    private String importResultType;


    public ExcelMemberEnroll() {
        valid = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTwId() {
        return twId;
    }

    public void setTwId(String twId) {
        this.twId = twId;
    }

    public String getCtName() {
        return ctName;
    }

    public void setCtName(String ctName) {
        this.ctName = ctName;
    }

    public String getGroupNum() {
        return groupNum;
    }

    public void setGroupNum(String groupNum) {
        this.groupNum = groupNum;
    }

    public String getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(String memberNum) {
        this.memberNum = memberNum;
    }

    public String getJobList() {
        return jobList;
    }

    public void setJobList(String jobList) {
        this.jobList = jobList;
    }

    public String getCanceled() {
        return canceled;
    }

    public void setCanceled(String canceled) {
        this.canceled = canceled;
    }

    public String getFullyAttended() {
        return fullyAttended;
    }

    public void setFullyAttended(String fullyAttended) {
        this.fullyAttended = fullyAttended;
    }

    public String getGraduated() {
        return graduated;
    }

    public void setGraduated(String graduated) {
        this.graduated = graduated;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getBirthdayParsed() {
        return birthdayParsed;
    }

    public void setBirthdayParsed(String birthdayParsed) {
        this.birthdayParsed = birthdayParsed;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        try {
            this.birthDate = formatter.parse(birthdayParsed);
        } catch (Exception e) {
            this.birthDate = null;
        }
    }

    public String getGenderParsed() {
        return genderParsed;
    }

    public void setGenderParsed(String genderParsed) {
        this.genderParsed = genderParsed;
    }

    public String getTwIdParsed() {
        return twIdParsed;
    }

    public void setTwIdParsed(String twIdParsed) {
        this.twIdParsed = twIdParsed;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getGroupNumParsed() {
        return groupNumParsed;
    }

    public void setGroupNumParsed(int groupNumParsed) {
        this.groupNumParsed = groupNumParsed;
    }

    public int getMemberNumParsed() {
        return memberNumParsed;
    }

    public void setMemberNumParsed(int memberNumParsed) {
        this.memberNumParsed = memberNumParsed;
    }

    public boolean isCanceledParsed() {
        return canceledParsed;
    }

    public void setCanceledParsed(boolean canceledParsed) {
        this.canceledParsed = canceledParsed;
    }

    public boolean isFullyAttendedParsed() {
        return fullyAttendedParsed;
    }

    public void setFullyAttendedParsed(boolean fullyAttendedParsed) {
        this.fullyAttendedParsed = fullyAttendedParsed;
    }

    public boolean isGraduatedParsed() {
        return graduatedParsed;
    }

    public void setGraduatedParsed(boolean graduatedParsed) {
        this.graduatedParsed = graduatedParsed;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getImportErrorContent() {
        return importErrorContent;
    }

    public void setImportErrorContent(String importErrorContent) {
        this.importErrorContent = importErrorContent;
    }

    public String getSuccessUpdateContent() {
        return successUpdateContent;
    }

    public void setSuccessUpdateContent(String successUpdateContent) {
        this.successUpdateContent = successUpdateContent;
    }

    public String getImportResultType() {
        return importResultType;
    }

    public void setImportResultType(String importResultType) {
        this.importResultType = importResultType;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setData(String memberId, String formId, String importResultType, String successUpdateContent, String importErrorContent) {
        this.memberId = memberId;
        this.formId = formId;
        this.importResultType = importResultType;
        this.successUpdateContent = successUpdateContent;
        this.importErrorContent = importErrorContent;
    }


}
