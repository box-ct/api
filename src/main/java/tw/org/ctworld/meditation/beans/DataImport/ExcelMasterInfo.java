package tw.org.ctworld.meditation.beans.DataImport;

public class ExcelMasterInfo {
    private String masterId;
    private String masterName;

    public ExcelMasterInfo(){}

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }
}
