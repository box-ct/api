package tw.org.ctworld.meditation.beans.EventCategoryDef;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class EventCategoryDef007Response extends BaseResponse {

    private List<EventCategoryDef007Object> items;

    public EventCategoryDef007Response(int errCode, String errMsg, List<EventCategoryDef007Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<EventCategoryDef007Object> getItems() {
        return items;
    }

    public void setItems(List<EventCategoryDef007Object> items) {
        this.items = items;
    }
}
