package tw.org.ctworld.meditation.beans.Anonymous;

import java.util.List;

public class Anonymous006_1Object {

    private String classId, classPeriodNum, className, dayOfWeek, classStatus;
    private List<Anonymous006_2Object> classDates;

    public Anonymous006_1Object(String classId, String classPeriodNum, String className, String dayOfWeek,
                                String classStatus, List<Anonymous006_2Object> classDates) {
        this.classId = classId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.dayOfWeek = dayOfWeek;
        this.classStatus = classStatus;
        this.classDates = classDates;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getClassStatus() {
        return classStatus;
    }

    public void setClassStatus(String classStatus) {
        this.classStatus = classStatus;
    }

    public List<Anonymous006_2Object> getClassDates() {
        return classDates;
    }

    public void setClassDates(List<Anonymous006_2Object> classDates) {
        this.classDates = classDates;
    }
}
