package tw.org.ctworld.meditation.beans.UnitMaster;

import tw.org.ctworld.meditation.models.UnitMasterInfo;

import java.util.List;

public class UnitMaster002Data {
    private data data;

    public UnitMaster002Data() {

    }

    public UnitMaster002Data(UnitMaster002Data.data data) {
        this.data = data;
    }

    public UnitMaster002Data.data getData() {
        return data;
    }

    public void setData(UnitMaster002Data.data data) {
        this.data = data;
    }

    public class data {
        private String unitName, certUnitName, unitId, fundUnitId, certUnitEngName, unitPhoneNum, unitFaxNum, unitZoneName, timeZone, unitPhoneNumCode, unitMobileNum, unitEmail, unitMailingAddress, stateName,
                abbotId, abbotName, abbotEngName, district;
        private boolean isAbbot;
        private boolean isTreasurer;
        private boolean overseasUnit;
        private List<UnitMasterInfo> unitMasterInfos;

        public data() {

        }

        public data(String unitName, String certUnitName, String unitId, String fundUnitId, String certUnitEngName, String unitPhoneNum, String unitFaxNum, String unitZoneName, String timeZone, String unitPhoneNumCode, String unitMobileNum, String unitEmail, String unitMailingAddress, String stateName, String abbotId, String abbotName, String abbotEngName, String district, boolean isAbbot, boolean isTreasurer, boolean overseasUnit, List<UnitMasterInfo> unitMasterInfos) {
            this.unitName = unitName;
            this.certUnitName = certUnitName;
            this.unitId = unitId;
            this.fundUnitId = fundUnitId;
            this.certUnitEngName = certUnitEngName;
            this.unitPhoneNum = unitPhoneNum;
            this.unitFaxNum = unitFaxNum;
            this.unitZoneName = unitZoneName;
            this.timeZone = timeZone;
            this.unitPhoneNumCode = unitPhoneNumCode;
            this.unitMobileNum = unitMobileNum;
            this.unitEmail = unitEmail;
            this.unitMailingAddress = unitMailingAddress;
            this.stateName = stateName;
            this.abbotId = abbotId;
            this.abbotName = abbotName;
            this.abbotEngName = abbotEngName;
            this.district = district;
            this.isAbbot = isAbbot;
            this.isTreasurer = isTreasurer;
            this.overseasUnit = overseasUnit;
            this.unitMasterInfos = unitMasterInfos;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }

        public String getCertUnitName() {
            return certUnitName;
        }

        public void setCertUnitName(String certUnitName) {
            this.certUnitName = certUnitName;
        }

        public String getUnitId() {
            return unitId;
        }

        public void setUnitId(String unitId) {
            this.unitId = unitId;
        }

        public String getFundUnitId() {
            return fundUnitId;
        }

        public void setFundUnitId(String fundUnitId) {
            this.fundUnitId = fundUnitId;
        }

        public String getCertUnitEngName() {
            return certUnitEngName;
        }

        public void setCertUnitEngName(String certUnitEngName) {
            this.certUnitEngName = certUnitEngName;
        }

        public String getUnitPhoneNum() {
            return unitPhoneNum;
        }

        public void setUnitPhoneNum(String unitPhoneNum) {
            this.unitPhoneNum = unitPhoneNum;
        }

        public String getUnitFaxNum() {
            return unitFaxNum;
        }

        public void setUnitFaxNum(String unitFaxNum) {
            this.unitFaxNum = unitFaxNum;
        }

        public String getUnitZoneName() {
            return unitZoneName;
        }

        public void setUnitZoneName(String unitZoneName) {
            this.unitZoneName = unitZoneName;
        }

        public String getTimeZone() {
            return timeZone;
        }

        public void setTimeZone(String timeZone) {
            this.timeZone = timeZone;
        }

        public String getUnitPhoneNumCode() {
            return unitPhoneNumCode;
        }

        public void setUnitPhoneNumCode(String unitPhoneNumCode) {
            this.unitPhoneNumCode = unitPhoneNumCode;
        }

        public String getUnitMobileNum() {
            return unitMobileNum;
        }

        public void setUnitMobileNum(String unitMobileNum) {
            this.unitMobileNum = unitMobileNum;
        }

        public String getUnitEmail() {
            return unitEmail;
        }

        public void setUnitEmail(String unitEmail) {
            this.unitEmail = unitEmail;
        }

        public String getUnitMailingAddress() {
            return unitMailingAddress;
        }

        public void setUnitMailingAddress(String unitMailingAddress) {
            this.unitMailingAddress = unitMailingAddress;
        }

        public String getStateName() {
            return stateName;
        }

        public void setStateName(String stateName) {
            this.stateName = stateName;
        }

        public String getAbbotId() {
            return abbotId;
        }

        public void setAbbotId(String abbotId) {
            this.abbotId = abbotId;
        }

        public String getAbbotName() {
            return abbotName;
        }

        public void setAbbotName(String abbotName) {
            this.abbotName = abbotName;
        }

        public String getAbbotEngName() {
            return abbotEngName;
        }

        public void setAbbotEngName(String abbotEngName) {
            this.abbotEngName = abbotEngName;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public boolean isAbbot() {
            return isAbbot;
        }

        public void setAbbot(boolean abbot) {
            isAbbot = abbot;
        }

        public boolean isTreasurer() {
            return isTreasurer;
        }

        public void setTreasurer(boolean treasurer) {
            isTreasurer = treasurer;
        }

        public boolean isOverseasUnit() {
            return overseasUnit;
        }

        public void setOverseasUnit(boolean overseasUnit) {
            this.overseasUnit = overseasUnit;
        }

        public List<UnitMasterInfo> getUnitMasterInfos() {
            return unitMasterInfos;
        }

        public void setUnitMasterInfos(List<UnitMasterInfo> unitMasterInfos) {
            this.unitMasterInfos = unitMasterInfos;
        }
    }
}