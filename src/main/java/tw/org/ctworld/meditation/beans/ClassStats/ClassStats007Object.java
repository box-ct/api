package tw.org.ctworld.meditation.beans.ClassStats;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;

public class ClassStats007Object {

    private String unitId, unitName, memberId, gender, aliasName, ctDharmaName, nameTail, twIdNum, birthDate,
            birthCity, address, homePhoneNum1, mobileNum1, schoolNameaAndMajor, companyNameAndJobTitle,
            classPeriodNum, className, classGroupId, memberGroupNum;
    private Date classStartDate, classEndDate;
    private String dayOfWeek;
    private Boolean isFullAttended, isMakeupToFullAttended, isGraduated, isDiligentAward;
    private String skillList, healthList, currentClassIntroducerName, urgentContactPersonName1,
            urgentContactPersonPhoneNum1,dataUnitMemberId, classJobTitleList;
    private Boolean okSendMessage, okSendMagazine, isLeadGroup;

    public ClassStats007Object(String unitId, String unitName, String memberId, String gender, String aliasName,
                               String ctDharmaName, String nameTail, String twIdNum, Date birthDate,
                               String birthCity, String address, String homePhoneNum1, String mobileNum1,
                               String schoolNameaAndMajor, String companyNameAndJobTitle, String classPeriodNum,
                               String className, String classGroupId, String memberGroupNum, Date classStartDate,
                               Date classEndDate, String dayOfWeek, Boolean isFullAttended,
                               Boolean isMakeupToFullAttended, Boolean isGraduated, Boolean isDiligentAward,
                               String skillList, String healthList, String currentClassIntroducerName,
                               String urgentContactPersonName1, String urgentContactPersonPhoneNum1,
                               String dataUnitMemberId, Boolean okSendMessage, Boolean okSendMagazine,
                               String classJobTitleList, Boolean isLeadGroup) {
        this.unitId = unitId;
        this.unitName = unitName;
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.nameTail = nameTail;
        this.twIdNum = twIdNum;
        this.birthDate = DateTime2DateString(birthDate);
        this.birthCity = birthCity;
        this.address = address;
        this.homePhoneNum1 = homePhoneNum1;
        this.mobileNum1 = mobileNum1;
        this.schoolNameaAndMajor = schoolNameaAndMajor;
        this.companyNameAndJobTitle = companyNameAndJobTitle;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classStartDate = classStartDate;
        this.classEndDate = classEndDate;
        this.dayOfWeek = dayOfWeek;
        this.isFullAttended = isFullAttended;
        this.isMakeupToFullAttended = isMakeupToFullAttended;
        this.isGraduated = isGraduated;
        this.isDiligentAward = isDiligentAward;
        this.skillList = skillList;
        this.healthList = healthList;
        this.currentClassIntroducerName = currentClassIntroducerName;
        this.urgentContactPersonName1 = urgentContactPersonName1;
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
        this.dataUnitMemberId = dataUnitMemberId;
        this.okSendMessage = okSendMessage;
        this.okSendMagazine = okSendMagazine;

        this.classJobTitleList = classJobTitleList;
        this.isLeadGroup = isLeadGroup;
    }

    public ClassStats007Object() {
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getNameTail() {
        return nameTail;
    }

    public void setNameTail(String nameTail) {
        this.nameTail = nameTail;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getSchoolNameaAndMajor() {
        return schoolNameaAndMajor;
    }

    public void setSchoolNameaAndMajor(String schoolNameaAndMajor) {
        this.schoolNameaAndMajor = schoolNameaAndMajor;
    }

    public String getCompanyNameAndJobTitle() {
        return companyNameAndJobTitle;
    }

    public void setCompanyNameAndJobTitle(String companyNameAndJobTitle) {
        this.companyNameAndJobTitle = companyNameAndJobTitle;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public Date getClassStartDate() {
        return classStartDate;
    }

    public void setClassStartDate(Date classStartDate) {
        this.classStartDate = classStartDate;
    }

    public Date getClassEndDate() {
        return classEndDate;
    }

    public void setClassEndDate(Date classEndDate) {
        this.classEndDate = classEndDate;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Boolean getIsFullAttended() {
        if (isFullAttended != null) {
            return isFullAttended;
        } else {
            return Boolean.FALSE;
        }
    }

    public void setIsFullAttended(Boolean fullAttended) {
        isFullAttended = fullAttended;
    }

    public Boolean getIsMakeupToFullAttended() {
        if (isMakeupToFullAttended != null) {
            return isMakeupToFullAttended;
        } else {
            return Boolean.FALSE;
        }
    }

    public void setIsMakeupToFullAttended(Boolean makeupToFullAttended) {
        isMakeupToFullAttended = makeupToFullAttended;
    }

    public Boolean getIsGraduated() {
        if (isGraduated != null) {
            return isGraduated;
        } else {
            return Boolean.FALSE;
        }
    }

    public void setIsGraduated(Boolean graduated) {
        isGraduated = graduated;
    }

    public Boolean getIsDiligentAward() {
        if (isDiligentAward != null) {
            return isDiligentAward;
        } else {
            return Boolean.FALSE;
        }

    }

    public void setIsDiligentAward(Boolean diligentAward) {
        isDiligentAward = diligentAward;
    }

    public String getSkillList() {
        return skillList;
    }

    public void setSkillList(String skillList) {
        this.skillList = skillList;
    }

    public String getHealthList() {
        return healthList;
    }

    public void setHealthList(String healthList) {
        this.healthList = healthList;
    }

    public String getCurrentClassIntroducerName() {
        return currentClassIntroducerName;
    }

    public void setCurrentClassIntroducerName(String currentClassIntroducerName) {
        this.currentClassIntroducerName = currentClassIntroducerName;
    }

    public String getUrgentContactPersonName1() {
        return urgentContactPersonName1;
    }

    public void setUrgentContactPersonName1(String urgentContactPersonName1) {
        this.urgentContactPersonName1 = urgentContactPersonName1;
    }

    public String getUrgentContactPersonPhoneNum1() {
        return urgentContactPersonPhoneNum1;
    }

    public void setUrgentContactPersonPhoneNum1(String urgentContactPersonPhoneNum1) {
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
    }

    public String getDataUnitMemberId() {
        return dataUnitMemberId;
    }

    public void setDataUnitMemberId(String dataUnitMemberId) {
        this.dataUnitMemberId = dataUnitMemberId;
    }

    public Boolean getOkSendMessage() {
        if (okSendMessage != null) {
            return okSendMessage;
        } else {
            return Boolean.FALSE;
        }
    }

    public void setOkSendMessage(Boolean okSendMessage) {
        this.okSendMessage = okSendMessage;
    }

    public Boolean getOkSendMagazine() {
        if (okSendMagazine != null) {
            return okSendMagazine;
        } else {
            return Boolean.FALSE;
        }
    }

    public void setOkSendMagazine(Boolean okSendMagazine) {
        this.okSendMagazine = okSendMagazine;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public Boolean getLeadGroup() {
        return isLeadGroup;
    }

    public void setLeadGroup(Boolean leadGroup) {
        isLeadGroup = leadGroup;
    }
}
