package tw.org.ctworld.meditation.beans.CtEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class CtEventMaintain004Response extends BaseResponse {

    private CtEventMaintain004_2Object result;

    public CtEventMaintain004Response(int errCode, String errMsg, CtEventMaintain004_2Object result) {
        super(errCode, errMsg);
        this.result = result;
    }

    public CtEventMaintain004_2Object getResult() {
        return result;
    }

    public void setResult(CtEventMaintain004_2Object result) {
        this.result = result;
    }
}
