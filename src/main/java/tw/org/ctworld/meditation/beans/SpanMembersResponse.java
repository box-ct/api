package tw.org.ctworld.meditation.beans;

import tw.org.ctworld.meditation.beans.Member.MembersObject;
import tw.org.ctworld.meditation.beans.Member.MembersObject;

import java.util.List;

public class SpanMembersResponse extends BaseResponse{
    private List<MembersObject> items;

    public SpanMembersResponse() {
    }

    public SpanMembersResponse(int errCode, String errMsg, List<MembersObject> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<MembersObject> getItems() {
        return items;
    }

    public void setItems(List<MembersObject> items) {
        this.items = items;
    }
}
