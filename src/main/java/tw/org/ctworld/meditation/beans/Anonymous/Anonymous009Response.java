package tw.org.ctworld.meditation.beans.Anonymous;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Anonymous009Response extends BaseResponse {

    private List<Anonymous009_1Object> items;

    public Anonymous009Response(int errCode, String errMsg, List<Anonymous009_1Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Anonymous009_1Object> getItems() {
        return items;
    }

    public void setItems(List<Anonymous009_1Object> items) {
        this.items = items;
    }
}
