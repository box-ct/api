package tw.org.ctworld.meditation.beans.UnitMaster;

import tw.org.ctworld.meditation.models.UnitMasterInfo;

public class UnitMaster001Object{
    private UnitMasterInfo unitMasterInfo;

    public UnitMaster001Object() {
    }

    public UnitMaster001Object(UnitMasterInfo unitMasterInfo){
        this.unitMasterInfo=unitMasterInfo;
    }

    public UnitMasterInfo getUnitMasterInfo() {
        return unitMasterInfo;
    }

    public void setUnitMasterInfo(UnitMasterInfo unitMasterInfo) {
        this.unitMasterInfo = unitMasterInfo;
    }
}
//public class UnitMaster001Object {
//    private String createDtTm,creatorId, creatorIp, creatorName,creatorUnitId,creatorUnitName,updateDtTm, updatorId,updatorIp,updatorName,updatorUnitId,updatorUnitName,
//            jobOrder,jobTitle,masterId,masterName, masterPreceptTypeName,mobileNum,preceptOrder, unitId,unitName;
//
//    private boolean isAbbot,isTreasurer;
//
//    public UnitMaster001Object(String createDtTm, String creatorId, String  creatorIp, String  creatorName, String creatorUnitId, String creatorUnitName, String updateDtTm,
//                               String  updatorId, String updatorIp, String updatorName, String updatorUnitId, String updatorUnitName, String jobOrder, String jobTitle,
//                               String masterId, String masterName, String  masterPreceptTypeName, String mobileNum, String preceptOrder, String  unitId, String unitName,
//                               boolean isAbbot,boolean isTreasurer){
//        this.createDtTm=createDtTm;
//        this.creatorId=creatorId;
//        this.creatorIp=creatorIp;
//        this.creatorName=creatorName;
//        this.creatorUnitId=creatorUnitId;
//        this.creatorUnitName=creatorUnitName;
//        this.updateDtTm=updateDtTm;
//        this.updatorId=updatorId;
//        this.updatorIp=updatorIp;
//        this.updatorName=updatorName;
//        this.updatorUnitId=updatorUnitId;
//        this.updatorUnitName=updatorUnitName;
//        this.jobOrder=jobOrder;
//        this.jobTitle=jobTitle;
//        this.masterId=masterId;
//        this.masterName=masterName;
//        this.masterPreceptTypeName=masterPreceptTypeName;
//        this.mobileNum=mobileNum;
//        this.preceptOrder=preceptOrder;
//        this.unitId=unitId;
//        this.unitName=unitName;
//        this.isAbbot=isAbbot;
//        this.isTreasurer=isTreasurer;
//    }
//
//    public String getCreateDtTm(){return createDtTm;}
//    public String getCreatorId(){return creatorId;}
//    public String getCreatorIp(){return creatorIp;}
//    public String getCreatorName(){return creatorName;}
//    public String getCreatorUnitId(){return creatorUnitId;}
//    public String getCreatorUnitName(){return creatorUnitName;}
//    public String getUpdateDtTm(){return updateDtTm;}
//    public String getUpdatorId(){return updatorId;}
//    public String getUpdatorIp(){return updatorIp;}
//    public String getUpdatorName(){return updatorName;}
//    public String getUpdatorUnitId(){return updatorUnitId;}
//    public String getUpdatorUnitName(){return updatorUnitName;}
//    public String getJobOrder(){return jobOrder;}
//    public String getJobTitle(){return jobTitle;}
//    public String getMasterId(){return masterId;}
//    public String getMasterName(){return masterName;}
//    public String getMasterPreceptTypeName(){return masterPreceptTypeName;}
//    public String getMobileNum(){return mobileNum;}
//    public String getPreceptOrder(){return preceptOrder;}
//    public String getUnitId(){return unitId;}
//    public String getUnitName(){return unitName;}
//    public boolean getIsAbbot(){return isAbbot;}
//    public boolean getIsTreasurer(){return isTreasurer;}
//
//    public void setCreateDtTm(String createDtTm){this.createDtTm=createDtTm;}
//    public void setCreatorId(String creatorId){this.creatorId=creatorId;}
//    public void setCreatorIp(String creatorIp){this.creatorIp=creatorIp;}
//    public void setCreatorName(String creatorName){this.creatorName=creatorName;}
//    public void setCreatorUnitId(String creatorUnitId){this.creatorUnitId=creatorUnitId;}
//    public void setCreatorUnitName(String creatorUnitName){this.creatorUnitName=creatorUnitName;}
//    public void setUpdateDtTm(String updateDtTm){this.updateDtTm=updateDtTm;}
//    public void setUpdatorId(String updatorId){this.updatorId=updatorId;}
//    public void setUpdatorIp(String updatorIp){this.updatorIp=updatorIp;}
//    public void setUpdatorName(String updatorName){this.updatorName=updatorName;}
//    public void setUpdatorUnitId(String updatorUnitId){this.updatorUnitId=updatorUnitId;}
//    public void setUpdatorUnitName(String updatorUnitName){this.updatorUnitName=updatorUnitName;}
//    public void setJobOrder(String jobOrder){this.jobOrder=jobOrder;}
//    public void setJobTitle(String jobTitle){this.jobTitle=jobTitle;}
//    public void setMasterId(String masterId){this.masterId=masterId;}
//    public void setMasterName(String masterName){this.masterName=masterName;}
//    public void setMasterPreceptTypeName(String masterPreceptTypeName){this.masterPreceptTypeName=masterPreceptTypeName;}
//    public void setMobileNum(String mobileNum){this.mobileNum=mobileNum;}
//    public void setPreceptOrder(String preceptOrder){this.preceptOrder=preceptOrder;}
//    public void setUnitId(String unitId) {this.unitId = unitId;}
//    public void setUnitName(String unitName){this.unitName=unitName;}
//    public void setIsAbbot(boolean isAbbot){this.isAbbot=isAbbot;}
//    public void setIsTreasuere(boolean isTreasuere){this.isTreasurer=isTreasuere;}
//}
