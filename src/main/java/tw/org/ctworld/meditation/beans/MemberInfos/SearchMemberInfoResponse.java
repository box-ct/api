package tw.org.ctworld.meditation.beans.MemberInfos;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class SearchMemberInfoResponse extends BaseResponse {
    private List<SearchMemberInfo> items;

    public SearchMemberInfoResponse(int errCode, String errMsg, List<SearchMemberInfo> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<SearchMemberInfo> getItems() {
        return items;
    }

    public void setItems(List<SearchMemberInfo> items) {
        this.items = items;
    }
}
