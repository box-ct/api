package tw.org.ctworld.meditation.beans.Anonymous;

public class Anonymous006_2Object {

    private String classDate;
    private int classWeeksNum, diffNum;

    public Anonymous006_2Object(String classDate, int classWeeksNum, int diffNum) {
        this.classDate = classDate;
        this.classWeeksNum = classWeeksNum;
        this.diffNum = diffNum;
    }

    public String getClassDate() {
        return classDate;
    }

    public void setClassDate(String classDate) {
        this.classDate = classDate;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public int getDiffNum() {
        return diffNum;
    }

    public void setDiffNum(int diffNum) {
        this.diffNum = diffNum;
    }
}
