package tw.org.ctworld.meditation.beans.Sys;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.RecordImportLog;

import java.util.List;

public class ClassRecordImportResponse extends BaseResponse {
    private List<RecordImportLog> items;
    private String enrollFile;
    private String memberFile;
    private String enrollFilename;
    private String memberFilename;

    public ClassRecordImportResponse(int errCode, String errMsg, List<RecordImportLog> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<RecordImportLog> getItems() {
        return items;
    }

    public void setItems(List<RecordImportLog> items) {
        this.items = items;
    }

    public String getEnrollFile() {
        return enrollFile;
    }

    public void setEnrollFile(String enrollFile) {
        this.enrollFile = enrollFile;
    }

    public String getMemberFile() {
        return memberFile;
    }

    public void setMemberFile(String memberFile) {
        this.memberFile = memberFile;
    }

    public String getEnrollFilename() {
        return enrollFilename;
    }

    public void setEnrollFilename(String enrollFilename) {
        this.enrollFilename = enrollFilename;
    }

    public String getMemberFilename() {
        return memberFilename;
    }

    public void setMemberFilename(String memberFilename) {
        this.memberFilename = memberFilename;
    }
}
