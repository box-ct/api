package tw.org.ctworld.meditation.beans.Common;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class Common010Response extends BaseResponse {

    private String classPeriodNum;

    public Common010Response(int errCode, String errMsg, String classPeriodNum) {
        super(errCode, errMsg);
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }
}
