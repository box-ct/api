package tw.org.ctworld.meditation.beans.Unit;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Unit003_2Response extends BaseResponse {

    private List<Unit003_2Object> items;

    public Unit003_2Response(int errCode, String errMsg, List<Unit003_2Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Unit003_2Object> getItems() {
        return items;
    }

    public void setItems(List<Unit003_2Object> items) {
        this.items = items;
    }
}
