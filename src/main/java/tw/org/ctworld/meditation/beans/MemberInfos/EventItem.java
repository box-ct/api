package tw.org.ctworld.meditation.beans.MemberInfos;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;

public class EventItem {
    private String enrollUserId;
    private String enrollUserName;
    private String ctDharmaName;
    private Integer classPeriodNum;
    private String className;
    private Integer classGroupId;
    private String eventName;
    private String arriveCtDate;
    private String leaveCtDate;
    private String idType1;
    private String idType2;
    private String unitName;

    public EventItem(String enrollUserId, String enrollUserName, String ctDharmaName, Integer classPeriodNum, String className, Integer classGroupId, String eventName, Date arriveCtDate, Date leaveCtDate, String idType1, String idType2, String unitName) {
        this.enrollUserId = enrollUserId;
        this.enrollUserName = enrollUserName;
        this.ctDharmaName = ctDharmaName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.eventName = eventName;
        this.arriveCtDate = CommonUtils.DateTime2DateString(arriveCtDate);
        this.leaveCtDate = CommonUtils.DateTime2DateString(leaveCtDate);
        this.idType1 = idType1;
        this.idType2 = idType2;
        this.unitName = unitName;
    }

    public String getEnrollUserId() {
        return enrollUserId;
    }

    public void setEnrollUserId(String enrollUserId) {
        this.enrollUserId = enrollUserId;
    }

    public String getEnrollUserName() {
        return enrollUserName;
    }

    public void setEnrollUserName(String enrollUserName) {
        this.enrollUserName = enrollUserName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public Integer getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(Integer classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(Integer classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getArriveCtDate() {
        return arriveCtDate;
    }

    public void setArriveCtDate(String arriveCtDate) {
        this.arriveCtDate = arriveCtDate;
    }

    public String getLeaveCtDate() {
        return leaveCtDate;
    }

    public void setLeaveCtDate(String leaveCtDate) {
        this.leaveCtDate = leaveCtDate;
    }

    public String getIdType1() {
        return idType1;
    }

    public void setIdType1(String idType1) {
        this.idType1 = idType1;
    }

    public String getIdType2() {
        return idType2;
    }

    public void setIdType2(String idType2) {
        this.idType2 = idType2;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
