package tw.org.ctworld.meditation.beans.Printer;

public class Printer005Object {

    private Long id;
    private String certTemplateId, certTypeName, certTemplateName, description, exampleImageSrc;

    public Printer005Object(Long id, String certTemplateId, String certTypeName, String certTemplateName,
                            String description, String exampleImageSrc) {
        this.id = id;
        this.certTemplateId = certTemplateId;
        this.certTypeName = certTypeName;
        this.certTemplateName = certTemplateName;
        this.description = description;
        this.exampleImageSrc = exampleImageSrc;
    }

    public Printer005Object(Long id, String certTemplateId, String certTypeName, String certTemplateName, String description) {
        this.id = id;
        this.certTemplateId = certTemplateId;
        this.certTypeName = certTypeName;
        this.certTemplateName = certTemplateName;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertTemplateId() {
        return certTemplateId;
    }

    public void setCertTemplateId(String certTemplateId) {
        this.certTemplateId = certTemplateId;
    }

    public String getCertTypeName() {
        return certTypeName;
    }

    public void setCertTypeName(String certTypeName) {
        this.certTypeName = certTypeName;
    }

    public String getCertTemplateName() {
        return certTemplateName;
    }

    public void setCertTemplateName(String certTemplateName) {
        this.certTemplateName = certTemplateName;
    }

    public String getDescription() {
        return description == null ? "" : description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExampleImageSrc() {
        return exampleImageSrc;
    }

    public void setExampleImageSrc(String exampleImageSrc) {
        this.exampleImageSrc = exampleImageSrc;
    }
}
