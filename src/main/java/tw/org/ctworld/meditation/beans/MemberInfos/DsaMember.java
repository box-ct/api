package tw.org.ctworld.meditation.beans.MemberInfos;

public class DsaMember {
    private String unitName;
    private String ctDsaYear;
    private String centerDsaYear;
    private String dsaJobTitleNew;

    public DsaMember(String unitName, String ctDsaYear, String centerDsaYear, String dsaJobTitleNew) {
        this.unitName = unitName;
        this.ctDsaYear = ctDsaYear;
        this.centerDsaYear = centerDsaYear;
        this.dsaJobTitleNew = dsaJobTitleNew;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getCtDsaYear() {
        return ctDsaYear;
    }

    public void setCtDsaYear(String ctDsaYear) {
        this.ctDsaYear = ctDsaYear;
    }

    public String getCenterDsaYear() {
        return centerDsaYear;
    }

    public void setCenterDsaYear(String centerDsaYear) {
        this.centerDsaYear = centerDsaYear;
    }

    public String getDsaJobTitleNew() {
        return dsaJobTitleNew;
    }

    public void setDsaJobTitleNew(String dsaJobTitleNew) {
        this.dsaJobTitleNew = dsaJobTitleNew;
    }
}
