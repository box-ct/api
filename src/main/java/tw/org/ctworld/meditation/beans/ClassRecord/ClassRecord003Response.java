package tw.org.ctworld.meditation.beans.ClassRecord;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class ClassRecord003Response extends BaseResponse {

    private List<ClassRecord003_1Object> items;

    public ClassRecord003Response(int errCode, String errMsg, List<ClassRecord003_1Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassRecord003_1Object> getItems() {
        return items;
    }

    public void setItems(List<ClassRecord003_1Object> items) {
        this.items = items;
    }


    public static class ClassRecord003_1Object {

        private String memberId, aliasName, ctDharmaName, gender, className, classGroupId, memberGroupNum, classJobTitleList;
        private boolean isDroppedClass, changeAndWaitPrintMark;
        private String classDroppedDate;
        private boolean userDefinedLabel1, userDefinedLabel2;
        private String newReturnChangeMark, newReturnChangeDate, classErollFormNote;
        private List<ClassRecord003_2Object> attendRecordList;

        public ClassRecord003_1Object(String memberId, String aliasName, String ctDharmaName, String gender,
                                      String className, String classGroupId, String memberGroupNum,
                                      String classJobTitleList, boolean isDroppedClass, boolean changeAndWaitPrintMark,
                                      String classDroppedDate, boolean userDefinedLabel1, boolean userDefinedLabel2,
                                      String newReturnChangeMark, String newReturnChangeDate, String classErollFormNote,
                                      List<ClassRecord003_2Object> attendRecordList) {
            this.memberId = memberId;
            this.aliasName = aliasName;
            this.ctDharmaName = ctDharmaName;
            this.gender = gender;
            this.className = className;
            this.classGroupId = classGroupId;
            this.memberGroupNum = memberGroupNum;
            this.classJobTitleList = classJobTitleList;
            this.isDroppedClass = isDroppedClass;
            this.changeAndWaitPrintMark = changeAndWaitPrintMark;
            this.classDroppedDate = classDroppedDate;
            this.userDefinedLabel1 = userDefinedLabel1;
            this.userDefinedLabel2 = userDefinedLabel2;
            this.newReturnChangeMark = newReturnChangeMark;
            this.newReturnChangeDate = newReturnChangeDate;
            this.classErollFormNote = classErollFormNote;
            this.attendRecordList = attendRecordList;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getAliasName() {
            return aliasName;
        }

        public void setAliasName(String aliasName) {
            this.aliasName = aliasName;
        }

        public String getCtDharmaName() {
            return ctDharmaName;
        }

        public void setCtDharmaName(String ctDharmaName) {
            this.ctDharmaName = ctDharmaName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(String classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getMemberGroupNum() {
            return memberGroupNum;
        }

        public void setMemberGroupNum(String memberGroupNum) {
            this.memberGroupNum = memberGroupNum;
        }

        public String getClassJobTitleList() {
            return classJobTitleList;
        }

        public void setClassJobTitleList(String classJobTitleList) {
            this.classJobTitleList = classJobTitleList;
        }

        public boolean getIsDroppedClass() {
            return isDroppedClass;
        }

        public void setIsDroppedClass(boolean droppedClass) {
            isDroppedClass = droppedClass;
        }

        public boolean isChangeAndWaitPrintMark() {
            return changeAndWaitPrintMark;
        }

        public void setChangeAndWaitPrintMark(boolean changeAndWaitPrintMark) {
            this.changeAndWaitPrintMark = changeAndWaitPrintMark;
        }

        public String getClassDroppedDate() {
            return classDroppedDate;
        }

        public void setClassDroppedDate(String classDroppedDate) {
            this.classDroppedDate = classDroppedDate;
        }

        public boolean getUserDefinedLabel1() {
            return userDefinedLabel1;
        }

        public void setUserDefinedLabel1(boolean userDefinedLabel1) {
            this.userDefinedLabel1 = userDefinedLabel1;
        }

        public boolean getUserDefinedLabel2() {
            return userDefinedLabel2;
        }

        public void setUserDefinedLabel2(boolean userDefinedLabel2) {
            this.userDefinedLabel2 = userDefinedLabel2;
        }

        public String getNewReturnChangeMark() {
            return newReturnChangeMark;
        }

        public void setNewReturnChangeMark(String newReturnChangeMark) {
            this.newReturnChangeMark = newReturnChangeMark;
        }

        public String getNewReturnChangeDate() {
            return newReturnChangeDate;
        }

        public void setNewReturnChangeDate(String newReturnChangeDate) {
            this.newReturnChangeDate = newReturnChangeDate;
        }

        public String getClassErollFormNote() {
            return classErollFormNote;
        }

        public void setClassErollFormNote(String classErollFormNote) {
            this.classErollFormNote = classErollFormNote;
        }

        public List<ClassRecord003_2Object> getAttendRecordList() {
            return attendRecordList;
        }

        public void setAttendRecordList(List<ClassRecord003_2Object> attendRecordList) {
            this.attendRecordList = attendRecordList;
        }
    }

    public static class ClassRecord003_2Object {

        private int classWeeksNum;
        private String classDate, attendMark;

        public ClassRecord003_2Object(int classWeeksNum, String classDate, String attendMark) {
            this.classWeeksNum = classWeeksNum;
            this.classDate = classDate;
            this.attendMark = attendMark;
        }

        public int getClassWeeksNum() {
            return classWeeksNum;
        }

        public void setClassWeeksNum(int classWeeksNum) {
            this.classWeeksNum = classWeeksNum;
        }

        public String getClassDate() {
            return classDate;
        }

        public void setClassDate(String classDate) {
            this.classDate = classDate;
        }

        public String getAttendMark() {
            return attendMark;
        }

        public void setAttendMark(String attendMark) {
            this.attendMark = attendMark;
        }
    }
}
