package tw.org.ctworld.meditation.beans.ClassRecord;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class ClassRecord007Response extends BaseResponse {

    private List<ClassRecord007Object> items;

    public ClassRecord007Response(int errCode, String errMsg, List<ClassRecord007Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassRecord007Object> getItems() {
        return items;
    }

    public void setItems(List<ClassRecord007Object> items) {
        this.items = items;
    }
}
