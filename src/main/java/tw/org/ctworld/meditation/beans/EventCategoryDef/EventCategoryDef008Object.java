package tw.org.ctworld.meditation.beans.EventCategoryDef;

public class EventCategoryDef008Object {

    private String pgmCategoryId, pgmCategoryName, pgmId, pgmName, pgmDesc;
    private Boolean isGenPgm;
    private String genPgmUiType, genPgmInputOption, pgmStartTime, pgmEndTime;
    private Boolean isCenterAvailable;
    private String pgmNote;

    public String getPgmCategoryId() {
        return pgmCategoryId;
    }

    public void setPgmCategoryId(String pgmCategoryId) {
        this.pgmCategoryId = pgmCategoryId;
    }

    public String getPgmCategoryName() {
        return pgmCategoryName;
    }

    public void setPgmCategoryName(String pgmCategoryName) {
        this.pgmCategoryName = pgmCategoryName;
    }

    public String getPgmId() {
        return pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }

    public String getPgmName() {
        return pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

    public String getPgmDesc() {
        return pgmDesc;
    }

    public void setPgmDesc(String pgmDesc) {
        this.pgmDesc = pgmDesc;
    }

    public Boolean getIsGenPgm() {
        return isGenPgm;
    }

    public void setIsGenPgm(Boolean genPgm) {
        isGenPgm = genPgm;
    }

    public String getGenPgmUiType() {
        return genPgmUiType;
    }

    public void setGenPgmUiType(String genPgmUiType) {
        this.genPgmUiType = genPgmUiType;
    }

    public String getGenPgmInputOption() {
        return genPgmInputOption;
    }

    public void setGenPgmInputOption(String genPgmInputOption) {
        this.genPgmInputOption = genPgmInputOption;
    }

    public String getPgmStartTime() {
        return pgmStartTime;
    }

    public void setPgmStartTime(String pgmStartTime) {
        this.pgmStartTime = pgmStartTime;
    }

    public String getPgmEndTime() {
        return pgmEndTime;
    }

    public void setPgmEndTime(String pgmEndTime) {
        this.pgmEndTime = pgmEndTime;
    }

    public Boolean getIsCenterAvailable() {
        return isCenterAvailable;
    }

    public void setIsCenterAvailable(Boolean centerAvailable) {
        isCenterAvailable = centerAvailable;
    }

    public String getPgmNote() {
        return pgmNote;
    }

    public void setPgmNote(String pgmNote) {
        this.pgmNote = pgmNote;
    }
}
