package tw.org.ctworld.meditation.beans;

public class ResultResponse<T> extends BaseResponse{
    private T result;

    public ResultResponse(int errCode, String errMsg, T result) {
        super(errCode, errMsg);
        this.result = result;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
