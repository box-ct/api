package tw.org.ctworld.meditation.beans.Anonymous;

public class AnonymousMakeupRequest {

    private String memberId, classId;
    private int classWeeksNum, latestListenFlag;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public int getLatestListenFlag() {
        return latestListenFlag;
    }

    public void setLatestListenFlag(int latestListenFlag) {
        this.latestListenFlag = latestListenFlag;
    }
}
