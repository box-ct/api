package tw.org.ctworld.meditation.beans.UnitStartUp;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;

public class UnitStartUp002_1Object {
    
    private String eventYear, sponsorUnitName, eventName, eventStartDate, eventEndDate;
    private int masterEnrollCount, notMasterEnrollCount;
    private Boolean isNeedAbbotSigned;
    private int notAbbotSignedCount, peopleForecast, status;
    private String eventNote, eventId;

    public UnitStartUp002_1Object(String eventYear, String sponsorUnitName, String eventName, Date eventStartDate,
                                  Date eventEndDate, Boolean isNeedAbbotSigned, int status, String eventNote,
                                  String eventId) {
        this.eventYear = eventYear;
        this.sponsorUnitName = sponsorUnitName;
        this.eventName = eventName;
        this.eventStartDate = DateTime2DateString(eventStartDate);
        this.eventEndDate = DateTime2DateString(eventEndDate);
        this.isNeedAbbotSigned = isNeedAbbotSigned;
        this.status = status;
        this.eventNote = eventNote;
        this.eventId = eventId;
    }

    public UnitStartUp002_1Object(String eventYear, String sponsorUnitName, String eventName, String eventStartDate,
                                  String eventEndDate, int masterEnrollCount, int notMasterEnrollCount,
                                  Boolean isNeedAbbotSigned, int notAbbotSignedCount, int peopleForecast,
                                  int status, String eventNote, String eventId) {
        this.eventYear = eventYear;
        this.sponsorUnitName = sponsorUnitName;
        this.eventName = eventName;
        this.eventStartDate = eventStartDate;
        this.eventEndDate = eventEndDate;
        this.masterEnrollCount = masterEnrollCount;
        this.notMasterEnrollCount = notMasterEnrollCount;
        this.isNeedAbbotSigned = isNeedAbbotSigned;
        this.notAbbotSignedCount = notAbbotSignedCount;
        this.peopleForecast = peopleForecast;
        this.status = status;
        this.eventNote = eventNote;
        this.eventId = eventId;
    }

    public String getEventYear() {
        return eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public String getSponsorUnitName() {
        return sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public int getMasterEnrollCount() {
        return masterEnrollCount;
    }

    public void setMasterEnrollCount(int masterEnrollCount) {
        this.masterEnrollCount = masterEnrollCount;
    }

    public int getNotMasterEnrollCount() {
        return notMasterEnrollCount;
    }

    public void setNotMasterEnrollCount(int notMasterEnrollCount) {
        this.notMasterEnrollCount = notMasterEnrollCount;
    }

    public Boolean getIsNeedAbbotSigned() {
        return isNeedAbbotSigned;
    }

    public void setIsNeedAbbotSigned(Boolean needAbbotSigned) {
        isNeedAbbotSigned = needAbbotSigned;
    }

    public int getNotAbbotSignedCount() {
        return notAbbotSignedCount;
    }

    public void setNotAbbotSignedCount(int notAbbotSignedCount) {
        this.notAbbotSignedCount = notAbbotSignedCount;
    }

    public int getPeopleForecast() {
        return peopleForecast;
    }

    public void setPeopleForecast(int peopleForecast) {
        this.peopleForecast = peopleForecast;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getEventNote() {
        return eventNote;
    }

    public void setEventNote(String eventNote) {
        this.eventNote = eventNote;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
