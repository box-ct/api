package tw.org.ctworld.meditation.beans.ClassRecord;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class ClassRecord002Response extends BaseResponse {

    private List<ClassRecord002object> items;

    public ClassRecord002Response(int errCode, String errMsg, List<ClassRecord002object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassRecord002object> getItems() {
        return items;
    }

    public void setItems(List<ClassRecord002object> items) {
        this.items = items;
    }
}
