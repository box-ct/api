package tw.org.ctworld.meditation.beans.ClassUpgrade;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class ClassUpgrade001Response extends BaseResponse {

    private List<String> items;

    public ClassUpgrade001Response(int errCode, String errMsg, List<String> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
