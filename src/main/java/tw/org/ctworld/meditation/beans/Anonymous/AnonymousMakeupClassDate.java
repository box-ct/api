package tw.org.ctworld.meditation.beans.Anonymous;

public class AnonymousMakeupClassDate {

    private int classWeeksNum, fileTimeTotalLength, latestListenFlag;
    private String classDate, classContent, classDayOfWeek, makeupFileName;
    private boolean isNeedMakeup, isComplete;

    public AnonymousMakeupClassDate(int classWeeksNum, String classDate, String classContent, String classDayOfWeek, String makeupFileName, int fileTimeTotalLength, int latestListenFlag, boolean isNeedMakeup, boolean isComplete) {
        this.classWeeksNum = classWeeksNum;
        this.fileTimeTotalLength = fileTimeTotalLength;
        this.latestListenFlag = latestListenFlag;
        this.classDate = classDate;
        this.classContent = classContent;
        this.classDayOfWeek = classDayOfWeek;
        this.makeupFileName = makeupFileName;
        this.isNeedMakeup = isNeedMakeup;
        this.isComplete = isComplete;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public int getFileTimeTotalLength() {
        return fileTimeTotalLength;
    }

    public void setFileTimeTotalLength(int fileTimeTotalLength) {
        this.fileTimeTotalLength = fileTimeTotalLength;
    }

    public int getLatestListenFlag() {
        return latestListenFlag;
    }

    public void setLatestListenFlag(int latestListenFlag) {
        this.latestListenFlag = latestListenFlag;
    }

    public String getClassDate() {
        return classDate;
    }

    public void setClassDate(String classDate) {
        this.classDate = classDate;
    }

    public String getClassContent() {
        return classContent;
    }

    public void setClassContent(String classContent) {
        this.classContent = classContent;
    }

    public String getClassDayOfWeek() {
        return classDayOfWeek;
    }

    public void setClassDayOfWeek(String classDayOfWeek) {
        this.classDayOfWeek = classDayOfWeek;
    }

    public String getMakeupFileName() {
        return makeupFileName;
    }

    public void setMakeupFileName(String makeupFileName) {
        this.makeupFileName = makeupFileName;
    }

    public boolean isNeedMakeup() {
        return isNeedMakeup;
    }

    public void setNeedMakeup(boolean needMakeup) {
        isNeedMakeup = needMakeup;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
