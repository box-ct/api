package tw.org.ctworld.meditation.beans.UnitStartUp;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class UnitStartUp006Response extends BaseResponse {

    private List<UnitStartUp006Object> items;

    public UnitStartUp006Response(int errCode, String errMsg, List<UnitStartUp006Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<UnitStartUp006Object> getItems() {
        return items;
    }

    public void setItems(List<UnitStartUp006Object> items) {
        this.items = items;
    }
}
