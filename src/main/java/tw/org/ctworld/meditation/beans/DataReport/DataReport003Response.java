package tw.org.ctworld.meditation.beans.DataReport;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class DataReport003Response extends BaseResponse {

    private List<DataReport003_1Object> items;

    public DataReport003Response(int errCode, String errMsg, List<DataReport003_1Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<DataReport003_1Object> getItems() {
        return items;
    }

    public void setItems(List<DataReport003_1Object> items) {
        this.items = items;
    }


    public static class DataReport003_1Object {

        private String unitName;
        private List<DataReport003_2Object> classes;

        public DataReport003_1Object(String unitName, List<DataReport003_2Object> classes) {
            this.unitName = unitName;
            this.classes = classes;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }

        public List<DataReport003_2Object> getClasses() {
            return classes;
        }

        public void setClasses(List<DataReport003_2Object> classes) {
            this.classes = classes;
        }
    }

    public static class DataReport003_2Object {

        private String title, number;

        public DataReport003_2Object(String title, String number) {
            this.title = title;
            this.number = number;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }
    }
}
