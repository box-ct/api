package tw.org.ctworld.meditation.beans.Member;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Members006Response extends BaseResponse {

    private List<Members006Object> items;

    public Members006Response(int errCode, String errMsg, List<Members006Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Members006Object> getItems() {
        return items;
    }

    public void setItems(List<Members006Object> items) {
        this.items = items;
    }
}
