package tw.org.ctworld.meditation.beans.UnitStartUp;

public class UnitStartUp004Object {

    private int id;
    private String sInputId, sInputName, sInputValue;
    private Boolean isRequired;
    private String sInputExample;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getsInputId() {
        return sInputId;
    }

    public void setsInputId(String sInputId) {
        this.sInputId = sInputId;
    }

    public String getsInputName() {
        return sInputName;
    }

    public void setsInputName(String sInputName) {
        this.sInputName = sInputName;
    }

    public String getsInputValue() {
        return sInputValue;
    }

    public void setsInputValue(String sInputValue) {
        this.sInputValue = sInputValue;
    }

    public Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean required) {
        isRequired = required;
    }

    public String getsInputExample() {
        return sInputExample;
    }

    public void setsInputExample(String sInputExample) {
        this.sInputExample = sInputExample;
    }
}
