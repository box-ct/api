package tw.org.ctworld.meditation.beans.Anonymous;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class Anonymous008Response extends BaseResponse {

    private int attendRate;
    private String attendDt;

    public Anonymous008Response(int errCode, String errMsg, int attendRate, String attendDt) {
        super(errCode, errMsg);
        this.attendRate = attendRate;
        this.attendDt = attendDt;
    }

    public Anonymous008Response(int attendRate, String attendDt) {
        this.attendRate = attendRate;
        this.attendDt = attendDt;
    }

    public int getAttendRate() {
        return attendRate;
    }

    public void setAttendRate(int attendRate) {
        this.attendRate = attendRate;
    }

    public String getAttendDt() {
        return attendDt;
    }

    public void setAttendDt(String attendDt) {
        this.attendDt = attendDt;
    }
}
