package tw.org.ctworld.meditation.beans.ClassGroup;

public class ClassGroup001Object {

    private String classId, classPeriodNum, className;

    public ClassGroup001Object(String classId, String classPeriodNum, String className) {
        this.classId = classId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
