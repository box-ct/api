package tw.org.ctworld.meditation.beans.MemberInfos;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;

public class MemberNote {
    private long id;
    private String note;
    private String noteType;
    private String createDtTm;
    private String creatorName;
    private String updateDtTm;
    private String updatorName;

    public MemberNote(long id, String note, String noteType, Date createDtTm, String creatorName, Date updateDtTm, String updatorName) {
        this.id = id;
        this.note = note;
        this.noteType = noteType;
        this.createDtTm = CommonUtils.DateTime2DateTimeString(createDtTm);
        this.creatorName = creatorName;
        this.updateDtTm = CommonUtils.DateTime2DateTimeString(updateDtTm);
        this.updatorName = updatorName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }

    public String getCreateDtTm() {
        return createDtTm;
    }

    public void setCreateDtTm(String createDtTm) {
        this.createDtTm = createDtTm;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(String updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }
}
