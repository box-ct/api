package tw.org.ctworld.meditation.beans.ClassRecord;

import java.util.List;

public class ClassRecord004Request {

    private List<memberObject> memberList;

    public List<memberObject> getMemberList() {
        return memberList;
    }

    public void setMemberList(List<memberObject> memberList) {
        this.memberList = memberList;
    }


    public static class memberObject {

        private String memberId;
        private List<attendRecordObject> attendRecordList;

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public List<attendRecordObject> getAttendRecordList() {
            return attendRecordList;
        }

        public void setAttendRecordList(List<attendRecordObject> attendRecordList) {
            this.attendRecordList = attendRecordList;
        }
    }

    public static class attendRecordObject {

        private String className;
        private int classWeeksNum;
        private String classDate, attendMark;

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public int getClassWeeksNum() {
            return classWeeksNum;
        }

        public void setClassWeeksNum(int classWeeksNum) {
            this.classWeeksNum = classWeeksNum;
        }

        public String getClassDate() {
            return classDate;
        }

        public void setClassDate(String classDate) {
            this.classDate = classDate;
        }

        public String getAttendMark() {
            return attendMark;
        }

        public void setAttendMark(String attendMark) {
            this.attendMark = attendMark;
        }
    }
}
