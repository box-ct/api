package tw.org.ctworld.meditation.beans.Classes;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Classes009Response extends BaseResponse {

    private long id;
    private String classEnrollFormId, memberId, gender, aliasName, ctDharmaName, classGroupId, memberGroupNum,
            classJobTitleList, classJobContentList;
    private boolean isLeadGroup;
    private String managedClassList;

    public Classes009Response(int errCode, String errMsg, long id, String classEnrollFormId, String memberId,
                              String gender, String aliasName, String ctDharmaName, String classGroupId,
                              String memberGroupNum, String classJobTitleList, String classJobContentList,
                              boolean isLeadGroup, String managedClassList) {
        super(errCode, errMsg);
        this.id = id;
        this.classEnrollFormId = classEnrollFormId;
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classJobTitleList = classJobTitleList;
        this.classJobContentList = classJobContentList;
        this.isLeadGroup = isLeadGroup;
        this.managedClassList = managedClassList;
    }

    public Classes009Response(long id, String classEnrollFormId, String memberId, String gender, String aliasName,
                              String ctDharmaName, String classGroupId, String memberGroupNum, String classJobTitleList,
                              String classJobContentList, boolean isLeadGroup, String managedClassList) {
        this.id = id;
        this.classEnrollFormId = classEnrollFormId;
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classJobTitleList = classJobTitleList;
        this.classJobContentList = classJobContentList;
        this.isLeadGroup = isLeadGroup;
        this.managedClassList = managedClassList;
    }

    public Classes009Response(long id, String classEnrollFormId, String memberId, String gender, String aliasName,
                              String ctDharmaName, String classGroupId, String memberGroupNum, String classJobTitleList,
                              String classJobContentList, boolean isLeadGroup) {
        this.id = id;
        this.classEnrollFormId = classEnrollFormId;
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classJobTitleList = classJobTitleList;
        this.classJobContentList = classJobContentList;
        this.isLeadGroup = isLeadGroup;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public String getClassJobContentList() {
        return classJobContentList;
    }

    public void setClassJobContentList(String classJobContentList) {
        this.classJobContentList = classJobContentList;
    }

    public boolean getIsLeadGroup() {
        return isLeadGroup;
    }

    public void setIsLeadGroup(boolean leadGroup) {
        isLeadGroup = leadGroup;
    }

    public String getManagedClassList() {
        return managedClassList;
    }

    public void setManagedClassList(String managedClassList) {
        this.managedClassList = managedClassList;
    }
}
