package tw.org.ctworld.meditation.beans.ClassRecord;

public class ClassRecord002object {

    private String classId, className, classPeriodNum, classStatus, classDayOrNight;
    private int classTypeNum;

    public ClassRecord002object(String classId, String className) {
        this.classId = classId;
        this.className = className;
    }

    public ClassRecord002object(String classId, String className, String classPeriodNum) {
        this.classId = classId;
        this.className = className;
        this.classPeriodNum = classPeriodNum;
    }

    public ClassRecord002object(String classId, String className, String classPeriodNum, String classStatus, String classDayOrNight, int classTypeNum) {
        this.classId = classId;
        this.className = className;
        this.classPeriodNum = classPeriodNum;
        this.classStatus = classStatus;
        this.classDayOrNight = classDayOrNight;
        this.classTypeNum = classTypeNum;
    }


    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassStatus() {
        return classStatus;
    }

    public void setClassStatus(String classStatus) {
        this.classStatus = classStatus;
    }

    public String getClassDayOrNight() {
        return classDayOrNight;
    }

    public void setClassDayOrNight(String classDayOrNight) {
        this.classDayOrNight = classDayOrNight;
    }

    public int getClassTypeNum() {
        return classTypeNum;
    }

    public void setClassTypeNum(int classTypeNum) {
        this.classTypeNum = classTypeNum;
    }
}
