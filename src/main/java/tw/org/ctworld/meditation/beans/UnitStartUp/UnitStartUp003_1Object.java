package tw.org.ctworld.meditation.beans.UnitStartUp;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;
import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateTimeString;

public class UnitStartUp003_1Object {

    private String eventYear, sponsorUnitName, eventName, eventStartDate, eventEndDate, eventNote, eventId, eventStartTime, eventEndTime;
    private int masterEnrollCount, notMasterEnrollCount, notAbbotSignedCount, peopleForecast, status;
    private Boolean isNeedAbbotSigned;

    public UnitStartUp003_1Object(String eventYear, String sponsorUnitName, String eventName, Date eventStartDate,
                                  Date eventEndDate, Boolean isNeedAbbotSigned, int status, String eventNote,
                                  String eventId, Date eventStartTime, Date eventEndTime) {
        this.eventYear = eventYear;
        this.sponsorUnitName = sponsorUnitName;
        this.eventName = eventName;
        this.eventStartDate = DateTime2DateString(eventStartDate);
        this.eventEndDate = DateTime2DateString(eventEndDate);
        this.masterEnrollCount = 0;
        this.notMasterEnrollCount = 0;
        this.isNeedAbbotSigned = isNeedAbbotSigned;
        this.notAbbotSignedCount = 0;
        this.peopleForecast = 0;
        this.status = status;
        this.eventNote = eventNote;
        this.eventId = eventId;

        this.eventStartTime = CommonUtils.DateTime2TimeString(eventStartTime);
        this.eventEndTime = CommonUtils.DateTime2TimeString(eventEndTime);
    }

    public String getEventYear() {
        return eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public String getSponsorUnitName() {
        return sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public int getMasterEnrollCount() {
        return masterEnrollCount;
    }

    public void setMasterEnrollCount(int masterEnrollCount) {
        this.masterEnrollCount = masterEnrollCount;
    }

    public int getNotMasterEnrollCount() {
        return notMasterEnrollCount;
    }

    public void setNotMasterEnrollCount(int notMasterEnrollCount) {
        this.notMasterEnrollCount = notMasterEnrollCount;
    }

    public Boolean getIsNeedAbbotSigned() {
        return isNeedAbbotSigned;
    }

    public void setIsNeedAbbotSigned(Boolean needAbbotSigned) {
        isNeedAbbotSigned = needAbbotSigned;
    }

    public int getNotAbbotSignedCount() {
        return notAbbotSignedCount;
    }

    public void setNotAbbotSignedCount(int notAbbotSignedCount) {
        this.notAbbotSignedCount = notAbbotSignedCount;
    }

    public int getPeopleForecast() {
        return peopleForecast;
    }

    public void setPeopleForecast(int peopleForecast) {
        this.peopleForecast = peopleForecast;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getEventNote() {
        return eventNote;
    }

    public void setEventNote(String eventNote) {
        this.eventNote = eventNote;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

}
