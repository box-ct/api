package tw.org.ctworld.meditation.beans.MemberInfos;

public class MemberInfos001_1Object {

    private String memberId, gender, aliasName, ctDharmaName, className;
    private int age;
    private String homePhoneNum1, mobileNum1, dsaJobNameList;

    public MemberInfos001_1Object(String memberId, String gender, String aliasName, String ctDharmaName,
                                  String className, int age, String homePhoneNum1, String mobileNum1,
                                  String dsaJobNameList) {
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.className = className;
        this.age = age;
        this.homePhoneNum1 = homePhoneNum1;
        this.mobileNum1 = mobileNum1;
        this.dsaJobNameList = dsaJobNameList;
    }

    public MemberInfos001_1Object(String memberId, String gender, String aliasName, String ctDharmaName,
                                  Integer age, String homePhoneNum1, String mobileNum1, String dsaJobNameList) {
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.age = age == null ? 0 : age;
        this.homePhoneNum1 = homePhoneNum1;
        this.mobileNum1 = mobileNum1;
        this.dsaJobNameList = dsaJobNameList;
    }

    public MemberInfos001_1Object(String memberId, String className) {
        this.memberId = memberId;
        this.className = className;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList == null ? "" : dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }
}
