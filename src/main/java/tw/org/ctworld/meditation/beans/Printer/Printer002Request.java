package tw.org.ctworld.meditation.beans.Printer;

import java.util.List;

public class Printer002Request {

    private String printerName, sourcePrinterId, note;
    private List<String> certTypeNames;

    public String getPrinterName() {
        return printerName;
    }

    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    public String getSourcePrinterId() {
        return sourcePrinterId;
    }

    public void setSourcePrinterId(String sourcePrinterId) {
        this.sourcePrinterId = sourcePrinterId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getCertTypeNames() {
        return certTypeNames;
    }

    public void setCertTypeNames(List<String> certTypeNames) {
        this.certTypeNames = certTypeNames;
    }
}
