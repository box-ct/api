package tw.org.ctworld.meditation.beans.UnitEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class UnitEventsMaintain002Response extends BaseResponse {

    private List<UnitEventsMaintain002Object> items;

    public UnitEventsMaintain002Response(int errCode, String errMsg, List<UnitEventsMaintain002Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<UnitEventsMaintain002Object> getItems() {
        return items;
    }

    public void setItems(List<UnitEventsMaintain002Object> items) {
        this.items = items;
    }
}
