package tw.org.ctworld.meditation.beans.Member;

public class Members002ProfileObject {

    private String fullName, aliasName, ctDharmaName, memberId, gender, twIdNum, email1;
    private int age;
    private String schoolDegree, schoolName, schoolMajor;
    private boolean okSnedMessage, okSendMagazine, allOk;
    private String mailingCountry, mailingState, mailingCity, mailingStreet, companyName, companyJobTitle,
            officePhoneNum1, familyLeaderName, familyLeaderRelationship, urgentContactPersonName1,
            urgentContactPersonPhoneNum1, urgentContactPersonRelationship1, introducerName, introducerPhoneNum,
            introducerRelationship, comingReasonId;
    private boolean takeRefuge;
    private String refugeDate, refugePlace;
    private boolean takePrecept5;
    private String precept5Date, precept5Place;
    private boolean takeBodhiPrecept;
    private String bodhiPreceptDate, bodhiPreceptPlace;
    private String dataUnitMemberId, dataSentToCTDate, creatorName, createDt, firstDt, healthList, skillList,
            thisPersonStatus, dsaJobNameList, birthDate, birthCountry, birthCity, homePhoneNum1, mobileNum1;

    public Members002ProfileObject(String fullName, String aliasName, String ctDharmaName, String memberId,
                                   String gender, String twIdNum, String email1, int age, String schoolDegree,
                                   String schoolName, String schoolMajor, boolean okSnedMessage, boolean okSendMagazine,
                                   boolean allOk, String mailingCountry, String mailingState, String mailingCity,
                                   String mailingStreet, String companyName, String companyJobTitle,
                                   String officePhoneNum1, String familyLeaderName, String familyLeaderRelationship,
                                   String urgentContactPersonName1, String urgentContactPersonPhoneNum1,
                                   String urgentContactPersonRelationship1, String introducerName,
                                   String introducerPhoneNum, String introducerRelationship, String comingReasonId,
                                   boolean takeRefuge, String refugeDate, String refugePlace, boolean takePrecept5,
                                   String precept5Date, String precept5Place, boolean takeBodhiPrecept,
                                   String bodhiPreceptDate, String bodhiPreceptPlace, String dataUnitMemberId,
                                   String dataSentToCTDate, String creatorName, String createDt, String firstDt,
                                   String healthList, String skillList, String thisPersonStatus, String dsaJobNameList,
                                   String birthDate, String birthCountry, String birthCity, String homePhoneNum1,
                                   String mobileNum1) {
        this.fullName = fullName;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.memberId = memberId;
        this.gender = gender;
        this.twIdNum = twIdNum;
        this.email1 = email1;
        this.age = age;
        this.schoolDegree = schoolDegree;
        this.schoolName = schoolName;
        this.schoolMajor = schoolMajor;
        this.okSnedMessage = okSnedMessage;
        this.okSendMagazine = okSendMagazine;
        this.allOk = allOk;
        this.mailingCountry = mailingCountry;
        this.mailingState = mailingState;
        this.mailingCity = mailingCity;
        this.mailingStreet = mailingStreet;
        this.companyName = companyName;
        this.companyJobTitle = companyJobTitle;
        this.officePhoneNum1 = officePhoneNum1;
        this.familyLeaderName = familyLeaderName;
        this.familyLeaderRelationship = familyLeaderRelationship;
        this.urgentContactPersonName1 = urgentContactPersonName1;
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
        this.introducerName = introducerName;
        this.introducerPhoneNum = introducerPhoneNum;
        this.introducerRelationship = introducerRelationship;
        this.comingReasonId = comingReasonId;
        this.takeRefuge = takeRefuge;
        this.refugeDate = refugeDate;
        this.refugePlace = refugePlace;
        this.takePrecept5 = takePrecept5;
        this.precept5Date = precept5Date;
        this.precept5Place = precept5Place;
        this.takeBodhiPrecept = takeBodhiPrecept;
        this.bodhiPreceptDate = bodhiPreceptDate;
        this.bodhiPreceptPlace = bodhiPreceptPlace;
        this.dataUnitMemberId = dataUnitMemberId;
        this.dataSentToCTDate = dataSentToCTDate;
        this.creatorName = creatorName;
        this.createDt = createDt;
        this.firstDt = firstDt;
        this.healthList = healthList;
        this.skillList = skillList;
        this.thisPersonStatus = thisPersonStatus;
        this.dsaJobNameList = dsaJobNameList;
        this.birthDate = birthDate;
        this.birthCountry = birthCountry;
        this.birthCity = birthCity;
        this.homePhoneNum1 = homePhoneNum1;
        this.mobileNum1 = mobileNum1;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSchoolDegree() {
        return schoolDegree;
    }

    public void setSchoolDegree(String schoolDegree) {
        this.schoolDegree = schoolDegree;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolMajor() {
        return schoolMajor;
    }

    public void setSchoolMajor(String schoolMajor) {
        this.schoolMajor = schoolMajor;
    }

    public boolean isOkSnedMessage() {
        return okSnedMessage;
    }

    public void setOkSnedMessage(boolean okSnedMessage) {
        this.okSnedMessage = okSnedMessage;
    }

    public boolean isOkSendMagazine() {
        return okSendMagazine;
    }

    public void setOkSendMagazine(boolean okSendMagazine) {
        this.okSendMagazine = okSendMagazine;
    }

    public boolean isAllOk() {
        return allOk;
    }

    public void setAllOk(boolean allOk) {
        this.allOk = allOk;
    }

    public String getMailingCountry() {
        return mailingCountry;
    }

    public void setMailingCountry(String mailingCountry) {
        this.mailingCountry = mailingCountry;
    }

    public String getMailingState() {
        return mailingState;
    }

    public void setMailingState(String mailingState) {
        this.mailingState = mailingState;
    }

    public String getMailingCity() {
        return mailingCity;
    }

    public void setMailingCity(String mailingCity) {
        this.mailingCity = mailingCity;
    }

    public String getMailingStreet() {
        return mailingStreet;
    }

    public void setMailingStreet(String mailingStreet) {
        this.mailingStreet = mailingStreet;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyJobTitle() {
        return companyJobTitle;
    }

    public void setCompanyJobTitle(String companyJobTitle) {
        this.companyJobTitle = companyJobTitle;
    }

    public String getOfficePhoneNum1() {
        return officePhoneNum1;
    }

    public void setOfficePhoneNum1(String officePhoneNum1) {
        this.officePhoneNum1 = officePhoneNum1;
    }

    public String getFamilyLeaderName() {
        return familyLeaderName;
    }

    public void setFamilyLeaderName(String familyLeaderName) {
        this.familyLeaderName = familyLeaderName;
    }

    public String getFamilyLeaderRelationship() {
        return familyLeaderRelationship;
    }

    public void setFamilyLeaderRelationship(String familyLeaderRelationship) {
        this.familyLeaderRelationship = familyLeaderRelationship;
    }

    public String getUrgentContactPersonName1() {
        return urgentContactPersonName1;
    }

    public void setUrgentContactPersonName1(String urgentContactPersonName1) {
        this.urgentContactPersonName1 = urgentContactPersonName1;
    }

    public String getUrgentContactPersonPhoneNum1() {
        return urgentContactPersonPhoneNum1;
    }

    public void setUrgentContactPersonPhoneNum1(String urgentContactPersonPhoneNum1) {
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
    }

    public String getUrgentContactPersonRelationship1() {
        return urgentContactPersonRelationship1;
    }

    public void setUrgentContactPersonRelationship1(String urgentContactPersonRelationship1) {
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
    }

    public String getIntroducerName() {
        return introducerName;
    }

    public void setIntroducerName(String introducerName) {
        this.introducerName = introducerName;
    }

    public String getIntroducerPhoneNum() {
        return introducerPhoneNum;
    }

    public void setIntroducerPhoneNum(String introducerPhoneNum) {
        this.introducerPhoneNum = introducerPhoneNum;
    }

    public String getIntroducerRelationship() {
        return introducerRelationship;
    }

    public void setIntroducerRelationship(String introducerRelationship) {
        this.introducerRelationship = introducerRelationship;
    }

    public String getComingReasonId() {
        return comingReasonId;
    }

    public void setComingReasonId(String comingReasonId) {
        this.comingReasonId = comingReasonId;
    }

    public boolean isTakeRefuge() {
        return takeRefuge;
    }

    public void setTakeRefuge(boolean takeRefuge) {
        this.takeRefuge = takeRefuge;
    }

    public String getRefugeDate() {
        return refugeDate;
    }

    public void setRefugeDate(String refugeDate) {
        this.refugeDate = refugeDate;
    }

    public String getRefugePlace() {
        return refugePlace;
    }

    public void setRefugePlace(String refugePlace) {
        this.refugePlace = refugePlace;
    }

    public boolean isTakePrecept5() {
        return takePrecept5;
    }

    public void setTakePrecept5(boolean takePrecept5) {
        this.takePrecept5 = takePrecept5;
    }

    public String getPrecept5Date() {
        return precept5Date;
    }

    public void setPrecept5Date(String precept5Date) {
        this.precept5Date = precept5Date;
    }

    public String getPrecept5Place() {
        return precept5Place;
    }

    public void setPrecept5Place(String precept5Place) {
        this.precept5Place = precept5Place;
    }

    public boolean isTakeBodhiPrecept() {
        return takeBodhiPrecept;
    }

    public void setTakeBodhiPrecept(boolean takeBodhiPrecept) {
        this.takeBodhiPrecept = takeBodhiPrecept;
    }

    public String getBodhiPreceptDate() {
        return bodhiPreceptDate;
    }

    public void setBodhiPreceptDate(String bodhiPreceptDate) {
        this.bodhiPreceptDate = bodhiPreceptDate;
    }

    public String getBodhiPreceptPlace() {
        return bodhiPreceptPlace;
    }

    public void setBodhiPreceptPlace(String bodhiPreceptPlace) {
        this.bodhiPreceptPlace = bodhiPreceptPlace;
    }

    public String getDataUnitMemberId() {
        return dataUnitMemberId;
    }

    public void setDataUnitMemberId(String dataUnitMemberId) {
        this.dataUnitMemberId = dataUnitMemberId;
    }

    public String getDataSentToCTDate() {
        return dataSentToCTDate;
    }

    public void setDataSentToCTDate(String dataSentToCTDate) {
        this.dataSentToCTDate = dataSentToCTDate;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getFirstDt() {
        return firstDt;
    }

    public void setFirstDt(String firstDt) {
        this.firstDt = firstDt;
    }

    public String getHealthList() {
        return healthList;
    }

    public void setHealthList(String healthList) {
        this.healthList = healthList;
    }

    public String getSkillList() {
        return skillList;
    }

    public void setSkillList(String skillList) {
        this.skillList = skillList;
    }

    public String getThisPersonStatus() {
        return thisPersonStatus;
    }

    public void setThisPersonStatus(String thisPersonStatus) {
        this.thisPersonStatus = thisPersonStatus;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthCountry() {
        return birthCountry;
    }

    public void setBirthCountry(String birthCountry) {
        this.birthCountry = birthCountry;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }


}
