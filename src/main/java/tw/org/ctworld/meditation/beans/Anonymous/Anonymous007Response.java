package tw.org.ctworld.meditation.beans.Anonymous;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Anonymous007Response extends BaseResponse {

    private List<Anonymous007Object> items;

    public Anonymous007Response(int errCode, String errMsg, List<Anonymous007Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Anonymous007Object> getItems() {
        return items;
    }

    public void setItems(List<Anonymous007Object> items) {
        this.items = items;
    }
}
