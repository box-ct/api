package tw.org.ctworld.meditation.beans.DataSearch;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;

public class DataSearchEventEnrollObject {

    private String memberId;
    private int classPeriodNum;
    private String className, gender;
    private int classGroupId;
    private String aliasName, ctDharmaName, eventId, eventName, idType1, idType2;

    public DataSearchEventEnrollObject(String memberId, int classPeriodNum, String className, String gender,
                                       int classGroupId, String aliasName, String ctDharmaName,
                                       String eventId, String eventName, String idType1,
                                       String idType2) {
        this.memberId = memberId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.gender = gender;
        this.classGroupId = classGroupId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.eventId = eventId;
        this.eventName = eventName;
        this.idType1 = idType1;
        this.idType2 = idType2;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(int classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className == null ? "" : className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(int classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName == null ? "" : ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getIdType1() {
        return idType1 == null ? "" : idType1;
    }

    public void setIdType1(String idType1) {
        this.idType1 = idType1;
    }

    public String getIdType2() {
        return idType2 == null ? "" : idType2;
    }

    public void setIdType2(String idType2) {
        this.idType2 = idType2;
    }
}
