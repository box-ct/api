package tw.org.ctworld.meditation.beans.Member;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class Members003Response extends BaseResponse {

    private String classEnrollFormId;

    public Members003Response(int errCode, String errMsg, String classEnrollFormId) {
        super(errCode, errMsg);
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }
}
