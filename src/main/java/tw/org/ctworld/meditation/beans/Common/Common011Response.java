package tw.org.ctworld.meditation.beans.Common;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Common011Response extends BaseResponse {

    private List<Common011Object> items;

    public Common011Response(int errCode, String errMsg, List<Common011Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Common011Object> getItems() {
        return items;
    }

    public void setItems(List<Common011Object> items) {
        this.items = items;
    }
}
