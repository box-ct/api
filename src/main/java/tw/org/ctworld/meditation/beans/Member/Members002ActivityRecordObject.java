package tw.org.ctworld.meditation.beans.Member;

public class Members002ActivityRecordObject {

    private String enrollUserId, enrollUserName, ctDharmaName;
    private int classPeriodNum;
    private String className;
    private int classGroupId;
    private String eventName, arriveCtDate, leaveCtDate, idType1, idType2;

    public Members002ActivityRecordObject(String enrollUserId, String enrollUserName, String ctDharmaName,
                                          int classPeriodNum, String className, int classGroupId, String eventName,
                                          String arriveCtDate, String leaveCtDate, String idType1, String idType2) {
        this.enrollUserId = enrollUserId;
        this.enrollUserName = enrollUserName;
        this.ctDharmaName = ctDharmaName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.eventName = eventName;
        this.arriveCtDate = arriveCtDate;
        this.leaveCtDate = leaveCtDate;
        this.idType1 = idType1;
        this.idType2 = idType2;
    }

    public String getEnrollUserId() {
        return enrollUserId;
    }

    public void setEnrollUserId(String enrollUserId) {
        this.enrollUserId = enrollUserId;
    }

    public String getEnrollUserName() {
        return enrollUserName;
    }

    public void setEnrollUserName(String enrollUserName) {
        this.enrollUserName = enrollUserName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public int getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(int classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(int classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getArriveCtDate() {
        return arriveCtDate;
    }

    public void setArriveCtDate(String arriveCtDate) {
        this.arriveCtDate = arriveCtDate;
    }

    public String getLeaveCtDate() {
        return leaveCtDate;
    }

    public void setLeaveCtDate(String leaveCtDate) {
        this.leaveCtDate = leaveCtDate;
    }

    public String getIdType1() {
        return idType1;
    }

    public void setIdType1(String idType1) {
        this.idType1 = idType1;
    }

    public String getIdType2() {
        return idType2;
    }

    public void setIdType2(String idType2) {
        this.idType2 = idType2;
    }
}
