package tw.org.ctworld.meditation.beans.DataSearch;

public class DataSearchClassEnrollForm {
    private String classId;
    private String gender;
    private String classGroupId;
    private String classJobTitleList;

    public DataSearchClassEnrollForm() {
    }

    public DataSearchClassEnrollForm(String classId, String gender, String classGroupId, String classJobTitleList) {
        this.classId = classId;
        this.gender = gender;
        this.classGroupId = classGroupId;
        this.classJobTitleList = classJobTitleList;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }
}
