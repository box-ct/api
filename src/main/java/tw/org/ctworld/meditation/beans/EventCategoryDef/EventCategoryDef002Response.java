package tw.org.ctworld.meditation.beans.EventCategoryDef;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class EventCategoryDef002Response extends BaseResponse {

    private List<EventCategoryDef002Object> items;

    public EventCategoryDef002Response(int errCode, String errMsg, List<EventCategoryDef002Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<EventCategoryDef002Object> getItems() {
        return items;
    }

    public void setItems(List<EventCategoryDef002Object> items) {
        this.items = items;
    }
}
