package tw.org.ctworld.meditation.beans.Sys;

import java.util.List;

public class ReadMessagesRequest {
    private List<String> messageIds;

    public List<String> getMessageIds() {
        return messageIds;
    }

    public void setMessageIds(List<String> messageIds) {
        this.messageIds = messageIds;
    }
}
