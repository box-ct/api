package tw.org.ctworld.meditation.beans.MemberInfos;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SearchMemberInfo {
    private String memberId;
    private String photo;
    private String gender;
    private String aliasName;
    private String ctDharmaName;
    private int age;
    private String homePhoneNum1;
    private String mobileNum1;
    private String dsaJobNameList;
    private boolean droppedClass;
    private List<SearchMemberInfoEnrollForm> currentEnrollForms;
    private String unitId;
    private String unitName;
    private String birthDate;
    private String updateDtTm;

    public SearchMemberInfo(String memberId, String gender, String aliasName, String ctDharmaName, Integer age,
                            String homePhoneNum1, String mobileNum1, String dsaJobNameList, String unitId,
                            String unitName, Date birthDate, Date updateDtTm) {
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.age = age == null ? 0 : age;
        this.homePhoneNum1 = homePhoneNum1;
        this.mobileNum1 = mobileNum1;
        this.dsaJobNameList = dsaJobNameList;
        this.unitId = unitId;
        this.unitName = unitName;
        this.birthDate = birthDate == null ? "" : new SimpleDateFormat("yyyy-MM-dd").format(birthDate);
        this.updateDtTm = updateDtTm == null ? "" : new SimpleDateFormat("yyyy-MM-dd").format(updateDtTm);
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public boolean getDroppedClass() {
        return droppedClass;
    }

    public void setDroppedClass(boolean droppedClass) {
        this.droppedClass = droppedClass;
    }

    public List<SearchMemberInfoEnrollForm> getCurrentEnrollForms() {
        return currentEnrollForms;
    }

    public void setCurrentEnrollForms(List<SearchMemberInfoEnrollForm> currentEnrollForms) {
        this.currentEnrollForms = currentEnrollForms;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(String updateDtTm) {
        this.updateDtTm = updateDtTm;
    }
}
