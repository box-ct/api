package tw.org.ctworld.meditation.beans.MemberInfos;

import java.util.List;

public class MemberIds {
    private List<String> memberIds;

    public List<String> getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(List<String> memberIds) {
        this.memberIds = memberIds;
    }
}
