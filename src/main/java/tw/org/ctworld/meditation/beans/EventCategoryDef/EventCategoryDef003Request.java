package tw.org.ctworld.meditation.beans.EventCategoryDef;

public class EventCategoryDef003Request {

    private String eventCategoryId, eventCategoryName;
    private Boolean isCenterAvailable;

    public String getEventCategoryId() {
        return eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public Boolean getIsCenterAvailable() {
        return isCenterAvailable;
    }

    public void setIsCenterAvailable(Boolean centerAvailable) {
        isCenterAvailable = centerAvailable;
    }
}
