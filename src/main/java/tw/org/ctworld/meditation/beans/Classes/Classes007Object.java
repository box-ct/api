package tw.org.ctworld.meditation.beans.Classes;

import java.util.List;

public class Classes007Object {

    private String
            userId,
            classEnrollFormId,
            memberId,
            gender,
            aliasName,
            ctDharmaName,
            classGroupId,
            memberGroupNum,
            classJobTitleList,
            classJobContentList,
            managedClassList,
            mobileNum1;

    private boolean
            isLeadGroup,
            isExecuteClassAttendRecord,
            isExecuteClassGraduateStats,
            isExecuteClassUpGrade,
            isSeeAllClassRecord;

    private Integer age;

    public Classes007Object(String classEnrollFormId,
                            String memberId,
                            String gender,
                            String aliasName,
                            String ctDharmaName,
                            String classGroupId,
                            String memberGroupNum,
                            String classJobTitleList,
                            String classJobContentList,
                            Boolean isLeadGroup,
                            Integer age,
                            String mobileNum1) {
        this.classEnrollFormId = classEnrollFormId;
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classJobTitleList = classJobTitleList;
        this.classJobContentList = classJobContentList;
        this.isLeadGroup = isLeadGroup != null ? isLeadGroup : false;
        this.isExecuteClassAttendRecord = false;
        this.isExecuteClassGraduateStats = false;
        this.isExecuteClassUpGrade = false;
        this.isSeeAllClassRecord = false;
        this.age = age != null ? age : 0;
        this.mobileNum1 = mobileNum1;
    }

//    public Classes007Object(String userId,
//                            String classEnrollFormId,
//                            String memberId,
//                            String gender,
//                            String aliasName,
//                            String ctDharmaName,
//                            String classGroupId,
//                            String memberGroupNum,
//                            String classJobTitleList,
//                            String classJobContentList,
//                            Boolean isLeadGroup,
//                            String managedClassList) {
//        this.userId = userId;
//        this.classEnrollFormId = classEnrollFormId;
//        this.memberId = memberId;
//        this.gender = gender;
//        this.aliasName = aliasName;
//        this.ctDharmaName = ctDharmaName;
//        this.classGroupId = classGroupId;
//        this.memberGroupNum = memberGroupNum;
//        this.classJobTitleList = classJobTitleList;
//        this.classJobContentList = classJobContentList;
//        this.isLeadGroup = isLeadGroup != null ? isLeadGroup : false;
//        this.managedClassList = managedClassList;
//
//    }

    public Classes007Object(String memberId, Long id, String managedClassList) {
        this.memberId = memberId;
        this.userId = String.valueOf(id);
        this.managedClassList = managedClassList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }
    
    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName == null ? "" : ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public String getClassJobContentList() {
        return classJobContentList;
    }

    public void setClassJobContentList(String classJobContentList) {
        this.classJobContentList = classJobContentList;
    }

    public boolean getIsLeadGroup() {
        return isLeadGroup;
    }

    public void setIsLeadGroup(boolean leadGroup) {
        isLeadGroup = leadGroup;
    }

    public boolean getIsExecuteClassAttendRecord() {
        return isExecuteClassAttendRecord;
    }

    public void setIsExecuteClassAttendRecord(boolean executeClassAttendRecord) {
        isExecuteClassAttendRecord = executeClassAttendRecord;
    }

    public boolean getIsExecuteClassGraduateStats() {
        return isExecuteClassGraduateStats;
    }

    public void setIsExecuteClassGraduateStats(boolean executeClassGraduateStats) {
        isExecuteClassGraduateStats = executeClassGraduateStats;
    }

    public boolean getIsExecuteClassUpGrade() {
        return isExecuteClassUpGrade;
    }

    public void setIsExecuteClassUpGrade(boolean executeClassUpGrade) {
        isExecuteClassUpGrade = executeClassUpGrade;
    }

    public boolean getIsSeeAllClassRecord() {
        return isSeeAllClassRecord;
    }

    public void setIsSeeAllClassRecord(boolean seeAllClassRecord) {
        isSeeAllClassRecord = seeAllClassRecord;
    }

    public String getManagedClassList() {
        return managedClassList;
    }

    public void setManagedClassList(String managedClassList) {
        this.managedClassList = managedClassList;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
