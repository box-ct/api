package tw.org.ctworld.meditation.beans.CtEventMaintain;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;
import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2TimeString;

public class CtEventMaintain011Object {

    private String pgmUniqueId;
    private Boolean isDisabledByOCA, isRequired;
    private Integer pgmUiOrderNum;
    private String pgmDate, pgmStartTime, pgmEndTime;
    private Integer pgmCtOrderNum;
    private String pgmName, pgmNote, pgmDesc, goTransDate, goTransType, returnTransDate, returnTransType, pgmCategoryId,
            pgmCategoryName, pgmId;
    private Boolean isGenPgm;
    private String genPgmUiType, genPgmInputOption;
    private Boolean isCenterAvailable;

    public CtEventMaintain011Object(String pgmUniqueId, Boolean isDisabledByOCA, Boolean isRequired,
                                    Integer pgmUiOrderNum, Date pgmDate, Date pgmStartTime,
                                    Date pgmEndTime, Integer pgmCtOrderNum, String pgmName, String pgmNote,
                                    String pgmDesc, Date goTransDate, String goTransType, Date returnTransDate,
                                    String returnTransType, String pgmCategoryId, String pgmCategoryName,
                                    String pgmId, Boolean isGenPgm, String genPgmUiType, String genPgmInputOption,
                                    Boolean isCenterAvailable) {
        this.pgmUniqueId = pgmUniqueId;
        this.isDisabledByOCA = isDisabledByOCA;
        this.isRequired = isRequired;
        this.pgmUiOrderNum = pgmUiOrderNum;
        this.pgmDate = DateTime2DateString(pgmDate);
        this.pgmStartTime = DateTime2TimeString(pgmStartTime);
        this.pgmEndTime = DateTime2TimeString(pgmEndTime);
        this.pgmCtOrderNum = pgmCtOrderNum;
        this.pgmName = pgmName;
        this.pgmNote = pgmNote;
        this.pgmDesc = pgmDesc;
        this.goTransDate = DateTime2DateString(goTransDate);
        this.goTransType = goTransType;
        this.returnTransDate = DateTime2DateString(returnTransDate);
        this.returnTransType = returnTransType;
        this.pgmCategoryId = pgmCategoryId;
        this.pgmCategoryName = pgmCategoryName;
        this.pgmId = pgmId;
        this.isGenPgm = isGenPgm;
        this.genPgmUiType = genPgmUiType;
        this.genPgmInputOption = genPgmInputOption;
        this.isCenterAvailable = isCenterAvailable;
    }

    public String getPgmUniqueId() {
        return pgmUniqueId;
    }

    public void setPgmUniqueId(String pgmUniqueId) {
        this.pgmUniqueId = pgmUniqueId;
    }

    public Boolean getIsDisabledByOCA() {
        return isDisabledByOCA;
    }

    public void setIsDisabledByOCA(Boolean disabledByOCA) {
        isDisabledByOCA = disabledByOCA;
    }

    public Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean required) {
        isRequired = required;
    }

    public Integer getPgmUiOrderNum() {
        return pgmUiOrderNum;
    }

    public void setPgmUiOrderNum(Integer pgmUiOrderNum) {
        this.pgmUiOrderNum = pgmUiOrderNum;
    }

    public String getPgmDate() {
        return pgmDate;
    }

    public void setPgmDate(String pgmDate) {
        this.pgmDate = pgmDate;
    }

    public String getPgmStartTime() {
        return pgmStartTime;
    }

    public void setPgmStartTime(String pgmStartTime) {
        this.pgmStartTime = pgmStartTime;
    }

    public String getPgmEndTime() {
        return pgmEndTime;
    }

    public void setPgmEndTime(String pgmEndTime) {
        this.pgmEndTime = pgmEndTime;
    }

    public Integer getPgmCtOrderNum() {
        return pgmCtOrderNum;
    }

    public void setPgmCtOrderNum(Integer pgmCtOrderNum) {
        this.pgmCtOrderNum = pgmCtOrderNum;
    }

    public String getPgmName() {
        return pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

    public String getPgmNote() {
        return pgmNote;
    }

    public void setPgmNote(String pgmNote) {
        this.pgmNote = pgmNote;
    }

    public String getPgmDesc() {
        return pgmDesc;
    }

    public void setPgmDesc(String pgmDesc) {
        this.pgmDesc = pgmDesc;
    }

    public String getGoTransDate() {
        return goTransDate;
    }

    public void setGoTransDate(String goTransDate) {
        this.goTransDate = goTransDate;
    }

    public String getGoTransType() {
        return goTransType;
    }

    public void setGoTransType(String goTransType) {
        this.goTransType = goTransType;
    }

    public String getReturnTransDate() {
        return returnTransDate;
    }

    public void setReturnTransDate(String returnTransDate) {
        this.returnTransDate = returnTransDate;
    }

    public String getReturnTransType() {
        return returnTransType;
    }

    public void setReturnTransType(String returnTransType) {
        this.returnTransType = returnTransType;
    }

    public String getPgmCategoryId() {
        return pgmCategoryId;
    }

    public void setPgmCategoryId(String pgmCategoryId) {
        this.pgmCategoryId = pgmCategoryId;
    }

    public String getPgmCategoryName() {
        return pgmCategoryName;
    }

    public void setPgmCategoryName(String pgmCategoryName) {
        this.pgmCategoryName = pgmCategoryName;
    }

    public String getPgmId() {
        return pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }

    public Boolean getIsGenPgm() {
        return isGenPgm;
    }

    public void setIsGenPgm(Boolean genPgm) {
        isGenPgm = genPgm;
    }

    public String getGenPgmUiType() {
        return genPgmUiType;
    }

    public void setGenPgmUiType(String genPgmUiType) {
        this.genPgmUiType = genPgmUiType;
    }

    public String getGenPgmInputOption() {
        return genPgmInputOption;
    }

    public void setGenPgmInputOption(String genPgmInputOption) {
        this.genPgmInputOption = genPgmInputOption;
    }

    public Boolean getIsCenterAvailable() {
        return isCenterAvailable;
    }

    public void setIsCenterAvailable(Boolean centerAvailable) {
        isCenterAvailable = centerAvailable;
    }
}
