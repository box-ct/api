package tw.org.ctworld.meditation.beans.DataSearch;

public class DataSearchClassInfo {
    private String classId;
    private String className;
    private String classPeriodNum;
    private int classTypeNum;

    public DataSearchClassInfo(String classId, String className, Integer classTypeNum) {
        this.classId = classId;
        this.className = className;
        this.classTypeNum = classTypeNum != null ? classTypeNum : 0;
    }

    public DataSearchClassInfo(String classId, String className, String classPeriodNum, int classTypeNum) {
        this.classId = classId;
        this.className = className;
        this.classPeriodNum = classPeriodNum;
        this.classTypeNum = classTypeNum;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public int getClassTypeNum() {
        return classTypeNum;
    }

    public void setClassTypeNum(int classTypeNum) {
        this.classTypeNum = classTypeNum;
    }
}
