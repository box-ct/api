package tw.org.ctworld.meditation.beans.Auth;

public class Auth004Request {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
