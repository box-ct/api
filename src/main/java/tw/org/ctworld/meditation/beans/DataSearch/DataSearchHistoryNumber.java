package tw.org.ctworld.meditation.beans.DataSearch;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class DataSearchHistoryNumber extends BaseResponse {
    private List<HistoryPeriod> items;

    public DataSearchHistoryNumber(int errCode, String errMsg, List<HistoryPeriod> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<HistoryPeriod> getItems() {
        return items;
    }

    public void setItems(List<HistoryPeriod> items) {
        this.items = items;
    }

    public static class HistoryPeriod {
        private int classPeriodNum;
        private List<DataSearchHistoryClass> classes;

        public HistoryPeriod() {
        }

        public HistoryPeriod(int classPeriodNum, List<DataSearchHistoryClass> classes) {
            this.classPeriodNum = classPeriodNum;
            this.classes = classes;
        }

        public int getClassPeriodNum() {
            return classPeriodNum;
        }

        public void setClassPeriodNum(int classPeriodNum) {
            this.classPeriodNum = classPeriodNum;
        }

        public List<DataSearchHistoryClass> getClasses() {
            return classes;
        }

        public void setClasses(List<DataSearchHistoryClass> classes) {
            this.classes = classes;
        }
    }
}
