package tw.org.ctworld.meditation.beans.ClassGroup;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.ClassInfo;

import java.util.List;

public class ClassGroup001Response extends BaseResponse {

    private List<ClassGroup001Object> items;

    public ClassGroup001Response(int errCode, String errMsg, List<ClassGroup001Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassGroup001Object> getItems() {
        return items;
    }

    public void setItems(List<ClassGroup001Object> items) {
        this.items = items;
    }
}
