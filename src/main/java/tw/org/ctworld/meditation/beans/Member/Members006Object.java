package tw.org.ctworld.meditation.beans.Member;

public class Members006Object {

    private String classId, className, classGroupId;

    public Members006Object(String classId, String className, String classGroupId) {
        this.classId = classId;
        this.className = className;
        this.classGroupId = classGroupId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }
}
