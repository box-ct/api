package tw.org.ctworld.meditation.beans.ClassStats;

public class ClassStats005Object {

    private String unitId, unitName, gender, aliasName,ctDharmaName;
    private int age;
    private String classDayOrNight, classTypeName, classPeriodNum, className, classGroupId, memberGroupNum, dayOfWeek,
            classDesc, classFullName, certificateName;
    private boolean isFullAttended, isGraduated;

    public ClassStats005Object(String unitId, String unitName, String gender, String aliasName, String ctDharmaName,
                               int age, String classDayOrNight, String classTypeName, String classPeriodNum,
                               String className, String classGroupId, String memberGroupNum, String dayOfWeek,
                               String classDesc, String classFullName, String certificateName,
                               boolean isFullAttended, boolean isGraduated) {
        this.unitId = unitId;
        this.unitName = unitName;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.age = age;
        this.classDayOrNight = classDayOrNight;
        this.classTypeName = classTypeName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.dayOfWeek = dayOfWeek;
        this.classDesc = classDesc;
        this.classFullName = classFullName;
        this.certificateName = certificateName;
        this.isFullAttended = isFullAttended;
        this.isGraduated = isGraduated;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getClassDayOrNight() {
        return classDayOrNight;
    }

    public void setClassDayOrNight(String classDayOrNight) {
        this.classDayOrNight = classDayOrNight;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getClassDesc() {
        return classDesc;
    }

    public void setClassDesc(String classDesc) {
        this.classDesc = classDesc;
    }

    public String getClassFullName() {
        return classFullName;
    }

    public void setClassFullName(String classFullName) {
        this.classFullName = classFullName;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public boolean getIsFullAttended() {
        return isFullAttended;
    }

    public void setIsFullAttended(boolean fullAttended) {
        isFullAttended = fullAttended;
    }

    public boolean getIsGraduated() {
        return isGraduated;
    }

    public void setIsGraduated(boolean graduated) {
        isGraduated = graduated;
    }

}
