package tw.org.ctworld.meditation.beans.ClassUpgrade;

import java.util.List;

public class ClassUpgrade006Request {

    private List<String> memberIdList;
    private boolean isNotComeBack;

    public List<String> getMemberIdList() {
        return memberIdList;
    }

    public void setMemberIdList(List<String> memberIdList) {
        this.memberIdList = memberIdList;
    }

    public boolean getIsNotComeBack() {
        return isNotComeBack;
    }

    public void setIsNotComeBack(boolean notComeBack) {
        isNotComeBack = notComeBack;
    }
}
