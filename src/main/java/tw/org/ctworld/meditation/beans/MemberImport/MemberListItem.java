package tw.org.ctworld.meditation.beans.MemberImport;

import java.util.Date;

public class MemberListItem {
    private String photo;
    private String memberId;
    private String gender;
    private String aliasName;
    private String ctDharmaName;
    private String dsaJobNameList;
    private String classList;
    private String donationNameList;
    private String healthList;
    private Date birthDate;
    private String twIdNum;
    private String passportNum;
    private String urgentContactPersonName1;
    private String urgentContactPersonRelationship1;
    private String urgentContactPersonPhoneNum1;
    private String currentClassJobTitle;//本期禪修班別(職事)
    private String currentHomeClassNote;//歸屬班別
    private Integer age;
    public MemberListItem(){}
    public MemberListItem( String memberId, String gender, String aliasName, String ctDharmaName, String dsaJobNameList, String classList, String donationNameList, String healthList, Date birthDate, String twIdNum, String passportNum, String urgentContactPersonName1, String urgentContactPersonRelationship1, String urgentContactPersonPhoneNum1, Integer age) {
        this.photo=memberId+"_Photo.jpg";
        this.memberId = memberId;
        this.gender = (gender.equals("M")||gender.equals("男"))?"男":(gender.equals("F")||gender.equals("女"))?"女":"";
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.dsaJobNameList = dsaJobNameList;
        this.classList = classList;
        this.donationNameList = donationNameList;
        this.healthList = healthList;
        this.birthDate = birthDate;
        this.twIdNum = twIdNum;
        this.passportNum = passportNum;
        this.urgentContactPersonName1 = urgentContactPersonName1;
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
        this.age = age;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getClassList() {
        return classList;
    }

    public void setClassList(String classList) {
        this.classList = classList;
    }

    public String getDonationNameList() {
        return donationNameList;
    }

    public void setDonationNameList(String donationNameList) {
        this.donationNameList = donationNameList;
    }

    public String getHealthList() {
        return healthList;
    }

    public void setHealthList(String healthList) {
        this.healthList = healthList;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public String getUrgentContactPersonName1() {
        return urgentContactPersonName1;
    }

    public void setUrgentContactPersonName1(String urgentContactPersonName1) {
        this.urgentContactPersonName1 = urgentContactPersonName1;
    }

    public String getUrgentContactPersonRelationship1() {
        return urgentContactPersonRelationship1;
    }

    public void setUrgentContactPersonRelationship1(String urgentContactPersonRelationship1) {
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
    }

    public String getUrgentContactPersonPhoneNum1() {
        return urgentContactPersonPhoneNum1;
    }

    public void setUrgentContactPersonPhoneNum1(String urgentContactPersonPhoneNum1) {
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
    }

    public String getCurrentClassJobTitle() {
        return currentClassJobTitle;
    }

    public void setCurrentClassJobTitle(String currentClassJobTitle) {
        this.currentClassJobTitle = currentClassJobTitle;
    }

    public String getCurrentHomeClassNote() {
        return currentHomeClassNote;
    }

    public void setCurrentHomeClassNote(String currentHomeClassNote) {
        this.currentHomeClassNote = currentHomeClassNote;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }



}
