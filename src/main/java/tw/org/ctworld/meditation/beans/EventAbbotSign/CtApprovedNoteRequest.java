package tw.org.ctworld.meditation.beans.EventAbbotSign;

public class CtApprovedNoteRequest {
    private String formAutoAddNum;
    private String ctApprovedNote;
    private String changeEndDate;

    public String getFormAutoAddNum() {
        return formAutoAddNum;
    }

    public void setFormAutoAddNum(String formAutoAddNum) {
        this.formAutoAddNum = formAutoAddNum;
    }

    public String getCtApprovedNote() {
        return ctApprovedNote;
    }

    public void setCtApprovedNote(String ctApprovedNote) {
        this.ctApprovedNote = ctApprovedNote;
    }

    public String getChangeEndDate() {
        return changeEndDate;
    }

    public void setChangeEndDate(String changeEndDate) {
        this.changeEndDate = changeEndDate;
    }
}
