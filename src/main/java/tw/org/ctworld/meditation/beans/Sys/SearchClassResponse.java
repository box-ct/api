package tw.org.ctworld.meditation.beans.Sys;

import com.fasterxml.jackson.databind.ser.Serializers;
import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class SearchClassResponse extends BaseResponse {
    private List<String> items;

    public SearchClassResponse() {
    }

    public SearchClassResponse(int errCode, String errMsg, List<String> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
