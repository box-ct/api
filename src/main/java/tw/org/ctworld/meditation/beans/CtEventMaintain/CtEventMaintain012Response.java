package tw.org.ctworld.meditation.beans.CtEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class CtEventMaintain012Response extends BaseResponse {

    private List<CtEventMaintain012_2Object> importedItems;

    public CtEventMaintain012Response(int errCode, String errMsg, List<CtEventMaintain012_2Object> importedItems) {
        super(errCode, errMsg);
        this.importedItems = importedItems;
    }

    public List<CtEventMaintain012_2Object> getImportedItems() {
        return importedItems;
    }

    public void setImportedItems(List<CtEventMaintain012_2Object> importedItems) {
        this.importedItems = importedItems;
    }
}
