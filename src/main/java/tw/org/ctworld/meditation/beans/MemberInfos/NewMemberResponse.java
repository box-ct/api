package tw.org.ctworld.meditation.beans.MemberInfos;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.CtMemberInfo;

public class NewMemberResponse extends BaseResponse {
    private CtMemberInfo memberInfo;

    public NewMemberResponse(int errCode, String errMsg, CtMemberInfo memberInfo) {
        super(errCode, errMsg);
        this.memberInfo = memberInfo;
    }

    public CtMemberInfo getMemberInfo() {
        return memberInfo;
    }

    public void setMemberInfo(CtMemberInfo memberInfo) {
        this.memberInfo = memberInfo;
    }
}
