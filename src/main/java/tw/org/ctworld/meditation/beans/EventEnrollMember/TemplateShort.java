package tw.org.ctworld.meditation.beans.EventEnrollMember;

public class TemplateShort {
    private String templateName;
    private String templateUniqueId;

    public TemplateShort(String templateName, String templateUniqueId) {
        this.templateName = templateName;
        this.templateUniqueId = templateUniqueId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateUniqueId() {
        return templateUniqueId;
    }

    public void setTemplateUniqueId(String templateUniqueId) {
        this.templateUniqueId = templateUniqueId;
    }
}
