package tw.org.ctworld.meditation.beans.Common;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.UserInfo;

public class Common001Response extends BaseResponse {

    private UserInfo userInfo;

    public Common001Response(int errCode, String errMsg, UserInfo userInfo) {
        super(errCode, errMsg);
        this.userInfo = userInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
