package tw.org.ctworld.meditation.beans.Common;

public class Common011Object {

    private String year, classStatus, classPeriodNum, classId, className;

    public Common011Object(String classPeriodNum, String className, String classId) {
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classId = classId;
    }

    public Common011Object(String year, String classStatus, String classPeriodNum, String classId, String className) {
        this.year = year;
        this.classStatus = classStatus;
        this.classPeriodNum = classPeriodNum;
        this.classId = classId;
        this.className = className;
    }

    public String getYear() {
        return year != null ? year : "";
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getClassStatus() {
        return classStatus != null ? classStatus : "";
    }

    public void setClassStatus(String classStatus) {
        this.classStatus = classStatus;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
