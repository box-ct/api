package tw.org.ctworld.meditation.beans.ClassStats;

public class ClassStats006Object {

    private String memberId, classJobTitleList, gender, aliasName, ctDharmaName;
    private int age;
    private String classPeriodNum, className, classGroupId;
    private int weekTotal, week;
    private boolean isFullAttended, isGraduated, isMakeupToFullAttended;
    private String classAttendedResult, note;

    public ClassStats006Object(String memberId, String classJobTitleList, String gender, String aliasName,
                               String ctDharmaName, int age, String classPeriodNum, String className,
                               String classGroupId, int weekTotal, int week, boolean isFullAttended,
                               boolean isGraduated, boolean isMakeupToFullAttended, String classAttendedResult,
                               String note) {
        this.memberId = memberId;
        this.classJobTitleList = classJobTitleList;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.age = age;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.weekTotal = weekTotal;
        this.week = week;
        this.isFullAttended = isFullAttended;
        this.isGraduated = isGraduated;
        this.isMakeupToFullAttended = isMakeupToFullAttended;
        this.classAttendedResult = classAttendedResult;
        this.note = note;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public int getWeekTotal() {
        return weekTotal;
    }

    public void setWeekTotal(int weekTotal) {
        this.weekTotal = weekTotal;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public boolean getIsFullAttended() {
        return isFullAttended;
    }

    public void setIsFullAttended(boolean fullAttended) {
        isFullAttended = fullAttended;
    }

    public boolean getIsGraduated() {
        return isGraduated;
    }

    public void setIsGraduated(boolean graduated) {
        isGraduated = graduated;
    }

    public boolean getIsMakeupToFullAttended() {
        return isMakeupToFullAttended;
    }

    public void setIsMakeupToFullAttended(boolean makeupToFullAttended) {
        isMakeupToFullAttended = makeupToFullAttended;
    }

    public String getClassAttendedResult() {
        return classAttendedResult;
    }

    public void setClassAttendedResult(String classAttendedResult) {
        this.classAttendedResult = classAttendedResult;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
