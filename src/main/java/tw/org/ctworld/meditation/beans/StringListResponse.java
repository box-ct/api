package tw.org.ctworld.meditation.beans;

import java.util.List;

public class StringListResponse extends BaseResponse {
    private List<String> items;

    public StringListResponse(int errCode, String errMsg, List<String> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
