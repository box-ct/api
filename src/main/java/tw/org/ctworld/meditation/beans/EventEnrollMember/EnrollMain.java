package tw.org.ctworld.meditation.beans.EventEnrollMember;

import com.fasterxml.jackson.annotation.JsonFormat;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.EventEnrollMain;

import java.util.Date;

public class EnrollMain {
    private String
            arriveCtDate,
            arriveCtTime,
            changeEndDate,
            changeNote,
            classId,
            className,
            classNameAndJobList,
            ctApprovedNote,
            ctDormEndDate,
            ctDormStartDate,
            donationNameList,
            dormNote,
            dsaJobNameList,
            enrollGroupName,
            enrollLatestChange,
            enrollNote,
            enrollUnitName,
            enrollUserCtDharmaName,
            enrollUserId,
            enrollUserName,
            familyLeaderName,
            formAutoAddNum,
            gender,
            homeClassNote,
            idType1,
            idType2,
            isAbbotSignedByUserName,
            isAbbotSignedDt,
            isCtApprovedByUserName,
            isCtApprovedDt,
            isCtAprovedByUserName,
            jobOrder,
            jobTitle,
            leaveCtDate,
            leaveCtTime,
            leaveUnitDate,
            leaveUnitTime,
            masterId,
            masterName,
            masterPreceptTypeName,
            mobileNum1,
            photo,
            preceptOrder,
            relativeMeritList,
            summerDonation1,
            templateName,
            templateUniqueId,
            unitId,
            unitName,
            updateDtTm,
            updatorName,
            vlntrGroupNameIdList;

    private Boolean
            isAbbot,
            isAbbotSigned,
            isCanceled,
            isCtApproved,
            isCtDorm,
            isTreasurer;

    private Integer
            age,
            enrollOrderNum,
            summerDonation1Count;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date birthDate;


    public EnrollMain(EventEnrollMain eventEnrollMain) {
        this.age = eventEnrollMain.getAge();
        this.arriveCtDate = CommonUtils.DateTime2DateString(eventEnrollMain.getArriveCtDate());
        this.arriveCtTime = CommonUtils.DateTime2TimeString(eventEnrollMain.getArriveCtTime());
        this.changeEndDate = CommonUtils.DateTime2DateString(eventEnrollMain.getChangeEndDate());
        this.changeNote = eventEnrollMain.getChangeNote();
        this.classId = eventEnrollMain.getClassId();
        this.className = eventEnrollMain.getClassName();
        this.classNameAndJobList = eventEnrollMain.getClassNameAndJobList();
        this.ctApprovedNote = eventEnrollMain.getCtApprovedNote();
        this.ctDormEndDate = CommonUtils.DateTime2DateString(eventEnrollMain.getCtDormEndDate());
        this.ctDormStartDate = CommonUtils.DateTime2DateString(eventEnrollMain.getCtDormStartDate());
        this.donationNameList = eventEnrollMain.getDonationNameList();
        this.dormNote = eventEnrollMain.getDormNote();
        this.dsaJobNameList = eventEnrollMain.getDsaJobNameList();
        this.enrollGroupName = eventEnrollMain.getEnrollGroupName();
        this.enrollLatestChange = eventEnrollMain.getEnrollLatestChange();
        this.enrollNote = eventEnrollMain.getEnrollNote();
        this.enrollOrderNum = eventEnrollMain.getEnrollOrderNum();
        this.enrollUnitName = eventEnrollMain.getEnrollUnitName();
        this.enrollUserCtDharmaName = eventEnrollMain.getEnrollUserCtDharmaName();
        this.enrollUserId = eventEnrollMain.getEnrollUserId();
        this.enrollUserName = eventEnrollMain.getEnrollUserName();
        this.familyLeaderName = eventEnrollMain.getFamilyLeaderName();
        this.formAutoAddNum = eventEnrollMain.getFormAutoAddNum();
        this.gender = eventEnrollMain.getGender();
        this.homeClassNote = eventEnrollMain.getHomeClassNote();
        this.idType1 = eventEnrollMain.getIdType1();
        this.idType2 = eventEnrollMain.getIdType2();
        this.isAbbot = eventEnrollMain.getIsAbbot() != null ? eventEnrollMain.getIsAbbot() : false;
        this.isAbbotSigned = eventEnrollMain.getIsAbbotSigned();
        this.isAbbotSignedByUserName = eventEnrollMain.getIsAbbotSignedByUserName();
        this.isAbbotSignedByUserName = eventEnrollMain.getIsAbbotSignedByUserName();
        this.isAbbotSignedDt = CommonUtils.DateTime2DateString(eventEnrollMain.getIsAbbotSignedDt());
        this.isCanceled = eventEnrollMain.getIsCanceled();
        this.isCtApproved = eventEnrollMain.getIsCtApproved() != null ? eventEnrollMain.getIsCtApproved() : false;
        this.isCtApprovedByUserName = eventEnrollMain.getIsCtApprovedByUserName();
        this.isCtApprovedDt = CommonUtils.DateTime2DateString(eventEnrollMain.getIsCtApprovedDt());
        this.isCtDorm = eventEnrollMain.getIsCtDorm();
        this.isTreasurer = eventEnrollMain.getIsTreasurer() != null ? eventEnrollMain.getIsTreasurer() : false;
        this.jobOrder = eventEnrollMain.getJobOrder();
        this.jobTitle = eventEnrollMain.getJobTitle();
        this.leaveCtDate = CommonUtils.DateTime2DateString(eventEnrollMain.getLeaveCtDate());
        this.leaveCtTime = CommonUtils.DateTime2TimeString(eventEnrollMain.getLeaveCtTime());
        this.leaveUnitDate = CommonUtils.DateTime2DateString(eventEnrollMain.getLeaveUnitDate());
        this.leaveUnitTime = CommonUtils.DateTime2TimeString(eventEnrollMain.getLeaveUnitTime());
        this.masterId = eventEnrollMain.getEnrollUserId();
        this.masterName = eventEnrollMain.getEnrollUserName();
        this.masterPreceptTypeName = eventEnrollMain.getMasterPreceptTypeName();
        this.mobileNum1 = eventEnrollMain.getMobileNum1();
        this.preceptOrder = eventEnrollMain.getPreceptOrder();
        this.relativeMeritList = eventEnrollMain.getRelativeMeritList();
        this.summerDonation1 = eventEnrollMain.getSummerDonation1();
        this.summerDonation1Count = eventEnrollMain.getSummerDonation1Count();
        this.templateName = eventEnrollMain.getTemplateName();
        this.templateUniqueId = eventEnrollMain.getTemplateUniqueId();
        this.unitId = eventEnrollMain.getUnitId();
        this.unitName = eventEnrollMain.getUnitName();
        this.updateDtTm = CommonUtils.DateTime2DateTimeString(eventEnrollMain.getUpdateDtTm());
        this.updatorName = eventEnrollMain.getUpdatorName();
        this.vlntrGroupNameIdList = eventEnrollMain.getVlntrGroupNameIdList();

        this.birthDate = eventEnrollMain.getBirthDate();
    }

    
    public String getEnrollUnitName() {
        return enrollUnitName;
    }

    public void setEnrollUnitName(String enrollUnitName) {
        this.enrollUnitName = enrollUnitName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEnrollUserId() {
        return enrollUserId;
    }

    public void setEnrollUserId(String enrollUserId) {
        this.enrollUserId = enrollUserId;
    }

    public String getEnrollUserCtDharmaName() {
        return enrollUserCtDharmaName;
    }

    public void setEnrollUserCtDharmaName(String enrollUserCtDharmaName) {
        this.enrollUserCtDharmaName = enrollUserCtDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getEnrollGroupName() {
        return enrollGroupName;
    }

    public void setEnrollGroupName(String enrollGroupName) {
        this.enrollGroupName = enrollGroupName;
    }

    public String getVlntrGroupNameIdList() {
        return vlntrGroupNameIdList;
    }

    public void setVlntrGroupNameIdList(String vlntrGroupNameIdList) {
        this.vlntrGroupNameIdList = vlntrGroupNameIdList;
    }

    public String getSummerDonation1() {
        return summerDonation1;
    }

    public void setSummerDonation1(String summerDonation1) {
        this.summerDonation1 = summerDonation1;
    }

    public String getDonationNameList() {
        return donationNameList;
    }

    public void setDonationNameList(String donationNameList) {
        this.donationNameList = donationNameList;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getClassNameAndJobList() {
        return classNameAndJobList;
    }

    public void setClassNameAndJobList(String classNameAndJobList) {
        this.classNameAndJobList = classNameAndJobList;
    }

    public String getHomeClassNote() {
        return homeClassNote;
    }

    public void setHomeClassNote(String homeClassNote) {
        this.homeClassNote = homeClassNote;
    }

    public String getIdType1() {
        return idType1;
    }

    public void setIdType1(String idType1) {
        this.idType1 = idType1;
    }

    public String getIdType2() {
        return idType2;
    }

    public void setIdType2(String idType2) {
        this.idType2 = idType2;
    }

    public String getLeaveUnitDate() {
        return leaveUnitDate;
    }

    public void setLeaveUnitDate(String leaveUnitDate) {
        this.leaveUnitDate = leaveUnitDate;
    }

    public String getLeaveUnitTime() {
        return leaveUnitTime;
    }

    public void setLeaveUnitTime(String leaveUnitTime) {
        this.leaveUnitTime = leaveUnitTime;
    }

    public String getArriveCtDate() {
        return arriveCtDate;
    }

    public void setArriveCtDate(String arriveCtDate) {
        this.arriveCtDate = arriveCtDate;
    }

    public String getArriveCtTime() {
        return arriveCtTime;
    }

    public void setArriveCtTime(String arriveCtTime) {
        this.arriveCtTime = arriveCtTime;
    }

    public String getLeaveCtDate() {
        return leaveCtDate;
    }

    public void setLeaveCtDate(String leaveCtDate) {
        this.leaveCtDate = leaveCtDate;
    }

    public String getLeaveCtTime() {
        return leaveCtTime;
    }

    public void setLeaveCtTime(String leaveCtTime) {
        this.leaveCtTime = leaveCtTime;
    }

    public String getCtDormStartDate() {
        return ctDormStartDate;
    }

    public void setCtDormStartDate(String ctDormStartDate) {
        this.ctDormStartDate = ctDormStartDate;
    }

    public String getCtDormEndDate() {
        return ctDormEndDate;
    }

    public void setCtDormEndDate(String ctDormEndDate) {
        this.ctDormEndDate = ctDormEndDate;
    }

    public String getDormNote() {
        return dormNote;
    }

    public void setDormNote(String dormNote) {
        this.dormNote = dormNote;
    }

    public String getEnrollNote() {
        return enrollNote;
    }

    public void setEnrollNote(String enrollNote) {
        this.enrollNote = enrollNote;
    }

    public String getFormAutoAddNum() {
        return formAutoAddNum;
    }

    public void setFormAutoAddNum(String formAutoAddNum) {
        this.formAutoAddNum = formAutoAddNum;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

    public String getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(String updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public Boolean getIsCanceled() {
        return isCanceled;
    }

    public void setIsCanceled(Boolean canceled) {
        isCanceled = canceled;
    }

    public Boolean getIsAbbotSigned() {
        return isAbbotSigned;
    }

    public void setISAbbotSigned(Boolean abbotSigned) {
        isAbbotSigned = abbotSigned;
    }

    public Boolean getIsCtDorm() {
        return isCtDorm;
    }

    public void setIsCtDorm(Boolean ctDorm) {
        isCtDorm = ctDorm;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getEnrollOrderNum() {
        return enrollOrderNum;
    }

    public void setEnrollOrderNum(Integer enrollOrderNum) {
        this.enrollOrderNum = enrollOrderNum;
    }

    public Integer getSummerDonation1Count() {
        return summerDonation1Count;
    }

    public void setSummerDonation1Count(Integer summerDonation1Count) {
        this.summerDonation1Count = summerDonation1Count;
    }

    public String getEnrollUserName() {
        return enrollUserName;
    }

    public void setEnrollUserName(String enrollUserName) {
        this.enrollUserName = enrollUserName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getChangeNote() {
        return changeNote;
    }

    public void setChangeNote(String changeNote) {
        this.changeNote = changeNote;
    }

    public String getFamilyLeaderName() {
        return familyLeaderName;
    }

    public void setFamilyLeaderName(String familyLeaderName) {
        this.familyLeaderName = familyLeaderName;
    }

    public String getEnrollLatestChange() {
        return enrollLatestChange;
    }

    public void setEnrollLatestChange(String enrollLatestChange) {
        this.enrollLatestChange = enrollLatestChange;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getChangeEndDate() {
        return changeEndDate;
    }

    public void setChangeEndDate(String changeEndDate) {
        this.changeEndDate = changeEndDate;
    }

    public String getIsAbbotSignedByUserName() {
        return isAbbotSignedByUserName;
    }

    public void setIsAbbotSignedByUserName(String isAbbotSignedByUserName) {
        this.isAbbotSignedByUserName = isAbbotSignedByUserName;
    }

    public String getIsAbbotSignedDt() {
        return isAbbotSignedDt;
    }

    public void setIsAbbotSignedDt(String isAbbotSignedDt) {
        this.isAbbotSignedDt = isAbbotSignedDt;
    }

    public String getIsCtApprovedByUserName() {
        return isCtApprovedByUserName;
    }

    public void setIsCtApprovedByUserName(String isCtApprovedByUserName) {
        this.isCtApprovedByUserName = isCtApprovedByUserName;
    }

    public String getIsCtApprovedDt() {
        return isCtApprovedDt;
    }

    public void setIsCtApprovedDt(String isCtApprovedDt) {
        this.isCtApprovedDt = isCtApprovedDt;
    }

    public Boolean getIsCtApproved() {
        return isCtApproved;
    }

    public void setIsCtApproved(Boolean ctApproved) {
        isCtApproved = ctApproved;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getCtApprovedNote() {
        return ctApprovedNote;
    }

    public void setCtApprovedNote(String ctApprovedNote) {
        this.ctApprovedNote = ctApprovedNote;
    }

    public String getTemplateUniqueId() {
        return templateUniqueId;
    }

    public void setTemplateUniqueId(String templateUniqueId) {
        this.templateUniqueId = templateUniqueId;
    }

    public String getIsCtAprovedByUserName() {
        return isCtAprovedByUserName;
    }

    public void setIsCtAprovedByUserName(String isCtAprovedByUserName) {
        this.isCtAprovedByUserName = isCtAprovedByUserName;
    }

    public String getJobOrder() {
        return jobOrder;
    }

    public void setJobOrder(String jobOrder) {
        this.jobOrder = jobOrder;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getPreceptOrder() {
        return preceptOrder;
    }

    public void setPreceptOrder(String preceptOrder) {
        this.preceptOrder = preceptOrder;
    }

    public String getRelativeMeritList() {
        return relativeMeritList;
    }

    public void setRelativeMeritList(String relativeMeritList) {
        this.relativeMeritList = relativeMeritList;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public Boolean getIsAbbot() {
        return isAbbot;
    }

    public void setIsAbbot(Boolean abbot) {
        isAbbot = abbot;
    }

    public Boolean getIsTreasurer() {
        return isTreasurer;
    }

    public void setIsTreasurer(Boolean treasurer) {
        isTreasurer = treasurer;
    }

    public String getMasterPreceptTypeName() {
        return masterPreceptTypeName;
    }

    public void setMasterPreceptTypeName(String masterPreceptTypeName) {
        this.masterPreceptTypeName = masterPreceptTypeName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}
