package tw.org.ctworld.meditation.beans.ClassReport;

import java.util.List;

public class ClearPrintMarkerRequest {
    private List<String> memberIdList;

    public List<String> getMemberIdList() {
        return memberIdList;
    }

    public void setMemberIdList(List<String> memberIdList) {
        this.memberIdList = memberIdList;
    }
}
