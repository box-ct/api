package tw.org.ctworld.meditation.beans.CtEventMaintain;


public class CtEventMaintain003_2Object {

    private String eventIs;
    private Boolean isMaster, isAbbotSigned;

    public CtEventMaintain003_2Object(String eventIs) {
        this.eventIs = eventIs;
    }

    public CtEventMaintain003_2Object(String eventIs, Boolean isMaster, Boolean isAbbotSigned) {
        this.eventIs = eventIs;
        this.isMaster = isMaster;
        this.isAbbotSigned = isAbbotSigned;
    }

    public String getEventIs() {
        return eventIs;
    }

    public void setEventIs(String eventIs) {
        this.eventIs = eventIs;
    }

    public Boolean getIsMaster() {
        return isMaster;
    }

    public void setIsMaster(Boolean master) {
        isMaster = master;
    }

    public Boolean getIsAbbotSigned() {
        return isAbbotSigned;
    }

    public void setIsAbbotSigned(Boolean abbotSigned) {
        isAbbotSigned = abbotSigned;
    }
}
