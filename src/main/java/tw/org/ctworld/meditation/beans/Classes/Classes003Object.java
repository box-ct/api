package tw.org.ctworld.meditation.beans.Classes;

public class Classes003Object {

    private int id, classWeeksNum;
    private String classDate, classDayOfWeek, classContent, actualClassContent;
    private boolean isCancelled, editable;

    public Classes003Object(int id, int classWeeksNum, String classDate, String classDayOfWeek, String classContent,
                            String actualClassContent, boolean isCancelled, boolean editable) {
        this.id = id;
        this.classWeeksNum = classWeeksNum;
        this.classDate = classDate;
        this.classDayOfWeek = classDayOfWeek;
        this.classContent = classContent;
        this.actualClassContent = actualClassContent;
        this.isCancelled = isCancelled;
        this.editable = editable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public String getClassDate() {
        return classDate;
    }

    public void setClassDate(String classDate) {
        this.classDate = classDate;
    }

    public String getClassDayOfWeek() {
        return classDayOfWeek;
    }

    public void setClassDayOfWeek(String classDayOfWeek) {
        this.classDayOfWeek = classDayOfWeek;
    }

    public String getClassContent() {
        return classContent;
    }

    public void setClassContent(String classContent) {
        this.classContent = classContent;
    }

    public String getActualClassContent() {
        return actualClassContent;
    }

    public void setActualClassContent(String actualClassContent) {
        this.actualClassContent = actualClassContent;
    }

    public boolean getIsCancelled() {
        return isCancelled;
    }

    public void setIsCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}
