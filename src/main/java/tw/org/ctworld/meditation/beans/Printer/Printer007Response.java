package tw.org.ctworld.meditation.beans.Printer;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Printer007Response extends BaseResponse {

    private String printerId, certTypeName, certTemplateName, description;
    private List<Printer007Object> settings;

    public Printer007Response(int errCode, String errMsg, String printerId, String certTypeName,
                              String certTemplateName, String description, List<Printer007Object> settings) {
        super(errCode, errMsg);
        this.printerId = printerId;
        this.certTypeName = certTypeName;
        this.certTemplateName = certTemplateName;
        this.description = description;
        this.settings = settings;
    }

    public String getPrinterId() {
        return printerId;
    }

    public void setPrinterId(String printerId) {
        this.printerId = printerId;
    }

    public String getCertTypeName() {
        return certTypeName;
    }

    public void setCertTypeName(String certTypeName) {
        this.certTypeName = certTypeName;
    }

    public String getCertTemplateName() {
        return certTemplateName;
    }

    public void setCertTemplateName(String certTemplateName) {
        this.certTemplateName = certTemplateName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Printer007Object> getSettings() {
        return settings;
    }

    public void setSettings(List<Printer007Object> settings) {
        this.settings = settings;
    }
}
