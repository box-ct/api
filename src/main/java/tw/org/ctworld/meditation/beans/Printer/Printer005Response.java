package tw.org.ctworld.meditation.beans.Printer;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Printer005Response extends BaseResponse {

    private List<Printer005Object> items;

    public Printer005Response(int errCode, String errMsg, List<Printer005Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Printer005Object> getItems() {
        return items;
    }

    public void setItems(List<Printer005Object> items) {
        this.items = items;
    }
}
