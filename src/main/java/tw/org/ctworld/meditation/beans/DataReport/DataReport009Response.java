package tw.org.ctworld.meditation.beans.DataReport;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class DataReport009Response{

    private List<String>[] items;

    public DataReport009Response(List<String>[] items) {
        this.items = items;
    }

    public List<String>[] getItems() {
        return items;
    }

    public void setItems(List<String>[] items) {
        this.items = items;
    }
}
