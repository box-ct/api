package tw.org.ctworld.meditation.beans.UnitStartUp;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;

public class UnitStartUp007Object {

    private String
            photo,
            enrollUserId,
            enrollUserName,
            enrollUserCtDharmaName,
            gender,
            birthDate,
            dsaJobNameList,
            updateDtTm,
            mobileNum1;


    public UnitStartUp007Object(String enrollUserId, String enrollUserName, String enrollUserCtDharmaName,
                                String gender, Date birthDate, String dsaJobNameList, Date mUpdateDtTm, String mobileNum1) {
        this.photo = null;
        this.enrollUserId = enrollUserId;
        this.enrollUserName = enrollUserName;
        this.enrollUserCtDharmaName = enrollUserCtDharmaName;
        this.gender = (gender.equals("M")||gender.equals("男"))?"男":(gender.equals("F")||gender.equals("女"))?"女":"";
        this.birthDate = DateTime2DateString(birthDate);
        this.dsaJobNameList = dsaJobNameList;
        this.updateDtTm = CommonUtils.DateTime2DateTimeString(mUpdateDtTm);
        this.mobileNum1 = mobileNum1;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEnrollUserId() {
        return enrollUserId;
    }

    public void setEnrollUserId(String enrollUserId) {
        this.enrollUserId = enrollUserId;
    }

    public String getEnrollUserName() {
        return enrollUserName;
    }

    public void setEnrollUserName(String enrollUserName) {
        this.enrollUserName = enrollUserName;
    }

    public String getEnrollUserCtDharmaName() {
        return enrollUserCtDharmaName;
    }

    public void setEnrollUserCtDharmaName(String enrollUserCtDharmaName) {
        this.enrollUserCtDharmaName = enrollUserCtDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(String updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }
}
