package tw.org.ctworld.meditation.beans.UnitStartUp;

import java.util.List;

public class UnitStartUp004Request {

    private int peopleForecast;
    private String leaveUnitDate, leaveUnitTime, arriveCtDate, arriveCtTime, leaveCtDate, leaveCtTime, leaderMasterName,
            leaderMasterId, leaderMasterPhoneNum, eventLeader, eventLeaderMemberId, eventLeaderPhone, viceEventLeader1,
            viceEventLeader1MemberId, viceEventLeader1Phone, viceEventLeader2, viceEventLeader2MemberId,
            viceEventLeader2Phone, eventEnrollGroupNameList, eventUnitNote;
    private List<UnitStartUp004Object> eventUnitSpecialInputs;

    public int getPeopleForecast() {
        return peopleForecast;
    }

    public void setPeopleForecast(int peopleForecast) {
        this.peopleForecast = peopleForecast;
    }

    public String getLeaveUnitDate() {
        return leaveUnitDate;
    }

    public void setLeaveUnitDate(String leaveUnitDate) {
        this.leaveUnitDate = leaveUnitDate;
    }

    public String getLeaveUnitTime() {
        return leaveUnitTime;
    }

    public void setLeaveUnitTime(String leaveUnitTime) {
        this.leaveUnitTime = leaveUnitTime;
    }

    public String getArriveCtDate() {
        return arriveCtDate;
    }

    public void setArriveCtDate(String arriveCtDate) {
        this.arriveCtDate = arriveCtDate;
    }

    public String getArriveCtTime() {
        return arriveCtTime;
    }

    public void setArriveCtTime(String arriveCtTime) {
        this.arriveCtTime = arriveCtTime;
    }

    public String getLeaveCtDate() {
        return leaveCtDate;
    }

    public void setLeaveCtDate(String leaveCtDate) {
        this.leaveCtDate = leaveCtDate;
    }

    public String getLeaveCtTime() {
        return leaveCtTime;
    }

    public void setLeaveCtTime(String leaveCtTime) {
        this.leaveCtTime = leaveCtTime;
    }

    public String getLeaderMasterName() {
        return leaderMasterName;
    }

    public void setLeaderMasterName(String leaderMasterName) {
        this.leaderMasterName = leaderMasterName;
    }

    public String getLeaderMasterId() {
        return leaderMasterId;
    }

    public void setLeaderMasterId(String leaderMasterId) {
        this.leaderMasterId = leaderMasterId;
    }

    public String getLeaderMasterPhoneNum() {
        return leaderMasterPhoneNum;
    }

    public void setLeaderMasterPhoneNum(String leaderMasterPhoneNum) {
        this.leaderMasterPhoneNum = leaderMasterPhoneNum;
    }

    public String getEventLeader() {
        return eventLeader;
    }

    public void setEventLeader(String eventLeader) {
        this.eventLeader = eventLeader;
    }

    public String getEventLeaderMemberId() {
        return eventLeaderMemberId;
    }

    public void setEventLeaderMemberId(String eventLeaderMemberId) {
        this.eventLeaderMemberId = eventLeaderMemberId;
    }

    public String getEventLeaderPhone() {
        return eventLeaderPhone;
    }

    public void setEventLeaderPhone(String eventLeaderPhone) {
        this.eventLeaderPhone = eventLeaderPhone;
    }

    public String getViceEventLeader1() {
        return viceEventLeader1;
    }

    public void setViceEventLeader1(String viceEventLeader1) {
        this.viceEventLeader1 = viceEventLeader1;
    }

    public String getViceEventLeader1MemberId() {
        return viceEventLeader1MemberId;
    }

    public void setViceEventLeader1MemberId(String viceEventLeader1MemberId) {
        this.viceEventLeader1MemberId = viceEventLeader1MemberId;
    }

    public String getViceEventLeader1Phone() {
        return viceEventLeader1Phone;
    }

    public void setViceEventLeader1Phone(String viceEventLeader1Phone) {
        this.viceEventLeader1Phone = viceEventLeader1Phone;
    }

    public String getViceEventLeader2() {
        return viceEventLeader2;
    }

    public void setViceEventLeader2(String viceEventLeader2) {
        this.viceEventLeader2 = viceEventLeader2;
    }

    public String getViceEventLeader2MemberId() {
        return viceEventLeader2MemberId;
    }

    public void setViceEventLeader2MemberId(String viceEventLeader2MemberId) {
        this.viceEventLeader2MemberId = viceEventLeader2MemberId;
    }

    public String getViceEventLeader2Phone() {
        return viceEventLeader2Phone;
    }

    public void setViceEventLeader2Phone(String viceEventLeader2Phone) {
        this.viceEventLeader2Phone = viceEventLeader2Phone;
    }

    public String getEventEnrollGroupNameList() {
        return eventEnrollGroupNameList;
    }

    public void setEventEnrollGroupNameList(String eventEnrollGroupNameList) {
        this.eventEnrollGroupNameList = eventEnrollGroupNameList;
    }

    public List<UnitStartUp004Object> getEventUnitSpecialInputs() {
        return eventUnitSpecialInputs;
    }

    public void setEventUnitSpecialInputs(List<UnitStartUp004Object> eventUnitSpecialInputs) {
        this.eventUnitSpecialInputs = eventUnitSpecialInputs;
    }

    public String getEventUnitNote() {
        return eventUnitNote;
    }

    public void setEventUnitNote(String eventUnitNote) {
        this.eventUnitNote = eventUnitNote;
    }
}
