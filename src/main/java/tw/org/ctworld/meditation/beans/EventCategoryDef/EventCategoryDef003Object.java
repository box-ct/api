package tw.org.ctworld.meditation.beans.EventCategoryDef;

public class EventCategoryDef003Object {

    private long  id;
    private String eventCategoryId, eventCategoryName;
    private Boolean isCenterAvailable;

    public EventCategoryDef003Object(long id, String eventCategoryId, String eventCategoryName, Boolean isCenterAvailable) {
        this.id = id;
        this.eventCategoryId = eventCategoryId;
        this.eventCategoryName = eventCategoryName;
        this.isCenterAvailable = isCenterAvailable;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEventCategoryId() {
        return eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public Boolean getIsCenterAvailable() {
        return isCenterAvailable;
    }

    public void setIsCenterAvailable(Boolean centerAvailable) {
        isCenterAvailable = centerAvailable;
    }
}
