package tw.org.ctworld.meditation.beans;

public class UserSession {
    public enum Keys {

        RecordId("loginRecordId"),
        UnitId("loginUnitId"),
        UnitId2("loginUnitId2"),
        UnitName("loginUnitName"),
        UnitName2("loginUnitName2"),
        RoleType("loginRoleType"),
        UserId("loginUserId"),
        UserName("loginUserName"),
        UserJobTitle("loginUserJobTitle"),
        MobileNum("loginMobileNum"),
        IsDisabled("loginIsDisabled"),
        Group("loginGroup"),
        XUserName("XUserName"),
        UserIP("loginIP"),
        LogInTime("loginTime"),
        LoginUserInfo("LoginUserInfo"),
        TimeZone("TimeZone"),
        MemberId("MemberId");


        private final String value;

        Keys(String value) {
            this.value = value;
        }

        public String getValue() { return value; }
    }
}
