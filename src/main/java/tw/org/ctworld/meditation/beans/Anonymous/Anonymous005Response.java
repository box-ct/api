package tw.org.ctworld.meditation.beans.Anonymous;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Anonymous005Response extends BaseResponse {

    private List<Anonymous005_2Object> items;

    public Anonymous005Response(int errCode, String errMsg, List<Anonymous005_2Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Anonymous005_2Object> getItems() {
        return items;
    }

    public void setItems(List<Anonymous005_2Object> items) {
        this.items = items;
    }
}
