package tw.org.ctworld.meditation.beans.ClassReport;

import java.util.List;

public class ClassReportRequest {
    private int category;
    private List<String> memberIdList;

    public ClassReportRequest() {
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public List<String> getMemberIdList() {
        return memberIdList;
    }

    public void setMemberIdList(List<String> memberIdList) {
        this.memberIdList = memberIdList;
    }
}
