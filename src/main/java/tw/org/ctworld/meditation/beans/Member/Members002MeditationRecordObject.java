package tw.org.ctworld.meditation.beans.Member;

public class Members002MeditationRecordObject {

    private String memberId, classPeriodNum, className, gender, classGroupId, aliasName;
    private String classJobTitleList;
    private boolean isInClass, isDroppedClass, isFullAttended, isGraduated, isNotComeBack, isEnrollToNewUpperClass;
    private String nextNewClassName, classStatus, unitName;

    public Members002MeditationRecordObject(String memberId, String classPeriodNum, String className, String gender,
                                            String classGroupId, String aliasName, String classJobTitleList,
                                            Boolean isInClass, Boolean isDroppedClass, Boolean isFullAttended,
                                            Boolean isGraduated, Boolean isNotComeBack, Boolean isEnrollToNewUpperClass,
                                            String nextNewClassName, String classStatus, String unitName) {
        this.memberId = memberId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.gender = gender;
        this.classGroupId = classGroupId;
        this.aliasName = aliasName;
        this.classJobTitleList = classJobTitleList;
        this.isInClass = isInClass != null ? isInClass : false;
        this.isDroppedClass = isDroppedClass != null ? isDroppedClass : false;
        this.isFullAttended = isFullAttended !=null ? isFullAttended : false;
        this.isGraduated = isGraduated != null ? isGraduated : false;
        this.isNotComeBack = isNotComeBack != null ? isNotComeBack : false;
        this.isEnrollToNewUpperClass = isEnrollToNewUpperClass != null ? isEnrollToNewUpperClass : false;
        this.nextNewClassName = nextNewClassName;
        this.classStatus = classStatus;
        this.unitName = unitName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public boolean getIsInClass() {
        return isInClass;
    }

    public void setIsInClass(boolean inClass) {
        isInClass = inClass;
    }

    public boolean getIsDroppedClass() {
        return isDroppedClass;
    }

    public void setIsDroppedClass(boolean droppedClass) {
        isDroppedClass = droppedClass;
    }

    public boolean getIsFullAttended() {
        return isFullAttended;
    }

    public void setIsFullAttended(boolean isFullAttended) {
        this.isFullAttended = isFullAttended;
    }

    public boolean getIsGraduated() {
        return isGraduated;
    }

    public void setIsGraduated(boolean isGraduated) {
        this.isGraduated = isGraduated;
    }

    public boolean getIsNotComeBack() {
        return isNotComeBack;
    }

    public void setIsNotComeBack(boolean isNotComeBack) {
        this.isNotComeBack = isNotComeBack;
    }

    public boolean getIsEnrollToNewUpperClass() {
        return isEnrollToNewUpperClass;
    }

    public void setIsEnrollToNewUpperClass(boolean isEnrollToNewUpperClass) {
        this.isEnrollToNewUpperClass = isEnrollToNewUpperClass;
    }

    public String getNextNewClassName() {
        return nextNewClassName;
    }

    public void setNextNewClassName(String nextNewClassName) {
        this.nextNewClassName = nextNewClassName;
    }

    public boolean isDroppedClass() {
        return isDroppedClass;
    }

    public void setDroppedClass(boolean droppedClass) {
        isDroppedClass = droppedClass;
    }

    public String getClassStatus() {
        return classStatus;
    }

    public void setClassStatus(String classStatus) {
        this.classStatus = classStatus;
    }

    public boolean isInClass() {
        return isInClass;
    }

    public void setInClass(boolean inClass) {
        isInClass = inClass;
    }

    public boolean isFullAttended() {
        return isFullAttended;
    }

    public void setFullAttended(boolean fullAttended) {
        isFullAttended = fullAttended;
    }

    public boolean isGraduated() {
        return isGraduated;
    }

    public void setGraduated(boolean graduated) {
        isGraduated = graduated;
    }

    public boolean isNotComeBack() {
        return isNotComeBack;
    }

    public void setNotComeBack(boolean notComeBack) {
        isNotComeBack = notComeBack;
    }

    public boolean isEnrollToNewUpperClass() {
        return isEnrollToNewUpperClass;
    }

    public void setEnrollToNewUpperClass(boolean enrollToNewUpperClass) {
        isEnrollToNewUpperClass = enrollToNewUpperClass;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
