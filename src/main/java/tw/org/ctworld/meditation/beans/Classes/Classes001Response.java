package tw.org.ctworld.meditation.beans.Classes;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.Classes.Classes001Object;

import java.util.List;

public class Classes001Response extends BaseResponse {

    private List<Classes001Object> items;

    public Classes001Response(int errCode, String errMsg, List<Classes001Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Classes001Object> getItems() {
        return items;
    }

    public void setItems(List<Classes001Object> items) {
        this.items = items;
    }
}
