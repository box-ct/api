package tw.org.ctworld.meditation.beans.DataReport;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class DataReport007Response {

    private List<String>[] items;

    public DataReport007Response(List<String>[] items) {
        this.items = items;
    }

    public List<String>[] getItems() {
        return items;
    }

    public void setItems(List<String>[] items) {
        this.items = items;
    }
}
