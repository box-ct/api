package tw.org.ctworld.meditation.beans.CtEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class CtEventMaintain016Response extends BaseResponse {

    private List<CtEventMaintain016Object> items;

    public CtEventMaintain016Response(int errCode, String errMsg, List<CtEventMaintain016Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<CtEventMaintain016Object> getItems() {
        return items;
    }

    public void setItems(List<CtEventMaintain016Object> items) {
        this.items = items;
    }
}
