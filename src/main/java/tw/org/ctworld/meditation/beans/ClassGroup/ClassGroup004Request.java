package tw.org.ctworld.meditation.beans.ClassGroup;

import java.util.List;

public class ClassGroup004Request {

    private List<String> enrollFormIds;

    public List<String> getEnrollFormIds() {
        return enrollFormIds;
    }

    public void setEnrollFormIds(List<String> enrollFormIds) {
        this.enrollFormIds = enrollFormIds;
    }
}
