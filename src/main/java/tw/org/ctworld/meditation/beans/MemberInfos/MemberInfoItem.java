package tw.org.ctworld.meditation.beans.MemberInfos;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;

public class MemberInfoItem {
    private Boolean isDeleteByUnit;
    private String aliasName;
    private String birthDate;
    private String ctDharmaName;
    private String dsaJobNameList;
    private String gender;
    private String memberId;
    private String photo;
    private String twIdNum;
    private String unitId;
    private String unitName;
    private String updateDtTm;
    private String updatorName;

//    public MemberInfoItem(String aliasName, String memberId, String ctDharmaName, String gender, String birthDate,
//                          String dsaJobNameList, String updateDtTm, String photo) {
//        this.aliasName = aliasName;
//        this.memberId = memberId;
//        this.ctDharmaName = ctDharmaName;
//        this.gender = gender;
//        this.birthDate = birthDate;
//        this.dsaJobNameList = dsaJobNameList;
//        this.updateDtTm = updateDtTm;
//        this.photo = photo;
//    }
//
//    public MemberInfoItem(String aliasName, String memberId, String ctDharmaName, String gender, Date birthDate,
//                          String dsaJobNameList, Date updateDtTm, String twIdNum, Boolean isDeleteByUnit) {
//        this.aliasName = aliasName;
//        this.memberId = memberId;
//        this.ctDharmaName = ctDharmaName;
//        this.gender = gender;
//        this.birthDate = CommonUtils.DateTime2DateString(birthDate);
//        this.dsaJobNameList = dsaJobNameList;
//        this.updateDtTm = CommonUtils.DateTime2DateTimeString(updateDtTm);
//        this.twIdNum = twIdNum;
//        this.isDeleteByUnit = isDeleteByUnit;
//    }

    public MemberInfoItem(String aliasName, String memberId, String ctDharmaName, String gender, Date birthDate,
                          String dsaJobNameList, Date updateDtTm, String twIdNum, Boolean isDeleteByUnit,
                          String updatorName, String unitId, String unitName) {
        this.aliasName = aliasName;
        this.memberId = memberId;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.birthDate = CommonUtils.DateTime2DateString(birthDate);
        this.dsaJobNameList = dsaJobNameList;
        this.updateDtTm = CommonUtils.DateTime2DateTimeString(updateDtTm);
        this.twIdNum = twIdNum;
        this.isDeleteByUnit = isDeleteByUnit;
        this.updatorName = updatorName;

        this.unitId = unitId;
        this.unitName = unitName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList == null ? "" : dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(String updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public Boolean getIsDeleteByUnit() {
        return isDeleteByUnit;
    }

    public void setIsDeleteByUnit(Boolean deleteByUnit) {
        isDeleteByUnit = deleteByUnit;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
