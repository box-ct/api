package tw.org.ctworld.meditation.beans.UnitEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class UnitEventsMaintain008Response extends BaseResponse {

    private UnitEventsMaintain008Object result;

    public UnitEventsMaintain008Response(int errCode, String errMsg, UnitEventsMaintain008Object importedItem) {
        super(errCode, errMsg);
        this.result = importedItem;
    }

    public UnitEventsMaintain008Object getImportedItem() {
        return result;
    }

    public void setImportedItems(UnitEventsMaintain008Object importedItem) {
        this.result = importedItem;
    }
}
