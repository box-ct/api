package tw.org.ctworld.meditation.beans.Printer;

public class Printer007Object {

    private Long id;
    private String certTypeName, fieldName, fieldPositionNum, fontName;
    private int fontSize;
    private Boolean isBold, isItalic;
    private String horizontalAlignType, verticalAlignType, textDirection;
    private int width, height, yDistance, xDistance;
    private Boolean isEnabled;

    public Printer007Object(Long id, String certTypeName, String fieldName, String fieldPositionNum, String fontName, int fontSize,
                            Boolean isBold, Boolean isItalic, String horizontalAlignType, String verticalAlignType,
                            String textDirection, int width, int height, int yDistance, int xDistance,
                            Boolean isEnabled) {
        this.id = id;
        this.certTypeName = certTypeName;
        this.fieldName = fieldName;
        this.fieldPositionNum = fieldPositionNum;
        this.fontName = fontName;
        this.fontSize = fontSize;
        this.isBold = isBold;
        this.isItalic = isItalic;
        this.horizontalAlignType = horizontalAlignType;
        this.verticalAlignType = verticalAlignType;
        this.textDirection = textDirection;
        this.width = width;
        this.height = height;
        this.yDistance = yDistance;
        this.xDistance = xDistance;
        this.isEnabled = isEnabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertTypeName() {
        return certTypeName;
    }

    public void setCertTypeName(String certTypeName) {
        this.certTypeName = certTypeName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldPositionNum() {
        return fieldPositionNum;
    }

    public void setFieldPositionNum(String fieldPositionNum) {
        this.fieldPositionNum = fieldPositionNum;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public Boolean getIsBold() {
        return isBold;
    }

    public void setIsBold(Boolean bold) {
        isBold = bold;
    }

    public Boolean getIsItalic() {
        return isItalic;
    }

    public void setIsItalic(Boolean italic) {
        isItalic = italic;
    }

    public String getHorizontalAlignType() {
        return horizontalAlignType == null ? "" : horizontalAlignType;
    }

    public void setHorizontalAlignType(String horizontalAlignType) {
        this.horizontalAlignType = horizontalAlignType;
    }

    public String getVerticalAlignType() {
        return verticalAlignType == null ? "" : verticalAlignType;
    }

    public void setVerticalAlignType(String verticalAlignType) {
        this.verticalAlignType = verticalAlignType;
    }

    public String getTextDirection() {
        return textDirection;
    }

    public void setTextDirection(String textDirection) {
        this.textDirection = textDirection;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getyDistance() {
        return yDistance;
    }

    public void setyDistance(int yDistance) {
        this.yDistance = yDistance;
    }

    public int getxDistance() {
        return xDistance;
    }

    public void setxDistance(int xDistance) {
        this.xDistance = xDistance;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean enabled) {
        isEnabled = enabled;
    }
}
