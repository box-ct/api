package tw.org.ctworld.meditation.beans.BatchRegister;

import java.util.List;

public class BatchRegister001Request {

    private boolean simulation;
    private List<BatchRegister001_1Object> members;

    public boolean getSimulation() {
        return simulation;
    }

    public void setSimulation(boolean simulation) {
        this.simulation = simulation;
    }

    public List<BatchRegister001_1Object> getMembers() {
        return members;
    }

    public void setMembers(List<BatchRegister001_1Object> members) {
        this.members = members;
    }
}
