package tw.org.ctworld.meditation.beans.DataReport;

import java.util.Date;

public class DataReport007Object {

    private String unitId;
    private boolean overseasUnit;
    private Date classDate;
    private String memberId, classId, classEnrollFormId, attendMark, classDayOrNight, classTypeName, classLanguage;

    public DataReport007Object(String unitId, boolean overseasUnit) {
        this.unitId = unitId;
        this.overseasUnit = overseasUnit;
    }

    public DataReport007Object(String unitId, Date classDate, String memberId, String classId, String classEnrollFormId,
                               String attendMark) {
        this.unitId = unitId;
        this.classDate = classDate;
        this.memberId = memberId;
        this.classId = classId;
        this.classEnrollFormId = classEnrollFormId;
        this.attendMark = attendMark;
    }

    public DataReport007Object(String unitId, Date classDate, String memberId, String classId, String classEnrollFormId,
                               String attendMark, String classDayOrNight, String classTypeName, String classLanguage) {
        this.unitId = unitId;
        this.classDate = classDate;
        this.memberId = memberId;
        this.classId = classId;
        this.classEnrollFormId = classEnrollFormId;
        this.attendMark = attendMark;
        this.classDayOrNight = classDayOrNight;
        this.classTypeName = classTypeName;
        this.classLanguage = classLanguage;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public boolean getOverseasUnit() {
        return overseasUnit;
    }

    public void setOverseasUnit(boolean overseasUnit) {
        this.overseasUnit = overseasUnit;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }

    public String getClassDayOrNight() {
        return classDayOrNight;
    }

    public void setClassDayOrNight(String classDayOrNight) {
        this.classDayOrNight = classDayOrNight;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassLanguage() {
        return classLanguage;
    }

    public void setClassLanguage(String classLanguage) {
        this.classLanguage = classLanguage;
    }
}
