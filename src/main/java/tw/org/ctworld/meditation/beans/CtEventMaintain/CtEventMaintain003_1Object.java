package tw.org.ctworld.meditation.beans.CtEventMaintain;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;
import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateTimeString;

public class CtEventMaintain003_1Object {

    private String eventYear, sponsorUnitName, eventCategoryName, eventName, eventStartDate, eventEndDate;
    private int masterEnrollCount, notMasterEnrollCount, peopleForecast;
    private String status, eventNote, eventId, updatorName, updateDtTm;
    private Date mUpdateDtTm;

    public CtEventMaintain003_1Object(String eventYear, String sponsorUnitName, String eventCategoryName,
                                      String eventName, String eventStartDate, String eventEndDate, int masterEnrollCount,
                                      int notMasterEnrollCount, int peopleForecast, String status, String eventNote,
                                      String eventId, String updatorName, String updateDtTm) {
        this.eventYear = eventYear;
        this.sponsorUnitName = sponsorUnitName;
        this.eventCategoryName = eventCategoryName;
        this.eventName = eventName;
        this.eventStartDate = eventStartDate;
        this.eventEndDate = eventEndDate;
        this.masterEnrollCount = masterEnrollCount;
        this.notMasterEnrollCount = notMasterEnrollCount;
        this.peopleForecast = peopleForecast;
        this.status = status;
        this.eventNote = eventNote;
        this.eventId = eventId;
        this.updatorName = updatorName;
        this.updateDtTm = updateDtTm;
    }

    public CtEventMaintain003_1Object(String eventYear, String sponsorUnitName, String eventCategoryName,
                                      String eventName, Date eventStartDate, Date eventEndDate, int status,
                                      String eventNote, String eventId, String updatorName, Date updateDtTm,
                                      Date mUpdateDtTm) {
        this.eventYear = eventYear;
        this.sponsorUnitName = sponsorUnitName;
        this.eventCategoryName = eventCategoryName;
        this.eventName = eventName;
        this.eventStartDate = DateTime2DateString(eventStartDate);
        this.eventEndDate = DateTime2DateString(eventEndDate);
        this.status = String.valueOf(status);
        this.eventNote = eventNote;
        this.eventId = eventId;
        this.updatorName = updatorName;
        this.updateDtTm = null;
        this.mUpdateDtTm = mUpdateDtTm;
    }

    public String getEventYear() {
        return eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public String getSponsorUnitName() {
        return sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getEventCategoryName() {
        return eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public int getMasterEnrollCount() {
        return masterEnrollCount;
    }

    public void setMasterEnrollCount(int masterEnrollCount) {
        this.masterEnrollCount = masterEnrollCount;
    }

    public int getNotMasterEnrollCount() {
        return notMasterEnrollCount;
    }

    public void setNotMasterEnrollCount(int notMasterEnrollCount) {
        this.notMasterEnrollCount = notMasterEnrollCount;
    }

    public int getPeopleForecast() {
        return peopleForecast;
    }

    public void setPeopleForecast(int peopleForecast) {
        this.peopleForecast = peopleForecast;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEventNote() {
        return eventNote;
    }

    public void setEventNote(String eventNote) {
        this.eventNote = eventNote;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

    public String getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(String updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public Date getmUpdateDtTm() {
        return mUpdateDtTm;
    }

    public void setmUpdateDtTm(Date mUpdateDtTm) {
        this.mUpdateDtTm = mUpdateDtTm;
    }
}
