package tw.org.ctworld.meditation.beans.MemberInfos;

public class NewMemberNoteRequest {
    private String note;
    private String noteType;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }
}
