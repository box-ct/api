package tw.org.ctworld.meditation.beans.EventCategoryDef;

import java.util.List;

public class EventCategoryDef008Request {

    private List<EventCategoryDef008Object> items;

    public List<EventCategoryDef008Object> getItems() {
        return items;
    }

    public void setItems(List<EventCategoryDef008Object> items) {
        this.items = items;
    }
}
