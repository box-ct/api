package tw.org.ctworld.meditation.beans.Sys;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class SearchMemberResponse extends BaseResponse {
    private List<SearchMember> items;

    public SearchMemberResponse() {
    }

    public SearchMemberResponse(int errCode, String errMsg, List<SearchMember> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<SearchMember> getItems() {
        return items;
    }

    public void setItems(List<SearchMember> items) {
        this.items = items;
    }
}
