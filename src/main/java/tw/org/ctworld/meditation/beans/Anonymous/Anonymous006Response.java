package tw.org.ctworld.meditation.beans.Anonymous;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Anonymous006Response extends BaseResponse {

    private List<Anonymous006_1Object> items;

    public Anonymous006Response(int errCode, String errMsg, List<Anonymous006_1Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public Anonymous006Response(List<Anonymous006_1Object> items) {
        this.items = items;
    }

    public List<Anonymous006_1Object> getItems() {
        return items;
    }

    public void setItems(List<Anonymous006_1Object> items) {
        this.items = items;
    }
}
