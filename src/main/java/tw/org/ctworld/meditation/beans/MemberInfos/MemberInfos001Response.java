package tw.org.ctworld.meditation.beans.MemberInfos;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class MemberInfos001Response extends BaseResponse {

    private List<MemberInfos001_1Object> items;

    public MemberInfos001Response(int errCode, String errMsg, List<MemberInfos001_1Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<MemberInfos001_1Object> getItems() {
        return items;
    }

    public void setItems(List<MemberInfos001_1Object> items) {
        this.items = items;
    }
}
