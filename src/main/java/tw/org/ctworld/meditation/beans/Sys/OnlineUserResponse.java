package tw.org.ctworld.meditation.beans.Sys;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.UserLoginLog;

import java.util.List;

public class OnlineUserResponse extends BaseResponse {
    private List<UserLoginLog> items;

    public OnlineUserResponse(int errCode, String errMsg, List<UserLoginLog> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<UserLoginLog> getItems() {
        return items;
    }

    public void setItems(List<UserLoginLog> items) {
        this.items = items;
    }
}
