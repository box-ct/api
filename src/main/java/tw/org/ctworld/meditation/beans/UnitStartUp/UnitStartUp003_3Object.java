package tw.org.ctworld.meditation.beans.UnitStartUp;


public class UnitStartUp003_3Object {
    
    private int id;
    private String sInputId, sInputName, sInputValue, sInputExample, sInputCategory, sInputWriteType;

    private Boolean isRequired;

    public UnitStartUp003_3Object(long id, String sInputId, String sInputName, String sInputValue, Boolean isRequired,
                                  String sInputExample, String sInputCategory, String sInputWriteType) {
        this.id = (int) id;
        this.sInputId = sInputId;
        this.sInputName = sInputName;
        this.sInputValue = sInputValue;
        this.isRequired = isRequired;
        this.sInputExample = sInputExample;
        this.sInputCategory = sInputCategory;
        this.sInputWriteType = sInputWriteType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getsInputId() {
        return sInputId;
    }

    public void setsInputId(String sInputId) {
        this.sInputId = sInputId;
    }

    public String getsInputName() {
        return sInputName;
    }

    public void setsInputName(String sInputName) {
        this.sInputName = sInputName;
    }

    public String getsInputValue() {
        return sInputValue;
    }

    public void setsInputValue(String sInputValue) {
        this.sInputValue = sInputValue;
    }

    public String getsInputExample() {
        return sInputExample;
    }

    public void setsInputExample(String sInputExample) {
        this.sInputExample = sInputExample;
    }

    public String getsInputCategory() {
        return sInputCategory;
    }

    public void setsInputCategory(String sInputCategory) {
        this.sInputCategory = sInputCategory;
    }

    public String getsInputWriteType() {
        return sInputWriteType;
    }

    public void setsInputWriteType(String sInputWriteType) {
        this.sInputWriteType = sInputWriteType;
    }

    public Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean required) {
        isRequired = required;
    }
}
