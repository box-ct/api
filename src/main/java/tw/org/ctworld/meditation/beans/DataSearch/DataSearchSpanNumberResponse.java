package tw.org.ctworld.meditation.beans.DataSearch;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class DataSearchSpanNumberResponse extends BaseResponse {
    private List<SpanNumbers> items;

    public DataSearchSpanNumberResponse(int errCode, String errMsg, List<SpanNumbers> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<SpanNumbers> getItems() {
        return items;
    }

    public void setItems(List<SpanNumbers> items) {
        this.items = items;
    }

    public static class SpanNumbers {
        private int spanNumber;
        private int memberMNumber;
        private int memberFNumber;
        private int leaderMNumber;
        private int leaderFNumber;

        public SpanNumbers(int spanNumber, int memberMNumber, int memberFNumber, int leaderMNumber, int leaderFNumber) {
            this.spanNumber = spanNumber;
            this.memberMNumber = memberMNumber;
            this.memberFNumber = memberFNumber;
            this.leaderMNumber = leaderMNumber;
            this.leaderFNumber = leaderFNumber;
        }

        public SpanNumbers() {
        }

        public int getSpanNumber() {
            return spanNumber;
        }

        public void setSpanNumber(int spanNumber) {
            this.spanNumber = spanNumber;
        }

        public int getMemberMNumber() {
            return memberMNumber;
        }

        public void setMemberMNumber(int memberMNumber) {
            this.memberMNumber = memberMNumber;
        }

        public int getMemberFNumber() {
            return memberFNumber;
        }

        public void setMemberFNumber(int memberFNumber) {
            this.memberFNumber = memberFNumber;
        }

        public int getLeaderMNumber() {
            return leaderMNumber;
        }

        public void setLeaderMNumber(int leaderMNumber) {
            this.leaderMNumber = leaderMNumber;
        }

        public int getLeaderFNumber() {
            return leaderFNumber;
        }

        public void setLeaderFNumber(int leaderFNumber) {
            this.leaderFNumber = leaderFNumber;
        }
    }
}
