package tw.org.ctworld.meditation.beans.Public;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class TrainingFileResponse extends BaseResponse {
    private List<TrainingFile> items;

    public TrainingFileResponse(int errCode, String errMsg, List<TrainingFile> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<TrainingFile> getItems() {
        return items;
    }

    public void setItems(List<TrainingFile> items) {
        this.items = items;
    }
}
