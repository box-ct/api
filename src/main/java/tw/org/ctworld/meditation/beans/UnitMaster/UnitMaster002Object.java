package tw.org.ctworld.meditation.beans.UnitMaster;

import tw.org.ctworld.meditation.models.UnitMasterInfo;

import java.util.List;

public class UnitMaster002Object {
    private String unitName,certUnitName,unitId,fundUnitId,certUnitEngName,unitPhoneNum,unitFaxNum,timeZone,unitPhoneNumCode,unitMobileNum,unitEmail,unitMailingAddress,stateName,
            abbotId,abbotName,abbotEngName,district,groupName;
    private boolean isAbbot,isTreasurer,isOverseasUnit;
    private List<UnitMasterInfo> unitMasterInfos;


    public UnitMaster002Object(String unitName, String certUnitName, String unitId, String fundUnitId, String certUnitEngName, String unitPhoneNum, String unitFaxNum,
                               String timeZone, String unitPhoneNumCode, String unitMobileNum, String unitEmail, String unitMailingAddress, String stateName, String abbotId, String abbotName,
                               String abbotEngName, Boolean isOverseasUnit, String district, String groupName, List<UnitMasterInfo> unitMasterInfos){
        this.unitName=unitName;
        this.abbotEngName=abbotEngName;
        this.abbotId=abbotId;
        this.abbotName=abbotName;
        this.certUnitEngName=certUnitEngName;
        this.certUnitName=certUnitName;
        this.fundUnitId=fundUnitId;
        this.unitEmail=unitEmail;
        this.unitId=unitId;
        this.unitPhoneNum=unitPhoneNum;
        this.unitFaxNum=unitFaxNum;
        this.timeZone=timeZone;
        this.unitPhoneNumCode=unitPhoneNumCode;
        this.unitMobileNum=unitMobileNum;
        this.unitMailingAddress=unitMailingAddress;
        this.isOverseasUnit=isOverseasUnit;
        this.stateName=stateName;
        this.district=district;
        this.groupName=(groupName.equals("本山"))?"本山單位":(isOverseasUnit)?"海外精舍":"國內精舍";
        //list
        this.unitMasterInfos=unitMasterInfos;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getCertUnitName() {
        return certUnitName;
    }

    public void setCertUnitName(String certUnitName) {
        this.certUnitName = certUnitName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getFundUnitId() {
        return fundUnitId;
    }

    public void setFundUnitId(String fundUnitId) {
        this.fundUnitId = fundUnitId;
    }

    public String getCertUnitEngName() {
        return certUnitEngName;
    }

    public void setCertUnitEngName(String certUnitEngName) {
        this.certUnitEngName = certUnitEngName;
    }

    public String getUnitPhoneNum() {
        return unitPhoneNum;
    }

    public void setUnitPhoneNum(String unitPhoneNum) {
        this.unitPhoneNum = unitPhoneNum;
    }

    public String getUnitFaxNum() {
        return unitFaxNum;
    }

    public void setUnitFaxNum(String unitFaxNum) {
        this.unitFaxNum = unitFaxNum;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getUnitPhoneNumCode() {
        return unitPhoneNumCode;
    }

    public void setUnitPhoneNumCode(String unitPhoneNumCode) {
        this.unitPhoneNumCode = unitPhoneNumCode;
    }

    public String getUnitMobileNum() {
        return unitMobileNum;
    }

    public void setUnitMobileNum(String unitMobileNum) {
        this.unitMobileNum = unitMobileNum;
    }

    public String getUnitEmail() {
        return unitEmail;
    }

    public void setUnitEmail(String unitEmail) {
        this.unitEmail = unitEmail;
    }

    public String getUnitMailingAddress() {
        return unitMailingAddress;
    }

    public void setUnitMailingAddress(String unitMailingAddress) {
        this.unitMailingAddress = unitMailingAddress;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getAbbotId() {
        return abbotId;
    }

    public void setAbbotId(String abbotId) {
        this.abbotId = abbotId;
    }

    public String getAbbotName() {
        return abbotName;
    }

    public void setAbbotName(String abbotName) {
        this.abbotName = abbotName;
    }

    public String getAbbotEngName() {
        return abbotEngName;
    }

    public void setAbbotEngName(String abbotEngName) {
        this.abbotEngName = abbotEngName;
    }

    public boolean getIsAbbot() {
        return isAbbot;
    }

    public void setIsAbbot(boolean abbot) {
        isAbbot = abbot;
    }

    public boolean getIsTreasurer() {
        return isTreasurer;
    }

    public void setIsTreasurer(boolean treasurer) {
        isTreasurer = treasurer;
    }

    public boolean getIsOverseasUnit() {
        return isOverseasUnit;
    }

    public void setIsOverseasUnit(boolean overseasUnit) {
        isOverseasUnit = overseasUnit;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }


    public List<UnitMasterInfo> getUnitMasterInfos() {
        return unitMasterInfos;
    }

    public void setUnitMasterInfos(List<UnitMasterInfo> unitMasterInfos) {
        this.unitMasterInfos = unitMasterInfos;
    }
}
