package tw.org.ctworld.meditation.beans.Anonymous;

public class Anonymous007Object {

    private String memberId, enrollFormId, aliasName, ctDharmaName, mobileNum1;
    private Boolean isAttend;

    public Anonymous007Object(String memberId, String enrollFormId, String aliasName, String ctDharmaName, String mobileNum1, Boolean isAttend) {
        this.memberId = memberId;
        this.enrollFormId = enrollFormId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.mobileNum1 = mobileNum1;
        this.isAttend = isAttend;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getEnrollFormId() {
        return enrollFormId;
    }

    public void setEnrollFormId(String enrollFormId) {
        this.enrollFormId = enrollFormId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName == null ? "" : ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getMobileNum1() {
        return mobileNum1 == null ? "" : mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public Boolean getIsAttend() {
        return isAttend;
    }

    public void setIsAttend(Boolean attend) {
        isAttend = attend;
    }
}
