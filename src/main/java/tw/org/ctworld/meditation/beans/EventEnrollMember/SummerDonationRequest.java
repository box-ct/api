package tw.org.ctworld.meditation.beans.EventEnrollMember;

public class SummerDonationRequest {
    private Integer specialDonationSum;
    private Integer summerDonation1and2Sum;
    private Integer summerDonation1Count;
    private String enrollUserId;
    private String enrollUserName;
    private String onThatDayDonation;
    private String specialDonation;
    private String specialDonationCompleteNote;
    private String summerDonation1;
    private String summerDonation2;
    private String summerDonationType;
    private String summerDonationYear;
    private String summerRelativeMeritList;
    private String unitId;
    private String unitName;

    public Integer getSpecialDonationSum() {
        return specialDonationSum;
    }

    public void setSpecialDonationSum(Integer specialDonationSum) {
        this.specialDonationSum = specialDonationSum;
    }

    public Integer getSummerDonation1and2Sum() {
        return summerDonation1and2Sum;
    }

    public void setSummerDonation1and2Sum(Integer summerDonation1and2Sum) {
        this.summerDonation1and2Sum = summerDonation1and2Sum;
    }

    public Integer getSummerDonation1Count() {
        return summerDonation1Count;
    }

    public void setSummerDonation1Count(Integer summerDonation1Count) {
        this.summerDonation1Count = summerDonation1Count;
    }

    public String getEnrollUserId() {
        return enrollUserId;
    }

    public void setEnrollUserId(String enrollUserId) {
        this.enrollUserId = enrollUserId;
    }

    public String getEnrollUserName() {
        return enrollUserName;
    }

    public void setEnrollUserName(String enrollUserName) {
        this.enrollUserName = enrollUserName;
    }

    public String getOnThatDayDonation() {
        return onThatDayDonation;
    }

    public void setOnThatDayDonation(String onThatDayDonation) {
        this.onThatDayDonation = onThatDayDonation;
    }

    public String getSpecialDonation() {
        return specialDonation;
    }

    public void setSpecialDonation(String specialDonation) {
        this.specialDonation = specialDonation;
    }

    public String getSpecialDonationCompleteNote() {
        return specialDonationCompleteNote;
    }

    public void setSpecialDonationCompleteNote(String specialDonationCompleteNote) {
        this.specialDonationCompleteNote = specialDonationCompleteNote;
    }

    public String getSummerDonation1() {
        return summerDonation1;
    }

    public void setSummerDonation1(String summerDonation1) {
        this.summerDonation1 = summerDonation1;
    }

    public String getSummerDonation2() {
        return summerDonation2;
    }

    public void setSummerDonation2(String summerDonation2) {
        this.summerDonation2 = summerDonation2;
    }

    public String getSummerDonationType() {
        return summerDonationType;
    }

    public void setSummerDonationType(String summerDonationType) {
        this.summerDonationType = summerDonationType;
    }

    public String getSummerDonationYear() {
        return summerDonationYear;
    }

    public void setSummerDonationYear(String summerDonationYear) {
        this.summerDonationYear = summerDonationYear;
    }

    public String getSummerRelativeMeritList() {
        return summerRelativeMeritList;
    }

    public void setSummerRelativeMeritList(String summerRelativeMeritList) {
        this.summerRelativeMeritList = summerRelativeMeritList;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
