package tw.org.ctworld.meditation.beans.Classes;

public class Classes001Object {

    private String year, classId, classStartDate, classEndDate, dayOfWeek, classDayOrNight, classTypeName,
            classPeriodNum, className, classTotalNum, classDesc, classStatus;

    public Classes001Object(String year, String classId, String classStartDate, String classEndDate, String dayOfWeek,
                            String classDayOrNight, String classTypeName, String classPeriodNum, String className,
                            String classTotalNum, String classDesc, String classStatus) {
        this.year = year;
        this.classId = classId;
        this.classStartDate = classStartDate;
        this.classEndDate = classEndDate;
        this.dayOfWeek = dayOfWeek;
        this.classDayOrNight = classDayOrNight;
        this.classTypeName = classTypeName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classTotalNum = classTotalNum;
        this.classDesc = classDesc;
        this.classStatus = classStatus;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassStartDate() {
        return classStartDate;
    }

    public void setClassStartDate(String classStartDate) {
        this.classStartDate = classStartDate;
    }

    public String getClassEndDate() {
        return classEndDate;
    }

    public void setClassEndDate(String classEndDate) {
        this.classEndDate = classEndDate;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getClassDayOrNight() {
        return classDayOrNight;
    }

    public void setClassDayOrNight(String classDayOrNight) {
        this.classDayOrNight = classDayOrNight;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassTotalNum() {
        return classTotalNum;
    }

    public void setClassTotalNum(String classTotalNum) {
        this.classTotalNum = classTotalNum;
    }

    public String getClassDesc() {
        return classDesc;
    }

    public void setClassDesc(String classDesc) {
        this.classDesc = classDesc;
    }

    public String getClassStatus() {
        return classStatus;
    }

    public void setClassStatus(String classStatus) {
        this.classStatus = classStatus;
    }
}
