package tw.org.ctworld.meditation.beans.Makeup;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class ClassesResponse extends BaseResponse {
    private List<MakeupClassInfo> items;

    public ClassesResponse(int errCode, String errMsg, List<MakeupClassInfo> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<MakeupClassInfo> getItems() {
        return items;
    }

    public void setItems(List<MakeupClassInfo> items) {
        this.items = items;
    }
}
