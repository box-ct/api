package tw.org.ctworld.meditation.beans.EventPgmDef;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class EventPgmDef001Response extends BaseResponse {

    private List<EventPgmDef001Object> items;

    public EventPgmDef001Response(int errCode, String errMsg, List<EventPgmDef001Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<EventPgmDef001Object> getItems() {
        return items;
    }

    public void setItems(List<EventPgmDef001Object> items) {
        this.items = items;
    }
}
