package tw.org.ctworld.meditation.beans.UnitStartUp;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class UnitStartUp002Response extends BaseResponse {

    private List<UnitStartUp002_1Object> items;

    public UnitStartUp002Response(int errCode, String errMsg, List<UnitStartUp002_1Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<UnitStartUp002_1Object> getItems() {
        return items;
    }

    public void setItems(List<UnitStartUp002_1Object> items) {
        this.items = items;
    }
}
