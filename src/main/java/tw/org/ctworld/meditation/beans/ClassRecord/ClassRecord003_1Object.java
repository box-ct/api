package tw.org.ctworld.meditation.beans.ClassRecord;

import java.util.Date;
import java.util.List;

public class ClassRecord003_1Object {

    private String memberId, aliasName, ctDharmaName, gender, className, classGroupId, memberGroupNum, classJobTitleList;
    private boolean isDroppedClass, changeAndWaitPrintMark;
    private Date classDroppedDate;
    private boolean userDefinedLabel1, userDefinedLabel2;
    private String newReturnChangeMark;
    private Date newReturnChangeDate;
    private String classErollFormNote;

    public ClassRecord003_1Object(String memberId, String aliasName, String ctDharmaName, String gender,
                                  String className, String classGroupId, String memberGroupNum,
                                  String classJobTitleList, Boolean isDroppedClass, Boolean changeAndWaitPrintMark,
                                  Date classDroppedDate, Boolean userDefinedLabel1, Boolean userDefinedLabel2,
                                  String newReturnChangeMark, Date newReturnChangeDate, String classErollFormNote) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classJobTitleList = classJobTitleList;
        this.isDroppedClass = isDroppedClass != null ? isDroppedClass : false;
        this.changeAndWaitPrintMark = changeAndWaitPrintMark != null ? changeAndWaitPrintMark : false;
        this.classDroppedDate = classDroppedDate;
        this.userDefinedLabel1 = userDefinedLabel1 != null ? userDefinedLabel1 : false;
        this.userDefinedLabel2 = userDefinedLabel2 != null ? userDefinedLabel2 : false;
        this.newReturnChangeMark = newReturnChangeMark;
        this.newReturnChangeDate = newReturnChangeDate;
        this.classErollFormNote = classErollFormNote;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public boolean getIsDroppedClass() {
        return isDroppedClass;
    }

    public void setIsDroppedClass(boolean droppedClass) {
        isDroppedClass = droppedClass;
    }

    public boolean isChangeAndWaitPrintMark() {
        return changeAndWaitPrintMark;
    }

    public void setChangeAndWaitPrintMark(boolean changeAndWaitPrintMark) {
        this.changeAndWaitPrintMark = changeAndWaitPrintMark;
    }

    public Date getClassDroppedDate() {
        return classDroppedDate;
    }

    public void setClassDroppedDate(Date classDroppedDate) {
        this.classDroppedDate = classDroppedDate;
    }

    public boolean getUserDefinedLabel1() {
        return userDefinedLabel1;
    }

    public void setUserDefinedLabel1(boolean userDefinedLabel1) {
        this.userDefinedLabel1 = userDefinedLabel1;
    }

    public boolean getUserDefinedLabel2() {
        return userDefinedLabel2;
    }

    public void setUserDefinedLabel2(boolean userDefinedLabel2) {
        this.userDefinedLabel2 = userDefinedLabel2;
    }

    public String getNewReturnChangeMark() {
        return newReturnChangeMark;
    }

    public void setNewReturnChangeMark(String newReturnChangeMark) {
        this.newReturnChangeMark = newReturnChangeMark;
    }

    public Date getNewReturnChangeDate() {
        return newReturnChangeDate;
    }

    public void setNewReturnChangeDate(Date newReturnChangeDate) {
        this.newReturnChangeDate = newReturnChangeDate;
    }

    public String getClassErollFormNote() {
        return classErollFormNote;
    }

    public void setClassErollFormNote(String classErollFormNote) {
        this.classErollFormNote = classErollFormNote;
    }
}
