package tw.org.ctworld.meditation.beans.Classes;

import java.util.List;

public class Classes010Request {

    private String classGroupId;
    private List<String> classJobTitleList, classJobContentList;
    private boolean isLeadGroup, isExecuteClassAttendRecord, isExecuteClassGraduateStats, isExecuteClassUpGrade,
            isSeeAllClassRecord;

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public List<String> getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(List<String> classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public List<String> getClassJobContentList() {
        return classJobContentList;
    }

    public void setClassJobContentList(List<String> classJobContentList) {
        this.classJobContentList = classJobContentList;
    }

    public boolean getIsLeadGroup() {
        return isLeadGroup;
    }

    public void setIsLeadGroup(boolean leadGroup) {
        isLeadGroup = leadGroup;
    }

    public boolean getIsExecuteClassAttendRecord() {
        return isExecuteClassAttendRecord;
    }

    public void setIsExecuteClassAttendRecord(boolean executeClassAttendRecord) {
        isExecuteClassAttendRecord = executeClassAttendRecord;
    }

    public boolean getIsExecuteClassGraduateStats() {
        return isExecuteClassGraduateStats;
    }

    public void setIsExecuteClassGraduateStats(boolean executeClassGraduateStats) {
        isExecuteClassGraduateStats = executeClassGraduateStats;
    }

    public boolean getIsExecuteClassUpGrade() {
        return isExecuteClassUpGrade;
    }

    public void setIsExecuteClassUpGrade(boolean executeClassUpGrade) {
        isExecuteClassUpGrade = executeClassUpGrade;
    }

    public boolean getIsSeeAllClassRecord() {
        return isSeeAllClassRecord;
    }

    public void setIsSeeAllClassRecord(boolean seeAllClassRecord) {
        isSeeAllClassRecord = seeAllClassRecord;
    }
}
