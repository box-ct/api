package tw.org.ctworld.meditation.beans.ClassReport;

import tw.org.ctworld.meditation.models.CtMemberInfo;

import java.util.List;

public class ClassMemberRecord {
    private String photo;
    private String memberId;
    private String gender;
    private String aliasName;
    private String ctDharmaName;
    private int classPeriodNum;
    private String className;
    private String classGroupId;
    private String memberGroupNum;
    private String address;
    private String companyNameAndJobTitle;
    private String schoolNameAndMajor;

    private List<ClassAttendance> attendRecordList;

    private int weekTotal;
    private int week;
    private int percent;
    private boolean droppedClass;
    private String previousClassName;
    private String birthDate;
    private String twIdNum;
    private String dsaJobNameList;
    private String homePhoneNum1;
    private String mobileNum1;

    private String currentClassIntroducerName;
    private String currentClassIntroducerRelationship;
    private String classEnrollFormNote;
    private boolean changeAndWaitPrintMark;
    private String classTypeName;
    private String classJobTitleList;
    private int age;

    private String dayOfWeek;
    private String dayOrNight;

    private boolean fullAttended;
    private boolean graduated;
    private boolean homeClass;
    private String homeClassNote;

    private CtMemberInfo memberInfo;


    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public int getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(int classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyNameAndJobTitle() {
        return companyNameAndJobTitle;
    }

    public void setCompanyNameAndJobTitle(String companyNameAndJobTitle) {
        this.companyNameAndJobTitle = companyNameAndJobTitle;
    }

    public String getSchoolNameAndMajor() {
        return schoolNameAndMajor;
    }

    public void setSchoolNameAndMajor(String schoolNameAndMajor) {
        this.schoolNameAndMajor = schoolNameAndMajor;
    }

    public List<ClassAttendance> getAttendRecordList() {
        return attendRecordList;
    }

    public void setAttendRecordList(List<ClassAttendance> attendRecordList) {
        this.attendRecordList = attendRecordList;
    }

    public int getWeekTotal() {
        return weekTotal;
    }

    public void setWeekTotal(int weekTotal) {
        this.weekTotal = weekTotal;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public boolean isDroppedClass() {
        return droppedClass;
    }

    public void setDroppedClass(boolean droppedClass) {
        this.droppedClass = droppedClass;
    }

    public String getPreviousClassName() {
        return previousClassName;
    }

    public void setPreviousClassName(String previousClassName) {
        this.previousClassName = previousClassName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getCurrentClassIntroducerName() {
        return currentClassIntroducerName;
    }

    public void setCurrentClassIntroducerName(String currentClassIntroducerName) {
        this.currentClassIntroducerName = currentClassIntroducerName;
    }

    public String getCurrentClassIntroducerRelationship() {
        return currentClassIntroducerRelationship;
    }

    public void setCurrentClassIntroducerRelationship(String currentClassIntroducerRelationship) {
        this.currentClassIntroducerRelationship = currentClassIntroducerRelationship;
    }

    public String getClassEnrollFormNote() {
        return classEnrollFormNote;
    }

    public void setClassEnrollFormNote(String classEnrollFormNote) {
        this.classEnrollFormNote = classEnrollFormNote;
    }

    public boolean isChangeAndWaitPrintMark() {
        return changeAndWaitPrintMark;
    }

    public void setChangeAndWaitPrintMark(boolean changeAndWaitPrintMark) {
        this.changeAndWaitPrintMark = changeAndWaitPrintMark;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getDayOrNight() {
        return dayOrNight;
    }

    public void setDayOrNight(String dayOrNight) {
        this.dayOrNight = dayOrNight;
    }

    public boolean isFullAttended() {
        return fullAttended;
    }

    public void setFullAttended(boolean fullAttended) {
        this.fullAttended = fullAttended;
    }

    public boolean isGraduated() {
        return graduated;
    }

    public void setGraduated(boolean graduated) {
        this.graduated = graduated;
    }

    public boolean isHomeClass() {
        return homeClass;
    }

    public void setHomeClass(boolean homeClass) {
        this.homeClass = homeClass;
    }

    public String getHomeClassNote() {
        return homeClassNote;
    }

    public void setHomeClassNote(String homeClassNote) {
        this.homeClassNote = homeClassNote;
    }

    public CtMemberInfo getMemberInfo() {
        return memberInfo;
    }

    public void setMemberInfo(CtMemberInfo memberInfo) {
        this.memberInfo = memberInfo;
    }
}
