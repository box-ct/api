package tw.org.ctworld.meditation.beans.Makeup;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.ClassDateInfo;

import java.util.List;

public class ClassDateInfoResponse extends BaseResponse {
    private List<ClassDateInfo> item;

    public ClassDateInfoResponse(int errCode, String errMsg, List<ClassDateInfo> item) {
        super(errCode, errMsg);
        this.item = item;
    }

    public List<ClassDateInfo> getItem() {
        return item;
    }

    public void setItem(List<ClassDateInfo> item) {
        this.item = item;
    }
}
