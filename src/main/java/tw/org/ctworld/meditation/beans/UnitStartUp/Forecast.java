package tw.org.ctworld.meditation.beans.UnitStartUp;

public class Forecast {
    private Integer forecast;
    private String eventId;

    public Forecast(Integer forecast, String eventId) {
        this.forecast = forecast;
        this.eventId = eventId;
    }

    public Forecast(long forecast, String eventId) {
        this.forecast = (int)forecast;
        this.eventId = eventId;
    }

    public Integer getForecast() {
        return forecast;
    }

    public void setForecast(Integer forecast) {
        this.forecast = forecast;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
