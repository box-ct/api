package tw.org.ctworld.meditation.beans.Printer;

import java.util.List;

public class Printer001Object {

    private long id;
    private String printerName, printerId, note;
    private String certTypeName;
    private List<String> certTypeNames;

    public Printer001Object(long id, String printerName, String printerId, String note) {
        this.id = id;
        this.printerName = printerName;
        this.printerId = printerId;
        this.note = note;
    }

    public Printer001Object(long id, String printerName, String printerId, String note, List<String> certTypeNames) {
        this.id = id;
        this.printerName = printerName;
        this.printerId = printerId;
        this.note = note;
        this.certTypeNames = certTypeNames;
    }

    public Printer001Object(String printerId, String certTypeName) {
        this.printerId = printerId;
        this.certTypeName = certTypeName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrinterName() {
        return printerName;
    }

    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    public String getPrinterId() {
        return printerId;
    }

    public void setPrinterId(String printerId) {
        this.printerId = printerId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getCertTypeNames() {
        return certTypeNames;
    }

    public void setCertTypeNames(List<String> certTypeNames) {
        this.certTypeNames = certTypeNames;
    }

    public String getCertTypeName() {
        return certTypeName;
    }

    public void setCertTypeName(String certTypeName) {
        this.certTypeName = certTypeName;
    }
}
