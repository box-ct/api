package tw.org.ctworld.meditation.beans.Member;

import java.util.List;

public class Members005Request {

    private Members0051Object prev;
    private Members0052Object next;

    public Members0051Object getPrev() {
        return prev;
    }

    public void setPrev(Members0051Object prev) {
        this.prev = prev;
    }

    public Members0052Object getNext() {
        return next;
    }

    public void setNext(Members0052Object next) {
        this.next = next;
    }

    public class Members0051Object {

        private String classId;

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }
    }

    public class Members0052Object {

        private String classId, classGroupId;

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(String classGroupId) {
            this.classGroupId = classGroupId;
        }
    }
}
