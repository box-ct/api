package tw.org.ctworld.meditation.beans.Member;

import javax.persistence.Column;
import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;

public class MembersObject {
    private String memberId, gender,aliasName, ctDharmaName, classPeriodNum, className, classGroupId, homePhoneNum1,
            mobileNum1, dsaJobNameList, memberGroupNum, homeClassNote,
            currentClassIntroducerName, currentClassIntroducerRelationship, twIdNum, asistantMasterName, dayOfWeek,
            classJobTitleList, eventId, eventName, eventStartDate, idType1, idType2, classId, mailingCountry,
            mailingState, mailingCity, mailingStreet, companyJobTitle, schoolName, schoolMajor, birthDate;

    private int age;
    private boolean isDroppedClass, isHomeClass, isNew, notPhoto, notTwId, notPID, notPConsent;

    public MembersObject(
            String memberId,
            String gender,
            String aliasName,
            String ctDharmaName,
            String classPeriodNum,
            String className,
            Integer age,
            String classGroupId,
            String homePhoneNum1,
            String mobileNum1,
            String dsaJobNameList,
            Boolean isDroppedClass) {
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.homePhoneNum1 = homePhoneNum1;
        this.mobileNum1 = mobileNum1;
        this.dsaJobNameList = dsaJobNameList;
        this.age = age != null ? age : 0;
        this.isDroppedClass = isDroppedClass != null ? isDroppedClass : false;
    }


    public MembersObject(String memberId,
                         String gender,
                         String aliasName,
                         String ctDharmaName,
                         String classPeriodNum,
                         String className,
                         String classGroupId,
                         String memberGroupNum,
                         Boolean homeClass,
                         String homeClassNote,
                         String classId) {

        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.isHomeClass = homeClass != null ? homeClass : false;
        this.homeClassNote = homeClassNote;
        this.classId = classId;
    }

    public MembersObject(String memberId,
                         String classPeriodNum,
                         String className,
                         String classGroupId,
                         String memberGroupNum,
                         String gender,
                         String aliasName,
                         String ctDharmaName,
                         Boolean droppedClass,
                         String twIdNum,
                         String currentClassIntroducerName,
                         String currentClassIntroducerRelationship,
                         Integer age) {

        this.memberId = memberId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.isDroppedClass = droppedClass != null ? droppedClass : false;
        this.twIdNum = twIdNum;
        this.currentClassIntroducerName = currentClassIntroducerName;
        this.currentClassIntroducerRelationship = currentClassIntroducerRelationship;
        this.age = age != null ? age : 0;

        this.setNew(false);
        this.setNotPhoto(true);
        this.setNotTwId(true);
        this.setNotPID(true);
        this.setNotPConsent(true);
    }

    public MembersObject(
            String classPeriodNum,
            String className,
            String classGroupId,
            String memberGroupNum,
            String gender,
            String aliasName,
            String ctDharmaName,
            String classJobTitleList,
            String homePhoneNum1,
            String mobileNum1,
            String asistantMasterName,
            String dayOfWeek) {
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classJobTitleList = classJobTitleList;
        this.homePhoneNum1 = homePhoneNum1;
        this.mobileNum1 = mobileNum1;
        this.asistantMasterName = asistantMasterName;
        this.dayOfWeek = dayOfWeek;
    }

    public MembersObject(
            String memberId,
            String gender,
            String classJobTitleList) {
        this.memberId = memberId;
        this.gender = gender;
        this.classJobTitleList = classJobTitleList;
        if (this.classJobTitleList == null) {
            this.classJobTitleList = "";
        }
    }

    public MembersObject(
            String memberId,
            int classPeriodNum,
            String className,
            String gender,
            int classGroupId,
            String memberGroupNum,
            String aliasName,
            String ctDharmaName,
            String eventId,
            String eventName,
            String eventStartDate,
            String idType1,
            String idType2
    ) {
        this.memberId = memberId;
        this.classPeriodNum = String.valueOf(classPeriodNum);
        this.className = className;
        this.gender = gender;
        this.classGroupId = String.valueOf(classGroupId);
        this.memberGroupNum = memberGroupNum;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventStartDate = eventStartDate;
        this.idType1 = idType1;
        this.idType2 = idType2;
    }

    public MembersObject(
            String memberId,
            String gender,
            String aliasName,
            String ctDharmaName,
            String homePhoneNum1,
            String mobileNum1,
            String dsaJobNameList,
            String twIdNum,
            String mailingCountry,
            String mailingState,
            String mailingCity,
            String mailingStreet,
            String companyJobTitle,
            String schoolName,
            String schoolMajor,
            Date birthDate,
            Integer age) {
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.homePhoneNum1 = homePhoneNum1;
        this.mobileNum1 = mobileNum1;
        this.dsaJobNameList = dsaJobNameList;
        this.twIdNum = twIdNum;
        this.mailingCountry = mailingCountry;
        this.mailingState = mailingState;
        this.mailingCity = mailingCity;
        this.mailingStreet = mailingStreet;
        this.companyJobTitle = companyJobTitle;
        this.schoolName = schoolName;
        this.schoolMajor = schoolMajor;
        this.birthDate = DateTime2DateString(birthDate);
        this.age = age != null ? age : 0;
    }

    public MembersObject() {
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public boolean getIsDroppedClass() {
        return isDroppedClass;
    }

    public void setIsDroppedClass(boolean droppedClass) {
        isDroppedClass = droppedClass;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public String getHomeClassNote() {
        return homeClassNote;
    }

    public void setHomeClassNote(String homeClassNote) {
        this.homeClassNote = homeClassNote;
    }

    public Boolean getHomeClass() {
        return isHomeClass;
    }

    public void setHomeClass(Boolean homeClass) {
        isHomeClass = homeClass;
    }

    public String getCurrentClassIntroducerName() {
        return currentClassIntroducerName;
    }

    public void setCurrentClassIntroducerName(String currentClassIntroducerName) {
        this.currentClassIntroducerName = currentClassIntroducerName;
    }

    public String getCurrentClassIntroducerRelationship() {
        return currentClassIntroducerRelationship;
    }

    public void setCurrentClassIntroducerRelationship(String currentClassIntroducerRelationship) {
        this.currentClassIntroducerRelationship = currentClassIntroducerRelationship;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public Boolean getNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }

    public boolean isNotPhoto() {
        return notPhoto;
    }

    public void setNotPhoto(boolean notPhoto) {
        this.notPhoto = notPhoto;
    }

    public boolean isNotTwId() {
        return notTwId;
    }

    public void setNotTwId(boolean notTwId) {
        this.notTwId = notTwId;
    }

    public boolean isNotPID() {
        return notPID;
    }

    public void setNotPID(boolean notPID) {
        this.notPID = notPID;
    }

    public boolean isNotPConsent() {
        return notPConsent;
    }

    public void setNotPConsent(boolean notPConsent) {
        this.notPConsent = notPConsent;
    }

    public String getAsistantMasterName() {
        return asistantMasterName;
    }

    public void setAsistantMasterName(String asistantMasterName) {
        this.asistantMasterName = asistantMasterName;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getIdType1() {
        return idType1;
    }

    public void setIdType1(String idType1) {
        this.idType1 = idType1;
    }

    public String getIdType2() {
        return idType2;
    }

    public void setIdType2(String idType2) {
        this.idType2 = idType2;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getMailingCountry() {
        return mailingCountry;
    }

    public void setMailingCountry(String mailingCountry) {
        this.mailingCountry = mailingCountry;
    }

    public String getMailingState() {
        return mailingState;
    }

    public void setMailingState(String mailingState) {
        this.mailingState = mailingState;
    }

    public String getMailingCity() {
        return mailingCity;
    }

    public void setMailingCity(String mailingCity) {
        this.mailingCity = mailingCity;
    }

    public String getMailingStreet() {
        return mailingStreet;
    }

    public void setMailingStreet(String mailingStreet) {
        this.mailingStreet = mailingStreet;
    }

    public String getCompanyJobTitle() {
        return companyJobTitle;
    }

    public void setCompanyJobTitle(String companyJobTitle) {
        this.companyJobTitle = companyJobTitle;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolMajor() {
        return schoolMajor;
    }

    public void setSchoolMajor(String schoolMajor) {
        this.schoolMajor = schoolMajor;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}

