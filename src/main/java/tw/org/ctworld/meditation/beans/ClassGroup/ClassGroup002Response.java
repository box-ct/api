package tw.org.ctworld.meditation.beans.ClassGroup;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class ClassGroup002Response extends BaseResponse {

    private List<ClassGroup002Object> items;

    public ClassGroup002Response(int errCode, String errMsg, List<ClassGroup002Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassGroup002Object> getItems() {
        return items;
    }

    public void setItems(List<ClassGroup002Object> items) {
        this.items = items;
    }
}
