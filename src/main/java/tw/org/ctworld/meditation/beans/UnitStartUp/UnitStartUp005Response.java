package tw.org.ctworld.meditation.beans.UnitStartUp;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class UnitStartUp005Response extends BaseResponse {

    private List<UnitStartUp005Object> items;

    public UnitStartUp005Response(int errCode, String errMsg, List<UnitStartUp005Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<UnitStartUp005Object> getItems() {
        return items;
    }

    public void setItems(List<UnitStartUp005Object> items) {
        this.items = items;
    }
}
