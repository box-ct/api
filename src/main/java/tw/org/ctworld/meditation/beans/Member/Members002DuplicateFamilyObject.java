package tw.org.ctworld.meditation.beans.Member;

public class Members002DuplicateFamilyObject {

    private String memberId, aliasName, ctDharmaName, gender, birthDate, familyLeaderName, familyLeaderMemberId,
            familyLeaderRelationship, homePhoneNum1, mobileNum1;

    public Members002DuplicateFamilyObject(String memberId, String aliasName, String ctDharmaName, String gender,
                                           String birthDate, String familyLeaderName, String familyLeaderMemberId,
                                           String familyLeaderRelationship, String homePhoneNum1, String mobileNum1) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.familyLeaderName = familyLeaderName;
        this.familyLeaderMemberId = familyLeaderMemberId;
        this.familyLeaderRelationship = familyLeaderRelationship;
        this.homePhoneNum1 = homePhoneNum1;
        this.mobileNum1 = mobileNum1;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getFamilyLeaderName() {
        return familyLeaderName;
    }

    public void setFamilyLeaderName(String familyLeaderName) {
        this.familyLeaderName = familyLeaderName;
    }

    public String getFamilyLeaderMemberId() {
        return familyLeaderMemberId;
    }

    public void setFamilyLeaderMemberId(String familyLeaderMemberId) {
        this.familyLeaderMemberId = familyLeaderMemberId;
    }

    public String getFamilyLeaderRelationship() {
        return familyLeaderRelationship;
    }

    public void setFamilyLeaderRelationship(String familyLeaderRelationship) {
        this.familyLeaderRelationship = familyLeaderRelationship;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }
}
