package tw.org.ctworld.meditation.beans.UnitMaster;

import org.springframework.beans.factory.annotation.Autowired;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.UnitMasterInfo;
import tw.org.ctworld.meditation.models.UnitInfo;

import java.util.List;

public class UnitMaster002Response extends BaseResponse {

    @Autowired
    private UnitInfo unitInfo;
    private UnitMaster002Object data;

    public UnitMaster002Response(int errCode, String errMsg,String unitName,String certUnitName,String unitId,String fundUnitId,String certUnitEngName,String unitPhoneNum,String unitFaxNum,                                String timeZone,String unitPhoneNumCode,String unitMobileNum,String unitEmail,String unitMailingAddress,String stateName,String abbotId,String abbotName,
                                 String abbotEngName,Boolean isOverseasUnit,String district,String groupName , List<UnitMasterInfo> unitMasterInfos){
        super(errCode, errMsg);
        this.data=new UnitMaster002Object(unitName,certUnitName,unitId,fundUnitId,certUnitEngName,unitPhoneNum,unitFaxNum,
                timeZone,unitPhoneNumCode,unitMobileNum,unitEmail,unitMailingAddress,stateName,abbotId,abbotName,abbotEngName,isOverseasUnit,district,groupName,unitMasterInfos);
    }

    public UnitMaster002Object getData() {
        return data;
    }

    public void setData(UnitMaster002Object data) {
        this.data = data;
    }
}
