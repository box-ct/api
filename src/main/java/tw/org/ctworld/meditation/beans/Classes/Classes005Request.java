package tw.org.ctworld.meditation.beans.Classes;

import java.util.List;

public class Classes005Request {

    private String classStatus,
            classStartDate,
            classEndDate,
            dayOfWeek,
            classDayOrNight,
            classTypeName,
            classPeriodNum,
            className,
            classLanguage,
            teachMasterName,
            assistantMasterName,
            teachMemberName,
            classPlace,
            classFullName,
            certificateName,
            classStartTime,
            classEndTime,
            classDesc,
            classNote,
            abbotName,
            abbotEngName,
            certificateEngName,
            stateName,
            certUnitName,
            certUnitEngName;

    private int classTypeNum,
            absenceCountToFullAttend,
            absenceCountToGraduate,
            makeupCountToGraduate,
            absenceCountToDiligentAward,
            makeupCountToDiligentAward,
            minutesBeLate,
            minMakeupCountToDiligentAward,
            maxMakeupCountToDiligentAward;

    private List<Classes005Object> classDates;


    public String getClassStatus() {
        return classStatus;
    }

    public void setClassStatus(String classStatus) {
        this.classStatus = classStatus;
    }

    public String getClassStartDate() {
        return classStartDate;
    }

    public void setClassStartDate(String classStartDate) {
        this.classStartDate = classStartDate;
    }

    public String getClassEndDate() {
        return classEndDate;
    }

    public void setClassEndDate(String classEndDate) {
        this.classEndDate = classEndDate;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getClassDayOrNight() {
        return classDayOrNight;
    }

    public void setClassDayOrNight(String classDayOrNight) {
        this.classDayOrNight = classDayOrNight;
    }

    public int getClassTypeNum() {
        return classTypeNum;
    }

    public void setClassTypeNum(int classTypeNum) {
        this.classTypeNum = classTypeNum;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassLanguage() {
        return classLanguage;
    }

    public void setClassLanguage(String classLanguage) {
        this.classLanguage = classLanguage;
    }

    public String getTeachMasterName() {
        return teachMasterName;
    }

    public void setTeachMasterName(String teachMasterName) {
        this.teachMasterName = teachMasterName;
    }

    public String getAssistantMasterName() {
        return assistantMasterName;
    }

    public void setAssistantMasterName(String assistantMasterName) {
        this.assistantMasterName = assistantMasterName;
    }

    public String getTeachMemberName() {
        return teachMemberName;
    }

    public void setTeachMemberName(String teachMemberName) {
        this.teachMemberName = teachMemberName;
    }

    public String getClassPlace() {
        return classPlace;
    }

    public void setClassPlace(String classPlace) {
        this.classPlace = classPlace;
    }

    public int getAbsenceCountToFullAttend() {
        return absenceCountToFullAttend;
    }

    public void setAbsenceCountToFullAttend(int absenceCountToFullAttend) {
        this.absenceCountToFullAttend = absenceCountToFullAttend;
    }

    public int getAbsenceCountToGraduate() {
        return absenceCountToGraduate;
    }

    public void setAbsenceCountToGraduate(int absenceCountToGraduate) {
        this.absenceCountToGraduate = absenceCountToGraduate;
    }

    public int getMakeupCountToGraduate() {
        return makeupCountToGraduate;
    }

    public void setMakeupCountToGraduate(int makeupCountToGraduate) {
        this.makeupCountToGraduate = makeupCountToGraduate;
    }

    public int getAbsenceCountToDiligentAward() {
        return absenceCountToDiligentAward;
    }

    public void setAbsenceCountToDiligentAward(int absenceCountToDiligentAward) {
        this.absenceCountToDiligentAward = absenceCountToDiligentAward;
    }

    public int getMakeupCountToDiligentAward() {
        return makeupCountToDiligentAward;
    }

    public void setMakeupCountToDiligentAward(int makeupCountToDiligentAward) {
        this.makeupCountToDiligentAward = makeupCountToDiligentAward;
    }

    public int getMinutesBeLate() {
        return minutesBeLate;
    }

    public void setMinutesBeLate(int minutesBeLate) {
        this.minutesBeLate = minutesBeLate;
    }

    public String getClassFullName() {
        return classFullName;
    }

    public void setClassFullName(String classFullName) {
        this.classFullName = classFullName;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public String getClassStartTime() {
        return classStartTime;
    }

    public void setClassStartTime(String classStartTime) {
        this.classStartTime = classStartTime;
    }

    public String getClassEndTime() {
        return classEndTime;
    }

    public void setClassEndTime(String classEndTime) {
        this.classEndTime = classEndTime;
    }

    public String getClassDesc() {
        return classDesc;
    }

    public void setClassDesc(String classDesc) {
        this.classDesc = classDesc;
    }

    public String getClassNote() {
        return classNote;
    }

    public void setClassNote(String classNote) {
        this.classNote = classNote;
    }

    public List<Classes005Object> getClassDates() {
        return classDates;
    }

    public void setClassDates(List<Classes005Object> classDates) {
        this.classDates = classDates;
    }

    public String getAbbotName() {
        return abbotName;
    }

    public void setAbbotName(String abbotName) {
        this.abbotName = abbotName;
    }

    public String getAbbotEngName() {
        return abbotEngName;
    }

    public void setAbbotEngName(String abbotEngName) {
        this.abbotEngName = abbotEngName;
    }

    public String getCertificateEngName() {
        return certificateEngName;
    }

    public void setCertificateEngName(String certificateEngName) {
        this.certificateEngName = certificateEngName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public int getMinMakeupCountToDiligentAward() {
        return minMakeupCountToDiligentAward;
    }

    public void setMinMakeupCountToDiligentAward(int minMakeupCountToDiligentAward) {
        this.minMakeupCountToDiligentAward = minMakeupCountToDiligentAward;
    }

    public int getMaxMakeupCountToDiligentAward() {
        return maxMakeupCountToDiligentAward;
    }

    public void setMaxMakeupCountToDiligentAward(int maxMakeupCountToDiligentAward) {
        this.maxMakeupCountToDiligentAward = maxMakeupCountToDiligentAward;
    }

    public String getCertUnitName() {
        return certUnitName;
    }

    public void setCertUnitName(String certUnitName) {
        this.certUnitName = certUnitName;
    }

    public String getCertUnitEngName() {
        return certUnitEngName;
    }

    public void setCertUnitEngName(String certUnitEngName) {
        this.certUnitEngName = certUnitEngName;
    }
}
