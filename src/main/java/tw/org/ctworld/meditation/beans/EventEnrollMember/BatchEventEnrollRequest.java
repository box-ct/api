package tw.org.ctworld.meditation.beans.EventEnrollMember;

import tw.org.ctworld.meditation.beans.EventEnrollMaster.EnrollMaster;

import java.util.List;

public class BatchEventEnrollRequest {
    private List<String> memberIds;
    private List<EnrollMaster> masterList;
    private List<String> formAutoAddNums;
    private EventEnrollRequest enrollData;
    private List<ClassEnrollFormShort> homeClassIds;

    public List<String> getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(List<String> memberIds) {
        this.memberIds = memberIds;
    }

    public List<EnrollMaster> getMasterList() {
        return masterList;
    }

    public void setMasterList(List<EnrollMaster> masterList) {
        this.masterList = masterList;
    }

    public List<String> getFormAutoAddNums() {
        return formAutoAddNums;
    }

    public void setFormAutoAddNums(List<String> formAutoAddNums) {
        this.formAutoAddNums = formAutoAddNums;
    }

    public EventEnrollRequest getEnrollData() {
        return enrollData;
    }

    public void setEnrollData(EventEnrollRequest enrollData) {
        this.enrollData = enrollData;
    }

    public List<ClassEnrollFormShort> getHomeClassIds() {
        return homeClassIds;
    }

    public void setHomeClassIds(List<ClassEnrollFormShort> homeClassIds) {
        this.homeClassIds = homeClassIds;
    }
}
