package tw.org.ctworld.meditation.beans.Member;

public class Members008Object {

    private String classStatus, className, classPeriodNum, classId;

    public Members008Object(String classStatus, String className, int classTypeNum, String classPeriodNum, String classId) {
        this.classStatus = classStatus;
        this.className = className;
        this.classPeriodNum = classPeriodNum;
        this.classId = classId;
    }

    public Members008Object(String classStatus, String className, String classPeriodNum, String classId) {
        this.classStatus = classStatus;
        this.className = className;
        this.classPeriodNum = classPeriodNum;
        this.classId = classId;
    }

    public String getClassStatus() {
        return classStatus;
    }

    public void setClassStatus(String classStatus) {
        this.classStatus = classStatus;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }
}
