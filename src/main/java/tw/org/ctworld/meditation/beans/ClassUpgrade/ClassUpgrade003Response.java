package tw.org.ctworld.meditation.beans.ClassUpgrade;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class ClassUpgrade003Response extends BaseResponse {

    private List<ClassUpgrade003Object> items;

    public ClassUpgrade003Response(int errCode, String errMsg, List<ClassUpgrade003Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassUpgrade003Object> getItems() {
        return items;
    }

    public void setItems(List<ClassUpgrade003Object> items) {
        this.items = items;
    }


    public static class ClassUpgrade003Object {

        private String classEnrollFormId, memberId, gender, aliasName, ctDharmaName, classPeriodNum, className, classGroupId, memberGroupNum,
                classJobTitleList;
        private boolean isNotComeBack, isCompleteNextNewClassEnroll, isEnrollToNewUpperClass, isFullAttended, isGraduated;
        private String previousClassName, previousClassGroupName, nextNewClassName, nextNewClassNote;
        private boolean isAttend;
        private String classJobContentList;
        private boolean isLeadGroup;

        public ClassUpgrade003Object(String classEnrollFormId, String memberId, String gender, String aliasName,
                                     String ctDharmaName, String classPeriodNum, String className, String classGroupId,
                                     String memberGroupNum, String classJobTitleList, boolean isNotComeBack,
                                     boolean isCompleteNextNewClassEnroll, boolean isEnrollToNewUpperClass,
                                     boolean isFullAttended, boolean isGraduated, String previousClassName,
                                     String previousClassGroupName, String nextNewClassName, String nextNewClassNote,
                                     boolean isAttend, String classJobContentList, boolean isLeadGroup) {
            this.classEnrollFormId = classEnrollFormId;
            this.memberId = memberId;
            this.gender = gender;
            this.aliasName = aliasName;
            this.ctDharmaName = ctDharmaName;
            this.classPeriodNum = classPeriodNum;
            this.className = className;
            this.classGroupId = classGroupId;
            this.memberGroupNum = memberGroupNum;
            this.classJobTitleList = classJobTitleList;
            this.isNotComeBack = isNotComeBack;
            this.isCompleteNextNewClassEnroll = isCompleteNextNewClassEnroll;
            this.isEnrollToNewUpperClass = isEnrollToNewUpperClass;
            this.isFullAttended = isFullAttended;
            this.isGraduated = isGraduated;
            this.previousClassName = previousClassName;
            this.previousClassGroupName = previousClassGroupName;
            this.nextNewClassName = nextNewClassName;
            this.nextNewClassNote = nextNewClassNote;
            this.isAttend = isAttend;
            this.classJobContentList = classJobContentList;
            this.isLeadGroup = isLeadGroup;
        }

        public String getClassEnrollFormId() {
            return classEnrollFormId;
        }

        public void setClassEnrollFormId(String classEnrollFormId) {
            this.classEnrollFormId = classEnrollFormId;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAliasName() {
            return aliasName;
        }

        public void setAliasName(String aliasName) {
            this.aliasName = aliasName;
        }

        public String getCtDharmaName() {
            return ctDharmaName;
        }

        public void setCtDharmaName(String ctDharmaName) {
            this.ctDharmaName = ctDharmaName;
        }

        public String getClassPeriodNum() {
            return classPeriodNum;
        }

        public void setClassPeriodNum(String classPeriodNum) {
            this.classPeriodNum = classPeriodNum;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(String classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getMemberGroupNum() {
            return memberGroupNum;
        }

        public void setMemberGroupNum(String memberGroupNum) {
            this.memberGroupNum = memberGroupNum;
        }

        public String getClassJobTitleList() {
            return classJobTitleList;
        }

        public void setClassJobTitleList(String classJobTitleList) {
            this.classJobTitleList = classJobTitleList;
        }

        public boolean getIsNotComeBack() {
            return isNotComeBack;
        }

        public void setIsNotComeBack(boolean notComeBack) {
            isNotComeBack = notComeBack;
        }

        public boolean getIsCompleteNextNewClassEnroll() {
            return isCompleteNextNewClassEnroll;
        }

        public void setIsCompleteNextNewClassEnroll(boolean completeNextNewClassEnroll) {
            isCompleteNextNewClassEnroll = completeNextNewClassEnroll;
        }

        public boolean getIsEnrollToNewUpperClass() {
            return isEnrollToNewUpperClass;
        }

        public void setIsEnrollToNewUpperClass(boolean enrollToNewUpperClass) {
            isEnrollToNewUpperClass = enrollToNewUpperClass;
        }

        public boolean getIsFullAttended() {
            return isFullAttended;
        }

        public void setIsFullAttended(boolean fullAttended) {
            isFullAttended = fullAttended;
        }

        public boolean getIsGraduated() {
            return isGraduated;
        }

        public void setIsGraduated(boolean graduated) {
            isGraduated = graduated;
        }

        public String getPreviousClassName() {
            return previousClassName;
        }

        public void setPreviousClassName(String previousClassName) {
            this.previousClassName = previousClassName;
        }

        public String getPreviousClassGroupName() {
            return previousClassGroupName;
        }

        public void setPreviousClassGroupName(String previousClassGroupName) {
            this.previousClassGroupName = previousClassGroupName;
        }

        public String getNextNewClassName() {
            return nextNewClassName;
        }

        public void setNextNewClassName(String nextNewClassName) {
            this.nextNewClassName = nextNewClassName;
        }

        public String getNextNewClassNote() {
            return nextNewClassNote;
        }

        public void setNextNewClassNote(String nextNewClassNote) {
            this.nextNewClassNote = nextNewClassNote;
        }

        public boolean getIsAttend() {
            return isAttend;
        }

        public void setIsAttend(boolean attend) {
            isAttend = attend;
        }

        public String getClassJobContentList() {
            return classJobContentList;
        }

        public void setClassJobContentList(String classJobContentList) {
            this.classJobContentList = classJobContentList;
        }

        public boolean getIsLeadGroup() {
            return isLeadGroup;
        }

        public void setIsLeadGroup(boolean leadGroup) {
            isLeadGroup = leadGroup;
        }
    }
}
