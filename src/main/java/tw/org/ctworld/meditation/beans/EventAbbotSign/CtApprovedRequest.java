package tw.org.ctworld.meditation.beans.EventAbbotSign;

import java.util.List;

public class CtApprovedRequest {
    private List<String> formAutoAddNums;

    private boolean isCtApproved;
    private String ctApprovedNote;

    public List<String> getFormAutoAddNums() {
        return formAutoAddNums;
    }

    public void setFormAutoAddNums(List<String> formAutoAddNums) {
        this.formAutoAddNums = formAutoAddNums;
    }

    public boolean getIsCtApproved() {
        return isCtApproved;
    }

    public void setIsCtApproved(boolean ctApproved) {
        isCtApproved = ctApproved;
    }

    public String getCtApprovedNote() {
        return ctApprovedNote;
    }

    public void setCtApprovedNote(String ctApprovedNote) {
        this.ctApprovedNote = ctApprovedNote;
    }
}
