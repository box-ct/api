package tw.org.ctworld.meditation.beans.Common;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Common012Response extends BaseResponse {

    private List<Common012_1Object> items;

    public Common012Response(int errCode, String errMsg, List<Common012_1Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Common012_1Object> getItems() {
        return items;
    }

    public void setItems(List<Common012_1Object> items) {
        this.items = items;
    }
}
