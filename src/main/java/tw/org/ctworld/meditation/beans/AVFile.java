package tw.org.ctworld.meditation.beans;

public class AVFile {
    private String filename;
    private long size;
    private String sizeStr;
    private long lastModified;
    private String lastModifiedStr;


    public AVFile() {
    }

    public AVFile(String filename, long size, long lastModified) {
        this.filename = filename;
        this.size = size;
        this.lastModified = lastModified;
    }

    public AVFile(String filename, long size, long lastModified, String sizeStr, String lastModifiedStr) {
        this.filename = filename;
        this.size = size;
        this.lastModified = lastModified;
        this.lastModifiedStr = lastModifiedStr;
        this.sizeStr = sizeStr;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getSizeStr() {
        return sizeStr;
    }

    public void setSizeStr(String sizeStr) {
        this.sizeStr = sizeStr;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public String getLastModifiedStr() {
        return lastModifiedStr;
    }

    public void setLastModifiedStr(String lastModifiedStr) {
        this.lastModifiedStr = lastModifiedStr;
    }
}
