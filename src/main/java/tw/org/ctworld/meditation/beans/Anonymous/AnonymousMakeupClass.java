package tw.org.ctworld.meditation.beans.Anonymous;

import java.util.List;

public class AnonymousMakeupClass {

    private String classId, className, dayOfWeek;
    private List<AnonymousMakeupClassDate> classDates;
    private boolean isAttend;
    private int attendRate, absentCount;

    public AnonymousMakeupClass(String classId, String className, String dayOfWeek) {
        this.classId = classId;
        this.className = className;
        this.dayOfWeek = dayOfWeek;

        this.isAttend = false;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public List<AnonymousMakeupClassDate> getClassDates() {
        return classDates;
    }

    public void setClassDates(List<AnonymousMakeupClassDate> classDates) {
        this.classDates = classDates;
    }

    public boolean isAttend() {
        return isAttend;
    }

    public void setAttend(boolean attend) {
        isAttend = attend;
    }

    public int getAttendRate() {
        return attendRate;
    }

    public void setAttendRate(int attendRate) {
        this.attendRate = attendRate;
    }

    public int getAbsentCount() {
        return absentCount;
    }

    public void setAbsentCount(int absentCount) {
        this.absentCount = absentCount;
    }
}
