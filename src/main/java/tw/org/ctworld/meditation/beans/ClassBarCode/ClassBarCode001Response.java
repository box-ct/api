package tw.org.ctworld.meditation.beans.ClassBarCode;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.Date;
import java.util.List;

public class ClassBarCode001Response extends BaseResponse{

    private List<ClassBarCode001Response.ClassBarCode001Object> items;

    public ClassBarCode001Response(int errCode, String errMsg, List<ClassBarCode001Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassBarCode001Object> getItems() {
        return items;
    }

    public void setItems(List<ClassBarCode001Object> items) {
        this.items = items;
    }

    public static class ClassBarCode001Object {

        private String id, memberId, gender, aliasName, ctDharmaName, classPeriodNum, className, classGroupId,
                memberGroupNum;
        private int classWeeksNum;
        private String attendCheckinDate, attendCheckinTime, attendMark;

        public ClassBarCode001Object(String id, String memberId, String gender, String aliasName, String ctDharmaName,
                                     String classPeriodNum, String className, String classGroupId,
                                     String memberGroupNum, int classWeeksNum, String attendCheckinDate,
                                     String attendCheckinTime, String attendMark) {
            this.id = id;
            this.memberId = memberId;
            this.gender = gender;
            this.aliasName = aliasName;
            this.ctDharmaName = ctDharmaName;
            this.classPeriodNum = classPeriodNum;
            this.className = className;
            this.classGroupId = classGroupId;
            this.memberGroupNum = memberGroupNum;
            this.classWeeksNum = classWeeksNum;
            this.attendCheckinDate = attendCheckinDate;
            this.attendCheckinTime = attendCheckinTime;
            this.attendMark = attendMark;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAliasName() {
            return aliasName;
        }

        public void setAliasName(String aliasName) {
            this.aliasName = aliasName;
        }

        public String getCtDharmaName() {
            return ctDharmaName;
        }

        public void setCtDharmaName(String ctDharmaName) {
            this.ctDharmaName = ctDharmaName;
        }

        public String getClassPeriodNum() {
            return classPeriodNum;
        }

        public void setClassPeriodNum(String classPeriodNum) {
            this.classPeriodNum = classPeriodNum;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(String classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getMemberGroupNum() {
            return memberGroupNum;
        }

        public void setMemberGroupNum(String memberGroupNum) {
            this.memberGroupNum = memberGroupNum;
        }

        public int getClassWeeksNum() {
            return classWeeksNum;
        }

        public void setClassWeeksNum(int classWeeksNum) {
            this.classWeeksNum = classWeeksNum;
        }

        public String getAttendCheckinDate() {
            return attendCheckinDate;
        }

        public void setAttendCheckinDate(String attendCheckinDate) {
            this.attendCheckinDate = attendCheckinDate;
        }

        public String getAttendCheckinTime() {
            return attendCheckinTime;
        }

        public void setAttendCheckinTime(String attendCheckinTime) {
            this.attendCheckinTime = attendCheckinTime;
        }

        public String getAttendMark() {
            return attendMark;
        }

        public void setAttendMark(String attendMark) {
            this.attendMark = attendMark;
        }
    }
}
