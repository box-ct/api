package tw.org.ctworld.meditation.beans.ClassUpgrade;

import java.util.List;

public class ClassUpgrade008Request {

    private List<String> memberIdList;

    public List<String> getMemberIdList() {
        return memberIdList;
    }

    public void setMemberIdList(List<String> memberIdList) {
        this.memberIdList = memberIdList;
    }
}
