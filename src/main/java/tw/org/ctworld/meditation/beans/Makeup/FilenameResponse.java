package tw.org.ctworld.meditation.beans.Makeup;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class FilenameResponse extends BaseResponse {
    private String filename;

    public FilenameResponse(int errCode, String errMsg, String filename) {
        super(errCode, errMsg);
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
