package tw.org.ctworld.meditation.beans.BatchRegister;

public class BatchRegister001_1Object {

    private String memberId, classId, classGroupId;
    private boolean success;

    public BatchRegister001_1Object(String memberId, String classId, String classGroupId, boolean success) {
        this.memberId = memberId;
        this.classId = classId;
        this.classGroupId = classGroupId;
        this.success = success;
    }

    public BatchRegister001_1Object(String memberId, boolean success) {
        this.memberId = memberId;
        this.success = success;
    }

    public BatchRegister001_1Object() {
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
