package tw.org.ctworld.meditation.beans.DataSearch;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.Member.MembersObject;

import java.util.List;

public class DataSearchEventEnrollResponse extends BaseResponse {

    private List<DataSearchEventEnrollObject> items;

    public DataSearchEventEnrollResponse(int errCode, String errMsg, List<DataSearchEventEnrollObject> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<DataSearchEventEnrollObject> getItems() {
        return items;
    }

    public void setItems(List<DataSearchEventEnrollObject> items) {
        this.items = items;
    }
}
