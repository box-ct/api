package tw.org.ctworld.meditation.beans.Makeup;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class StatusResponse extends BaseResponse {
    private List<String> items;

    public StatusResponse(int errCode, String errMsg, List<String> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
