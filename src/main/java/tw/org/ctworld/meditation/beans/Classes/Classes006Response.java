package tw.org.ctworld.meditation.beans.Classes;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Classes006Response extends BaseResponse {

    private List<Classes006Object> items;

    public Classes006Response(int errCode, String errMsg, List<Classes006Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Classes006Object> getItems() {
        return items;
    }

    public void setItems(List<Classes006Object> items) {
        this.items = items;
    }
}
