package tw.org.ctworld.meditation.beans.Member;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class Members009Response extends BaseResponse {

    private String status;

    public Members009Response(int errCode, String errMsg, String status) {
        super(errCode, errMsg);
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
