package tw.org.ctworld.meditation.beans.ClassUpgrade;

import java.util.List;

public class ClassUpgrade007Request {

    private List<String> memberIdList;
    private String nextClassId;

    public List<String> getMemberIdList() {
        return memberIdList;
    }

    public void setMemberIdList(List<String> memberIdList) {
        this.memberIdList = memberIdList;
    }

    public String getNextClassId() {
        return nextClassId;
    }

    public void setNextClassId(String nextClassId) {
        this.nextClassId = nextClassId;
    }
}
