package tw.org.ctworld.meditation.beans.ClassStats;

public class ClassStats008_1Object {

    private String memberId, classPeriodNum, className, gender, aliasName, ctDharmaName, classGroupId, memberGroupNum;
    private boolean isFullAttended;
    private String classAttendedResult;

    public ClassStats008_1Object(String memberId, String classPeriodNum, String className, String gender,
                                 String aliasName, String ctDharmaName, String classGroupId, String memberGroupNum,
                                 boolean isFullAttended, String classAttendedResult) {
        this.memberId = memberId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.isFullAttended = isFullAttended;
        this.classAttendedResult = classAttendedResult;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public boolean getIsFullAttended() {
        return isFullAttended;
    }

    public void setIsFullAttended(boolean fullAttended) {
        isFullAttended = fullAttended;
    }

    public String getClassAttendedResult() {
        return classAttendedResult;
    }

    public void setClassAttendedResult(String classAttendedResult) {
        this.classAttendedResult = classAttendedResult;
    }
}
