package tw.org.ctworld.meditation.beans.Printer;

public class Printer006Request {

    private String printerId, certTypeName, certTemplateName, description;

    public String getPrinterId() {
        return printerId;
    }

    public void setPrinterId(String printerId) {
        this.printerId = printerId;
    }

    public String getCertTypeName() {
        return certTypeName;
    }

    public void setCertTypeName(String certTypeName) {
        this.certTypeName = certTypeName;
    }

    public String getCertTemplateName() {
        return certTemplateName;
    }

    public void setCertTemplateName(String certTemplateName) {
        this.certTemplateName = certTemplateName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
