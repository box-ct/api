package tw.org.ctworld.meditation.beans.CtEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class CtEventMaintain010Response extends BaseResponse {

    private List<CtEventMaintain010Object> importedItems;

    public CtEventMaintain010Response(int errCode, String errMsg, List<CtEventMaintain010Object> importedItems) {
        super(errCode, errMsg);
        this.importedItems = importedItems;
    }

    public List<CtEventMaintain010Object> getImportedItems() {
        return importedItems;
    }

    public void setImportedItems(List<CtEventMaintain010Object> importedItems) {
        this.importedItems = importedItems;
    }
}
