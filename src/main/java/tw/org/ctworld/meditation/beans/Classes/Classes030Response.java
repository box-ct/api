package tw.org.ctworld.meditation.beans.Classes;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Classes030Response extends BaseResponse {

    private List<Classes030Object> items;

    public Classes030Response(int errCode, String errMsg, List<Classes030Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Classes030Object> getItems() {
        return items;
    }

    public void setItems(List<Classes030Object> items) {
        this.items = items;
    }
}
