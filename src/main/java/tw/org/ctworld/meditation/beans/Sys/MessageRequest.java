package tw.org.ctworld.meditation.beans.Sys;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageRequest {
    private String announceSubject;
    private String announceContent;
    @JsonProperty
    private boolean isEnabled;

    public MessageRequest() {
    }

    public String getAnnounceSubject() {
        return announceSubject;
    }

    public void setAnnounceSubject(String announceSubject) {
        this.announceSubject = announceSubject;
    }

    public String getAnnounceContent() {
        return announceContent;
    }

    public void setAnnounceContent(String announceContent) {
        this.announceContent = announceContent;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }
}
