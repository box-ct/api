package tw.org.ctworld.meditation.beans.EventAbbotSign;

import java.util.List;

public class AbbotSignRequest {
    private List<String> formAutoAddNums;

    private boolean isAbbotSigned;

    public List<String> getFormAutoAddNums() {
        return formAutoAddNums;
    }

    public void setFormAutoAddNums(List<String> formAutoAddNums) {
        this.formAutoAddNums = formAutoAddNums;
    }

    public boolean getIsAbbotSigned() {
        return isAbbotSigned;
    }

    public void setIsAbbotSigned(boolean abbotSigned) {
        isAbbotSigned = abbotSigned;
    }
}
