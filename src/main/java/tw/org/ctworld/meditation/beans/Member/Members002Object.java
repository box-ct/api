package tw.org.ctworld.meditation.beans.Member;

public class Members002Object {

    private String filePathKind, filePathRoot, filePath, fileName;

    public Members002Object(String filePathKind, String filePathRoot, String filePath, String fileName) {
        this.filePathKind = filePathKind;
        this.filePathRoot = filePathRoot;
        this.filePath = filePath;
        this.fileName = fileName;
    }

    public String getFilePathKind() {
        return filePathKind;
    }

    public void setFilePathKind(String filePathKind) {
        this.filePathKind = filePathKind;
    }

    public String getFilePathRoot() {
        return filePathRoot;
    }

    public void setFilePathRoot(String filePathRoot) {
        this.filePathRoot = filePathRoot;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
