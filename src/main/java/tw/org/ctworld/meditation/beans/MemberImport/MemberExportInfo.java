package tw.org.ctworld.meditation.beans.MemberImport;

import java.util.List;

public class MemberExportInfo {
    private String unitId;
    private String unitName;
    private List<MemberTemplateItem> memberTemplateItems;
    private List<String> selectedField;
    public MemberExportInfo(){}

    public MemberExportInfo(String unitId, String unitName, List<MemberTemplateItem> memberTemplateItems, List<String> selectedField) {
        this.unitId = unitId;
        this.unitName = unitName;
        this.memberTemplateItems = memberTemplateItems;
        this.selectedField = selectedField;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public List<MemberTemplateItem> getMemberTemplateItems() {
        return memberTemplateItems;
    }

    public void setMemberTemplateItems(List<MemberTemplateItem> memberTemplateItems) {
        this.memberTemplateItems = memberTemplateItems;
    }

    public List<String> getSelectedField() {
        return selectedField;
    }

    public void setSelectedField(List<String> selectedField) {
        this.selectedField = selectedField;
    }
}
