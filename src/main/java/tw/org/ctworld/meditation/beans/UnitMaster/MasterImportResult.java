package tw.org.ctworld.meditation.beans.UnitMaster;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class MasterImportResult extends BaseResponse {
    private List<MasterInfo> successed;

    private List<String> failed;

    public MasterImportResult(int errCode, String errMsg,List<MasterInfo> successed,List<String> failed){
        super(errCode, errMsg);
        this.successed=successed;
        this.failed=failed;
    }

    public List<MasterInfo> getMasterInfoList() {
        return successed;
    }

    public void setMasterInfoList(List<MasterInfo> successed) {
        this.successed = successed;
    }

    public List<String> getFailed() {
        return failed;
    }

    public void setFailed(List<String> failed) {
        this.failed = failed;
    }
}
