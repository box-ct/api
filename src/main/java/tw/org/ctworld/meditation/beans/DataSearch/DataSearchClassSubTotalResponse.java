package tw.org.ctworld.meditation.beans.DataSearch;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class DataSearchClassSubTotalResponse extends BaseResponse {
    private List<ClassSubTotal> items;

    public DataSearchClassSubTotalResponse(int errCode, String errMsg, List<ClassSubTotal> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassSubTotal> getItems() {
        return items;
    }

    public void setItems(List<ClassSubTotal> items) {
        this.items = items;
    }

    public static class ClassSubTotal {
        private String className;
        private int memberMNumber;
        private int memberFNumber;
        private int leaderMNumber;
        private int leaderFNumber;

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public int getMemberMNumber() {
            return memberMNumber;
        }

        public void setMemberMNumber(int memberMNumber) {
            this.memberMNumber = memberMNumber;
        }

        public int getMemberFNumber() {
            return memberFNumber;
        }

        public void setMemberFNumber(int memberFNumber) {
            this.memberFNumber = memberFNumber;
        }

        public int getLeaderMNumber() {
            return leaderMNumber;
        }

        public void setLeaderMNumber(int leaderMNumber) {
            this.leaderMNumber = leaderMNumber;
        }

        public int getLeaderFNumber() {
            return leaderFNumber;
        }

        public void setLeaderFNumber(int leaderFNumber) {
            this.leaderFNumber = leaderFNumber;
        }
    }
}
