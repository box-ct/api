package tw.org.ctworld.meditation.beans.EventEnrollMember;

import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.EventPgmDef;
import tw.org.ctworld.meditation.models.EventPgmOca;

public class EventPgm {
    private String pgmUniqueId, pgmDate, pgmStartTime, pgmEndTime, pgmName, pgmNote, pgmDesc, goTransDate, goTransType, returnTransDate, returnTransType, pgmCategoryId, pgmCategoryName, pgmId, genPgmUiType, genPgmInputOption;
    private boolean isDisabledByOCA, isRequired, isGenPgm, isCenterAvailable;
    private int pgmUiOrderNum, pgmCtOrderNum;

    public EventPgm() {
    }

    public EventPgm(EventPgmOca eventPgm, EventPgmDef eventPgmDef) {
        this.pgmUniqueId = eventPgm.getPgmUniqueId();
        this.pgmName = eventPgm.getPgmName();
        this.pgmNote = eventPgm.getPgmNote();
        this.pgmDesc = eventPgm.getPgmDesc();
        this.goTransType = eventPgm.getGoTransType();
        this.returnTransType = eventPgm.getReturnTransType();
        this.pgmCategoryId = eventPgm.getPgmCategoryId();
        this.pgmCategoryName = eventPgm.getPgmCategoryName();
        this.pgmId = eventPgm.getPgmId();
        this.isDisabledByOCA = eventPgm.getIsDisabledByOCA();
        this.isRequired = eventPgm.getIsRequired();
        this.isGenPgm = eventPgm.getIsGenPgm();
        this.pgmUiOrderNum = eventPgm.getPgmUiOrderNum() != null ? eventPgm.getPgmUiOrderNum() : 0;

        this.pgmDate = CommonUtils.DateTime2DateString(eventPgm.getPgmDate());
        this.pgmStartTime = CommonUtils.DateTime2TimeString(eventPgm.getPgmStartTime());
        this.pgmEndTime = CommonUtils.DateTime2TimeString(eventPgm.getPgmEndTime());
        this.pgmCtOrderNum = eventPgm.getPgmCtOrderNum() != null ? eventPgm.getPgmCtOrderNum() : 0;
        this.goTransDate = CommonUtils.DateTime2DateString(eventPgm.getGoTransDate());
        this.returnTransDate = CommonUtils.DateTime2DateString(eventPgm.getReturnTransDate());

        if (eventPgmDef != null) {
            this.genPgmUiType = eventPgmDef.getGenPgmUiType();
            this.genPgmInputOption = eventPgmDef.getGenPgmInputOption();
            this.isCenterAvailable = eventPgmDef.getIsCenterAvailable();
        }
    }

    public String getPgmUniqueId() {
        return pgmUniqueId;
    }

    public void setPgmUniqueId(String pgmUniqueId) {
        this.pgmUniqueId = pgmUniqueId;
    }

    public String getPgmDate() {
        return pgmDate;
    }

    public void setPgmDate(String pgmDate) {
        this.pgmDate = pgmDate;
    }

    public String getPgmStartTime() {
        return pgmStartTime;
    }

    public void setPgmStartTime(String pgmStartTime) {
        this.pgmStartTime = pgmStartTime;
    }

    public String getPgmEndTime() {
        return pgmEndTime;
    }

    public void setPgmEndTime(String pgmEndTime) {
        this.pgmEndTime = pgmEndTime;
    }

    public int getPgmCtOrderNum() {
        return pgmCtOrderNum;
    }

    public void setPgmCtOrderNum(int pgmCtOrderNum) {
        this.pgmCtOrderNum = pgmCtOrderNum;
    }

    public String getPgmName() {
        return pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

    public String getPgmNote() {
        return pgmNote;
    }

    public void setPgmNote(String pgmNote) {
        this.pgmNote = pgmNote;
    }

    public String getPgmDesc() {
        return pgmDesc;
    }

    public void setPgmDesc(String pgmDesc) {
        this.pgmDesc = pgmDesc;
    }

    public String getGoTransDate() {
        return goTransDate;
    }

    public void setGoTransDate(String goTransDate) {
        this.goTransDate = goTransDate;
    }

    public String getGoTransType() {
        return goTransType;
    }

    public void setGoTransType(String goTransType) {
        this.goTransType = goTransType;
    }

    public String getReturnTransDate() {
        return returnTransDate;
    }

    public void setReturnTransDate(String returnTransDate) {
        this.returnTransDate = returnTransDate;
    }

    public String getReturnTransType() {
        return returnTransType;
    }

    public void setReturnTransType(String returnTransType) {
        this.returnTransType = returnTransType;
    }

    public String getPgmCategoryId() {
        return pgmCategoryId;
    }

    public void setPgmCategoryId(String pgmCategoryId) {
        this.pgmCategoryId = pgmCategoryId;
    }

    public String getPgmCategoryName() {
        return pgmCategoryName;
    }

    public void setPgmCategoryName(String pgmCategoryName) {
        this.pgmCategoryName = pgmCategoryName;
    }

    public String getPgmId() {
        return pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }

    public String getGenPgmUiType() {
        return genPgmUiType;
    }

    public void setGenPgmUiType(String genPgmUiType) {
        this.genPgmUiType = genPgmUiType;
    }

    public String getGenPgmInputOption() {
        return genPgmInputOption;
    }

    public void setGenPgmInputOption(String genPgmInputOption) {
        this.genPgmInputOption = genPgmInputOption;
    }

    public boolean getIsDisabledByOCA() {
        return isDisabledByOCA;
    }

    public void setIsDisabledByOCA(boolean disabledByOCA) {
        isDisabledByOCA = disabledByOCA;
    }

    public boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(boolean required) {
        isRequired = required;
    }

    public boolean getIsGenPgm() {
        return isGenPgm;
    }

    public void setIsGenPgm(boolean genPgm) {
        isGenPgm = genPgm;
    }

    public boolean getIsCenterAvailable() {
        return isCenterAvailable;
    }

    public void setIsCenterAvailable(boolean centerAvailable) {
        isCenterAvailable = centerAvailable;
    }

    public int getPgmUiOrderNum() {
        return pgmUiOrderNum;
    }

    public void setPgmUiOrderNum(int pgmUiOrderNum) {
        this.pgmUiOrderNum = pgmUiOrderNum;
    }
}
