package tw.org.ctworld.meditation.beans.Unit;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.UnitInfo;

import java.util.List;

public class UnitResponse extends BaseResponse {
    private List<UnitInfo> items;

    public UnitResponse(int errCode, String errMsg, List<UnitInfo> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<UnitInfo> getItems() {
        return items;
    }

    public void setItems(List<UnitInfo> items) {
        this.items = items;
    }
}
