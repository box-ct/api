package tw.org.ctworld.meditation.beans.EventEnrollMember;

public class EnrollSigned {
    private String eventId, unitId;
    private boolean isAbbotSigned, isCtApproved, isMaster;

    public EnrollSigned(String eventId, String unitId, Boolean isAbbotSigned, Boolean isCtApproved, Boolean isMaster) {
        this.eventId = eventId;
        this.unitId = unitId;
        this.isAbbotSigned = isAbbotSigned != null ? isAbbotSigned : false;
        this.isCtApproved = isCtApproved != null ? isCtApproved : false;
        this.isMaster = isMaster != null ? isMaster : false;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public boolean getIsAbbotSigned() {
        return isAbbotSigned;
    }

    public void setIsAbbotSigned(boolean abbotSigned) {
        isAbbotSigned = abbotSigned;
    }

    public boolean getIsCtApproved() {
        return isCtApproved;
    }

    public void setIsCtApproved(boolean ctApproved) {
        isCtApproved = ctApproved;
    }

    public boolean getIsMaster() {
        return isMaster;
    }

    public void setIsMaster(boolean master) {
        isMaster = master;
    }
}
