package tw.org.ctworld.meditation.beans.Classes;

public class Classes030Object {

    private String codeId, codeName;

    public Classes030Object(String codeId, String codeName) {
        this.codeId = codeId;
        this.codeName = codeName;
    }

    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }
}
