package tw.org.ctworld.meditation.beans.Public;

public class TrainingFile {

    private String fileName;
    private String lastModified;
    private String fileType;
    private String path;

    public TrainingFile(String fileName, String lastModified, String fileType, String path) {
        this.fileName = fileName;
        this.lastModified = lastModified;
        this.fileType = fileType;
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
