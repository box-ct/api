package tw.org.ctworld.meditation.beans.Sys;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.UserInfo;

import java.util.List;

public class ClassLoginUsersResponse extends BaseResponse {

    private List<UserInfo> items;


    public ClassLoginUsersResponse(int errCode, String errMsg, List<UserInfo> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<UserInfo> getItems() {
        return items;
    }

    public void setItems(List<UserInfo> items) {
        this.items = items;
    }
}
