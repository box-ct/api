package tw.org.ctworld.meditation.beans.DataReport;

import java.util.Date;

public class DataReport011_2Object {

    private String classId, classDayOfWeek;
    private int classWeeksNum;
    private Date classDate;

    public DataReport011_2Object(String classId, String classDayOfWeek, int classWeeksNum, Date classDate) {
        this.classId = classId;
        this.classDayOfWeek = classDayOfWeek;
        this.classWeeksNum = classWeeksNum;
        this.classDate = classDate;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassDayOfWeek() {
        return classDayOfWeek;
    }

    public void setClassDayOfWeek(String classDayOfWeek) {
        this.classDayOfWeek = classDayOfWeek;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }
}
