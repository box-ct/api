package tw.org.ctworld.meditation.beans.Anonymous;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Anonymous002Response extends BaseResponse {

    private List<Anonymous002Object> loginInfo;

    public Anonymous002Response(int errCode, String errMsg, List<Anonymous002Object> loginInfo) {
        super(errCode, errMsg);
        this.loginInfo = loginInfo;
    }

    public List<Anonymous002Object> getLoginInfo() {
        return loginInfo;
    }

    public void setLoginInfo(List<Anonymous002Object> loginInfo) {
        this.loginInfo = loginInfo;
    }
}
