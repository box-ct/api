package tw.org.ctworld.meditation.beans.Printer;

public class Printer009Object {

    private Long id;
    private int fontSize;
    private Boolean isBold, isItalic;
    private String horizontalAlignType, verticalAlignType, textDirection;
    private int width, height, yDistance, xDistance;
    private Boolean isEnabled;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public Boolean getIsBold() {
        return isBold;
    }

    public void setIsBold(Boolean bold) {
        isBold = bold;
    }

    public Boolean getIsItalic() {
        return isItalic;
    }

    public void setIsItalic(Boolean italic) {
        isItalic = italic;
    }

    public String getHorizontalAlignType() {
        return horizontalAlignType;
    }

    public void setHorizontalAlignType(String horizontalAlignType) {
        this.horizontalAlignType = horizontalAlignType;
    }

    public String getVerticalAlignType() {
        return verticalAlignType;
    }

    public void setVerticalAlignType(String verticalAlignType) {
        this.verticalAlignType = verticalAlignType;
    }

    public String getTextDirection() {
        return textDirection;
    }

    public void setTextDirection(String textDirection) {
        this.textDirection = textDirection;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getyDistance() {
        return yDistance;
    }

    public void setyDistance(int yDistance) {
        this.yDistance = yDistance;
    }

    public int getxDistance() {
        return xDistance;
    }

    public void setxDistance(int xDistance) {
        this.xDistance = xDistance;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean enabled) {
        isEnabled = enabled;
    }
}
