package tw.org.ctworld.meditation.beans.ClassRecord;

public class ClassRecord006Request {

    private boolean changeAndWaitPrintMark, isDroppedClass, userDefinedLabel1, userDefinedLabel2;
    private String  classEnrollFormNote;

    public boolean getChangeAndWaitPrintMark() {
        return changeAndWaitPrintMark;
    }

    public void setChangeAndWaitPrintMark(boolean changeAndWaitPrintMark) {
        this.changeAndWaitPrintMark = changeAndWaitPrintMark;
    }

    public boolean getIsDroppedClass() {
        return isDroppedClass;
    }

    public void setIsDroppedClass(boolean droppedClass) {
        isDroppedClass = droppedClass;
    }

    public boolean getUserDefinedLabel1() {
        return userDefinedLabel1;
    }

    public void setUserDefinedLabel1(boolean userDefinedLabel1) {
        this.userDefinedLabel1 = userDefinedLabel1;
    }

    public boolean getUserDefinedLabel2() {
        return userDefinedLabel2;
    }

    public void setUserDefinedLabel2(boolean userDefinedLabel2) {
        this.userDefinedLabel2 = userDefinedLabel2;
    }

    public String getClassEnrollFormNote() {
        return classEnrollFormNote;
    }

    public void setClassEnrollFormNote(String classEnrollFormNote) {
        this.classEnrollFormNote = classEnrollFormNote;
    }
}
