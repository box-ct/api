package tw.org.ctworld.meditation.beans.Member;

import com.fasterxml.jackson.annotation.JsonIgnore;
import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;

public class Members002RegistrationRecordObject {

    private String memberId, aliasName, ctDharmaName, classPeriodNum, className, classGroupId, memberGroupNum;
    @JsonIgnore
    private Date classDateD, attendCheckinDateD;
    private String classDate, attendCheckinDate;
    private int classWeeksNum;
    private String attendCheckinTime, attendMark;

    public Members002RegistrationRecordObject(String memberId, String aliasName, String ctDharmaName,
                                              String classPeriodNum, String className, String classGroupId,
                                              String memberGroupNum, Date classDateD, Date attendCheckinDateD,
                                              int classWeeksNum, String attendMark) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classDateD = classDateD;
        this.classDate = CommonUtils.DateTime2DateString(classDateD);
        this.attendCheckinDateD = attendCheckinDateD;
        this.attendCheckinDate = CommonUtils.DateTime2DateString(attendCheckinDateD);
        this.attendCheckinTime = CommonUtils.DateTime2TimeString(attendCheckinDateD);
        this.classWeeksNum = classWeeksNum;
        this.attendMark = attendMark;
    }

    public Members002RegistrationRecordObject(String memberId, String aliasName, String ctDharmaName,
                                              String classPeriodNum, String className, String classGroupId,
                                              String memberGroupNum, String classDate, String attendCheckinDate,
                                              int classWeeksNum, String attendCheckinTime, String attendMark) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classDate = classDate;
        this.attendCheckinDate = attendCheckinDate;
        this.classWeeksNum = classWeeksNum;
        this.attendCheckinTime = attendCheckinTime;
        this.attendMark = attendMark;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public Date getClassDateD() {
        return classDateD;
    }

    public void setClassDateD(Date classDateD) {
        this.classDateD = classDateD;
    }

    public Date getAttendCheckinDateD() {
        return attendCheckinDateD;
    }

    public void setAttendCheckinDateD(Date attendCheckinDateD) {
        this.attendCheckinDateD = attendCheckinDateD;
    }

    public String getClassDate() {
        return classDate;
    }

    public void setClassDate(String classDate) {
        this.classDate = classDate;
    }

    public String getAttendCheckinDate() {
        return attendCheckinDate;
    }

    public void setAttendCheckinDate(String attendCheckinDate) {
        this.attendCheckinDate = attendCheckinDate;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public String getAttendCheckinTime() {
        return attendCheckinTime;
    }

    public void setAttendCheckinTime(String attendCheckinTime) {
        this.attendCheckinTime = attendCheckinTime;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }
}
