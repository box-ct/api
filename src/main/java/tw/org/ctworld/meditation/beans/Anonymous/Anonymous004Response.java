package tw.org.ctworld.meditation.beans.Anonymous;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class Anonymous004Response extends BaseResponse {

    private String photo, aliasName, ctDharmaName;

    public Anonymous004Response(int errCode, String errMsg, String photo, String aliasName, String ctDharmaName) {
        super(errCode, errMsg);
        this.photo = photo;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
    }

    public Anonymous004Response(String photo, String aliasName, String ctDharmaName) {
        this.photo = photo;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }
}
