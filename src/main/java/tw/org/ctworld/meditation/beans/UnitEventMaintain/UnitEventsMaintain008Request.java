package tw.org.ctworld.meditation.beans.UnitEventMaintain;

public class UnitEventsMaintain008Request {

    private String pgmId;
    private Integer pgmUiOrderNum;
    private String pgmDate, pgmStartTime, pgmEndTime, pgmName, pgmNote;

    public String getPgmId() {
        return pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }

    public Integer getPgmUiOrderNum() {
        return pgmUiOrderNum;
    }

    public void setPgmUiOrderNum(Integer pgmUiOrderNum) {
        this.pgmUiOrderNum = pgmUiOrderNum;
    }

    public String getPgmDate() {
        return pgmDate;
    }

    public void setPgmDate(String pgmDate) {
        this.pgmDate = pgmDate;
    }

    public String getPgmStartTime() {
        return pgmStartTime;
    }

    public void setPgmStartTime(String pgmStartTime) {
        this.pgmStartTime = pgmStartTime;
    }

    public String getPgmEndTime() {
        return pgmEndTime;
    }

    public void setPgmEndTime(String pgmEndTime) {
        this.pgmEndTime = pgmEndTime;
    }

    public String getPgmName() {
        return pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

    public String getPgmNote() {
        return pgmNote;
    }

    public void setPgmNote(String pgmNote) {
        this.pgmNote = pgmNote;
    }
}
