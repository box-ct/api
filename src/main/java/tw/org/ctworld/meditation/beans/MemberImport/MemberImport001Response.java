package tw.org.ctworld.meditation.beans.MemberImport;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class MemberImport001Response  extends BaseResponse {

    private List<MemberListItem> result;

    public MemberImport001Response(int errCode, String errMsg, List<MemberListItem> result){
        super(errCode, errMsg);
        this.result=result;
    }

    public List<MemberListItem> getResult() {
        return result;
    }

    public void setResult(List<MemberListItem> result) {
        this.result = result;
    }
}
