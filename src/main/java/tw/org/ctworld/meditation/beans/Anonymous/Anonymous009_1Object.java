package tw.org.ctworld.meditation.beans.Anonymous;

import java.util.List;

public class Anonymous009_1Object {

    private String classId, className, dayOfWeek;
    private List<Anonymous009_2Object> classDates;

    public Anonymous009_1Object(String classId, String className, String dayOfWeek, List<Anonymous009_2Object> classDates) {
        this.classId = classId;
        this.className = className;
        this.dayOfWeek = dayOfWeek;
        this.classDates = classDates;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public List<Anonymous009_2Object> getClassDates() {
        return classDates;
    }

    public void setClassDates(List<Anonymous009_2Object> classDates) {
        this.classDates = classDates;
    }
}
