package tw.org.ctworld.meditation.beans.Common;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.CodeDef;

import java.util.List;

public class Common009Response extends BaseResponse {

    private List<CodeDef> items;

    public Common009Response(int errCode, String errMsg, List<CodeDef> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<CodeDef> getItems() {
        return items;
    }

    public void setItems(List<CodeDef> items) {
        this.items = items;
    }
}
