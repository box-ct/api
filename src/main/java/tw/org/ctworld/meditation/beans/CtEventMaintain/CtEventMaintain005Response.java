package tw.org.ctworld.meditation.beans.CtEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.EventMain;

public class CtEventMaintain005Response extends BaseResponse {

    private EventMain result;

    public CtEventMaintain005Response(int errCode, String errMsg, EventMain result) {
        super(errCode, errMsg);
        this.result = result;
    }

    public EventMain getResult() {
        return result;
    }

    public void setResult(EventMain result) {
        this.result = result;
    }
}
