package tw.org.ctworld.meditation.beans.CtEventMaintain;

public class CtEventMaintain016Object {

    private String sInputId, sInputName, sInputExample, sInputCategory, sInputWriteType;
    private Integer sInputOrderNum;
    private Boolean isRequired;

    public CtEventMaintain016Object(String sInputId, String sInputName, String sInputExample, String sInputCategory,
                                    String sInputWriteType, Integer sInputOrderNum, Boolean isRequired) {
        this.sInputId = sInputId;
        this.sInputName = sInputName;
        this.sInputExample = sInputExample;
        this.sInputCategory = sInputCategory;
        this.sInputWriteType = sInputWriteType;
        this.sInputOrderNum = sInputOrderNum;
        this.isRequired = isRequired;
    }

    public String getsInputId() {
        return sInputId;
    }

    public void setsInputId(String sInputId) {
        this.sInputId = sInputId;
    }

    public String getsInputName() {
        return sInputName;
    }

    public void setsInputName(String sInputName) {
        this.sInputName = sInputName;
    }

    public String getsInputExample() {
        return sInputExample;
    }

    public void setsInputExample(String sInputExample) {
        this.sInputExample = sInputExample;
    }

    public String getsInputCategory() {
        return sInputCategory;
    }

    public void setsInputCategory(String sInputCategory) {
        this.sInputCategory = sInputCategory;
    }

    public String getsInputWriteType() {
        return sInputWriteType;
    }

    public void setsInputWriteType(String sInputWriteType) {
        this.sInputWriteType = sInputWriteType;
    }

    public Integer getsInputOrderNum() {
        return sInputOrderNum;
    }

    public void setsInputOrderNum(Integer sInputOrderNum) {
        this.sInputOrderNum = sInputOrderNum;
    }

    public Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean required) {
        isRequired = required;
    }
}
