package tw.org.ctworld.meditation.beans.ClassGroup;

import java.util.List;

public class ClassGroup003Request {

    private List<String> enrollFormIds;
    private String classGroupId;

    public List<String> getEnrollFormIds() {
        return enrollFormIds;
    }

    public void setEnrollFormIds(List<String> enrollFormIds) {
        this.enrollFormIds = enrollFormIds;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }
}
