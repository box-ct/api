package tw.org.ctworld.meditation.beans.MemberInfos;

public class SearchMemberInfoEnrollForm {
    private String memberId;
    private String classId;
    private String className;
    private String classPeriodNum;
    private boolean isDroppedClass;

    public SearchMemberInfoEnrollForm(String memberId, String classId, String className, String classPeriodNum, Boolean isDroppedClass) {
        this.memberId = memberId;
        this.classId = classId;
        this.className = className;
        this.classPeriodNum = classPeriodNum;
        this.isDroppedClass = isDroppedClass != null ? isDroppedClass : false;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public boolean getIsDroppedClass() {
        return isDroppedClass;
    }

    public void setIsDroppedClass(boolean isDroppedClass) {
        this.isDroppedClass = isDroppedClass;
    }
}
