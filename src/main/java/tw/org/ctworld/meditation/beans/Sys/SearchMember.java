package tw.org.ctworld.meditation.beans.Sys;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;

public class SearchMember {
    
    private String memberId;
    private String aliasName;
    private String ctDharmaName;
    private String gender;
    private String unitName;
    private String birthDate;
    private String mobileNum1;

    public SearchMember() {
    }

    public SearchMember(String memberId, String aliasName, String ctDharmaName, String gender, String unitName, Date birthDate, String mobileNum1) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.unitName = unitName;
        this.birthDate = DateTime2DateString(birthDate);
        this.mobileNum1 = mobileNum1;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }
}
