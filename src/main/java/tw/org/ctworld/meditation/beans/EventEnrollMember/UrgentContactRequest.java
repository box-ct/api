package tw.org.ctworld.meditation.beans.EventEnrollMember;

public class UrgentContactRequest {
    private String urgentContactPersonName1;
    private String urgentContactPersonPhoneNum1;
    private String urgentContactPersonRelationship1;

    public String getUrgentContactPersonName1() {
        return urgentContactPersonName1;
    }

    public void setUrgentContactPersonName1(String urgentContactPersonName1) {
        this.urgentContactPersonName1 = urgentContactPersonName1;
    }

    public String getUrgentContactPersonPhoneNum1() {
        return urgentContactPersonPhoneNum1;
    }

    public void setUrgentContactPersonPhoneNum1(String urgentContactPersonPhoneNum1) {
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
    }

    public String getUrgentContactPersonRelationship1() {
        return urgentContactPersonRelationship1;
    }

    public void setUrgentContactPersonRelationship1(String urgentContactPersonRelationship1) {
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
    }
}
