package tw.org.ctworld.meditation.beans.UnitEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class UnitEventsMaintain006Response extends BaseResponse {

    private List<UnitEventsMaintain006Object> items;

    public UnitEventsMaintain006Response(int errCode, String errMsg, List<UnitEventsMaintain006Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<UnitEventsMaintain006Object> getItems() {
        return items;
    }

    public void setItems(List<UnitEventsMaintain006Object> items) {
        this.items = items;
    }
}
