package tw.org.ctworld.meditation.beans.ClassStats;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class ClassStats001Response extends BaseResponse {

    private List<String> items;

    public ClassStats001Response(int errCode, String errMsg, List<String> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
