package tw.org.ctworld.meditation.beans.DataReport;

import java.util.Date;

public class DataReport011_1Object {

    private String unitId;
    private boolean overseasUnit;
    private Date classDate;
    private String memberId, classId, classEnrollFormId, attendMark;
    private int classWeeksNum;


    public DataReport011_1Object(String unitId, Date classDate, String memberId, String classId, String classEnrollFormId,
                                 String attendMark, int classWeeksNum) {
        this.unitId = unitId;
        this.classDate = classDate;
        this.memberId = memberId;
        this.classId = classId;
        this.classEnrollFormId = classEnrollFormId;
        this.attendMark = attendMark;
        this.classWeeksNum = classWeeksNum;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public boolean getOverseasUnit() {
        return overseasUnit;
    }

    public void setOverseasUnit(boolean overseasUnit) {
        this.overseasUnit = overseasUnit;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }
}
