package tw.org.ctworld.meditation.beans;

import com.fasterxml.jackson.databind.ser.Serializers;
import org.springframework.data.domain.Page;
import tw.org.ctworld.meditation.beans.EventEnrollMember.MemberInfoNotEnrolled;

import java.util.List;

public class PageResponse<T> extends BaseResponse {
    private int page;
    private int size;
    private long total;
    private int totalPage;
    private List<T> items;

    public PageResponse(int errCode, String errMsg, int page, int size, long total, int totalPage, List<T> items) {
        super(errCode, errMsg);
        this.page = page;
        this.size = size;
        this.total = total;
        this.totalPage = totalPage;
        this.items = items;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
