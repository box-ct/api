package tw.org.ctworld.meditation.beans.SysCode;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class LoginContactResponse extends BaseResponse {
    private String text;

    public LoginContactResponse(int errCode, String errMsg, String text) {
        super(errCode, errMsg);
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
