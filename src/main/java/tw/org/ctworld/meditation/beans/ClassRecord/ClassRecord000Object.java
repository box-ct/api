package tw.org.ctworld.meditation.beans.ClassRecord;

public class ClassRecord000Object {

    private String classId, classStatus, year, classPeriodNum, className, memberId, classJobTitleList;
    private boolean isExecuteClassAttendRecord, isExecuteClassGraduateStats, isExecuteClassUpGrade, isSeeAllClassRecord;

    public ClassRecord000Object(String classId, String classStatus, String year, String classPeriodNum, String className,
                                String memberId, String classJobTitleList) {
        this.classId = classId;
        this.classStatus = classStatus;
        this.year = year;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.memberId = memberId;
        this.classJobTitleList = classJobTitleList;
        this.isExecuteClassAttendRecord = false;
        this.isExecuteClassGraduateStats = false;
        this.isExecuteClassUpGrade = false;
        this.isSeeAllClassRecord = false;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassStatus() {
        return classStatus;
    }

    public void setClassStatus(String classStatus) {
        this.classStatus = classStatus;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public boolean getIsExecuteClassAttendRecord() {
        return isExecuteClassAttendRecord;
    }

    public void setIsExecuteClassAttendRecord(boolean executeClassAttendRecord) {
        isExecuteClassAttendRecord = executeClassAttendRecord;
    }

    public boolean getIsExecuteClassGraduateStats() {
        return isExecuteClassGraduateStats;
    }

    public void setIsExecuteClassGraduateStats(boolean executeClassGraduateStats) {
        isExecuteClassGraduateStats = executeClassGraduateStats;
    }

    public boolean getIsExecuteClassUpGrade() {
        return isExecuteClassUpGrade;
    }

    public void setIsExecuteClassUpGrade(boolean executeClassUpGrade) {
        isExecuteClassUpGrade = executeClassUpGrade;
    }

    public boolean getIsSeeAllClassRecord() {
        return isSeeAllClassRecord;
    }

    public void setIsSeeAllClassRecord(boolean seeAllClassRecord) {
        isSeeAllClassRecord = seeAllClassRecord;
    }
}
