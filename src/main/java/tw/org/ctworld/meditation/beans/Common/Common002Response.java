package tw.org.ctworld.meditation.beans.Common;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Common002Response extends BaseResponse {

    private String total;
    private String attendance;
    private List<Common002Object> items;

    public Common002Response(String total, String attendance, List<Common002Object> items) {
        this.total = total;
        this.attendance = attendance;
        this.items = items;
    }

    public Common002Response(int errCode, String errMsg, String total, String attendance, List<Common002Object> items) {
        super(errCode, errMsg);
        this.total = total;
        this.attendance = attendance;
        this.items = items;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public List<Common002Object> getItems() {
        return items;
    }

    public void setItems(List<Common002Object> items) {
        this.items = items;
    }

    public static class Common002Object {

        private String className, total, attendance;

        public Common002Object(String className, String total, String attendance) {
            this.className = className;
            this.total = total;
            this.attendance = attendance;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getAttendance() {
            return attendance;
        }

        public void setAttendance(String attendance) {
            this.attendance = attendance;
        }
    }
}
