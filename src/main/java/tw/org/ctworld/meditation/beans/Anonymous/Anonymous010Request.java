package tw.org.ctworld.meditation.beans.Anonymous;

import java.util.List;

public class Anonymous010Request {

    private String memberId, classId, note;
    private List<Integer> classWeeksNums;
    private String userName;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<Integer> getClassWeeksNums() {
        return classWeeksNums;
    }

    public void setClassWeeksNums(List<Integer> classWeeksNums) {
        this.classWeeksNums = classWeeksNums;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
