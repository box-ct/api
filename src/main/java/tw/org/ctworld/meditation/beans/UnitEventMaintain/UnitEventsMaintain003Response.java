package tw.org.ctworld.meditation.beans.UnitEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class UnitEventsMaintain003Response extends BaseResponse {

    private UnitEventsMaintain003_2Object result;

    public UnitEventsMaintain003Response(int errCode, String errMsg, UnitEventsMaintain003_2Object result) {
        super(errCode, errMsg);
        this.result = result;
    }

    public UnitEventsMaintain003_2Object getResult() {
        return result;
    }

    public void setResult(UnitEventsMaintain003_2Object result) {
        this.result = result;
    }
}
