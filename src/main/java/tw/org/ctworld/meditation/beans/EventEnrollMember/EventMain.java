package tw.org.ctworld.meditation.beans.EventEnrollMember;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.List;

public class EventMain {
    private String id, eventId, eventName, eventCategoryId, eventCategoryName, eventCreatorUnitId, eventCreatorUnitName, sponsorUnitId, sponsorUnitName, eventNote, idType1List, idType2List, attendUnitList, noTimeLimitUnitList, eventYear, eventPlace, eventEndDate, eventEndTime, eventStartDate, eventStartTime, changeEndDate, enrollEndDate, finalDate, summerDonationType;
    private boolean isAvailableForAllUnit, isShowSummerDonationButton, isMultipleEnroll, isSyncCtTable, isNeedCtApproved, isNeedAbbotSigned, isOnlyForMaster, isShowRefugeTab, isShowPrecept5Tab, isShowBodhiPreceptTab, isShowZen7Tab, isShowPrecept8Tab, isOtherUnitCanApplyThisUnitEvent, isCreatedByCt, isCreatedByUnit, isHeldInUnit;
    private int status;
    private List<EventPgm> pgmList;

    public EventMain() {
    }

    public EventMain(tw.org.ctworld.meditation.models.EventMain eventMain) {
        this.id = String.valueOf(eventMain.getId());
        this.eventId = eventMain.getEventId();
        this.eventName = eventMain.getEventName();
        this.eventCategoryId = eventMain.getEventCategoryId();
        this.eventCategoryName = eventMain.getEventCategoryName();
        this.eventCreatorUnitId = eventMain.getEventCreatorUnitId();
        this.eventCreatorUnitName = eventMain.getEventCreatorUnitName();
        this.sponsorUnitId = eventMain.getSponsorUnitId();
        this.sponsorUnitName = eventMain.getSponsorUnitName();
        this.eventNote = eventMain.getEventNote();
        this.idType1List = eventMain.getIdType1List();
        this.idType2List = eventMain.getIdType2List();
        this.attendUnitList = eventMain.getAttendUnitList();        
        this.noTimeLimitUnitList = eventMain.getNoTimeLimitUnitList();
        this.eventYear = eventMain.getEventYear();
        this.eventPlace = eventMain.getEventPlace();
        
        this.eventEndDate = CommonUtils.DateTime2DateString(eventMain.getEventEndDate());
        this.eventEndTime = CommonUtils.DateTime2TimeString(eventMain.getEventEndTime());
        this.eventStartDate = CommonUtils.DateTime2DateString(eventMain.getEventStartDate());
        this.eventStartTime = CommonUtils.DateTime2TimeString(eventMain.getEventStartTime());
        this.changeEndDate = CommonUtils.DateTime2DateString(eventMain.getChangeEndDate());
        this.enrollEndDate = CommonUtils.DateTime2DateString(eventMain.getEnrollEndDate());
        this.finalDate = CommonUtils.DateTime2DateString(eventMain.getFinalDate());
        
        this.summerDonationType = eventMain.getSummerDonationType();
        this.isAvailableForAllUnit = eventMain.getIsAvailableForAllUnit();
        this.isShowSummerDonationButton = eventMain.getIsShowSummerDonationButton();
        this.isMultipleEnroll = eventMain.getIsMultipleEnroll();
        this.isSyncCtTable = eventMain.getIsSyncCtTable();
        this.isNeedCtApproved = eventMain.getIsNeedCtApproved();
        this.isNeedAbbotSigned = eventMain.getIsNeedAbbotSigned();
        this.isOnlyForMaster = eventMain.getIsOnlyForMaster();
        this.isShowRefugeTab = eventMain.getIsShowRefugeTab();
        this.isShowPrecept5Tab = eventMain.getIsShowPrecept5Tab();
        this.isShowBodhiPreceptTab = eventMain.getIsShowBodhiPreceptTab();
        this.isShowZen7Tab = eventMain.getIsShowZen7Tab();
        this.isShowPrecept8Tab = eventMain.getIsShowPrecept8Tab();
        this.isOtherUnitCanApplyThisUnitEvent = eventMain.getIsOtherUnitCanApplyThisUnitEvent();
        this.isCreatedByCt = eventMain.getIsCreatedByCt();
        this.isCreatedByUnit = eventMain.getIsCreatedByUnit();
        this.isHeldInUnit = eventMain.getIsHeldInUnit();
        this.status = eventMain.getStatus();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventCategoryId() {
        return eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public String getEventCreatorUnitId() {
        return eventCreatorUnitId;
    }

    public void setEventCreatorUnitId(String eventCreatorUnitId) {
        this.eventCreatorUnitId = eventCreatorUnitId;
    }

    public String getEventCreatorUnitName() {
        return eventCreatorUnitName;
    }

    public void setEventCreatorUnitName(String eventCreatorUnitName) {
        this.eventCreatorUnitName = eventCreatorUnitName;
    }

    public String getSponsorUnitId() {
        return sponsorUnitId;
    }

    public void setSponsorUnitId(String sponsorUnitId) {
        this.sponsorUnitId = sponsorUnitId;
    }

    public String getSponsorUnitName() {
        return sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getEventNote() {
        return eventNote;
    }

    public void setEventNote(String eventNote) {
        this.eventNote = eventNote;
    }

    public String getIdType1List() {
        return idType1List;
    }

    public void setIdType1List(String idType1List) {
        this.idType1List = idType1List;
    }

    public String getIdType2List() {
        return idType2List;
    }

    public void setIdType2List(String idType2List) {
        this.idType2List = idType2List;
    }

    public String getAttendUnitList() {
        return attendUnitList;
    }

    public void setAttendUnitList(String attendUnitList) {
        this.attendUnitList = attendUnitList;
    }

    public String getNoTimeLimitUnitList() {
        return noTimeLimitUnitList;
    }

    public void setNoTimeLimitUnitList(String noTimeLimitUnitList) {
        this.noTimeLimitUnitList = noTimeLimitUnitList;
    }

    public String getEventYear() {
        return eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public String getEventPlace() {
        return eventPlace;
    }

    public void setEventPlace(String eventPlace) {
        this.eventPlace = eventPlace;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getChangeEndDate() {
        return changeEndDate;
    }

    public void setChangeEndDate(String changeEndDate) {
        this.changeEndDate = changeEndDate;
    }

    public String getEnrollEndDate() {
        return enrollEndDate;
    }

    public void setEnrollEndDate(String enrollEndDate) {
        this.enrollEndDate = enrollEndDate;
    }

    public String getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(String finalDate) {
        this.finalDate = finalDate;
    }

    public String getSummerDonationType() {
        return summerDonationType;
    }

    public void setSummerDonationType(String summerDonationType) {
        this.summerDonationType = summerDonationType;
    }

    public boolean getIsAvailableForAllUnit() {
        return isAvailableForAllUnit;
    }

    public void setIsAvailableForAllUnit(boolean availableForAllUnit) {
        isAvailableForAllUnit = availableForAllUnit;
    }

    public boolean getIsShowSummerDonationButton() {
        return isShowSummerDonationButton;
    }

    public void setIsShowSummerDonationButton(boolean showSummerDonationButton) {
        isShowSummerDonationButton = showSummerDonationButton;
    }

    public boolean getIsMultipleEnroll() {
        return isMultipleEnroll;
    }

    public void setIsMultipleEnroll(boolean multipleEnroll) {
        isMultipleEnroll = multipleEnroll;
    }

    public boolean getIsSyncCtTable() {
        return isSyncCtTable;
    }

    public void setIsSyncCtTable(boolean syncCtTable) {
        isSyncCtTable = syncCtTable;
    }

    public boolean getIsNeedCtApproved() {
        return isNeedCtApproved;
    }

    public void setIsNeedCtApproved(boolean needCtApproved) {
        isNeedCtApproved = needCtApproved;
    }

    public boolean getIsNeedAbbotSigned() {
        return isNeedAbbotSigned;
    }

    public void setIsNeedAbbotSigned(boolean needAbbotSigned) {
        isNeedAbbotSigned = needAbbotSigned;
    }

    public boolean getIsOnlyForMaster() {
        return isOnlyForMaster;
    }

    public void setIsOnlyForMaster(boolean onlyForMaster) {
        isOnlyForMaster = onlyForMaster;
    }

    public boolean getIsShowRefugeTab() {
        return isShowRefugeTab;
    }

    public void setIsShowRefugeTab(boolean showRefugeTab) {
        isShowRefugeTab = showRefugeTab;
    }

    public boolean getIsShowPrecept5Tab() {
        return isShowPrecept5Tab;
    }

    public void setIsShowPrecept5Tab(boolean showPrecept5Tab) {
        isShowPrecept5Tab = showPrecept5Tab;
    }

    public boolean getIsShowBodhiPreceptTab() {
        return isShowBodhiPreceptTab;
    }

    public void setIsShowBodhiPreceptTab(boolean showBodhiPreceptTab) {
        isShowBodhiPreceptTab = showBodhiPreceptTab;
    }

    public boolean getIsShowZen7Tab() {
        return isShowZen7Tab;
    }

    public void setIsShowZen7Tab(boolean showZen7Tab) {
        isShowZen7Tab = showZen7Tab;
    }

    public boolean getIsShowPrecept8Tab() {
        return isShowPrecept8Tab;
    }

    public void setIsShowPrecept8Tab(boolean showPrecept8Tab) {
        isShowPrecept8Tab = showPrecept8Tab;
    }

    public boolean getIsOtherUnitCanApplyThisUnitEvent() {
        return isOtherUnitCanApplyThisUnitEvent;
    }

    public void setIsOtherUnitCanApplyThisUnitEvent(boolean otherUnitCanApplyThisUnitEvent) {
        isOtherUnitCanApplyThisUnitEvent = otherUnitCanApplyThisUnitEvent;
    }

    public boolean getIsCreatedByCt() {
        return isCreatedByCt;
    }

    public void setIsCreatedByCt(boolean createdByCt) {
        isCreatedByCt = createdByCt;
    }

    public boolean getIsCreatedByUnit() {
        return isCreatedByUnit;
    }

    public void setIsCreatedByUnit(boolean createdByUnit) {
        isCreatedByUnit = createdByUnit;
    }

    public boolean getIsHeldInUnit() {
        return isHeldInUnit;
    }

    public void setIsHeldInUnit(boolean heldInUnit) {
        isHeldInUnit = heldInUnit;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<EventPgm> getPgmList() {
        return pgmList;
    }

    public void setPgmList(List<EventPgm> pgmList) {
        this.pgmList = pgmList;
    }
}
