package tw.org.ctworld.meditation.beans.EventEnrollMember;

import java.util.List;

public class CancelEnrollMainRequest {
    private List<String> formAutoAddNums;

    public List<String> getFormAutoAddNums() {
        return formAutoAddNums;
    }

    public void setFormAutoAddNums(List<String> formAutoAddNums) {
        this.formAutoAddNums = formAutoAddNums;
    }
}
