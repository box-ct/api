package tw.org.ctworld.meditation.beans.UnitStartUp;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class UnitStartUp001Response extends BaseResponse {

    private String eventEnrollGroupNameList;

    public UnitStartUp001Response(int errCode, String errMsg, String eventEnrollGroupNameList) {
        super(errCode, errMsg);
        this.eventEnrollGroupNameList = eventEnrollGroupNameList;
    }

    public String getEventEnrollGroupNameList() {
        return eventEnrollGroupNameList;
    }

    public void setEventEnrollGroupNameList(String eventEnrollGroupNameList) {
        this.eventEnrollGroupNameList = eventEnrollGroupNameList;
    }
}
