package tw.org.ctworld.meditation.beans.CtEventMaintain;

public class CtEventMaintain004Request {

    private String copyId;
    private CtEventMaintain004_1Object data;

    public String getCopyId() {
        return copyId;
    }

    public void setCopyId(String copyId) {
        this.copyId = copyId;
    }

    public CtEventMaintain004_1Object getData() {
        return data;
    }

    public void setData(CtEventMaintain004_1Object data) {
        this.data = data;
    }
}
