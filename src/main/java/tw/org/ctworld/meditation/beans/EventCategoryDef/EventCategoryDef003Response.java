package tw.org.ctworld.meditation.beans.EventCategoryDef;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class EventCategoryDef003Response extends BaseResponse {

    private EventCategoryDef003Object data;

    public EventCategoryDef003Response(int errCode, String errMsg, EventCategoryDef003Object data) {
        super(errCode, errMsg);
        this.data = data;
    }

    public EventCategoryDef003Object getData() {
        return data;
    }

    public void setData(EventCategoryDef003Object data) {
        this.data = data;
    }
}
