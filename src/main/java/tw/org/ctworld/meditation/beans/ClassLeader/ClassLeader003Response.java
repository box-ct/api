package tw.org.ctworld.meditation.beans.ClassLeader;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.ClassInfo;

import java.util.List;

public class ClassLeader003Response extends BaseResponse {

    private List<ClassInfo> items;

    public ClassLeader003Response(int errCode, String errMsg, List<ClassInfo> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassInfo> getItems() {
        return items;
    }

    public void setItems(List<ClassInfo> items) {
        this.items = items;
    }
}
