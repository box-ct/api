package tw.org.ctworld.meditation.beans.EventPgmDef;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class EventPgmDef002Response extends BaseResponse {

    private EventPgmDef002Object data;

    public EventPgmDef002Response(int errCode, String errMsg, EventPgmDef002Object data) {
        super(errCode, errMsg);
        this.data = data;
    }

    public EventPgmDef002Object getData() {
        return data;
    }

    public void setData(EventPgmDef002Object data) {
        this.data = data;
    }
}
