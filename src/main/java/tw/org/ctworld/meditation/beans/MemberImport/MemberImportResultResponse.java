package tw.org.ctworld.meditation.beans.MemberImport;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class MemberImportResultResponse extends BaseResponse {
    List<MemberImportResult> result;
    public MemberImportResultResponse(int errCode, String errMsg, List<MemberImportResult> result){
        super(errCode, errMsg);
        this.result=result;
    }

    public List<MemberImportResult> getResult() {
        return result;
    }

    public void setResult(List<MemberImportResult> result) {
        this.result = result;
    }
}
