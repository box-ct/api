package tw.org.ctworld.meditation.beans.MemberInfos;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;

public class DuplicateMember {
    private String memberId;
    private String twIdNum;
    private String fullName;
    private String aliasName;
    private String ctDharmaName;
    private String gender;
    private String birthDate;
    private String mailingCountry;
    private String mailingState;
    private String mailingCity;
    private String mailingStreet;
    private String familyLeaderName;
    private String familyLeaderMemberId;
    private String familyLeaderRelationship;
    private String homePhoneNum1;
    private String mobileNum1;

    public DuplicateMember(String memberId, String twIdNum, String fullName, String aliasName, String ctDharmaName, String gender, Date birthDate, String mailingCountry, String mailingState, String mailingCity, String mailingStreet) {
        this.memberId = memberId;
        this.twIdNum = twIdNum;
        this.fullName = fullName;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.birthDate = CommonUtils.DateTime2DateString(birthDate);
        this.mailingCountry = mailingCountry;
        this.mailingState = mailingState;
        this.mailingCity = mailingCity;
        this.mailingStreet = mailingStreet;
    }

    public DuplicateMember(String memberId, String aliasName, String ctDharmaName, String gender, Date birthDate, String familyLeaderName, String familyLeaderMemberId, String familyLeaderRelationship, String homePhoneNum1, String mobileNum1) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.birthDate = CommonUtils.DateTime2DateString(birthDate);
        this.familyLeaderName = familyLeaderName;
        this.familyLeaderMemberId = familyLeaderMemberId;
        this.familyLeaderRelationship = familyLeaderRelationship;
        this.homePhoneNum1 = homePhoneNum1;
        this.mobileNum1 = mobileNum1;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getMailingCountry() {
        return mailingCountry;
    }

    public void setMailingCountry(String mailingCountry) {
        this.mailingCountry = mailingCountry;
    }

    public String getMailingState() {
        return mailingState;
    }

    public void setMailingState(String mailingState) {
        this.mailingState = mailingState;
    }

    public String getMailingCity() {
        return mailingCity;
    }

    public void setMailingCity(String mailingCity) {
        this.mailingCity = mailingCity;
    }

    public String getMailingStreet() {
        return mailingStreet;
    }

    public void setMailingStreet(String mailingStreet) {
        this.mailingStreet = mailingStreet;
    }

    public String getFamilyLeaderName() {
        return familyLeaderName;
    }

    public void setFamilyLeaderName(String familyLeaderName) {
        this.familyLeaderName = familyLeaderName;
    }

    public String getFamilyLeaderMemberId() {
        return familyLeaderMemberId;
    }

    public void setFamilyLeaderMemberId(String familyLeaderMemberId) {
        this.familyLeaderMemberId = familyLeaderMemberId;
    }

    public String getFamilyLeaderRelationship() {
        return familyLeaderRelationship;
    }

    public void setFamilyLeaderRelationship(String familyLeaderRelationship) {
        this.familyLeaderRelationship = familyLeaderRelationship;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }
}
