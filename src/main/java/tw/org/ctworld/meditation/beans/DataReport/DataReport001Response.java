package tw.org.ctworld.meditation.beans.DataReport;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class DataReport001Response extends BaseResponse {

    private List<DataReport001Object> items;

    public DataReport001Response(int errCode, String errMsg, List<DataReport001Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<DataReport001Object> getItems() {
        return items;
    }

    public void setItems(List<DataReport001Object> items) {
        this.items = items;
    }


    public static class DataReport001Object {

        private String unitName, year, classPeriodNum, className, classId;
        private int registerMNumber, registerFNumber, cancelMNumber, cancelFNumber, graduatedMNumber, graduatedFNumber;

        public DataReport001Object(String unitName, String year, String classPeriodNum, String className, String classId,
                                   int registerMNumber, int registerFNumber, int cancelMNumber, int cancelFNumber,
                                   int graduatedMNumber, int graduatedFNumber) {
            this.unitName = unitName;
            this.year = year;
            this.classPeriodNum = classPeriodNum;
            this.className = className;
            this.classId = classId;
            this.registerMNumber = registerMNumber;
            this.registerFNumber = registerFNumber;
            this.cancelMNumber = cancelMNumber;
            this.cancelFNumber = cancelFNumber;
            this.graduatedMNumber = graduatedMNumber;
            this.graduatedFNumber = graduatedFNumber;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getClassPeriodNum() {
            return classPeriodNum;
        }

        public void setClassPeriodNum(String classPeriodNum) {
            this.classPeriodNum = classPeriodNum;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public int getRegisterMNumber() {
            return registerMNumber;
        }

        public void setRegisterMNumber(int registerMNumber) {
            this.registerMNumber = registerMNumber;
        }

        public int getRegisterFNumber() {
            return registerFNumber;
        }

        public void setRegisterFNumber(int registerFNumber) {
            this.registerFNumber = registerFNumber;
        }

        public int getCancelMNumber() {
            return cancelMNumber;
        }

        public void setCancelMNumber(int cancelMNumber) {
            this.cancelMNumber = cancelMNumber;
        }

        public int getCancelFNumber() {
            return cancelFNumber;
        }

        public void setCancelFNumber(int cancelFNumber) {
            this.cancelFNumber = cancelFNumber;
        }

        public int getGraduatedMNumber() {
            return graduatedMNumber;
        }

        public void setGraduatedMNumber(int graduatedMNumber) {
            this.graduatedMNumber = graduatedMNumber;
        }

        public int getGraduatedFNumber() {
            return graduatedFNumber;
        }

        public void setGraduatedFNumber(int graduatedFNumber) {
            this.graduatedFNumber = graduatedFNumber;
        }
    }
}
