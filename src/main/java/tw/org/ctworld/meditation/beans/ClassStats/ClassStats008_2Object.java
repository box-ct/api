package tw.org.ctworld.meditation.beans.ClassStats;

import java.util.Date;

public class ClassStats008_2Object {

    private Date classDate;
    private String attendMark;
    private Date updateDtTm;

    public ClassStats008_2Object(Date classDate, String attendMark, Date updateDtTm) {
        this.classDate = classDate;
        this.attendMark = attendMark;
        this.updateDtTm = updateDtTm;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }

    public Date getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(Date updateDtTm) {
        this.updateDtTm = updateDtTm;
    }
}
