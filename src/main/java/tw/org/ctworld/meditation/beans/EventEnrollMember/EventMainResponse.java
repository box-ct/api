package tw.org.ctworld.meditation.beans.EventEnrollMember;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class EventMainResponse extends BaseResponse {
    private EventMain eventMain;

    public EventMainResponse(int errCode, String errMsg, EventMain eventMain) {
        super(errCode, errMsg);
        this.eventMain = eventMain;
    }

    public EventMain getEventMain() {
        return eventMain;
    }

    public void setEventMain(EventMain eventMain) {
        this.eventMain = eventMain;
    }
}
