package tw.org.ctworld.meditation.beans.Unit;

public class Unit003Object {

    private String unitId, unitName, group, district, sortNum, timezone, unitStartIp, unitEndIp;
    private boolean isNoIpLimit;
    private String createDtTm, creatorName, creatorId, creatorIp, creatorUnitName, updateDtTm, updatorName, updatorId, updatorIp, updatorUnitName;
    private boolean isOverseasUnit;
    
    private String abbotName;
    private String abbotEngName;
    private String certUnitEngName;
    private String stateName;
    private String certUnitName;
    
    public Unit003Object(String unitId, String unitName, String group, String district, String sortNum,
                         String timezone, String unitStartIp, String unitEndIp, boolean isNoIpLimit,
                         String createDtTm, String creatorName, String creatorId, String creatorIp,
                         String creatorUnitName, String updateDtTm, String updatorName, String updatorId,
                         String updatorIp, String updatorUnitName, boolean isOverseasUnit,
                         String abbotName,
                         String abbotEngName,
                         String certUnitEngName,
                         String stateName,
                         String certUnitName) {
        this.unitId = unitId;
        this.unitName = unitName;
        this.group = group;
        this.district = district;
        this.sortNum = sortNum;
        this.timezone = timezone;
        this.unitStartIp = unitStartIp;
        this.unitEndIp = unitEndIp;
        this.isNoIpLimit = isNoIpLimit;
        this.createDtTm = createDtTm;
        this.creatorName = creatorName;
        this.creatorId = creatorId;
        this.creatorIp = creatorIp;
        this.creatorUnitName = creatorUnitName;
        this.updateDtTm = updateDtTm;
        this.updatorName = updatorName;
        this.updatorId = updatorId;
        this.updatorIp = updatorIp;
        this.updatorUnitName = updatorUnitName;
        this.isOverseasUnit = isOverseasUnit;

        this.abbotName = abbotName;
        this.abbotEngName = abbotEngName;
        this.certUnitEngName = certUnitEngName;
        this.stateName = stateName;
        this.certUnitName = certUnitName;
        
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getSortNum() {
        return sortNum;
    }

    public void setSortNum(String sortNum) {
        this.sortNum = sortNum;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getUnitStartIp() {
        return unitStartIp;
    }

    public void setUnitStartIp(String unitStartIp) {
        this.unitStartIp = unitStartIp;
    }

    public String getUnitEndIp() {
        return unitEndIp;
    }

    public void setUnitEndIp(String unitEndIp) {
        this.unitEndIp = unitEndIp;
    }

    public boolean isNoIpLimit() {
        return isNoIpLimit;
    }

    public void setNoIpLimit(boolean noIpLimit) {
        isNoIpLimit = noIpLimit;
    }

    public String getCreateDtTm() {
        return createDtTm;
    }

    public void setCreateDtTm(String createDtTm) {
        this.createDtTm = createDtTm;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorIp() {
        return creatorIp;
    }

    public void setCreatorIp(String creatorIp) {
        this.creatorIp = creatorIp;
    }

    public String getCreatorUnitName() {
        return creatorUnitName;
    }

    public void setCreatorUnitName(String creatorUnitName) {
        this.creatorUnitName = creatorUnitName;
    }

    public String getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(String updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId;
    }

    public String getUpdatorIp() {
        return updatorIp;
    }

    public void setUpdatorIp(String updatorIp) {
        this.updatorIp = updatorIp;
    }

    public String getUpdatorUnitName() {
        return updatorUnitName;
    }

    public void setUpdatorUnitName(String updatorUnitName) {
        this.updatorUnitName = updatorUnitName;
    }

    public boolean getIsOverseasUnit() {
        return isOverseasUnit;
    }

    public void setIsOverseasUnit(boolean overseasUnit) {
        isOverseasUnit = overseasUnit;
    }

    public String getAbbotName() {
        return abbotName;
    }

    public void setAbbotName(String abbotName) {
        this.abbotName = abbotName;
    }

    public String getAbbotEngName() {
        return abbotEngName;
    }

    public void setAbbotEngName(String abbotEngName) {
        this.abbotEngName = abbotEngName;
    }

    public String getCertUnitEngName() {
        return certUnitEngName;
    }

    public void setCertUnitEngName(String certUnitEngName) {
        this.certUnitEngName = certUnitEngName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCertUnitName() {
        return certUnitName;
    }

    public void setCertUnitName(String certUnitName) {
        this.certUnitName = certUnitName;
    }
}
