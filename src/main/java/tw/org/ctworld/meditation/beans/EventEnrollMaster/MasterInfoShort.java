package tw.org.ctworld.meditation.beans.EventEnrollMaster;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class MasterInfoShort {

    private boolean isAbbot;
    private boolean isCt;
    private boolean isTreasurer;
    private String jobOrder;
    private String jobTitle;
    private String masterId;
    private String masterName;
    private String masterPreceptTypeName;
    private String mobileNum;
    private String preceptOrder;
    private String unitId;
    private String unitName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Taipei")
    private Date updateDtTm;
    private String updatorName;

    public MasterInfoShort() {
    }

    public MasterInfoShort(Boolean isAbbot, Boolean isCt, Boolean isTreasurer, String jobOrder, String jobTitle, String masterId, String masterName, String masterPreceptTypeName, String mobileNum, String preceptOrder, String unitId, String unitName, Date updateDtTm, String updatorName) {
        this.isAbbot = isAbbot != null ? isAbbot : false;
        this.isCt = isCt != null ? isCt : false;
        this.isTreasurer = isTreasurer != null ? isTreasurer : false;
        this.jobOrder = jobOrder;
        this.jobTitle = jobTitle;
        this.masterId = masterId;
        this.masterName = masterName;
        this.masterPreceptTypeName = masterPreceptTypeName;
        this.mobileNum = mobileNum;
        this.preceptOrder = preceptOrder;
        this.unitId = unitId;
        this.unitName = unitName;
        this.updateDtTm = updateDtTm;
        this.updatorName = updatorName;
    }

//    public MasterInfoShort(Boolean isAbbot, Boolean isTreasurer, String jobOrder, String jobTitle, String masterId, String masterName, String masterPreceptTypeName, String mobileNum, String preceptOrder, Date updateDtTm, String updatorName) {
//        this.isAbbot = isAbbot != null ? isAbbot : false;
//        this.isCt = true;
//        this.isTreasurer = isTreasurer != null ? isTreasurer : false;
//        this.jobOrder = jobOrder;
//        this.jobTitle = jobTitle;
//        this.masterId = masterId;
//        this.masterName = masterName;
//        this.masterPreceptTypeName = masterPreceptTypeName;
//        this.mobileNum = mobileNum;
//        this.preceptOrder = preceptOrder;
//        this.updateDtTm = updateDtTm;
//        this.updatorName = updatorName;
//    }

    public boolean getIsAbbot() {
        return isAbbot;
    }

    public void setIsAbbot(boolean abbot) {
        isAbbot = abbot;
    }

    public boolean getIsCt() {
        return isCt;
    }

    public void setIsCt(boolean ct) {
        isCt = ct;
    }

    public boolean getIsTreasurer() {
        return isTreasurer;
    }

    public void setIsTreasurer(boolean treasurer) {
        isTreasurer = treasurer;
    }

    public String getJobOrder() {
        return jobOrder;
    }

    public void setJobOrder(String jobOrder) {
        this.jobOrder = jobOrder;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getMasterPreceptTypeName() {
        return masterPreceptTypeName;
    }

    public void setMasterPreceptTypeName(String masterPreceptTypeName) {
        this.masterPreceptTypeName = masterPreceptTypeName;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getPreceptOrder() {
        return preceptOrder;
    }

    public void setPreceptOrder(String preceptOrder) {
        this.preceptOrder = preceptOrder;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Date getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(Date updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

}
