package tw.org.ctworld.meditation.beans.UnitEventMaintain;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.*;


public class UnitEventsMaintain002Object {

    private String
            eventCategoryName,
            eventCategoryColorCode,
            eventEndDate,
            eventId,
            eventName,
            eventNote,
            eventStartDate,
            eventYear,
            sponsorUnitName,
            updateDtTm,
            updatorName;

    private int
            abbotSignedCount,
            masterEnrollCount,
            notAbbotSignedCount,
            notMasterEnrollCount,
            peopleForecast,
            status,
            waitCtApprovedCount;

    private boolean
            isNeedAbbotSigned,
            isOnlyForMaster;

//    public UnitEventsMaintain002Object(String eventYear, String sponsorUnitName, String eventCategoryName,
//                                       String eventName, String eventStartDate, String eventEndDate,
//                                       int masterEnrollCount, int notMasterEnrollCount, int peopleForecast,
//                                       int status, String eventNote, String eventId) {
//        this.eventYear = eventYear;
//        this.sponsorUnitName = sponsorUnitName;
//        this.eventCategoryName = eventCategoryName;
//        this.eventName = eventName;
//        this.eventStartDate = eventStartDate;
//        this.eventEndDate = eventEndDate;
//        this.masterEnrollCount = masterEnrollCount;
//        this.notMasterEnrollCount = notMasterEnrollCount;
//        this.peopleForecast = peopleForecast;
//        this.status = status;
//        this.eventNote = eventNote;
//        this.eventId = eventId;
//    }

    public UnitEventsMaintain002Object(String eventYear, String sponsorUnitName, String eventCategoryName, String eventCategoryColorCode,
                                       String eventName, Date eventStartDate, Date eventEndDate, Integer status,
                                       String eventNote, String eventId, String updatorName, Date updateDtTm) {
        this.eventYear = eventYear;
        this.sponsorUnitName = sponsorUnitName;
        this.eventCategoryName = eventCategoryName;
        this.eventCategoryColorCode = eventCategoryColorCode;
        this.eventName = eventName;
        this.eventStartDate = DateTime2DateString(eventStartDate);
        this.eventEndDate = DateTime2DateString(eventEndDate);
        this.status = status != null ? status : 0;
        this.eventNote = eventNote;
        this.eventId = eventId;
        this.updatorName = updatorName;
        this.updateDtTm = DateTime2DateTimeString(updateDtTm);
    }

    public UnitEventsMaintain002Object(String eventYear, String sponsorUnitName, String eventCategoryName, String eventCategoryColorCode,
                                       String eventName, Date eventStartDate, Date eventEndDate, Integer status,
                                       String eventNote, String eventId, String updatorName, Date updateDtTm,
                                       Boolean isNeedAbbotSigned, Boolean isOnlyForMaster) {
        this.eventYear = eventYear;
        this.sponsorUnitName = sponsorUnitName;
        this.eventCategoryName = eventCategoryName;
        this.eventCategoryColorCode = eventCategoryColorCode;
        this.eventName = eventName;
        this.eventStartDate = DateTime2DateString(eventStartDate);
        this.eventEndDate = DateTime2DateString(eventEndDate);
        this.status = status != null ? status : 0;
        this.eventNote = eventNote;
        this.eventId = eventId;
        this.updatorName = updatorName;
        this.updateDtTm = DateTime2DateTimeString(updateDtTm);
        this.isNeedAbbotSigned = isNeedAbbotSigned != null ? isNeedAbbotSigned : false;
        this.isOnlyForMaster = isOnlyForMaster != null ? isOnlyForMaster : false;
    }

    public String getEventYear() {
        return eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public String getSponsorUnitName() {
        return sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getEventCategoryName() {
        return eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public int getMasterEnrollCount() {
        return masterEnrollCount;
    }

    public void setMasterEnrollCount(int masterEnrollCount) {
        this.masterEnrollCount = masterEnrollCount;
    }

    public int getNotMasterEnrollCount() {
        return notMasterEnrollCount;
    }

    public void setNotMasterEnrollCount(int notMasterEnrollCount) {
        this.notMasterEnrollCount = notMasterEnrollCount;
    }

    public int getPeopleForecast() {
        return peopleForecast;
    }

    public void setPeopleForecast(int peopleForecast) {
        this.peopleForecast = peopleForecast;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getEventNote() {
        return eventNote;
    }

    public void setEventNote(String eventNote) {
        this.eventNote = eventNote;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

    public String getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(String updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public int getAbbotSignedCount() {
        return abbotSignedCount;
    }

    public void setAbbotSignedCount(int abbotSignedCount) {
        this.abbotSignedCount = abbotSignedCount;
    }

    public int getNotAbbotSignedCount() {
        return notAbbotSignedCount;
    }

    public void setNotAbbotSignedCount(int notAbbotSignedCount) {
        this.notAbbotSignedCount = notAbbotSignedCount;
    }

    public boolean getIsNeedAbbotSigned() {
        return isNeedAbbotSigned;
    }

    public void setIsNeedAbbotSigned(boolean needAbbotSigned) {
        isNeedAbbotSigned = needAbbotSigned;
    }

    public String getEventCategoryColorCode() {
        return eventCategoryColorCode;
    }

    public void setEventCategoryColorCode(String eventCategoryColorCode) {
        this.eventCategoryColorCode = eventCategoryColorCode;
    }

    public int getWaitCtApprovedCount() {
        return waitCtApprovedCount;
    }

    public void setWaitCtApprovedCount(int waitCtApprovedCount) {
        this.waitCtApprovedCount = waitCtApprovedCount;
    }

    public boolean getIsOnlyForMaster() {
        return isOnlyForMaster;
    }

    public void setIsOnlyForMaster(boolean onlyForMaster) {
        isOnlyForMaster = onlyForMaster;
    }
}
