package tw.org.ctworld.meditation.beans.MemberImport;

import java.util.List;

public class MemberImportRequest {
    private String updateType;
    private List<MemberImportResult> items;

    public MemberImportRequest(){}
    public MemberImportRequest(String updateType, List<MemberImportResult> items) {
        this.updateType = updateType;
        this.items = items;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    public List<MemberImportResult> getItems() {
        return items;
    }

    public void setItems(List<MemberImportResult> items) {
        this.items = items;
    }
}
