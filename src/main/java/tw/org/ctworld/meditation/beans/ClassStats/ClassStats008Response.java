package tw.org.ctworld.meditation.beans.ClassStats;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.Date;
import java.util.List;

public class ClassStats008Response extends BaseResponse {


    public static class ClassStats008_1Object {

        private String memberId, classPeriodNum, className, gender, aliasName, ctDharmaName, classGroupId, memberGroupNum,
                classStartDate, classEndDate;
        private boolean isFullAttended;
        private String classAttendedResult;
        private List<ClassStats008_2Object> attendRecordList;

        public ClassStats008_1Object(String memberId, String classPeriodNum, String className, String gender,
                                     String aliasName, String ctDharmaName, String classGroupId, String memberGroupNum,
                                     String classStartDate, String classEndDate, String classAttendedResult,
                                     boolean isFullAttended, List<ClassStats008_2Object> attendRecordList) {
            this.memberId = memberId;
            this.classPeriodNum = classPeriodNum;
            this.className = className;
            this.gender = gender;
            this.aliasName = aliasName;
            this.ctDharmaName = ctDharmaName;
            this.classGroupId = classGroupId;
            this.memberGroupNum = memberGroupNum;
            this.classStartDate = classStartDate;
            this.classEndDate = classEndDate;
            this.classAttendedResult = classAttendedResult;
            this.isFullAttended = isFullAttended;
            this.attendRecordList = attendRecordList;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getClassPeriodNum() {
            return classPeriodNum;
        }

        public void setClassPeriodNum(String classPeriodNum) {
            this.classPeriodNum = classPeriodNum;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAliasName() {
            return aliasName;
        }

        public void setAliasName(String aliasName) {
            this.aliasName = aliasName;
        }

        public String getCtDharmaName() {
            return ctDharmaName;
        }

        public void setCtDharmaName(String ctDharmaName) {
            this.ctDharmaName = ctDharmaName;
        }

        public String getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(String classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getMemberGroupNum() {
            return memberGroupNum;
        }

        public void setMemberGroupNum(String memberGroupNum) {
            this.memberGroupNum = memberGroupNum;
        }

        public String getClassStartDate() {
            return classStartDate;
        }

        public void setClassStartDate(String classStartDate) {
            this.classStartDate = classStartDate;
        }

        public String getClassEndDate() {
            return classEndDate;
        }

        public void setClassEndDate(String classEndDate) {
            this.classEndDate = classEndDate;
        }

        public String getClassAttendedResult() {
            return classAttendedResult;
        }

        public void setClassAttendedResult(String classAttendedResult) {
            this.classAttendedResult = classAttendedResult;
        }

        public boolean getIsFullAttended() {
            return isFullAttended;
        }

        public void setIsFullAttended(boolean fullAttended) {
            isFullAttended = fullAttended;
        }

        public List<ClassStats008_2Object> getAttendRecordList() {
            return attendRecordList;
        }

        public void setAttendRecordList(List<ClassStats008_2Object> attendRecordList) {
            this.attendRecordList = attendRecordList;
        }
    }

    public static class ClassStats008_2Object {

        private String attendStatus, color;

        public ClassStats008_2Object(String attendStatus, String color) {
            this.attendStatus = attendStatus;
            this.color = color;
        }

        public String getAttendStatus() {
            return attendStatus;
        }

        public void setAttendStatus(String attendStatus) {
            this.attendStatus = attendStatus;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }
    }
}
