package tw.org.ctworld.meditation.beans.SysCode;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.SysCode;

import java.util.List;

public class SysCodeResponse extends BaseResponse {

    List<SysCode> items;

    public SysCodeResponse(int errCode, String errMsg, List<SysCode> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<SysCode> getItems() {
        return items;
    }

    public void setItems(List<SysCode> items) {
        this.items = items;
    }
}
