package tw.org.ctworld.meditation.beans.ClassStats;

public class ClassStats003_1Object {

    private String classEnrollFormId, memberId, aliasName, ctDharmaName, gender, classPeriodNum, className, classGroupId,
            memberGroupNum, classJobTitleList;
    private boolean isFullAttended, isGraduated;
    private String classAttendedResult;
    private int age;

    public ClassStats003_1Object(String classEnrollFormId, String memberId, String aliasName, String ctDharmaName, String gender,
                                 String classPeriodNum, String className, String classGroupId, String memberGroupNum,
                                 String classJobTitleList, Boolean isFullAttended, Boolean isGraduated,
                                 String classAttendedResult, int age) {
        this.classEnrollFormId = classEnrollFormId;
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classJobTitleList = classJobTitleList;
        this.isFullAttended = isFullAttended != null ? isFullAttended : false;
        this.isGraduated = isGraduated != null ? isGraduated : false;
        this.classAttendedResult = classAttendedResult;
        this.age = age;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public boolean getIsFullAttended() {
        return isFullAttended;
    }

    public void setIsFullAttended(boolean fullAttended) {
        isFullAttended = fullAttended;
    }

    public boolean getIsGraduated() {
        return isGraduated;
    }

    public void setIsGraduated(boolean graduated) {
        isGraduated = graduated;
    }

    public String getClassAttendedResult() {
        return classAttendedResult;
    }

    public void setClassAttendedResult(String classAttendedResult) {
        this.classAttendedResult = classAttendedResult;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
