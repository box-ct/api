package tw.org.ctworld.meditation.beans.Common;

public class Common012_1Object {

    private String classGroupId;
    private int f, m;

    public Common012_1Object(String classGroupId, int f, int m) {
        this.classGroupId = classGroupId;
        this.f = f;
        this.m = m;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }
}
