package tw.org.ctworld.meditation.beans.MemberImport;

import tw.org.ctworld.meditation.libs.CommonUtils;
import java.util.Date;

public class MemberCompareItem {
    private String memberId;
    private String aliasName;
    private String fullName;
    private String twIdNum;
    private String birthDate;

    public MemberCompareItem(){}
    public MemberCompareItem(String memberId, String aliasName, String fullName, String twIdNum, Date birthDate) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.fullName = fullName;
        this.twIdNum = twIdNum;
        this.birthDate =  CommonUtils.DateTime2DateString(birthDate);
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}
