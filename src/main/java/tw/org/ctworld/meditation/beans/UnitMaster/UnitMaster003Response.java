package tw.org.ctworld.meditation.beans.UnitMaster;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.UnitMasterInfo;

public class UnitMaster003Response  extends BaseResponse{
    private UnitMasterInfo result;

    public UnitMaster003Response(int errCode, String errMsg, UnitMasterInfo result) {
        super(errCode, errMsg);
        this.result = result;
    }

    public UnitMasterInfo getResult() {
        return result;
    }

    public void setResult(UnitMasterInfo result) {
        this.result = result;
    }
}
