package tw.org.ctworld.meditation.beans.CtEventMaintain;

public class CtEventMaintain017Request {

    private String sInputId, sInputName, sInputExample, sInputCategory, sInputWriteType;
    private Integer sInputOrderNum;
    private Boolean isRequired;

    public String getsInputId() {
        return sInputId;
    }

    public void setsInputId(String sInputId) {
        this.sInputId = sInputId;
    }

    public String getsInputName() {
        return sInputName;
    }

    public void setsInputName(String sInputName) {
        this.sInputName = sInputName;
    }

    public String getsInputExample() {
        return sInputExample;
    }

    public void setsInputExample(String sInputExample) {
        this.sInputExample = sInputExample;
    }

    public String getsInputCategory() {
        return sInputCategory;
    }

    public void setsInputCategory(String sInputCategory) {
        this.sInputCategory = sInputCategory;
    }

    public String getsInputWriteType() {
        return sInputWriteType;
    }

    public void setsInputWriteType(String sInputWriteType) {
        this.sInputWriteType = sInputWriteType;
    }

    public Integer getsInputOrderNum() {
        return sInputOrderNum;
    }

    public void setsInputOrderNum(Integer sInputOrderNum) {
        this.sInputOrderNum = sInputOrderNum;
    }

    public Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean required) {
        isRequired = required;
    }
}
