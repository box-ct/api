package tw.org.ctworld.meditation.beans.UnitStartUp;

public class UnitStartUp002_2Object {

    private String eventIs;
    private Boolean isMaster, isNeedAbbotSigned;

    public UnitStartUp002_2Object(String eventIs, Boolean isMaster, Boolean isNeedAbbotSigned) {
        this.eventIs = eventIs;
        this.isMaster = isMaster;
        this.isNeedAbbotSigned = isNeedAbbotSigned;
    }

    public String getEventIs() {
        return eventIs;
    }

    public void setEventIs(String eventIs) {
        this.eventIs = eventIs;
    }

    public Boolean getIsMaster() {
        return isMaster;
    }

    public void setIsMaster(Boolean master) {
        isMaster = master;
    }

    public Boolean getIsNeedAbbotSigned() {
        return isNeedAbbotSigned;
    }

    public void setIsNeedAbbotSigned(Boolean needAbbotSigned) {
        isNeedAbbotSigned = needAbbotSigned;
    }
}
