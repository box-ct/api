package tw.org.ctworld.meditation.beans.Anonymous;

import java.util.Date;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;

public class Anonymous005_1Object {

    private String memberId, aliasName, ctDharmaName, twIdNum, birthDate, mobileNum1, mobileNum2, homePhoneNum1, homePhoneNum2;

    public Anonymous005_1Object(String memberId, String aliasName, String ctDharmaName, String twIdNum,
                                Date birthDate, String mobileNum1, String mobileNum2,
                                String homePhoneNum1, String homePhoneNum2) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.twIdNum = twIdNum;
        this.birthDate = DateTime2DateString(birthDate);
        this.mobileNum1 = mobileNum1;
        this.mobileNum2 = mobileNum2;
        this.homePhoneNum1 = homePhoneNum1;
        this.homePhoneNum2 = homePhoneNum2;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getTwIdNum() {
        return twIdNum == null ? "" : twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getBirthDate() {
        return birthDate == null ? "" : birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getMobileNum1() {
        return mobileNum1 == null ? "" : mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getMobileNum2() {
        return mobileNum2 == null ? "" : mobileNum2;
    }

    public void setMobileNum2(String mobileNum2) {
        this.mobileNum2 = mobileNum2;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1 == null ? "" : homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getHomePhoneNum2() {
        return homePhoneNum2 == null ? "" : homePhoneNum2;
    }

    public void setHomePhoneNum2(String homePhoneNum2) {
        this.homePhoneNum2 = homePhoneNum2;
    }
}
