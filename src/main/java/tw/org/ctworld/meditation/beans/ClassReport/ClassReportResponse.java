package tw.org.ctworld.meditation.beans.ClassReport;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class ClassReportResponse extends BaseResponse {
    private List<ClassMemberRecord> items;

    public ClassReportResponse() {
    }

    public ClassReportResponse(int errCode, String errMsg, List<ClassMemberRecord> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassMemberRecord> getItems() {
        return items;
    }

    public void setItems(List<ClassMemberRecord> items) {
        this.items = items;
    }
}
