package tw.org.ctworld.meditation.beans.Member;

public class Members002ImagesObject {

    private String memberId, photo, id1, id2, data, agree;

    public Members002ImagesObject(String photo, String id1, String id2, String data, String agree) {
        this.photo = photo;
        this.id1 = id1;
        this.id2 = id2;
        this.data = data;
        this.agree = agree;
    }

    public Members002ImagesObject(String memberId, String photo, String id1, String id2, String data, String agree) {
        this.memberId = memberId;
        this.photo = photo;
        this.id1 = id1;
        this.id2 = id2;
        this.data = data;
        this.agree = agree;
    }

    public Members002ImagesObject(String memberId, String photo) {
        this.memberId = memberId;
        this.photo = photo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getId1() {
        return id1;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }

    public String getId2() {
        return id2;
    }

    public void setId2(String id2) {
        this.id2 = id2;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getAgree() {
        return agree;
    }

    public void setAgree(String agree) {
        this.agree = agree;
    }
}
