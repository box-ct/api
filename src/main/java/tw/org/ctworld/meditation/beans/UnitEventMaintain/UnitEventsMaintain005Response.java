package tw.org.ctworld.meditation.beans.UnitEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.EventMain;

public class UnitEventsMaintain005Response extends BaseResponse {

    private EventMain result;

    public UnitEventsMaintain005Response(int errCode, String errMsg, EventMain result) {
        super(errCode, errMsg);
        this.result = result;
    }

    public EventMain getResult() {
        return result;
    }

    public void setResult(EventMain result) {
        this.result = result;
    }
}
