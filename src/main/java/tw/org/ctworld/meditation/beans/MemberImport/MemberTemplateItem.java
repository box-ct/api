package tw.org.ctworld.meditation.beans.MemberImport;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;

public class MemberTemplateItem {
    private String memberId;
    private String gender;
    private String aliasName;
    private String engLastName;
    private String engFirstName;
    private String ctDharmaName;
    private String ctRefugeMaster;
    private String birthDate;
    private String twIdNum;
    private String passportNum;
    private String homePhoneNum1;
    private String mobileNum1;
    private String officePhoneNum1;
    private String email;
    private String skillList;
    private String healthList;
    private String mailingZip;
    private String mailingCountry;
    private String mailingState;
    private String mailingCity;
    private String mailingStreet;
    private String birthCountry;
    private String nationality;
    private String urgentContactPersonName1;
    private String urgentContactPersonRelationship1;
    private String urgentContactPersonPhoneNum1;
    private String introducerName;
    private String introducerRelationship;
    private String introducerPhoneNum;
    private String okSendEletter;
    private String okSendMagazine;
    private String okSendMessage;
    private String companyName;
    private String companyJobTitle;
    private String schoolDegree;
    private String schoolName;
    private String schoolMajor;
    private String clubName;
    private String clubJobTitleNow;
    private String clubJobTitlePrev;
    private String parentsName;
    private String parentsPhoneNum;
    private String parentsJobTitle;
    private String parentsEmployer;
    private String dataUnitMemberId;
    private String dataSentToCTDate;
    private String classList;
    private String isTakeRefuge;
    private String RefugeDate;
    private String isTakePrecept5;
    private String Precept5Date;
    private String isTakeBodhiPrecept;
    private String BodhiPreceptDate;
    private String donationNameList;
    private String dsaJobNameList;
    private int idx;

    public MemberTemplateItem() {
    }

    public MemberTemplateItem(String memberId, String gender, String aliasName, String engLastName, String engFirstName,
                              String ctDharmaName, String ctRefugeMaster, Date birthDate, String twIdNum,
                              String passportNum, String homePhoneNum1, String mobileNum1, String officePhoneNum1,
                              String email, String skillList, String healthList, String mailingZip,
                              String mailingCountry, String mailingState, String mailingCity, String mailingStreet,
                              String birthCountry, String nationality, String urgentContactPersonName1,
                              String urgentContactPersonRelationship1, String urgentContactPersonPhoneNum1,
                              String introducerName, String introducerRelationship, String introducerPhoneNum,
                              Boolean okSendEletter, Boolean okSendMagazine, Boolean okSendMessage, String companyName,
                              String companyJobTitle, String schoolDegree, String schoolName, String schoolMajor,
                              String clubName, String clubJobTitleNow, String clubJobTitlePrev, String parentsName,
                              String parentsPhoneNum, String parentsJobTitle, String parentsEmployer,
                              String dataUnitMemberId, Date dataSentToCTDate, String classList, Boolean isTakeRefuge,
                              String refugeDate, Boolean isTakePrecept5, String precept5Date, Boolean isTakeBodhiPrecept,
                              String bodhiPreceptDate, String donationNameList, String dsaJobNameList) {
        this.memberId = memberId; // 0
        this.gender =gender.toUpperCase();// (gender.equals("M")||gender.equals("男"))?"男":(gender.equals("F")||gender.equals("女"))?"女":"";
        this.aliasName = aliasName;
        this.engLastName = engLastName;
        this.engFirstName = engFirstName;
        this.ctDharmaName = ctDharmaName;
        this.ctRefugeMaster = ctRefugeMaster;
        this.birthDate =  CommonUtils.DateTime2DateString(birthDate);
        this.twIdNum = twIdNum;
        this.passportNum = passportNum;
        this.homePhoneNum1 = homePhoneNum1; // 10
        this.mobileNum1 = mobileNum1;
        this.officePhoneNum1 = officePhoneNum1;
        this.email = email;
        this.skillList = skillList;
        this.healthList = healthList;
        this.mailingZip = mailingZip;
        this.mailingCountry = mailingCountry;
        this.mailingState = mailingState;
        this.mailingCity = mailingCity;
        this.mailingStreet = mailingStreet; // 20
        this.birthCountry = birthCountry;
        this.nationality = nationality;
        this.urgentContactPersonName1 = urgentContactPersonName1;
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
        this.introducerName = introducerName;
        this.introducerRelationship = introducerRelationship;
        this.introducerPhoneNum = introducerPhoneNum;
        this.okSendEletter = (okSendEletter!=null && okSendEletter)?"Y":"";
        this.okSendMagazine = (okSendMagazine!=null && okSendMagazine)?"Y":""; // 30
        this.okSendMessage = (okSendMessage!=null && okSendMessage)?"Y":"";
        this.companyName = companyName;
        this.companyJobTitle = companyJobTitle;
        this.schoolDegree = schoolDegree;
        this.schoolName = schoolName;
        this.schoolMajor = schoolMajor;
        this.clubName = clubName;
        this.clubJobTitleNow = clubJobTitleNow;
        this.clubJobTitlePrev = clubJobTitlePrev;
        this.parentsName = parentsName; // 40
        this.parentsPhoneNum = parentsPhoneNum;
        this.parentsJobTitle = parentsJobTitle;
        this.parentsEmployer = parentsEmployer;
        this.dataUnitMemberId = dataUnitMemberId;
        this.dataSentToCTDate = CommonUtils.DateTime2DateString(dataSentToCTDate);
        this.classList = classList;
        this.isTakeRefuge = (isTakeRefuge!=null && isTakeRefuge)?"Y":"";
        this.RefugeDate = refugeDate;
        this.isTakePrecept5 = (isTakePrecept5!=null && isTakePrecept5)?"Y":"";
        this.Precept5Date = precept5Date; // 50
        this.isTakeBodhiPrecept = (isTakeBodhiPrecept!=null && isTakeBodhiPrecept)?"Y":"";
        this.BodhiPreceptDate = bodhiPreceptDate;
        this.donationNameList = donationNameList;
        this.dsaJobNameList = dsaJobNameList;
    }
    public MemberTemplateItem(String memberId, String gender, String aliasName, String engLastName, String engFirstName,
                              String ctDharmaName, String ctRefugeMaster, String birthDate, String twIdNum,
                              String passportNum, String homePhoneNum1, String mobileNum1, String officePhoneNum1,
                              String email, String skillList, String healthList, String mailingZip,
                              String mailingCountry, String mailingState, String mailingCity, String mailingStreet,
                              String birthCountry, String nationality, String urgentContactPersonName1,
                              String urgentContactPersonRelationship1, String urgentContactPersonPhoneNum1,
                              String introducerName, String introducerRelationship, String introducerPhoneNum,
                              String okSendEletter, String okSendMagazine, String okSendMessage, String companyName,
                              String companyJobTitle, String schoolDegree, String schoolName, String schoolMajor,
                              String clubName, String clubJobTitleNow, String clubJobTitlePrev, String parentsName,
                              String parentsPhoneNum, String parentsJobTitle, String parentsEmployer,
                              String dataUnitMemberId, String dataSentToCTDate, String classList, String isTakeRefuge,
                              String refugeDate, String isTakePrecept5, String precept5Date, String isTakeBodhiPrecept,
                              String bodhiPreceptDate, String donationNameList, String dsaJobNameList) {
        this.memberId = memberId; // 0
        this.gender =gender.toUpperCase();// (gender.equals("M")||gender.equals("男"))?"男":(gender.equals("F")||gender.equals("女"))?"女":"";
        this.aliasName = aliasName;
        this.engLastName = engLastName;
        this.engFirstName = engFirstName;
        this.ctDharmaName = ctDharmaName;
        this.ctRefugeMaster = ctRefugeMaster;
        this.birthDate =  birthDate;
        this.twIdNum = twIdNum;
        this.passportNum = passportNum;
        this.homePhoneNum1 = homePhoneNum1; // 10
        this.mobileNum1 = mobileNum1;
        this.officePhoneNum1 = officePhoneNum1;
        this.email = email;
        this.skillList = skillList;
        this.healthList = healthList;
        this.mailingZip = mailingZip;
        this.mailingCountry = mailingCountry;
        this.mailingState = mailingState;
        this.mailingCity = mailingCity;
        this.mailingStreet = mailingStreet; // 20
        this.birthCountry = birthCountry;
        this.nationality = nationality;
        this.urgentContactPersonName1 = urgentContactPersonName1;
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
        this.introducerName = introducerName;
        this.introducerRelationship = introducerRelationship;
        this.introducerPhoneNum = introducerPhoneNum;
        this.okSendEletter = okSendEletter;
        this.okSendMagazine = okSendMagazine;
        this.okSendMessage = okSendMessage;
        this.companyName = companyName;
        this.companyJobTitle = companyJobTitle;
        this.schoolDegree = schoolDegree;
        this.schoolName = schoolName;
        this.schoolMajor = schoolMajor;
        this.clubName = clubName;
        this.clubJobTitleNow = clubJobTitleNow;
        this.clubJobTitlePrev = clubJobTitlePrev;
        this.parentsName = parentsName; // 40
        this.parentsPhoneNum = parentsPhoneNum;
        this.parentsJobTitle = parentsJobTitle;
        this.parentsEmployer = parentsEmployer;
        this.dataUnitMemberId = dataUnitMemberId;
        this.dataSentToCTDate = dataSentToCTDate;
        this.classList = classList;
        this.isTakeRefuge = isTakeRefuge;
        this.RefugeDate = refugeDate;
        this.isTakePrecept5 = isTakePrecept5;
        this.Precept5Date = precept5Date; // 50
        this.isTakeBodhiPrecept = isTakeBodhiPrecept;
        this.BodhiPreceptDate = bodhiPreceptDate;
        this.donationNameList = donationNameList;
        this.dsaJobNameList = dsaJobNameList;
    }
    public String getIsTakeRefuge() {
        return isTakeRefuge;
    }

    public void setIsTakeRefuge(String isTakeRefuge) {
        this.isTakeRefuge = isTakeRefuge;
    }

    public String getIsTakePrecept5() {
        return isTakePrecept5;
    }

    public void setIsTakePrecept5(String isTakePrecept5) {
        this.isTakePrecept5 = isTakePrecept5;
    }

    public String getIsTakeBodhiPrecept() {
        return isTakeBodhiPrecept;
    }

    public void setIsTakeBodhiPrecept(String isTakeBodhiPrecept) {
        this.isTakeBodhiPrecept = isTakeBodhiPrecept;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getEngLastName() {
        return engLastName;
    }

    public void setEngLastName(String engLastName) {
        this.engLastName = engLastName;
    }

    public String getEngFirstName() {
        return engFirstName;
    }

    public void setEngFirstName(String engFirstName) {
        this.engFirstName = engFirstName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getCtRefugeMaster() {
        return ctRefugeMaster;
    }

    public void setCtRefugeMaster(String ctRefugeMaster) {
        this.ctRefugeMaster = ctRefugeMaster;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getOfficePhoneNum1() {
        return officePhoneNum1;
    }

    public void setOfficePhoneNum1(String officePhoneNum1) {
        this.officePhoneNum1 = officePhoneNum1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSkillList() {
        return skillList;
    }

    public void setSkillList(String skillList) {
        this.skillList = skillList;
    }

    public String getHealthList() {
        return healthList;
    }

    public void setHealthList(String healthList) {
        this.healthList = healthList;
    }

    public String getMailingZip() {
        return mailingZip;
    }

    public void setMailingZip(String mailingZip) {
        this.mailingZip = mailingZip;
    }

    public String getMailingCountry() {
        return mailingCountry;
    }

    public void setMailingCountry(String mailingCountry) {
        this.mailingCountry = mailingCountry;
    }

    public String getMailingState() {
        return mailingState;
    }

    public void setMailingState(String mailingState) {
        this.mailingState = mailingState;
    }

    public String getMailingCity() {
        return mailingCity;
    }

    public void setMailingCity(String mailingCity) {
        this.mailingCity = mailingCity;
    }

    public String getMailingStreet() {
        return mailingStreet;
    }

    public void setMailingStreet(String mailingStreet) {
        this.mailingStreet = mailingStreet;
    }

    public String getBirthCountry() {
        return birthCountry;
    }

    public void setBirthCountry(String birthCountry) {
        this.birthCountry = birthCountry;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getUrgentContactPersonName1() {
        return urgentContactPersonName1;
    }

    public void setUrgentContactPersonName1(String urgentContactPersonName1) {
        this.urgentContactPersonName1 = urgentContactPersonName1;
    }

    public String getUrgentContactPersonRelationship1() {
        return urgentContactPersonRelationship1;
    }

    public void setUrgentContactPersonRelationship1(String urgentContactPersonRelationship1) {
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
    }

    public String getUrgentContactPersonPhoneNum1() {
        return urgentContactPersonPhoneNum1;
    }

    public void setUrgentContactPersonPhoneNum1(String urgentContactPersonPhoneNum1) {
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
    }

    public String getIntroducerName() {
        return introducerName;
    }

    public void setIntroducerName(String introducerName) {
        this.introducerName = introducerName;
    }

    public String getIntroducerRelationship() {
        return introducerRelationship;
    }

    public void setIntroducerRelationship(String introducerRelationship) {
        this.introducerRelationship = introducerRelationship;
    }

    public String getIntroducerPhoneNum() {
        return introducerPhoneNum;
    }

    public void setIntroducerPhoneNum(String introducerPhoneNum) {
        this.introducerPhoneNum = introducerPhoneNum;
    }

    public String getOkSendEletter() {
        return okSendEletter;
    }

    public void setOkSendEletter(String okSendEletter) {
        this.okSendEletter = okSendEletter;
    }

    public String getOkSendMagazine() {
        return okSendMagazine;
    }

    public void setOkSendMagazine(String okSendMagazine) {
        this.okSendMagazine = okSendMagazine;
    }

    public String getOkSendMessage() {
        return okSendMessage;
    }

    public void setOkSendMessage(String okSendMessage) {
        this.okSendMessage = okSendMessage;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyJobTitle() {
        return companyJobTitle;
    }

    public void setCompanyJobTitle(String companyJobTitle) {
        this.companyJobTitle = companyJobTitle;
    }

    public String getSchoolDegree() {
        return schoolDegree;
    }

    public void setSchoolDegree(String schoolDegree) {
        this.schoolDegree = schoolDegree;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolMajor() {
        return schoolMajor;
    }

    public void setSchoolMajor(String schoolMajor) {
        this.schoolMajor = schoolMajor;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getClubJobTitleNow() {
        return clubJobTitleNow;
    }

    public void setClubJobTitleNow(String clubJobTitleNow) {
        this.clubJobTitleNow = clubJobTitleNow;
    }

    public String getClubJobTitlePrev() {
        return clubJobTitlePrev;
    }

    public void setClubJobTitlePrev(String clubJobTitlePrev) {
        this.clubJobTitlePrev = clubJobTitlePrev;
    }

    public String getParentsName() {
        return parentsName;
    }

    public void setParentsName(String parentsName) {
        this.parentsName = parentsName;
    }

    public String getParentsPhoneNum() {
        return parentsPhoneNum;
    }

    public void setParentsPhoneNum(String parentsPhoneNum) {
        this.parentsPhoneNum = parentsPhoneNum;
    }

    public String getParentsJobTitle() {
        return parentsJobTitle;
    }

    public void setParentsJobTitle(String parentsJobTitle) {
        this.parentsJobTitle = parentsJobTitle;
    }

    public String getParentsEmployer() {
        return parentsEmployer;
    }

    public void setParentsEmployer(String parentsEmployer) {
        this.parentsEmployer = parentsEmployer;
    }

    public String getDataUnitMemberId() {
        return dataUnitMemberId;
    }

    public void setDataUnitMemberId(String dataUnitMemberId) {
        this.dataUnitMemberId = dataUnitMemberId;
    }

    public String getDataSentToCTDate() {
        return dataSentToCTDate;
    }

    public void setDataSentToCTDate(String dataSentToCTDate) {
        this.dataSentToCTDate = dataSentToCTDate;
    }

    public String getClassList() {
        return classList;
    }

    public void setClassList(String classList) {
        this.classList = classList;
    }

    public String getTakeRefuge() {
        return isTakeRefuge;
    }

    public void setTakeRefuge(String takeRefuge) {
        isTakeRefuge = takeRefuge;
    }

    public String getRefugeDate() {
        return RefugeDate;
    }

    public void setRefugeDate(String refugeDate) {
        RefugeDate = refugeDate;
    }

    public String getTakePrecept5() {
        return isTakePrecept5;
    }

    public void setTakePrecept5(String takePrecept5) {
        isTakePrecept5 = takePrecept5;
    }

    public String getPrecept5Date() {
        return Precept5Date;
    }

    public void setPrecept5Date(String precept5Date) {
        Precept5Date = precept5Date;
    }

    public String getTakeBodhiPrecept() {
        return isTakeBodhiPrecept;
    }

    public void setTakeBodhiPrecept(String takeBodhiPrecept) {
        isTakeBodhiPrecept = takeBodhiPrecept;
    }

    public String getBodhiPreceptDate() {
        return BodhiPreceptDate;
    }

    public void setBodhiPreceptDate(String bodhiPreceptDate) {
        BodhiPreceptDate = bodhiPreceptDate;
    }

    public String getDonationNameList() {
        return donationNameList;
    }

    public void setDonationNameList(String donationNameList) {
        this.donationNameList = donationNameList;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }
}
