package tw.org.ctworld.meditation.beans.DataReport;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class DataReport011Response extends BaseResponse{

    private List<String>[] items;

    public DataReport011Response(int errCode, String errMsg, List<String>[] items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<String>[] getItems() {
        return items;
    }

    public void setItems(List<String>[] items) {
        this.items = items;
    }
}
