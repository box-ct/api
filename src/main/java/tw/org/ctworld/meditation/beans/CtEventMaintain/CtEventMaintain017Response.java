package tw.org.ctworld.meditation.beans.CtEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class CtEventMaintain017Response extends BaseResponse {

    private CtEventMaintain017Request result;

    public CtEventMaintain017Response(int errCode, String errMsg, CtEventMaintain017Request result) {
        super(errCode, errMsg);
        this.result = result;
    }

    public CtEventMaintain017Request getResult() {
        return result;
    }

    public void setResult(CtEventMaintain017Request result) {
        this.result = result;
    }
}
