package tw.org.ctworld.meditation.beans.Unit;

import java.util.ArrayList;
import java.util.List;

public class Unit003_2Object {
    private String UnitId;
    private String UnitName;
    private List<Unit003_2Object> SubUnit = new ArrayList<Unit003_2Object>();

    public Unit003_2Object(String unitId, String unitName) {
        UnitId = unitId;
        UnitName = unitName;
    }

    public String getUnitId() {
        return UnitId;
    }

    public void setUnitId(String unitId) {
        UnitId = unitId;
    }

    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }

    public List<Unit003_2Object> getSubUnit() {
        return SubUnit;
    }

    public void addSubUnit(Unit003_2Object subUnit) {
        if (SubUnit == null) {
            SubUnit = new ArrayList<Unit003_2Object>();
        }

        SubUnit.add(subUnit);
    }

    public boolean hasSubUnit() {
        if (SubUnit == null) {
            return false;
        } else {
            return true;
        }
    }
}
