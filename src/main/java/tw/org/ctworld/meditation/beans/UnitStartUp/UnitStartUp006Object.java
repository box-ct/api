package tw.org.ctworld.meditation.beans.UnitStartUp;

public class UnitStartUp006Object {

    private Boolean isEnroll;
    private String masterId, masterName, jobTitle;
    private Boolean isAbbot, isTreasurer;
    private String mobileNum;

    public UnitStartUp006Object(Boolean isEnroll, String masterId, String masterName, String jobTitle,
                                Boolean isAbbot, Boolean isTreasurer, String mobileNum) {
        this.isEnroll = isEnroll;
        this.masterId = masterId;
        this.masterName = masterName;
        this.jobTitle = jobTitle;
        this.isAbbot = isAbbot;
        this.isTreasurer = isTreasurer;
        this.mobileNum = mobileNum;
    }

    public Boolean getIsEnroll() {
        return isEnroll;
    }

    public void setIsEnroll(Boolean isEnroll) {
        this.isEnroll = isEnroll;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Boolean getIsAbbot() {
        return isAbbot;
    }

    public void setIsAbbot(Boolean isAbbot) {
        this.isAbbot = isAbbot;
    }

    public Boolean getIsTreasurer() {
        return isTreasurer;
    }

    public void setIsTreasurer(Boolean isTreasurer) {
        this.isTreasurer = isTreasurer;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }
}
