package tw.org.ctworld.meditation.beans.Makeup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tw.org.ctworld.meditation.controllers.SysCodeController;
import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;

public class MakeupClassInfo {
    private String year;
    private String unitName;
    private String classId;
    private String classStatus;
    private String classStartDate;
    private String classEndDate;
    private String dayOfWeek;
    private String classDayOrNight;
    private String classTypeName;
    private String classPeriodNum;
    private String className;
    private String classDesc;

    private int classTotalNum;
    private int classFileTotalNum;
    private int classFilePlayTotalNum;
    private int classFileSizeTotalNum;

    private static final Logger logger = LoggerFactory.getLogger(MakeupClassInfo.class);

    public MakeupClassInfo(String year, String unitName, String classId, String classStatus, Date classStartDate, Date classEndDate, String dayOfWeek, String classDayOrNight, String classTypeName, String classPeriodNum, String className, String classDesc, String classTotalNum) {
        this.year = year;
        this.unitName = unitName;
        this.classId = classId;
        this.classStatus = classStatus;
        this.classStartDate = CommonUtils.DateTime2DateString(classStartDate);
        this.classEndDate = CommonUtils.DateTime2DateString(classEndDate);
        this.dayOfWeek = dayOfWeek;
        this.classDayOrNight = classDayOrNight;
        this.classTypeName = classTypeName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classDesc = classDesc;

        try {
            this.classTotalNum = Integer.parseInt(classTotalNum);
        } catch (NumberFormatException e) {
            this.classTotalNum = 0;
        }
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassStatus() {
        return classStatus;
    }

    public void setClassStatus(String classStatus) {
        this.classStatus = classStatus;
    }

    public String getClassStartDate() {
        return classStartDate;
    }

    public void setClassStartDate(String classStartDate) {
        this.classStartDate = classStartDate;
    }

    public String getClassEndDate() {
        return classEndDate;
    }

    public void setClassEndDate(String classEndDate) {
        this.classEndDate = classEndDate;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getClassDayOrNight() {
        return classDayOrNight;
    }

    public void setClassDayOrNight(String classDayOrNight) {
        this.classDayOrNight = classDayOrNight;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassDesc() {
        return classDesc;
    }

    public void setClassDesc(String classDesc) {
        this.classDesc = classDesc;
    }

    public int getClassTotalNum() {
        return classTotalNum;
    }

    public void setClassTotalNum(int classTotalNum) {
        this.classTotalNum = classTotalNum;
    }

    public int getClassFileTotalNum() {
        return classFileTotalNum;
    }

    public void setClassFileTotalNum(int classFileTotalNum) {
        this.classFileTotalNum = classFileTotalNum;
    }

    public int getClassFilePlayTotalNum() {
        return classFilePlayTotalNum;
    }

    public void setClassFilePlayTotalNum(int classFilePlayTotalNum) {
        this.classFilePlayTotalNum = classFilePlayTotalNum;
    }

    public int getClassFileSizeTotalNum() {
        return classFileSizeTotalNum;
    }

    public void setClassFileSizeTotalNum(int classFileSizeTotalNum) {
        this.classFileSizeTotalNum = classFileSizeTotalNum;
    }
}
