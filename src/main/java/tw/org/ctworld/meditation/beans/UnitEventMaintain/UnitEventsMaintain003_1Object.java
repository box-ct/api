package tw.org.ctworld.meditation.beans.UnitEventMaintain;

public class UnitEventsMaintain003_1Object {

    private String eventName, eventCategoryId, eventCategoryName, eventId;
    private int status;
    private String eventStartDate, eventEndDate, eventStartTime, eventEndTime, eventPlace, eventCreatorUnitId,
            eventCreatorUnitName, sponsorUnitName, sponsorUnitId, idType1List, idType2List, enrollEndDate,
            changeEndDate, finalDate;
    private Boolean isSyncCtTable, isShowSummerDonationButton;
    private String summerDonationType;
    private Boolean isNeedCtApproved, isNeedAbbotSigned, isOnlyForMaster, isMultipleEnroll, isShowPrecept8Tab,
            isShowZen7Tab, isShowRefugeTab, isShowPrecept5Tab, isShowBodhiPreceptTab, isAvailableForAllUnit;
    private String attendUnitList, noTimeLimitUnitList, eventNote;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventCategoryId() {
        return eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventPlace() {
        return eventPlace;
    }

    public void setEventPlace(String eventPlace) {
        this.eventPlace = eventPlace;
    }

    public String getEventCreatorUnitId() {
        return eventCreatorUnitId;
    }

    public void setEventCreatorUnitId(String eventCreatorUnitId) {
        this.eventCreatorUnitId = eventCreatorUnitId;
    }

    public String getEventCreatorUnitName() {
        return eventCreatorUnitName;
    }

    public void setEventCreatorUnitName(String eventCreatorUnitName) {
        this.eventCreatorUnitName = eventCreatorUnitName;
    }

    public String getSponsorUnitName() {
        return sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getSponsorUnitId() {
        return sponsorUnitId;
    }

    public void setSponsorUnitId(String sponsorUnitId) {
        this.sponsorUnitId = sponsorUnitId;
    }

    public String getIdType1List() {
        return idType1List;
    }

    public void setIdType1List(String idType1List) {
        this.idType1List = idType1List;
    }

    public String getIdType2List() {
        return idType2List;
    }

    public void setIdType2List(String idType2List) {
        this.idType2List = idType2List;
    }

    public String getEnrollEndDate() {
        return enrollEndDate;
    }

    public void setEnrollEndDate(String enrollEndDate) {
        this.enrollEndDate = enrollEndDate;
    }

    public String getChangeEndDate() {
        return changeEndDate;
    }

    public void setChangeEndDate(String changeEndDate) {
        this.changeEndDate = changeEndDate;
    }

    public String getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(String finalDate) {
        this.finalDate = finalDate;
    }

    public Boolean getIsSyncCtTable() {
        return isSyncCtTable;
    }

    public void setIsSyncCtTable(Boolean syncCtTable) {
        isSyncCtTable = syncCtTable;
    }

    public Boolean getIsShowSummerDonationButton() {
        return isShowSummerDonationButton;
    }

    public void setIsShowSummerDonationButton(Boolean showSummerDonationButton) {
        isShowSummerDonationButton = showSummerDonationButton;
    }

    public String getSummerDonationType() {
        return summerDonationType;
    }

    public void setSummerDonationType(String summerDonationType) {
        this.summerDonationType = summerDonationType;
    }

    public Boolean getIsNeedCtApproved() {
        return isNeedCtApproved;
    }

    public void setIsNeedCtApproved(Boolean needCtApproved) {
        isNeedCtApproved = needCtApproved;
    }

    public Boolean getIsNeedAbbotSigned() {
        return isNeedAbbotSigned;
    }

    public void setIsNeedAbbotSigned(Boolean needAbbotSigned) {
        isNeedAbbotSigned = needAbbotSigned;
    }

    public Boolean getIsOnlyForMaster() {
        return isOnlyForMaster;
    }

    public void setIsOnlyForMaster(Boolean onlyForMaster) {
        isOnlyForMaster = onlyForMaster;
    }

    public Boolean getIsMultipleEnroll() {
        return isMultipleEnroll;
    }

    public void setIsMultipleEnroll(Boolean multipleEnroll) {
        isMultipleEnroll = multipleEnroll;
    }

    public Boolean getIsShowPrecept8Tab() {
        return isShowPrecept8Tab;
    }

    public void setIsShowPrecept8Tab(Boolean showPrecept8Tab) {
        isShowPrecept8Tab = showPrecept8Tab;
    }

    public Boolean getIsShowZen7Tab() {
        return isShowZen7Tab;
    }

    public void setIsShowZen7Tab(Boolean showZen7Tab) {
        isShowZen7Tab = showZen7Tab;
    }

    public Boolean getIsShowRefugeTab() {
        return isShowRefugeTab;
    }

    public void setIsShowRefugeTab(Boolean showRefugeTab) {
        isShowRefugeTab = showRefugeTab;
    }

    public Boolean getIsShowPrecept5Tab() {
        return isShowPrecept5Tab;
    }

    public void setIsShowPrecept5Tab(Boolean showPrecept5Tab) {
        isShowPrecept5Tab = showPrecept5Tab;
    }

    public Boolean getIsShowBodhiPreceptTab() {
        return isShowBodhiPreceptTab;
    }

    public void setIsShowBodhiPreceptTab(Boolean showBodhiPreceptTab) {
        isShowBodhiPreceptTab = showBodhiPreceptTab;
    }

    public Boolean getIsAvailableForAllUnit() {
        return isAvailableForAllUnit;
    }

    public void setIsAvailableForAllUnit(Boolean availableForAllUnit) {
        isAvailableForAllUnit = availableForAllUnit;
    }

    public String getAttendUnitList() {
        return attendUnitList;
    }

    public void setAttendUnitList(String attendUnitList) {
        this.attendUnitList = attendUnitList;
    }

    public String getNoTimeLimitUnitList() {
        return noTimeLimitUnitList;
    }

    public void setNoTimeLimitUnitList(String noTimeLimitUnitList) {
        this.noTimeLimitUnitList = noTimeLimitUnitList;
    }

    public String getEventNote() {
        return eventNote;
    }

    public void setEventNote(String eventNote) {
        this.eventNote = eventNote;
    }
}
