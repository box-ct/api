package tw.org.ctworld.meditation.beans.EventEnrollMember;

public class EventPgmShort {
    private String pgmId;
    private String pgmUniqueId;
    private String genPgmUserInputValue;

    public String getPgmUniqueId() {
        return pgmUniqueId;
    }

    public void setPgmUniqueId(String pgmUniqueId) {
        this.pgmUniqueId = pgmUniqueId;
    }

    public String getGenPgmUserInputValue() {
        return genPgmUserInputValue;
    }

    public void setGenPgmUserInputValue(String genPgmUserInputValue) {
        this.genPgmUserInputValue = genPgmUserInputValue;
    }

    public String getPgmId() {
        return pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }
}
