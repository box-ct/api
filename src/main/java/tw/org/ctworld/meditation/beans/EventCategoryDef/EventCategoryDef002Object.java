package tw.org.ctworld.meditation.beans.EventCategoryDef;

import static tw.org.ctworld.meditation.libs.CommonUtils.Null2Empty;

public class EventCategoryDef002Object {

    private Long id;
    private String eventCategoryId, eventCategoryName;
    private Boolean isCenterAvailable;

    public EventCategoryDef002Object(Long id, String eventCategoryId, String eventCategoryName, Boolean isCenterAvailable) {
        this.id = id;
        this.eventCategoryId = eventCategoryId;
        this.eventCategoryName = eventCategoryName;
        this.isCenterAvailable = isCenterAvailable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventCategoryId() {
        return eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = Null2Empty(eventCategoryName);
    }

    public Boolean getIsCenterAvailable() {
        return isCenterAvailable;
    }

    public void setIsCenterAvailable(Boolean centerAvailable) {
        isCenterAvailable = centerAvailable;
    }
}
