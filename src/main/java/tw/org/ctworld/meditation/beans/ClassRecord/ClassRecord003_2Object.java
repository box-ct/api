package tw.org.ctworld.meditation.beans.ClassRecord;

import java.util.Date;

public class ClassRecord003_2Object {

    private String memberId;
    private int classWeeksNum;
    private Date classDate;
    private String attendMark;

    public ClassRecord003_2Object(String memberId, int classWeeksNum, Date classDate, String attendMark) {
        this.memberId = memberId;
        this.classWeeksNum = classWeeksNum;
        this.classDate = classDate;
        this.attendMark = attendMark;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }
}
