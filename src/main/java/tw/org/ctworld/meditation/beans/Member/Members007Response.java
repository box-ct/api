package tw.org.ctworld.meditation.beans.Member;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class Members007Response extends BaseResponse {

    private String classEnrollFormId, classGroupId, sameGroupMemberName, currentClassIntroducerName, currentClassIntroducerRelationship,
            childContactPersonName, childContactPersonPhoneNum;
    private boolean isDroppedClass;

    public Members007Response(int errCode, String errMsg, String classEnrollFormId, String classGroupId, String sameGroupMemberName,
                              String currentClassIntroducerName, String currentClassIntroducerRelationship,
                              String childContactPersonName, String childContactPersonPhoneNum, boolean isDroppedClass) {
        super(errCode, errMsg);
        this.classEnrollFormId = classEnrollFormId;
        this.classGroupId = classGroupId;
        this.sameGroupMemberName = sameGroupMemberName;
        this.currentClassIntroducerName = currentClassIntroducerName;
        this.currentClassIntroducerRelationship = currentClassIntroducerRelationship;
        this.childContactPersonName = childContactPersonName;
        this.childContactPersonPhoneNum = childContactPersonPhoneNum;
        this.isDroppedClass = isDroppedClass;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getSameGroupMemberName() {
        return sameGroupMemberName;
    }

    public void setSameGroupMemberName(String sameGroupMemberName) {
        this.sameGroupMemberName = sameGroupMemberName;
    }

    public String getCurrentClassIntroducerName() {
        return currentClassIntroducerName;
    }

    public void setCurrentClassIntroducerName(String currentClassIntroducerName) {
        this.currentClassIntroducerName = currentClassIntroducerName;
    }

    public String getCurrentClassIntroducerRelationship() {
        return currentClassIntroducerRelationship;
    }

    public void setCurrentClassIntroducerRelationship(String currentClassIntroducerRelationship) {
        this.currentClassIntroducerRelationship = currentClassIntroducerRelationship;
    }

    public String getChildContactPersonName() {
        return childContactPersonName;
    }

    public void setChildContactPersonName(String childContactPersonName) {
        this.childContactPersonName = childContactPersonName;
    }

    public String getChildContactPersonPhoneNum() {
        return childContactPersonPhoneNum;
    }

    public void setChildContactPersonPhoneNum(String childContactPersonPhoneNum) {
        this.childContactPersonPhoneNum = childContactPersonPhoneNum;
    }

    public boolean getIsDroppedClass() {
        return isDroppedClass;
    }

    public void setIsDroppedClass(boolean droppedClass) {
        isDroppedClass = droppedClass;
    }
}
