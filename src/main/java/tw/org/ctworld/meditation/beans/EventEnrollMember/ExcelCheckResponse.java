package tw.org.ctworld.meditation.beans.EventEnrollMember;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;


public class ExcelCheckResponse<T> extends BaseResponse {
    private List<String> notMatchList;
    private List<T> items;

    public ExcelCheckResponse(int errCode, String errMsg, List<String> notMatchList, List<T> items) {
        super(errCode, errMsg);
        this.notMatchList = notMatchList;
        this.items = items;
    }

    public List<String> getNotMatchList() {
        return notMatchList;
    }

    public void setNotMatchList(List<String> notMatchList) {
        this.notMatchList = notMatchList;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
