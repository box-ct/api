package tw.org.ctworld.meditation.beans.Anonymous;

import tw.org.ctworld.meditation.beans.UserSession;

public class Anonymous002Object {

    private String updatorId; // UserId
    private String updatorName; // UserName
    private String updatorUnitName; // UnitName

    public Anonymous002Object(String updatorId, String updatorName, String updatorUnitName) {
        this.updatorId = updatorId;
        this.updatorName = updatorName;
        this.updatorUnitName = updatorUnitName;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

    public String getUpdatorUnitName() {
        return updatorUnitName;
    }

    public void setUpdatorUnitName(String updatorUnitName) {
        this.updatorUnitName = updatorUnitName;
    }
}
