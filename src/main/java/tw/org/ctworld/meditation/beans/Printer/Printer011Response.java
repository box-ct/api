package tw.org.ctworld.meditation.beans.Printer;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.ClassEnrollForm;

import java.util.List;

public class Printer011Response extends BaseResponse {

    private List<ClassEnrollForm> items;

    public Printer011Response(int errCode, String errMsg, List<ClassEnrollForm> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassEnrollForm> getItems() {
        return items;
    }

    public void setItems(List<ClassEnrollForm> items) {
        this.items = items;
    }
}
