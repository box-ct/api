package tw.org.ctworld.meditation.beans.ClassBarCode;

import java.util.Date;

public class ClassBarCode001Object {

    private long id;
    private String memberId, gender, aliasName, ctDharmaName, classPeriodNum, className, classGroupId,
            memberGroupNum;
    private int classWeeksNum;
    private Date attendCheckinDtTm;
    private String attendCheckinTime, attendMark;

    public ClassBarCode001Object(long id, String memberId, String gender, String aliasName, String ctDharmaName,
                                 String classPeriodNum, String className, String classGroupId, String memberGroupNum,
                                 int classWeeksNum, Date attendCheckinDtTm, String attendMark) {
        this.id = id;
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classWeeksNum = classWeeksNum;
        this.attendCheckinDtTm = attendCheckinDtTm;
        this.attendMark = attendMark;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public Date getAttendCheckinDtTm() {
        return attendCheckinDtTm;
    }

    public void setAttendCheckinDtTm(Date attendCheckinDtTm) {
        this.attendCheckinDtTm = attendCheckinDtTm;
    }

    public String getAttendCheckinTime() {
        return attendCheckinTime;
    }

    public void setAttendCheckinTime(String attendCheckinTime) {
        this.attendCheckinTime = attendCheckinTime;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }
}
