package tw.org.ctworld.meditation.beans.ClassStats;

public class ClassStats003_2Object {

    private int absenceCountToFullAttend, absenceCountToGraduate, makeupCountToGraduate, absenceCountToDiligentAward, minMakeupCountToDiligentAward, maxMakeupCountToDiligentAward;

    public ClassStats003_2Object(
            int absenceCountToFullAttend,
            int absenceCountToGraduate,
            int makeupCountToGraduate,
            int absenceCountToDiligentAward,
            int minMakeupCountToDiligentAward,
            int maxMakeupCountToDiligentAward) {

        this.absenceCountToFullAttend = absenceCountToFullAttend;
        this.absenceCountToGraduate = absenceCountToGraduate;
        this.makeupCountToGraduate = makeupCountToGraduate;
        this.absenceCountToDiligentAward = absenceCountToDiligentAward;
        this.minMakeupCountToDiligentAward = minMakeupCountToDiligentAward;
        this.maxMakeupCountToDiligentAward = maxMakeupCountToDiligentAward;
    }

    public int getAbsenceCountToFullAttend() {
        return absenceCountToFullAttend;
    }

    public void setAbsenceCountToFullAttend(int absenceCountToFullAttend) {
        this.absenceCountToFullAttend = absenceCountToFullAttend;
    }

    public int getAbsenceCountToGraduate() {
        return absenceCountToGraduate;
    }

    public void setAbsenceCountToGraduate(int absenceCountToGraduate) {
        this.absenceCountToGraduate = absenceCountToGraduate;
    }

    public int getMakeupCountToGraduate() {
        return makeupCountToGraduate;
    }

    public void setMakeupCountToGraduate(int makeupCountToGraduate) {
        this.makeupCountToGraduate = makeupCountToGraduate;
    }

    public int getAbsenceCountToDiligentAward() {
        return absenceCountToDiligentAward;
    }

    public void setAbsenceCountToDiligentAward(int absenceCountToDiligentAward) {
        this.absenceCountToDiligentAward = absenceCountToDiligentAward;
    }

    public int getMinMakeupCountToDiligentAward() {
        return minMakeupCountToDiligentAward;
    }

    public void setMinMakeupCountToDiligentAward(int minMakeupCountToDiligentAward) {
        this.minMakeupCountToDiligentAward = minMakeupCountToDiligentAward;
    }

    public int getMaxMakeupCountToDiligentAward() {
        return maxMakeupCountToDiligentAward;
    }

    public void setMaxMakeupCountToDiligentAward(int maxMakeupCountToDiligentAward) {
        this.maxMakeupCountToDiligentAward = maxMakeupCountToDiligentAward;
    }
}
