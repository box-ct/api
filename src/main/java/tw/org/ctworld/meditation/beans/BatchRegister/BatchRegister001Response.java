package tw.org.ctworld.meditation.beans.BatchRegister;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class BatchRegister001Response extends BaseResponse {

    private List<BatchRegister001_1Object> members;

    public BatchRegister001Response(int errCode, String errMsg, List<BatchRegister001_1Object> members) {
        super(errCode, errMsg);
        this.members = members;
    }

    public List<BatchRegister001_1Object> getMembers() {
        return members;
    }

    public void setMembers(List<BatchRegister001_1Object> members) {
        this.members = members;
    }
}
