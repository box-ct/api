package tw.org.ctworld.meditation.beans.Printer;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Printer004Response extends BaseResponse {

    private String id, printerId, printerName, note;
    private List<String> certTypeNames;

    public Printer004Response(int errCode, String errMsg, String id, String printerId, String printerName, String note,
                              List<String> certTypeNames) {
        super(errCode, errMsg);
        this.id = id;
        this.printerId = printerId;
        this.printerName = printerName;
        this.note = note;
        this.certTypeNames = certTypeNames;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrinterId() {
        return printerId;
    }

    public void setPrinterId(String printerId) {
        this.printerId = printerId;
    }

    public String getPrinterName() {
        return printerName;
    }

    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getCertTypeNames() {
        return certTypeNames;
    }

    public void setCertTypeNames(List<String> certTypeNames) {
        this.certTypeNames = certTypeNames;
    }
}
