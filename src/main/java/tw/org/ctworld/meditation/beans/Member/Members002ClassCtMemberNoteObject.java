package tw.org.ctworld.meditation.beans.Member;

public class Members002ClassCtMemberNoteObject {

    private String noteDate, note, noteType;

    public Members002ClassCtMemberNoteObject(String noteDate, String note, String noteType) {
        this.noteDate = noteDate;
        this.note = note;
        this.noteType = noteType;
    }

    public String getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(String noteDate) {
        this.noteDate = noteDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }
}
