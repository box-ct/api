package tw.org.ctworld.meditation.beans.Sys;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;

public class ClassUnitRequest {
    private String unitId;
    private String unitName;
    private String groupName;
    private String district;
    private String sortNum;
    private String timezone;
    private String unitStartIp;
    private String unitEndIp;
    @JsonProperty("noIpLimit")
    private boolean noIpLimit;
    @JsonProperty("isOverseasUnit")
    private boolean isOverseasUnit;
    private String abbotId;
    private String abbotName;
    private String abbotEngName;
    private String certUnitEngName;
    private String stateName;
    private String certUnitName;
    private String unitPhoneNum;
    private String unitFaxNum;
    private String unitEmail;
    private String unitPhoneNumCode;
    private String unitMobileNum;
    private String unitMailingAddress;
    private String fundUnitId;

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getSortNum() {
        return sortNum;
    }

    public void setSortNum(String sortNum) {
        this.sortNum = sortNum;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getUnitStartIp() {
        return unitStartIp;
    }

    public void setUnitStartIp(String unitStartIp) {
        this.unitStartIp = unitStartIp;
    }

    public String getUnitEndIp() {
        return unitEndIp;
    }

    public void setUnitEndIp(String unitEndIp) {
        this.unitEndIp = unitEndIp;
    }

    public boolean isNoIpLimit() {
        return noIpLimit;
    }

    public void setNoIpLimit(boolean noIpLimit) {
        this.noIpLimit = noIpLimit;
    }

    public boolean isOverseasUnit() {
        return isOverseasUnit;
    }

    public void setOverseasUnit(boolean overseasUnit) {
        isOverseasUnit = overseasUnit;
    }

    public String getAbbotId() {
        return abbotId;
    }

    public void setAbbotId(String abbotId) {
        this.abbotId = abbotId;
    }

    public String getAbbotName() {
        return abbotName;
    }

    public void setAbbotName(String abbotName) {
        this.abbotName = abbotName;
    }

    public String getAbbotEngName() {
        return abbotEngName;
    }

    public void setAbbotEngName(String abbotEngName) {
        this.abbotEngName = abbotEngName;
    }

    public String getCertUnitEngName() {
        return certUnitEngName;
    }

    public void setCertUnitEngName(String certUnitEngName) {
        this.certUnitEngName = certUnitEngName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCertUnitName() {
        return certUnitName;
    }

    public void setCertUnitName(String certUnitName) {
        this.certUnitName = certUnitName;
    }

    public String getUnitPhoneNum() {
        return unitPhoneNum;
    }

    public void setUnitPhoneNum(String unitPhoneNum) {
        this.unitPhoneNum = unitPhoneNum;
    }

    public String getUnitFaxNum() {
        return unitFaxNum;
    }

    public void setUnitFaxNum(String unitFaxNum) {
        this.unitFaxNum = unitFaxNum;
    }

    public String getUnitEmail() {
        return unitEmail;
    }

    public void setUnitEmail(String unitEmail) {
        this.unitEmail = unitEmail;
    }

    public String getUnitPhoneNumCode() {
        return unitPhoneNumCode;
    }

    public void setUnitPhoneNumCode(String unitPhoneNumCode) {
        this.unitPhoneNumCode = unitPhoneNumCode;
    }

    public String getUnitMobileNum() {
        return unitMobileNum;
    }

    public void setUnitMobileNum(String unitMobileNum) {
        this.unitMobileNum = unitMobileNum;
    }

    public String getUnitMailingAddress() {
        return unitMailingAddress;
    }

    public void setUnitMailingAddress(String unitMailingAddress) {
        this.unitMailingAddress = unitMailingAddress;
    }

    public String getFundUnitId() {
        return fundUnitId;
    }

    public void setFundUnitId(String fundUnitId) {
        this.fundUnitId = fundUnitId;
    }
}
