package tw.org.ctworld.meditation.beans.EventEnrollMaster;

import tw.org.ctworld.meditation.beans.EventEnrollMember.EventPgmShort;
import tw.org.ctworld.meditation.models.EventEnrollMain;

import java.util.List;

public class EventEnrollMainPgm extends EventEnrollMain{
    private List<EventPgmShort> genPgmList;
    private List<EventPgmShort> pgmList;

    public List<EventPgmShort> getGenPgmList() {
        return genPgmList;
    }

    public void setGenPgmList(List<EventPgmShort> genPgmList) {
        this.genPgmList = genPgmList;
    }

    public List<EventPgmShort> getPgmList() {
        return pgmList;
    }

    public void setPgmList(List<EventPgmShort> pgmList) {
        this.pgmList = pgmList;
    }
}
