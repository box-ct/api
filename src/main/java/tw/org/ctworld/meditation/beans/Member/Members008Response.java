package tw.org.ctworld.meditation.beans.Member;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Members008Response extends BaseResponse {

    private List<Members008Object> items;

    public Members008Response(int errCode, String errMsg, List<Members008Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Members008Object> getItems() {
        return items;
    }

    public void setItems(List<Members008Object> items) {
        this.items = items;
    }
}
