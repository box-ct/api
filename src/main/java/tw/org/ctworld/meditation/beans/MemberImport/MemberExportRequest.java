package tw.org.ctworld.meditation.beans.MemberImport;

public class MemberExportRequest {
    private String[] selectedField;
    private String[] memberIds;

    public MemberExportRequest(){}
    public MemberExportRequest(String[] selectedField, String[] memberIds) {
        this.selectedField = selectedField;
        this.memberIds = memberIds;
    }

    public String[] getSelectedField() {
        return selectedField;
    }

    public void setSelectedField(String[] selectedField) {
        this.selectedField = selectedField;
    }

    public String[] getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(String[] memberIds) {
        this.memberIds = memberIds;
    }
}
