package tw.org.ctworld.meditation.beans.DataReport;

public class MonthlyTotal {
    private String year;
    private String month;
    private long total;

    public MonthlyTotal() {
    }

    public MonthlyTotal(String year, String month, long total) {
        this.year = year;
        this.month = month;
        this.total = total;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
