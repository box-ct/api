package tw.org.ctworld.meditation.beans.CtEventMaintain;

import java.util.List;

public class CtEventMaintain012Request {

    private List<CtEventMaintain012_1Object> items;

    public List<CtEventMaintain012_1Object> getItems() {
        return items;
    }

    public void setItems(List<CtEventMaintain012_1Object> items) {
        this.items = items;
    }
}
