package tw.org.ctworld.meditation.beans.Common;

public class Common012_2Object {

    private String memberId, gender, classGroupId, classId;

    public Common012_2Object(String memberId, String gender, String classGroupId, String classId) {
        this.memberId = memberId;
        this.gender = gender;
        this.classGroupId = classGroupId;
        this.classId = classId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }
}
