package tw.org.ctworld.meditation.beans.EventEnrollMember;

public class ExcelMember {
    private String name;
    private String dharmaName;
    private String twIdNum;
    private String memberId;

    public ExcelMember() {
    }

    public ExcelMember(String name, String dharmaName) {
        this.name = name;
        this.dharmaName = dharmaName;
    }

    public ExcelMember(String name, String dharmaName, String twIdNum, String memberId) {
        this.name = name;
        this.dharmaName = dharmaName;
        this.twIdNum = twIdNum;
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDharmaName() {
        return dharmaName;
    }

    public void setDharmaName(String dharmaName) {
        this.dharmaName = dharmaName;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
