package tw.org.ctworld.meditation.beans.EventEnrollMember;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ClassEnrollFormShort {
    private String classId;
    private String className;
    private String classPeriodNum;
    private String classGroupId;
    private String classTypeNum;
    private String classTypeName;
    @JsonIgnore
    private String classJobTitleList;

    private String memberId;
    @JsonIgnore
    private String homeClassNote;

    public ClassEnrollFormShort() {
    }

    public ClassEnrollFormShort(String classId, String className, String classPeriodNum, String classGroupId, Integer classTypeNum, String classTypeName, String memberId) {
        this.classId = classId;
        this.className = className;
        this.classPeriodNum = classPeriodNum;
        this.classGroupId = classGroupId;
        this.classTypeNum = classTypeNum != null ? String.valueOf(classTypeNum) : null;
        this.classTypeName = classTypeName;
        this.memberId = memberId;
    }

    public ClassEnrollFormShort(String className, String classJobTitleList, String memberId, String homeClassNote, Integer classTypeNum) {
        this.className = className;
        this.classJobTitleList = classJobTitleList;
        this.memberId = memberId;
        this.homeClassNote = homeClassNote;
        this.classTypeNum = classTypeNum != null ? String.valueOf(classTypeNum) : null;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getClassTypeNum() {
        return classTypeNum;
    }

    public void setClassTypeNum(String classTypeNum) {
        this.classTypeNum = classTypeNum;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public String getHomeClassNote() {
        return homeClassNote;
    }

    public void setHomeClassNote(String homeClassNote) {
        this.homeClassNote = homeClassNote;
    }
}
