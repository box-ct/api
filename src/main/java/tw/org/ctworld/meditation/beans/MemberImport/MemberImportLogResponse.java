package tw.org.ctworld.meditation.beans.MemberImport;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class MemberImportLogResponse extends BaseResponse {
    private List<MemberImportLog> items;

    public MemberImportLogResponse(int errCode, String errMsg, List<MemberImportLog> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<MemberImportLog> getItems() {
        return items;
    }

    public void setItems(List<MemberImportLog> items) {
        this.items = items;
    }
}
