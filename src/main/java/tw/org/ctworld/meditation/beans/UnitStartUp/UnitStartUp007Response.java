package tw.org.ctworld.meditation.beans.UnitStartUp;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class UnitStartUp007Response extends BaseResponse {

    List<UnitStartUp007Object> items;

    public UnitStartUp007Response(int errCode, String errMsg, List<UnitStartUp007Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<UnitStartUp007Object> getItems() {
        return items;
    }

    public void setItems(List<UnitStartUp007Object> items) {
        this.items = items;
    }
}
