package tw.org.ctworld.meditation.beans.Sys;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.AnnounceInfo;

import java.util.List;

public class MessagesResponse extends BaseResponse {

    private List<AnnounceInfo> items;

    public MessagesResponse(int errCode, String errMsg, List<AnnounceInfo> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<AnnounceInfo> getItems() {
        return items;
    }

    public void setItems(List<AnnounceInfo> items) {
        this.items = items;
    }
}
