package tw.org.ctworld.meditation.beans.UnitMaster;

import java.util.List;

public class MasterImportResultObj {
    private List<MasterInfo> successed;

    private List<String> failed;
    public MasterImportResultObj(List<MasterInfo> successed,List<String> failed){
        this.successed=successed;
        this.failed=failed;
    }
    public List<MasterInfo> getMasterInfoList() {
        return successed;
    }

    public void setMasterInfoList(List<MasterInfo> masterInfoList) {
        this.successed = masterInfoList;
    }

    public List<String> getFailed() {
        return failed;
    }

    public void setFailed(List<String> failed) {
        this.failed = failed;
    }
}

