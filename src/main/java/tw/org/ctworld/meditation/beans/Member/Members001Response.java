package tw.org.ctworld.meditation.beans.Member;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Members001Response extends BaseResponse {

    private List<Members001Object> items;

    public Members001Response(int errCode, String errMsg, List<Members001Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Members001Object> getItems() {
        return items;
    }

    public void setItems(List<Members001Object> items) {
        this.items = items;
    }
}
