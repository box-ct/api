package tw.org.ctworld.meditation.beans.Printer;

import java.util.List;

public class Printer010Request {

    private List<String> classEnrollFormIds;
    private String printType;

    public List<String> getClassEnrollFormIds() {
        return classEnrollFormIds;
    }

    public void setClassEnrollFormIds(List<String> classEnrollFormIds) {
        this.classEnrollFormIds = classEnrollFormIds;
    }

    public String getPrintType() {
        return printType;
    }

    public void setPrintType(String printType) {
        this.printType = printType;
    }
}
