package tw.org.ctworld.meditation.beans.Printer;

import java.util.List;

public class Printer009Request  {

    private String certTemplateName, description;
    private List<Printer009Object> settings;

    public String getCertTemplateName() {
        return certTemplateName;
    }

    public void setCertTemplateName(String certTemplateName) {
        this.certTemplateName = certTemplateName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Printer009Object> getSettings() {
        return settings;
    }

    public void setSettings(List<Printer009Object> settings) {
        this.settings = settings;
    }
}
