package tw.org.ctworld.meditation.beans;

public class ValueRequest<T> {
    private T value;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
