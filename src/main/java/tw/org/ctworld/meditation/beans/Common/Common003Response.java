package tw.org.ctworld.meditation.beans.Common;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class Common003Response extends BaseResponse {

    private int unreadMsgCount;

    public Common003Response(int errCode, String errMsg, int unreadMsgCount) {
        super(errCode, errMsg);
        this.unreadMsgCount = unreadMsgCount;
    }

    public int getUnreadMsgCount() {
        return unreadMsgCount;
    }

    public void setUnreadMsgCount(int unreadMsgCount) {
        this.unreadMsgCount = unreadMsgCount;
    }
}
