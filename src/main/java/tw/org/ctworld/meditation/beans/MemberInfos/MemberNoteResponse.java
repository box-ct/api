package tw.org.ctworld.meditation.beans.MemberInfos;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class MemberNoteResponse extends BaseResponse {
    private List<MemberNote> items;

    public MemberNoteResponse() {
    }

    public MemberNoteResponse(int errCode, String errMsg, List<MemberNote> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<MemberNote> getItems() {
        return items;
    }

    public void setItems(List<MemberNote> items) {
        this.items = items;
    }
}
