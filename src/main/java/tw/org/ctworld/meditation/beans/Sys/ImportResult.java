package tw.org.ctworld.meditation.beans.Sys;

import tw.org.ctworld.meditation.beans.DataImport.ExcelMemberEnroll;
import tw.org.ctworld.meditation.models.ClassInfo;
import tw.org.ctworld.meditation.models.RecordImportLog;

import java.util.List;

public class ImportResult {
    private ClassInfo classInfo;
    private List<ExcelMemberEnroll> enrolls;
    private List<RecordImportLog> logs;

    public ImportResult() {
    }

    public ClassInfo getClassInfo() {
        return classInfo;
    }

    public void setClassInfo(ClassInfo classInfo) {
        this.classInfo = classInfo;
    }

    public List<ExcelMemberEnroll> getEnrolls() {
        return enrolls;
    }

    public void setEnrolls(List<ExcelMemberEnroll> enrolls) {
        this.enrolls = enrolls;
    }

    public List<RecordImportLog> getLogs() {
        return logs;
    }

    public void setLogs(List<RecordImportLog> logs) {
        this.logs = logs;
    }
}
