package tw.org.ctworld.meditation.beans.ClassRecord;

public class ClassRecord007Object {

    private String attendMark;
    private boolean isFullAttended, isGraduated, isDiligentAward, isMakeupClass;

    public ClassRecord007Object(String attendMark, boolean isFullAttended, boolean isGraduated, boolean isDiligentAward, boolean isMakeupClass) {
        this.attendMark = attendMark;
        this.isFullAttended = isFullAttended;
        this.isGraduated = isGraduated;
        this.isDiligentAward = isDiligentAward;
        this.isMakeupClass = isMakeupClass;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }

    public boolean getIsFullAttended() {
        return isFullAttended;
    }

    public void setIsFullAttended(boolean fullAttended) {
        isFullAttended = fullAttended;
    }

    public boolean getIsGraduated() {
        return isGraduated;
    }

    public void setIsGraduated(boolean graduated) {
        isGraduated = graduated;
    }

    public boolean getIsDiligentAward() {
        return isDiligentAward;
    }

    public void setIsDiligentAward(boolean diligentAward) {
        isDiligentAward = diligentAward;
    }

    public boolean getIsMakeupClass() {
        return isMakeupClass;
    }

    public void setIsMakeupClass(boolean makeupClass) {
        isMakeupClass = makeupClass;
    }
}
