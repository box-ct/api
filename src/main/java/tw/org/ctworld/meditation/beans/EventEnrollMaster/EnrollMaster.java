package tw.org.ctworld.meditation.beans.EventEnrollMaster;

public class EnrollMaster {
    private String masterId;
    private String unitId;
    private Boolean isCt;

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public Boolean getIsCt() {
        return isCt;
    }

    public void setIsCt(Boolean ct) {
        isCt = ct;
    }
}
