package tw.org.ctworld.meditation.beans.DataReport;

import java.util.Date;

public class DataReport005Object {

    private String unitName, classId, classPeriodNum, className, classTypeName, classDayOrNight, attendMark, classEnrollFormId;
    private boolean isDroppedClass;
    private Date classDate;
    private int classTypeNum;

    public DataReport005Object(String unitName, String classId, String classPeriodNum, String className,
                               String classTypeName, String classDayOrNight, String attendMark, boolean isDroppedClass,
                               Date classDate, int classTypeNum) {
        this.unitName = unitName;
        this.classId = classId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classTypeName = classTypeName;
        this.classDayOrNight = classDayOrNight;
        this.attendMark = attendMark;
        this.isDroppedClass = isDroppedClass;
        this.classDate = classDate;
        this.classTypeNum = classTypeNum;
    }

    public DataReport005Object(String unitName, String classId, String classPeriodNum, String className,
                               String attendMark, Date classDate, int classTypeNum) {
        this.unitName = unitName;
        this.classId = classId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.attendMark = attendMark;
        this.classDate = classDate;
        this.classTypeNum = classTypeNum;
    }

    public DataReport005Object(String unitName, String classId, String classPeriodNum, String className,
                               int classTypeNum, String classDayOrNight, String classEnrollFormId, Boolean isDroppedClass) {
        this.unitName = unitName;
        this.classId = classId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classTypeNum = classTypeNum;
        this.classDayOrNight = classDayOrNight;

        this.classEnrollFormId = classEnrollFormId;
        this.isDroppedClass = isDroppedClass != null ? isDroppedClass : false;
    }

    public DataReport005Object(String classId, String attendMark, Date classDate, String classEnrollFormId) {
        this.classId = classId;
        this.attendMark = attendMark;
        this.classDate = classDate;

        this.classEnrollFormId = classEnrollFormId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassDayOrNight() {
        return classDayOrNight;
    }

    public void setClassDayOrNight(String classDayOrNight) {
        this.classDayOrNight = classDayOrNight;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }

    public boolean getIsDroppedClass() {
        return isDroppedClass;
    }

    public void setIsDroppedClass(boolean droppedClass) {
        isDroppedClass = droppedClass;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }

    public int getClassTypeNum() {
        return classTypeNum;
    }

    public void setClassTypeNum(int classTypeNum) {
        this.classTypeNum = classTypeNum;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

}
