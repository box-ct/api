package tw.org.ctworld.meditation.beans.Printer;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Printer010Response extends BaseResponse{

    private List<Printer007Object> classCertFieldSettingList;
    private List<Printer011Object> memberNameAndAgeList;
    private String certContent, certMaster, certDate, certFileName, certTypeName;

    public Printer010Response(int errCode, String errMsg, List<Printer007Object> classCertFieldSettingList,
                              List<Printer011Object> memberNameAndAgeList, String certContent, String certMaster,
                              String certDate, String certFileName, String certTypeName) {
        super(errCode, errMsg);
        this.classCertFieldSettingList = classCertFieldSettingList;
        this.memberNameAndAgeList = memberNameAndAgeList;
        this.certContent = certContent;
        this.certMaster = certMaster;
        this.certDate = certDate;
        this.certFileName = certFileName;
        this.certTypeName = certTypeName;
    }

    public List<Printer007Object> getClassCertFieldSettingList() {
        return classCertFieldSettingList;
    }

    public void setClassCertFieldSettingList(List<Printer007Object> classCertFieldSettingList) {
        this.classCertFieldSettingList = classCertFieldSettingList;
    }

    public List<Printer011Object> getMemberNameAndAgeList() {
        return memberNameAndAgeList;
    }

    public void setMemberNameAndAgeList(List<Printer011Object> memberNameAndAgeList) {
        this.memberNameAndAgeList = memberNameAndAgeList;
    }

    public String getCertContent() {
        return certContent;
    }

    public void setCertContent(String certContent) {
        this.certContent = certContent;
    }

    public String getCertMaster() {
        return certMaster;
    }

    public void setCertMaster(String certMaster) {
        this.certMaster = certMaster;
    }

    public String getCertDate() {
        return certDate;
    }

    public void setCertDate(String certDate) {
        this.certDate = certDate;
    }

    public String getCertFileName() {
        return certFileName;
    }

    public void setCertFileName(String certFileName) {
        this.certFileName = certFileName;
    }

    public String getCertTypeName() {
        return certTypeName;
    }

    public void setCertTypeName(String certTypeName) {
        this.certTypeName = certTypeName;
    }
}
