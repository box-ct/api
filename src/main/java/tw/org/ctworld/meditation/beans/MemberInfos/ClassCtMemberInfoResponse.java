package tw.org.ctworld.meditation.beans.MemberInfos;

import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.models.CtMemberInfo;

public class ClassCtMemberInfoResponse extends BaseResponse {
    private CtMemberInfo member;

    public ClassCtMemberInfoResponse(int errCode, String errMsg, CtMemberInfo member) {
        super(errCode, errMsg);
        this.member = member;
    }

    public CtMemberInfo getMember() {
        return member;
    }

    public void setMember(CtMemberInfo member) {
        this.member = member;
    }
}
