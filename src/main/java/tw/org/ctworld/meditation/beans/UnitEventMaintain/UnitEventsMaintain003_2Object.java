package tw.org.ctworld.meditation.beans.UnitEventMaintain;

public class UnitEventsMaintain003_2Object {
    
    private String id, eventId, eventName, eventCategoryId, eventCategoryName, eventCreatorUnitId, eventCreatorUnitName,
            sponsorUnitId, sponsorUnitName, eventNote, idType1List, idType2List, attendUnitList;
    private Boolean isAvailableForAllUnit;
    private String noTimeLimitUnitList, eventYear, eventPlace, eventEndDate, eventEndTime, eventStartDate, 
            eventStartTime, changeEndDate, enrollEndDate, finalDate, summerDonationType;
    private Boolean isShowSummerDonationButton, isMultipleEnroll, isSyncCtTable, isNeedCtApproved, isNeedAbbotSigned,
            isOnlyForMaster, isShowRefugeTab, isShowPrecept5Tab, isShowBodhiPreceptTab, isShowZen7Tab,
            isShowPrecept8Tab, isOtherUnitCanApplyThisUnitEvent, isCreatedByCt, isCreatedByUnit, isHeldInUnit;
    private int status;

    public UnitEventsMaintain003_2Object(long id, String eventId, String eventName, String eventCategoryId,
                                         String eventCategoryName, String eventCreatorUnitId,
                                         String eventCreatorUnitName, String sponsorUnitId, String sponsorUnitName,
                                         String eventNote, String idType1List, String idType2List,
                                         String attendUnitList, Boolean isAvailableForAllUnit,
                                         String noTimeLimitUnitList, String eventYear, String eventPlace,
                                         String eventEndDate, String eventEndTime, String eventStartDate,
                                         String eventStartTime, String changeEndDate, String enrollEndDate,
                                         String finalDate, String summerDonationType, Boolean isShowSummerDonationButton,
                                         Boolean isMultipleEnroll, Boolean isSyncCtTable, Boolean isNeedCtApproved,
                                         Boolean isNeedAbbotSigned, Boolean isOnlyForMaster, Boolean isShowRefugeTab,
                                         Boolean isShowPrecept5Tab, Boolean isShowBodhiPreceptTab,
                                         Boolean isShowZen7Tab, Boolean isShowPrecept8Tab,
                                         Boolean isOtherUnitCanApplyThisUnitEvent, Boolean isCreatedByCt,
                                         Boolean isCreatedByUnit, Boolean isHeldInUnit, int status) {
        this.id = String.valueOf(id);
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventCategoryId = eventCategoryId;
        this.eventCategoryName = eventCategoryName;
        this.eventCreatorUnitId = eventCreatorUnitId;
        this.eventCreatorUnitName = eventCreatorUnitName;
        this.sponsorUnitId = sponsorUnitId;
        this.sponsorUnitName = sponsorUnitName;
        this.eventNote = eventNote; // 10
        this.idType1List = idType1List;
        this.idType2List = idType2List;
        this.attendUnitList = attendUnitList;
        this.isAvailableForAllUnit = isAvailableForAllUnit;
        this.noTimeLimitUnitList = noTimeLimitUnitList;
        this.eventYear = eventYear;
        this.eventPlace = eventPlace;
        this.eventEndDate = eventEndDate;
        this.eventEndTime = eventEndTime;
        this.eventStartDate = eventStartDate;
        this.eventStartTime = eventStartTime;
        this.changeEndDate = changeEndDate;
        this.enrollEndDate = enrollEndDate;
        this.finalDate = finalDate;
        this.summerDonationType = summerDonationType;
        this.isShowSummerDonationButton = isShowSummerDonationButton;
        this.isMultipleEnroll = isMultipleEnroll;
        this.isSyncCtTable = isSyncCtTable;
        this.isNeedCtApproved = isNeedCtApproved;
        this.isNeedAbbotSigned = isNeedAbbotSigned;
        this.isOnlyForMaster = isOnlyForMaster;
        this.isShowRefugeTab = isShowRefugeTab;
        this.isShowPrecept5Tab = isShowPrecept5Tab;
        this.isShowBodhiPreceptTab = isShowBodhiPreceptTab;
        this.isShowZen7Tab = isShowZen7Tab;
        this.isShowPrecept8Tab = isShowPrecept8Tab;
        this.isOtherUnitCanApplyThisUnitEvent = isOtherUnitCanApplyThisUnitEvent;
        this.isCreatedByCt = isCreatedByCt;
        this.isCreatedByUnit = isCreatedByUnit;
        this.isHeldInUnit = isHeldInUnit;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventCategoryId() {
        return eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public String getEventCreatorUnitId() {
        return eventCreatorUnitId;
    }

    public void setEventCreatorUnitId(String eventCreatorUnitId) {
        this.eventCreatorUnitId = eventCreatorUnitId;
    }

    public String getEventCreatorUnitName() {
        return eventCreatorUnitName;
    }

    public void setEventCreatorUnitName(String eventCreatorUnitName) {
        this.eventCreatorUnitName = eventCreatorUnitName;
    }

    public String getSponsorUnitId() {
        return sponsorUnitId;
    }

    public void setSponsorUnitId(String sponsorUnitId) {
        this.sponsorUnitId = sponsorUnitId;
    }

    public String getSponsorUnitName() {
        return sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getEventNote() {
        return eventNote;
    }

    public void setEventNote(String eventNote) {
        this.eventNote = eventNote;
    }

    public String getIdType1List() {
        return idType1List;
    }

    public void setIdType1List(String idType1List) {
        this.idType1List = idType1List;
    }

    public String getIdType2List() {
        return idType2List;
    }

    public void setIdType2List(String idType2List) {
        this.idType2List = idType2List;
    }

    public String getAttendUnitList() {
        return attendUnitList;
    }

    public void setAttendUnitList(String attendUnitList) {
        this.attendUnitList = attendUnitList;
    }

    public Boolean getIsAvailableForAllUnit() {
        return isAvailableForAllUnit;
    }

    public void setIsAvailableForAllUnit(Boolean availableForAllUnit) {
        isAvailableForAllUnit = availableForAllUnit;
    }

    public String getNoTimeLimitUnitList() {
        return noTimeLimitUnitList;
    }

    public void setNoTimeLimitUnitList(String noTimeLimitUnitList) {
        this.noTimeLimitUnitList = noTimeLimitUnitList;
    }

    public String getEventYear() {
        return eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public String getEventPlace() {
        return eventPlace;
    }

    public void setEventPlace(String eventPlace) {
        this.eventPlace = eventPlace;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getChangeEndDate() {
        return changeEndDate;
    }

    public void setChangeEndDate(String changeEndDate) {
        this.changeEndDate = changeEndDate;
    }

    public String getEnrollEndDate() {
        return enrollEndDate;
    }

    public void setEnrollEndDate(String enrollEndDate) {
        this.enrollEndDate = enrollEndDate;
    }

    public String getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(String finalDate) {
        this.finalDate = finalDate;
    }

    public String getSummerDonationType() {
        return summerDonationType;
    }

    public void setSummerDonationType(String summerDonationType) {
        this.summerDonationType = summerDonationType;
    }

    public Boolean getIsShowSummerDonationButton() {
        return isShowSummerDonationButton;
    }

    public void setIsShowSummerDonationButton(Boolean showSummerDonationButton) {
        isShowSummerDonationButton = showSummerDonationButton;
    }

    public Boolean getIsMultipleEnroll() {
        return isMultipleEnroll;
    }

    public void setIsMultipleEnroll(Boolean multipleEnroll) {
        isMultipleEnroll = multipleEnroll;
    }

    public Boolean getIsSyncCtTable() {
        return isSyncCtTable;
    }

    public void setIsSyncCtTable(Boolean syncCtTable) {
        isSyncCtTable = syncCtTable;
    }

    public Boolean getIsNeedCtApproved() {
        return isNeedCtApproved;
    }

    public void setIsNeedCtApproved(Boolean needCtApproved) {
        isNeedCtApproved = needCtApproved;
    }

    public Boolean getIsNeedAbbotSigned() {
        return isNeedAbbotSigned;
    }

    public void setIsNeedAbbotSigned(Boolean needAbbotSigned) {
        isNeedAbbotSigned = needAbbotSigned;
    }

    public Boolean getIsOnlyForMaster() {
        return isOnlyForMaster;
    }

    public void setIsOnlyForMaster(Boolean onlyForMaster) {
        isOnlyForMaster = onlyForMaster;
    }

    public Boolean getIsShowRefugeTab() {
        return isShowRefugeTab;
    }

    public void setIsShowRefugeTab(Boolean showRefugeTab) {
        isShowRefugeTab = showRefugeTab;
    }

    public Boolean getIsShowPrecept5Tab() {
        return isShowPrecept5Tab;
    }

    public void setIsShowPrecept5Tab(Boolean showPrecept5Tab) {
        isShowPrecept5Tab = showPrecept5Tab;
    }

    public Boolean getIsShowBodhiPreceptTab() {
        return isShowBodhiPreceptTab;
    }

    public void setIsShowBodhiPreceptTab(Boolean showBodhiPreceptTab) {
        isShowBodhiPreceptTab = showBodhiPreceptTab;
    }

    public Boolean getIsShowZen7Tab() {
        return isShowZen7Tab;
    }

    public void setIsShowZen7Tab(Boolean showZen7Tab) {
        isShowZen7Tab = showZen7Tab;
    }

    public Boolean getIsShowPrecept8Tab() {
        return isShowPrecept8Tab;
    }

    public void setIsShowPrecept8Tab(Boolean showPrecept8Tab) {
        isShowPrecept8Tab = showPrecept8Tab;
    }

    public Boolean getIsOtherUnitCanApplyThisUnitEvent() {
        return isOtherUnitCanApplyThisUnitEvent;
    }

    public void setIsOtherUnitCanApplyThisUnitEvent(Boolean otherUnitCanApplyThisUnitEvent) {
        isOtherUnitCanApplyThisUnitEvent = otherUnitCanApplyThisUnitEvent;
    }

    public Boolean getIsCreatedByCt() {
        return isCreatedByCt;
    }

    public void setIsCreatedByCt(Boolean createdByCt) {
        isCreatedByCt = createdByCt;
    }

    public Boolean getIsCreatedByUnit() {
        return isCreatedByUnit;
    }

    public void setIsCreatedByUnit(Boolean createdByUnit) {
        isCreatedByUnit = createdByUnit;
    }

    public Boolean getIsHeldInUnit() {
        return isHeldInUnit;
    }

    public void setIsHeldInUnit(Boolean heldInUnit) {
        isHeldInUnit = heldInUnit;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
