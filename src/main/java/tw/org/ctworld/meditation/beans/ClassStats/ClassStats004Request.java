package tw.org.ctworld.meditation.beans.ClassStats;

import java.util.List;

public class ClassStats004Request {

    private boolean isOverWrite;
    private List<ClassStats004Object> memberList;

    public boolean getIsOverWrite() {
        return isOverWrite;
    }

    public void setIsOverWrite(boolean overWrite) {
        isOverWrite = overWrite;
    }

    public List<ClassStats004Object> getMemberList() {
        return memberList;
    }

    public void setMemberList(List<ClassStats004Object> memberList) {
        this.memberList = memberList;
    }

    public static class ClassStats004Object {

        private String classEnrollFormId;
        private boolean isGraduated, isFullAttended;
        private String classAttendedResult;

        public String getClassEnrollFormId() {
            return classEnrollFormId;
        }

        public void setClassEnrollFormId(String classEnrollFormId) {
            this.classEnrollFormId = classEnrollFormId;
        }

        public boolean getIsGraduated() {
            return isGraduated;
        }

        public void setIsGraduated(boolean graduated) {
            isGraduated = graduated;
        }

        public boolean getIsFullAttended() {
            return isFullAttended;
        }

        public void setIsFullAttended(boolean fullAttended) {
            isFullAttended = fullAttended;
        }

        public String getClassAttendedResult() {
            return classAttendedResult;
        }

        public void setClassAttendedResult(String classAttendedResult) {
            this.classAttendedResult = classAttendedResult;
        }
    }
}
