package tw.org.ctworld.meditation.beans.Classes;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Classes007Response extends BaseResponse {

    private List<Classes007Object> items;

    public Classes007Response(int errCode, String errMsg, List<Classes007Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Classes007Object> getItems() {
        return items;
    }

    public void setItems(List<Classes007Object> items) {
        this.items = items;
    }
}
