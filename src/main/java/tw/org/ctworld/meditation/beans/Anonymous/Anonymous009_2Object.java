package tw.org.ctworld.meditation.beans.Anonymous;

public class Anonymous009_2Object {

    private String classDate;
    private int classWeeksNum;
    private String marker, note;

    public Anonymous009_2Object(String classDate, int classWeeksNum, String marker, String note) {
        this.classDate = classDate;
        this.classWeeksNum = classWeeksNum;
        this.marker = marker;
        this.note = note;
    }

    public String getClassDate() {
        return classDate;
    }

    public void setClassDate(String classDate) {
        this.classDate = classDate;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
