package tw.org.ctworld.meditation.beans.UnitEventMaintain;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;

public class UnitEventsMaintain008Object {
    private String pgmId;
    private String pgmName;
    private String pgmUniqueId;
    private String pgmCategoryName;
    private String pgmDate;
    private String pgmStartTime;
    private String pgmEndTime;
    private String pgmNote;
    private Integer pgmUiOrderNum;

    public UnitEventsMaintain008Object(String pgmId, String pgmName, String pgmUniqueId, String pgmCategoryName, Date pgmDate, Date pgmStartTime, Date pgmEndTime, String pgmNote, Integer pgmUiOrderNum) {
        this.pgmId = pgmId;
        this.pgmName = pgmName;
        this.pgmUniqueId = pgmUniqueId;
        this.pgmCategoryName = pgmCategoryName;
        this.pgmDate = CommonUtils.DateTime2DateString(pgmDate);
        this.pgmStartTime = CommonUtils.DateTime2TimeString(pgmStartTime);
        this.pgmEndTime = CommonUtils.DateTime2TimeString(pgmEndTime);
        this.pgmNote = pgmNote;
        this.pgmUiOrderNum = pgmUiOrderNum;
    }

    public String getPgmId() {
        return pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }

    public String getPgmName() {
        return pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

    public String getPgmUniqueId() {
        return pgmUniqueId;
    }

    public void setPgmUniqueId(String pgmUniqueId) {
        this.pgmUniqueId = pgmUniqueId;
    }

    public String getPgmCategoryName() {
        return pgmCategoryName;
    }

    public void setPgmCategoryName(String pgmCategoryName) {
        this.pgmCategoryName = pgmCategoryName;
    }

    public String getPgmDate() {
        return pgmDate;
    }

    public void setPgmDate(String pgmDate) {
        this.pgmDate = pgmDate;
    }

    public String getPgmStartTime() {
        return pgmStartTime;
    }

    public void setPgmStartTime(String pgmStartTime) {
        this.pgmStartTime = pgmStartTime;
    }

    public String getPgmEndTime() {
        return pgmEndTime;
    }

    public void setPgmEndTime(String pgmEndTime) {
        this.pgmEndTime = pgmEndTime;
    }

    public String getPgmNote() {
        return pgmNote;
    }

    public void setPgmNote(String pgmNote) {
        this.pgmNote = pgmNote;
    }

    public Integer getPgmUiOrderNum() {
        return pgmUiOrderNum;
    }

    public void setPgmUiOrderNum(Integer pgmUiOrderNum) {
        this.pgmUiOrderNum = pgmUiOrderNum;
    }
}
