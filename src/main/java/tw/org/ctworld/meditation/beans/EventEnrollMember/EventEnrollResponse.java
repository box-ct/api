package tw.org.ctworld.meditation.beans.EventEnrollMember;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class EventEnrollResponse extends BaseResponse {

    public EventEnrollResponse(int errCode, String errMsg, String formAutoAddNum) {
        super(errCode, errMsg);
        this.result.setFormAutoAddNum(formAutoAddNum);
    }

    private EventEnrollResult result = new EventEnrollResult();

    public EventEnrollResult getResult() {
        return result;
    }

    public void setResult(EventEnrollResult result) {
        this.result = result;
    }
}
