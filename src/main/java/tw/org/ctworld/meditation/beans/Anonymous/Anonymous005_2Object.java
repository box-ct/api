package tw.org.ctworld.meditation.beans.Anonymous;

public class Anonymous005_2Object {

    private String memberId, photo, aliasName, ctDharmaName, unEmptyKey;

    public Anonymous005_2Object(String memberId, String aliasName, String ctDharmaName) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
    }

    public Anonymous005_2Object(String memberId, String photo, String aliasName, String ctDharmaName, String unEmptyKey) {
        this.memberId = memberId;
        this.photo = photo;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.unEmptyKey = unEmptyKey;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getUnEmptyKey() {
        return unEmptyKey;
    }

    public void setUnEmptyKey(String unEmptyKey) {
        this.unEmptyKey = unEmptyKey;
    }
}
