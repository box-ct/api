package tw.org.ctworld.meditation.beans.CtEventMaintain;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class CtEventMaintain003Response extends BaseResponse {

    private List<CtEventMaintain003_1Object> items;

    public CtEventMaintain003Response(int errCode, String errMsg, List<CtEventMaintain003_1Object> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<CtEventMaintain003_1Object> getItems() {
        return items;
    }

    public void setItems(List<CtEventMaintain003_1Object> items) {
        this.items = items;
    }
}
