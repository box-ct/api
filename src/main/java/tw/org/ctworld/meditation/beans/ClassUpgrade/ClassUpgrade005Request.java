package tw.org.ctworld.meditation.beans.ClassUpgrade;

import java.util.List;

public class ClassUpgrade005Request {

    private String classGroupId, memberGroupNum;
    private List<String> classJobTitleList, classJobContentList;
    private boolean isLeadGroup;

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public List<String> getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(List<String> classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public List<String> getClassJobContentList() {
        return classJobContentList;
    }

    public void setClassJobContentList(List<String> classJobContentList) {
        this.classJobContentList = classJobContentList;
    }

    public boolean getIsLeadGroup() {
        return isLeadGroup;
    }

    public void setIsLeadGroup(boolean leadGroup) {
        isLeadGroup = leadGroup;
    }
}
