package tw.org.ctworld.meditation.beans;

import java.util.List;

public class ManagedClassRequest {
    private String id;
    private List<String> classIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getClassIds() {
        return classIds;
    }

    public void setClassIds(List<String> classIds) {
        this.classIds = classIds;
    }
}
