package tw.org.ctworld.meditation.beans.Member;

public class Members004Object {

    private String className, dayOfWeek, classDayOrNight, aliasName, classJobTitleList, ctDharmaName, gender,
            classGroupId, memberId, photo, classTypeName, unitName;

    public Members004Object(String className, String dayOfWeek, String classDayOrNight, String aliasName,
                            String classJobTitleList, String ctDharmaName, String gender, String classGroupId,
                            String memberId, String photo) {
        this.className = className;
        this.dayOfWeek = dayOfWeek;
        this.classDayOrNight = classDayOrNight;
        this.aliasName = aliasName;
        this.classJobTitleList = classJobTitleList;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.classGroupId = classGroupId;
        this.memberId = memberId;
        this.photo = photo;
    }

    public Members004Object(String className, String dayOfWeek, String classDayOrNight, String aliasName,
                            String classJobTitleList, String ctDharmaName, String gender, String classGroupId,
                            String memberId, String classTypeName, String unitName) {
        this.className = className;
        this.dayOfWeek = dayOfWeek;
        this.classDayOrNight = classDayOrNight;
        this.aliasName = aliasName;
        this.classJobTitleList = classJobTitleList;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.classGroupId = classGroupId;
        this.memberId = memberId;
        this.classTypeName = classTypeName;
        this.unitName = unitName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getClassDayOrNight() {
        return classDayOrNight;
    }

    public void setClassDayOrNight(String classDayOrNight) {
        this.classDayOrNight = classDayOrNight;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
