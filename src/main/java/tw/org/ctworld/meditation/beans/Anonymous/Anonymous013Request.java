package tw.org.ctworld.meditation.beans.Anonymous;

public class Anonymous013Request {

    private String enrollFormId;
    private int classWeeksNum;

    public String getEnrollFormId() {
        return enrollFormId;
    }

    public void setEnrollFormId(String enrollFormId) {
        this.enrollFormId = enrollFormId;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }
}
