package tw.org.ctworld.meditation.beans.EventCategoryDef;

import tw.org.ctworld.meditation.beans.BaseResponse;

public class EventCategoryDef004Response extends BaseResponse {

    private String eventCategoryId, eventCategoryName;
    private Boolean isCenterAvailable;

    public EventCategoryDef004Response(int errCode, String errMsg, String eventCategoryId, String eventCategoryName,
                                       Boolean isCenterAvailable) {
        super(errCode, errMsg);
        this.eventCategoryId = eventCategoryId;
        this.eventCategoryName = eventCategoryName;
        this.isCenterAvailable = isCenterAvailable;
    }

    public String getEventCategoryId() {
        return eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public Boolean getIsCenterAvailable() {
        return isCenterAvailable;
    }

    public void setIsCenterAvailable(Boolean centerAvailable) {
        isCenterAvailable = centerAvailable;
    }
}
