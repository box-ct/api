package tw.org.ctworld.meditation.beans.DataReport;

public class DataReport001Object {

    private String unitName, year, classPeriodNum, className, classId, gender;
    private boolean isDroppedClass, isGraduated;
    private String classDayOrNight, classTypeName;

    public DataReport001Object(String unitName, String year, String classPeriodNum, String className, String classId,
                               String gender, Boolean isDroppedClass, Boolean isGraduated, String classDayOrNight,
                               String classTypeName) {
        this.unitName = unitName;
        this.year = year;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classId = classId;
        this.gender = gender;
        this.isDroppedClass = isDroppedClass != null ? isDroppedClass : false;
        this.isGraduated = isGraduated != null ? isGraduated : false;
        this.classDayOrNight = classDayOrNight;
        this.classTypeName = classTypeName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean getIsDroppedClass() {
        return isDroppedClass;
    }

    public void setIsDroppedClass(boolean droppedClass) {
        isDroppedClass = droppedClass;
    }

    public boolean getIsGraduated() {
        return isGraduated;
    }

    public void setIsGraduated(boolean graduated) {
        isGraduated = graduated;
    }

    public String getClassDayOrNight() {
        return classDayOrNight;
    }

    public void setClassDayOrNight(String classDayOrNight) {
        this.classDayOrNight = classDayOrNight;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }
}
