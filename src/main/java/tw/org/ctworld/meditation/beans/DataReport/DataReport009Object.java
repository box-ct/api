package tw.org.ctworld.meditation.beans.DataReport;

public class DataReport009Object {

    private String year, month;
    private int dayClass1Stats, dayClass2Stats, dayClass3Stats, dayClass4Stats, dayClass5Stats,
            nightClass1Stats, nightClass2Stats, nightClass3Stats, nightClass4Stats, nightClass5Stats,
            engDayClass1Stats, engDayClass2Stats, engDayClass3Stats, engDayClass4Stats,
            engNightClass1Stats, engNightClass2Stats, engNightClass3Stats, engNightClass4Stats,
            japDayClass1Stats, childrenClass, seniorClass, otherClass;

    public DataReport009Object(String year, String month, int dayClass1Stats, int dayClass2Stats, int dayClass3Stats, int dayClass4Stats,
                               int dayClass5Stats, int nightClass1Stats, int nightClass2Stats, int nightClass3Stats,
                               int nightClass4Stats, int nightClass5Stats, int childrenClass, int seniorClass,
                               int otherClass) {
        this.year = year;
        this.month = month;
        this.dayClass1Stats = dayClass1Stats;
        this.dayClass2Stats = dayClass2Stats;
        this.dayClass3Stats = dayClass3Stats;
        this.dayClass4Stats = dayClass4Stats;
        this.dayClass5Stats = dayClass5Stats;
        this.nightClass1Stats = nightClass1Stats;
        this.nightClass2Stats = nightClass2Stats;
        this.nightClass3Stats = nightClass3Stats;
        this.nightClass4Stats = nightClass4Stats;
        this.nightClass5Stats = nightClass5Stats;
        this.childrenClass = childrenClass;
        this.seniorClass = seniorClass;
        this.otherClass = otherClass;

    }

    public DataReport009Object(String year, String month, int engDayClass1Stats, int engDayClass2Stats, int engDayClass3Stats,
                               int engDayClass4Stats, int engNightClass1Stats, int engNightClass2Stats,
                               int engNightClass3Stats, int engNightClass4Stats, int japDayClass1Stats,
                               int childrenClass, int seniorClass, int otherClass) {

        this.year = year;
        this.month = month;
        this.engDayClass1Stats = engDayClass1Stats;
        this.engDayClass2Stats = engDayClass2Stats;
        this.engDayClass3Stats = engDayClass3Stats;
        this.engDayClass4Stats = engDayClass4Stats;
        this.engNightClass1Stats = engNightClass1Stats;
        this.engNightClass2Stats = engNightClass2Stats;
        this.engNightClass3Stats = engNightClass3Stats;
        this.engNightClass4Stats = engNightClass4Stats;
        this.japDayClass1Stats = japDayClass1Stats;
        this.childrenClass = childrenClass;
        this.seniorClass = seniorClass;
        this.otherClass = otherClass;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getDayClass1Stats() {
        return dayClass1Stats;
    }

    public void setDayClass1Stats(int dayClass1Stats) {
        this.dayClass1Stats = dayClass1Stats;
    }

    public int getDayClass2Stats() {
        return dayClass2Stats;
    }

    public void setDayClass2Stats(int dayClass2Stats) {
        this.dayClass2Stats = dayClass2Stats;
    }

    public int getDayClass3Stats() {
        return dayClass3Stats;
    }

    public void setDayClass3Stats(int dayClass3Stats) {
        this.dayClass3Stats = dayClass3Stats;
    }

    public int getDayClass4Stats() {
        return dayClass4Stats;
    }

    public void setDayClass4Stats(int dayClass4Stats) {
        this.dayClass4Stats = dayClass4Stats;
    }

    public int getDayClass5Stats() {
        return dayClass5Stats;
    }

    public void setDayClass5Stats(int dayClass5Stats) {
        this.dayClass5Stats = dayClass5Stats;
    }

    public int getNightClass1Stats() {
        return nightClass1Stats;
    }

    public void setNightClass1Stats(int nightClass1Stats) {
        this.nightClass1Stats = nightClass1Stats;
    }

    public int getNightClass2Stats() {
        return nightClass2Stats;
    }

    public void setNightClass2Stats(int nightClass2Stats) {
        this.nightClass2Stats = nightClass2Stats;
    }

    public int getNightClass3Stats() {
        return nightClass3Stats;
    }

    public void setNightClass3Stats(int nightClass3Stats) {
        this.nightClass3Stats = nightClass3Stats;
    }

    public int getNightClass4Stats() {
        return nightClass4Stats;
    }

    public void setNightClass4Stats(int nightClass4Stats) {
        this.nightClass4Stats = nightClass4Stats;
    }

    public int getNightClass5Stats() {
        return nightClass5Stats;
    }

    public void setNightClass5Stats(int nightClass5Stats) {
        this.nightClass5Stats = nightClass5Stats;
    }

    public int getEngDayClass1Stats() {
        return engDayClass1Stats;
    }

    public void setEngDayClass1Stats(int engDayClass1Stats) {
        this.engDayClass1Stats = engDayClass1Stats;
    }

    public int getEngDayClass2Stats() {
        return engDayClass2Stats;
    }

    public void setEngDayClass2Stats(int engDayClass2Stats) {
        this.engDayClass2Stats = engDayClass2Stats;
    }

    public int getEngDayClass3Stats() {
        return engDayClass3Stats;
    }

    public void setEngDayClass3Stats(int engDayClass3Stats) {
        this.engDayClass3Stats = engDayClass3Stats;
    }

    public int getEngDayClass4Stats() {
        return engDayClass4Stats;
    }

    public void setEngDayClass4Stats(int engDayClass4Stats) {
        this.engDayClass4Stats = engDayClass4Stats;
    }

    public int getEngNightClass1Stats() {
        return engNightClass1Stats;
    }

    public void setEngNightClass1Stats(int engNightClass1Stats) {
        this.engNightClass1Stats = engNightClass1Stats;
    }

    public int getEngNightClass2Stats() {
        return engNightClass2Stats;
    }

    public void setEngNightClass2Stats(int engNightClass2Stats) {
        this.engNightClass2Stats = engNightClass2Stats;
    }

    public int getEngNightClass3Stats() {
        return engNightClass3Stats;
    }

    public void setEngNightClass3Stats(int engNightClass3Stats) {
        this.engNightClass3Stats = engNightClass3Stats;
    }

    public int getEngNightClass4Stats() {
        return engNightClass4Stats;
    }

    public void setEngNightClass4Stats(int engNightClass4Stats) {
        this.engNightClass4Stats = engNightClass4Stats;
    }

    public int getJapDayClass1Stats() {
        return japDayClass1Stats;
    }

    public void setJapDayClass1Stats(int japDayClass1Stats) {
        this.japDayClass1Stats = japDayClass1Stats;
    }

    public int getChildrenClass() {
        return childrenClass;
    }

    public void setChildrenClass(int childrenClass) {
        this.childrenClass = childrenClass;
    }

    public int getSeniorClass() {
        return seniorClass;
    }

    public void setSeniorClass(int seniorClass) {
        this.seniorClass = seniorClass;
    }

    public int getOtherClass() {
        return otherClass;
    }

    public void setOtherClass(int otherClass) {
        this.otherClass = otherClass;
    }
}
