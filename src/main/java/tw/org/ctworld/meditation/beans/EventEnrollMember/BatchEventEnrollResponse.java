package tw.org.ctworld.meditation.beans.EventEnrollMember;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class BatchEventEnrollResponse extends BaseResponse {
    private List<EventEnrollResult> result;

    public BatchEventEnrollResponse(int errCode, String errMsg, List<EventEnrollResult> result) {
        super(errCode, errMsg);
        this.result = result;
    }

    public List<EventEnrollResult> getResult() {
        return result;
    }

    public void setResult(List<EventEnrollResult> result) {
        this.result = result;
    }
}
