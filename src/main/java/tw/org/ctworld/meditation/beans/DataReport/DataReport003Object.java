package tw.org.ctworld.meditation.beans.DataReport;

public class DataReport003Object {

    private String unitName, classId, className, classTypeName, memberId, attendMark;
    private boolean isDroppedClass;

    public DataReport003Object(String unitName, String classId, String className, String classTypeName, String memberId,
                               String attendMark, Boolean isDroppedClass) {
        this.unitName = unitName;
        this.classId = classId;
        this.className = className;
        this.classTypeName = classTypeName;
        this.memberId = memberId;
        this.attendMark = attendMark;
        this.isDroppedClass = isDroppedClass != null ? isDroppedClass : false;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }

    public boolean getIsDroppedClass() {
        return isDroppedClass;
    }

    public void setIsDroppedClass(boolean droppedClass) {
        isDroppedClass = droppedClass;
    }
}
