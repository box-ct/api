package tw.org.ctworld.meditation.beans.EventAbbotSign;

import com.fasterxml.jackson.annotation.JsonFormat;
import tw.org.ctworld.meditation.models.EventUnitInfo;

import java.util.Date;

public class SignUnit {
    private String unitId;
    private String unitName;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date enrollEndDate;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date changeEndDate;
    private int masterEnrollCount;
    private int notMasterEnrollCount;
    private boolean isNeedAbbotSigned;
    private int abbotSignedCount;
    private int notAbbotSignedCount;

    public SignUnit(EventUnitInfo eventUnitInfo) {
        this.unitId = eventUnitInfo.getUnitId();
        this.unitName = eventUnitInfo.getUnitName();
        this.enrollEndDate = eventUnitInfo.getEnrollEndDate();
        this.changeEndDate = eventUnitInfo.getChangeEndDate();
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Date getEnrollEndDate() {
        return enrollEndDate;
    }

    public void setEnrollEndDate(Date enrollEndDate) {
        this.enrollEndDate = enrollEndDate;
    }

    public Date getChangeEndDate() {
        return changeEndDate;
    }

    public void setChangeEndDate(Date changeEndDate) {
        this.changeEndDate = changeEndDate;
    }

    public int getMasterEnrollCount() {
        return masterEnrollCount;
    }

    public void setMasterEnrollCount(int masterEnrollCount) {
        this.masterEnrollCount = masterEnrollCount;
    }

    public int getNotMasterEnrollCount() {
        return notMasterEnrollCount;
    }

    public void setNotMasterEnrollCount(int notMasterEnrollCount) {
        this.notMasterEnrollCount = notMasterEnrollCount;
    }

    public boolean getIsNeedAbbotSigned() {
        return isNeedAbbotSigned;
    }

    public void setIsNeedAbbotSigned(boolean needAbbotSigned) {
        isNeedAbbotSigned = needAbbotSigned;
    }

    public int getAbbotSignedCount() {
        return abbotSignedCount;
    }

    public void setAbbotSignedCount(int abbotSignedCount) {
        this.abbotSignedCount = abbotSignedCount;
    }

    public int getNotAbbotSignedCount() {
        return notAbbotSignedCount;
    }

    public void setNotAbbotSignedCount(int notAbbotSignedCount) {
        this.notAbbotSignedCount = notAbbotSignedCount;
    }
}
