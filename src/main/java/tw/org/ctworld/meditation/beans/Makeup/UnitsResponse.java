package tw.org.ctworld.meditation.beans.Makeup;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class UnitsResponse extends BaseResponse {
    private List<Unit> items;

    public UnitsResponse(int errCode, String errMsg, List<Unit> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<Unit> getItems() {
        return items;
    }

    public void setItems(List<Unit> items) {
        this.items = items;
    }
}
