package tw.org.ctworld.meditation.beans.Anonymous;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Anonymous011Response extends BaseResponse {

    private List<AnonymousMakeupClass> items;

    public Anonymous011Response(int errCode, String errMsg, List<AnonymousMakeupClass> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<AnonymousMakeupClass> getItems() {
        return items;
    }

    public void setItems(List<AnonymousMakeupClass> items) {
        this.items = items;
    }
}
