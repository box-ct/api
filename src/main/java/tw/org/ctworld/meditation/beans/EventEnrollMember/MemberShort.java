package tw.org.ctworld.meditation.beans.EventEnrollMember;

import tw.org.ctworld.meditation.libs.CommonUtils;

import java.util.Date;
import java.util.List;

public class MemberShort {

    private String memberId;
    private String aliasName;
    private String ctDharmaName;
    private String gender;
    private String birthDate;
    private String photo;
    private List<ClassEnrollFormShort> classList;

    public MemberShort(String memberId, String aliasName, String ctDharmaName, String gender, Date birthDate) {
        this.memberId = memberId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.gender = gender;
        this.birthDate = CommonUtils.DateTime2DateString(birthDate);
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<ClassEnrollFormShort> getClassList() {
        return classList;
    }

    public void setClassList(List<ClassEnrollFormShort> classList) {
        this.classList = classList;
    }
}
