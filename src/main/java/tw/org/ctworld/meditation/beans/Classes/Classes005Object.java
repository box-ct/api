package tw.org.ctworld.meditation.beans.Classes;

public class Classes005Object {

    private int id, classWeeksNum;
    private String classDate, classDayOfWeek, classContent, actualClassContent;
    private boolean isCancelled, isDeleted, isEdited, isAdded;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public String getClassDate() {
        return classDate;
    }

    public void setClassDate(String classDate) {
        this.classDate = classDate;
    }

    public String getClassDayOfWeek() {
        return classDayOfWeek;
    }

    public void setClassDayOfWeek(String classDayOfWeek) {
        this.classDayOfWeek = classDayOfWeek;
    }

    public String getClassContent() {
        return classContent;
    }

    public void setClassContent(String classContent) {
        this.classContent = classContent;
    }

    public String getActualClassContent() {
        return actualClassContent;
    }

    public void setActualClassContent(String actualClassContent) {
        this.actualClassContent = actualClassContent;
    }

    public boolean getIsCancelled() {
        return isCancelled;
    }

    public void setIsCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(boolean edited) {
        isEdited = edited;
    }

    public boolean getIsAdded() {
        return isAdded;
    }

    public void setIsAdded(boolean added) {
        isAdded = added;
    }
}
