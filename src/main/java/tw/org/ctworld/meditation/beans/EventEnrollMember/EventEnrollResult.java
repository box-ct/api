package tw.org.ctworld.meditation.beans.EventEnrollMember;

public class EventEnrollResult {
    private String memberId;
    private String formAutoAddNum;

    public EventEnrollResult() {
    }

    public EventEnrollResult(String memberId, String formAutoAddNum) {
        this.memberId = memberId;
        this.formAutoAddNum = formAutoAddNum;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getFormAutoAddNum() {
        return formAutoAddNum;
    }

    public void setFormAutoAddNum(String formAutoAddNum) {
        this.formAutoAddNum = formAutoAddNum;
    }
}
