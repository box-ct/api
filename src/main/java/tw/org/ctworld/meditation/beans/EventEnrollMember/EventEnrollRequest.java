package tw.org.ctworld.meditation.beans.EventEnrollMember;

import java.util.List;

public class EventEnrollRequest {

    private String
            address,
            arriveCtDate,
            arriveCtTime,
            birthDate,
            bodhiPreceptDate,
            cancelledDate,
            carLicenseNum1,
            changeNote,
            classGroupId,
            classId,
            classList,
            className,
            classNameAndJobList,
            classTypeName,
            clubJobTitleNow,
            clubJobTitlePrev,
            clubName,
            companyJobTitle,
            companyName,
            ctDormChangeHistory,
            ctDormEndDate,
            ctDormLatestChange,
            ctDormPurpose,
            ctDormStartDate,
            ctNote1,
            ctNote2,
            ctNote3,
            ctNote4,
            ctNote5,
            ctRefugeMaster,
            degreeInfo,
            donationNameList,
            dormNote,
            dsaJobNameList,
            email1,
            enrollChangeHistory,
            enrollGroupName,
            enrollLatestChange,
            enrollNote,
            enrollOrderNum,
            enrollUnitId,
            enrollUnitName,
            enrollUserCtDharmaName,
            enrollUserId,
            enrollUserName,
            eventCategoryId,
            eventCreatorUnitId,
            eventCreatorUnitName,
            eventFormUid,
            eventId,
            eventName,
            familyLeaderMemberId,
            familyLeaderName,
            formAutoAddNum,
            gender,
            goTransDate,
            goTransType,
            healthList,
            highestPreceptName,
            homeClassId,
            homeClassNote,
            homePhoneNum1,
            idType1,
            idType2,
            introducerName,
            introducerPhoneNum,
            introducerRelationship,
            jobOrder,
            jobTitle,
            leaveCtDate,
            leaveCtTime,
            leaveUnitDate,
            leaveUnitTime,
            mailingCity,
            mailingCountry,
            mailingState,
            mailingStreet,
            mailingZip,
            masterDormNote,
            masterPreceptTypeName,
            mobileNum1,
            officePhoneNum1,
            onThatDayDonation,
            parentsJobTitle,
            parentsName,
            parentsPhoneNum,
            passportNum,
            precept5Date,
            preceptOrder,
            refugeDate,
            relativeMeritList,
            relativeName,
            relativeRelationship,
            relativeTwId,
            returnTransDate,
            returnTransType,
            sameGoTransMemberName,
            sameReturnTransMemberName,
            schoolDegree,
            schoolMajor,
            schoolName,
            skillList,
            socialTitle,
            specialDonation,
            specialDonationCompleteNote,
            sponsorUnitId,
            sponsorUnitName,
            summerDonation1,
            summerDonation2,
            summerDonationType,
            summerRelativeMeritList,
            templateName,
            templateUniqueId,
            translateLanguage,
            twIdNum,
            unitId,
            unitName,
            urgentContactPersonName1,
            urgentContactPersonPhoneNum1,
            urgentContactPersonRelationship1,
            vehicleNumJobTitleList,
            vlntrEndDate,
            vlntrEndTime,
            vlntrGroupNameIdList,
            vlntrStartDate,
            vlntrStartTime;

    private Boolean
            isAbbot,
            isAbbotSigned,
            isCanceled,
            isCancelPrecept5ThisTime,
            isCancelRefugeThisTime,
            isCtDorm,
            isEarlyBookCtDorm,
            isGoCtBySelf,
            isMaster,
            isNeedExtraPrecept5CertThisTime,
            isNeedParkingPass,
            isNeedPrecept5RobeThisTime,
            isNeedTranslator,
            isNotDorm,
            isRelativePreceptOrZen7Vlntr,
            isRelativeTakePreceptOrZen7,
            isSentAfterEnrollEndDate,
            isTakeBodhiPrecept,
            isTakeExtraPrecept5ThisTime,
            isTakePrecept5,
            isTakePrecept5ThisTime,
            isTakeRefuge,
            isTakeRefugeThisTime,
            isTreasurer,
            isWaitForCtSync;

    private Integer
            age,
            classPeriodNum,
            classTypeNum,
            memberGroupNum,
            preceptVlntrCount,
            relativeAge,
            specialDonationSum,
            summerDonation1and2Sum,
            summerDonation1Count,
            summerVlntrCount,
            zen7Count,
            zen7VlntrCount;

    private Double
            height,
            weight;

    private List<EventPgmShort> genPgmList;
    private List<EventPgmShort> pgmList;

    // addition for tamplate request
    private String eventStartDate, eventStartTime, eventEndDate, eventEndTime, templateNote;
    private Boolean isShowInKiosk, isForMaster;


    public String getEventCategoryId() {
        return eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventCreatorUnitId() {
        return eventCreatorUnitId;
    }

    public void setEventCreatorUnitId(String eventCreatorUnitId) {
        this.eventCreatorUnitId = eventCreatorUnitId;
    }

    public String getEventCreatorUnitName() {
        return eventCreatorUnitName;
    }

    public void setEventCreatorUnitName(String eventCreatorUnitName) {
        this.eventCreatorUnitName = eventCreatorUnitName;
    }

    public String getSponsorUnitId() {
        return sponsorUnitId;
    }

    public void setSponsorUnitId(String sponsorUnitId) {
        this.sponsorUnitId = sponsorUnitId;
    }

    public String getSponsorUnitName() {
        return sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getEnrollUnitId() {
        return enrollUnitId;
    }

    public void setEnrollUnitId(String enrollUnitId) {
        this.enrollUnitId = enrollUnitId;
    }

    public String getEnrollUnitName() {
        return enrollUnitName;
    }

    public void setEnrollUnitName(String enrollUnitName) {
        this.enrollUnitName = enrollUnitName;
    }

    public String getEventFormUid() {
        return eventFormUid;
    }

    public void setEventFormUid(String eventFormUid) {
        this.eventFormUid = eventFormUid;
    }

    public String getTemplateUniqueId() {
        return templateUniqueId;
    }

    public void setTemplateUniqueId(String templateUniqueId) {
        this.templateUniqueId = templateUniqueId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getClassNameAndJobList() {
        return classNameAndJobList;
    }

    public void setClassNameAndJobList(String classNameAndJobList) {
        this.classNameAndJobList = classNameAndJobList;
    }

    public String getHomeClassNote() {
        return homeClassNote;
    }

    public void setHomeClassNote(String homeClassNote) {
        this.homeClassNote = homeClassNote;
    }

    public String getHomeClassId() {
        return homeClassId;
    }

    public void setHomeClassId(String homeClassId) {
        this.homeClassId = homeClassId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(String cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getEnrollOrderNum() {
        return enrollOrderNum;
    }

    public void setEnrollOrderNum(String enrollOrderNum) {
        this.enrollOrderNum = enrollOrderNum;
    }

    public String getEnrollUserId() {
        return enrollUserId;
    }

    public void setEnrollUserId(String enrollUserId) {
        this.enrollUserId = enrollUserId;
    }

    public String getEnrollUserName() {
        return enrollUserName;
    }

    public void setEnrollUserName(String enrollUserName) {
        this.enrollUserName = enrollUserName;
    }

    public String getEnrollUserCtDharmaName() {
        return enrollUserCtDharmaName;
    }

    public void setEnrollUserCtDharmaName(String enrollUserCtDharmaName) {
        this.enrollUserCtDharmaName = enrollUserCtDharmaName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getClassList() {
        return classList;
    }

    public void setClassList(String classList) {
        this.classList = classList;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getDonationNameList() {
        return donationNameList;
    }

    public void setDonationNameList(String donationNameList) {
        this.donationNameList = donationNameList;
    }

    public String getHealthList() {
        return healthList;
    }

    public void setHealthList(String healthList) {
        this.healthList = healthList;
    }

    public String getRelativeMeritList() {
        return relativeMeritList;
    }

    public void setRelativeMeritList(String relativeMeritList) {
        this.relativeMeritList = relativeMeritList;
    }

    public String getLeaveUnitDate() {
        return leaveUnitDate;
    }

    public void setLeaveUnitDate(String leaveUnitDate) {
        this.leaveUnitDate = leaveUnitDate;
    }

    public String getLeaveUnitTime() {
        return leaveUnitTime;
    }

    public void setLeaveUnitTime(String leaveUnitTime) {
        this.leaveUnitTime = leaveUnitTime;
    }

    public String getArriveCtDate() {
        return arriveCtDate;
    }

    public void setArriveCtDate(String arriveCtDate) {
        this.arriveCtDate = arriveCtDate;
    }

    public String getArriveCtTime() {
        return arriveCtTime;
    }

    public void setArriveCtTime(String arriveCtTime) {
        this.arriveCtTime = arriveCtTime;
    }

    public String getLeaveCtDate() {
        return leaveCtDate;
    }

    public void setLeaveCtDate(String leaveCtDate) {
        this.leaveCtDate = leaveCtDate;
    }

    public String getLeaveCtTime() {
        return leaveCtTime;
    }

    public void setLeaveCtTime(String leaveCtTime) {
        this.leaveCtTime = leaveCtTime;
    }

    public String getEnrollGroupName() {
        return enrollGroupName;
    }

    public void setEnrollGroupName(String enrollGroupName) {
        this.enrollGroupName = enrollGroupName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getMasterPreceptTypeName() {
        return masterPreceptTypeName;
    }

    public void setMasterPreceptTypeName(String masterPreceptTypeName) {
        this.masterPreceptTypeName = masterPreceptTypeName;
    }

    public String getIdType1() {
        return idType1;
    }

    public void setIdType1(String idType1) {
        this.idType1 = idType1;
    }

    public String getIdType2() {
        return idType2;
    }

    public void setIdType2(String idType2) {
        this.idType2 = idType2;
    }

    public String getVlntrGroupNameIdList() {
        return vlntrGroupNameIdList;
    }

    public void setVlntrGroupNameIdList(String vlntrGroupNameIdList) {
        this.vlntrGroupNameIdList = vlntrGroupNameIdList;
    }

    public String getVlntrStartDate() {
        return vlntrStartDate;
    }

    public void setVlntrStartDate(String vlntrStartDate) {
        this.vlntrStartDate = vlntrStartDate;
    }

    public String getVlntrStartTime() {
        return vlntrStartTime;
    }

    public void setVlntrStartTime(String vlntrStartTime) {
        this.vlntrStartTime = vlntrStartTime;
    }

    public String getVlntrEndDate() {
        return vlntrEndDate;
    }

    public void setVlntrEndDate(String vlntrEndDate) {
        this.vlntrEndDate = vlntrEndDate;
    }

    public String getVlntrEndTime() {
        return vlntrEndTime;
    }

    public void setVlntrEndTime(String vlntrEndTime) {
        this.vlntrEndTime = vlntrEndTime;
    }

    public String getEnrollNote() {
        return enrollNote;
    }

    public void setEnrollNote(String enrollNote) {
        this.enrollNote = enrollNote;
    }

    public String getCtDormStartDate() {
        return ctDormStartDate;
    }

    public void setCtDormStartDate(String ctDormStartDate) {
        this.ctDormStartDate = ctDormStartDate;
    }

    public String getCtDormEndDate() {
        return ctDormEndDate;
    }

    public void setCtDormEndDate(String ctDormEndDate) {
        this.ctDormEndDate = ctDormEndDate;
    }

    public String getDormNote() {
        return dormNote;
    }

    public void setDormNote(String dormNote) {
        this.dormNote = dormNote;
    }

    public String getMasterDormNote() {
        return masterDormNote;
    }

    public void setMasterDormNote(String masterDormNote) {
        this.masterDormNote = masterDormNote;
    }

    public String getFamilyLeaderMemberId() {
        return familyLeaderMemberId;
    }

    public void setFamilyLeaderMemberId(String familyLeaderMemberId) {
        this.familyLeaderMemberId = familyLeaderMemberId;
    }

    public String getFamilyLeaderName() {
        return familyLeaderName;
    }

    public void setFamilyLeaderName(String familyLeaderName) {
        this.familyLeaderName = familyLeaderName;
    }

    public String getHighestPreceptName() {
        return highestPreceptName;
    }

    public void setHighestPreceptName(String highestPreceptName) {
        this.highestPreceptName = highestPreceptName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDegreeInfo() {
        return degreeInfo;
    }

    public void setDegreeInfo(String degreeInfo) {
        this.degreeInfo = degreeInfo;
    }

    public String getSocialTitle() {
        return socialTitle;
    }

    public void setSocialTitle(String socialTitle) {
        this.socialTitle = socialTitle;
    }

    public String getSummerDonationType() {
        return summerDonationType;
    }

    public void setSummerDonationType(String summerDonationType) {
        this.summerDonationType = summerDonationType;
    }

    public String getSummerDonation1() {
        return summerDonation1;
    }

    public void setSummerDonation1(String summerDonation1) {
        this.summerDonation1 = summerDonation1;
    }

    public String getSummerDonation2() {
        return summerDonation2;
    }

    public void setSummerDonation2(String summerDonation2) {
        this.summerDonation2 = summerDonation2;
    }

    public String getSpecialDonation() {
        return specialDonation;
    }

    public void setSpecialDonation(String specialDonation) {
        this.specialDonation = specialDonation;
    }

    public String getSpecialDonationCompleteNote() {
        return specialDonationCompleteNote;
    }

    public void setSpecialDonationCompleteNote(String specialDonationCompleteNote) {
        this.specialDonationCompleteNote = specialDonationCompleteNote;
    }

    public String getOnThatDayDonation() {
        return onThatDayDonation;
    }

    public void setOnThatDayDonation(String onThatDayDonation) {
        this.onThatDayDonation = onThatDayDonation;
    }

    public String getSummerRelativeMeritList() {
        return summerRelativeMeritList;
    }

    public void setSummerRelativeMeritList(String summerRelativeMeritList) {
        this.summerRelativeMeritList = summerRelativeMeritList;
    }

    public String getUrgentContactPersonName1() {
        return urgentContactPersonName1;
    }

    public void setUrgentContactPersonName1(String urgentContactPersonName1) {
        this.urgentContactPersonName1 = urgentContactPersonName1;
    }

    public String getUrgentContactPersonRelationship1() {
        return urgentContactPersonRelationship1;
    }

    public void setUrgentContactPersonRelationship1(String urgentContactPersonRelationship1) {
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
    }

    public String getUrgentContactPersonPhoneNum1() {
        return urgentContactPersonPhoneNum1;
    }

    public void setUrgentContactPersonPhoneNum1(String urgentContactPersonPhoneNum1) {
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
    }

    public String getIntroducerName() {
        return introducerName;
    }

    public void setIntroducerName(String introducerName) {
        this.introducerName = introducerName;
    }

    public String getIntroducerRelationship() {
        return introducerRelationship;
    }

    public void setIntroducerRelationship(String introducerRelationship) {
        this.introducerRelationship = introducerRelationship;
    }

    public String getCtRefugeMaster() {
        return ctRefugeMaster;
    }

    public void setCtRefugeMaster(String ctRefugeMaster) {
        this.ctRefugeMaster = ctRefugeMaster;
    }

    public String getRefugeDate() {
        return refugeDate;
    }

    public void setRefugeDate(String refugeDate) {
        this.refugeDate = refugeDate;
    }

    public String getPrecept5Date() {
        return precept5Date;
    }

    public void setPrecept5Date(String precept5Date) {
        this.precept5Date = precept5Date;
    }

    public String getBodhiPreceptDate() {
        return bodhiPreceptDate;
    }

    public void setBodhiPreceptDate(String bodhiPreceptDate) {
        this.bodhiPreceptDate = bodhiPreceptDate;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public String getMailingZip() {
        return mailingZip;
    }

    public void setMailingZip(String mailingZip) {
        this.mailingZip = mailingZip;
    }

    public String getMailingCountry() {
        return mailingCountry;
    }

    public void setMailingCountry(String mailingCountry) {
        this.mailingCountry = mailingCountry;
    }

    public String getMailingState() {
        return mailingState;
    }

    public void setMailingState(String mailingState) {
        this.mailingState = mailingState;
    }

    public String getMailingCity() {
        return mailingCity;
    }

    public void setMailingCity(String mailingCity) {
        this.mailingCity = mailingCity;
    }

    public String getMailingStreet() {
        return mailingStreet;
    }

    public void setMailingStreet(String mailingStreet) {
        this.mailingStreet = mailingStreet;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyJobTitle() {
        return companyJobTitle;
    }

    public void setCompanyJobTitle(String companyJobTitle) {
        this.companyJobTitle = companyJobTitle;
    }

    public String getOfficePhoneNum1() {
        return officePhoneNum1;
    }

    public void setOfficePhoneNum1(String officePhoneNum1) {
        this.officePhoneNum1 = officePhoneNum1;
    }

    public String getSchoolDegree() {
        return schoolDegree;
    }

    public void setSchoolDegree(String schoolDegree) {
        this.schoolDegree = schoolDegree;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolMajor() {
        return schoolMajor;
    }

    public void setSchoolMajor(String schoolMajor) {
        this.schoolMajor = schoolMajor;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getClubJobTitleNow() {
        return clubJobTitleNow;
    }

    public void setClubJobTitleNow(String clubJobTitleNow) {
        this.clubJobTitleNow = clubJobTitleNow;
    }

    public String getClubJobTitlePrev() {
        return clubJobTitlePrev;
    }

    public void setClubJobTitlePrev(String clubJobTitlePrev) {
        this.clubJobTitlePrev = clubJobTitlePrev;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getSkillList() {
        return skillList;
    }

    public void setSkillList(String skillList) {
        this.skillList = skillList;
    }

    public String getCarLicenseNum1() {
        return carLicenseNum1;
    }

    public void setCarLicenseNum1(String carLicenseNum1) {
        this.carLicenseNum1 = carLicenseNum1;
    }

    public String getTranslateLanguage() {
        return translateLanguage;
    }

    public void setTranslateLanguage(String translateLanguage) {
        this.translateLanguage = translateLanguage;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getCtDormPurpose() {
        return ctDormPurpose;
    }

    public void setCtDormPurpose(String ctDormPurpose) {
        this.ctDormPurpose = ctDormPurpose;
    }

    public String getRelativeName() {
        return relativeName;
    }

    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName;
    }

    public Integer getRelativeAge() {
        return relativeAge;
    }

    public void setRelativeAge(Integer relativeAge) {
        this.relativeAge = relativeAge;
    }

    public String getRelativeTwId() {
        return relativeTwId;
    }

    public void setRelativeTwId(String relativeTwId) {
        this.relativeTwId = relativeTwId;
    }

    public String getRelativeRelationship() {
        return relativeRelationship;
    }

    public void setRelativeRelationship(String relativeRelationship) {
        this.relativeRelationship = relativeRelationship;
    }

    public String getParentsName() {
        return parentsName;
    }

    public void setParentsName(String parentsName) {
        this.parentsName = parentsName;
    }

    public String getParentsPhoneNum() {
        return parentsPhoneNum;
    }

    public void setParentsPhoneNum(String parentsPhoneNum) {
        this.parentsPhoneNum = parentsPhoneNum;
    }

    public String getParentsJobTitle() {
        return parentsJobTitle;
    }

    public void setParentsJobTitle(String parentsJobTitle) {
        this.parentsJobTitle = parentsJobTitle;
    }

    public String getVehicleNumJobTitleList() {
        return vehicleNumJobTitleList;
    }

    public void setVehicleNumJobTitleList(String vehicleNumJobTitleList) {
        this.vehicleNumJobTitleList = vehicleNumJobTitleList;
    }

    public String getEnrollLatestChange() {
        return enrollLatestChange;
    }

    public void setEnrollLatestChange(String enrollLatestChange) {
        this.enrollLatestChange = enrollLatestChange;
    }

    public String getEnrollChangeHistory() {
        return enrollChangeHistory;
    }

    public void setEnrollChangeHistory(String enrollChangeHistory) {
        this.enrollChangeHistory = enrollChangeHistory;
    }

    public String getCtDormLatestChange() {
        return ctDormLatestChange;
    }

    public void setCtDormLatestChange(String ctDormLatestChange) {
        this.ctDormLatestChange = ctDormLatestChange;
    }

    public String getCtDormChangeHistory() {
        return ctDormChangeHistory;
    }

    public void setCtDormChangeHistory(String ctDormChangeHistory) {
        this.ctDormChangeHistory = ctDormChangeHistory;
    }

    public String getCtNote1() {
        return ctNote1;
    }

    public void setCtNote1(String ctNote1) {
        this.ctNote1 = ctNote1;
    }

    public String getCtNote2() {
        return ctNote2;
    }

    public void setCtNote2(String ctNote2) {
        this.ctNote2 = ctNote2;
    }

    public String getCtNote3() {
        return ctNote3;
    }

    public void setCtNote3(String ctNote3) {
        this.ctNote3 = ctNote3;
    }

    public String getCtNote4() {
        return ctNote4;
    }

    public void setCtNote4(String ctNote4) {
        this.ctNote4 = ctNote4;
    }

    public String getCtNote5() {
        return ctNote5;
    }

    public void setCtNote5(String ctNote5) {
        this.ctNote5 = ctNote5;
    }

    public Boolean getIsCanceled() {
        return isCanceled;
    }

    public void setCanceled(Boolean canceled) {
        isCanceled = canceled;
    }

    public Boolean getIsMaster() {
        return isMaster;
    }

    public void setMaster(Boolean master) {
        isMaster = master;
    }

    public Boolean getIsTreasurer() {
        return isTreasurer;
    }

    public void setTreasurer(Boolean treasurer) {
        isTreasurer = treasurer;
    }

    public Boolean getIsAbbot() {
        return isAbbot;
    }

    public void setAbbot(Boolean abbot) {
        isAbbot = abbot;
    }

    public Boolean getIsAbbotSigned() {
        return isAbbotSigned;
    }

    public void setIsAbbotSigned(Boolean abbotSigned) {
        isAbbotSigned = abbotSigned;
    }

    public Boolean getIsGoCtBySelf() {
        return isGoCtBySelf;
    }

    public void setGoCtBySelf(Boolean goCtBySelf) {
        isGoCtBySelf = goCtBySelf;
    }

    public Boolean getIsCtDorm() {
        return isCtDorm;
    }

    public void setCtDorm(Boolean ctDorm) {
        isCtDorm = ctDorm;
    }

    public Boolean getIsTakePrecept5() {
        return isTakePrecept5;
    }

    public void setTakePrecept5(Boolean takePrecept5) {
        isTakePrecept5 = takePrecept5;
    }

    public Boolean getIsTakeBodhiPrecept() {
        return isTakeBodhiPrecept;
    }

    public void setTakeBodhiPrecept(Boolean takeBodhiPrecept) {
        isTakeBodhiPrecept = takeBodhiPrecept;
    }

    public Boolean getIsNotDorm() {
        return isNotDorm;
    }

    public void setNotDorm(Boolean notDorm) {
        isNotDorm = notDorm;
    }

    public Boolean getIsIsSentAfterEnrollEndDate() {
        return isSentAfterEnrollEndDate;
    }

    public void setIsSentAfterEnrollEndDate(Boolean isSentAfterEnrollEndDate) {
        this.isSentAfterEnrollEndDate = isSentAfterEnrollEndDate;
    }

    public Boolean getIsNeedParkingPass() {
        return isNeedParkingPass;
    }

    public void setNeedParkingPass(Boolean needParkingPass) {
        isNeedParkingPass = needParkingPass;
    }

    public Boolean getIsTakeRefuge() {
        return isTakeRefuge;
    }

    public void setTakeRefuge(Boolean takeRefuge) {
        isTakeRefuge = takeRefuge;
    }

    public Boolean getIsNeedTranslator() {
        return isNeedTranslator;
    }

    public void setNeedTranslator(Boolean needTranslator) {
        isNeedTranslator = needTranslator;
    }

    public Boolean getIsTakeRefugeThisTime() {
        return isTakeRefugeThisTime;
    }

    public void setTakeRefugeThisTime(Boolean takeRefugeThisTime) {
        isTakeRefugeThisTime = takeRefugeThisTime;
    }

    public Boolean getIsCancelRefugeThisTime() {
        return isCancelRefugeThisTime;
    }

    public void setCancelRefugeThisTime(Boolean cancelRefugeThisTime) {
        isCancelRefugeThisTime = cancelRefugeThisTime;
    }

    public Boolean getIsTakePrecept5ThisTime() {
        return isTakePrecept5ThisTime;
    }

    public void setTakePrecept5ThisTime(Boolean takePrecept5ThisTime) {
        isTakePrecept5ThisTime = takePrecept5ThisTime;
    }

    public Boolean getIsCancelPrecept5ThisTime() {
        return isCancelPrecept5ThisTime;
    }

    public void setCancelPrecept5ThisTime(Boolean cancelPrecept5ThisTime) {
        isCancelPrecept5ThisTime = cancelPrecept5ThisTime;
    }

    public Boolean getIsTakeExtraPrecept5ThisTime() {
        return isTakeExtraPrecept5ThisTime;
    }

    public void setTakeExtraPrecept5ThisTime(Boolean takeExtraPrecept5ThisTime) {
        isTakeExtraPrecept5ThisTime = takeExtraPrecept5ThisTime;
    }

    public Boolean getIsNeedPrecept5RobeThisTime() {
        return isNeedPrecept5RobeThisTime;
    }

    public void setNeedPrecept5RobeThisTime(Boolean needPrecept5RobeThisTime) {
        isNeedPrecept5RobeThisTime = needPrecept5RobeThisTime;
    }

    public Boolean getIsNeedExtraPrecept5CertThisTime() {
        return isNeedExtraPrecept5CertThisTime;
    }

    public void setNeedExtraPrecept5CertThisTime(Boolean needExtraPrecept5CertThisTime) {
        isNeedExtraPrecept5CertThisTime = needExtraPrecept5CertThisTime;
    }

    public Boolean getIsEarlyBookCtDorm() {
        return isEarlyBookCtDorm;
    }

    public void setEarlyBookCtDorm(Boolean earlyBookCtDorm) {
        isEarlyBookCtDorm = earlyBookCtDorm;
    }

    public Boolean getIsRelativeTakePreceptOrZen7() {
        return isRelativeTakePreceptOrZen7;
    }

    public void setRelativeTakePreceptOrZen7(Boolean relativeTakePreceptOrZen7) {
        isRelativeTakePreceptOrZen7 = relativeTakePreceptOrZen7;
    }

    public Boolean getIsRelativePreceptOrZen7Vlntr() {
        return isRelativePreceptOrZen7Vlntr;
    }

    public void setRelativePreceptOrZen7Vlntr(Boolean relativePreceptOrZen7Vlntr) {
        isRelativePreceptOrZen7Vlntr = relativePreceptOrZen7Vlntr;
    }

    public Boolean getIsWaitForCtSync() {
        return isWaitForCtSync;
    }

    public void setWaitForCtSync(Boolean waitForCtSync) {
        isWaitForCtSync = waitForCtSync;
    }

    public String getFormAutoAddNum() {
        return formAutoAddNum;
    }

    public void setFormAutoAddNum(String formAutoAddNum) {
        this.formAutoAddNum = formAutoAddNum;
    }

    public Integer getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(Integer memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public Integer getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(Integer classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public Integer getClassTypeNum() {
        return classTypeNum;
    }

    public void setClassTypeNum(Integer classTypeNum) {
        this.classTypeNum = classTypeNum;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getJobOrder() {
        return jobOrder;
    }

    public void setJobOrder(String jobOrder) {
        this.jobOrder = jobOrder;
    }

    public String getPreceptOrder() {
        return preceptOrder;
    }

    public void setPreceptOrder(String preceptOrder) {
        this.preceptOrder = preceptOrder;
    }

    public Integer getSummerDonation1Count() {
        return summerDonation1Count;
    }

    public void setSummerDonation1Count(Integer summerDonation1Count) {
        this.summerDonation1Count = summerDonation1Count;
    }

    public Integer getSummerDonation1and2Sum() {
        return summerDonation1and2Sum;
    }

    public void setSummerDonation1and2Sum(Integer summerDonation1and2Sum) {
        this.summerDonation1and2Sum = summerDonation1and2Sum;
    }

    public Integer getSpecialDonationSum() {
        return specialDonationSum;
    }

    public void setSpecialDonationSum(Integer specialDonationSum) {
        this.specialDonationSum = specialDonationSum;
    }

    public String getIntroducerPhoneNum() {
        return introducerPhoneNum;
    }

    public void setIntroducerPhoneNum(String introducerPhoneNum) {
        this.introducerPhoneNum = introducerPhoneNum;
    }

    public Integer getZen7Count() {
        return zen7Count;
    }

    public void setZen7Count(Integer zen7Count) {
        this.zen7Count = zen7Count;
    }

    public Integer getZen7VlntrCount() {
        return zen7VlntrCount;
    }

    public void setZen7VlntrCount(Integer zen7VlntrCount) {
        this.zen7VlntrCount = zen7VlntrCount;
    }

    public Integer getSummerVlntrCount() {
        return summerVlntrCount;
    }

    public void setSummerVlntrCount(Integer summerVlntrCount) {
        this.summerVlntrCount = summerVlntrCount;
    }

    public Integer getPreceptVlntrCount() {
        return preceptVlntrCount;
    }

    public void setPreceptVlntrCount(Integer preceptVlntrCount) {
        this.preceptVlntrCount = preceptVlntrCount;
    }

    public List<EventPgmShort> getGenPgmList() {
        return genPgmList;
    }

    public void setGenPgmList(List<EventPgmShort> genPgmList) {
        this.genPgmList = genPgmList;
    }

    public List<EventPgmShort> getPgmList() {
        return pgmList;
    }

    public void setPgmList(List<EventPgmShort> pgmList) {
        this.pgmList = pgmList;
    }

    public String getGoTransType() {
        return goTransType;
    }

    public void setGoTransType(String goTransType) {
        this.goTransType = goTransType;
    }

    public String getGoTransDate() {
        return goTransDate;
    }

    public void setGoTransDate(String goTransDate) {
        this.goTransDate = goTransDate;
    }

    public String getReturnTransType() {
        return returnTransType;
    }

    public void setReturnTransType(String returnTransType) {
        this.returnTransType = returnTransType;
    }

    public String getReturnTransDate() {
        return returnTransDate;
    }

    public void setReturnTransDate(String returnTransDate) {
        this.returnTransDate = returnTransDate;
    }

    public String getTemplateNote() {
        return templateNote;
    }

    public void setTemplateNote(String templateNote) {
        this.templateNote = templateNote;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public Boolean getIsShowInKiosk() {
        return isShowInKiosk;
    }

    public void setIsShowInKiosk(Boolean showInKiosk) {
        isShowInKiosk = showInKiosk;
    }

    public Boolean getIsForMaster() {
        return isForMaster;
    }

    public void setIsForMaster(Boolean forMaster) {
        isForMaster = forMaster;
    }

    public String getChangeNote() {
        return changeNote;
    }

    public String getSameGoTransMemberName() {
        return sameGoTransMemberName;
    }

    public void setSameGoTransMemberName(String sameGoTransMemberName) {
        this.sameGoTransMemberName = sameGoTransMemberName;
    }

    public String getSameReturnTransMemberName() {
        return sameReturnTransMemberName;
    }

    public void setSameReturnTransMemberName(String sameReturnTransMemberName) {
        this.sameReturnTransMemberName = sameReturnTransMemberName;
    }

    public void setChangeNote(String changeNote) {
        this.changeNote = changeNote;
    }


}
