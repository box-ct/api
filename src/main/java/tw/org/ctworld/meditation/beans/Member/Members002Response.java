package tw.org.ctworld.meditation.beans.Member;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class Members002Response extends BaseResponse {

    private Members002ProfileObject profile;
    private List<Members002DuplicateNameObject> duplicateName;
    private List<Members002DuplicateAddressObject> duplicateAddress;
    private List<Members002DuplicateFamilyObject> duplicateFamily;
    private List<Members002MeditationRecordObject> meditationRecord;
    private List<Members002ActivityRecordObject> activityRecord;
    private List<Members002RegistrationRecordObject> registrationRecord;
    private List<Members002ClassCtMemberNoteObject> classCtMemberNote;
    private Members002ImagesObject images;

    public Members002Response(int errCode, String errMsg, Members002ProfileObject profile,
                              List<Members002DuplicateNameObject> duplicateName,
                              List<Members002DuplicateAddressObject> duplicateAddress,
                              List<Members002DuplicateFamilyObject> duplicateFamily,
                              List<Members002MeditationRecordObject> meditationRecord,
                              List<Members002ActivityRecordObject> activityRecord,
                              List<Members002RegistrationRecordObject> registrationRecord,
                              List<Members002ClassCtMemberNoteObject> classCtMemberNote,
                              Members002ImagesObject images) {
        super(errCode, errMsg);
        this.profile = profile;
        this.duplicateName = duplicateName;
        this.duplicateAddress = duplicateAddress;
        this.duplicateFamily = duplicateFamily;
        this.meditationRecord = meditationRecord;
        this.activityRecord = activityRecord;
        this.registrationRecord = registrationRecord;
        this.classCtMemberNote = classCtMemberNote;
        this.images = images;
    }

    public Members002ProfileObject getProfile() {
        return profile;
    }

    public void setProfile(Members002ProfileObject profile) {
        this.profile = profile;
    }

    public List<Members002DuplicateNameObject> getDuplicateName() {
        return duplicateName;
    }

    public void setDuplicateName(List<Members002DuplicateNameObject> duplicateName) {
        this.duplicateName = duplicateName;
    }

    public List<Members002DuplicateAddressObject> getDuplicateAddress() {
        return duplicateAddress;
    }

    public void setDuplicateAddress(List<Members002DuplicateAddressObject> duplicateAddress) {
        this.duplicateAddress = duplicateAddress;
    }

    public List<Members002DuplicateFamilyObject> getDuplicateFamily() {
        return duplicateFamily;
    }

    public void setDuplicateFamily(List<Members002DuplicateFamilyObject> duplicateFamily) {
        this.duplicateFamily = duplicateFamily;
    }

    public List<Members002MeditationRecordObject> getMeditationRecord() {
        return meditationRecord;
    }

    public void setMeditationRecord(List<Members002MeditationRecordObject> meditationRecord) {
        this.meditationRecord = meditationRecord;
    }

    public List<Members002ActivityRecordObject> getActivityRecord() {
        return activityRecord;
    }

    public void setActivityRecord(List<Members002ActivityRecordObject> activityRecord) {
        this.activityRecord = activityRecord;
    }

    public List<Members002RegistrationRecordObject> getRegistrationRecord() {
        return registrationRecord;
    }

    public void setRegistrationRecord(List<Members002RegistrationRecordObject> registrationRecord) {
        this.registrationRecord = registrationRecord;
    }

    public List<Members002ClassCtMemberNoteObject> getClassCtMemberNote() {
        return classCtMemberNote;
    }

    public void setClassCtMemberNote(List<Members002ClassCtMemberNoteObject> classCtMemberNote) {
        this.classCtMemberNote = classCtMemberNote;
    }

    public Members002ImagesObject getImages() {
        return images;
    }

    public void setImages(Members002ImagesObject images) {
        this.images = images;
    }
}
