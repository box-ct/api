package tw.org.ctworld.meditation.beans.DataSearch;

import tw.org.ctworld.meditation.beans.BaseResponse;

import java.util.List;

public class DataSearchGroupSubTotalResponse extends BaseResponse {

    private List<ClassSubTotal> items;

    public DataSearchGroupSubTotalResponse(int errCode, String errMsg, List<ClassSubTotal> items) {
        super(errCode, errMsg);
        this.items = items;
    }

    public List<ClassSubTotal> getItems() {
        return items;
    }

    public void setItems(List<ClassSubTotal> items) {
        this.items = items;
    }

    public static class ClassSubTotal {
        private String className;
        private String gender;
        private List<GroupSubTotal> groups;

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public List<GroupSubTotal> getGroups() {
            return groups;
        }

        public void setGroups(List<GroupSubTotal> groups) {
            this.groups = groups;
        }
    }

    public static class GroupSubTotal {
        private String groupId;
        private int number;

        public GroupSubTotal() {
        }

        public GroupSubTotal(String groupId, int number) {
            this.groupId = groupId;
            this.number = number;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }
    }
}
