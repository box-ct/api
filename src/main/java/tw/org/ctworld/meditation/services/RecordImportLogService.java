package tw.org.ctworld.meditation.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.DataImport.ExcelMemberEnroll;
import tw.org.ctworld.meditation.models.RecordImportLog;
import tw.org.ctworld.meditation.repositories.RecordImportLogRepo;

import javax.servlet.http.HttpSession;

@Service
public class RecordImportLogService {

    @Autowired
    RecordImportLogRepo recordImportLogRepo;

    public RecordImportLog add(HttpSession session, ExcelMemberEnroll enroll) {
        RecordImportLog log = new RecordImportLog();
        log.setCreatorAndUpdater(session);

        log.setAliasName(enroll.getName());
        log.setTwIdNum(enroll.getTwIdParsed());
        log.setBirthDate(enroll.getBirthDate());

        log.setMemberId(enroll.getMemberId());
        log.setCtDharmaName(enroll.getCtName());
        log.setClassEnrollFormId(enroll.getClassId());
        log.setImportErrorContent(enroll.getImportErrorContent());
        log.setImportResultType(enroll.getImportResultType());
        log.setSuccessUpdateContent(enroll.getSuccessUpdateContent());
        log.setUnitId(enroll.getUnitId());
        log.setUnitName(enroll.getUnitName());
        log.setClassEnrollFormId(enroll.getFormId());
        log.setMemberId(enroll.getMemberId());

        return recordImportLogRepo.save(log);
    }
}
