package tw.org.ctworld.meditation.services;

import org.hibernate.QueryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.DataSearch.DataSearchEventEnrollObject;
import tw.org.ctworld.meditation.beans.EventAbbotSign.AbbotSignRequest;
import tw.org.ctworld.meditation.beans.EventAbbotSign.CtApprovedNoteRequest;
import tw.org.ctworld.meditation.beans.EventAbbotSign.CtApprovedRequest;
import tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort;
import tw.org.ctworld.meditation.beans.EventEnrollMember.*;
import tw.org.ctworld.meditation.beans.Member.Members002ImagesObject;
import tw.org.ctworld.meditation.beans.PageResponse;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.libs.RedundantException;
import tw.org.ctworld.meditation.libs.UnacceptableException;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.models.EventMain;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EventEnrollMainService {

    private static final Logger logger = LoggerFactory.getLogger(EventEnrollMainService.class);

    @Autowired
    private EventEnrollMainRepo eventEnrollMainRepo;

    @Autowired
    private CtMemberInfoRepo ctMemberInfoRepo;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;

    @Autowired
    private EventPgmDefRepo eventPgmDefRepo;

    @Autowired
    private EventPgmUnitRepo eventPgmUnitRepo;

    @Autowired
    private EventEnrollPgmRepo eventEnrollPgmRepo;

    @Autowired
    private EventMainRepo eventMainRepo;

    @Autowired
    private ClassEnrollFormRepo classEnrollFormRepo;

    @Autowired
    private EventSummerDonationRecordRepo summerDonationRecordRepo;

    @Autowired
    private EventUnitInfoRepo eventUnitInfoRepo;

    @Autowired
    private EventUnitVehicleInfoRepo eventUnitVehicleInfoRepo;

    @Autowired
    private UnitMasterInfoRepo unitMasterInfoRepo;

    @Autowired
    private CtMasterInfoRepo ctMasterInfoRepo;

    @Autowired
    private UnitMasterRepo unitMasterRepo;

    @Autowired
    private ClassInfoRepo classInfoRepo;

    // Data Search - get event enrolls by unit and events
    public List<DataSearchEventEnrollObject> getClassEventEnrollsByUnitEventIds(String unitId, List<String> eventIds) {

        return eventEnrollMainRepo.GetEventEnrollMainByUnitEvents(unitId, eventIds);
    }

    public List<EnrollMain> getEnrolledMembersByEventId(String unitId, String eventId) {

        List<EventEnrollMain> enrolls = eventEnrollMainRepo.GetEnrolledMembersByEventId(unitId, eventId);
        List<String> memberIds = enrolls.stream().map(e -> e.getEnrollUserId()).distinct().collect(Collectors.toList());
        List<Members002ImagesObject> photos = ctMemberInfoService.getPhotosByMemberIdList(memberIds, true);
        List<EnrollMain> records = new ArrayList<>();

        for (EventEnrollMain enroll : enrolls) {
            Optional<Members002ImagesObject> photo = photos.stream().filter(p -> p.getMemberId().equals(enroll.getEnrollUserId())).findAny();

            EnrollMain enrollMain = new EnrollMain(enroll);

            if (photo.isPresent()) {
                enrollMain.setPhoto(photo.get().getPhoto());
            }

            records.add(enrollMain);
        }

        return records;
    }

    public List<EnrollMain> getEnrolledMasterByEventId(String type, String unitId, String eventId) {

        if (type.toUpperCase().equals("CT")) {
            unitId = null;
        }

        List<EventEnrollMain> enrolls = eventEnrollMainRepo.GetEnrolledMastersByEventId(unitId, eventId);

        List<EnrollMain> records = new ArrayList<>();

        for (EventEnrollMain enroll : enrolls) {
            EnrollMain enrollMain = new EnrollMain(enroll);
            records.add(enrollMain);
        }

        return records;
    }

    // add new event_enroll_main
    public EventEnrollMain enroll(HttpSession session, EventEnrollRequest request, boolean isMaster) throws RedundantException, NotFoundException {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
        String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(request.getEventId());

        if (eventMain != null) {

            // check for multiple enroll
            if (!eventMain.getIsMultipleEnroll()) {
                long count = eventEnrollMainRepo.CheckEnrollByMemberIdEventId(request.getEventId(), request.getEnrollUserId());

                if (count > 0) {
                    throw new RedundantException();
                }
            }

            CtMemberInfo memberInfo = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId(request.getEnrollUserId());

            if (memberInfo != null) {
                request.setUnitId(memberInfo.getUnitId());
                request.setUnitName(memberInfo.getUnitName());
            }

            request.setEnrollUnitId(unitId);
            request.setEnrollUnitName(unitName);

            // add event enroll main
            EventEnrollMain enrollMain = new EventEnrollMain();
            if (isMaster) {
                enrollMain.copyEnroll4Master(request, false);

                if (request.getEnrollUserId().substring(0, 1).toUpperCase().equals("A")) {
                    enrollMain.setEventFormUid(genEventFormUid(enrollMain.getUnitId(), "A"));
                } else {
                    enrollMain.setEventFormUid(genEventFormUid(enrollMain.getUnitId(), "B"));
                }
            } else {
                enrollMain.copyEnroll(request, false);
                String classId = request.getClassId();
                ClassInfo classInfo = classId != null ? classInfoRepo.GetClassInfoByClassId(classId) : null;
                ClassEnrollForm classEnrollForms = classId != null ? classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(request.getEnrollUserId(), classId) : null;

                if (classInfo != null) {
                    enrollMain.copyClassInfo(classInfo);
                }

                if (classEnrollForms != null) {
                    enrollMain.copyClassEnrollForm(classEnrollForms);
                }
                enrollMain.setEventFormUid(genEventFormUid(enrollMain.getUnitId(), enrollMain.getGender()));
            }

            enrollMain.setCreatorAndUpdater(session);
            enrollMain.setIsWaitForCtSync(true);
            enrollMain.setChangeEndDate(eventMain.getChangeEndDate());
            enrollMain.setFormAutoAddNum(genFormAutoAddNum());
            eventEnrollMainRepo.save(enrollMain);

            if (isMaster) {
                updateUnitMaster(session, enrollMain);
            } else {
                // update ct member info
                updateMember(session, enrollMain);
            }

            // add pgms
            if ((request.getPgmList() != null && request.getPgmList().size() > 0) || ((request.getGenPgmList() != null && request.getGenPgmList().size() > 0))) {
                addPgms(request, unitId, enrollMain, session);
            }

            return enrollMain;
        } else {
            throw new NotFoundException();
        }
    }

    // create new event enroll pgm (not saved)
    private EventEnrollPgm createEventEnrollPgm(EventEnrollMain enrollMain, EventPgmUnit eventPgmUnit, String genPgmUserInputValue, HttpSession session) {

        String userId = (String) session.getAttribute(UserSession.Keys.UserId.getValue());
        String userName = (String) session.getAttribute(UserSession.Keys.UserName.getValue());

        EventEnrollPgm eventEnrollPgm = new EventEnrollPgm();
        eventEnrollPgm.setUnitId(enrollMain.getEnrollUserId());
        eventEnrollPgm.setUnitName(enrollMain.getEnrollUserName());
        eventEnrollPgm.setEventId(enrollMain.getEventId());
        eventEnrollPgm.setEventName(enrollMain.getEventName());
        eventEnrollPgm.setEventCreatorUnitId(enrollMain.getEventCreatorUnitId());
        eventEnrollPgm.setEventCreatorUnitName(enrollMain.getEventCreatorUnitName());
        eventEnrollPgm.setSponsorUnitId(enrollMain.getSponsorUnitId());
        eventEnrollPgm.setSponsorUnitName(enrollMain.getSponsorUnitName());
        eventEnrollPgm.setEnrollUserId(userId);
        eventEnrollPgm.setEnrollUserName(userName);
        eventEnrollPgm.setFormAutoAddNum(enrollMain.getFormAutoAddNum());
        eventEnrollPgm.setGenPgmUserInputValue(genPgmUserInputValue);
        eventEnrollPgm.setIsDisabledByOCA(eventPgmUnit.getIsDisabledByOCA() != null ? eventPgmUnit.getIsDisabledByOCA() : false);
        eventEnrollPgm.setIsRequired(eventPgmUnit.getIsRequired() != null ? eventPgmUnit.getIsRequired() : false);
        eventEnrollPgm.setIsGenPgm(eventPgmUnit.getIsGenPgm() != null ? eventPgmUnit.getIsGenPgm() : false);
        eventEnrollPgm.setPgmCategoryId(eventPgmUnit.getPgmCategoryId());
        eventEnrollPgm.setPgmCategoryName(eventPgmUnit.getPgmCategoryName());
        eventEnrollPgm.setPgmId(eventPgmUnit.getPgmId());
        eventEnrollPgm.setPgmName(eventPgmUnit.getPgmName());
        eventEnrollPgm.setPgmUniqueId(eventPgmUnit.getPgmUniqueId());
        eventEnrollPgm.setPgmCtOrderNum(eventPgmUnit.getPgmCtOrderNum());
        eventEnrollPgm.setPgmDate(eventPgmUnit.getPgmDate());
        eventEnrollPgm.setPgmDesc(eventPgmUnit.getPgmDesc());
        eventEnrollPgm.setPgmNote(eventPgmUnit.getPgmNote());
        eventEnrollPgm.setPgmStartTime(eventPgmUnit.getPgmStartTime());
        eventEnrollPgm.setPgmEndTime(eventPgmUnit.getPgmEndTime());
        eventEnrollPgm.setPgmUiOrderNum(eventPgmUnit.getPgmUiOrderNum());

        eventEnrollPgm.setCreatorAndUpdater(session);

        return eventEnrollPgm;
    }

    // generate max + 1 formAutoAddNum of event_enroll_main
    private String genFormAutoAddNum() {
        String prefix = "EEF" + CommonUtils.GetMinGoDateStr();

        String maxAutoAddNum = eventEnrollMainRepo.GetMaxFormAutoAddNum(prefix);

        int max = 0;

        if (maxAutoAddNum != null) {
            max = Integer.parseInt(maxAutoAddNum.substring(maxAutoAddNum.length() - 7, maxAutoAddNum.length()));
        }

        max++;

        return prefix + String.format("%07d", max);
    }

    // generate max + 1 eventFormUid of event_enroll_main
    private String genEventFormUid(String memberUnitId, String gender) {

        String prefix = "EFUID" + memberUnitId.substring(memberUnitId.length() - 5, memberUnitId.length()) + gender.substring(0, 1).toUpperCase();

        String maxEventFormUid = eventEnrollMainRepo.GetMaxEventFormUid(prefix);

        int max = 0;

        if (maxEventFormUid != null) {
            max = Integer.parseInt(maxEventFormUid.substring(maxEventFormUid.length() - 7, maxEventFormUid.length()));
        }

        max++;

        return prefix + String.format("%07d", max);
    }

    // get next id from current id not db
    private String incrementGeneratedId(String currentId, int length) {
        String prefix = currentId.substring(0, currentId.length() - length);
        int current = Integer.parseInt(currentId.substring(currentId.length() - length));
        current++;

        return prefix + String.format("%0" + length + "d", current);
    }

    // get event enroll main and enroll pgm
    public EventEnrollMainPgmResponse getEnroll(String eventId, String formAutoAddNum, boolean isMaster) {
        EventEnrollMainPgmResponse response = new EventEnrollMainPgmResponse();
        response.setErrCode(200);
        response.setErrMsg("success");

        // get enroll main
        List<EventEnrollMain> eventEnrollMains = eventEnrollMainRepo.GetEnrolledByEventIdFormAutoAddNum(eventId, formAutoAddNum);

        if (eventEnrollMains.size() > 0) {
            response.setResult(eventEnrollMains.get(0));

            // get enroll pgm
            List<EventEnrollPgm> eventEnrollPgms = eventEnrollPgmRepo.GetByEventIdFormAutoAddNum(eventId, formAutoAddNum);

            List<EventPgmShort> genPgmList = new ArrayList<>();
            List<EventPgmShort> pgmList = new ArrayList<>();

            response.setGenPgmList(genPgmList);
            response.setPgmList(pgmList);

            if (!isMaster) {
                Members002ImagesObject photos = ctMemberInfoService.getPhotosByMemberId(eventEnrollMains.get(0).getEnrollUserId());

                if (photos != null && photos.getPhoto() != null) {
                    response.getResult().setPhoto(photos.getPhoto());
                }
            }

            response.getResult().setLeaderText(buildLeaderText(eventId, response.getResult().getUnitId(), response.getResult().getEnrollUserId()));

            for (EventEnrollPgm eventEnrollPgm : eventEnrollPgms) {
                EventPgmShort pgmShort = new EventPgmShort();
                pgmShort.setPgmId(eventEnrollPgm.getPgmId());
                pgmShort.setPgmUniqueId(eventEnrollPgm.getPgmUniqueId());
                pgmShort.setGenPgmUserInputValue(eventEnrollPgm.getGenPgmUserInputValue());

                if (eventEnrollPgm.getIsGenPgm()) {
                    genPgmList.add(pgmShort);
                } else {
                    pgmList.add(pgmShort);
                }
            }

        } else {
            response.setErrCode(404);
            response.setErrMsg("活動報名表不存在");
        }

        return response;
    }

    private String buildLeaderText(String eventId, String unitId, String memberId) {

        StringBuilder sb = new StringBuilder();

        EventUnitInfo eventUnitInfo = eventUnitInfoRepo.GetEventUnitInfoDetailByEventIdAndUnitId(eventId, unitId);

        List<EventUnitVehicleInfo> eventUnitVehicleInfoList = eventUnitVehicleInfoRepo.GetByEventIdAndUnitId(eventId, unitId);

        if (eventUnitInfo != null) {
            if (eventUnitInfo.getEventLeaderMemberId() != null && eventUnitInfo.getEventLeaderMemberId().equals(memberId)) {
                sb.append("活動總領隊/");
            }

            if (eventUnitInfo.getViceEventLeader1MemberId() != null && eventUnitInfo.getViceEventLeader1MemberId().equals(memberId)) {
                sb.append("活動副領隊1/");
            }

            if (eventUnitInfo.getViceEventLeader2MemberId() != null && eventUnitInfo.getViceEventLeader2MemberId().equals(memberId)) {
                sb.append("活動副領隊2/");
            }
        }

        if (eventUnitVehicleInfoList.size() > 0) {
            for (EventUnitVehicleInfo vehicleInfo : eventUnitVehicleInfoList) {
                if (vehicleInfo.getMasterId() != null && vehicleInfo.getMasterId().equals(memberId)) {
                    sb.append(vehicleInfo.getVehicleType() + "(" + vehicleInfo.getVehicleNum() + ")車:隨車法師/");
                }

                if (vehicleInfo.getBusLeaderMemberId() != null && vehicleInfo.getBusLeaderMemberId().equals(memberId)) {
                    sb.append(vehicleInfo.getVehicleType() + "(" + vehicleInfo.getVehicleNum() + ")車:領隊/");
                }

                if (vehicleInfo.getViceBusLeader1MemberId() != null && vehicleInfo.getViceBusLeader1MemberId().equals(memberId)) {
                    sb.append(vehicleInfo.getVehicleType() + "(" + vehicleInfo.getVehicleNum() + ")車:副領隊1/");
                }

                if (vehicleInfo.getViceBusLeader2MemberId() != null && vehicleInfo.getViceBusLeader2MemberId().equals(memberId)) {
                    sb.append(vehicleInfo.getVehicleType() + "(" + vehicleInfo.getVehicleNum() + ")車:副領隊2/");
                }

                if (vehicleInfo.getViceBusLeader3MemberId() != null && vehicleInfo.getViceBusLeader3MemberId().equals(memberId)) {
                    sb.append(vehicleInfo.getVehicleType() + "(" + vehicleInfo.getVehicleNum() + ")車:副領隊3/");
                }
            }
        }

        String leaderText = sb.toString();

        if (leaderText.length() > 0) {
            leaderText = leaderText.substring(0, leaderText.length() - 1);
        }

        return leaderText;
    }

    // update event enroll main & member info & enroll pgm
    public void updateEnroll(HttpSession session, String eventId, String formAutoAddNum, EventEnrollRequest request, boolean isMaster)
            throws NotFoundException {

//        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        // get enroll main
        List<EventEnrollMain> eventEnrollMains = eventEnrollMainRepo.GetEnrolledByEventIdFormAutoAddNum(eventId, formAutoAddNum);

        if (eventEnrollMains.size() > 0) {

            // update enroll Main
            EventEnrollMain enrollMain = eventEnrollMains.get(0);
            if (isMaster) {
                enrollMain.copyEnroll4Master(request, false);
            } else {
                enrollMain.copyEnroll(request, false);
            }
            enrollMain.setUpdater(session);
            enrollMain.setIsWaitForCtSync(true);
            eventEnrollMainRepo.save(enrollMain);

            if (isMaster) {
                updateUnitMaster(session, enrollMain);
            } else {
                // update ct member info
                updateMember(session, enrollMain);
            }

            // update enroll pgm(s)
            List<String> formAutoAddNums = new ArrayList<>();
            List<EventEnrollMain> enrollMains = new ArrayList<>();

            formAutoAddNums.add(formAutoAddNum);
            enrollMains.add(enrollMain);

            updateEnrollPgmList(session, eventId, request, formAutoAddNums, enrollMains);

        } else {
            throw new NotFoundException();
        }
    }

    private void updateMember(HttpSession session, EventEnrollMain enrollMain) {
        CtMemberInfo member = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId(enrollMain.getEnrollUserId());

        if (member != null) {
            member.updateFromEnroll(enrollMain);
            member.setUpdater(session);
            ctMemberInfoRepo.save(member);
        }
    }

    private void printList(List<String> input) {
        for (String item : input) {
            logger.debug(item);
        }
    }

    public void updateEnrollOrderNum(HttpSession session, String eventId, String formAutoAddNum, int value) throws NotFoundException {

        // get enroll main
        List<EventEnrollMain> eventEnrollMains = eventEnrollMainRepo.GetEnrolledByEventIdFormAutoAddNum(eventId, formAutoAddNum);

        if (eventEnrollMains.size() > 0) {
            EventEnrollMain eventEnrollMain = eventEnrollMains.get(0);

            eventEnrollMain.setEnrollOrderNum(value);
            eventEnrollMain.setUpdater(session);
            eventEnrollMainRepo.save(eventEnrollMain);

        } else {
            throw new NotFoundException();
        }
    }

    public List<EventEnrollResult> batchEnroll(HttpSession session, BatchEventEnrollRequest request, boolean isMaster) throws RedundantException, NotFoundException {
        List<EventEnrollResult> results = new ArrayList<>();

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
        String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(request.getEnrollData().getEventId());

        if (eventMain == null) {
            throw new NotFoundException();
        }

        request.getEnrollData().setEnrollUnitId(unitId);
        request.getEnrollData().setEnrollUnitName(unitName);

        // add event enroll main
        List<EventEnrollMain> eventEnrollMains = new ArrayList<>();
        String currentId = genFormAutoAddNum();

        if (isMaster) {
            // check for multiple enroll
            if (!eventMain.getIsMultipleEnroll()) {
                List<String> masterIds = request.getMasterList().stream().map(m -> m.getMasterId()).collect(Collectors.toList());

                if (masterIds.size() > 0) {
                    long count = eventEnrollMainRepo.CheckEnrollByMemberIdEventIds(request.getEnrollData().getEventId(), masterIds);

                    if (count > 0) {
                        throw new RedundantException();
                    }
                }
            }

            List<UnitMasterInfo> masterInfoList = unitMasterRepo.GetMatchedMasters(request.getMasterList());

            logger.debug("unit master size:" + masterInfoList.size());
            List<String> ctMasterIds = request.getMasterList().stream().filter(m -> m.getIsCt()).map(m -> m.getMasterId()).collect(Collectors.toList());

            if (ctMasterIds.size() > 0) {
                List<UnitMasterInfo> ctMasterInfoList = ctMasterInfoRepo.GetAsUnitMasterInfoByMasterIds(ctMasterIds);
                logger.debug("ct master size:" + ctMasterInfoList.size());
                if (ctMasterInfoList.size() > 0) {
                    masterInfoList.addAll(ctMasterInfoList);
                }
            }

            if (masterInfoList.size() > 0) {
                String currentAId = genEventFormUid(request.getEnrollData().getEventId(), "A");
                String currentBId = genEventFormUid(request.getEnrollData().getEventId(), "B");

                for (UnitMasterInfo masterInfo : masterInfoList) {
                    EventEnrollMain enrollMain = new EventEnrollMain();
                    enrollMain.copyEnroll4Master(request.getEnrollData(), true);
                    enrollMain.copyMasterInfo(masterInfo);
                    enrollMain.setCreatorAndUpdater(session);
                    enrollMain.setChangeEndDate(eventMain.getChangeEndDate());
                    enrollMain.setIsWaitForCtSync(true);
                    enrollMain.setFormAutoAddNum(currentId);

                    if (masterInfo.getMasterId().substring(0, 1).toUpperCase().equals("A")) {
                        enrollMain.setEventFormUid(currentAId);
                        currentAId = incrementGeneratedId(currentAId, 7);
                    } else {
                        enrollMain.setEventFormUid(currentBId);
                        currentBId = incrementGeneratedId(currentBId, 7);
                    }

                    eventEnrollMains.add(enrollMain);

                    results.add(new EventEnrollResult(enrollMain.getEnrollUserId(), enrollMain.getFormAutoAddNum()));
                    currentId = incrementGeneratedId(currentId, 7);
                }

                eventEnrollMainRepo.saveAll(eventEnrollMains);
            }
        } else {
            // check for multiple enroll
            if (!eventMain.getIsMultipleEnroll()) {
                long count = eventEnrollMainRepo.CheckEnrollByMemberIdEventIds(request.getEnrollData().getEventId(), request.getMemberIds());

                if (count > 0) {
                    throw new RedundantException();
                }
            }

            // get members
            List<CtMemberInfo> memberInfoList = ctMemberInfoRepo.GetMemberInfoByMemberIds(request.getMemberIds());

            // get class enroll form
            String classId = request.getEnrollData().getClassId();
            ClassInfo classInfo = classId!= null ? classInfoRepo.GetClassInfoByClassId(classId) : null;

            List<ClassEnrollForm> classEnrollForms = classId != null ? classEnrollFormRepo.GetClassEnrollFormByMemberIdsAndClassId(request.getMemberIds(), classId) : new ArrayList<>();

            if (memberInfoList.size() > 0) {

                String currentMId = genEventFormUid(request.getEnrollData().getEventId(), "M");
                String currentFId = genEventFormUid(request.getEnrollData().getEventId(), "F");

                List<ClassEnrollFormShort> homeClassList = request.getHomeClassIds();

                for (CtMemberInfo memberInfo : memberInfoList) {

                    EventEnrollMain enrollMain = new EventEnrollMain();
                    enrollMain.copyEnroll(request.getEnrollData(), true);
                    enrollMain.copyMemberInfo(memberInfo);
                    enrollMain.setCreatorAndUpdater(session);
                    enrollMain.setIsWaitForCtSync(true);
                    enrollMain.setFormAutoAddNum(currentId);

                    Optional<ClassEnrollFormShort> enroll = homeClassList.stream().filter(h -> h.getMemberId().equals(memberInfo.getMemberId())).findAny();

                    if (enroll.isPresent()) {
                        enrollMain.copyHomeClassInfo(enroll.get());
                    }

                    if (classInfo != null) {
                        enrollMain.copyClassInfo(classInfo);
                    }

                    Optional<ClassEnrollForm> optionalClassEnrollForm = classEnrollForms.stream().filter(e -> e.getMemberId().equals(memberInfo.getMemberId())).findAny();

                    if (optionalClassEnrollForm.isPresent()) {
                        enrollMain.copyClassEnrollForm(optionalClassEnrollForm.get());
                    }

                    if (memberInfo.getGender().substring(0, 1).toUpperCase().equals("M") || memberInfo.getGender().equals("男")) {
                        enrollMain.setEventFormUid(currentMId);
                        currentMId = incrementGeneratedId(currentMId, 7);
                    } else {
                        enrollMain.setEventFormUid(currentFId);
                        currentFId = incrementGeneratedId(currentFId, 7);
                    }

                    eventEnrollMains.add(enrollMain);

                    results.add(new EventEnrollResult(enrollMain.getEnrollUserId(), enrollMain.getFormAutoAddNum()));
                    currentId = incrementGeneratedId(currentId, 7);
                }

                eventEnrollMainRepo.saveAll(eventEnrollMains);
            }
        }


        if (eventEnrollMains.size() > 0 &&
                ((request.getEnrollData().getPgmList() != null && request.getEnrollData().getPgmList().size() > 0) ||
                        ((request.getEnrollData().getGenPgmList() != null && request.getEnrollData().getGenPgmList().size() > 0)))) {
            // add pgms
            List<EventPgmShort> pgms = request.getEnrollData().getGenPgmList();
            pgms.addAll(request.getEnrollData().getPgmList());

            // get all pgmUniqueIds
            List<String> pgmUniqueIds = pgms.stream().map(p -> p.getPgmUniqueId()).collect(Collectors.toList());

            // get all pgmIds of all event_pgm_unit where unitId matched and in pgmUniqueIds
            List<EventPgmUnit> eventPgmUnits = eventPgmUnitRepo.GetByUnitIdInPgmUniqueIds(unitId, pgmUniqueIds);

            // create event_enroll_pgm
            List<EventEnrollPgm> eventEnrollPgms = new ArrayList<>();

            for (EventPgmShort item : pgms) {
                logger.debug(item.getPgmUniqueId() + "/" + item.getGenPgmUserInputValue());

                Optional<EventPgmUnit> eventPgmUnit = eventPgmUnits.stream().filter(e -> e.getPgmUniqueId().equals(item.getPgmUniqueId())).findAny();

                if (eventPgmUnit.isPresent()) {
                    for (EventEnrollMain enrollMain : eventEnrollMains) {
                        EventEnrollPgm eventEnrollPgm = createEventEnrollPgm(enrollMain, eventPgmUnit.get(), CommonUtils.Empty2Null(item.getGenPgmUserInputValue()), session);
                        eventEnrollPgms.add(eventEnrollPgm);
                    }
                } else {
                    logger.error("pgm unit not found! pgmUniqueId: " + item.getPgmUniqueId());
                }
            }

            // need dorm
            if (request.getEnrollData().getIsCtDorm()) {
                EventPgmUnit eventPgmUnitDorm = eventPgmUnitRepo.GetByUnitIdEventIdDorm(unitId, request.getEnrollData().getEventId());

                if (eventPgmUnitDorm != null) {
                    for (EventEnrollMain enrollMain : eventEnrollMains) {
                        EventEnrollPgm eventEnrollPgm = createEventEnrollPgm(enrollMain, eventPgmUnitDorm, null, session);
                        eventEnrollPgm.setPgmDate(enrollMain.getCtDormStartDate());
                        eventEnrollPgm.setCtDormStartDate(enrollMain.getCtDormStartDate());
                        eventEnrollPgm.setCtDormEndDate(enrollMain.getCtDormEndDate());
                        eventEnrollPgms.add(eventEnrollPgm);
                    }
                } else {
                    logger.error("pgm unit not found! dorm unitId:" + unitId + " eventId: " + request.getEnrollData().getEventId());
                }
            }

            // need trans
            if (!request.getEnrollData().getIsGoCtBySelf()) {
                EventPgmUnit eventPgmUnitTrans = eventPgmUnitRepo.GetByUnitIdEventIdTrans(unitId, request.getEnrollData().getEventId());

                if (eventPgmUnitTrans != null) {
                    for (EventEnrollMain enrollMain : eventEnrollMains) {
                        EventEnrollPgm eventEnrollPgm = createEventEnrollPgm(enrollMain, eventPgmUnitTrans, null, session);
                        eventEnrollPgm.setGoTransDate(CommonUtils.ParseDate(request.getEnrollData().getGoTransDate()));
                        eventEnrollPgm.setGoTransType(CommonUtils.Empty2Null(request.getEnrollData().getGoTransType()));
                        eventEnrollPgm.setReturnTransDate(CommonUtils.ParseDate(request.getEnrollData().getReturnTransDate()));
                        eventEnrollPgm.setReturnTransType(CommonUtils.Empty2Null(request.getEnrollData().getReturnTransType()));
                        eventEnrollPgms.add(eventEnrollPgm);
                    }
                } else {
                    logger.error("pgm unit not found! trans unitId:" + unitId + " eventId: " + request.getEnrollData().getEventId());
                }
            }

            if (eventEnrollPgms.size() > 0) {
                eventEnrollPgmRepo.saveAll(eventEnrollPgms);
            }
        }

        return results;
    }

    public void batchUpdateEnroll(HttpSession session, BatchEventEnrollRequest request) throws NotFoundException {

        // get enroll main
        List<EventEnrollMain> eventEnrollMains = eventEnrollMainRepo.GetEnrolledByEventIdFormAutoAddNums(request.getEnrollData().getEventId(), request.getFormAutoAddNums());

        logger.debug("enroll main size: " + eventEnrollMains.size());

        if (eventEnrollMains != null && eventEnrollMains.size() == request.getFormAutoAddNums().size()) {
            // update enroll main
            for (EventEnrollMain eventEnrollMain : eventEnrollMains) {
                eventEnrollMain.copyEnroll4BatchUpdate(request.getEnrollData());
                eventEnrollMain.setUpdater(session);
                eventEnrollMain.setIsWaitForCtSync(true);
            }

            eventEnrollMainRepo.saveAll(eventEnrollMains);

            updateEnrollPgmList(session, request.getEnrollData().getEventId(), request.getEnrollData(), request.getFormAutoAddNums(), eventEnrollMains);

        } else {
            throw new NotFoundException();
        }
    }

    private void updateEnrollPgmList(HttpSession session, String eventId, EventEnrollRequest request, List<String> formAutoAddNums, List<EventEnrollMain> enrollMainList) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        // get enroll pgm
        List<EventEnrollPgm> allEventEnrollPgmList = eventEnrollPgmRepo.GetByEventIdFormAutoAddNum(eventId, formAutoAddNums);

        // request pgm(s)
        List<EventPgmShort> pgms = request.getGenPgmList();
        pgms.addAll(request.getPgmList());

        logger.debug("request pgm size:" + pgms.size());

        // get all pgmUniqueIds
        List<String> pgmUniqueIds = pgms.stream().map(p -> p.getPgmUniqueId()).collect(Collectors.toList());

        // dorm
        if (request.getIsCtDorm()) {
            EventPgmUnit eventPgmUnitDorm = eventPgmUnitRepo.GetByUnitIdEventIdDorm(unitId, request.getEventId());
            if (eventPgmUnitDorm != null) pgmUniqueIds.add(eventPgmUnitDorm.getPgmUniqueId());
        }
        //trans
        if (!request.getIsGoCtBySelf()) {
            EventPgmUnit eventPgmUnitTrans = eventPgmUnitRepo.GetByUnitIdEventIdTrans(unitId, request.getEventId());
            if (eventPgmUnitTrans != null) pgmUniqueIds.add(eventPgmUnitTrans.getPgmUniqueId());
        }

        // one enroll at a time
        Date dormStartDate = CommonUtils.ParseDate(request.getCtDormStartDate());
        Date dormEndDate = CommonUtils.ParseDate(request.getCtDormStartDate());
        Date goTransDate = CommonUtils.ParseDate(request.getGoTransDate());
        Date returnTransDate = CommonUtils.ParseDate(request.getReturnTransDate());

        List<EventEnrollPgm> updEventEnrollPgmList = new ArrayList<>();
        List<EventEnrollPgm> newEventEnrollPgmList = new ArrayList<>();
        List<EventEnrollPgm> delEventEnrollPgmList = new ArrayList<>();

        for (String formAutoAddNum : formAutoAddNums) {
            Optional<EventEnrollMain> enrollMain = enrollMainList.stream().filter(e -> e.getFormAutoAddNum().equals(formAutoAddNum)).findAny();

            // 1. update enroll pgm(s)
            // pgms per formAutoAddNum
            List<EventEnrollPgm> eventEnrollPgmList = allEventEnrollPgmList.stream().filter(e -> e.getFormAutoAddNum().equals(formAutoAddNum)).collect(Collectors.toList());
            // pgms for update
            List<EventEnrollPgm> updateCheckPgmList = eventEnrollPgmList.stream().filter(e -> pgmUniqueIds.contains(e.getPgmUniqueId())).collect(Collectors.toList());

            for (EventEnrollPgm item : updateCheckPgmList) {
                boolean change = false;

                if (item.getPgmUniqueId().equals("PGM10014")) {

                    if (item.getPgmDate().compareTo(dormStartDate) != 0) {
                        item.setPgmDate(dormStartDate);
                        change = true;
                    }
                    if (item.getCtDormStartDate().compareTo(dormStartDate) != 0) {
                        item.setCtDormStartDate(dormStartDate);
                        change = true;
                    }
                    if (item.getCtDormEndDate().compareTo(dormEndDate) != 0) {
                        item.setCtDormEndDate(dormEndDate);
                        change = true;
                    }
                } else if (item.getPgmUniqueId().equals("PGM10017")) {

                    if (item.getGoTransDate().compareTo(goTransDate) != 0) {
                        item.setGoTransDate(goTransDate);
                        change = true;
                    }

                    if (!item.getGoTransType().equals(request.getGoTransType())) {
                        item.setGoTransType(request.getGoTransType());
                        change = true;
                    }

                    if (item.getReturnTransDate().compareTo(returnTransDate) != 0) {
                        item.setReturnTransDate(returnTransDate);
                        change = true;
                    }

                    if (!item.getReturnTransType().equals(request.getReturnTransType())) {
                        item.setReturnTransType(request.getReturnTransType());
                        change = true;
                    }
                } else {
                    if (item.getIsGenPgm()) {
                        Optional<EventPgmShort> target = pgms.stream().filter(p -> p.getPgmUniqueId().equals(item.getPgmUniqueId())).findAny();

                        if (target.isPresent()) {
                            item.setGenPgmUserInputValue(target.get().getGenPgmUserInputValue());
                            change = true;
                        }
                    }
                }

                if (change) {
                    item.setUpdater(session);
                    updEventEnrollPgmList.add(item);
                }
            }

            // 2. add enroll pgm(s)
            List<String> existPgmUniqueIds = updateCheckPgmList.stream().map(i -> i.getPgmUniqueId()).collect(Collectors.toList());

            logger.debug("exist");
            printList(existPgmUniqueIds);

            List<String> newLPgmUniqueIds;

            if (existPgmUniqueIds.size() > 0) {
                newLPgmUniqueIds = pgmUniqueIds.stream().filter(i -> !existPgmUniqueIds.contains(i)).collect(Collectors.toList());
            } else {
                newLPgmUniqueIds = pgmUniqueIds;
            }

            logger.debug("new");
            printList(newLPgmUniqueIds);

            // has new pgm
            if (newLPgmUniqueIds.size() > 0) {
                // get all pgmIds of all event_pgm_unit where unitId matched and in pgmUniqueIds
                List<EventPgmUnit> eventPgmUnits = eventPgmUnitRepo.GetByUnitIdInPgmUniqueIds(unitId, newLPgmUniqueIds);

                // create event_enroll_pgm

                List<EventPgmShort> newPgms = pgms.stream().filter(p -> newLPgmUniqueIds.contains(p.getPgmUniqueId())).collect(Collectors.toList());

                for (EventPgmShort item : newPgms) {
                    logger.debug(item.getPgmUniqueId() + "/" + item.getGenPgmUserInputValue());

                    Optional<EventPgmUnit> eventPgmUnit = eventPgmUnits.stream().filter(e -> e.getPgmUniqueId().equals(item.getPgmUniqueId())).findAny();

                    if (eventPgmUnit.isPresent() && enrollMain.isPresent()) {
                        EventEnrollPgm eventEnrollPgm = createEventEnrollPgm(enrollMain.get(), eventPgmUnit.get(), CommonUtils.Empty2Null(item.getGenPgmUserInputValue()), session);

                        if (eventPgmUnit.get().getPgmUniqueId().equals("PGM10014")) {
                            eventEnrollPgm.setPgmDate(dormStartDate);
                            eventEnrollPgm.setCtDormStartDate(dormStartDate);
                            eventEnrollPgm.setCtDormEndDate(dormEndDate);
                        } else if (eventPgmUnit.get().getPgmUniqueId().equals("PGM10017")) {
                            eventEnrollPgm.setGoTransDate(goTransDate);
                            eventEnrollPgm.setGoTransType(CommonUtils.Empty2Null(request.getGoTransType()));
                            eventEnrollPgm.setReturnTransDate(returnTransDate);
                            eventEnrollPgm.setReturnTransType(CommonUtils.Empty2Null(request.getReturnTransType()));
                        }

                        newEventEnrollPgmList.add(eventEnrollPgm);
                    } else {
                        logger.error("pgm unit not found! pgmUniqueId: " + item.getPgmUniqueId());
                    }
                }
            }

            // 3. delete enroll pgm(s)
            List<EventEnrollPgm> deletePgmList = eventEnrollPgmList.stream().filter(e -> !pgmUniqueIds.contains(e.getPgmUniqueId())).collect(Collectors.toList());

            if (deletePgmList.size() > 0) {
                delEventEnrollPgmList.addAll(deletePgmList);
            }
        }

        if (updEventEnrollPgmList.size() > 0) {
            eventEnrollPgmRepo.saveAll(updEventEnrollPgmList);
        }

        if (newEventEnrollPgmList.size() > 0) {
            eventEnrollPgmRepo.saveAll(newEventEnrollPgmList);
        }

        if (delEventEnrollPgmList.size() > 0) {
            eventEnrollPgmRepo.deleteAll(delEventEnrollPgmList);
        }
    }

    public void batchCancel(HttpSession session, String eventId, List<String> formAutoAddNums) throws NotFoundException {
        if (formAutoAddNums != null || formAutoAddNums.size() > 0) {
            List<EventEnrollMain> eventEnrollMains = eventEnrollMainRepo.GetEnrolledByEventIdFormAutoAddNums(eventId, formAutoAddNums);

            Date now = new Date();
            String updateStr = "[異動時間:" + CommonUtils.DateTime2DateTimeString(now) + "]取消報名表。";

            List<EventEnrollMain> changeList = new ArrayList<>();

            for (EventEnrollMain eventEnrollMain : eventEnrollMains) {
                if (eventEnrollMain.getIsCanceled() == null || eventEnrollMain.getIsCanceled() == false) {
                    eventEnrollMain.setIsCanceled(true);
                    eventEnrollMain.setCancelledDate(now);
                    eventEnrollMain.setEnrollLatestChange(updateStr);
                    eventEnrollMain.setEnrollChangeHistory(updateStr + eventEnrollMain.getEnrollChangeHistory());
                    eventEnrollMain.setUpdater(session);
                    eventEnrollMain.setIsWaitForCtSync(true);
                    changeList.add(eventEnrollMain);
                }
            }

            if (changeList.size() > 0) {
                eventEnrollMainRepo.saveAll(changeList);
            }

            // get matched enroll pgm(s)
            List<EventEnrollPgm> transPgms = eventEnrollPgmRepo.GetTransByEventIdFormAutoAddNum(eventId, formAutoAddNums);

            for (EventEnrollPgm item : transPgms) {
                item.setGoTransId(null);
                item.setGoTransNum(null);
                item.setReturnTransId(null);
                item.setReturnTransNum(null);
                item.setUpdater(session);
            }

            eventEnrollPgmRepo.saveAll(transPgms);

        } else {
            throw new NotFoundException();
        }
    }

    public void batchRecover(HttpSession session, String eventId, List<String> formAutoAddNums) throws NotFoundException {
        if (formAutoAddNums != null || formAutoAddNums.size() > 0) {
            List<EventEnrollMain> eventEnrollMains = eventEnrollMainRepo.GetEnrolledByEventIdFormAutoAddNums(eventId, formAutoAddNums);

            Date now = new Date();
            String updateStr = "[異動時間:" + CommonUtils.DateTime2DateTimeString(now) + "]恢復報名表。";

            List<EventEnrollMain> changeList = new ArrayList<>();

            for (EventEnrollMain eventEnrollMain : eventEnrollMains) {
                if (eventEnrollMain.getIsCanceled() != null && eventEnrollMain.getIsCanceled() == true) {
                    eventEnrollMain.setIsCanceled(false);
                    eventEnrollMain.setCancelledDate(null);
                    eventEnrollMain.setEnrollLatestChange(updateStr);
                    eventEnrollMain.setEnrollChangeHistory(updateStr + eventEnrollMain.getEnrollChangeHistory());
                    eventEnrollMain.setUpdater(session);
                    eventEnrollMain.setIsWaitForCtSync(true);
                    changeList.add(eventEnrollMain);
                }
            }

            if (changeList.size() > 0) {
                eventEnrollMainRepo.saveAll(changeList);
            }
        } else {
            throw new NotFoundException();
        }
    }

    public EventMain checkEvent(String eventId) throws NotFoundException {
        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) {
            throw new NotFoundException();
        }

        return eventMain;
    }

    public List<MemberInfoNotEnrolled> getNotEnrolledMembers(HttpSession session, String eventId, String aliasName, String classId) throws NotFoundException {
        logger.debug(eventId + "/" + aliasName + "/" + classId);

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) {
            throw new NotFoundException();
        }

        // exclude enrolled to event
        List<String> enrolledMemberIds = eventEnrollMainRepo.GetEnrolledMemberIdsByEventId(unitId, eventId);
        // include only enrolled to class
        List<String> inClassMemberIds = classEnrollFormRepo.GetEnrolledMemberId(unitId, classId);

        boolean hasEnroll, hasInClass;

        hasEnroll = enrolledMemberIds.size() > 0;
        hasInClass = classId != null;

//        logger.debug(hasEnroll + "/" + enrolledMemberIds.size() + "/" + hasInClass + "/" + inClassMemberIds.size());

        // dummy ids for query
        if (!hasEnroll) {
            enrolledMemberIds.add("!@#$%^&*()_+");
        }

        if (inClassMemberIds.size() <= 0) {
            inClassMemberIds.add("!@#$%^&*()_+");
        }

        List<MemberInfoNotEnrolled> notEnrolledList = ctMemberInfoRepo.GetMembersExcludeMemberIds(unitId, enrolledMemberIds, hasEnroll, inClassMemberIds, hasInClass, aliasName);

        packMemberInfo(unitId, notEnrolledList);

        return notEnrolledList;
    }

    // pack data with photo / summer donation / enrolled class info
    public List<MemberInfoNotEnrolled> packMemberInfo(String unitId, List<MemberInfoNotEnrolled> notEnrolledList) {

        if (notEnrolledList.size() > 0) {
            List<String> memberIds = notEnrolledList.stream().map(n -> n.getMemberId()).collect(Collectors.toList());

            List<Members002ImagesObject> photos = ctMemberInfoService.getPhotosByMemberIdList(memberIds, true);

            List<ClassEnrollFormShort> enrolls = classEnrollFormRepo.GetClassNameAndJobList(unitId, memberIds);

            Date date = new Date();
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            String currentYear = String.valueOf(localDate.getYear());

            List<String> summerDonationMemberIDs = summerDonationRecordRepo.GetByMemberIdsYear(memberIds, currentYear);

            for (MemberInfoNotEnrolled item : notEnrolledList) {
                Optional<Members002ImagesObject> photo = photos.stream().filter(p -> p.getMemberId().equals(item.getMemberId())).findAny();

                List<ClassEnrollFormShort> memberEnrollList = enrolls.stream().filter(e -> e.getMemberId().equals(item.getMemberId())).collect(Collectors.toList());

                if (photo.isPresent()) {
                    item.setPhoto(photo.get().getPhoto());
                }

                if (memberEnrollList.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    String prefix = "";
                    for (ClassEnrollFormShort enroll : memberEnrollList) {
                        sb.append(prefix);
                        prefix = ",";
                        sb.append(enroll.getClassName());
                        if (enroll.getClassJobTitleList() != null && !enroll.getClassJobTitleList().isEmpty()) {
                            sb.append("(" + enroll.getClassJobTitleList() + ")");
                        }
                    }

                    item.setClassNameAndJobList(sb.toString());
                    item.setHomeClassNote(memberEnrollList.get(0).getHomeClassNote());
                }

                item.setIsThisYearSummerDonation(summerDonationMemberIDs.contains(item.getMemberId()));
            }
        }

        return null;
    }

    public PageResponse<MemberInfoNotEnrolled> getNotEnrolledMembers4Ct(String eventId, String unitId, String name, int page, int pageSize, String keyword, String sortKey, Integer isDesc) throws NotFoundException, UnacceptableException {
        logger.debug(eventId + "/" + unitId + "/" + name);

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) {
            throw new NotFoundException();
        }

        Pageable pageable;
        if (sortKey != null) {
            if (isDesc == null || isDesc == 0) {
                pageable = PageRequest.of(page, pageSize, Sort.by(sortKey).ascending());
            } else {
                pageable = PageRequest.of(page, pageSize, Sort.by(sortKey).descending());
            }
        } else {
            pageable = PageRequest.of(page, pageSize);
        }


        // exclude enrolled to event
        List<String> enrolledMemberIds = eventEnrollMainRepo.GetEnrolledMemberIdsByEventId(null, eventId);

        boolean hasEnroll = true;
        if (enrolledMemberIds.size() <= 0) {
            enrolledMemberIds.add("!@#$%^&*()_+");
            hasEnroll = false;
        }

        try {
            Page<MemberInfoNotEnrolled> results = ctMemberInfoRepo.GetMembersExcludeMemberIds(unitId, enrolledMemberIds, hasEnroll, name, keyword, pageable);

            packMemberInfo(null, results.getContent());

            return new PageResponse<>(200, "success", results.getNumber(), results.getSize(), results.getTotalElements(), results.getTotalPages(), results.getContent());

        } catch (QueryException q) {
            throw new UnacceptableException();
        }
    }

    // get masters not checking for enrollment
    public List<MasterInfoShort> getNotEnrolledMasters(String type, String unitId, String masterIdPrefix, String keyword) {

        List<MasterInfoShort> unitMasterInfoList = new ArrayList<>();
        List<MasterInfoShort> ctMasterInfoList = new ArrayList<>();

        unitMasterInfoList = unitMasterInfoRepo.GetUnitMasterShort(unitId, masterIdPrefix, keyword);

        logger.debug("unit master size:" + unitMasterInfoList.size());

        if (type.toUpperCase().equals("CT")) {

            List<String> foundMasterId = unitMasterInfoList.stream().map(u -> u.getMasterId()).distinct().collect(Collectors.toList());

            ctMasterInfoList = ctMasterInfoRepo.GetCtMasterShort(masterIdPrefix, keyword);

            logger.debug("ct master size:" + ctMasterInfoList.size());

            if (foundMasterId.size() > 0) {
                ctMasterInfoList = ctMasterInfoList.stream().filter(m -> !foundMasterId.contains(m.getMasterId())).collect(Collectors.toList());
            }
        }

        ctMasterInfoList.addAll(unitMasterInfoList);

        logger.debug("all master size:" + ctMasterInfoList.size());

//        if (eventMain.getIsMultipleEnroll() == null || !eventMain.getIsMultipleEnroll()) {
//            List<String> enrolledMasterIds = eventEnrollMainRepo.GetEnrolledMasterIdsByEventId(unitId, eventId);
//            logger.debug("enrolled size:" + enrolledMasterIds.size());
//            if (masterInfoList.size() > 0 && enrolledMasterIds.size() > 0) {
//                masterInfoList = masterInfoList.stream().filter(m -> !enrolledMasterIds.contains(m.getMasterId())).collect(Collectors.toList());
//
//                logger.debug("not enrolled size:" + masterInfoList.size());
//            }
//        }

        return ctMasterInfoList;
    }

    private void addPgms(EventEnrollRequest request, String unitId, EventEnrollMain enrollMain, HttpSession session) {
        List<EventPgmShort> pgms = request.getGenPgmList();
        pgms.addAll(request.getPgmList());

        // get all pgmUniqueIds
        List<String> pgmUniqueIds = pgms.stream().map(p -> p.getPgmUniqueId()).collect(Collectors.toList());

        // get all pgmIds of all event_pgm_unit where unitId matched and in pgmUniqueIds
        List<EventPgmUnit> eventPgmUnits = eventPgmUnitRepo.GetByUnitIdInPgmUniqueIds(unitId, pgmUniqueIds);

        // create event_enroll_pgm
        List<EventEnrollPgm> eventEnrollPgms = new ArrayList<>();

        for (EventPgmShort item : pgms) {
//            logger.debug(item.getPgmUniqueId() + "/" + item.getGenPgmUserInputValue());

            Optional<EventPgmUnit> eventPgmUnit = eventPgmUnits.stream().filter(e -> e.getPgmUniqueId().equals(item.getPgmUniqueId())).findAny();

            if (eventPgmUnit.isPresent()) {
                EventEnrollPgm eventEnrollPgm = createEventEnrollPgm(enrollMain, eventPgmUnit.get(), CommonUtils.Empty2Null(item.getGenPgmUserInputValue()), session);
                eventEnrollPgms.add(eventEnrollPgm);
            } else {
//                logger.error("pgm unit not found! pgmUniqueId: " + item.getPgmUniqueId());
            }
        }

        // need dorm
        if (enrollMain.getIsCtDorm()) {
            EventPgmUnit eventPgmUnitDorm = eventPgmUnitRepo.GetByUnitIdEventIdDorm(unitId, enrollMain.getEventId());

            if (eventPgmUnitDorm != null) {
                EventEnrollPgm eventEnrollPgm = createEventEnrollPgm(enrollMain, eventPgmUnitDorm, null, session);
                eventEnrollPgm.setPgmDate(enrollMain.getCtDormStartDate());
                eventEnrollPgm.setCtDormStartDate(enrollMain.getCtDormStartDate());
                eventEnrollPgm.setCtDormEndDate(enrollMain.getCtDormEndDate());
                eventEnrollPgms.add(eventEnrollPgm);
            } else {
//                logger.error("pgm unit not found! dorm unitId:" + unitId + " eventId: " + enrollMain.getEventId());
            }
        }

        // need trans
        if (!enrollMain.getIsGoCtBySelf()) {
            EventPgmUnit eventPgmUnitTrans = eventPgmUnitRepo.GetByUnitIdEventIdTrans(unitId, enrollMain.getEventId());

            if (eventPgmUnitTrans != null) {
                EventEnrollPgm eventEnrollPgm = createEventEnrollPgm(enrollMain, eventPgmUnitTrans, null, session);
                eventEnrollPgm.setGoTransDate(CommonUtils.ParseDate(request.getGoTransDate()));
                eventEnrollPgm.setGoTransType(CommonUtils.Empty2Null(request.getGoTransType()));
                eventEnrollPgm.setReturnTransDate(CommonUtils.ParseDate(request.getReturnTransDate()));
                eventEnrollPgm.setReturnTransType(CommonUtils.Empty2Null(request.getReturnTransType()));
                eventEnrollPgms.add(eventEnrollPgm);
            } else {
//                logger.error("pgm unit not found! trans unitId:" + unitId + " eventId: " + enrollMain.getEventId());
            }
        }

        if (eventEnrollPgms.size() > 0) {
            eventEnrollPgmRepo.saveAll(eventEnrollPgms);
        }
    }

    private void updateUnitMaster(HttpSession session, EventEnrollMain enrollMain) {
        List<UnitMasterInfo> unitMasterInfoList = unitMasterInfoRepo.GetByMasterId(enrollMain.getUnitId(), enrollMain.getEnrollUserId());

        if (unitMasterInfoList.size() > 0) {
            UnitMasterInfo unitMasterInfo = unitMasterInfoList.get(0);

            unitMasterInfo.setMasterName(enrollMain.getEnrollUserName());
            unitMasterInfo.setJobOrder(enrollMain.getJobOrder());
            unitMasterInfo.setPreceptOrder(enrollMain.getPreceptOrder());
            unitMasterInfo.setIsTreasurer(enrollMain.getIsTreasurer());
            unitMasterInfo.setIsAbbot(enrollMain.getIsAbbot());
            unitMasterInfo.setJobTitle(enrollMain.getJobTitle());
            unitMasterInfo.setMasterPreceptTypeName(enrollMain.getMasterPreceptTypeName());
            unitMasterInfo.setMobileNum(enrollMain.getMobileNum1());
            unitMasterInfo.setUpdater(session);

            unitMasterInfoRepo.save(unitMasterInfo);
        }
    }

    public void batchSign(HttpSession session, AbbotSignRequest request) {
        List<EventEnrollMain> eventEnrollMainList = eventEnrollMainRepo.GetEnrollsByFormAutoAddNums(request.getFormAutoAddNums());

        String userName = (String) session.getAttribute(UserSession.Keys.UserName.getValue());

        for (EventEnrollMain enrollMain : eventEnrollMainList) {

            if (request.getIsAbbotSigned()) {
                enrollMain.setIsAbbotSigned(true);
                enrollMain.setIsAbbotSignedDt(new Date());
                enrollMain.setIsAbbotSignedByUserName(userName);
            } else {
                enrollMain.setIsAbbotSigned(null);
                enrollMain.setIsAbbotSignedDt(null);
                enrollMain.setIsAbbotSignedByUserName(null);
            }

            enrollMain.setUpdater(session);
        }

        if (eventEnrollMainList.size() > 0) {
            eventEnrollMainRepo.saveAll(eventEnrollMainList);
        }
    }

    public void batchCtApproved(HttpSession session, CtApprovedRequest request) {
        List<EventEnrollMain> eventEnrollMainList = eventEnrollMainRepo.GetEnrollsByFormAutoAddNums(request.getFormAutoAddNums());

        for (EventEnrollMain enrollMain : eventEnrollMainList) {

            enrollMain.setIsCtApproved(request.getIsCtApproved());
            enrollMain.setCtApprovedNote(CommonUtils.Empty2Null(request.getCtApprovedNote()));

            enrollMain.setUpdater(session);
        }

        if (eventEnrollMainList.size() > 0) {
            eventEnrollMainRepo.saveAll(eventEnrollMainList);
        }
    }

    public void updateCtApprovedNote(HttpSession session, CtApprovedNoteRequest request) throws NotFoundException {

        List<EventEnrollMain> eventEnrollMainList = eventEnrollMainRepo.GetEnrolledByFormAutoAddNum(request.getFormAutoAddNum());

        if (eventEnrollMainList.size() > 0) {
            EventEnrollMain eventEnrollMain = eventEnrollMainList.get(0);

            eventEnrollMain.setCtApprovedNote(CommonUtils.Empty2Null(request.getCtApprovedNote()));
            eventEnrollMain.setChangeEndDate(CommonUtils.ParseDate(request.getChangeEndDate()));

            eventEnrollMain.setUpdater(session);

            eventEnrollMainRepo.save(eventEnrollMain);
        } else {
            throw new NotFoundException();
        }
    }

}
