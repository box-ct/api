package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.Anonymous.Anonymous006_1Object;
import tw.org.ctworld.meditation.beans.Anonymous.Anonymous006_2Object;
import tw.org.ctworld.meditation.beans.Makeup.MakeupClassInfo;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.ClassDateInfo;
import tw.org.ctworld.meditation.models.ClassInfo;
import tw.org.ctworld.meditation.repositories.ClassDateInfoRepo;
import tw.org.ctworld.meditation.repositories.ClassInfoRepo;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ClassDateInfoService {

    private static final Logger logger = LoggerFactory.getLogger(ClassDateInfoService.class);

    @Autowired
    private ClassDateInfoRepo classDateInfoRepo;

    @Autowired
    private ClassInfoRepo classInfoRepo;

    @Autowired
    private VideoService videoService;

    @Autowired
    private SysCodeService sysCodeService;


    // 取得報到班別列表(依「開課時間最接近」排序)
    public List<Anonymous006_1Object> getClassByNow(HttpSession session) {

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));
        String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());

        List<Anonymous006_1Object> items = new ArrayList<>();

        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date endDate = c.getTime();

        c.setTime(currentDate);
        c.add(Calendar.DATE, -1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date startDate = c.getTime();

//        logger.error("Version : 0.0.20 UnitId : " + unitId + " StartDate : " + startDate + " EndDate : " + endDate);
        List<String> classIdList = classDateInfoRepo.GetClassIdByDate(unitId, startDate, endDate);

        SimpleDateFormat dateFormatInput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatInput.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));

        SimpleDateFormat dateFormatOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // 各IP當地時間
        Date localTime = null;
        try {
            localTime = dateFormatOutput.parse(dateFormatInput.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (classIdList.size() != 0) {

            List<String> classIdList2 = classInfoRepo.GetClassIdListByClassIdList2(classIdList);

            List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetClassByClassIdList(classIdList2);
            List<ClassInfo> classInfoList = classInfoRepo.GetClassListByClassIdList2(classIdList2);

            for (String classId : classIdList2) {

                List<ClassDateInfo> classIdDateInfoList = classDateInfoList.stream()
                        .filter(s -> s.getClassId().equals(classId))
                        .collect(Collectors.toList());

                // 時間秒差
                int diffNum = 0;
                List<mClassDateInfo> mClassDateInfoList = new ArrayList<>();

                for (ClassDateInfo classDateInfo : classIdDateInfoList) {

                    Date mClassDate = null;
                    try {
                        mClassDate = dateFormatOutput.parse(
                                dateFormatOutput.format(classDateInfo.getClassDate()).split(" ")[0] + " " +
                                        classDateInfo.getClassStartTime() + ":00");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    diffNum = (int) Math.abs((localTime.getTime() / 1000) - (mClassDate.getTime() / 1000));

                    mClassDateInfoList.add(new mClassDateInfo(
                            classDateInfo.getClassWeeksNum(), diffNum, dateFormatOutput.format(mClassDate)));
                }

                Collections.sort(mClassDateInfoList, new Comparator<mClassDateInfo>() {
                    @Override
                    public int compare(mClassDateInfo o1, mClassDateInfo o2) {
                        return o1.getDiffNum() - o2.getDiffNum();
                    }
                });

                List<Anonymous006_2Object> mClassDates = new ArrayList<>();

                ClassInfo mClassInfo = classInfoList.stream()
                        .filter(s -> s.getClassId().equals(classId))
                        .collect(Collectors.toList()).get(0);

                for (mClassDateInfo mClassDateInfo : mClassDateInfoList) {

                    mClassDates.add(new Anonymous006_2Object(
                            mClassDateInfo.getClassDate(),
                            mClassDateInfo.getClassWeeksNum(),
                            mClassDateInfo.getDiffNum()
                    ));
                }

                items.add(new Anonymous006_1Object(
                        mClassInfo.getClassId(),
                        mClassInfo.getClassPeriodNum(),
                        mClassInfo.getClassName(),
                        mClassInfo.getDayOfWeek(),
                        mClassInfo.getClassStatus(),
                        mClassDates
                ));
            }

            Collections.sort(items, new Comparator<Anonymous006_1Object>() {
                @Override
                public int compare(Anonymous006_1Object o1, Anonymous006_1Object o2) {
                    return o1.getClassDates().get(0).getDiffNum() - o2.getClassDates().get(0).getDiffNum();
                }
            });
        }

//        logger.debug("Result Size : " + items.size());
//
//        for (Anonymous006_1Object item : items) {
//            logger.debug("ClassId : " + item.getClassId() + " ClassPeriodNum : " + item.getClassPeriodNum() +
//                    " ClassName : " + item.getClassName() + " ClassDayOfWeek : " + item.getDayOfWeek());
//
//            for (Anonymous006_2Object classDate : item.getClassDates()) {
//                logger.debug("ClassDate : " + classDate.getClassDate() + " ClassWeekNum : " + classDate.getClassWeeksNum() +
//                        " DiffNum : " + classDate.getDiffNum());
//            }
//        }

        return items;
    }

    public class mClassDateInfo{

        private int classWeeksNum, diffNum;
        private String classDate;

        public mClassDateInfo(int classWeeksNum, int diffNum, String classDate) {
            this.classWeeksNum = classWeeksNum;
            this.diffNum = diffNum;
            this.classDate = classDate;
        }

        public int getClassWeeksNum() {
            return classWeeksNum;
        }

        public void setClassWeeksNum(int classWeeksNum) {
            this.classWeeksNum = classWeeksNum;
        }

        public int getDiffNum() {
            return diffNum;
        }

        public void setDiffNum(int diffNum) {
            this.diffNum = diffNum;
        }

        public String getClassDate() {
            return classDate;
        }

        public void setClassDate(String classDate) {
            this.classDate = classDate;
        }
    }

    // classes
    public void createClassDates(String classId, HttpSession session) {

        List<ClassInfo> classInfoList = classInfoRepo.FindClassByClassId(classId);

        if (classInfoList.size() > 0) {
            ClassInfo classInfo = classInfoList.get(0);

            List<ClassDateInfo> classDateInfoList = new ArrayList<>();

//            int totalClasses = Integer.parseInt(classInfo.getClassTotalNum());

            Calendar firstDate = Calendar.getInstance();
            firstDate.setTime(classInfo.getClassStartDate());

            int dayOfWeek = CommonUtils.CStringDayOfWeek2Int(classInfo.getDayOfWeek());

            while (firstDate.get(Calendar.DAY_OF_WEEK) != dayOfWeek) {
                firstDate.add(Calendar.DATE, 1);
            }

            Calendar lastDate = Calendar.getInstance();
            lastDate.setTime(classInfo.getClassEndDate());

            List<Calendar> classDates = new ArrayList<>();

            while (firstDate.compareTo(lastDate) <= 0) {
                Calendar date = Calendar.getInstance();
                date.setTime(firstDate.getTime());

                classDates.add(date);
                firstDate.add(Calendar.DATE, 7);
            }

            for (int i = 0; i < classDates.size(); i++) {
                ClassDateInfo classDateInfo = new ClassDateInfo();

                classDateInfo.setCreatorAndUpdater(session);
                classDateInfo.setUnitId(classInfo.getUnitId());
                classDateInfo.setUnitName(classInfo.getUnitName());
                classDateInfo.setClassId(classId);
                classDateInfo.setClassWeeksNum(i+1);
                classDateInfo.setClassDate(classDates.get(i).getTime());
                classDateInfo.setClassDayOfWeek(classInfo.getDayOfWeek());
                classDateInfo.setClassStartTime(classInfo.getClassStartTime());
                classDateInfo.setClassEndTime(classInfo.getClassEndTime());
                classDateInfo.setCancelled(false);

                classDateInfoList.add(classDateInfo);
            }

            if (classDateInfoList.size() > 0) {
                classDateInfoRepo.saveAll(classDateInfoList);

                classInfo.setClassTotalNum(String.valueOf(classDates.size()));
                classInfoRepo.save(classInfo);
            }

        }

    }

    // makeup
    public List<ClassDateInfo> getClassDateByClassId(String classId) {

        String className = classInfoRepo.GetClassNameByClassID(classId);

        List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetClassDateByClassId(classId);

        for (ClassDateInfo classDateInfo : classDateInfoList) {

            classDateInfo.setClassName(className);
        }

        return classDateInfoList;
    }

    // makeup
    public boolean deleteMakeupFileById(long id, HttpSession session) {

        try {
            ClassDateInfo classDateInfo = classDateInfoRepo.getOne(id);

            if (classDateInfo.getMakeupFileName() != null && !classDateInfo.getMakeupFileName().isEmpty()) {
                String makeupFilePath = sysCodeService.getMakeupFilePath();
                String originalFilePath = sysCodeService.getOriginalMakeupFilePath();

                videoService.deleteFile(makeupFilePath + "/" + classDateInfo.getMakeupFileName());
                videoService.deleteFilesLike(originalFilePath, classDateInfo.getMakeupFileName());

                classDateInfo.setMakeupFileName(null);
                classDateInfo.setMakeupFileType(null);
                classDateInfo.setFileTimeTotalLength(0);
                classDateInfo.setOriginFileName(null);
                classDateInfo.setUserDefinedFileDesc(null);
                classDateInfo.setMakeupFileProcessStatus("刪除檔案");
                classDateInfo.setMakeupFileSizeMB(0);
                classDateInfo.setUploadDtTm(null);
                classDateInfo.setUpdater(session);
                String log = CommonUtils.Null2Empty((String) session.getAttribute(UserSession.Keys.UserName.getValue())) + CommonUtils.DateTime2DateTimeString(new Date()) + "刪除檔案|";
                classDateInfo.setFileProcessedLog(classDateInfo.getFileProcessedLog() == null ? "" : classDateInfo.getFileProcessedLog() + log);

                classDateInfoRepo.save(classDateInfo);
            }

            return true;
        } catch (EntityNotFoundException en) {
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // makeup
    public boolean deleteMakeupFileByClassIds(List<String> classIds, HttpSession session) {
        List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetAllClassDateByClassIdList(classIds);

        if (classDateInfoList.size() > 0) {
            String log = CommonUtils.Null2Empty((String)session.getAttribute(UserSession.Keys.UserName.getValue())) + CommonUtils.DateTime2DateTimeString(new Date()) + "刪除檔案|";

            String makeupFilePath = sysCodeService.getMakeupFilePath();
            String originalFilePath = sysCodeService.getOriginalMakeupFilePath();

            for (ClassDateInfo classDateInfo : classDateInfoList) {

                if (classDateInfo.getMakeupFileName() != null && !classDateInfo.getMakeupFileName().isEmpty()) {
                    videoService.deleteFile(makeupFilePath + "/" + classDateInfo.getMakeupFileName());
                    videoService.deleteFilesLike(originalFilePath, classDateInfo.getMakeupFileName());

                    classDateInfo.setMakeupFileName(null);
                    classDateInfo.setMakeupFileType(null);
                    classDateInfo.setFileTimeTotalLength(0);
                    classDateInfo.setOriginFileName(null);
                    classDateInfo.setUserDefinedFileDesc(null);
                    classDateInfo.setMakeupFileProcessStatus("刪除檔案");
                    classDateInfo.setMakeupFileSizeMB(0);
                    classDateInfo.setUploadDtTm(null);
                    classDateInfo.setUpdater(session);
                    classDateInfo.setFileProcessedLog(classDateInfo.getFileProcessedLog() == null ? "" : classDateInfo.getFileProcessedLog() + log);
                }
            }

            classDateInfoRepo.saveAll(classDateInfoList);
            return true;
        } else {
            return false;
        }
    }

    // makeup
    public  ClassDateInfo getClassDateInfoById(long id) {
        return classDateInfoRepo.getOne(id);
    }

    // makeup
    public String genMakeupFileNameById(ClassDateInfo classDateInfo) {

        if (classDateInfo != null) {
            List<MakeupClassInfo> makeupClassInfoList = classInfoRepo.GetClassByClassId(classDateInfo.getClassId());

            if (makeupClassInfoList.size() > 0) {
                MakeupClassInfo makeupClassInfo = makeupClassInfoList.get(0);

                String temp = classDateInfo.getUnitId() + "_"
                        + classDateInfo.getUnitName() + "_"
                        + makeupClassInfo.getClassName() + "_"
                        + CommonUtils.DateTime2DateString(classDateInfo.getClassDate()) + "_"
                        + classDateInfo.getClassId() + "_"
                        + CommonUtils.DateTime2DateTimeCompactString(new Date());

                temp = temp.replace(" ", "");

                return temp;
            }
        }

        return null;
    }

    // makeup
    public void saveClassDateInfo(ClassDateInfo classDateInfo, HttpSession session) {
        if (session != null) {
            classDateInfo.setUpdater(session);
        }

        classDateInfoRepo.save(classDateInfo);
    }

    // makeup
//    public String genMakeupFileId() {
//        Calendar c = Calendar.getInstance();
//        DecimalFormat d2Format= new DecimalFormat("00");
//        DecimalFormat d4Format= new DecimalFormat("00");
//
//        String criteria = "MKF" + String.valueOf(c.get(Calendar.YEAR) - 1911) + d2Format.format(c.get(Calendar.MONTH)) + d2Format.format(c.get(Calendar.DAY_OF_MONTH));
//
//        String currentMaxValue = classDateInfoRepo.GetMaxMakeupFileId(criteria + '%');
//
//        logger.debug("criteria: " + criteria);
//        logger.debug("current max: " + currentMaxValue);
//
//        long tempId = 0;
//
//        if (currentMaxValue != null && currentMaxValue.length() > 4) {
//
//            currentMaxValue = currentMaxValue.substring(currentMaxValue.length() - 4);
//
//            tempId = Long.parseLong(currentMaxValue);
//        }
//
//        tempId++;
//
//        return criteria + d4Format.format(tempId);
//    }

    public void DeleteMakeupFilesOverYear() {

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetFilesOverYear(calendar.getTime());

        String makeupFilePath = sysCodeService.getMakeupFilePath();
        String originalFilePath = sysCodeService.getOriginalMakeupFilePath();

        Date now = new Date();
        if (classDateInfoList.size() > 0) {
            for (ClassDateInfo dateInfo : classDateInfoList) {
                videoService.deleteFile(makeupFilePath + "/" + dateInfo.getMakeupFileName());
                videoService.deleteFilesLike(originalFilePath, dateInfo.getMakeupFileName());

                dateInfo.setMakeupFileProcessStatus("系統刪除");
                dateInfo.setFileProcessedLog(dateInfo.getFileProcessedLog() + CommonUtils.DateTime2DateTimeString(now) + " 系統刪除|");

                dateInfo.setOriginFileName(null);
                dateInfo.setUploadDtTm(null);
                dateInfo.setUserDefinedFileDesc(null);
                dateInfo.setMakeupFileType(null);
                dateInfo.setMakeupFileName(null);
            }

            classDateInfoRepo.saveAll(classDateInfoList);
        }
    }
}
