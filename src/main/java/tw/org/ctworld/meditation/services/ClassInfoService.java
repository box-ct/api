package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup001Object;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord000Object;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002object;
import tw.org.ctworld.meditation.beans.ClassStats.ClassStats000Object;
import tw.org.ctworld.meditation.beans.ClassUpgrade.ClassUpgrade000Object;
import tw.org.ctworld.meditation.beans.Classes.*;
import tw.org.ctworld.meditation.beans.Common.*;
import tw.org.ctworld.meditation.beans.Makeup.MakeupClassInfo;
import tw.org.ctworld.meditation.beans.Makeup.Unit;
import tw.org.ctworld.meditation.beans.Member.Members008Object;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.libs.CommonUtils.Empty2Null;
import static tw.org.ctworld.meditation.libs.CommonUtils.ParseTime;

@Service
public class ClassInfoService {

    private static final Logger logger = LoggerFactory.getLogger(ClassInfoService.class);

    @Autowired
    private ClassInfoRepo classInfoRepo;

    @Autowired
    private ClassEnrollFormRepo classEnrollFormRepo;

    @Autowired
    private ClassAttendRecordRepo classAttendRecordRepo;

    @Autowired
    private ClassAttendMarkService classAttendMarkService;

    @Autowired
    private ClassDateInfoRepo classDateInfoRepo;

    @Autowired
    private ClassMakeupRecordRepo classMakeupRecordRepo;

    @Autowired
    private UnitInfoService unitInfoService;

    @Autowired
    private UserInfoService userInfoService;

    public List<String> getClassYearComboBox() {

        List<String> classYearList = classInfoRepo.GetClassYearComboBox();

        return classYearList;
    }

    public List<String> getClassYearComboBox(String unitId) {

        List<String> classYearList = classInfoRepo.GetClassYearComboBox(unitId);

        return classYearList;
    }

    public List<String> getClassInfoComboBox(String classStatus, String unitId) {

        List<String> classInfoList = classInfoRepo.GetClassInfoComboBox(classStatus, unitId);

        return classInfoList;
    }

    public List<String> getClassNameComboBox(String classStatus, String classPeriodNum, String unitId) {

        List<String> classNameList = classInfoRepo.GetClassNameComboBox(classStatus, classPeriodNum, unitId);

        return classNameList;
    }

    public List<String> getClassGroupComboBox(String classStatus, String classPeriodNum, String getClassName, String unitId) {

        List<String> classIds = classInfoRepo.GetClassId(classStatus, classPeriodNum, getClassName, unitId); // 柯思回應只會有一筆，所以只抓第一筆

        String classId = classIds.size() != 0 ? classIds.get(0) : null;

        List<String> classGroupList = new ArrayList<>();

        if (classId != null) {
            classGroupList = classEnrollFormRepo.GetClassGroupComboBox(classId);
        }

        return classGroupList;
    }

    public Common002Response getClassCounts(HttpSession session, String roleType) {

        String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());
        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        Common002Response common002Response = new Common002Response("0","0",new ArrayList<>());

        // 參考 https://blog.csdn.net/10km/article/details/53906993
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT" + timeZone));
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        Date startDate = calendar.getTime();
//        logger.debug("Calendar 1 : " + startDate);

        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
        Date endDate = calendar.getTime();
//        logger.debug("Calendar 2 : " + endDate);

//        List<ClassInfo> classInfoList = classInfoRepo.GetClassInfoByClassStatus("開課中");
        List<String> classIds = classInfoRepo.GetClassIdByClassStatus2("開課中");

        int sumTotal = 0; // 總人數
        int total = 0; // 該門課總人數
        int sumEnroll = 0; // 總報到人數
        int classEnroll = 0; // 該門課報到人數
        int attendance = 0; // 出席率

        List<Common002Response.Common002Object> items = new ArrayList<>();

//        DecimalFormat df = new DecimalFormat("##0.00");

        // 出席率標準
        List<String> attendMarkList = new ArrayList<>();
        attendMarkList.add("V");
        attendMarkList.add("L");
        attendMarkList.add("E");

        if (roleType.equals("精舍使用者")) {
            // 登入時身分別為"精舍使用者"

            List<String> classIdList = new ArrayList<>();

            List<String> classIds2 = classAttendRecordRepo.GetClassIdListByUnitIdAndClassDate(unitId, startDate, endDate);

            for (String classId : classIds) {

                if (classIds2.contains(classId) && !classIdList.contains(classId)) {

                    classIdList.add(classId);
                }
            }

            for (String classId : classIdList) {

                total = classAttendRecordRepo.GetTotalCountByClassIdAndClassDate(classId, startDate, endDate);
                classEnroll = classAttendRecordRepo.GetEnrollCountByClassIdAndClassDate(classId, startDate, endDate, attendMarkList);
                sumTotal = sumTotal + total;
                sumEnroll = sumEnroll + classEnroll;

                ClassInfo classInfo = classInfoRepo.GetAClassByClassId(classId);
                String className = classInfo.getClassName();

                if (total > 0) {
                    attendance = CommonUtils.PercentageInt(classEnroll, total);
                }else {
                    attendance = 0;
                }

                items.add(new Common002Response.Common002Object(
                        className, String.valueOf(classEnroll), String.valueOf(attendance)
                ));
            }

            common002Response.setItems(items);

        }else {
            // 登入時身分別為"本山管理單位"
            List<String> classIdList = new ArrayList<>();

            logger.debug(startDate.toString() + "/" + endDate.toString());

            List<String> classIds2 = classAttendRecordRepo.GetClassIdListByUnitIdAndClassDate(startDate, endDate);

            int classDay1Total = 0;
            int classDay1Enroll = 0;
            int classDay2Total = 0;
            int classDay2Enroll = 0;
            int classDay3Total = 0;
            int classDay3Enroll = 0;
            int classDay4Total = 0;
            int classDay4Enroll = 0;
            int classDay5Total = 0;
            int classDay5Enroll = 0;
            int classNight1Total = 0;
            int classNight1Enroll = 0;
            int classNight2Total = 0;
            int classNight2Enroll = 0;
            int classNight3Total = 0;
            int classNight3Enroll = 0;
            int classNight4Total = 0;
            int classNight4Enroll = 0;
            int classNight5Total = 0;
            int classNight5Enroll = 0;
            int classOtherTotal = 0;
            int classOtherEnroll = 0;

            for (String classId : classIds) {
                if (classIds2.contains(classId) && !classIdList.contains(classId)) {
                    classIdList.add(classId);
                }
            }

            for (String classId : classIdList) {

                total = classAttendRecordRepo.GetTotalCountByClassIdAndClassDate(classId, startDate, endDate);
                classEnroll = classAttendRecordRepo.GetEnrollCountByClassIdAndClassDate(classId, startDate, endDate, attendMarkList);
                sumTotal = sumTotal + total;
                sumEnroll = sumEnroll + classEnroll;

                ClassInfo classInfo = classInfoRepo.GetAClassByClassId(classId);

                if (classInfo.getClassTypeNum() <= 5) {

                    int classDayOrNight = 0;

                    if (classInfo.getClassDayOrNight() != null && classInfo.getClassDayOrNight().length() > 0) {
                        classDayOrNight = classInfo.getClassDayOrNight().substring(0,1).equals("日") ? 1 : 2;
                    }

                    int item = (classDayOrNight * 10) + classInfo.getClassTypeNum();

                    switch (item) {
                        case 11: // 日初

                            classDay1Enroll += classEnroll;
                            classDay1Total += total;
                            break;
                        case 12: // 日中

                            classDay2Enroll += classEnroll;
                            classDay2Total += total;
                            break;
                        case 13: // 日高

                            classDay3Enroll += classEnroll;
                            classDay3Total += total;
                            break;
                        case 14: // 日研一

                            classDay4Enroll += classEnroll;
                            classDay4Total += total;
                            break;
                        case 15: // 日研二

                            classDay5Enroll += classEnroll;
                            classDay5Total += total;
                            break;
                        case 21: // 夜初

                            classNight1Enroll += classEnroll;
                            classNight1Total += total;
                            break;
                        case 22: // 夜中

                            classNight2Enroll += classEnroll;
                            classNight2Total += total;
                            break;
                        case 23: // 夜高

                            classNight3Enroll += classEnroll;
                            classNight3Total += total;
                            break;
                        case 24: // 夜研一

                            classNight4Enroll += classEnroll;
                            classNight4Total += total;
                            break;
                        case 25: // 夜研二

                            classNight5Enroll += classEnroll;
                            classNight5Total += total;
                            break;
                    }

                }else {
                    classOtherEnroll += classEnroll;
                    classOtherTotal += total;
                }
            }

            items.add(new Common002Response.Common002Object("日初", String.valueOf(classDay1Enroll), CommonUtils.Percentage(classDay1Enroll, classDay1Total)));
            items.add(new Common002Response.Common002Object("日中", String.valueOf(classDay2Enroll), CommonUtils.Percentage(classDay2Enroll, classDay2Total)));
            items.add(new Common002Response.Common002Object("日高", String.valueOf(classDay3Enroll), CommonUtils.Percentage(classDay3Enroll, classDay3Total)));
            items.add(new Common002Response.Common002Object("日研一", String.valueOf(classDay4Enroll), CommonUtils.Percentage(classDay4Enroll, classDay4Total)));
            items.add(new Common002Response.Common002Object("日研二", String.valueOf(classDay5Enroll), CommonUtils.Percentage(classDay5Enroll, classDay5Total)));
            items.add(new Common002Response.Common002Object("夜初", String.valueOf(classNight1Enroll), CommonUtils.Percentage(classNight1Enroll, classNight1Total)));
            items.add(new Common002Response.Common002Object("夜中", String.valueOf(classNight2Enroll), CommonUtils.Percentage(classNight2Enroll, classNight2Total)));
            items.add(new Common002Response.Common002Object("夜高", String.valueOf(classNight3Enroll), CommonUtils.Percentage(classNight3Enroll, classNight3Total)));
            items.add(new Common002Response.Common002Object("夜研一", String.valueOf(classNight4Enroll), CommonUtils.Percentage(classNight4Enroll, classNight4Total)));
            items.add(new Common002Response.Common002Object("夜研二", String.valueOf(classNight5Enroll), CommonUtils.Percentage(classNight5Enroll, classNight5Total)));
            items.add(new Common002Response.Common002Object("其他", String.valueOf(classOtherEnroll), CommonUtils.Percentage(classOtherEnroll, classOtherTotal)));

            common002Response.setItems(items);

        }

        if (sumTotal > 0) {
            attendance = CommonUtils.PercentageInt(sumEnroll, sumTotal);
        }

//            logger.debug("Total : " + sumTotal);
//            logger.debug("Enroll : " + sumEnroll);
//            logger.debug("Attendance : " + attendance);

//        common002Response.setTotal(String.valueOf(total).replace(".0",""));
        common002Response.setTotal(String.valueOf(sumEnroll).replace(".0",""));
        common002Response.setAttendance(String.valueOf(attendance));

        return common002Response;
    }

    public String addClassInfo(Classes002Request request, HttpSession session) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        String errMessage = null;

        // 檢查期別
        int classPeriodNum = 1;

        List<String> className = new ArrayList<>();

//        if (classInfoRepo.GetClassInfoNumByClassDayOrNight(request.getClassDayOrNight()) != 0 &&
//                classInfoRepo.GetClassInfoNumByClassTypeNum(request.getClassTypeNum()) != 0) {
//
//            if (classInfoRepo.GetMaxClassPeriodNum(request.getClassDayOrNight(), request.getClassTypeNum()) != null) {
//
//                classPeriodNum = classInfoRepo.GetMaxClassPeriodNum(request.getClassDayOrNight(), request.getClassTypeNum());
//            }
//
//            className = classInfoRepo.GetClassListByClassPeriodNumAndUnitId(String.valueOf(classPeriodNum), unitId);
//        }

        if (classInfoRepo.GetClassInfoNumByClassDayOrNight(request.getClassDayOrNight(), unitId) != 0 &&
                classInfoRepo.GetClassInfoNumByClassTypeNum(request.getClassTypeNum(), unitId) != 0) {

            if (classInfoRepo.GetMaxClassPeriodNum(request.getClassDayOrNight(), request.getClassTypeNum(), unitId) != null) {

                classPeriodNum = classInfoRepo.GetMaxClassPeriodNum(request.getClassDayOrNight(), request.getClassTypeNum(), unitId);
            }

            className = classInfoRepo.GetClassListByClassPeriodNumAndUnitId(String.valueOf(classPeriodNum), unitId);
        }

        // 檢查起始、結束日期
        long start = java.sql.Date.valueOf(request.getClassStartDate()).getTime();
        long end = java.sql.Date.valueOf(request.getClassEndDate()).getTime();

        if (start >= end) {

            errMessage = "400;結業日期必須大於開課日期";

        }else {

            if (classPeriodNum > Integer.parseInt(request.getClassPeriodNum())) {

                errMessage = "400;期別必須大於或等於" + classPeriodNum;

            }else {

                if (classPeriodNum == Integer.parseInt(request.getClassPeriodNum()) && className.contains(request.getClassName())) {

                    errMessage = "400;班別名稱重複";

                }else {

                    ClassInfo classInfo = new ClassInfo();
                    classInfo.setUnitId((String) session.getAttribute(UserSession.Keys.UnitId.getValue()));
                    classInfo.setUnitName((String) session.getAttribute(UserSession.Keys.UnitName.getValue()));
//                    classInfo.setClassStatus(request.getClassStatus().equals("已結業") ? "已結業" : "未啟動");
                    classInfo.setClassStatus(request.getClassStatus());
                    classInfo.setClassId(classIdGenerator());
                    classInfo.setYear(java.sql.Date.valueOf(request.getClassStartDate()).toString().split("-")[0]);
                    classInfo.setClassStartDate(java.sql.Date.valueOf(request.getClassStartDate()));
                    classInfo.setClassEndDate(java.sql.Date.valueOf(request.getClassEndDate()));
                    classInfo.setDayOfWeek(request.getDayOfWeek());
                    classInfo.setClassDayOrNight(request.getClassDayOrNight());
                    classInfo.setClassTypeNum(request.getClassTypeNum());
                    classInfo.setClassTypeName(request.getClassTypeName());
                    classInfo.setClassPeriodNum(request.getClassPeriodNum());
                    classInfo.setClassName(request.getClassName());
                    classInfo.setClassLanguage(""); // 預設
                    classInfo.setTeachMasterName(""); // 預設
                    classInfo.setAssistantMasterName(""); // 預設
                    classInfo.setTeachMemberName(""); // 預設
                    classInfo.setClassPlace(""); // 預設
                    classInfo.setClassTotalNum(""); // 預設
                    classInfo.setAbsenceCountToFullAttend(3); // 預設
                    classInfo.setAbsenceCountToGraduate(2); // 預設
                    classInfo.setMakeupCountToGraduate(3); // 預設
                    classInfo.setAbsenceCountToDiligentAward(2); // 預設
//                    classInfo.setMakeupCountToDiligentAward(3); // 預設
                    classInfo.setMinutesBeLate(10); // 預設
//                    classInfo.setClassFullName(""); // 預設
                    classInfo.setClassFullName(request.getClassDayOrNight() + request.getClassTypeName()); // 預設
//                    classInfo.setCertificateName(""); // 預設
                    classInfo.setCertificateName(request.getClassDayOrNight() + request.getClassTypeName()); // 預設
                    classInfo.setAbbotName(""); // 預設
                    classInfo.setAbbotEngName(""); // 預設

                    if (request.getClassDayOrNight().equals("日間")){
                        classInfo.setClassStartTime(CommonUtils.ParseTime("9:30"));
                        classInfo.setClassEndTime(CommonUtils.ParseTime("11:30"));
                    } else if (request.getClassDayOrNight().equals("夜間")) {
                        classInfo.setClassStartTime(CommonUtils.ParseTime("19:30"));
                        classInfo.setClassEndTime(CommonUtils.ParseTime("21:30"));
                    }

                    classInfo.setClassDesc(""); // 預設
                    classInfo.setClassNote(""); // 預設
                    classInfo.setCreatorAndUpdater(session);

                    if (!request.getUnitId().equals("") && !request.getUnitName().equals("")) {

                        classInfo.setUnitId(request.getUnitId());
                        classInfo.setUnitName(request.getUnitName());
                    }

                    String classId = classInfoRepo.save(classInfo).getClassId();

                    classAttendMarkService.addClassAttendMark(classInfo.getClassId(), classInfo.getClassTypeNum(),
                            classInfo.getClassTypeName(), classInfo.getClassPeriodNum(), classInfo.getClassName(), session);

                    errMessage = "200;" + classId;
                }
            }
        }

        return errMessage;
    }

    public List<Classes001Object> getClassInfoList(String year, String classStatus, String classPeriodNum,
                                                   String className, String unitId) {

        List<Classes001Object> classes001ObjectList = new ArrayList<>();

        List<ClassInfo> classInfoList = new ArrayList<>();

        String code = (year.equals("") ? "0" : "1") +
                (classStatus.equals("") ? "0" : "1") +
                (classPeriodNum.equals("") ? "0" : "1") +
                (className.equals("") ? "0" : "1");

        switch (code) {
            case "1000":
                classInfoList = classInfoRepo.GetClassInfoList1000(year, unitId);
                break;
            case "0100":
                classInfoList = classInfoRepo.GetClassInfoList0100(classStatus, unitId);
                break;
            case "1100":
                classInfoList = classInfoRepo.GetClassInfoList1100(year, classStatus, unitId);
                break;
            case "1110":
                classInfoList = classInfoRepo.GetClassInfoList1110(year, classStatus, classPeriodNum, unitId);
                break;
            case "1111":
                classInfoList = classInfoRepo.GetClassInfoList1111(year, classStatus, classPeriodNum, className, unitId);
                break;
            case "0110":
                classInfoList = classInfoRepo.GetClassInfoList0110(classStatus, classPeriodNum, unitId);
                break;
        }

        if (classInfoList.size() != 0) {

            for (ClassInfo classInfo : classInfoList) {
                classes001ObjectList.add(new Classes001Object(
                        classInfo.getYear(),
                        classInfo.getClassId(),
                        classInfo.getClassStartDate().toString().split(" ")[0],
                        classInfo.getClassEndDate().toString().split(" ")[0],
                        classInfo.getDayOfWeek(),
                        classInfo.getClassDayOrNight(),
                        classInfo.getClassTypeName(),
                        classInfo.getClassPeriodNum(),
                        classInfo.getClassName(),
                        classInfo.getClassTotalNum(),
                        classInfo.getClassDesc(),
                        classInfo.getClassStatus()
                ));
            }
        }

        logger.debug(unitId + " class count:" + classes001ObjectList.size());
        return classes001ObjectList;
    }

    public String deleteClassInfoByClassId(String classId) {

        classInfoRepo.DeleteClassInfoByClassId(classId);

        classDateInfoRepo.DeleteClassDateInfoByClassId(classId);

        classEnrollFormRepo.DeleteClassEnrollFormByClassId(classId);

        classAttendRecordRepo.DeleteClassAttendRecordByClassId(classId);

        return "200;success";
    }

    public Classes003Response getClassInfoByClassId(String classId) {

        ClassInfo classInfo = classInfoRepo.GetClassInfoByClassId(classId);

        List<Classes003Object> classes003ObjectList = new ArrayList<>();

        List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetClassDateInfoByClassId(classId);

        boolean timeEditable = false;

        if (classDateInfoList.size() != 0) {

            List<Integer> attendMarkLastWeekNumList = classAttendRecordRepo.GetAttendMarkLastWeekNumListByClassId(classId);

            int attendMarkLastWeekNum;

            if (attendMarkLastWeekNumList.size() == 0) {
                attendMarkLastWeekNum = 0;
            }else {
                attendMarkLastWeekNum = attendMarkLastWeekNumList.get(attendMarkLastWeekNumList.size() - 1);
            }

            List<Boolean> editableList = new ArrayList<>();

            for (ClassDateInfo classDateInfo : classDateInfoList) {

                boolean editable;

                if (attendMarkLastWeekNum != 0 && attendMarkLastWeekNum >= classDateInfo.getClassWeeksNum()) {
                    editable = false;
                }else {
                    editable = true;
                }

                editableList.add(editable);

                classes003ObjectList.add(new Classes003Object(
                        (int) classDateInfo.getId(),
                        classDateInfo.getClassWeeksNum(),
                        classDateInfo.getClassDate().toString().split(" ")[0],
                        classDateInfo.getClassDayOfWeek(),
                        classDateInfo.getClassContent(),
                        classDateInfo.getActualClassContent(),
                        classDateInfo.getCancelled(),
                        editable
                ));
            }

            timeEditable = editableList.contains(false) ? false : true;
        }else {

            timeEditable = true;
        }

        UnitInfo unitInfo = unitInfoService.getUnit(classInfo.getUnitId());

        String abbotName = null;
        String abbotEngName = null;

        if (classInfo.getAbbotName() == null || classInfo.getAbbotName().equals("")) {
            abbotName = unitInfo.getAbbotName();
        }else {
            abbotName = classInfo.getAbbotName();
        }

        if (classInfo.getAbbotEngName() == null || classInfo.getAbbotEngName().equals("")) {
            abbotEngName = unitInfo.getAbbotEngName();
        }else {
            abbotEngName = classInfo.getAbbotEngName();
        }

        Classes003Response classes003Response = new Classes003Response(
                200,
                "success",
                classInfo.getClassStatus(),
                classInfo.getClassStartDate().toString().split(" ")[0],
                classInfo.getClassEndDate().toString().split(" ")[0],
                classInfo.getDayOfWeek(),
                classInfo.getClassDayOrNight(),
                classInfo.getClassTypeNum(),
                classInfo.getClassTypeName(),
                classInfo.getClassPeriodNum(),
                classInfo.getClassName(),
                classInfo.getClassLanguage(),
                classInfo.getTeachMasterName(),
                classInfo.getAssistantMasterName(),
                classInfo.getTeachMemberName(),
                classInfo.getClassPlace(),
                classInfo.getAbsenceCountToFullAttend(),
                classInfo.getAbsenceCountToGraduate(),
                classInfo.getMakeupCountToGraduate(),
                classInfo.getAbsenceCountToDiligentAward(),
                classInfo.getMinutesBeLate(),
                classInfo.getClassFullName(),
                classInfo.getCertificateName(),
                classInfo.getClassStartTime(),
                classInfo.getClassEndTime(),
                classInfo.getClassDesc(),
                classInfo.getClassNote(),
                classes003ObjectList,
                timeEditable,
                abbotName,
                abbotEngName,
                classInfo.getCertificateEngName(),
                unitInfo.getStateName(),
                classInfo.getMinMakeupCountToDiligentAward(),
                classInfo.getMaxMakeupCountToDiligentAward(),
                unitInfo.getCertUnitName(),
                unitInfo.getCertUnitEngName()
        );

        return classes003Response;
    }

    public String editClassInfoByClassId(String unitId, String classId, Classes005Request request, HttpSession session) {

        String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());

        ClassInfo classInfo = classInfoRepo.GetClassInfoByClassId(classId);

        String errMessage = null;

        // 檢查期別
        int classPeriodNum = 1;

        if (classInfoRepo.GetClassInfoNumByClassDayOrNight(request.getClassDayOrNight(), unitId) != 0 &&
                classInfoRepo.GetClassInfoNumByClassTypeNum(request.getClassTypeNum(), unitId) != 0) {

            classPeriodNum = classInfoRepo.GetMaxClassPeriodNum(request.getClassDayOrNight(), request.getClassTypeNum(), unitId);
        }

        // 檢查起始、結束日期
        long start = java.sql.Date.valueOf(request.getClassStartDate()).getTime();
        long end = java.sql.Date.valueOf(request.getClassEndDate()).getTime();

        if (start >= end) {
            errMessage = "400;結業日期必須大於開課日期";
        }else {
            if (Integer.parseInt(classInfo.getClassPeriodNum()) != Integer.parseInt(request.getClassPeriodNum()) &&
                    classPeriodNum > Integer.parseInt(request.getClassPeriodNum())) {

                errMessage = "400;期別必須等於原期別" + classInfo.getClassPeriodNum() + "，或大於或等於" + classPeriodNum;
            }else {
//                int classTotalNum = 0;

                if (request.getClassDates().size() != 0) {

//                    // 不是新增、編輯、刪除的也要變更classDate
                    List<Classes005Object> classes005Objects = request.getClassDates().stream()
                            .filter(a -> a.getIsCancelled() == false && a.getIsEdited() == false &&
                                    a.getIsDeleted() == false && a.getIsAdded() == false)
                            .collect(Collectors.toList());
//                    print2JSON("Normal", classes005Objects);

                    if (classes005Objects.size() != 0) {

                        List<ClassDateInfo> classDateInfoList = new ArrayList<>();

                        List<ClassAttendRecord> classAttendRecordList = new ArrayList<>();

                        for (Classes005Object classes005Object : classes005Objects) {

                            ClassDateInfo classDateInfo = classDateInfoRepo.GetClassDateInfoById(classes005Object.getId());

                            int oldClassWeeksNum = classDateInfo.getClassWeeksNum();

                            int newClassWeeksNum = classes005Object.getClassWeeksNum();

                            classDateInfo.setClassWeeksNum(classes005Object.getClassWeeksNum());
                            classDateInfo.setClassDate(java.sql.Date.valueOf(classes005Object.getClassDate()));
                            classDateInfo.setClassDayOfWeek(classes005Object.getClassDayOfWeek());
                            classDateInfo.setClassContent(classes005Object.getClassContent());
                            classDateInfo.setActualClassContent(classes005Object.getActualClassContent());
                            classDateInfo.setClassStartTime(ParseTime(request.getClassStartTime()));
                            classDateInfo.setClassEndTime(ParseTime(request.getClassEndTime()));
                            classDateInfo.setCancelled(classes005Object.getIsCancelled());
                            classDateInfo.setUpdater(session);

                            classDateInfoList.add(classDateInfo);

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));
                            Date classDate = null;
                            try {
                                classDate = dateFormat.parse(classDateInfo.getClassDate().toString().split(" ")[0] + " " + classDateInfo.getClassStartTime());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            for (Long id : classAttendRecordRepo.GetClassAttendRecordListByClassIdAndClassWeeksNum(classId, oldClassWeeksNum)) {

                                ClassAttendRecord record = classAttendRecordRepo.GetClassAttendRecordById(id);

                                record.setClassWeeksNum(newClassWeeksNum);
                                record.setClassDate(classDate);

                                classAttendRecordList.add(record);
                            }
                        }

                        // edit classDateInfo
                        classDateInfoRepo.saveAll(classDateInfoList);

                        // edit classAttendRecord
                        classAttendRecordRepo.saveAll(classAttendRecordList);
                    }

                    // 刪除
                    classes005Objects = request.getClassDates().stream()
                            .filter(a -> a.getIsDeleted() == true).collect(Collectors.toList());
                    //print2JSON("Delete", classes005Objects);

                    if (classes005Objects.size() != 0) {

                        List<Long> ids = new ArrayList<>();

                        for (Classes005Object classes005Object : classes005Objects) {

                            ids.add((long) classes005Object.getId());
                        }

                        List<Integer> weeksNums = classDateInfoRepo.GetClassDateInfoWeeksNumsByIds(ids);

                        // delete classDateInfo
                        classDateInfoRepo.DeleteClassDateInfoByIds(ids);

                        // delete classAttendRecord
                        classAttendRecordRepo.DeleteClassAttendRecordByClassIdAndClassWeeksNums(classId, weeksNums);
                    }

                    // 編輯
                    classes005Objects = request.getClassDates().stream()
                            .filter(a -> a.getIsEdited() == true && a.getIsDeleted() == false).collect(Collectors.toList());
//                    print2JSON("Edit", classes005Objects);

                    if (classes005Objects.size() != 0) {

                        List<ClassDateInfo> classDateInfoList = new ArrayList<>();

                        List<ClassAttendRecord> classAttendRecordList = new ArrayList<>();

                        for (Classes005Object classes005Object : classes005Objects) {

                            ClassDateInfo classDateInfo = classDateInfoRepo.GetClassDateInfoById(classes005Object.getId());

                            int oldClassWeeksNum = classDateInfo.getClassWeeksNum();

                            int newClassWeeksNum = classes005Object.getClassWeeksNum();

                            classDateInfo.setClassWeeksNum(classes005Object.getClassWeeksNum());
                            classDateInfo.setClassDate(java.sql.Date.valueOf(classes005Object.getClassDate()));
                            classDateInfo.setClassDayOfWeek(classes005Object.getClassDayOfWeek());
                            classDateInfo.setClassContent(classes005Object.getClassContent());
                            classDateInfo.setActualClassContent(classes005Object.getActualClassContent());
                            classDateInfo.setClassStartTime(ParseTime(request.getClassStartTime()));
                            classDateInfo.setClassEndTime(ParseTime(request.getClassEndTime()));
                            classDateInfo.setCancelled(classes005Object.getIsCancelled());
                            classDateInfo.setUpdater(session);

                            classDateInfoList.add(classDateInfo);

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));
                            Date classDate = null;
                            try {
                                classDate = dateFormat.parse(classDateInfo.getClassDate().toString().split(" ")[0] + " " + classDateInfo.getClassStartTime());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            for (Long id : classAttendRecordRepo.GetClassAttendRecordListByClassIdAndClassWeeksNum(classId, oldClassWeeksNum)) {

                                ClassAttendRecord record = classAttendRecordRepo.GetClassAttendRecordById(id);

                                record.setClassWeeksNum(newClassWeeksNum);
                                record.setClassDate(classDate);

                                classAttendRecordList.add(record);
                            }
                        }

                        // edit classDateInfo
                        classDateInfoRepo.saveAll(classDateInfoList);

                        // edit classAttendRecord
                        classAttendRecordRepo.saveAll(classAttendRecordList);
                    }

                    // 新增
                    classes005Objects = request.getClassDates().stream()
                            .filter(a -> a.getIsAdded() == true).collect(Collectors.toList());
                    //print2JSON("Add", classes005Objects);

                    if (classes005Objects.size() != 0) {

                        List<ClassEnrollForm> classEnrollFormList = classEnrollFormRepo.GetClassEnrollFormListByClassId(classId);

                        List<ClassDateInfo> classDateInfoList = new ArrayList<>();

                        List<ClassAttendRecord> classAttendRecordList = new ArrayList<>();

                        for (Classes005Object classes005Object : classes005Objects) {

                            ClassDateInfo classDateInfo = new ClassDateInfo();

                            classDateInfo.setUnitId((String) session.getAttribute(UserSession.Keys.UnitId.getValue()));
                            classDateInfo.setUnitName((String) session.getAttribute(UserSession.Keys.UnitName.getValue()));
                            classDateInfo.setClassId(classId);
                            classDateInfo.setClassWeeksNum(classes005Object.getClassWeeksNum());
                            classDateInfo.setClassDate(java.sql.Date.valueOf(classes005Object.getClassDate()));
                            classDateInfo.setClassDayOfWeek(classes005Object.getClassDayOfWeek());
                            classDateInfo.setClassContent(classes005Object.getClassContent());
                            classDateInfo.setActualClassContent(classes005Object.getActualClassContent());
                            classDateInfo.setClassStartTime(ParseTime(request.getClassStartTime()));
                            classDateInfo.setClassEndTime(ParseTime(request.getClassEndTime()));
                            classDateInfo.setCancelled(classes005Object.getIsCancelled());
                            classDateInfo.setCreatorAndUpdater(session);

                            classDateInfoList.add(classDateInfo);

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));
                            Date classDate = null;
                            try {
                                classDate = dateFormat.parse(classDateInfo.getClassDate().toString().split(" ")[0] + " " + classDateInfo.getClassStartTime());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            for (ClassEnrollForm classEnrollForm : classEnrollFormList) {

                                ClassAttendRecord classAttendRecord = new ClassAttendRecord(
                                        classDateInfo.getUnitId(),
                                        classDateInfo.getUnitName(),
                                        classEnrollForm.getClassEnrollFormId(),
                                        classEnrollForm.getMemberId(),
                                        classEnrollForm.getGender(),
                                        classEnrollForm.getAliasName(),
                                        classEnrollForm.getCtDharmaName(),
                                        classEnrollForm.getClassId(),
                                        classEnrollForm.getClassPeriodNum(),
                                        classEnrollForm.getClassName(),
                                        classEnrollForm.getClassGroupId(),
                                        classDateInfo.getClassWeeksNum(),
                                        classDate,
                                        null,
                                        null,
                                        null,
                                        null
                                );

                                classAttendRecord.setCreatorAndUpdater(session);

                                classAttendRecordList.add(classAttendRecord);
                            }
                        }

                        // add classDateInfo
                        classDateInfoRepo.saveAll(classDateInfoList);

                        // add classAttendRecord
                        classAttendRecordRepo.saveAll(classAttendRecordList);
                    }

                }else {

                    // 刪除
                    classDateInfoRepo.DeleteAllClassDateInfoByClassId(classId);

                    // 刪除classAttendRecord
                    classAttendRecordRepo.DeleteClassAttendRecordByClassId(classId);

                }

                classInfo.setClassStatus(request.getClassStatus());
                classInfo.setClassStartDate(java.sql.Date.valueOf(request.getClassStartDate()));
                classInfo.setClassEndDate(java.sql.Date.valueOf(request.getClassEndDate()));
                classInfo.setYear(java.sql.Date.valueOf(request.getClassStartDate()).toString().split("-")[0]);
                classInfo.setDayOfWeek(request.getDayOfWeek());
                classInfo.setClassDayOrNight(request.getClassDayOrNight());
                classInfo.setClassTypeNum(request.getClassTypeNum());
                classInfo.setClassTypeName(request.getClassTypeName());
                classInfo.setClassPeriodNum(request.getClassPeriodNum());
                classInfo.setClassName(request.getClassName());
                classInfo.setClassLanguage(request.getClassLanguage());
                classInfo.setTeachMasterName(request.getTeachMasterName());
                classInfo.setAssistantMasterName(request.getAssistantMasterName());
                classInfo.setTeachMemberName(request.getTeachMemberName());
                classInfo.setClassPlace(request.getClassPlace());
                classInfo.setClassTotalNum(String.valueOf(classDateInfoRepo.GetClassDateInfoCountByClassId(classId)));
                classInfo.setAbsenceCountToFullAttend(request.getAbsenceCountToFullAttend());
                classInfo.setAbsenceCountToGraduate(request.getAbsenceCountToGraduate());
                classInfo.setMakeupCountToGraduate(request.getMakeupCountToGraduate());
                classInfo.setAbsenceCountToDiligentAward(request.getAbsenceCountToDiligentAward());
//                classInfo.setMakeupCountToDiligentAward(request.getMakeupCountToDiligentAward());
                classInfo.setMinutesBeLate(request.getMinutesBeLate());
                classInfo.setClassFullName(request.getClassFullName());
                classInfo.setCertificateName(request.getCertificateName());
                classInfo.setClassStartTime(ParseTime(request.getClassStartTime()));
                classInfo.setClassEndTime(ParseTime(request.getClassEndTime()));
                classInfo.setClassDesc(request.getClassDesc());
                classInfo.setClassNote(request.getClassNote());

                classInfo.setAbbotName(request.getAbbotName());
                classInfo.setAbbotEngName(request.getAbbotEngName());
                classInfo.setCertificateEngName(request.getCertificateEngName());

                classInfo.setMinMakeupCountToDiligentAward(request.getMinMakeupCountToDiligentAward());
                classInfo.setMaxMakeupCountToDiligentAward(request.getMaxMakeupCountToDiligentAward());

                classInfo.setUpdater(session);

                classInfoRepo.save(classInfo);

                errMessage = "200;success. Edited classId = " + classId;
            }
        }

        unitInfoService.updateUnit(unitId, request, session);

        return errMessage;
    }

    public List<Members008Object> getClassDataList(String unitId) {

        List<Members008Object> classDataList = classInfoRepo.GetClassDataList(unitId);

        return classDataList;
    }

    // DataSearch class count by unit id and class status
    public Long getClassCountByUnitIdClassStatus(String unitId, String classStatus) {
        return classInfoRepo.GetClassCountByUnitStatus(unitId, classStatus);
    }

    // generate class id
    public String classIdGenerator() {
        String criteria = "CLS" + CommonUtils.GetMinGoYearStr();

        String currentMaxValue = classInfoRepo.GetMaxAssetId(criteria + '%');

        logger.debug("criteria: " + criteria);
        logger.debug("current max: " + currentMaxValue);

        long tempId = 0;

        if (currentMaxValue != null) {

            currentMaxValue = currentMaxValue.substring(3);

            tempId = Long.parseLong(currentMaxValue);
        } else {
            tempId = Long.parseLong(criteria.substring(3)) * 100000;
        }

        tempId++;

        return "CLS" + Long.toString(tempId);
    }

    public ClassInfo getClassInfoByClassId4Import(String classId) {
        return classInfoRepo.GetClassInfoByClassId(classId);
    }

    // 取得上課記錄的狀態列表
    public List<String> getClassStatus(HttpSession session, String year) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
        String userId = (String) session.getAttribute(UserSession.Keys.UserId.getValue());

        List<String> classStatusList = new ArrayList<>();

        if (isMaster(session)) {
            classStatusList = classInfoRepo.GetStatusByUnitYear(unitId, year);
        } else {
            List<String> classIds = userInfoService.getManagedClassIds(userId, unitId);

            if (classIds.size() > 0) {
                classStatusList = classInfoRepo.GetStatusByClassIds(classIds, year);
            }
        }

        return classStatusList;
    }

    // 取得上課記錄的期別列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK
    public List<String> getClassRecordClassPeriodNumList(HttpSession session, String year, String classStatus) {

        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());
        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<String> classPeriodNumList = new ArrayList<>();

        if (isMaster(session)) {
            // 法師權限

            classPeriodNumList = classInfoRepo.GetClassPeriodNumList1(year, classStatus, unitId);
        } else {
            // 居士權限

            List<ClassRecord000Object> classRecord000ObjectList = classEnrollFormRepo.GetClassRecordList(memberId);

            List<ClassRecord000Object> classRecord000ObjectList2;

            switch (classStatus) {
                case "開課中":

                    // 跨班調閱
                    boolean isSeeAllClassRecord = false;

                    isSeeAllClassRecord = classRecord000ObjectList
                            .stream()
                            .filter(p -> p.getIsSeeAllClassRecord() == true &&
                                    p.getYear().equals(year) &&
                                    p.getClassStatus().equals("開課中"))
                            .collect(Collectors.toList())
                            .size() == 0 ? false : true;

                    if (isSeeAllClassRecord) {

                        // 可跨班調閱，同法師權限可查全部

                        classPeriodNumList = classInfoRepo.GetClassPeriodNumList1(year, classStatus, unitId);
                    }else {

                        // 不可跨班調閱

                        classRecord000ObjectList2 = classRecord000ObjectList
                                .stream()
                                .filter(p -> p.getYear().equals(year) &&
                                        p.getClassStatus().equals("開課中") &&
                                        p.getClassJobTitleList() != null &&
                                        p.getIsExecuteClassAttendRecord() == true)
                                .collect(Collectors.toList());

                        if (classRecord000ObjectList2.size() != 0) {

                            for (ClassRecord000Object classRecord000Object : classRecord000ObjectList2) {

                                if (!classPeriodNumList.contains(classRecord000Object.getClassPeriodNum())) {

                                    classPeriodNumList.add(classRecord000Object.getClassPeriodNum());
                                }
                            }
                        }
                    }
                    break;
                case "待結業":

                    classRecord000ObjectList2 = classRecord000ObjectList
                            .stream()
                            .filter(p -> p.getYear().equals(year) &&
                                    p.getClassStatus().equals("待結業") &&
                                    p.getClassJobTitleList() != null &&
                                    p.getIsExecuteClassAttendRecord() == true)
                            .collect(Collectors.toList());

                    if (classRecord000ObjectList2.size() != 0) {

                        for (ClassRecord000Object classRecord000Object : classRecord000ObjectList2) {

                            if (!classPeriodNumList.contains(classRecord000Object.getClassPeriodNum())) {

                                classPeriodNumList.add(classRecord000Object.getClassPeriodNum());
                            }
                        }
                    }
                    break;
                case "已結業":

                    classRecord000ObjectList2 = classRecord000ObjectList
                            .stream()
                            .filter(p -> p.getYear().equals(year) &&
                                    p.getClassStatus().equals("待結業") &&
                                    p.getClassJobTitleList() != null &&
                                    p.getIsExecuteClassAttendRecord() == true)
                            .collect(Collectors.toList());

                    if (classRecord000ObjectList2.size() != 0) {

                        for (ClassRecord000Object classRecord000Object : classRecord000ObjectList2) {

                            if (!classPeriodNumList.contains(classRecord000Object.getClassPeriodNum())) {

                                classPeriodNumList.add(classRecord000Object.getClassPeriodNum());
                            }
                        }
                    }
                    break;
            }
        }

        Collections.sort(classPeriodNumList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });

        return classPeriodNumList;
    }

    // 取得上課記錄的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK (2018/12/03 舊版暫時關閉)
//    public List<ClassRecord002object> getClassRecordClassNameList(HttpSession session, String year, String classStatus, String classPeriodNum) {
//
//        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());
//
//        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
//
//        List<ClassRecord002object> classNameList = new ArrayList<>();
//
//        if (isMaster(session)) {
//            // 法師權限
//
//            classNameList = classInfoRepo.GetClassNameList1(year, classStatus, classPeriodNum, unitId);
//        }else {
//            // 居士權限
//
//            List<ClassRecord000Object> classRecord000ObjectList = classEnrollFormRepo.GetClassRecordList(memberId);
//
//            List<ClassRecord000Object> classRecord000ObjectList2 = new ArrayList<>();
//
//            switch (classStatus) {
//                case "開課中":
//
//                    // 跨班調閱
//                    boolean isSeeAllClassRecord = false;
//
//                    isSeeAllClassRecord = classRecord000ObjectList
//                            .stream()
//                            .filter(p -> p.getIsSeeAllClassRecord() == true &&
//                                    p.getYear().equals(year) &&
//                                    p.getClassStatus().equals("開課中"))
//                            .collect(Collectors.toList())
//                            .size() == 0 ? false : true;
//
//                    if (isSeeAllClassRecord) {
//
//                        classNameList = classInfoRepo.GetClassNameList1(year, classStatus, classPeriodNum, unitId);
//                    }else {
//
//                        classRecord000ObjectList2 = classRecord000ObjectList
//                                .stream()
//                                .filter(p -> p.getYear().equals(year) &&
//                                        p.getClassStatus().equals("開課中") &&
//                                        p.getClassPeriodNum().equals(classPeriodNum) &&
//                                        p.getClassJobTitleList() != null &&
//                                        p.getIsExecuteClassAttendRecord() == true)
//                                .collect(Collectors.toList());
//                    }
//                    break;
//                case "待結業":
//
//                    classRecord000ObjectList2 = classRecord000ObjectList
//                            .stream()
//                            .filter(p -> p.getYear().equals(year) &&
//                                    p.getClassStatus().equals("待結業") &&
//                                    p.getClassPeriodNum().equals(classPeriodNum) &&
//                                    p.getClassJobTitleList() != null &&
//                                    p.getIsExecuteClassAttendRecord() == true)
//                            .collect(Collectors.toList());
//                    break;
//                case "已結業":
//
//                    classRecord000ObjectList2 = classRecord000ObjectList
//                            .stream()
//                            .filter(p -> p.getYear().equals(year) &&
//                                    p.getClassStatus().equals("已結業") &&
//                                    p.getClassPeriodNum().equals(classPeriodNum) &&
//                                    p.getClassJobTitleList() != null &&
//                                    p.getIsExecuteClassAttendRecord() == true)
//                            .collect(Collectors.toList());
//                    break;
//            }
//
//            if (classRecord000ObjectList2.size() != 0) {
//
//                for (ClassRecord000Object classRecord000Object : classRecord000ObjectList2) {
//
//                    classNameList.add(new ClassRecord002object(
//                            classRecord000Object.getClassId(),classRecord000Object.getClassName()
//                    ));
//                }
//            }
//        }
//
//        return classNameList;
//    }

    // 取得上課記錄的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) (2018/12/03 CR新版)
    public List<ClassRecord002object> getClassRecordClassNameList(HttpSession session, String year, String classStatus) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
        String userId = (String) session.getAttribute(UserSession.Keys.UserId.getValue());

        List<ClassRecord002object> classNameList = new ArrayList<>();

        if (isMaster(session)) {
            logger.debug("master");
            classNameList = classInfoRepo.GetClassNameList(year, classStatus, unitId);
        } else {
            logger.debug("not master");
            List<String> classIds = userInfoService.getManagedClassIds(userId, unitId);

            if (classIds.size() > 0) {
                classNameList = classInfoRepo.GetClassNameList(year, classStatus, classIds);
            }
        }

        return classNameList;
    }

    // get upgrade classed
    public List<ClassRecord002object> getClassRecordClassNameList(HttpSession session, String classId) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<ClassRecord002object> classNameList = new ArrayList<>();

        classNameList = classInfoRepo.GetUpgradeClassList(unitId, classId);

        return classNameList;
    }

    // 取得修業統計的狀態列表 ( 需卡法師 / 居士 / 學員長職事 身份) ( 開課中/待結業/已結業) OK
    public List<String> getClassStatsClassStatusList(HttpSession session) {

        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());

        List<String> classStatusList = new ArrayList<>();

        if (isMaster(session)) {
            // 法師權限

            classStatusList.add("開課中");
            classStatusList.add("待結業");
            classStatusList.add("已結業");
        }else {
            // 居士權限
            if (memberId != null) {

                List<ClassStats000Object> classStats000ObjectList = classEnrollFormRepo.getClassStatsList(memberId);

                List<ClassStats000Object> classStats000ObjectList2 = classStats000ObjectList
                        .stream().filter(p -> p.getClassJobTitleList() != null &&
                                p.getIsExecuteClassGraduateStats() == true)
                        .collect(Collectors.toList());

                List<String> classStatus = new ArrayList<>();

                for (ClassStats000Object classStats000Object : classStats000ObjectList2) {

                    if (!classStatus.contains(classStats000Object.getClassStatus())) {
                        classStatus.add(classStats000Object.getClassStatus());
                    }
                }

                if (classStatus.size() != 0) {

                    if (classStatus.contains("開課中")) {
                        classStatusList.add("開課中");
                    }

                    if (classStatus.contains("待結業")) {
                        classStatusList.add("待結業");
                    }

                    if (classStatus.contains("已結業")) {
                        classStatusList.add("已結業");
                    }
                }
            }
        }

        return classStatusList;
    }

    // 取得修業統計的期別列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK
    public List<String> getClassStatsClassPeriodNumList(HttpSession session, String classStatus) {

        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<String> classPeriodNumList = new ArrayList<>();

        if (isMaster(session)) {
            // 法師權限

            classPeriodNumList = classInfoRepo.GetClassPeriodNumList3(classStatus, unitId);
        }else {
            // 居士權限

            List<ClassStats000Object> classStats000ObjectList = classEnrollFormRepo.getClassStatsList(memberId);

            List<ClassStats000Object> classStats000ObjectList2;

            switch (classStatus) {
                case "開課中":

                    // 跨班調閱
                    boolean isSeeAllClassRecord = false;

                    isSeeAllClassRecord = classStats000ObjectList
                            .stream()
                            .filter(p -> p.getIsSeeAllClassRecord() == true &&
                                    p.getClassStatus().equals("開課中"))
                            .collect(Collectors.toList())
                            .size() == 0 ? false : true;

                    if (isSeeAllClassRecord) {

                        // 可跨班調閱，同法師權限可查全部

                        classPeriodNumList = classInfoRepo.GetClassPeriodNumList3(classStatus, unitId);
                    }else {

                        // 不可跨班調閱

                        classStats000ObjectList2 = classStats000ObjectList
                                .stream()
                                .filter(p -> p.getClassStatus().equals("開課中") &&
                                        p.getClassJobTitleList() != null &&
                                        p.getIsExecuteClassGraduateStats() == true)
                                .collect(Collectors.toList());

                        if (classStats000ObjectList2.size() != 0) {

                            for (ClassStats000Object classStats000Object : classStats000ObjectList2) {

                                if (!classPeriodNumList.contains(classStats000Object.getClassPeriodNum())) {

                                    classPeriodNumList.add(classStats000Object.getClassPeriodNum());
                                }
                            }
                        }
                    }
                    break;
                case "待結業":

                    classStats000ObjectList2 = classStats000ObjectList
                            .stream()
                            .filter(p -> p.getClassStatus().equals("待結業") &&
                                    p.getClassJobTitleList() != null &&
                                    p.getIsExecuteClassGraduateStats() == true)
                            .collect(Collectors.toList());

                    if (classStats000ObjectList2.size() != 0) {

                        for (ClassStats000Object classStats000Object : classStats000ObjectList2) {

                            if (!classPeriodNumList.contains(classStats000Object.getClassPeriodNum())) {

                                classPeriodNumList.add(classStats000Object.getClassPeriodNum());
                            }
                        }
                    }
                    break;
                case "已結業":

                    classStats000ObjectList2 = classStats000ObjectList
                            .stream()
                            .filter(p -> p.getClassStatus().equals("待結業") &&
                                    p.getClassJobTitleList() != null &&
                                    p.getIsExecuteClassGraduateStats() == true)
                            .collect(Collectors.toList());

                    if (classStats000ObjectList2.size() != 0) {

                        for (ClassStats000Object classStats000Object : classStats000ObjectList2) {

                            if (!classPeriodNumList.contains(classStats000Object.getClassPeriodNum())) {

                                classPeriodNumList.add(classStats000Object.getClassPeriodNum());
                            }
                        }
                    }
                    break;
            }
        }

        Collections.sort(classPeriodNumList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });

        return classPeriodNumList;
    }

    // 取得修業統計的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK (2018/12/03 舊版暫時關閉)
//    public List<ClassRecord002object> getClassStatsClassNameList(HttpSession session, String classStatus, String classPeriodNum) {
//
//        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());
//
//        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
//
//        List<ClassRecord002object> classNameList = new ArrayList<>();
//
//        if (isMaster(session)) {
//            // 法師權限
//
//            classNameList = classInfoRepo.GetClassNameList3(classStatus, classPeriodNum, unitId);
//        }else {
//            // 居士權限
//
//            List<ClassStats000Object> classStats000ObjectList = classEnrollFormRepo.getClassStatsList(memberId);
//
//            List<ClassStats000Object> classStats000ObjectList2 = new ArrayList<>();
//
//            switch (classStatus) {
//                case "開課中":
//
//                    // 跨班調閱
//                    boolean isSeeAllClassRecord = false;
//
//                    isSeeAllClassRecord = classStats000ObjectList
//                            .stream()
//                            .filter(p -> p.getIsSeeAllClassRecord() == true &&
//                                    p.getClassStatus().equals("開課中"))
//                            .collect(Collectors.toList())
//                            .size() == 0 ? false : true;
//
//                    if (isSeeAllClassRecord) {
//
//                        classNameList = classInfoRepo.GetClassNameList3(classStatus, classPeriodNum, unitId);
//                    }else {
//
//                        classStats000ObjectList2 = classStats000ObjectList
//                                .stream()
//                                .filter(p -> p.getClassStatus().equals("開課中") &&
//                                        p.getClassPeriodNum().equals(classPeriodNum) &&
//                                        p.getClassJobTitleList() != null &&
//                                        p.getIsExecuteClassGraduateStats() == true)
//                                .collect(Collectors.toList());
//                    }
//                    break;
//                case "待結業":
//
//                    classStats000ObjectList2 = classStats000ObjectList
//                            .stream()
//                            .filter(p -> p.getClassStatus().equals("待結業") &&
//                                    p.getClassPeriodNum().equals(classPeriodNum) &&
//                                    p.getClassJobTitleList() != null &&
//                                    p.getIsExecuteClassGraduateStats() == true)
//                            .collect(Collectors.toList());
//                    break;
//                case "已結業":
//
//                    classStats000ObjectList2 = classStats000ObjectList
//                            .stream()
//                            .filter(p -> p.getClassStatus().equals("已結業") &&
//                                    p.getClassPeriodNum().equals(classPeriodNum) &&
//                                    p.getClassJobTitleList() != null &&
//                                    p.getIsExecuteClassGraduateStats() == true)
//                            .collect(Collectors.toList());
//                    break;
//            }
//
//            if (classStats000ObjectList2.size() != 0) {
//
//                for (ClassStats000Object classStats000Object : classStats000ObjectList2) {
//
//                    classNameList.add(new ClassRecord002object(
//                            classStats000Object.getClassId(), classStats000Object.getClassName()
//                    ));
//                }
//            }
//        }
//
//        return classNameList;
//    }

    // 取得修業統計的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK (2018/12/03 舊版暫時關閉)
    public List<ClassRecord002object> getClassStatsClassNameList(HttpSession session, String classStatus) {

        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<ClassRecord002object> classNameList = new ArrayList<>();

        if (isMaster(session)) {
            // 法師權限

            classNameList = classInfoRepo.GetClassNameList3(classStatus, unitId);
        }else {
            // 居士權限

            List<ClassStats000Object> classStats000ObjectList = classEnrollFormRepo.getClassStatsList(memberId);

            List<ClassStats000Object> classStats000ObjectList2 = new ArrayList<>();

            switch (classStatus) {
                case "開課中":

                    // 跨班調閱
                    boolean isSeeAllClassRecord = false;

                    isSeeAllClassRecord = classStats000ObjectList
                            .stream()
                            .filter(p -> p.getIsSeeAllClassRecord() &&
                                    p.getClassStatus().equals("開課中"))
                            .collect(Collectors.toList())
                            .size() != 0;

                    if (isSeeAllClassRecord) {

                        classNameList = classInfoRepo.GetClassNameList3(classStatus, unitId);
                    }else {

                        classStats000ObjectList2 = classStats000ObjectList
                                .stream()
                                .filter(p -> p.getClassStatus().equals("開課中") &&
                                        p.getClassJobTitleList() != null &&
                                        p.getIsExecuteClassGraduateStats())
                                .collect(Collectors.toList());
                    }
                    break;
                case "待結業":

                    classStats000ObjectList2 = classStats000ObjectList
                            .stream()
                            .filter(p -> p.getClassStatus().equals("待結業") &&
                                    p.getClassJobTitleList() != null &&
                                    p.getIsExecuteClassGraduateStats())
                            .collect(Collectors.toList());
                    break;
                case "已結業":

                    classStats000ObjectList2 = classStats000ObjectList
                            .stream()
                            .filter(p -> p.getClassStatus().equals("已結業") &&
                                    p.getClassJobTitleList() != null &&
                                    p.getIsExecuteClassGraduateStats())
                            .collect(Collectors.toList());
                    break;
            }

            if (classStats000ObjectList2.size() != 0) {

                for (ClassStats000Object classStats000Object : classStats000ObjectList2) {

                    classNameList.add(new ClassRecord002object(
                            classStats000Object.getClassId(),
                            classStats000Object.getClassName(),
                            classStats000Object.getClassPeriodNum()
                    ));
                }
            }
        }

        return classNameList;
    }

    // 取得班級續升的狀態列表 ( 需卡法師 / 居士 / 學員長職事 身份) (可報名/開課中/待結業) OK
    public List<String> getClassUpgradeClassStatusList(HttpSession session) {

        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());

        List<String> classStatusList = new ArrayList<>();

        if (isMaster(session)) {
            // 法師權限

            classStatusList.add("可報名");
            classStatusList.add("開課中");
            classStatusList.add("待結業");
        }else {
            // 居士權限

            if (memberId != null) {

                List<ClassUpgrade000Object> classUpgrade000Objects = classEnrollFormRepo.getClassUpgradeList(memberId);

                List<ClassUpgrade000Object> classUpgrade000Objects2 = classUpgrade000Objects
                        .stream().filter(p -> p.getClassJobTitleList() != null &&
                                p.getIsExecuteClassUpGrade() == true)
                        .collect(Collectors.toList());

                List<String> classStatus = new ArrayList<>();

                for (ClassUpgrade000Object classUpgrade000Object : classUpgrade000Objects2) {

                    if (!classStatus.contains(classUpgrade000Object.getClassStatus())) {
                        classStatus.add(classUpgrade000Object.getClassStatus());
                    }
                }

                if (classStatus.size() != 0) {

                    if (classStatus.contains("可報名")) {
                        classStatusList.add("可報名");
                    }

                    if (classStatus.contains("開課中")) {
                        classStatusList.add("開課中");
                    }

                    if (classStatus.contains("待結業")) {
                        classStatusList.add("待結業");
                    }
                }
            }
        }

        return classStatusList;
    }

    // 取得班級續升的期別列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK
    public List<String> getClassUpgradeClassPeriodNumList(HttpSession session, String classStatus) {

        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<String> classPeriodNumList = new ArrayList<>();

        if (isMaster(session)) {
            // 法師權限

            classPeriodNumList = classInfoRepo.GetClassPeriodNumList3(classStatus, unitId);
        }else {
            // 居士權限

            List<ClassUpgrade000Object> classUpgrade000Objects = classEnrollFormRepo.getClassUpgradeList(memberId);

            List<ClassUpgrade000Object> classUpgrade000Objects2;

            switch (classStatus) {
                case "開課中":

                    // 跨班調閱
                    boolean isSeeAllClassRecord = false;

                    isSeeAllClassRecord = classUpgrade000Objects
                            .stream()
                            .filter(p -> p.getIsSeeAllClassRecord() == true &&
                                    p.getClassStatus().equals("開課中"))
                            .collect(Collectors.toList())
                            .size() == 0 ? false : true;

                    if (isSeeAllClassRecord) {

                        // 可跨班調閱，同法師權限可查全部

                        classPeriodNumList = classInfoRepo.GetClassPeriodNumList3(classStatus, unitId);
                    }else {

                        // 不可跨班調閱

                        classUpgrade000Objects2 = classUpgrade000Objects
                                .stream()
                                .filter(p -> p.getClassStatus().equals("開課中") &&
                                        p.getClassJobTitleList() != null &&
                                        p.getIsExecuteClassUpGrade() == true)
                                .collect(Collectors.toList());

                        if (classUpgrade000Objects2.size() != 0) {

                            for (ClassUpgrade000Object classUpgrade000Object : classUpgrade000Objects2) {

                                if (!classPeriodNumList.contains(classUpgrade000Object.getClassPeriodNum())) {

                                    classPeriodNumList.add(classUpgrade000Object.getClassPeriodNum());
                                }
                            }
                        }
                    }
                    break;
                case "待結業":

                    classUpgrade000Objects2 = classUpgrade000Objects
                            .stream()
                            .filter(p -> p.getClassStatus().equals("待結業") &&
                                    p.getClassJobTitleList() != null &&
                                    p.getIsExecuteClassUpGrade() == true)
                            .collect(Collectors.toList());

                    if (classUpgrade000Objects2.size() != 0) {

                        for (ClassUpgrade000Object classUpgrade000Object : classUpgrade000Objects2) {

                            if (!classPeriodNumList.contains(classUpgrade000Object.getClassPeriodNum())) {

                                classPeriodNumList.add(classUpgrade000Object.getClassPeriodNum());
                            }
                        }
                    }
                    break;
                case "已結業":

                    classUpgrade000Objects2 = classUpgrade000Objects
                            .stream()
                            .filter(p -> p.getClassStatus().equals("待結業") &&
                                    p.getClassJobTitleList() != null &&
                                    p.getIsExecuteClassUpGrade() == true)
                            .collect(Collectors.toList());

                    if (classUpgrade000Objects2.size() != 0) {

                        for (ClassUpgrade000Object classUpgrade000Object : classUpgrade000Objects2) {

                            if (!classPeriodNumList.contains(classUpgrade000Object.getClassPeriodNum())) {

                                classPeriodNumList.add(classUpgrade000Object.getClassPeriodNum());
                            }
                        }
                    }
                    break;
            }
        }

        Collections.sort(classPeriodNumList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });

        return classPeriodNumList;
    }

    // 取得班級續升的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK (2018/12/03 舊版暫時關閉)
//    public List<ClassRecord002object> getClassUpgradeClassNameList(HttpSession session, String classStatus, String classPeriodNum) {
//
//        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());
//
//        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
//
//        List<ClassRecord002object> classNameList = new ArrayList<>();
//
//        if (isMaster(session)) {
//            // 法師權限
//
//            classNameList = classInfoRepo.GetClassNameList3(classStatus, classPeriodNum, unitId);
//        }else {
//            // 居士權限
//
//            List<ClassUpgrade000Object> classUpgrade000Objects = classEnrollFormRepo.getClassUpgradeList(memberId);
//
//            List<ClassUpgrade000Object> classUpgrade000Objects2 = new ArrayList<>();
//
//            switch (classStatus) {
//                case "開課中":
//
//                    // 跨班調閱
//                    boolean isSeeAllClassRecord = false;
//
//                    isSeeAllClassRecord = classUpgrade000Objects
//                            .stream()
//                            .filter(p -> p.getIsSeeAllClassRecord() == true &&
//                                    p.getClassStatus().equals("開課中"))
//                            .collect(Collectors.toList())
//                            .size() == 0 ? false : true;
//
//                    if (isSeeAllClassRecord) {
//
//                        classNameList = classInfoRepo.GetClassNameList3(classStatus, classPeriodNum, unitId);
//                    }else {
//
//                        classUpgrade000Objects2 = classUpgrade000Objects
//                                .stream()
//                                .filter(p -> p.getClassStatus().equals("開課中") &&
//                                        p.getClassPeriodNum().equals(classPeriodNum) &&
//                                        p.getClassJobTitleList() != null &&
//                                        p.getIsExecuteClassUpGrade() == true)
//                                .collect(Collectors.toList());
//                    }
//                    break;
//                case "待結業":
//
//                    classUpgrade000Objects2 = classUpgrade000Objects
//                            .stream()
//                            .filter(p -> p.getClassStatus().equals("待結業") &&
//                                    p.getClassPeriodNum().equals(classPeriodNum) &&
//                                    p.getClassJobTitleList() != null &&
//                                    p.getIsExecuteClassUpGrade() == true)
//                            .collect(Collectors.toList());
//                    break;
//                case "已結業":
//
//                    classUpgrade000Objects2 = classUpgrade000Objects
//                            .stream()
//                            .filter(p -> p.getClassStatus().equals("已結業") &&
//                                    p.getClassPeriodNum().equals(classPeriodNum) &&
//                                    p.getClassJobTitleList() != null &&
//                                    p.getIsExecuteClassUpGrade() == true)
//                            .collect(Collectors.toList());
//                    break;
//            }
//
//            if (classUpgrade000Objects2.size() != 0) {
//
//                for (ClassUpgrade000Object classUpgrade000Object : classUpgrade000Objects2) {
//
//                    classNameList.add(new ClassRecord002object(
//                            classUpgrade000Object.getClassId(), classUpgrade000Object.getClassName()
//                    ));
//                }
//            }
//        }
//
//        return classNameList;
//    }

    // 取得班級續升的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK (2018/12/03 CR新版)
    public List<ClassRecord002object> getClassUpgradeClassNameList(HttpSession session, String classStatus) {

        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<ClassRecord002object> classNameList = new ArrayList<>();

        if (isMaster(session)) {
            // 法師權限

            classNameList = classInfoRepo.GetClassNameList3(classStatus, unitId);
        }else {
            // 居士權限

            List<ClassUpgrade000Object> classUpgrade000Objects = classEnrollFormRepo.getClassUpgradeList(memberId);

            List<ClassUpgrade000Object> classUpgrade000Objects2 = new ArrayList<>();

            switch (classStatus) {
                case "開課中":

                    // 跨班調閱
                    boolean isSeeAllClassRecord = false;

                    isSeeAllClassRecord = classUpgrade000Objects
                            .stream()
                            .filter(p -> p.getIsSeeAllClassRecord() &&
                                    p.getClassStatus().equals("開課中"))
                            .collect(Collectors.toList())
                            .size() != 0 ;

                    if (isSeeAllClassRecord) {

                        classNameList = classInfoRepo.GetClassNameList3(classStatus, unitId);
                    }else {

                        classUpgrade000Objects2 = classUpgrade000Objects
                                .stream()
                                .filter(p -> p.getClassStatus().equals("開課中") &&
                                        p.getClassJobTitleList() != null &&
                                        p.getIsExecuteClassUpGrade())
                                .collect(Collectors.toList());
                    }
                    break;
                case "待結業":

                    classUpgrade000Objects2 = classUpgrade000Objects
                            .stream()
                            .filter(p -> p.getClassStatus().equals("待結業") &&
                                    p.getClassJobTitleList() != null &&
                                    p.getIsExecuteClassUpGrade())
                            .collect(Collectors.toList());
                    break;
                case "已結業":

                    classUpgrade000Objects2 = classUpgrade000Objects
                            .stream()
                            .filter(p -> p.getClassStatus().equals("已結業") &&
                                    p.getClassJobTitleList() != null &&
                                    p.getIsExecuteClassUpGrade())
                            .collect(Collectors.toList());
                    break;
            }

            if (classUpgrade000Objects2.size() != 0) {

                for (ClassUpgrade000Object classUpgrade000Object : classUpgrade000Objects2) {

                    classNameList.add(new ClassRecord002object(
                            classUpgrade000Object.getClassId(),
                            classUpgrade000Object.getClassName(),
                            classUpgrade000Object.getClassPeriodNum()
                    ));
                }
            }
        }

        return classNameList;
    }

    // data import
    public List<String> findClasses(String unitId, String classDayOrNight, String classTypeName, String classPeriodNum, String className) {
        if (className == null) {
            className = "";
        }

        return classInfoRepo.FindClasses(unitId, classDayOrNight, classTypeName, classPeriodNum, className);
    }

    // 更新課程狀態
    public void updateClassStatus() {
        logger.debug("updateClassStatus");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String date;

        // 可報名 -> 開課中
        Calendar calendar1 = Calendar.getInstance();

        date = format.format(calendar1.getTime());

        classInfoRepo.UpdateClassStatusOpen(java.sql.Date.valueOf(date));

        // 開課中 -> 待結業
        Calendar calendar2 = Calendar.getInstance();

        calendar2.add(Calendar.DATE, -2);

        date = format.format(calendar2.getTime());

        classInfoRepo.UpdateClassStatusClose(java.sql.Date.valueOf(date));

        // 待結業 -> 已結業
        Calendar calendar3 = Calendar.getInstance();

        calendar3.add(Calendar.DATE, -14);

        date = format.format(calendar3.getTime());

        classInfoRepo.UpdateClassStatusDone(java.sql.Date.valueOf(date));
    }

    // 取得「年度、狀態」取得課程列表，如果條件皆為空值，預設傳出「本期 "開課中"、"待結業" 所有課程"」
    public List<ClassInfo> getClassInfoListBySession(HttpSession session, String year, String classStatus) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<ClassInfo> classInfoList;

        if (year.isEmpty() && classStatus.isEmpty()) {

            classInfoList = classInfoRepo.GetClassInfoList(unitId);
        }else {

            classInfoList = classInfoRepo.GetClassInfoListByYearAndClassStatus(
                    unitId, year.isEmpty() ? null : year, classStatus.isEmpty() ? null : classStatus);
        }

        return classInfoList;
    }

    // TEST 自動新增課程
    public String addClass(HttpSession session) {

        String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());

        String[][] classDataList = new String[0][];

        switch (unitName) {
            case "靈泉":

                classDataList = new String[][]{
                        {"2018-01-01", "2018-03-31", "一", "夜間", "1", "初級班", "1", "靈泉台夜初一", "台語"},
                        {"2018-04-01", "2018-06-30", "一", "夜間", "1", "初級班", "2", "靈泉台夜初二", "台語"},
                        {"2018-07-01", "2018-09-30", "一", "夜間", "1", "初級班", "3", "靈泉台夜初三", "台語"},
                        {"2018-10-01", "2018-12-31", "一", "夜間", "1", "初級班", "4", "靈泉台夜初四", "台語"},
                        {"2018-01-10", "2018-03-31", "二", "夜間", "2", "中級班", "1", "靈泉夜中一", "客語"},
                        {"2018-04-10", "2018-06-30", "二", "夜間", "2", "中級班", "2", "靈泉夜中二", "客語"},
                        {"2018-07-10", "2018-09-30", "二", "夜間", "2", "中級班", "3", "靈泉夜中三", "客語"},
                        {"2018-10-10", "2018-12-31", "二", "夜間", "2", "中級班", "4", "靈泉夜中四", "客語"},
                        {"2018-01-03", "2018-03-27", "三", "夜間", "3", "高級班", "1", "靈泉夜高一", "國語"},
                        {"2018-04-01", "2018-06-30", "三", "夜間", "3", "高級班", "2", "靈泉夜高二", "國語"},
                        {"2018-07-02", "2018-09-29", "三", "夜間", "3", "高級班", "3", "靈泉夜高三", "國語"},
                        {"2018-10-08", "2018-12-30", "三", "夜間", "3", "高級班", "4", "靈泉夜高四", "國語"},
                        {"2018-01-01", "2018-03-24", "四", "夜間", "1", "高級班", "1", "靈泉國夜初一", "國語"},
                        {"2018-04-02", "2018-06-28", "四", "夜間", "1", "高級班", "2", "靈泉國夜初二", "國語"},
                        {"2018-07-09", "2018-09-25", "四", "夜間", "1", "高級班", "3", "靈泉國夜初三", "國語"},
                        {"2018-10-10", "2018-12-17", "四", "夜間", "1", "高級班", "4", "靈泉國夜初四", "國語"},
                        {"2018-01-06", "2018-03-23", "六", "日間", "11", "長青班", "1", "靈泉長青一", "台語"},
                        {"2018-04-02", "2018-06-22", "六", "日間", "11", "長青班", "2", "靈泉長青二", "台語"},
                        {"2018-07-07", "2018-09-28", "六", "日間", "11", "長青班", "3", "靈泉長青三", "台語"},
                        {"2018-10-13", "2018-12-13", "六", "日間", "11", "長青班", "4", "靈泉長青四", "台語"},
                        {"2018-01-06", "2018-03-23", "六", "日間", "10", "青少年班", "1", "靈泉青少一", "國語"},
                        {"2018-04-02", "2018-06-22", "六", "日間", "10", "青少年班", "2", "靈泉青少二", "國語"},
                        {"2018-07-07", "2018-09-28", "六", "日間", "10", "青少年班", "3", "靈泉青少三", "國語"},
                        {"2018-10-13", "2018-12-13", "六", "日間", "10", "青少年班", "4", "靈泉青少四", "國語"},
                        {"2018-01-20", "2018-03-24", "日", "日間", "4", "研一班", "1", "靈泉國日研一一", "國語"},
                        {"2018-04-07", "2018-06-21", "日", "日間", "4", "研一班", "2", "靈泉國日研一二", "國語"},
                        {"2018-07-04", "2018-09-27", "日", "日間", "4", "研一班", "3", "靈泉國日研一三", "國語"},
                        {"2018-10-19", "2018-12-29", "日", "日間", "4", "研一班", "4", "靈泉國日研一四", "國語"},
                        {"2018-01-13", "2018-03-27", "五", "日間", "5", "研二班", "1", "靈泉國日研二一", "國語"},
                        {"2018-04-10", "2018-06-22", "五", "日間", "5", "研二班", "2", "靈泉國日研二二", "國語"},
                        {"2018-07-14", "2018-09-28", "五", "日間", "5", "研二班", "3", "靈泉國日研二三", "國語"},
                        {"2018-10-18", "2018-12-30", "五", "日間", "5", "研二班", "4", "靈泉國日研二四", "國語"},
                        {"2018-01-10", "2018-03-23", "二", "日間", "12", "兒童班", "1", "靈泉兒童一", "國語"},
                        {"2018-04-01", "2018-06-29", "二", "日間", "12", "兒童班", "2", "靈泉兒童二", "國語"},
                        {"2018-07-05", "2018-09-29", "二", "日間", "12", "兒童班", "3", "靈泉兒童三", "國語"},
                        {"2018-10-09", "2018-12-21", "二", "日間", "12", "兒童班", "4", "靈泉兒童四", "國語"},
                        {"2018-01-08", "2018-03-27", "三", "日間", "15", "才藝班", "1", "靈泉才藝一", "日語"},
                        {"2018-04-05", "2018-06-23", "三", "日間", "15", "才藝班", "2", "靈泉才藝二", "日語"},
                        {"2018-07-08", "2018-09-28", "三", "日間", "15", "才藝班", "3", "靈泉才藝三", "日語"},
                        {"2018-10-02", "2018-12-25", "三", "日間", "15", "才藝班", "4", "靈泉才藝四", "日語"},
                        {"2018-01-05", "2018-03-30", "一", "夜間", "4", "研一班", "1", "靈泉藝夜研一一", "義大利語"},
                        {"2018-04-06", "2018-06-25", "一", "夜間", "4", "研一班", "2", "靈泉藝夜研一二", "義大利語"},
                        {"2018-07-10", "2018-09-28", "一", "夜間", "4", "研一班", "3", "靈泉藝夜研一三", "義大利語"},
                        {"2018-10-10", "2018-12-30", "一", "夜間", "4", "研一班", "4", "靈泉藝夜研一四", "義大利語"},
                        {"2018-01-17", "2018-03-28", "一", "夜間", "1", "初級班", "1", "靈泉台夜初一2017", "台語"},
                        {"2018-04-20", "2018-06-29", "一", "夜間", "1", "初級班", "2", "靈泉台夜初二2017", "台語"},
                        {"2018-07-15", "2018-09-26", "一", "夜間", "1", "初級班", "3", "靈泉台夜初三2017", "台語"},
                        {"2018-10-01", "2018-12-30", "一", "夜間", "1", "初級班", "4", "靈泉台夜初四2017", "台語"},
                        {"2018-01-16", "2018-03-22", "二", "夜間", "2", "中級班", "1", "靈泉夜中一2017", "客語"},
                        {"2018-04-18", "2018-06-27", "二", "夜間", "2", "中級班", "2", "靈泉夜中二2017", "客語"},
                        {"2018-07-01", "2018-09-25", "二", "夜間", "2", "中級班", "3", "靈泉夜中三2017", "客語"},
                        {"2018-10-03", "2018-12-26", "二", "夜間", "2", "中級班", "4", "靈泉夜中四2017", "客語"},
                        {"2018-01-05", "2018-03-30", "三", "夜間", "3", "高級班", "1", "靈泉夜高一2017", "國語"},
                        {"2018-04-17", "2018-06-23", "三", "夜間", "3", "高級班", "2", "靈泉夜高二2017", "國語"},
                        {"2018-07-13", "2018-09-21", "三", "夜間", "3", "高級班", "3", "靈泉夜高三2017", "國語"},
                        {"2018-10-08", "2018-12-22", "三", "夜間", "3", "高級班", "4", "靈泉夜高四2017", "國語"},
                        {"2018-01-20", "2018-03-28", "四", "夜間", "1", "初級班", "1", "靈泉國夜初一2017", "國語"},
                        {"2018-04-04", "2018-06-24", "四", "夜間", "1", "初級班", "2", "靈泉國夜初二2017", "國語"},
                        {"2018-07-05", "2018-09-27", "四", "夜間", "1", "初級班", "3", "靈泉國夜初三2017", "國語"},
                        {"2018-10-16", "2018-12-22", "四", "夜間", "1", "初級班", "4", "靈泉國夜初四2017", "國語"},
                        {"2018-01-13", "2018-03-21", "六", "日間", "11", "長青班", "1", "靈泉長青一2017", "台語"},
                        {"2018-04-09", "2018-06-26", "六", "日間", "11", "長青班", "2", "靈泉長青二2017", "台語"},
                        {"2018-07-08", "2018-09-22", "六", "日間", "11", "長青班", "3", "靈泉長青三2017", "台語"},
                        {"2018-10-12", "2018-12-25", "六", "日間", "11", "長青班", "4", "靈泉長青四2017", "台語"},
                        {"2018-01-20", "2018-03-27", "六", "日間", "10", "青少年班", "1", "靈泉青少一2017", "國語"},
                        {"2018-04-01", "2018-06-25", "六", "日間", "10", "青少年班", "2", "靈泉青少二2017", "國語"},
                        {"2018-07-01", "2018-09-23", "六", "日間", "10", "青少年班", "3", "靈泉青少三2017", "國語"},
                        {"2018-10-11", "2018-12-25", "六", "日間", "10", "青少年班", "4", "靈泉青少四2017", "國語"},
                        {"2018-01-06", "2018-03-23", "日", "日間", "4", "研一班", "1", "靈泉國日研一一2017", "國語"},
                        {"2018-04-04", "2018-06-24", "日", "日間", "4", "研一班", "2", "靈泉國日研一二2017", "國語"},
                        {"2018-07-14", "2018-09-26", "日", "日間", "4", "研一班", "3", "靈泉國日研一三2017", "國語"},
                        {"2018-10-04", "2018-12-22", "日", "日間", "4", "研一班", "4", "靈泉國日研一四2017", "國語"},
                        {"2018-01-13", "2018-03-27", "五", "日間", "5", "研二班", "1", "靈泉國日研二一2017", "國語"},
                        {"2018-04-17", "2018-06-30", "五", "日間", "5", "研二班", "2", "靈泉國日研二二2017", "國語"},
                        {"2018-07-14", "2018-09-25", "五", "日間", "5", "研二班", "3", "靈泉國日研二三2017", "國語"},
                        {"2018-10-01", "2018-12-22", "五", "日間", "5", "研二班", "4", "靈泉國日研二四2017", "國語"},
                        {"2018-01-08", "2018-03-21", "二", "日間", "12", "兒童班", "1", "靈泉兒童一2017", "國語"},
                        {"2018-04-13", "2018-06-26", "二", "日間", "12", "兒童班", "2", "靈泉兒童二2017", "國語"},
                        {"2018-07-07", "2018-09-21", "二", "日間", "12", "兒童班", "3", "靈泉兒童三2017", "國語"},
                        {"2018-10-10", "2018-12-29", "二", "日間", "12", "兒童班", "4", "靈泉兒童四2017", "國語"},
                        {"2018-01-16", "2018-03-24", "三", "日間", "15", "才藝班", "1", "靈泉才藝一2017", "日語"},
                        {"2018-04-11", "2018-06-21", "三", "日間", "15", "才藝班", "2", "靈泉才藝二2017", "日語"},
                        {"2018-07-06", "2018-09-21", "三", "日間", "15", "才藝班", "3", "靈泉才藝三2017", "日語"},
                        {"2018-10-09", "2018-12-22", "三", "日間", "15", "才藝班", "4", "靈泉才藝四2017", "日語"},
                        {"2018-01-05", "2018-03-24", "一", "夜間", "4", "研一班", "1", "靈泉義夜研一一2017", "義大利語"},
                        {"2018-04-13", "2018-06-29", "一", "夜間", "4", "研一班", "2", "靈泉義夜研一二2017", "義大利語"},
                        {"2018-07-01", "2018-09-30", "一", "夜間", "4", "研一班", "3", "靈泉義夜研一三2017", "義大利語"},
                        {"2018-10-09", "2018-12-20", "一", "夜間", "4", "研一班", "4", "靈泉義夜研一四2017", "義大利語"},
                };
                break;
            case "天祥":

                classDataList = new String[][]{
                        {"2018-01-19", "2018-03-21", "一", "夜間", "1", "初級班", "1", "天祥台夜初一", "台語"},
                        {"2018-04-01", "2018-06-24", "二", "夜間", "1", "初級班", "2", "天祥台夜初二", "台語"},
                        {"2018-07-02", "2018-09-29", "二", "夜間", "1", "初級班", "3", "天祥台夜初三", "台語"},
                        {"2018-10-15", "2018-12-25", "五", "夜間", "1", "初級班", "4", "天祥台夜初四", "台語"},
                        {"2018-01-20", "2018-03-26", "三", "夜間", "2", "中級班", "1", "天祥夜中一", "客語"},
                        {"2018-04-18", "2018-06-25", "一", "夜間", "2", "中級班", "2", "天祥夜中", "客語"},
                        {"2018-07-04", "2018-09-26", "五", "夜間", "2", "中級班", "3", "天祥夜中三", "客語"},
                        {"2018-10-13", "2018-12-25", "一", "夜間", "2", "中級班", "4", "天祥夜中四", "客語"},
                        {"2018-01-08", "2018-03-26", "二", "夜間", "3", "高級班", "1", "天祥夜高一", "國語"},
                        {"2018-04-07", "2018-06-26", "三", "夜間", "3", "高級班", "2", "天祥夜高二", "國語"},
                        {"2018-07-15", "2018-09-26", "五", "夜間", "3", "高級班", "3", "天祥夜高三", "國語"},
                        {"2018-10-16", "2018-12-26", "六", "夜間", "3", "高級班", "4", "天祥夜高四", "國語"},
                        {"2018-01-11", "2018-03-25", "一", "夜間", "1", "初級班", "1", "天祥夜初一", "國語"},
                        {"2018-04-18", "2018-06-25", "日", "夜間", "1", "初級班", "2", "天祥夜初二", "國語"},
                        {"2018-07-12", "2018-09-21", "日", "夜間", "1", "初級班", "3", "天祥夜初三", "國語"},
                        {"2018-10-11", "2018-12-23", "二", "夜間", "1", "初級班", "4", "天祥夜初四", "國語"},
                        {"2018-01-08", "2018-03-26", "五", "日間", "11", "長青班", "1", "天祥長青一", "台語"},
                        {"2018-07-05", "2018-09-27", "四", "日間", "11", "長青班", "3", "天祥長青三", "台語"},
                        {"2018-10-07", "2018-12-27", "日", "日間", "11", "長青班", "4", "天祥長青四", "台語"},
                        {"2018-01-07", "2018-03-25", "日", "日間", "10", "青少年班", "1", "天祥青少一", "國語"},
                        {"2018-04-12", "2018-06-27", "四", "日間", "10", "青少年班", "2", "天祥青少二", "國語"},
                        {"2018-07-08", "2018-09-25", "二", "日間", "10", "青少年班", "3", "天祥青少三", "國語"},
                        {"2018-10-11", "2018-12-28", "四", "日間", "10", "青少年班", "4", "天祥青少四", "國語"},
                        {"2018-04-13", "2018-06-28", "二", "日間", "4", "日研一班", "2", "天祥國日研一二", "國語"},
                        {"2018-07-14", "2018-09-30", "日", "日間", "4", "日研一班", "3", "天祥國日研一三", "國語"},
                        {"2018-10-16", "2018-12-29", "三", "日間", "4", "日研一班", "4", "天祥國日研一四", "國語"},
                        {"2018-04-04", "2018-06-25", "四", "日間", "5", "日研二班", "2", "天祥國日研二二", "國語"},
                        {"2018-07-03", "2018-09-23", "三", "日間", "5", "日研二班", "3", "天祥國日研二三", "國語"},
                        {"2018-10-04", "2018-12-23", "二", "日間", "5", "日研二班", "4", "天祥國日研二四", "國語"},
                        {"2018-01-03", "2018-03-29", "五", "日間", "12", "兒童班", "1", "天祥兒童一", "國語"},
                        {"2018-04-16", "2018-06-28", "日", "日間", "12", "兒童班", "2", "天祥兒童二", "國語"},
                        {"2018-07-02", "2018-09-26", "二", "日間", "12", "兒童班", "3", "天祥兒童三", "國語"},
                        {"2018-10-09", "2018-12-28", "六", "日間", "12", "兒童班", "4", "天祥兒童四", "國語"},
                        {"2018-04-06", "2018-06-21", "三", "日間", "15", "才藝班", "2", "天祥才藝二", "日語"},
                        {"2018-07-02", "2018-09-24", "一", "日間", "15", "才藝班", "3", "天祥才藝三", "日語"},
                        {"2018-10-17", "2018-12-22", "六", "日間", "15", "才藝班", "4", "天祥才藝四", "日語"},
                        {"2018-04-13", "2018-06-23", "六", "夜間", "4", "研一班", "2", "天祥夜研一二", "義大利語"},
                        {"2018-07-13", "2018-09-21", "一", "夜間", "4", "研一班", "3", "天祥夜研一三", "義大利語"},
                        {"2018-10-01", "2018-12-27", "三", "夜間", "4", "研一班", "4", "天祥夜研一四", "義大利語"},
                        {"2018-07-06", "2018-09-21", "一", "夜間", "1", "初級班", "3", "天祥台夜初三2017", "台語"},
                        {"2018-10-03", "2018-12-27", "日", "夜間", "1", "初級班", "4", "天祥台夜初四2017", "台語"},
                        {"2018-07-12", "2018-09-30", "日", "夜間", "2", "中級班", "3", "天祥台夜中三2017", "客語"},
                        {"2018-10-01", "2018-12-26", "五", "夜間", "2", "中級班", "4", "天祥台夜中四2017", "客語"},
                        {"2018-07-13", "2018-09-24", "六", "夜間", "3", "高級班", "3", "天祥台夜高三2017", "國語"},
                        {"2018-07-14", "2018-09-25", "二", "夜間", "1", "初級班", "3", "天祥台夜初三2017", "國語"},
                        {"2018-10-06", "2018-12-22", "三", "日間", "11", "長青班", "4", "天祥長青四2017", "台語"},
                        {"2018-10-10", "2018-12-28", "一", "日間", "10", "青少年班", "4", "天祥青少四2017", "國語"},
                        {"2018-10-14", "2018-12-25", "日", "日間", "4", "研一班", "4", "天祥國日研一四2017", "國語"},
                        {"2018-10-14", "2018-12-25", "日", "日間", "4", "研一班", "4", "天祥國日研一四2017", "國語"},
                        {"2018-07-07", "2018-09-23", "六", "日間", "5", "研二班", "3", "天祥國日研二三2017", "國語"},
                        {"2018-10-20", "2018-12-21", "一", "日間", "5", "研二班", "4", "天祥國日研二四2017", "國語"},
                        {"2018-07-10", "2018-09-26", "四", "日間", "12", "兒童班", "3", "天祥兒童三2017", "國語"},
                        {"2018-10-06", "2018-12-29", "四", "日間", "15", "兒童班", "4", "天祥兒童四2017", "國語"},
                        {"2018-07-01", "2018-09-24", "三", "日間", "15", "才藝班", "3", "天祥才藝三2017", "國語"},
                        {"2018-07-13", "2018-09-22", "六", "夜間", "4", "研一班", "3", "天祥義夜研一三2017", "義大利語"},
                        {"2018-10-17", "2018-12-23", "二", "夜間", "4", "研一班", "4", "天祥義夜研一四2017", "義大利語"},
                };
                break;
            case "蓮社":

                classDataList = new String[][]{
                        {"2018-04-08", "2018-06-23", "三", "夜間", "1", "初級班", "2", "蓮社台夜初二", "英語"},
                        {"2018-07-12", "2018-09-27", "三", "夜間", "1", "初級班", "3", "蓮社台夜初三", "英語"},
                        {"2018-10-14", "2018-12-26", "三", "夜間", "1", "初級班", "4", "蓮社台夜初四", "英語"},
                        {"2018-01-15", "2018-03-30", "日", "夜間", "2", "中級班", "1", "蓮社夜中一", "英語"},
                        {"2018-04-16", "2018-06-22", "六", "夜間", "2", "中級班", "2", "蓮社夜中二", "英語"},
                        {"2018-07-01", "2018-09-23", "一", "夜間", "2", "中級班", "3", "蓮社夜中三", "英語"},
                        {"2018-10-07", "2018-12-20", "三", "夜間", "2", "中級班", "4", "蓮社夜中四", "英語"},
                        {"2018-04-07", "2018-06-26", "三", "夜間", "3", "高級班", "2", "蓮社夜高二", "英語"},
                        {"2018-07-12", "2018-09-21", "日", "夜間", "3", "高級班", "3", "蓮社夜高三", "英語"},
                        {"2018-10-01", "2018-12-25", "日", "夜間", "3", "高級班", "4", "蓮社夜高四", "英語"},
                        {"2018-01-18", "2018-03-29", "四", "夜間", "1", "初級班", "1", "蓮社國夜初一", "英語"},
                        {"2018-04-20", "2018-06-21", "日", "夜間", "1", "初級班", "2", "蓮社國夜初二", "英語"},
                        {"2018-07-07", "2018-09-21", "三", "夜間", "1", "初級班", "3", "蓮社國夜初三", "英語"},
                        {"2018-10-15", "2018-12-22", "四", "夜間", "1", "初級班", "4", "蓮社國夜初四", "英語"},
                        {"2018-01-07", "2018-03-30", "四", "日間", "11", "長青班", "1", "蓮社長青一", "英語"},
                        {"2018-04-09", "2018-06-29", "二", "日間", "11", "長青班", "2", "蓮社長青二", "英語"},
                        {"2018-07-06", "2018-09-30", "五", "日間", "11", "長青班", "3", "蓮社長青三", "英語"},
                        {"2018-01-13", "2018-03-23", "四", "日間", "10", "青少年班", "1", "蓮社青少一", "英語"},
                        {"2018-04-13", "2018-06-23", "二", "日間", "10", "青少年班", "2", "蓮社青少二", "英語"},
                        {"2018-07-14", "2018-09-27", "三", "日間", "10", "青少年班", "3", "蓮社青少三", "英語"},
                        {"2018-10-18", "2018-12-27", "三", "日間", "10", "青少年班", "4", "蓮社青少四", "英語"},
                        {"2018-01-08", "2018-03-25", "一", "日間", "4", "研一班", "1", "蓮社國日研一一", "義大利語"},
                        {"2018-07-04", "2018-09-25", "三", "日間", "4", "研一班", "3", "蓮社國日研一三", "義大利語"},
                        {"2018-10-14", "2018-12-21", "五", "日間", "4", "研一班", "4", "蓮社國日研一四", "義大利語"},
                        {"2018-01-15", "2018-03-29", "三", "日間", "5", "研二班", "1", "蓮社國日研二一", "英語"},
                        {"2018-04-17", "2018-06-22", "日", "日間", "5", "研二班", "2", "蓮社國日研二二", "英語"},
                        {"2018-07-01", "2018-09-24", "日", "日間", "5", "研二班", "3", "蓮社國日研二三", "英語"},
                        {"2018-10-12", "2018-12-22", "五", "日間", "5", "研二班", "4", "蓮社國日研二四", "英語"},
                        {"2018-01-17", "2018-03-29", "一", "日間", "12", "兒童班", "1", "蓮社兒童一", "英語"},
                        {"2018-07-14", "2018-09-21", "二", "日間", "12", "兒童班", "3", "蓮社兒童三", "英語"},
                        {"2018-10-11", "2018-12-29", "五", "日間", "12", "兒童班", "4", "蓮社兒童四", "英語"},
                        {"2018-01-20", "2018-03-30", "三", "日間", "15", "才藝班", "1", "蓮社才藝一", "國語"},
                        {"2018-04-16", "2018-06-27", "四", "日間", "15", "才藝班", "2", "蓮社才藝二", "國語"},
                        {"2018-07-07", "2018-09-22", "五", "日間", "15", "才藝班", "3", "蓮社才藝三", "國語"},
                        {"2018-10-01", "2018-12-27", "三", "日間", "15", "才藝班", "4", "蓮社才藝四", "國語"},
                        {"2018-01-20", "2018-03-28", "三", "夜間", "4", "研一班", "1", "蓮社義夜研一一", "英語"},
                        {"2018-04-12", "2018-06-23", "一", "夜間", "4", "研一班", "2", "蓮社義夜研一二", "英語"},
                        {"2018-07-06", "2018-09-26", "日", "夜間", "4", "研一班", "3", "蓮社義夜研一三", "英語"},
                        {"2018-10-03", "2018-12-29", "一", "夜間", "4", "研一班", "4", "蓮社義夜研一四", "英語"},
                        {"2018-07-04", "2018-09-29", "二", "夜間", "1", "初級班", "3", "蓮社台夜初三2017", "義大利語"},
                        {"2018-10-04", "2018-12-29", "三", "夜間", "1", "初級班", "4", "蓮社台夜初四2017", "義大利語"},
                        {"2018-10-04", "2018-12-22", "三", "夜間", "2", "中級班", "4", "蓮社夜中四2017", "英語"},
                        {"2018-07-12", "2018-09-27", "日", "夜間", "3", "高級班", "3", "蓮社夜高三2017", "英語"},
                        {"2018-10-14", "2018-12-26", "三", "夜間", "3", "高級班", "4", "蓮社夜高四2017", "英語"},
                        {"2018-07-02", "2018-09-30", "六", "夜間", "1", "初級班", "3", "蓮社國夜初三2017", "國語"},
                        {"2018-07-01", "2018-09-24", "五", "日間", "11", "長青班", "3", "蓮社長青三2017", "英語"},
                        {"2018-07-15", "2018-09-29", "日", "日間", "10", "青少年班", "3", "蓮社青少三2017", "英語"},
                        {"2018-07-09", "2018-09-26", "一", "日間", "4", "研一班", "3", "蓮社國日研一三2017", "國語"},
                        {"2018-07-06", "2018-09-23", "日", "日間", "5", "研二班", "3", "蓮社國日研二三2017", "國語"},
                        {"2018-07-14", "2018-09-22", "四", "日間", "12", "兒童班", "3", "蓮社兒童三2017", "英語"},
                        {"2018-07-14", "2018-09-25", "一", "日間", "15", "才藝班", "3", "蓮社才藝三2017", "英語"},
                        {"2018-07-03", "2018-09-30", "日", "夜間", "4", "研一班", "3", "蓮社義夜研一三2017", "英語"},
                };
                break;
        }

        for (int i = 0; i < classDataList.length; i++) {

            ClassInfo classInfo = new ClassInfo();
            classInfo.setUnitId((String) session.getAttribute(UserSession.Keys.UnitId.getValue()));
            classInfo.setUnitName((String) session.getAttribute(UserSession.Keys.UnitName.getValue()));
            classInfo.setClassStatus("未啟動");
            classInfo.setClassId(classIdGenerator());
            classInfo.setYear(java.sql.Date.valueOf(classDataList[i][0]).toString().split("-")[0]);
            classInfo.setClassStartDate(java.sql.Date.valueOf(classDataList[i][0]));
            classInfo.setClassEndDate(java.sql.Date.valueOf(classDataList[i][1]));
            classInfo.setDayOfWeek(classDataList[i][2]);
            classInfo.setClassDayOrNight(classDataList[i][3]);
            classInfo.setClassTypeNum(Integer.parseInt(classDataList[i][4]));
            classInfo.setClassTypeName(classDataList[i][5]);
            classInfo.setClassPeriodNum(classDataList[i][6]);
            classInfo.setClassName(classDataList[i][7]);
            classInfo.setClassLanguage(classDataList[i][8]); // 預設
            classInfo.setTeachMasterName(""); // 預設
            classInfo.setAssistantMasterName(""); // 預設
            classInfo.setTeachMemberName(""); // 預設
            classInfo.setClassPlace(""); // 預設
            classInfo.setClassTotalNum(""); // 預設
            classInfo.setAbsenceCountToFullAttend(3); // 預設
            classInfo.setAbsenceCountToGraduate(2); // 預設
            classInfo.setMakeupCountToGraduate(3); // 預設
            classInfo.setAbsenceCountToDiligentAward(2); // 預設
//            classInfo.setMakeupCountToDiligentAward(3); // 預設
            classInfo.setMinutesBeLate(10); // 預設
            classInfo.setClassFullName(""); // 預設
            classInfo.setCertificateName(""); // 預設
//            classInfo.setClassStartTime(""); // 預設
//            classInfo.setClassEndTime(""); // 預設
            classInfo.setClassDesc(""); // 預設
            classInfo.setClassNote(""); // 預設
            classInfo.setCreatorAndUpdater(session);

            classInfoRepo.save(classInfo).getClassId();

            classAttendMarkService.addClassAttendMark(classInfo.getClassId(), classInfo.getClassTypeNum(),
                    classInfo.getClassTypeName(), classInfo.getClassPeriodNum(), classInfo.getClassName(), session);
        }

        return "200;success";
    }

    public boolean checkIsExecuteClassAttendRecord(String memberId) {

        return classEnrollFormRepo.GetIsExecuteClassAttendRecord(memberId).get(0);
    }

    public boolean checkIsExecuteClassGraduateStats(String memberId) {

        return classEnrollFormRepo.GetIsExecuteClassGraduateStats(memberId).get(0);
    }

    public boolean GetIsExecuteClassUpGrade(String memberId) {

        return classEnrollFormRepo.GetIsExecuteClassUpGrade(memberId).get(0);
    }

    // 2018/11/06
    public boolean isMaster(HttpSession session) {
        String userId = (String) session.getAttribute(UserSession.Keys.UserId.getValue());
//        String memberId = (String) session.getAttribute(UserSession.Keys.MemberId.getValue());

        if (userId.startsWith("A") || userId.startsWith("B") || userId.startsWith("a") || userId.startsWith("B")) {
            return true;
        } else {
            return false;
        }
    }

    // 取得課程列表
    public Common011Response getClassList(String year, String classStatus, String unitId) {

        List<Common011Object> classInfoRecordList = classInfoRepo.GetClassListByUnitId(unitId);

        List<Common011Object> items = new ArrayList<>();

        String mYear = year.equals("") ? "0" : "1";
        String mClassStatus = classStatus.equals("") ? "0" : "1";
        String key = mYear + mClassStatus;

        List<Common011Object> classInfoList = new ArrayList<>();
        
        switch (key) {
            case "00":

                for (Common011Object classInfoRecord : classInfoRecordList) {

                    items.add(new Common011Object(
                            classInfoRecord.getYear(),
                            classInfoRecord.getClassStatus(),
                            classInfoRecord.getClassPeriodNum(),
                            classInfoRecord.getClassId(),
                            classInfoRecord.getClassName()
                    ));
                }
                break;
            case "10":

                classInfoList = classInfoRecordList.stream()
                        .filter(s -> s.getYear().equals(year))
                        .collect(Collectors.toList());

                for (Common011Object classInfo : classInfoList) {

                    items.add(new Common011Object(
                            classInfo.getYear(),
                            classInfo.getClassStatus(),
                            classInfo.getClassPeriodNum(),
                            classInfo.getClassId(),
                            classInfo.getClassName()
                    ));
                }
                break;
            case "01":

                classInfoList = classInfoRecordList.stream()
                        .filter(s -> s.getClassStatus().equals(classStatus))
                        .collect(Collectors.toList());

                for (Common011Object classInfo : classInfoList) {

                    items.add(new Common011Object(
                            classInfo.getYear(),
                            classInfo.getClassStatus(),
                            classInfo.getClassPeriodNum(),
                            classInfo.getClassId(),
                            classInfo.getClassName()
                    ));
                }
                break;
            case "11":

                classInfoList = classInfoRecordList.stream()
                        .filter(s -> s.getYear().equals(year) && s.getClassStatus().equals(classStatus))
                        .collect(Collectors.toList());

                for (Common011Object classInfo : classInfoList) {

                    items.add(new Common011Object(
                            classInfo.getYear(),
                            classInfo.getClassStatus(),
                            classInfo.getClassPeriodNum(),
                            classInfo.getClassId(),
                            classInfo.getClassName()

                    ));
                }
                break;
        }

        Common011Response common011Response = new Common011Response(
                200,
                "success",
                items
        );

        return common011Response;
    }

    // makeup
    public List<Unit> getDistinctUnitByYear(String year) {
        return classInfoRepo.GetDistinctUnitByYear(year);
    }

    // makeup
    public List<String> getDistinctStatusByYearUnit(String year, String unitId) {
        return classInfoRepo.GetDistinctStatusByYearUnit(year, unitId);
    }

    // makeup
//    public List<MakeupClassInfo> getClassesByYearUnitStatus(String year, String unitId, String classStatus) {
//
//        List<MakeupClassInfo> makeupClassInfoList = classInfoRepo.GetClassByUnitIdYearStatusClassIdNullable(unitId, year, classStatus, null);
//
//        return processMakeupClassInfo(makeupClassInfoList);
//    }

    // makeup
    public List<MakeupClassInfo> getClassByClassId(String unitId, String year, String classStatus, String classId) {

        List<MakeupClassInfo> makeupClassInfoList = classInfoRepo.GetClassByUnitIdYearStatusClassIdNullable(unitId, year, classStatus, classId);

        if (makeupClassInfoList.size() > 0) {
            return processMakeupClassInfo(makeupClassInfoList);
        } else {
            return new ArrayList<>();
        }
    }

    private List<MakeupClassInfo> processMakeupClassInfo(List<MakeupClassInfo> makeupClassInfoList) {
        // class ids
        List<String> classIds = makeupClassInfoList.stream().map(c -> c.getClassId()).collect(Collectors.toList());

        logger.debug("class count: " + classIds.size());

        // all class dates of classes
        List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetClassDateByClassIdList(classIds);

        logger.debug("all date count:　" + classDateInfoList.size());

        // all makeup records of classes
        List<ClassMakeupRecord> classMakeupRecordList = classMakeupRecordRepo.GetRecordByClassIdList(classIds);

        makeupClassInfoList.forEach(classItem -> {
            List<ClassDateInfo> dates = classDateInfoList.stream().filter(c -> c.getClassId().equals(classItem.getClassId()) && c.getMakeupFileName() != null && !c.getMakeupFileName().isEmpty()).collect(Collectors.toList());

            logger.debug("date count:　" + classDateInfoList.size());

            // file count
            classItem.setClassFileTotalNum(dates.size());

            // file size count
            int classFileSizeTotalNum = 0;

            for (int i = 0; i < dates.size(); i++) {
                classFileSizeTotalNum += dates.get(i).getMakeupFileSizeMB();
            }

            classItem.setClassFileSizeTotalNum(classFileSizeTotalNum);

            long playCount = classMakeupRecordList.stream().filter(c -> c.getClassId().equals(classItem.getClassId())).count();

            // file play count
            classItem.setClassFilePlayTotalNum((int)playCount);
        });

        return makeupClassInfoList;
    }


    // unitId is nullable
    public List<String> getYears(String unitId){
        return classInfoRepo.GetYears(unitId);
    }

    public List<ClassInfo> getClassesByClassIdList(List<String> classIds) {
        return classInfoRepo.GetClassesByClassIdList(classIds);
    }

    // managed classes
    public List<MakeupClassInfo> getClassByClassIdList(String unitId, String year, String classStatus, List<String> classIds) {

        List<MakeupClassInfo> makeupClassInfoList;

        if (classIds != null) {

            makeupClassInfoList = classInfoRepo.GetClassByUnitIdYearStatusClassIdNullable(unitId, Empty2Null(year), Empty2Null(classStatus), classIds);
        }else {

            makeupClassInfoList = classInfoRepo.GetClassByUnitIdYearStatusClassIdNullable(unitId, Empty2Null(year), Empty2Null(classStatus));
        }


        if (makeupClassInfoList.size() > 0) {
            return processMakeupClassInfo(makeupClassInfoList);
        } else {
            return new ArrayList<>();
        }
    }

    // 取得開課中的班別
    public List<ClassGroup001Object> getClassByUnitId(String unitId) {

        return classInfoRepo.GetClassesByUnitId(unitId);
    }
}
