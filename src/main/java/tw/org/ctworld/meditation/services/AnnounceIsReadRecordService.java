package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.models.AnnounceIsReadRecord;
import tw.org.ctworld.meditation.repositories.AnnounceInfoRepo;
import tw.org.ctworld.meditation.repositories.AnnounceIsReadRecordRepo;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service
public class AnnounceIsReadRecordService {

    private static final Logger logger = LoggerFactory.getLogger(AnnounceIsReadRecordService.class);

    @Autowired
    private AnnounceInfoRepo announceInfoRepo;

    @Autowired
    private AnnounceIsReadRecordRepo announceIsReadRecordRepo;

    public void add(HttpSession session, List<String> messages) {

        String userId = (String)session.getAttribute(UserSession.Keys.UserId.getValue());
        String userName = (String)session.getAttribute(UserSession.Keys.UserName.getValue());
        String unitId = (String)session.getAttribute(UserSession.Keys.UnitId.getValue());
        String unitName = (String)session.getAttribute(UserSession.Keys.UnitName.getValue());

        List<AnnounceIsReadRecord> readList = new ArrayList<>();

        for (int x = 0 ; x < messages.size() ; x++) {
            if (announceInfoRepo.HasMessageId(messages.get(x)) && !announceIsReadRecordRepo.IsRead(userId, messages.get(x))) {
                AnnounceIsReadRecord record = new AnnounceIsReadRecord(unitId, unitName, userId, userName, messages.get(x));
                readList.add(record);
            }
        }

        if (readList.size() > 0) {
            announceIsReadRecordRepo.saveAll(readList);
        }
    }
}
