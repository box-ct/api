package tw.org.ctworld.meditation.services;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.DataImport.ExcelMemberEnroll;

import javax.servlet.ServletContext;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.List;

@Service
public class ExcelWriterService {

    private ServletContext context;

    private static final Logger logger = LoggerFactory.getLogger(ExcelWriterService.class);

    @Autowired
    public ExcelWriterService(ServletContext context) {
        this.context = context;
    }

    public void buildImportDataExcel(List<ExcelMemberEnroll> items, String filename) {

        logger.debug("buildImportDataExcel");

        Workbook workbook = new XSSFWorkbook();

        // create excel xls sheet
        Sheet sheet = workbook.createSheet("sheet1");

        // create style for header cells
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.CENTER);

        Font headFont = workbook.createFont();
        headFont.setFontName("Arial");
        headFont.setBold(true);
        headStyle.setFont(headFont);

        int rowCount = 0;
        Row first= sheet.createRow(rowCount);
        Cell titleCell = first.createCell(0);
        titleCell.setCellValue("請填入以下資訊 | 單位：AA　禪修班類別：　　　　時段：　　　期別：　　班別：　　　開課日：　　　結業日：　　　星期：　");
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 10));

        rowCount ++;

        // create header row
        Row header = sheet.createRow(rowCount);
        String[] headStrings = {"姓名 (必填)", "生日 (必填)", "性別 (必填)", "身份證字號", "法名", "組別", "組號", "禪修班職稱", "取消", "結業", "全勤"};

        for (int i = 0; i < headStrings.length; i++) {
            Cell cell = header.createCell(i);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headStrings[i]);
        }

        rowCount++;

        // data rows
        for (ExcelMemberEnroll item : items) {
            Row row = sheet.createRow(rowCount++);
            row.createCell(0).setCellValue(item.getName());
            row.createCell(1).setCellValue(item.getBirthday());
            row.createCell(2).setCellValue(item.getGender());
            row.createCell(3).setCellValue(item.getTwId());
            row.createCell(4).setCellValue(item.getCtName());
            row.createCell(5).setCellValue(item.getGroupNum());
            row.createCell(6).setCellValue(item.getMemberNum());
            row.createCell(7).setCellValue(item.getJobList());
            row.createCell(8).setCellValue(item.getCanceled());
            row.createCell(9).setCellValue(item.getGraduated());
            row.createCell(10).setCellValue(item.getFullyAttended());
        }

        for (int i = 0; i < headStrings.length; i++) {
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, (int)(sheet.getColumnWidth(i) * 1.2));
        }

        saveWorkbook(workbook, filename);

    }

    public void buildNewMemberExcel(List<ExcelMemberEnroll> items, String filename) {

        logger.debug("buildNewMemberExcel");

        Workbook workbook = new XSSFWorkbook();

        // create excel xls sheet
        Sheet sheet = workbook.createSheet("sheet1");

        // create style for header cells
        CellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.CENTER);

        Font headFont = workbook.createFont();
        headFont.setFontName("Arial");
        headFont.setBold(true);
        headStyle.setFont(headFont);

        int rowCount = 0;

        // create header row
        Row header = sheet.createRow(rowCount);
        String[] headStrings = {"姓名", "性別", "出生日期", "身分證字號", "其他證件字號(如:護照號碼)", "住家電話", "行動電話", "法名", "中間名", "別名", "本山發心項目", "中台信眾編號", "資料回山日期", "三皈依", "五戒", "菩薩戒", "學佛經歷", "學位", "Email", "已婚", "出生地(國家)", "出生地(省)", "出生地(縣市)", "公司名稱", "職稱", "公司電話", "介紹人", "與介紹人關係", "介紹人連絡電話", "緊急連絡人", "緊急連絡人法名", "與緊急連絡人關係", "緊急連絡人電話", "學校名稱", "科系", "通訊地址(國家)", "通訊地址(縣市)", "通訊地址(鄉鎮區)", "通訊地址(郵遞區號)", "通訊地址(門牌號碼)"};

        for (int i = 0; i < headStrings.length; i++) {
            Cell cell = header.createCell(i);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headStrings[i]);
        }

        rowCount = 1;
        Row header2 = sheet.createRow(rowCount);
        String[] headStrings2 = {"fullName,姓名", "gender,性別", "birthDate,出生日期", "twIdNum,身分證字號", "otherIdNum,其他證件字號", "homePhoneNum1,住家電話", "mobileNum1,行動電話", "ctDharmaName,法名", "middleName,中間名", "aliasName,別名", "donationNameList,本山發心項目", "dataUnitMemberId,中台信眾編號", "dataSentToCTDate,資料回山日期", "takeRefuge,三皈依", "takePrecept5,五戒", "takeBodhiPrecept,菩薩戒", "classList,學佛經歷", "schoolDegree,學位", "email1,Email", "marriageStatus,已婚", "birthCountry,出生地(國家)", "birthProvinceCountry,出生地(省)", "birthCity,出生地(縣市)", "companyName,公司名稱", "companyJobTitle,職稱", "officePhoneNum1,公司電話", "introducerName,介紹人", "introducerRelationship,與介紹人關係", "introducerPhoneNum,介紹人連絡電話", "urgentContactPersonName1,緊急連絡人", "urgentContactPersonDharmaName1,緊急連絡人法名", "urgentContactPersonRelationship1,與緊急連絡人關係", "urgentContactPersonPhoneNum1,緊急連絡人電話", "schoolName,學校名稱", "schoolMajor,科系", "mailingCountry,通訊地址(國家)", "mailingState,通訊地址(縣市)", "mailingCity,通訊地址(鄉鎮區)", "mailingZip,通訊地址(郵遞區號)", "mailingAddress,通訊地址(門牌號碼)"};

        for (int i = 0; i < headStrings2.length; i++) {
            Cell cell = header2.createCell(i);
            cell.setCellStyle(headStyle);
            cell.setCellValue(headStrings2[i]);
        }

        // skip one row
        rowCount = 2;

        // data rows
        for (ExcelMemberEnroll item : items) {
            Row row = sheet.createRow(rowCount++);
            row.createCell(0).setCellValue(item.getName());
            row.createCell(1).setCellValue(item.getGender().equals("男") ? "M" : "F");
            row.createCell(2).setCellValue(item.getBirthday());
            row.createCell(3).setCellValue(item.getTwId());
        }

        for (int i = 0; i < headStrings.length; i++) {
            sheet.autoSizeColumn(i, true);
            sheet.setColumnWidth(i, (int)(sheet.getColumnWidth(i) * 1.2));
        }

        saveWorkbook(workbook, filename);
    }

    private void saveWorkbook(Workbook workbook, String filename) {
        String absolutePath = context.getRealPath("temp");
        String filePath = Paths.get(absolutePath, filename).toString();

        try {
            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(filePath);
            workbook.write(fileOut);
            fileOut.close();

            // Closing the workbook
            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }


}
