package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.Printer.*;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Service
public class ClassPrinterService {

    private static final Logger logger = LoggerFactory.getLogger(ClassPrinterService.class);

    @Autowired
    private ClassCertInfoRepo classCertInfoRepo;

    @Autowired
    private ClassUnitPrinterRepo classUnitPrinterRepo;

    @Autowired
    private ClassCertFieldSettingRepo classCertFieldSettingRepo;

    @Autowired
    private ClassEnrollFormRepo classEnrollFormRepo;

    @Autowired
    private ClassInfoRepo classInfoRepo;

    @Autowired
    private UnitInfoRepo unitInfoRepo;


    // 取得印表機列表
    public Printer001Response getPrinterList(HttpSession session) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<Printer001Object> mPrinter001ObjectList = new ArrayList<>();

        List<Printer001Object> printer001ObjectList = classUnitPrinterRepo.GetPrinterList(unitId);

        logger.debug("getPrinterList for " + unitId + " count: " + printer001ObjectList.size());

        List<String> printerIdList = printer001ObjectList.stream().map(p -> p.getPrinterId()).collect(Collectors.toList());

        List<Printer001Object> classCertTypeNameList = new ArrayList<>();

        if (printerIdList.size() > 0)
            classCertTypeNameList = classCertInfoRepo.GetClassCertTypeNameListByPrinterIdList(printerIdList);

        for (Printer001Object printer001Object : printer001ObjectList) {
//            logger.debug(printer001Object.getPrinterId());
            List<String> certTypeNames = classCertTypeNameList.stream()
                    .filter(w -> w.getPrinterId().equals(printer001Object.getPrinterId()))
                    .map(w -> w.getCertTypeName())
                    .collect(Collectors.toList());

            mPrinter001ObjectList.add(new Printer001Object(
                    printer001Object.getId(),
                    printer001Object.getPrinterName(),
                    printer001Object.getPrinterId(),
                    printer001Object.getNote(),
                    certTypeNames
            ));
        }

        return new Printer001Response(200, "success", mPrinter001ObjectList);
    }

    // 新增印表機資料
    public BaseResponse addPrinter(HttpSession session, Printer002Request printer002Request) {

        int errCode = 200;
        String errMsg = "success";

        if (printer002Request.getPrinterName().isEmpty()) {

            errCode = 400;
            errMsg = "請輸入印表機名稱!";
        } else {

            if (printer002Request.getSourcePrinterId().isEmpty() && printer002Request.getCertTypeNames().size() == 0) {

                errCode = 400;
                // SourcePrinterId and CertTypeNames invalid
                errMsg = "請至少選擇一個證書樣版產生方式!";
            } else {

                String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
                String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());
                String printerId = printerIdGenerator();
                String printName = printer002Request.getPrinterName();

                List<String> printerNameList = classUnitPrinterRepo.GetPrinterNameListByUnitId(unitId);

                if (printerNameList.contains(printName)) {

                    errCode = 400;
                    errMsg = "印表機名稱重複，請重新輸入印表機名稱!";
                } else {

                    ClassUnitPrinter classUnitPrinter = new ClassUnitPrinter();
                    classUnitPrinter.setUnitId(unitId);
                    classUnitPrinter.setUnitName(unitName);
                    classUnitPrinter.setPrinterId(printerId);
                    classUnitPrinter.setPrinterName(printName);
                    classUnitPrinter.setNote(printer002Request.getNote());
                    classUnitPrinter.setCreatorAndUpdater(session);

                    if (printer002Request.getSourcePrinterId().isEmpty()) {

                        // 建立預設證書樣板

                        logger.debug("add unit printer");

                        classUnitPrinterRepo.save(classUnitPrinter);

                        DecimalFormat df = new DecimalFormat("000000");

                        String criteria = "CTID" + CommonUtils.GetMinGoYearStr();

                        String currentMaxValue = classCertInfoRepo.GetMaxCertTemplateId();

                        long tempId = 0;

                        if (currentMaxValue != null) {

                            currentMaxValue = currentMaxValue.substring(7);

                            tempId = Long.parseLong(currentMaxValue);
                        }

                        List<ClassCertInfo> classCertInfoList = new ArrayList<>();

                        for (String certTypeName : printer002Request.getCertTypeNames()) {
                            logger.debug(certTypeName);

                            tempId++;

                            String certTemplateId = criteria + df.format(tempId);

                            ClassCertInfo classCertInfo = new ClassCertInfo();
                            classCertInfo.setUnitId(unitId);
                            classCertInfo.setUnitName(unitName);
                            classCertInfo.setCertTemplateId(certTemplateId);
                            classCertInfo.setCertTypeName(certTypeName);
                            classCertInfo.setCertTemplateName(certTypeName + "_00");
                            classCertInfo.setPrinterId(printerId);
                            classCertInfo.setPrinterName(printName);
                            classCertInfo.setDescription(null);
                            classCertInfo.setNote("登入單位(" + unitName + ")複製中台預設" + certTypeName + "樣版");
                            classCertInfo.setCreatorAndUpdater(session);

                            classCertInfoList.add(classCertInfo);
                        }

                        classCertInfoRepo.saveAll(classCertInfoList);

                        certGenerator(session, classCertInfoList);

                    } else {

                        // 複製證書樣板
                        logger.debug("copy unit printer");

                        List<ClassCertInfo> classCertInfoList =
                                classCertInfoRepo.GetClassCertInfoListByPrinterID(printer002Request.getSourcePrinterId());

                        if (classCertInfoList.size() == 0) {

                            errCode = 400;
                            errMsg = "印表機" + printer002Request.getSourcePrinterId() + "無對應證書樣板!";
                        } else {

                            classUnitPrinterRepo.save(classUnitPrinter);

                            DecimalFormat df = new DecimalFormat("000000");

                            String criteria = "CTID" + CommonUtils.GetMinGoYearStr();

                            String currentMaxValue = classCertInfoRepo.GetMaxCertTemplateId();

                            long tempId = 0;

                            if (currentMaxValue != null) {

                                currentMaxValue = currentMaxValue.substring(7);

                                tempId = Long.parseLong(currentMaxValue);
                            }

                            List<String> certTemplateIdList = new ArrayList<>();

                            List<CertTemplateIdRecord> certTemplateIdRecordList = new ArrayList<>();

                            List<ClassCertInfo> mClassCertInfoList = new ArrayList<>();

                            for (ClassCertInfo classCertInfo : classCertInfoList) {

                                tempId++;

                                String certTemplateId = criteria + df.format(tempId);

                                ClassCertInfo mClassCertInfo = new ClassCertInfo();
                                mClassCertInfo.setUnitId(unitId);
                                mClassCertInfo.setUnitName(unitName);
                                mClassCertInfo.setCertTemplateId(certTemplateId);
                                mClassCertInfo.setCertTypeName(classCertInfo.getCertTypeName());
                                mClassCertInfo.setCertTemplateName(classCertInfo.getCertTypeName() + "_00");
                                mClassCertInfo.setPrinterId(printerId);
                                mClassCertInfo.setPrinterName(printName);
                                mClassCertInfo.setDescription(null);
                                mClassCertInfo.setNote("登入單位(" + unitName + ")複製" + classCertInfo.getPrinterId() + classCertInfo.getCertTypeName() + "樣版");
                                mClassCertInfo.setCreatorAndUpdater(session);

                                mClassCertInfoList.add(mClassCertInfo);

                                certTemplateIdList.add(classCertInfo.getCertTemplateId());

                                certTemplateIdRecordList.add(new CertTemplateIdRecord(classCertInfo.getCertTemplateId(), certTemplateId));
                            }

                            classCertInfoRepo.saveAll(mClassCertInfoList);

                            List<ClassCertFieldSetting> classCertFieldSettingList =
                                    classCertFieldSettingRepo.GetCertTemplateIdListByCertTemplateId(certTemplateIdList);

                            List<ClassCertFieldSetting> mClassCertFieldSettingList = new ArrayList<>();

                            for (ClassCertFieldSetting classCertFieldSetting : classCertFieldSettingList) {

                                String certTemplateId = certTemplateIdRecordList.stream()
                                        .filter(s -> s.oldCertTemplateId.equals(classCertFieldSetting.getCertTemplateId()))
                                        .collect(Collectors.toList()).get(0).getNewCertTemplateId();

                                ClassCertFieldSetting mClassCertFieldSetting = new ClassCertFieldSetting();
                                mClassCertFieldSetting.setUnitId(unitId);
                                mClassCertFieldSetting.setUnitName(unitName);
                                mClassCertFieldSetting.setCertTypeName(classCertFieldSetting.getCertTypeName());
                                mClassCertFieldSetting.setCertTemplateId(certTemplateId);
                                mClassCertFieldSetting.setCertTemplateName(classCertFieldSetting.getCertTemplateName());
                                mClassCertFieldSetting.setFieldPositionNum(classCertFieldSetting.getFieldPositionNum());
                                mClassCertFieldSetting.setFieldName(classCertFieldSetting.getFieldName());
                                mClassCertFieldSetting.setFontName(classCertFieldSetting.getFontName());
                                mClassCertFieldSetting.setFontSize(classCertFieldSetting.getFontSize());
                                mClassCertFieldSetting.setIsBold(classCertFieldSetting.getIsBold());
                                mClassCertFieldSetting.setIsItalic(classCertFieldSetting.getIsItalic());
                                mClassCertFieldSetting.setHorizontalAlignType(classCertFieldSetting.getHorizontalAlignType());
                                mClassCertFieldSetting.setVerticalAlignType(classCertFieldSetting.getVerticalAlignType());
                                mClassCertFieldSetting.setTextDirection(classCertFieldSetting.getTextDirection());
                                mClassCertFieldSetting.setWidth(classCertFieldSetting.getWidth());
                                mClassCertFieldSetting.setHeight(classCertFieldSetting.getHeight());
                                mClassCertFieldSetting.setyDistance(classCertFieldSetting.getyDistance());
                                mClassCertFieldSetting.setxDistance(classCertFieldSetting.getxDistance());
                                mClassCertFieldSetting.setIsEnabled(classCertFieldSetting.getIsEnabled());
                                mClassCertFieldSetting.setCreatorAndUpdater(session);

                                mClassCertFieldSettingList.add(mClassCertFieldSetting);
                            }

                            classCertFieldSettingRepo.saveAll(mClassCertFieldSettingList);
                        }
                    }
                }
            }
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 修改單筆印表機資料
    public BaseResponse editPrinter(HttpSession session, Printer002Request printer002Request, String printerId) {

        int errCode = 200;
        String errMsg = "success";
        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
        String newPrinterName = printer002Request.getPrinterName();
        String newNote = printer002Request.getNote();

        if (newPrinterName.isEmpty()) {

            errCode = 400;
            errMsg = "請輸入印表機名稱!";
        } else {

            boolean isPass = false;

            List<ClassUnitPrinter> printerNameList =
                    classUnitPrinterRepo.GetClassUnitPrinterListByUnitIdAndPrinterName(unitId, newPrinterName);

            if (printerNameList.size() > 0) {

                List<ClassUnitPrinter> mPrinterNameList = printerNameList.stream()
                        .filter(s -> s.getPrinterId().equals(printerId))
                        .collect(Collectors.toList());

                if (mPrinterNameList.size() > 0) {

                    isPass = true;
                }
            } else {

                isPass = true;
            }

            if (!isPass) {

                errCode = 400;
                errMsg = "印表機名稱重複，請重新輸入印表機名稱!";
            } else {

                ClassUnitPrinter classUnitPrinter = classUnitPrinterRepo.GetClassUnitPrinterByPrinterID(printerId);

                if (classUnitPrinter == null) {

                    errCode = 400;
                    errMsg = "Printer Id : " + printerId + " not found!";
                } else {

                    classUnitPrinter.setPrinterName(newPrinterName);
                    classUnitPrinter.setNote(newNote);
                    classUnitPrinter.setUpdater(session);

                    classUnitPrinterRepo.save(classUnitPrinter);

                    List<ClassCertInfo> mClassCertInfoList = new ArrayList<>();

                    List<ClassCertInfo> classCertInfoList = classCertInfoRepo.GetClassCertInfoListByPrinterID(printerId);

                    for (ClassCertInfo classCertInfo : classCertInfoList) {

                        classCertInfo.setPrinterName(newPrinterName);
                        classCertInfo.setUpdater(session);

                        mClassCertInfoList.add(classCertInfo);
                    }

                    classCertInfoRepo.saveAll(mClassCertInfoList);
                }
            }
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 取得單筆印表機資料
    public Printer004Response getPrinterInfo(String printerId) {

        int errCode = 200;
        String errMsg = "success";
        String id = "";
        String mPrinterId = "";
        String printerName = "";
        String note = "";
        List<String> certTypeNames = new ArrayList<>();

        ClassUnitPrinter classUnitPrinter = classUnitPrinterRepo.GetClassUnitPrinterByPrinterID(printerId);

        if (classUnitPrinter == null) {

            errCode = 400;
            errMsg = "Printer Id : " + printerId + " not found!";
        } else {

            id = String.valueOf(classUnitPrinter.getId());
            mPrinterId = classUnitPrinter.getPrinterId();
            printerName = classUnitPrinter.getPrinterName();
            note = classUnitPrinter.getNote();
            certTypeNames = classCertInfoRepo.GetClassCertTypeNameListByPrinterId(printerId);
        }

        return new Printer004Response(
                errCode,
                errMsg,
                id,
                mPrinterId,
                printerName,
                note,
                certTypeNames
        );
    }

    // 取得印表機, 某個證書的版型列表
    public Printer005Response getCertInfoList(String certTypeName, String printerId) {

        int errCode = 200;
        String errMsg = "success";
        List<Printer005Object> items = new ArrayList<>();

        List<Printer005Object> objectList =
                classCertInfoRepo.GetClassClassCertInfoListByPrinterIDAndCertTypeName(printerId, certTypeName);

        if (objectList.size() == 0) {

            errCode = 400;
            errMsg = "PrinterId and CertTypeName invalid";
        } else {

            for (Printer005Object object : objectList) {

                items.add(new Printer005Object(
                        object.getId(),
                        object.getCertTemplateId(),
                        object.getCertTypeName(),
                        object.getCertTemplateName(),
                        object.getDescription(),
                        "示意圖位置未完成"
                ));
            }
        }

        return new Printer005Response(errCode, errMsg, items);
    }

    // 新增版型
    public BaseResponse addCertType(HttpSession session, Printer006Request printer006Request) {

        int errCode = 200;
        String errMsg = "success";
        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
        String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());
        String printerId = printer006Request.getPrinterId();
        String printName = classUnitPrinterRepo.GetPrinterNameByPrinterId(printerId);
        String certTypeName = printer006Request.getCertTypeName();
        String certTemplateName = printer006Request.getCertTemplateName();
        String description = printer006Request.getDescription().isEmpty() ? null : printer006Request.getDescription();

        if (certTemplateName.isEmpty()) {

            errCode = 400;
            errMsg = "樣版名稱為必填欄位，請填寫後，才能建立新樣版!";
        } else {

            List<String> certTempNameList = classCertInfoRepo.GetClassCertTypeLastNameListByPrinterId(printerId);

            if (certTempNameList.contains(certTemplateName)) {

                errCode = 400;
                errMsg = "樣版名稱重複，請修改樣版名稱";
            } else {

                DecimalFormat df = new DecimalFormat("000000");

                String criteria = "CTID" + CommonUtils.GetMinGoYearStr();

                String currentMaxValue = classCertInfoRepo.GetMaxCertTemplateId();

                long tempId = 0;

                if (currentMaxValue != null) {

                    currentMaxValue = currentMaxValue.substring(7);

                    tempId = Long.parseLong(currentMaxValue);
                }

                tempId++;

                String certTemplateId = criteria + df.format(tempId);

                ClassCertInfo classCertInfo = new ClassCertInfo();
                classCertInfo.setUnitId(unitId);
                classCertInfo.setUnitName(unitName);
                classCertInfo.setCertTemplateId(certTemplateId);
                classCertInfo.setCertTypeName(certTypeName);
                classCertInfo.setCertTemplateName(certTemplateName);
                classCertInfo.setPrinterId(printerId);
                classCertInfo.setPrinterName(printName);
                classCertInfo.setDescription(description);
                classCertInfo.setNote("由中台樣板class_cert_info複製");
                classCertInfo.setCreatorAndUpdater(session);

                classCertInfoRepo.save(classCertInfo);

                List<ClassCertInfo> classCertInfoList = new ArrayList<>();
                classCertInfoList.add(classCertInfo);

                certGenerator(session, classCertInfoList);
            }
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 取得單筆板型資料
    public Printer007Response getCertFieldSetting(String certTemplateId) {

        int errCode = 200;
        String errMsg = "success";
        String printerId = "";
        String certTypeName = "";
        String certTemplateName = "";
        String description = "";
        List<Printer007Object> settings = new ArrayList<>();

        ClassCertInfo classCertInfo = classCertInfoRepo.GetClassCertInfoByCertTemplateId(certTemplateId);

        if (classCertInfo == null) {

            errCode = 400;
            errMsg = "CertTemplateId : " + certTemplateId + " invalid";
        } else {

            printerId = classCertInfo.getPrinterId();
            certTypeName = classCertInfo.getCertTypeName();
            certTemplateName = classCertInfo.getCertTemplateName();
            description = classCertInfo.getDescription();
            settings = classCertFieldSettingRepo.GetCertTemplateIdListByCertTemplateId2(certTemplateId);
        }

        return new Printer007Response(
                errCode,
                errMsg,
                printerId,
                certTypeName,
                certTemplateName,
                description,
                settings
        );
    }

    // 刪除樣版
    public BaseResponse deleteCertFieldSetting(String certTemplateId) {

        int errCode = 200;
        String errMsg = "success";

        ClassCertInfo classCertInfo = classCertInfoRepo.GetClassCertInfoByCertTemplateId(certTemplateId);

        if (classCertInfo == null) {

            errCode = 400;
            errMsg = "CertTemplateId : " + certTemplateId + " invalid";
        } else {

            classCertInfoRepo.delete(classCertInfo);

            List<String> certTemplateIdList = new ArrayList<>();
            certTemplateIdList.add(certTemplateId);

            List<ClassCertFieldSetting> classCertFieldSettingList =
                    classCertFieldSettingRepo.GetCertTemplateIdListByCertTemplateId(certTemplateIdList);

            classCertFieldSettingRepo.deleteAll(classCertFieldSettingList);
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 更新板型
    public BaseResponse editCertFieldSetting(HttpSession session, String certTemplateId, Printer009Request printer009Request) {

        int errCode = 200;
        String errMsg = "success";
        String certTemplateName = printer009Request.getCertTemplateName();
        String description = printer009Request.getDescription().isEmpty() ? null : printer009Request.getDescription();

        ClassCertInfo classCertInfo = classCertInfoRepo.GetClassCertInfoByCertTemplateId(certTemplateId);

        if (classCertInfo == null) {

            errCode = 400;
            errMsg = "CertTemplateId : " + certTemplateId + " invalid";
        } else {

            classCertInfo.setCertTemplateName(certTemplateName);
            classCertInfo.setDescription(description);
            classCertInfo.setUpdater(session);

            classCertInfoRepo.save(classCertInfo);

            List<String> certTemplateIdList = new ArrayList<>();
            certTemplateIdList.add(certTemplateId);

            List<ClassCertFieldSetting> classCertFieldSettingList =
                    classCertFieldSettingRepo.GetCertTemplateIdListByCertTemplateId(certTemplateIdList);

            for (ClassCertFieldSetting classCertFieldSetting : classCertFieldSettingList) {

                classCertFieldSetting.setCertTemplateName(certTemplateName);
                classCertFieldSetting.setUpdater(session);
            }

            for (Printer009Object setting : printer009Request.getSettings()) {

                ClassCertFieldSetting classCertFieldSetting =
                        classCertFieldSettingList.stream()
                                .filter(s -> s.getId() == setting.getId()).collect(Collectors.toList()).get(0);

                classCertFieldSetting.setFontSize(setting.getFontSize());
                classCertFieldSetting.setIsBold(setting.getIsBold());
                classCertFieldSetting.setIsItalic(setting.getIsItalic());
                classCertFieldSetting.setHorizontalAlignType(
                        setting.getHorizontalAlignType().isEmpty() ? null : setting.getHorizontalAlignType());
                classCertFieldSetting.setVerticalAlignType(
                        setting.getVerticalAlignType().isEmpty() ? null : setting.getVerticalAlignType());
                classCertFieldSetting.setTextDirection(setting.getTextDirection());
                classCertFieldSetting.setWidth(setting.getWidth());
                classCertFieldSetting.setHeight(setting.getHeight());
                classCertFieldSetting.setyDistance(setting.getyDistance());
                classCertFieldSetting.setxDistance(setting.getxDistance());
                classCertFieldSetting.setIsEnabled(setting.getIsEnabled());
            }

            classCertFieldSettingRepo.saveAll(classCertFieldSettingList);
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 下載列印檔
    public Printer010Response exportCert(HttpSession session, String certTemplateId,
                                         Printer010Request printer010Request) {

        int errCode = 200;
        String errMsg = "success";
        String userName = (String) session.getAttribute(UserSession.Keys.UserName.getValue());
        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
        String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());
        String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());
        List<Printer007Object> classCertFieldSettingList = new ArrayList<>();
        List<Printer011Object> memberNameAndAgeList = new ArrayList<>();
        String certContent = "";
        String certMaster = "";
        String certDate = "";
        String certFileName = "";
        String certTypeName = "";


        if (printer010Request.getClassEnrollFormIds().size() == 0) {

            errCode = 400;
            errMsg = "ClassEnrollFormIds is empty";
        } else {

            classCertFieldSettingList =
                    classCertFieldSettingRepo.GetCertTemplateIdListByCertTemplateId2(certTemplateId);

            List<ClassEnrollForm> classEnrollFormList = classEnrollFormRepo
                    .GetClassEnrollFormListByClassEnrollFormIdList(printer010Request.getClassEnrollFormIds());

            certTypeName = classCertFieldSettingList.get(0).getCertTypeName();
            Boolean isGraduated = certTypeName.contains("結業");
            Date date = new Date();

            for (ClassEnrollForm classEnrollForm : classEnrollFormList) {

                if (isGraduated) {

                    classEnrollForm.setIsGraduatedPrinted(true);
                    classEnrollForm.setIsGraduatedCertPrintedDtTm(date);
                } else {

                    classEnrollForm.setIsFullAttendedPrinted(true);
                    classEnrollForm.setIsFullAttendedCertPrintedDtTm(date);
                }
                classEnrollForm.setCertPrintUserName(userName);
                classEnrollForm.setUpdater(session);
            }

            classEnrollFormRepo.saveAll(classEnrollFormList);

            memberNameAndAgeList = memberNameAndAgeGenerator(classEnrollFormList);

            UnitInfo unitInfo = unitInfoRepo.GetCertUnitNameByUnitId(unitId);

            SimpleDateFormat dateFormatInput = new SimpleDateFormat("yyyy-MM-dd");
            dateFormatInput.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));
            String[] localDate = dateFormatInput.format(new Date()).split("-");


            if (certTypeName.equals("中文結業證書") || certTypeName.equals("中文全勤證書")) {

                certContent = classDetailGenerate(classEnrollFormList.get(0).getClassId(), 1);

                if (unitName.contains("寺") || unitName.contains("精舍")) {

                    certMaster = unitName + "住持;" + unitInfo.getAbbotName();
                } else {

                    certMaster = unitName + "精舍住持;" + unitInfo.getAbbotName();
                }

                String year = String.valueOf(Integer.parseInt(localDate[0]) - 1911);
                year = numberToChineseGenerate(year.substring(0, 1)) + "百" +
                        (year.substring(1, 2).equals("0") ? numberToChineseGenerate(year.substring(1, 2)) : numberToChineseGenerate(year.substring(1, 2)).equals("0") + "十") +
                        numberToChineseGenerate(year.substring(2, 3).equals("0") ? "" : year.substring(2, 3));

                String month = String.valueOf(Integer.parseInt(localDate[1]));
                switch (month.length()) {
                    case 1:

                        month = numberToChineseGenerate(month);
                        break;
                    case 2:
                        month = numberToChineseGenerate(month.substring(0, 1)) + "十" +
                                numberToChineseGenerate(month.substring(1, 2).equals("0") ? "" : month.substring(1, 2));
                        break;
                }

                String day = String.valueOf(Integer.parseInt(localDate[2]));
                switch (day.length()) {
                    case 1:

                        day = numberToChineseGenerate(day);
                        break;
                    case 2:
                        day = numberToChineseGenerate(day.substring(0, 1)) + "十" +
                                numberToChineseGenerate(day.substring(1, 2).equals("0") ? "" : day.substring(1, 2));
                        break;
                }

                certDate = year + ";" + month + ";" + day;

                certFileName = certContent.split(";")[0] + "_" + certContent.split(";")[1] + "_中文證書套印_" +
                        userName + "_" + CommonUtils.GetCurrentTimeString() + ".doc";

            }

            if (certTypeName.equals("英文結業證書") || certTypeName.equals("英文全勤證書")) {

                certContent = classDetailGenerate(classEnrollFormList.get(0).getClassId(), 2) +
                        unitInfo.getCertUnitEngName() + ";" + unitInfo.getStateName();

                certMaster = unitInfo.getAbbotEngName() + ";Abbot of " + unitInfo.getCertUnitEngName();

                certFileName = certContent.split(";")[0] + "_英文證書套印_" +
                        userName + "_" + CommonUtils.GetCurrentTimeString() + ".doc";
            }

            if (certTypeName.equals("日文結業證書") || certTypeName.equals("日文全勤證書")) {

                certContent = classDetailGenerate(classEnrollFormList.get(0).getClassId(), 4);

                if (unitName.contains("寺") || unitName.contains("精舍")) {

                    certMaster = unitName + "住持;" + unitInfo.getAbbotName();
                } else {

                    certMaster = unitName + "精舍住持;" + unitInfo.getAbbotName();
                }

                String year = String.valueOf(Integer.parseInt(localDate[0]) - 1911);
                String month = String.valueOf(Integer.parseInt(localDate[1]));
                String day = String.valueOf(Integer.parseInt(localDate[2]));
                certDate = year + ";" + month + ";" + day;

                certFileName = certContent.split(";")[0] + "_日文證書套印_" +
                        userName + "_" + CommonUtils.GetCurrentTimeString() + ".doc";
            }

            if (certTypeName.equals("中英文結業證書") || certTypeName.equals("中英文全勤證書")) {

                if (printer010Request.getPrintType().equals("中文")) {

                    certContent = classDetailGenerate(classEnrollFormList.get(0).getClassId(), 3);

                    if (unitName.contains("寺") || unitName.contains("精舍")) {

                        certMaster = unitName + ";住持;" + unitInfo.getAbbotName();
                    } else {

                        certMaster = unitName + "精舍;住持;" + unitInfo.getAbbotName();
                    }
                } else {

                    certContent = classDetailGenerate(classEnrollFormList.get(0).getClassId(), 2) +
                            unitInfo.getCertUnitEngName() + ";" + unitInfo.getStateName();

                    certMaster = unitInfo.getAbbotEngName() + ";Abbot of " + unitInfo.getCertUnitEngName();
                }

                String year = String.valueOf(Integer.parseInt(localDate[0]) - 1911);
                String month = String.valueOf(Integer.parseInt(localDate[1]));
                String day = String.valueOf(Integer.parseInt(localDate[2]));
                certDate = year + ";" + month + ";" + day;

                certFileName = certContent.split(";")[0] + "_中英文證書套印_" +
                        userName + "_" + CommonUtils.GetCurrentTimeString() + ".doc";
            }

//            print2JSON("classCertFieldSettingList", classCertFieldSettingList);
//            print2JSON("memberNameAndAgeList", memberNameAndAgeList);
//            logger.error("Cert Content : " + certContent);
//            logger.error("Cert Master : " + certMaster);
//            logger.error("Cert Date : " + certDate);
//            logger.error("Cert File Name : " + certFileName);
//            logger.error("Cert Type Name : " + certTypeName + "-" + printer010Request.getPrintType());
        }

        return new Printer010Response(
                errCode,
                errMsg,
                classCertFieldSettingList,
                memberNameAndAgeList,
                certContent,
                certMaster,
                certDate,
                certFileName,
                certTypeName);
    }

    public class CertTemplateIdRecord {

        String oldCertTemplateId, newCertTemplateId;

        public CertTemplateIdRecord(String oldCertTemplateId, String newCertTemplateId) {
            this.oldCertTemplateId = oldCertTemplateId;
            this.newCertTemplateId = newCertTemplateId;
        }

        public String getOldCertTemplateId() {
            return oldCertTemplateId;
        }

        public void setOldCertTemplateId(String oldCertTemplateId) {
            this.oldCertTemplateId = oldCertTemplateId;
        }

        public String getNewCertTemplateId() {
            return newCertTemplateId;
        }

        public void setNewCertTemplateId(String newCertTemplateId) {
            this.newCertTemplateId = newCertTemplateId;
        }
    }

    // generate printer id
    public String printerIdGenerator() {

        DecimalFormat df = new DecimalFormat("000000");

        String currentMaxValue = classUnitPrinterRepo.GetMaxPrinterId();

//        logger.debug("current max: " + currentMaxValue);

        long tempId = 0;

        if (currentMaxValue != null) {

            currentMaxValue = currentMaxValue.substring(2);

            tempId = Long.parseLong(currentMaxValue);
        }

        tempId++;

        return "PT" + df.format(tempId);
    }

    // generate default cert
    public void certGenerator(HttpSession session, List<ClassCertInfo> classCertInfoList) {

        List<ClassCertFieldSetting> classCertFieldSettingList = new ArrayList<>();

        for (ClassCertInfo classCertInfo : classCertInfoList) {

            String unitId = classCertInfo.getUnitId();
            String unitName = classCertInfo.getUnitName();
            String certTypeName = classCertInfo.getCertTypeName();
            String certTemplateId = classCertInfo.getCertTemplateId();
            String certTemplateName = classCertInfo.getCertTemplateName();

            ClassCertFieldSetting classCertFieldSetting;

            switch (classCertInfo.getCertTypeName()) {
                case "中文結業證書":

                    // 1 學員姓名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("1");
                    classCertFieldSetting.setFieldName("學員姓名");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(22);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(58);
                    classCertFieldSetting.setHeight(160);
                    classCertFieldSetting.setyDistance(180);
                    classCertFieldSetting.setxDistance(509);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 2 年齡
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("2");
                    classCertFieldSetting.setFieldName("年齡");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(14);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(42);
                    classCertFieldSetting.setHeight(90);
                    classCertFieldSetting.setyDistance(356);
                    classCertFieldSetting.setxDistance(516);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 3-1 課程名稱及內容
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("3-1");
                    classCertFieldSetting.setFieldName("課程名稱及內容");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(44);
                    classCertFieldSetting.setHeight(365);
                    classCertFieldSetting.setyDistance(202);
                    classCertFieldSetting.setxDistance(472);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 3-2 課程名稱及內容
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("3-2");
                    classCertFieldSetting.setFieldName("課程名稱及內容");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(44);
                    classCertFieldSetting.setHeight(365);
                    classCertFieldSetting.setyDistance(202);
                    classCertFieldSetting.setxDistance(452);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 3-3 課程名稱及內容
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("3-3");
                    classCertFieldSetting.setFieldName("課程名稱及內容");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(44);
                    classCertFieldSetting.setHeight(365);
                    classCertFieldSetting.setyDistance(202);
                    classCertFieldSetting.setxDistance(432);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 4 精舍住持
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("4");
                    classCertFieldSetting.setFieldName("精舍住持");
                    classCertFieldSetting.setFontName("華康魏碑體");
                    classCertFieldSetting.setFontSize(20);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(52);
                    classCertFieldSetting.setHeight(180);
                    classCertFieldSetting.setyDistance(157);
                    classCertFieldSetting.setxDistance(196);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 5 精舍住持法名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("5");
                    classCertFieldSetting.setFieldName("精舍住持法名");
                    classCertFieldSetting.setFontName("超世紀細毛楷");
                    classCertFieldSetting.setFontSize(42);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(80);
                    classCertFieldSetting.setHeight(205);
                    classCertFieldSetting.setyDistance(301);
                    classCertFieldSetting.setxDistance(190);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 6 年
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("6");
                    classCertFieldSetting.setFieldName("年");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(12);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(39);
                    classCertFieldSetting.setHeight(100);
                    classCertFieldSetting.setyDistance(196);
                    classCertFieldSetting.setxDistance(53);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 7 月
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("7");
                    classCertFieldSetting.setFieldName("月");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(14);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(40);
                    classCertFieldSetting.setHeight(60);
                    classCertFieldSetting.setyDistance(291);
                    classCertFieldSetting.setxDistance(53);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 8 日
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("8");
                    classCertFieldSetting.setFieldName("日");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(14);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(40);
                    classCertFieldSetting.setHeight(80);
                    classCertFieldSetting.setyDistance(365);
                    classCertFieldSetting.setxDistance(53);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);
                    break;
                case "中文全勤證書":

                    // 1 學員姓名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("1");
                    classCertFieldSetting.setFieldName("學員姓名");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(22);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(56);
                    classCertFieldSetting.setHeight(160);
                    classCertFieldSetting.setyDistance(215);
                    classCertFieldSetting.setxDistance(536);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 2-1 課程名稱及內容
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("2-1");
                    classCertFieldSetting.setFieldName("課程名稱及內容");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(44);
                    classCertFieldSetting.setHeight(365);
                    classCertFieldSetting.setyDistance(190);
                    classCertFieldSetting.setxDistance(504);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 2-2 課程名稱及內容
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("2-2");
                    classCertFieldSetting.setFieldName("課程名稱及內容");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(44);
                    classCertFieldSetting.setHeight(365);
                    classCertFieldSetting.setyDistance(190);
                    classCertFieldSetting.setxDistance(484);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 2-3 課程名稱及內容
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("2-3");
                    classCertFieldSetting.setFieldName("課程名稱及內容");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(44);
                    classCertFieldSetting.setHeight(365);
                    classCertFieldSetting.setyDistance(190);
                    classCertFieldSetting.setxDistance(464);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 3 精舍住持
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("3");
                    classCertFieldSetting.setFieldName("精舍住持");
                    classCertFieldSetting.setFontName("華康魏碑體");
                    classCertFieldSetting.setFontSize(20);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(52);
                    classCertFieldSetting.setHeight(180);
                    classCertFieldSetting.setyDistance(143);
                    classCertFieldSetting.setxDistance(193);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 4 精舍住持法名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("4");
                    classCertFieldSetting.setFieldName("精舍住持法名");
                    classCertFieldSetting.setFontName("超世紀細毛楷");
                    classCertFieldSetting.setFontSize(42);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(80);
                    classCertFieldSetting.setHeight(205);
                    classCertFieldSetting.setyDistance(286);
                    classCertFieldSetting.setxDistance(187);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 5 年
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("5");
                    classCertFieldSetting.setFieldName("年");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(12);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(39);
                    classCertFieldSetting.setHeight(100);
                    classCertFieldSetting.setyDistance(192);
                    classCertFieldSetting.setxDistance(56);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 6 月
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("6");
                    classCertFieldSetting.setFieldName("月");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(14);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(40);
                    classCertFieldSetting.setHeight(60);
                    classCertFieldSetting.setyDistance(286);
                    classCertFieldSetting.setxDistance(57);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 7 日
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("7");
                    classCertFieldSetting.setFieldName("日");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(14);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType(null);
                    classCertFieldSetting.setVerticalAlignType("文字等分");
                    classCertFieldSetting.setTextDirection("垂直");
                    classCertFieldSetting.setWidth(40);
                    classCertFieldSetting.setHeight(80);
                    classCertFieldSetting.setyDistance(360);
                    classCertFieldSetting.setxDistance(57);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    break;
                case "英文結業證書":

                    // 1 學員姓名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("1");
                    classCertFieldSetting.setFieldName("學員姓名");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(320);
                    classCertFieldSetting.setHeight(43);
                    classCertFieldSetting.setyDistance(216);
                    classCertFieldSetting.setxDistance(116);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 2 課程名稱
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("2");
                    classCertFieldSetting.setFieldName("課程名稱");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(400);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(302);
                    classCertFieldSetting.setxDistance(85);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 3 精舍名稱
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("3");
                    classCertFieldSetting.setFieldName("精舍名稱(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(320);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(330);
                    classCertFieldSetting.setxDistance(25);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 4 州名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("4");
                    classCertFieldSetting.setFieldName("州名(外國)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(250);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(330);
                    classCertFieldSetting.setxDistance(275);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 5 精舍住持
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("5");
                    classCertFieldSetting.setFieldName("精舍住持");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(22);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(true);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(200);
                    classCertFieldSetting.setHeight(46);
                    classCertFieldSetting.setyDistance(400);
                    classCertFieldSetting.setxDistance(0);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 6 精舍住持法名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("6");
                    classCertFieldSetting.setFieldName("精舍住持法名");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(12);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(350);
                    classCertFieldSetting.setHeight(35);
                    classCertFieldSetting.setyDistance(408);
                    classCertFieldSetting.setxDistance(168);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);
                    break;
                case "英文全勤證書":

                    // 1 學員姓名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("1");
                    classCertFieldSetting.setFieldName("學員姓名");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(320);
                    classCertFieldSetting.setHeight(43);
                    classCertFieldSetting.setyDistance(216);
                    classCertFieldSetting.setxDistance(116);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 2 課程名稱
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("2");
                    classCertFieldSetting.setFieldName("課程名稱");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(400);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(302);
                    classCertFieldSetting.setxDistance(85);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 3 精舍名稱
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("3");
                    classCertFieldSetting.setFieldName("精舍名稱(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(320);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(330);
                    classCertFieldSetting.setxDistance(25);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 4 州名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("4");
                    classCertFieldSetting.setFieldName("州名(外國)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(250);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(330);
                    classCertFieldSetting.setxDistance(275);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 5 精舍住持
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("5");
                    classCertFieldSetting.setFieldName("精舍住持");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(22);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(true);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(200);
                    classCertFieldSetting.setHeight(46);
                    classCertFieldSetting.setyDistance(400);
                    classCertFieldSetting.setxDistance(0);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 6 精舍住持法名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("6");
                    classCertFieldSetting.setFieldName("精舍住持法名");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(12);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(350);
                    classCertFieldSetting.setHeight(35);
                    classCertFieldSetting.setyDistance(408);
                    classCertFieldSetting.setxDistance(168);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);
                    break;
                case "日文結業證書":

                    // 1 學員姓名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("1");
                    classCertFieldSetting.setFieldName("學員姓名(日文)");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(320);
                    classCertFieldSetting.setHeight(43);
                    classCertFieldSetting.setyDistance(154);
                    classCertFieldSetting.setxDistance(115);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 2 課程名稱
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("2");
                    classCertFieldSetting.setFieldName("課程名稱(日文)");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(250);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(222);
                    classCertFieldSetting.setxDistance(255);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 3 精舍住持
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("3");
                    classCertFieldSetting.setFieldName("精舍住持");
                    classCertFieldSetting.setFontName("華康魏碑體");
                    classCertFieldSetting.setFontSize(14);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("文字等分");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(175);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(426);
                    classCertFieldSetting.setxDistance(196);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 4 精舍住持法名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("4");
                    classCertFieldSetting.setFieldName("精舍住持法名");
                    classCertFieldSetting.setFontName("超世紀細毛楷");
                    classCertFieldSetting.setFontSize(40);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(180);
                    classCertFieldSetting.setHeight(80);
                    classCertFieldSetting.setyDistance(410);
                    classCertFieldSetting.setxDistance(326);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 5 年
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("5");
                    classCertFieldSetting.setFieldName("年");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(68);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(625);
                    classCertFieldSetting.setxDistance(145);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 6 月
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("6");
                    classCertFieldSetting.setFieldName("月");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(60);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(625);
                    classCertFieldSetting.setxDistance(285);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 7 日
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("7");
                    classCertFieldSetting.setFieldName("日");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(60);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(625);
                    classCertFieldSetting.setxDistance(395);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);
                    break;
                case "日文全勤證書":

                    // 1 學員姓名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("1");
                    classCertFieldSetting.setFieldName("學員姓名(日文)");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(320);
                    classCertFieldSetting.setHeight(43);
                    classCertFieldSetting.setyDistance(154);
                    classCertFieldSetting.setxDistance(115);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 2 課程名稱
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("2");
                    classCertFieldSetting.setFieldName("課程名稱(日文)");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(210);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(201);
                    classCertFieldSetting.setxDistance(275);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 3 精舍住持
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("3");
                    classCertFieldSetting.setFieldName("精舍住持");
                    classCertFieldSetting.setFontName("華康魏碑體");
                    classCertFieldSetting.setFontSize(14);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("文字等分");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(175);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(426);
                    classCertFieldSetting.setxDistance(196);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 4 精舍住持法名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("4");
                    classCertFieldSetting.setFieldName("精舍住持法名");
                    classCertFieldSetting.setFontName("超世紀細毛楷");
                    classCertFieldSetting.setFontSize(40);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(180);
                    classCertFieldSetting.setHeight(80);
                    classCertFieldSetting.setyDistance(410);
                    classCertFieldSetting.setxDistance(326);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 5 年
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("5");
                    classCertFieldSetting.setFieldName("年");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(68);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(625);
                    classCertFieldSetting.setxDistance(145);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 6 月
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("6");
                    classCertFieldSetting.setFieldName("月");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(60);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(625);
                    classCertFieldSetting.setxDistance(285);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 7 日
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("7");
                    classCertFieldSetting.setFieldName("日");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(60);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(625);
                    classCertFieldSetting.setxDistance(395);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);
                    break;
                case "中英文結業證書":

                    // 1 學員姓名(英文)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("1");
                    classCertFieldSetting.setFieldName("學員姓名(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(320);
                    classCertFieldSetting.setHeight(43);
                    classCertFieldSetting.setyDistance(235);
                    classCertFieldSetting.setxDistance(147);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 2 課程名稱(英文)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("2");
                    classCertFieldSetting.setFieldName("課程名稱(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(400);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(293);
                    classCertFieldSetting.setxDistance(115);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 3 精舍名稱(英文)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("3");
                    classCertFieldSetting.setFieldName("精舍名稱(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(337);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(321);
                    classCertFieldSetting.setxDistance(39);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 4 州名(外國)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("4");
                    classCertFieldSetting.setFieldName("州名(外國)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(267);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(321);
                    classCertFieldSetting.setxDistance(304);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 5 精舍住持法名(英文)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("5");
                    classCertFieldSetting.setFieldName("精舍住持法名(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(22);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(true);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(220);
                    classCertFieldSetting.setHeight(46);
                    classCertFieldSetting.setyDistance(378);
                    classCertFieldSetting.setxDistance(13);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 6 精舍住持(英文)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("6");
                    classCertFieldSetting.setFieldName("精舍住持(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(12);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(350);
                    classCertFieldSetting.setHeight(35);
                    classCertFieldSetting.setyDistance(387);
                    classCertFieldSetting.setxDistance(182);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 7 學員名稱
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("7");
                    classCertFieldSetting.setFieldName("學員名稱");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(20);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(210);
                    classCertFieldSetting.setHeight(45);
                    classCertFieldSetting.setyDistance(486);
                    classCertFieldSetting.setxDistance(185);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 8 課程名稱
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("8");
                    classCertFieldSetting.setFieldName("學員名稱");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(20);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(210);
                    classCertFieldSetting.setHeight(45);
                    classCertFieldSetting.setyDistance(517);
                    classCertFieldSetting.setxDistance(183);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 9-1 精舍住持
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("9-1");
                    classCertFieldSetting.setFieldName("精舍住持");
                    classCertFieldSetting.setFontName("華康魏碑體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("文字等分");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(116);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(645);
                    classCertFieldSetting.setxDistance(317);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 9-2 精舍住持
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("9-2");
                    classCertFieldSetting.setFieldName("精舍住持");
                    classCertFieldSetting.setFontName("華康魏碑體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("文字等分");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(116);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(665);
                    classCertFieldSetting.setxDistance(317);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 10 精舍住持法名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("10");
                    classCertFieldSetting.setFieldName("精舍住持法名");
                    classCertFieldSetting.setFontName("超世紀細毛楷");
                    classCertFieldSetting.setFontSize(44);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(200);
                    classCertFieldSetting.setHeight(80);
                    classCertFieldSetting.setyDistance(640);
                    classCertFieldSetting.setxDistance(392);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 11 年
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("11");
                    classCertFieldSetting.setFieldName("年");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(68);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(709);
                    classCertFieldSetting.setxDistance(160);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 12 月
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("12");
                    classCertFieldSetting.setFieldName("月");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(60);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(709);
                    classCertFieldSetting.setxDistance(320);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 13 日
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("13");
                    classCertFieldSetting.setFieldName("日");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(60);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(709);
                    classCertFieldSetting.setxDistance(430);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);
                    break;
                case "中英文全勤證書":

                    // 1 學員姓名(英文)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("1");
                    classCertFieldSetting.setFieldName("學員姓名(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(320);
                    classCertFieldSetting.setHeight(43);
                    classCertFieldSetting.setyDistance(235);
                    classCertFieldSetting.setxDistance(147);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 2 課程名稱(英文)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("2");
                    classCertFieldSetting.setFieldName("課程名稱(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(400);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(293);
                    classCertFieldSetting.setxDistance(115);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 3 精舍名稱(英文)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("3");
                    classCertFieldSetting.setFieldName("精舍名稱(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(363);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(321);
                    classCertFieldSetting.setxDistance(11);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 4 州名(外國)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("4");
                    classCertFieldSetting.setFieldName("州名(外國)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(16);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(295);
                    classCertFieldSetting.setHeight(38);
                    classCertFieldSetting.setyDistance(321);
                    classCertFieldSetting.setxDistance(298);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 5 精舍住持法名(英文)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("5");
                    classCertFieldSetting.setFieldName("精舍住持法名(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(22);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(true);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(220);
                    classCertFieldSetting.setHeight(46);
                    classCertFieldSetting.setyDistance(376);
                    classCertFieldSetting.setxDistance(-12);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 6 精舍住持(英文)
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("6");
                    classCertFieldSetting.setFieldName("精舍住持(英文)");
                    classCertFieldSetting.setFontName("Times New Roman");
                    classCertFieldSetting.setFontSize(12);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(350);
                    classCertFieldSetting.setHeight(35);
                    classCertFieldSetting.setyDistance(385);
                    classCertFieldSetting.setxDistance(156);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 7 學員名稱
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("7");
                    classCertFieldSetting.setFieldName("學員名稱");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(20);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(210);
                    classCertFieldSetting.setHeight(45);
                    classCertFieldSetting.setyDistance(481);
                    classCertFieldSetting.setxDistance(180);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 8 課程名稱
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("8");
                    classCertFieldSetting.setFieldName("學員名稱");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(20);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(180);
                    classCertFieldSetting.setHeight(45);
                    classCertFieldSetting.setyDistance(518);
                    classCertFieldSetting.setxDistance(123);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 9-1 精舍住持
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("9-1");
                    classCertFieldSetting.setFieldName("精舍住持");
                    classCertFieldSetting.setFontName("華康魏碑體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("文字等分");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(116);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(645);
                    classCertFieldSetting.setxDistance(317);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 9-2 精舍住持
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("9-2");
                    classCertFieldSetting.setFieldName("精舍住持");
                    classCertFieldSetting.setFontName("華康魏碑體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("文字等分");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(116);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(665);
                    classCertFieldSetting.setxDistance(317);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 10 精舍住持法名
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("10");
                    classCertFieldSetting.setFieldName("精舍住持法名");
                    classCertFieldSetting.setFontName("超世紀細毛楷");
                    classCertFieldSetting.setFontSize(44);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置左");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(200);
                    classCertFieldSetting.setHeight(80);
                    classCertFieldSetting.setyDistance(640);
                    classCertFieldSetting.setxDistance(392);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 11 年
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("11");
                    classCertFieldSetting.setFieldName("年");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(68);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(709);
                    classCertFieldSetting.setxDistance(160);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 12 月
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("12");
                    classCertFieldSetting.setFieldName("月");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(60);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(709);
                    classCertFieldSetting.setxDistance(320);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);

                    // 13 日
                    classCertFieldSetting = new ClassCertFieldSetting();
                    classCertFieldSetting.setUnitId(unitId);
                    classCertFieldSetting.setUnitName(unitName);
                    classCertFieldSetting.setCertTypeName(certTypeName);
                    classCertFieldSetting.setCertTemplateId(certTemplateId);
                    classCertFieldSetting.setCertTemplateName(certTemplateName);
                    classCertFieldSetting.setFieldPositionNum("13");
                    classCertFieldSetting.setFieldName("日");
                    classCertFieldSetting.setFontName("標楷體");
                    classCertFieldSetting.setFontSize(18);
                    classCertFieldSetting.setIsBold(false);
                    classCertFieldSetting.setIsItalic(false);
                    classCertFieldSetting.setHorizontalAlignType("置中");
                    classCertFieldSetting.setVerticalAlignType(null);
                    classCertFieldSetting.setTextDirection("水平");
                    classCertFieldSetting.setWidth(60);
                    classCertFieldSetting.setHeight(40);
                    classCertFieldSetting.setyDistance(709);
                    classCertFieldSetting.setxDistance(430);
                    classCertFieldSetting.setIsEnabled(true);
                    classCertFieldSetting.setCreatorAndUpdater(session);

                    classCertFieldSettingList.add(classCertFieldSetting);
                    break;
            }
        }

        classCertFieldSettingRepo.saveAll(classCertFieldSettingList);
    }

    // generate member name and  age
    public List<Printer011Object> memberNameAndAgeGenerator(List<ClassEnrollForm> classEnrollFormList) {

        List<Printer011Object> memberNameAndAgeList = new ArrayList<>();

        if (classEnrollFormList.size() != 0) {

            for (ClassEnrollForm classEnrollForm : classEnrollFormList) {

                String age = String.valueOf(classEnrollForm.getAge());

                switch (age.length()) {
                    case 0:

                        age = "";
                        break;
                    case 1:

                        age = numberToChineseGenerate(age);
                        break;
                    case 2:
                        age = numberToChineseGenerate(age.substring(0, 1)) + "十" +
                                numberToChineseGenerate(age.substring(1, 2).equals("0") ? "" : age.substring(1, 2));
                        break;
                    case 3:
                        age = numberToChineseGenerate(age.substring(0, 1)) + "百" +
                                (age.substring(1, 2).equals("0") ? numberToChineseGenerate(age.substring(1, 2)) : numberToChineseGenerate(age.substring(1, 2)) + "十") +
                                numberToChineseGenerate(age.substring(2, 3).equals("0") ? "" : age.substring(2, 3));
                        break;
                }

                memberNameAndAgeList.add(new Printer011Object(
                        classEnrollForm.getAliasName(),
                        age
                ));
            }
        }

        return memberNameAndAgeList;
    }

    // generate number to chinese
    public String numberToChineseGenerate(String number) {

        String chineseNumber = "";

        switch (number) {
            case "0":
                chineseNumber = "零";
                break;
            case "1":
                chineseNumber = "一";
                break;
            case "2":
                chineseNumber = "二";
                break;
            case "3":
                chineseNumber = "三";
                break;
            case "4":
                chineseNumber = "四";
                break;
            case "5":
                chineseNumber = "五";
                break;
            case "6":
                chineseNumber = "六";
                break;
            case "7":
                chineseNumber = "七";
                break;
            case "8":
                chineseNumber = "八";
                break;
            case "9":
                chineseNumber = "九";
                break;
        }

        return chineseNumber;
    }

    // generate classDetail
    public String classDetailGenerate(String classId, int certType) {

        String detail1 = "";
        String detail2 = "";
        String detail3 = "";

        String unitName = "";
        String classPeriodNum = "";
        String classDayOrNight = "";
        String classTypeName = "";
        String classDesc = "";

        ClassInfo classInfo = classInfoRepo.GetClassInfoByClassId2(classId);

        switch (certType) {
            case 1: // 中文

                unitName = "";
                classPeriodNum = classInfo.getClassPeriodNum();
                classDayOrNight = classInfo.getClassDayOrNight();
                classTypeName = classInfo.getClassTypeName();
                classDesc = classInfo.getClassDesc();

                if (classInfo.getUnitName().contains("寺") || classInfo.getUnitName().contains("精舍")) {

                    unitName = classInfo.getUnitName();
                } else {

                    unitName = classInfo.getUnitName() + "精舍";
                }

                switch (classPeriodNum.length()) {
                    case 1:

                        classPeriodNum = numberToChineseGenerate(classPeriodNum);
                        break;
                    case 2:
                        classPeriodNum = numberToChineseGenerate(classPeriodNum.substring(0, 1)) + "十" +
                                numberToChineseGenerate(classPeriodNum.substring(1, 2).equals("0") ? "" : classPeriodNum.substring(1, 2));
                        break;
                    case 3:
                        classPeriodNum = numberToChineseGenerate(classPeriodNum.substring(0, 1)) + "百" +
                                (classPeriodNum.substring(1, 2).equals("0") ? numberToChineseGenerate(classPeriodNum.substring(1, 2)) : numberToChineseGenerate(classPeriodNum.substring(1, 2)) + "十") +
                                numberToChineseGenerate(classPeriodNum.substring(2, 3).equals("0") ? "" : classPeriodNum.substring(2, 3));
                        break;
                }

                detail1 = unitName + "第" + classPeriodNum + "期;";
                detail2 = classDayOrNight + classTypeName.replace("班", "") + "禪修班";

                if (classInfo.getClassTypeNum() == 4 || classInfo.getClassTypeNum() == 5) {

                    detail3 = ";" + classDesc;
                }
                break;
            case 2: // 英文、中英文(英文)

                if (classInfo.getCertificateEngName() == null || classInfo.getCertificateEngName().isEmpty()) {
                    detail1 = ";";
                } else {
                    detail1 = classInfo.getCertificateEngName() + ";";
                }
                break;
            case 3: // 中英文(中文)

                if (classInfo.getCertificateName() == null || classInfo.getCertificateName().isEmpty()) {
                    detail1 = ";";
                } else {
                    detail1 = classInfo.getCertificateName() + ";";
                }
                break;
            case 4: // 日文
                if (classInfo.getCertificateEngName() == null || classInfo.getCertificateEngName().isEmpty()) {
                    detail1 = "";
                } else {
                    detail1 = classInfo.getCertificateEngName() + ";";
                }
                break;
        }

        return detail1 + detail2 + detail3;
    }

    // 刪除印表機資料
    public BaseResponse deletePinter(String printerId) {

        int errCode = 200;
        String errMsg = "success";

        ClassUnitPrinter classUnitPrinter = classUnitPrinterRepo.GetClassUnitPrinterByPrinterID(printerId);

        if (classUnitPrinter == null) {

            errCode = 400;
            errMsg = "PrinterId : " + printerId + " invalid";
        } else {

            List<ClassCertInfo> classCertInfoList = classCertInfoRepo.GetClassCertInfoByPrinterId(printerId);

            if (classCertInfoList.size() != 0) {

                List<String> certTemplateIdList = new ArrayList<>();

                for (ClassCertInfo classCertInfo : classCertInfoList) {

                    certTemplateIdList.add(classCertInfo.getCertTemplateId());
                }

                if (certTemplateIdList.size() != 0) {

                    List<ClassCertFieldSetting> classCertFieldSettingList =
                            classCertFieldSettingRepo.GetCertTemplateIdListByCertTemplateId(certTemplateIdList);

                    classCertFieldSettingRepo.deleteAll(classCertFieldSettingList);
                }

                classCertInfoRepo.deleteAll(classCertInfoList);
            }

            classUnitPrinterRepo.delete(classUnitPrinter);
        }

        return new BaseResponse(errCode, errMsg);
    }
}
