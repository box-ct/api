package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import tw.org.ctworld.meditation.beans.MemberInfos.MemberNote;
import tw.org.ctworld.meditation.beans.MemberInfos.MemberNoteResponse;
import tw.org.ctworld.meditation.beans.MemberInfos.NewMemberNoteRequest;
import tw.org.ctworld.meditation.models.CtMemberNote;
import tw.org.ctworld.meditation.repositories.CtMemberNoteRepo;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class ClassCtMemberNoteService {

    private static final Logger logger = LoggerFactory.getLogger(CodeDefService.class);

    @Autowired
    private CtMemberNoteRepo ctMemberNoteRepo;


    public MemberNoteResponse getNote(String memberId) {

        MemberNoteResponse response = new MemberNoteResponse();

        List<MemberNote> memberNoteList = ctMemberNoteRepo.GetClassCtMemberNote(memberId);

        response.setItems(memberNoteList);

        response.setErrCode(200);
        response.setErrMsg("success");

        return response;
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public void createNote(String memberId, NewMemberNoteRequest request, HttpSession session) {
        CtMemberNote note = new CtMemberNote();

        Integer maxId = ctMemberNoteRepo.GetMaxId();
        note.setId(maxId != null ? maxId + 1 : 1);
        note.setMemberId(memberId);
        note.setNote(request.getNote());
        note.setNoteType(request.getNoteType());
        note.setCreatorAndUpdater(session);

        ctMemberNoteRepo.save(note);
    }

    public boolean deleteNote(int noteId) {
        try {
            ctMemberNoteRepo.deleteById((long) noteId);
            return true;
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return false;
        }
    }


}
