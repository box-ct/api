package tw.org.ctworld.meditation.services;

import com.yubico.client.v2.VerificationResponse;
import com.yubico.client.v2.YubicoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class YubicoService {
    @Value("${yubico.clientId}")
    private int clientId;
    @Value("${yubico.secretKey}")
    private String secretKey;

    private static final Logger logger = LoggerFactory.getLogger(YubicoService.class);

    public boolean check(String otp) {
        YubicoClient client = YubicoClient.getClient(clientId, secretKey);

        try {
            VerificationResponse response = client.verify(otp);
            return response.isOk();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    public String getIdentity(String otp) {
        return YubicoClient.getPublicId(otp);
    }
}
