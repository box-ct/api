package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.UserLoginLog;
import tw.org.ctworld.meditation.repositories.UserLoginLogRepo;

import java.util.List;

@Service
public class UserLoginLogService {
    private static final Logger logger = LoggerFactory.getLogger(UserLoginLogService.class);

    @Autowired
    UserLoginLogRepo userLoginLogRepo;

    public List<UserLoginLog> getCurrentlyLogin() {
        return userLoginLogRepo.GetCurrentlyLogin(CommonUtils.GetDateFromToday(-1));
    }

    public void logoutAll() {
        userLoginLogRepo.LogoutAll();
    }
}
