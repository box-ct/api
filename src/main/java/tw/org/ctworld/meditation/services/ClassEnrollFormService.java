package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.Anonymous.Anonymous009_1Object;
import tw.org.ctworld.meditation.beans.Anonymous.Anonymous009_2Object;
import tw.org.ctworld.meditation.beans.Anonymous.AnonymousMakeupClass;
import tw.org.ctworld.meditation.beans.Anonymous.AnonymousMakeupClassDate;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup001Object;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup002Object;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup003Request;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup004Request;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord003Response;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord003_1Object;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord003_2Object;
import tw.org.ctworld.meditation.beans.ClassStats.*;
import tw.org.ctworld.meditation.beans.ClassReport.ClassAttendance;
import tw.org.ctworld.meditation.beans.ClassReport.ClassMemberRecord;
import tw.org.ctworld.meditation.beans.ClassUpgrade.*;
import tw.org.ctworld.meditation.beans.Classes.*;
import tw.org.ctworld.meditation.beans.Common.Common012_1Object;
import tw.org.ctworld.meditation.beans.Common.Common012Response;
import tw.org.ctworld.meditation.beans.Common.Common012_2Object;
import tw.org.ctworld.meditation.beans.DataReport.*;
import tw.org.ctworld.meditation.beans.Member.*;
import tw.org.ctworld.meditation.beans.Printer.Printer011Response;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import tw.org.ctworld.meditation.beans.DataSearch.*;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.models.CtMemberInfo;
import tw.org.ctworld.meditation.models.ClassEnrollForm;
import tw.org.ctworld.meditation.models.ClassInfo;
import tw.org.ctworld.meditation.repositories.ClassCommonFilepathRepo;
import tw.org.ctworld.meditation.repositories.CtMemberInfoRepo;
import tw.org.ctworld.meditation.repositories.ClassEnrollFormRepo;
import tw.org.ctworld.meditation.repositories.ClassInfoRepo;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.libs.CommonUtils.Empty2Null;
import static tw.org.ctworld.meditation.libs.CommonUtils.Null2Empty;

@Service
public class ClassEnrollFormService {

    private static final Logger logger = LoggerFactory.getLogger(ClassEnrollFormService.class);

    @Autowired
    private ClassEnrollFormRepo classEnrollFormRepo;

    @Autowired
    private CtMemberInfoRepo ctMemberInfoRepo;

    @Autowired
    private ClassInfoRepo classInfoRepo;

    @Autowired
    private ClassDateInfoRepo classDateInfoRepo;

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private ClassAttendRecordRepo classAttendRecordRepo;

    @Autowired
    private ClassCommonFilepathRepo classCommonFilepathRepo;

    @Autowired
    private CodeDefRepo codeDefRepo;

    @Autowired
    private ClassAttendMarkRepo classAttendMarkRepo;

    @Autowired
    private ClassMakeupRecordRepo classMakeupRecordRepo;

    @Autowired
    private UserInfoRepo userInfoRepo;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;


//    // 姓名為空值，有連動選單的條件
//    public List<Members001Object> getClassEnrollFormListByRequestParam(String classStatus,
//                                                                       String classPeriodNum,
//                                                                       String className,
//                                                                       String classGroupId,
//                                                                       String unitId) {
//        List<Members001Object> MembersObjectList = new ArrayList<>();
//
//        List<String> classIdList = classInfoRepo.GetClassIdByClassStatus(classStatus);
//
//        if (classIdList.size() != 0) {
//
//            List<ClassEnrollForm> classEnrollFormList = new ArrayList<>();
//
//            if (!classGroupId.equals("")) {
//
//                classEnrollFormList = classEnrollFormRepo.GetClassEnrollFormListByRequestParam(
//                        classIdList, classPeriodNum, className, classGroupId, unitId);
//            } else {
//
//                classEnrollFormList = classEnrollFormRepo.GetClassEnrollFormListByRequestParam(
//                        classIdList, classPeriodNum, className, unitId);
//            }
//
//
//            if (classEnrollFormList.size() != 0) {
//
//                for (int x = 0; x < classEnrollFormList.size(); x++) {
//                    MembersObjectList.add(new Members001Object(
//                            classEnrollFormList.get(x).getMemberId(),
//                            classEnrollFormList.get(x).getGender(),
//                            Null2Empty(classEnrollFormList.get(x).getAliasName()),
//                            Null2Empty(classEnrollFormList.get(x).getCtDharmaName()),
//                            Null2Empty(classEnrollFormList.get(x).getClassPeriodNum()),
//                            Null2Empty(classEnrollFormList.get(x).getClassName()),
//                            classEnrollFormList.get(x).getAge(),
//                            Null2Empty(classEnrollFormList.get(x).getClassGroupId()),
//                            Null2Empty(classEnrollFormList.get(x).getHomePhoneNum1()),
//                            Null2Empty(classEnrollFormList.get(x).getMobileNum1()),
//                            Null2Empty(classEnrollFormList.get(x).getDsaJobNameList()),
//                            classEnrollFormList.get(x).getIsDroppedClass()));
//                }
//            }
//        }
//
//        return MembersObjectList;
//    }
//
//    // 姓名為有值，有連動選單的條件
//    public List<Members001Object> getClassEnrollFormListByRequestParam(String classStatus,
//                                                                       String classPeriodNum,
//                                                                       String className,
//                                                                       String classGroupId,
//                                                                       String unitId,
//                                                                       String aliasName) {
//
//        List<Members001Object> MembersObjectList = new ArrayList<>();
//
//        List<String> classIdList = classInfoRepo.GetClassIdByClassStatus(classStatus);
//
//        List<ClassEnrollForm> classEnrollFormList = classEnrollFormRepo.GetClassEnrollFormListByRequestParam(
//                classIdList, classPeriodNum, className, classGroupId, unitId, aliasName);
//
//        if (classIdList.size() != 0) {
//
//            for (int x = 0; x < classEnrollFormList.size(); x++) {
//                MembersObjectList.add(new Members001Object(
//                        classEnrollFormList.get(x).getMemberId(),
//                        classEnrollFormList.get(x).getGender(),
//                        Null2Empty(classEnrollFormList.get(x).getAliasName()),
//                        Null2Empty(classEnrollFormList.get(x).getCtDharmaName()),
//                        Null2Empty(classEnrollFormList.get(x).getClassPeriodNum()),
//                        Null2Empty(classEnrollFormList.get(x).getClassName()),
//                        classEnrollFormList.get(x).getAge(),
//                        Null2Empty(classEnrollFormList.get(x).getClassGroupId()),
//                        Null2Empty(classEnrollFormList.get(x).getHomePhoneNum1()),
//                        Null2Empty(classEnrollFormList.get(x).getMobileNum1()),
//                        Null2Empty(classEnrollFormList.get(x).getDsaJobNameList()),
//                        classEnrollFormList.get(x).getIsDroppedClass()));
//            }
//        } else {
//
//            List<CtMemberInfo> ctMemberInfoList = ctMemberInfoRepo
//                    .GetClassCtMemberInfoListByRequestParam(aliasName, unitId, false);
//
//            if (ctMemberInfoList.size() != 0) {
//
//                for (int a = 0; a < ctMemberInfoList.size(); a++) {
//                    MembersObjectList.add(new Members001Object(
//                            ctMemberInfoList.get(a).getMemberId(),
//                            ctMemberInfoList.get(a).getGender().equals("M") ? "男" : "女",
//                            ctMemberInfoList.get(a).getAliasName(),
//                            ctMemberInfoList.get(a).getCtDharmaName(),
//                            "",
//                            "",
//                            ctMemberInfoList.get(a).getAge(),
//                            "",
//                            ctMemberInfoList.get(a).getHomePhoneNum1(),
//                            ctMemberInfoList.get(a).getMobileNum1(),
//                            ctMemberInfoList.get(a).getDsaJobNameList(),
//                            false));
//                }
//            }
//        }
//
//        return MembersObjectList;
//    }
//
//    // 姓名為有值，無連動選單的條件
//    public List<Members001Object> getClassEnrollFormListByRequestParam(String unitId, String aliasName) {
//
//        List<Members001Object> MembersObjectList = new ArrayList<>();
//
//        List<ClassEnrollForm> classEnrollFormList = classEnrollFormRepo.GetClassEnrollFormListByRequestParam(
//                unitId, aliasName);
//
//        if (classEnrollFormList.size() != 0) {
//
//            for (int x = 0; x < classEnrollFormList.size(); x++) {
//                MembersObjectList.add(new Members001Object(
//                        classEnrollFormList.get(x).getMemberId(),
//                        classEnrollFormList.get(x).getGender(),
//                        Null2Empty(classEnrollFormList.get(x).getAliasName()),
//                        Null2Empty(classEnrollFormList.get(x).getCtDharmaName()),
//                        Null2Empty(classEnrollFormList.get(x).getClassPeriodNum()),
//                        Null2Empty(classEnrollFormList.get(x).getClassName()),
//                        classEnrollFormList.get(x).getAge(),
//                        Null2Empty(classEnrollFormList.get(x).getClassGroupId()),
//                        Null2Empty(classEnrollFormList.get(x).getHomePhoneNum1()),
//                        Null2Empty(classEnrollFormList.get(x).getMobileNum1()),
//                        Null2Empty(classEnrollFormList.get(x).getDsaJobNameList()),
//                        classEnrollFormList.get(x).getIsDroppedClass()));
//            }
//        } else {
//
//            List<CtMemberInfo> ctMemberInfoList = ctMemberInfoRepo
//                    .GetClassCtMemberInfoListByRequestParam(aliasName, unitId, false);
//
//            if (ctMemberInfoList.size() != 0) {
//
//                for (int a = 0; a < ctMemberInfoList.size(); a++) {
//                    MembersObjectList.add(new Members001Object(
//                            ctMemberInfoList.get(a).getMemberId(),
//                            ctMemberInfoList.get(a).getGender().equals("M") ? "男" : "女",
//                            Null2Empty(ctMemberInfoList.get(a).getAliasName()),
//                            Null2Empty(ctMemberInfoList.get(a).getCtDharmaName()),
//                            "",
//                            "",
//                            ctMemberInfoList.get(a).getAge(),
//                            "",
//                            Null2Empty(ctMemberInfoList.get(a).getHomePhoneNum1()),
//                            Null2Empty(ctMemberInfoList.get(a).getMobileNum1()),
//                            Null2Empty(ctMemberInfoList.get(a).getDsaJobNameList()),
//                            false));
//                }
//            }
//        }
//
//        return MembersObjectList;
//    }

    // 取得報名狀態
    public String getClassRegisterStatus(String memberId, String classId, String unitId) {

        String classRegisterStatus = "未報名";

        List<ClassEnrollForm> classEnrollFormList = classEnrollFormRepo.GetClassRegisterStatus(memberId, classId, unitId);

        if (classEnrollFormList.size() != 0) {

            if (classEnrollFormList.get(0).getIsDroppedClass()) {

                classRegisterStatus = "已取消;" + classEnrollFormList.get(0).getClassEnrollFormId();
            } else {

                classRegisterStatus = "已報名;" + classEnrollFormList.get(0).getClassEnrollFormId();
            }
        } else {

            classRegisterStatus = "未報名";
        }

        return classRegisterStatus;
    }

    // 報名/更新資料
    public Members003Response checkClassEnrollForm(HttpSession session, String memberId, Members003Request members003Request) {

        int errCode = 0;
        String errMsg = "success";
        String classEnrollFormId = "";

        if (members003Request.getClassEnrollFormId().equals("")) {
            // 檢查重覆報名並拋出錯誤

            String[] classRegisterStatus = getClassRegisterStatus(
                    memberId,
                    members003Request.getClassId(),
                    (String) session.getAttribute(UserSession.Keys.UnitId.getValue())).split(";");

            switch (classRegisterStatus[0]) {
                case "已取消":

                    errCode = 400;
                    errMsg = classRegisterStatus[0];
                    classEnrollFormId = classRegisterStatus[1];
                    break;
                case "已報名":

                    errCode = 400;
                    errMsg = classRegisterStatus[0];
                    classEnrollFormId = classRegisterStatus[1];
                    break;
                case "未報名":

                    // 報名
                    errCode = 200;
                    String[] callback = addClassEnrollForm(session, memberId, members003Request).split(";");
                    errMsg = callback[0];
                    classEnrollFormId = callback[1];
                    break;
            }
        } else {
            // 更新
            errCode = 200;
            String[] callback = editClassEnrollForm(session, memberId, members003Request).split(";");
            errMsg = callback[0];
            classEnrollFormId = callback[1];
        }

        return new Members003Response(errCode, errMsg, classEnrollFormId);
    }

    // 報名/更新資料-報名
    public String addClassEnrollForm(HttpSession session, String memberId, Members003Request members003Request) {

        MembersObject classCtMemberInfo = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId2(memberId);

        ClassInfo classInfo = classInfoRepo.GetClassInfoByClassId(members003Request.getClassId());

        List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetClassDateInfoByClassId(members003Request.getClassId());

        String gender = "男";
        switch (classCtMemberInfo.getGender()) {
            case "F":
                gender = "女";
                break;
            default:
                gender = "男";
                break;
        }

        ClassEnrollForm classEnrollForm = new ClassEnrollForm();
        classEnrollForm.setUnitId((String) session.getAttribute(UserSession.Keys.UnitId.getValue()));
        classEnrollForm.setUnitName((String) session.getAttribute(UserSession.Keys.UnitName.getValue()));
        classEnrollForm.setClassEnrollFormId(classEnrollFormIdGenerator());
        classEnrollForm.setMemberId(classCtMemberInfo.getMemberId());
        classEnrollForm.setTwIdNum(classCtMemberInfo.getTwIdNum());
        classEnrollForm.setGender(gender);
        classEnrollForm.setAliasName(Empty2Null(classCtMemberInfo.getAliasName()));
        classEnrollForm.setCtDharmaName(Empty2Null(classCtMemberInfo.getCtDharmaName()));
        classEnrollForm.setAge(classCtMemberInfo.getAge());
        classEnrollForm.setHomePhoneNum1(Empty2Null(classCtMemberInfo.getHomePhoneNum1()));
        classEnrollForm.setMobileNum1(Empty2Null(classCtMemberInfo.getMobileNum1()));
        classEnrollForm.setDsaJobNameList(Empty2Null(classCtMemberInfo.getDsaJobNameList()));
        classEnrollForm.setAddress(Empty2Null(
                classCtMemberInfo.getMailingCountry() + classCtMemberInfo.getMailingState() +
                        classCtMemberInfo.getMailingCity() + classCtMemberInfo.getMailingStreet()));
        classEnrollForm.setCompanyNameAndJobTitle(Empty2Null(classCtMemberInfo.getCompanyJobTitle()));
        classEnrollForm.setSchoolNameAndMajor(Empty2Null(
                classCtMemberInfo.getSchoolName() + classCtMemberInfo.getSchoolMajor()));
        classEnrollForm.setBirthDate(CommonUtils.ParseDate(classCtMemberInfo.getBirthDate()));
        classEnrollForm.setClassId(classInfo.getClassId());
        classEnrollForm.setClassTypeNum(classInfo.getClassTypeNum());
        classEnrollForm.setClassTypeName(classInfo.getClassTypeName());
        classEnrollForm.setYear(classInfo.getYear());
        classEnrollForm.setClassStartDate(classInfo.getClassStartDate());
        classEnrollForm.setClassEndDate(classInfo.getClassEndDate());
        classEnrollForm.setClassPeriodNum(classInfo.getClassPeriodNum());
        classEnrollForm.setClassName(classInfo.getClassName());
        classEnrollForm.setDayOfWeek(classInfo.getDayOfWeek());
        classEnrollForm.setClassDesc(classInfo.getClassDesc());
        classEnrollForm.setClassFullName(classInfo.getClassFullName());
        classEnrollForm.setClassGroupId(Empty2Null(members003Request.getClassGroupId()));
        classEnrollForm.setMemberGroupNum(memberGroupNumGenerator(members003Request.getClassId(), members003Request.getClassGroupId())); // 學號
        classEnrollForm.setSameGroupMemberName(Empty2Null(members003Request.getSameGroupMemberName()));
        classEnrollForm.setCurrentClassIntroducerName(Empty2Null(members003Request.getCurrentClassIntroducerName()));
        classEnrollForm.setCurrentClassIntroducerRelationship(Empty2Null(members003Request.getCurrentClassIntroducerRelationship()));
        classEnrollForm.setChildContactPersonName(Empty2Null(members003Request.getChildContactPersonName()));
        classEnrollForm.setChildContactPersonPhoneNum(Empty2Null(members003Request.getChildContactPersonPhoneNum()));
        classEnrollForm.setNewReturnChangeMark("新報名");
        classEnrollForm.setNewReturnChangeDate(getLocalDate());
        classEnrollForm.setChangeAndWaitPrintMark(true);
        classEnrollForm.setUserDefinedLabel1(false);
        classEnrollForm.setUserDefinedLabel2(false);
        classEnrollForm.setAsistantMasterName(classInfo.getAssistantMasterName());
        classEnrollForm.setIsDroppedClass(false);
        classEnrollForm.setCreatorAndUpdater(session);

        if (classDateInfoList.size() != 0) {

            for (ClassDateInfo classDateInfo : classDateInfoList) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                Date classDate = null;
                try {
                    classDate = dateFormat.parse(classDateInfo.getClassDate().toString().split(" ")[0] + " " + classDateInfo.getClassStartTime() + ":00");
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                ClassAttendRecord classAttendRecord = new ClassAttendRecord(
                        classDateInfo.getUnitId(),
                        classDateInfo.getUnitName(),
                        classEnrollForm.getClassEnrollFormId(),
                        classEnrollForm.getMemberId(),
                        classEnrollForm.getGender(),
                        classEnrollForm.getAliasName(),
                        classEnrollForm.getCtDharmaName(),
                        classEnrollForm.getClassId(),
                        classEnrollForm.getClassPeriodNum(),
                        classEnrollForm.getClassName(),
                        classEnrollForm.getClassGroupId(),
                        classDateInfo.getClassWeeksNum(),
                        classDate,
                        null,
                        null,
                        null,
                        null
                );

                classAttendRecord.setCreatorAndUpdater(session);

                classAttendRecordRepo.save(classAttendRecord);
            }
        }

        return "success;" + classEnrollFormRepo.save(classEnrollForm).getClassEnrollFormId();
    }

    // 報名/更新資料-更新
    public String editClassEnrollForm(HttpSession session, String memberId, Members003Request members003Request) {

        ClassEnrollForm classEnrollForm = classEnrollFormRepo
                .GetClassEnrollFormByClassEnrollFormId(members003Request.getClassEnrollFormId());

        classEnrollForm.setClassGroupId(members003Request.getClassGroupId());
        classEnrollForm.setSameGroupMemberName(Empty2Null(members003Request.getSameGroupMemberName()));
        classEnrollForm.setCurrentClassIntroducerName(Empty2Null(members003Request.getCurrentClassIntroducerName()));
        classEnrollForm.setCurrentClassIntroducerRelationship(Empty2Null(members003Request.getCurrentClassIntroducerRelationship()));
        classEnrollForm.setChildContactPersonName(Empty2Null(members003Request.getChildContactPersonName()));
        classEnrollForm.setChildContactPersonPhoneNum(Empty2Null(members003Request.getChildContactPersonPhoneNum()));
        classEnrollForm.setIsDroppedClass(false);
        classEnrollForm.setUpdater(session);

        return "success;" + classEnrollFormRepo.save(classEnrollForm).getClassEnrollFormId();
    }

    // 輸出名牌
    public List<ClassMemberRecord> printCard(String classEnrollFormId, String lang) {

        List<ClassMemberRecord> iRecords = new ArrayList<>();

        ClassMemberRecord iRecord = new ClassMemberRecord();

        Members004Object members004Object = classEnrollFormRepo.GetCardInfoByMemberId(classEnrollFormId);

        List<String> memberIds = new ArrayList<>();
        memberIds.add(members004Object.getMemberId());

        List<Members002ImagesObject> photos = ctMemberInfoService.getPhotosByMemberIdList(memberIds, true);

        if (photos.size() > 0) {
            iRecord.setPhoto(photos.get(0).getPhoto());
        }

        iRecord.setClassName(members004Object.getClassName());
        iRecord.setDayOfWeek(members004Object.getDayOfWeek());
        iRecord.setDayOrNight(members004Object.getClassDayOrNight());
        iRecord.setAliasName(members004Object.getAliasName());
        iRecord.setClassJobTitleList(members004Object.getClassJobTitleList());
        iRecord.setCtDharmaName(members004Object.getCtDharmaName());
        iRecord.setGender(members004Object.getGender());
        iRecord.setClassGroupId(members004Object.getClassGroupId());
        iRecord.setMemberId(members004Object.getMemberId());
        iRecord.setClassTypeName(members004Object.getClassTypeName());

        iRecords.add(iRecord);

        return iRecords;
    }

    // 取得已報名該課程的學員列表
    public List<Classes006Object> getClassRegisterList(String classId, String unitId) {

        List<Classes006Object> classes006ObjectList =
                classEnrollFormRepo.GetClassRegisterList(classId, false, unitId);

        if (classes006ObjectList.size() != 0) {
            for (Classes006Object classes006Object : classes006ObjectList) {

                classes006Object.setCtDharmaName(Null2Empty(classes006Object.getCtDharmaName()));
                classes006Object.setClassJobTitleList(Null2Empty(classes006Object.getClassJobTitleList()));
                classes006Object.setPreviousClassName(Null2Empty(classes006Object.getPreviousClassName()));
                classes006Object.setPreviousClassGroupName(Null2Empty(classes006Object.getPreviousClassGroupName()));
                classes006Object.setNextNewClassName(Null2Empty(classes006Object.getNextNewClassName()));
                classes006Object.setNextNewClassNote(Null2Empty(classes006Object.getNextNewClassNote()));
                classes006Object.setMobileNum1(Null2Empty(classes006Object.getMobileNum1()));
            }
        }

        return classes006ObjectList;
    }

    // 取得學員長職事資料
    // CR3
    public List<Classes007Object> getClassLeaderList(String classId) {

        List<String> memberGroupNumList = new ArrayList<>();
        memberGroupNumList.add("0");
        memberGroupNumList.add("1");

        List<Classes007Object> classes007ObjectList = classEnrollFormRepo.GetClassLeaderListByClassId(classId, memberGroupNumList);

        List<String> memberIdList = new ArrayList<>();

        for (Classes007Object classes007Object : classes007ObjectList) {

            memberIdList.add(classes007Object.getMemberId());
        }

        List<Classes007Object> records = userInfoRepo.GetMemberIdAndManagedListByMemberIdList(memberIdList);

        if (records.size() != 0) {

            for (Classes007Object classes007Object : classes007ObjectList) {

                List<Classes007Object> myRecords = records.stream()
                        .filter(s -> s.getMemberId().equals(classes007Object.getMemberId()))
                        .collect(Collectors.toList());

                if (myRecords.size() != 0) {

                    classes007Object.setUserId(myRecords.get(0).getUserId());
                    classes007Object.setManagedClassList(myRecords.get(0).getManagedClassList());
                }else {
                    classes007Object.setUserId("");
                    classes007Object.setManagedClassList("");
                }
            }
        }

        return classes007ObjectList;
    }

    // 取得課程學員列表
    // CR3
    public List<Classes007Object> getClassMemberList(String classId) {

        List<Classes007Object> classes007ObjectList = classEnrollFormRepo.GetClassLeaderListByClassId(classId);

        List<String> memberIdList = new ArrayList<>();

        for (Classes007Object classes007Object : classes007ObjectList) {

            memberIdList.add(classes007Object.getMemberId());
        }

        List<Classes007Object> records = userInfoRepo.GetMemberIdAndManagedListByMemberIdList(memberIdList);

        if (records.size() != 0) {

            for (Classes007Object classes007Object : classes007ObjectList) {

                List<Classes007Object> myRecords = records.stream()
                        .filter(s -> s.getMemberId().equals(classes007Object.getMemberId()))
                        .collect(Collectors.toList());

                if (myRecords.size() != 0) {

                    classes007Object.setUserId(myRecords.get(0).getUserId());
                    classes007Object.setManagedClassList(myRecords.get(0).getManagedClassList());
                }else {
                    classes007Object.setUserId("");
                    classes007Object.setManagedClassList("");
                }
            }
        }

        return classes007ObjectList;
    }

    // 新增學員長執事
    public BaseResponse addClassEnrollFormJobTitle(HttpSession session, Classes008Request classes008Request) {

        int errCode = 200;
        String errMsg = "";

        ClassEnrollForm classEnrollForm =
                classEnrollFormRepo.GetClassEnrollFormByClassEnrollFormId(classes008Request.getClassEnrollFormId());

        String classJobHighestNum = null;

        String classJobTitleList = null;
        if (classes008Request.getClassJobTitleList().size() != 0) {

            for (int x = 0; x < classes008Request.getClassJobTitleList().size(); x++) {

                if (x == 0) {
                    classJobTitleList = classes008Request.getClassJobTitleList().get(x);

                    classJobHighestNum = codeDefRepo.GetClassCodeDefCodeId(
                            "學員長職事", classes008Request.getClassJobTitleList().get(x));
                } else {
                    classJobTitleList = classJobTitleList + "/" + classes008Request.getClassJobTitleList().get(x);
                }
            }
        }

        String classJobContentList = null;
        if (classes008Request.getClassJobContentList().size() != 0) {

            for (int x = 0; x < classes008Request.getClassJobContentList().size(); x++) {

                if (x == 0) {
                    classJobContentList = classes008Request.getClassJobContentList().get(x);
                } else {
                    classJobContentList = classJobContentList + "/" + classes008Request.getClassJobContentList().get(x);
                }
            }
        }

        if (classes008Request.getClassJobTitleList().size() != 0) {
            // 有執事

            if (classes008Request.getIsLeadGroup()) {
                // 有帶班
                classEnrollForm.setMemberGroupNum("0");
            } else {
                // 無帶班
                classEnrollForm.setMemberGroupNum("1");
            }
        } else {
            // 無執事

            if (classEnrollForm.getMemberGroupNum().equals("0") || classEnrollForm.getMemberGroupNum().equals("1")) {
                // 重新編號(一般學員)

                classEnrollForm.setMemberGroupNum(memberGroupNumGenerator(classEnrollForm.getClassId(), classEnrollForm.getClassGroupId()));
            }
        }

        UserInfo userInfo = userInfoRepo.GetClassLoginUserInfoById(classEnrollForm.getMemberId());

        if (userInfo != null) {

            userInfo.setManagedClassList(classes008Request.getManagedClassList());

            userInfoRepo.save(userInfo);
        }

        classEnrollForm.setClassGroupId(Empty2Null(classes008Request.getClassGroupId()));
        classEnrollForm.setClassJobHighestNum(classJobHighestNum);
        classEnrollForm.setClassJobTitleList(classJobTitleList);
        classEnrollForm.setClassJobContentList(classJobContentList);
        classEnrollForm.setIsLeadGroup(classes008Request.getIsLeadGroup());
        classEnrollForm.setUpdater(session);

        String classEnrollFormId = classEnrollFormRepo.save(classEnrollForm).getClassEnrollFormId();

        errMsg = "success ClassEnrollFormId : " + classEnrollFormId;

        return new BaseResponse(errCode, errMsg);
    }

    // 依id取得學員長執事資料
    public Classes009Response getClassEnrollForm(String classEnrollFormId) {

        Classes009Response classEnrollForm = classEnrollFormRepo.GetLeaderInfoByClassEnrollFormId(classEnrollFormId);

        String managedClassList = userInfoRepo.GetClassLoginUserInfoManagedClassListByMemberId(classEnrollForm.getMemberId());

        Classes009Response classes009Response = new Classes009Response(
                200,
                "success",
                classEnrollForm.getId(),
                classEnrollForm.getClassEnrollFormId(),
                classEnrollForm.getMemberId(),
                classEnrollForm.getGender(),
                classEnrollForm.getAliasName(),
                Null2Empty(classEnrollForm.getCtDharmaName()),
                classEnrollForm.getClassGroupId(),
                classEnrollForm.getMemberGroupNum(),
                classEnrollForm.getClassJobTitleList(),
                classEnrollForm.getClassJobContentList(),
                classEnrollForm.getIsLeadGroup(),
                managedClassList == null ? "" : managedClassList
        );

        return classes009Response;
    }

    // 更新學員長職事資料
    public String editClassEnrollFormJobTitle(HttpSession session, String classEnrollFormId, Classes008Request classes008Request) {

        ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByClassEnrollFormId(classEnrollFormId);

        String classJobHighestNum = null;

        String classJobTitleList = null;
        if (classes008Request.getClassJobTitleList().size() != 0) {

            for (int x = 0; x < classes008Request.getClassJobTitleList().size(); x++) {

                if (x == 0) {
                    classJobTitleList = classes008Request.getClassJobTitleList().get(x);

                    classJobHighestNum = codeDefRepo.GetClassCodeDefCodeId(
                            "學員長職事", classes008Request.getClassJobTitleList().get(x));
                } else {
                    classJobTitleList = classJobTitleList + "/" + classes008Request.getClassJobTitleList().get(x);
                }
            }
        }

        String classJobContentList = null;
        if (classes008Request.getClassJobContentList().size() != 0) {

            for (int x = 0; x < classes008Request.getClassJobContentList().size(); x++) {

                if (x == 0) {
                    classJobContentList = classes008Request.getClassJobContentList().get(x);
                } else {
                    classJobContentList = classJobContentList + "/" + classes008Request.getClassJobContentList().get(x);
                }
            }
        }

        if (classes008Request.getClassJobTitleList().size() != 0) {
            // 有執事

            if (classes008Request.getIsLeadGroup()) {
                // 有帶班
                classEnrollForm.setMemberGroupNum("0");
            } else {
                // 無帶班
                classEnrollForm.setMemberGroupNum("1");
            }
        } else {
            // 無執事

            if (classEnrollForm.getMemberGroupNum().equals("0") || classEnrollForm.getMemberGroupNum().equals("1")) {
                // 重新編號(一般學員)

                classEnrollForm.setMemberGroupNum(memberGroupNumGenerator(classEnrollForm.getClassId(), classEnrollForm.getClassGroupId()));
            }
        }

        UserInfo userInfo = userInfoRepo.GetClassLoginUserInfoById(classEnrollForm.getMemberId());

        if (userInfo != null) {

            userInfo.setManagedClassList(classes008Request.getManagedClassList());

            userInfoRepo.save(userInfo);
        }

        classEnrollForm.setClassGroupId(Empty2Null(classes008Request.getClassGroupId()));
        classEnrollForm.setClassJobHighestNum(classJobHighestNum);
        classEnrollForm.setClassJobTitleList(classJobTitleList);
        classEnrollForm.setClassJobContentList(classJobContentList);
        classEnrollForm.setIsLeadGroup(classes008Request.getIsLeadGroup());
        classEnrollForm.setUpdater(session);

        return "success ClassEnrollFormId : " + classEnrollFormRepo.save(classEnrollForm).getClassEnrollFormId();
    }

    // 依id刪除學員長職事資料
    public String deleteClassEnrollFormJobTitle(HttpSession session, String classEnrollFormId) {

        ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByClassEnrollFormId(classEnrollFormId);

        UserInfo userInfo = userInfoRepo.GetClassLoginUserInfoById(classEnrollForm.getMemberId());

        if (userInfo != null) {

            userInfo.setManagedClassList(null);

            userInfoRepo.save(userInfo);
        }

        classEnrollForm.setMemberGroupNum(memberGroupNumGenerator(classEnrollForm.getClassId(), classEnrollForm.getClassGroupId()));
        classEnrollForm.setClassJobHighestNum(null);
        classEnrollForm.setClassJobTitleList(null);
        classEnrollForm.setClassJobContentList(null);
        classEnrollForm.setIsLeadGroup(false);
        classEnrollForm.setUpdater(session);

        return "success ClassEnrollFormId : " + classEnrollFormRepo.save(classEnrollForm).getClassEnrollFormId();
    }

    // 取得學員班別
    public List<Members006Object> getMemberClassName(String memberId, String status, String unitId) {

        List<Members006Object> members006ObjectList = new ArrayList<>();

        members006ObjectList = classEnrollFormRepo.GetMemberClassName(memberId, status, unitId);

        return members006ObjectList;
    }

    // 變更班/組別
    public String changeClass(HttpSession session, String memberId, Members005Request members005Request) {

        // 取得轉入班別資訊
        ClassInfo classInfo = classInfoRepo.GetClassInfoByClassId(members005Request.getNext().getClassId());

        // 取得轉入班別所有課程
        List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetClassDateInfoByClassId(members005Request.getNext().getClassId());

        // 取得原報名表
        ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(memberId, members005Request.getPrev().getClassId());

        // 清空以下欄位
        classEnrollForm.setClassJobHighestNum(null);
        classEnrollForm.setClassJobTitleList(null);
        classEnrollForm.setClassJobContentList(null);
        classEnrollForm.setIsLeadGroup(false);
        classEnrollForm.setHomeClassName(null);
        classEnrollForm.setHomeClassPeriodNum(null);
        classEnrollForm.setHomeClassId(null);
        classEnrollForm.setIsHomeClass(false);

        // 寫入原課程名稱與Id
        classEnrollForm.setBeforeChangeClassName(classEnrollForm.getClassName()); // 轉前班別
        classEnrollForm.setBeforeChangeClassId(classEnrollForm.getClassId()); // 轉前班別ID

        // 寫入轉入班別課程資訊
        classEnrollForm.setClassId(members005Request.getNext().getClassId());
        classEnrollForm.setClassTypeNum(classInfo.getClassTypeNum());
        classEnrollForm.setClassTypeName(classInfo.getClassTypeName());
        classEnrollForm.setYear(classInfo.getYear());
        classEnrollForm.setClassStartDate(classInfo.getClassStartDate());
        classEnrollForm.setClassEndDate(classInfo.getClassEndDate());
        classEnrollForm.setClassPeriodNum(classInfo.getClassPeriodNum());
        classEnrollForm.setClassName(classInfo.getClassName());
        classEnrollForm.setDayOfWeek(classInfo.getDayOfWeek());
        classEnrollForm.setClassDesc(classInfo.getClassDesc());
        classEnrollForm.setClassFullName(classInfo.getClassFullName());
        classEnrollForm.setAsistantMasterName(classInfo.getAssistantMasterName());
        classEnrollForm.setClassGroupId(members005Request.getNext().getClassGroupId());
        classEnrollForm.setMemberGroupNum(memberGroupNumGenerator(members005Request.getNext().getClassId(), members005Request.getNext().getClassGroupId()));
        classEnrollForm.setChangeAndWaitPrintMark(true);
        classEnrollForm.setNewReturnChangeMark("轉班組");
        classEnrollForm.setNewReturnChangeDate(getLocalDate());
        classEnrollForm.setUpdater(session);


        // 新增轉入班別上課紀錄
        if (classDateInfoList.size() != 0) {

            for (ClassDateInfo classDateInfo : classDateInfoList) {

                ClassAttendRecord classAttendRecord = new ClassAttendRecord();
                classAttendRecord.setUnitId(classDateInfo.getUnitId());
                classAttendRecord.setUnitName(classDateInfo.getUnitName());
                classAttendRecord.setClassEnrollFormId(classEnrollForm.getClassEnrollFormId());
                classAttendRecord.setMemberId(classEnrollForm.getMemberId());
                classAttendRecord.setGender(classEnrollForm.getGender());
                classAttendRecord.setAliasName(classEnrollForm.getAliasName());
                classAttendRecord.setCtDharmaName(classEnrollForm.getCtDharmaName());
                classAttendRecord.setClassId(classEnrollForm.getClassId());
                classAttendRecord.setClassPeriodNum(classEnrollForm.getClassPeriodNum());
                classAttendRecord.setClassName(classEnrollForm.getClassName());
                classAttendRecord.setClassGroupId(classEnrollForm.getClassGroupId());
                classAttendRecord.setClassWeeksNum(classDateInfo.getClassWeeksNum());
                classAttendRecord.setClassDate(classDateInfo.getClassDate());
                long startDtTm = classDateInfo.getClassDate().getTime() + classDateInfo.getClassStartTime().getTime();
                classAttendRecord.setClassStartDtTm(new Date(startDtTm));
//                classAttendRecord.setAttendMark(null);
//                classAttendRecord.setAttendCheckinDtTm(null);
//                classAttendRecord.setNote(null);
                classAttendRecord.setCreatorAndUpdater(session);

                classAttendRecordRepo.save(classAttendRecord);
            }
        }

        // 取得原上課紀錄
        List<ClassAttendRecord> oldClassAttendRecordList = classAttendRecordRepo.GetClassAttendRecordListByMemberIdAndClassId(
                memberId, members005Request.getPrev().getClassId());

        if (oldClassAttendRecordList.size() != 0) {

            // 取得轉入班別上課紀錄
            List<ClassAttendRecord> newClassAttendRecordList = classAttendRecordRepo.GetClassAttendRecordListByMemberIdAndClassId(
                    memberId, members005Request.getNext().getClassId());

            int oldRecordSize = oldClassAttendRecordList.size();
            int newRecordSize = newClassAttendRecordList.size();
            int size = 0;

            if (oldRecordSize - newRecordSize == 0) {
                size = oldRecordSize;
            } else {
                if (oldRecordSize - newRecordSize > 0) {
                    size = newRecordSize;
                } else {
                    size = oldRecordSize;
                }
            }

            // 將原上課紀錄按週次，寫入轉入班別上課紀錄
            for (int x = 0; x < size; x++) {

                newClassAttendRecordList.get(x).setAttendMark(oldClassAttendRecordList.get(x).getAttendMark());
                newClassAttendRecordList.get(x).setAttendCheckinDtTm(oldClassAttendRecordList.get(x).getAttendCheckinDtTm());

                classAttendRecordRepo.save(newClassAttendRecordList.get(x));
            }

            // 刪除原上課紀錄
            classAttendRecordRepo.DeleteClassAttendRecordByMemberIdAndClassId(memberId, members005Request.getPrev().getClassId());
        }

        return "success classEnrollFormId : " + classEnrollFormRepo.save(classEnrollForm).getClassEnrollFormId();
    }

    // 取得學員的報名表資訊, 如無報名表，errCode回傳錯誤即可
    public Members007Response getClassEnrollForm(String memberId, String classId) {

        ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(memberId, classId);

        if (classEnrollForm == null) {

            return new Members007Response(
                    400,
                    "未報名",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    false);
        } else {

            return new Members007Response(
                    200,
                    "已報名",
                    classEnrollForm.getClassEnrollFormId(),
                    Null2Empty(classEnrollForm.getClassGroupId()),
                    Null2Empty(classEnrollForm.getSameGroupMemberName()),
                    Null2Empty(classEnrollForm.getCurrentClassIntroducerName()),
                    Null2Empty(classEnrollForm.getCurrentClassIntroducerRelationship()),
                    Null2Empty(classEnrollForm.getChildContactPersonName()),
                    Null2Empty(classEnrollForm.getChildContactPersonPhoneNum()),
                    classEnrollForm.getIsDroppedClass());
        }
    }

    // generate class enroll form id
    public String classEnrollFormIdGenerator() {
        String criteria = "CEF" + CommonUtils.GetMinGoYearStr();

        String currentMaxValue = classEnrollFormRepo.GetMaxAssetId(criteria + '%');

        long tempId = 0;

        if (currentMaxValue != null) {
            tempId = Long.parseLong(currentMaxValue.substring(6));
        }

        tempId++;

        return criteria + String.format("%05d", tempId);
    }

    // generate member group number
    public String memberGroupNumGenerator(String classId, String classGroupId) {

        String memberGroupNum = classEnrollFormRepo.GetMaxMemberGroupNum(classId, classGroupId);

        if (memberGroupNum == null) {
            memberGroupNum = "2";
        } else {
            if (Integer.parseInt(memberGroupNum) < 2) {
                memberGroupNum = "2";
            } else {
                memberGroupNum = String.valueOf(Integer.parseInt(memberGroupNum) + 1);
            }
        }

        return memberGroupNum;
    }

    // get the groupId available memberGroupNum list
    public List<String> availableMemberGroupNumListGenerator(String classId, String classGroupId) {

        List<String> availableMemberGroupNumList = new ArrayList<>();

        int maxMemberGroupNum = Integer.parseInt(memberGroupNumGenerator(classId, classGroupId));

        if (maxMemberGroupNum == 2) {

            availableMemberGroupNumList.add(String.valueOf(maxMemberGroupNum));
        } else {

            List<String> memberGroupNumList = classEnrollFormRepo.GetMemberGroupNumList(classId, classGroupId);

            for (int x = 2; x <= maxMemberGroupNum; x++) {

                if (!memberGroupNumList.contains(String.valueOf(x))) {

                    availableMemberGroupNumList.add(String.valueOf(x));
                }
            }
        }

        return availableMemberGroupNumList;
    }

    // get local date
    public Date getLocalDate() {

        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date newReturnChangeDate = calendar.getTime();

        return newReturnChangeDate;
    }

    // 本班註記 - 依單位編號，課程狀態找跨班學生
    public List<MembersObject> getSpanMembersByClassStatusUnitId(String unitId, String classStatus) {

        return classEnrollFormRepo.GetSpanClassMemberIdsByClassStatusUnitId(unitId, classStatus);
    }

    // 本班註記 - 設定本班
    public void updateSpanMember(String unitId, SpanMembersPostRequest request) throws NotFoundException {
        logger.debug(request.getMemberId() + "/" + request.getClassPeriodNum() + "/" + request.getClassName() + "/" + request.getClassId());

        List<ClassInfo> classInfos = classInfoRepo.GetClassInfoByUnitIdClassIdClassPeriodNum(unitId, request.getClassId(), request.getClassPeriodNum());

        if (classInfos != null && classInfos.size() > 0) {

            ClassInfo classInfo = classInfos.get(0);

            List<ClassEnrollForm> classEnrollFormList = classEnrollFormRepo.GetSpanClassByMemberIdClassPeriodNumClassName(
                    request.getMemberId(),
                    request.getClassPeriodNum());

            if (classEnrollFormList != null && classEnrollFormList.size() > 0) {

                for (ClassEnrollForm classEnrollForm : classEnrollFormList) {

                    if (classEnrollForm.getClassId().equals(classInfo.getClassId())) {

                        classEnrollForm.setIsHomeClass(true);
                    } else {

                        classEnrollForm.setIsHomeClass(false);
                    }

                    classEnrollForm.setHomeClassId(classInfo.getClassId());
                    classEnrollForm.setHomeClassName(classInfo.getClassName());
                    classEnrollForm.setHomeClassPeriodNum(classInfo.getClassPeriodNum());
                    classEnrollForm.setHomeClassNote(classInfo.getClassName() + "-" + classInfo.getClassPeriodNum() + "期");
                }

                classEnrollFormRepo.saveAll(classEnrollFormList);
            } else {
                throw new NotFoundException("查無報名表");
            }
        } else {
            throw new NotFoundException("查無班別");
        }
    }

    // 資料查詢 - 證件檢核 - 列表
    public List<MembersObject> getMembersByUnitIdNewestClassPeriod(String unitId) {

        // get opening class ids
        List<String> openingClassIds = classInfoRepo.GetOpeningClassIdsByUnitId(unitId);

        if (openingClassIds.size() > 0) {
            // get enroll forms of opening classes
            List<MembersObject> forms = classEnrollFormRepo.GetUniqueMemberEnrollFormByUnitIdClassIds(unitId, openingClassIds);

            if (forms != null && forms.size() > 0) {

                // get closed class ids
                List<String> closedClassIds = classInfoRepo.GetClosedClassIdsByUnitId(unitId);

                // get member ids for checking old/new student
                List<String> distinctMemberIds = forms.stream().filter(distinctByKey(MembersObject::getMemberId)).map(e -> e.getMemberId()).collect(Collectors.toList());

                // get member ids attended class before
                List<String> oldMembers = new ArrayList<>();

                if (closedClassIds.size() > 0 && distinctMemberIds.size() > 0) {
                    oldMembers = classEnrollFormRepo.GetMemberEnrollFormByUnitIdClassIdsMemberIds(unitId, closedClassIds, distinctMemberIds);
                }

                List<String> memberIds = forms.stream().map(f -> f.getMemberId()).distinct().collect(Collectors.toList());

                List<Members002ImagesObject> photos = ctMemberInfoService.getPhotosByMemberIdList(memberIds, false);

                for (MembersObject form : forms) {

                    Optional<Members002ImagesObject> photo = photos.stream().filter(p -> p.getMemberId().equals(form.getMemberId())).findAny();

                    if (photo.isPresent()) {

                        if (CommonUtils.Empty2Null(photo.get().getPhoto()) != null) {
                            form.setNotPhoto(false);
                        } else {
                            form.setNotPhoto(true);
                        }

                        if (CommonUtils.Empty2Null(photo.get().getId1()) != null && CommonUtils.Empty2Null(photo.get().getId2()) != null) {
                            form.setNotTwId(false);
                        } else {
                            form.setNotTwId(true);
                        }

                        if (CommonUtils.Empty2Null(photo.get().getData()) != null) {
                            form.setNotPID(false);
                        } else {
                            form.setNotPID(true);
                        }

                        if (CommonUtils.Empty2Null(photo.get().getAgree()) != null) {
                            form.setNotPConsent(false);
                        } else {
                            form.setNotPConsent(true);
                        }
                    }

                    if (form.getAge() > 12) {
                        form.setNotPConsent(false);
                    }

                    if (oldMembers.contains(form.getMemberId())) {
                        form.setNew(false);
                    } else {
                        form.setNew(true);
                    }
                }
            }

            return forms;
        } else {
            return new ArrayList<>();
        }
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    // 資料查詢 - 班別小計
    public List<DataSearchClassSubTotalResponse.ClassSubTotal> getClassSubTotal(String unitId) {

        // all opening classes
        List<DataSearchClassInfo> classes = classInfoRepo.GetClassInfoByUnitIdClassStatus(unitId, ClassInfo.Status.Opening.getValue());

        if (classes.size() > 0) {
            // get class ids
            List<String> classIds = classes.stream().map(DataSearchClassInfo::getClassId).collect(Collectors.toList());

            // get all enrolled of class ids
            List<DataSearchClassEnrollForm> forms = classEnrollFormRepo.GetClassEnrollFormByClassIds(classIds);

            List<DataSearchClassSubTotalResponse.ClassSubTotal> items = new ArrayList<>();

            for (DataSearchClassInfo iClass : classes) {
                DataSearchClassSubTotalResponse.ClassSubTotal item = new DataSearchClassSubTotalResponse.ClassSubTotal();
                item.setClassName(iClass.getClassName());

                List<DataSearchClassEnrollForm> males = forms.stream().filter(p -> iClass.getClassId().equals(p.getClassId()) && "男".equals(p.getGender())).collect(Collectors.toList());
                List<DataSearchClassEnrollForm> females = forms.stream().filter(p -> iClass.getClassId().equals(p.getClassId()) && "女".equals(p.getGender())).collect(Collectors.toList());

                int leaderMNumber = (int) males.stream().filter(p -> p.getClassJobTitleList() != null && p.getClassJobTitleList().contains("學員長")).count();
                item.setMemberMNumber(males.size() - leaderMNumber);
                item.setLeaderMNumber(leaderMNumber);

                int leaderFNumber = (int) females.stream().filter(p -> p.getClassJobTitleList() != null && p.getClassJobTitleList().contains("學員長")).count();
                item.setMemberFNumber(females.size() - leaderFNumber);
                item.setLeaderFNumber(leaderFNumber);

                items.add(item);
            }

            return items;
        } else {
            return new ArrayList<>();
        }
    }

    // 資料查詢 - 組別小計
    public List<DataSearchGroupSubTotalResponse.ClassSubTotal> getGroupSubTotal(String unitId) {

        // all opening classes
        List<DataSearchClassInfo> classes = classInfoRepo.GetClassInfoByUnitIdClassStatus(unitId, ClassInfo.Status.Opening.getValue());

        if (classes.size() > 0) {
            // get class ids
            List<String> classIds = classes.stream().map(DataSearchClassInfo::getClassId).collect(Collectors.toList());

            // get all enrolled of class ids
            List<DataSearchClassEnrollForm> forms = classEnrollFormRepo.GetClassEnrollFormByClassIds(classIds);

            List<DataSearchGroupSubTotalResponse.ClassSubTotal> items = new ArrayList<>();

            // fix null values in group id, replace null with empty string
            List<DataSearchClassEnrollForm> emptyGroupIds = forms.stream().filter(f -> f.getClassGroupId() == null).collect(Collectors.toList());

            for (DataSearchClassEnrollForm item : emptyGroupIds) {
                item.setClassGroupId("");
            }

            // each class
            for (DataSearchClassInfo iClass : classes) {
                // get males/females for a class
                DataSearchGroupSubTotalResponse.ClassSubTotal itemM = new DataSearchGroupSubTotalResponse.ClassSubTotal();
                DataSearchGroupSubTotalResponse.ClassSubTotal itemF = new DataSearchGroupSubTotalResponse.ClassSubTotal();

                List<DataSearchClassEnrollForm> males = forms.stream().filter(p -> iClass.getClassId().equals(p.getClassId()) && "男".equals(p.getGender())).collect(Collectors.toList());
                List<DataSearchClassEnrollForm> females = forms.stream().filter(p -> iClass.getClassId().equals(p.getClassId()) && "女".equals(p.getGender())).collect(Collectors.toList());

                itemM.setClassName(iClass.getClassName());
                itemM.setGender("男");
                itemF.setClassName(iClass.getClassName());
                itemF.setGender("女");

                // get groups and counts
                Map<String, Long> countedM = males.stream().collect(Collectors.groupingBy(x -> x.getClassGroupId(), Collectors.counting()));

                countedM = new TreeMap<>(countedM);

                List<DataSearchGroupSubTotalResponse.GroupSubTotal> groupsM = new ArrayList<>();

                for (Map.Entry<String, Long> entry : countedM.entrySet()) {
                    groupsM.add(new DataSearchGroupSubTotalResponse.GroupSubTotal(entry.getKey(), entry.getValue().intValue()));
                }

                groupsM = groupsM.stream().sorted((t1, t2) -> compareByGroupId(t1, t2)).collect(Collectors.toList());

                itemM.setGroups(groupsM);

                Map<String, Long> countedF = females.stream().collect(Collectors.groupingBy(x -> x.getClassGroupId(), Collectors.counting()));

                countedF = new TreeMap<>(countedF);

                List<DataSearchGroupSubTotalResponse.GroupSubTotal> groupsF = new ArrayList<>();

                for (Map.Entry<String, Long> entry : countedF.entrySet()) {
                    groupsF.add(new DataSearchGroupSubTotalResponse.GroupSubTotal(entry.getKey(), entry.getValue().intValue()));
                }

                groupsF = groupsF.stream().sorted((t1, t2) -> compareByGroupId(t1, t2)).collect(Collectors.toList());

                itemF.setGroups(groupsF);

                items.add(itemM);
                items.add(itemF);
            }

            return items;
        } else {
            return new ArrayList<>();
        }
    }

    private int compareByGroupId(DataSearchGroupSubTotalResponse.GroupSubTotal t1, DataSearchGroupSubTotalResponse.GroupSubTotal t2) {
        int it1 = Integer.parseInt(t1.getGroupId());
        int it2 = Integer.parseInt(t2.getGroupId());
        if (it1 == it2) {
            return 0;
        } else {
            return it1 > it2 ? 1 : -1;
        }
    }

    // 資料查詢 - 各期人數統計
    public List<DataSearchHistoryNumber.HistoryPeriod> getCountByPeriod(String unitId) {

        List<DataSearchHistoryNumber.HistoryPeriod> result = new ArrayList<>();

        // get all class periods without classStatus = '未啟動'&'可報名'
        List<String> periods = classInfoRepo.GetClassPeriods(unitId);

        for (String period : periods) {
            logger.debug("period# " + period);
            // get all class name and member count
            List<DataSearchHistoryClass> iClass = classEnrollFormRepo.GetClassMemberCountByUnitPeriod(unitId, period);

            DataSearchHistoryNumber.HistoryPeriod iPeriod = new DataSearchHistoryNumber.HistoryPeriod(Integer.parseInt(period), iClass);
            result.add(iPeriod);
        }

        return result;
    }

    // 資料查詢 - 當期學員長名冊
    public List<MembersObject> getLeaders(String unitId, String classStatus, String classPeriodNum, String className, String classGroupId) {

        List<MembersObject> result;

        // empty to null
        if (classPeriodNum != null && classPeriodNum.isEmpty()) classPeriodNum = null;
        if (className != null && className.isEmpty()) className = null;
        if (classGroupId != null && classGroupId.isEmpty()) classGroupId = null;

        if (unitId != null && !unitId.isEmpty() && classStatus != null && !classStatus.isEmpty()) {
            List<String> classIds = classInfoRepo.GetClassInfoByUnitIdClassStatus(unitId, classStatus).stream().map(DataSearchClassInfo::getClassId).collect(Collectors.toList());

            logger.debug("# classes found: " + classIds.size());
            if (classIds.size() > 0) {
                logger.debug("unitId : " + unitId);
                logger.debug("classPeriodNum : " + classPeriodNum);
                logger.debug("className : " + className);
                logger.debug("classGroupId : " + classGroupId);
                result = classEnrollFormRepo.GetLeaders(unitId, classIds, classPeriodNum, className, classGroupId);
            } else {
                result = new ArrayList<>();
            }
        } else {
            result = new ArrayList<>();
        }

        return result;
    }

    // 資料查詢 - 跨班學員及學員長
    public List<DataSearchSpanNumberResponse.SpanNumbers> getSpanMemberCounts(String unitId) {

        String classStatus = ClassInfo.Status.Opening.getValue();

        // class count for max span
        int classNum = classInfoService.getClassCountByUnitIdClassStatus(unitId, classStatus).intValue();

        List<DataSearchSpanNumberResponse.SpanNumbers> result = new ArrayList<>();

        // 1 to max span classes
        for (int i = 1; i <= classNum; i++) {
            // i span class(es) members
            List<MembersObject> members = classEnrollFormRepo.GetSpanClassMemberList(unitId, classStatus, Long.valueOf(i));

            List<MembersObject> uniqueMembers = members.stream()
                    .filter(distinctByKey(m -> m.getMemberId())).collect(Collectors.toList());

            List<MembersObject> maleMembers = uniqueMembers.stream()
                    .filter(m -> m.getGender().equals(CtMemberInfo.Gender.Male.getValue()))
                    .collect(Collectors.toList());

            List<MembersObject> femaleMembers = uniqueMembers.stream()
                    .filter(m -> m.getGender().equals(CtMemberInfo.Gender.Female.getValue()))
                    .collect(Collectors.toList());

            List<MembersObject> uniqueLeaders = members.stream()
                    .filter(m -> m.getClassJobTitleList().contains("學員長"))
                    .filter(distinctByKey(m -> m.getMemberId()))
                    .collect(Collectors.toList());

            List<MembersObject> maleLeaders = uniqueLeaders.stream()
                    .filter(m -> m.getGender().equals(CtMemberInfo.Gender.Male.getValue()))
                    .collect(Collectors.toList());

            List<MembersObject> femaleLeaders = uniqueLeaders.stream()
                    .filter(m -> m.getGender().equals(CtMemberInfo.Gender.Female.getValue()))
                    .collect(Collectors.toList());

            DataSearchSpanNumberResponse.SpanNumbers item = new DataSearchSpanNumberResponse.SpanNumbers(
                    i,
                    maleMembers.size(),
                    femaleMembers.size(),
                    maleLeaders.size(),
                    femaleLeaders.size());

            result.add(item);
        }

        return result;
    }

    // 依取得學員的上課記錄
    public ClassRecord003Response classRecord003(String classId) {

        logger.debug("classId:" + classId);

        List<ClassRecord003Response.ClassRecord003_1Object> items = new ArrayList<>();

        List<ClassRecord003_1Object> forms = classEnrollFormRepo.GetClassEnrollFormAndAttendRecordByClassId(classId);

        List<ClassRecord003_2Object> records = classAttendRecordRepo.GetClassAttendRecordListByClassId(classId);

        for (ClassRecord003_1Object form : forms) {

            List<ClassRecord003Response.ClassRecord003_2Object> attendRecordList = new ArrayList<>();

//            List<ClassRecord003_2Object> records = classAttendRecordRepo
//                    .GetClassAttendRecordByMemberIdAndClassId(form.getMemberId(), classId);

            List<ClassRecord003_2Object> attends = records.stream().filter(r -> r.getMemberId().equals(form.getMemberId())).collect(Collectors.toList());

            for (ClassRecord003_2Object attend : attends) {

                attendRecordList.add(new ClassRecord003Response.ClassRecord003_2Object(
                        attend.getClassWeeksNum(),
                        attend.getClassDate().toString().split(" ")[0],
                        Null2Empty(attend.getAttendMark())
                ));
            }

            items.add(new ClassRecord003Response.ClassRecord003_1Object(
                    form.getMemberId(),
                    form.getAliasName(),
                    Null2Empty(form.getCtDharmaName()),
                    form.getGender(),
                    form.getClassName(),
                    form.getClassGroupId(),
                    form.getMemberGroupNum(),
                    Null2Empty(form.getClassJobTitleList()),
                    form.getIsDroppedClass(),
                    form.isChangeAndWaitPrintMark(),
                    form.getClassDroppedDate() == null ? "" : form.getClassDroppedDate().toString().split(" ")[0],
                    form.getUserDefinedLabel1(),
                    form.getUserDefinedLabel2(),
                    form.getNewReturnChangeMark(),
                    form.getNewReturnChangeDate() == null ? "" : form.getNewReturnChangeDate().toString().split(" ")[0],
                    Null2Empty(form.getClassErollFormNote()),
                    attendRecordList
            ));
        }

        int errCode = 200;
        String errMsg = "success";

        ClassRecord003Response classRecord003Response = new ClassRecord003Response(
                errCode,
                errMsg,
                items
        );

        return classRecord003Response;
    }

    // sys - import data -
    public ClassEnrollForm getEnrollFormByNameTwIdClassId(String classId, String name, String twIdNum) {
        List<ClassEnrollForm> forms = classEnrollFormRepo.GetEnrollFormByNameTwIdClassId(classId, name, twIdNum);
        if (forms.size() > 0) {
            return forms.get(0);
        } else {
            return null;
        }
    }

    // sys - import data -
    public ClassEnrollForm getEnrollFormByNameBirthdayClassId(String classId, String name, Date birthday) {
        List<ClassEnrollForm> forms = classEnrollFormRepo.GetEnrollFormByNameBirthdayClassId(classId, name, birthday);
        if (forms.size() > 0) {
            return forms.get(0);
        } else {
            return null;
        }
    }

    // class report - list
    public List<ClassMemberRecord> getClassEnrollFormsAttendRecordsByClassId(String classId, boolean includeMember) {

//        long startTime = System.nanoTime();

        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd");
        SimpleDateFormat formatterBirth = new SimpleDateFormat("yyyyMMdd");

        List<ClassMemberRecord> iRecords = new ArrayList<>();

        ClassInfo classInfo = classInfoService.getClassInfoByClassId4Import(classId);

        // all members enrolled
        List<ClassEnrollForm> forms = classEnrollFormRepo.GetClassEnrollFormByClassId(classId);

        // all attend records
        List<ClassAttendRecord> records = classAttendRecordRepo.GetClassAttendRecordByClassId(classId);

        // get fully attend marks of class
//        List<String> fullyAttendMarks = classAttendMarkRepo.GetFullAttendMarkByClassId(classId);
        List<String> fullyAttendMarks = classAttendMarkRepo.GetIsGraduatedList(classId);

//        for(String mark: fullyAttendMarks) {
//            logger.debug("f mark " + mark);
//        }

        List<String> memberIds = forms.stream().map(ClassEnrollForm::getMemberId).collect(Collectors.toList());

        if (memberIds.size() > 0) {

            List<CtMemberInfo> members = null;

            if (includeMember) {
                members = ctMemberInfoRepo.GetMemberInfoByMemberIds(memberIds);
            }

            List<Members002ImagesObject> photos = ctMemberInfoService.getPhotosByMemberIdList(memberIds, true);

            for (ClassEnrollForm form : forms) {

                ClassMemberRecord iRecord = new ClassMemberRecord();

                Optional<Members002ImagesObject> photo = photos.stream().filter(p -> p.getMemberId().equals(form.getMemberId())).findAny();

                if (photo.isPresent()) {
                    iRecord.setPhoto(photo.get().getPhoto());
                }

                iRecord.setMemberId(form.getMemberId());
                iRecord.setGender(form.getGender());
                iRecord.setAliasName(form.getAliasName());
                iRecord.setCtDharmaName(form.getCtDharmaName());
                iRecord.setClassPeriodNum(Integer.parseInt(form.getClassPeriodNum()));
                iRecord.setClassName(form.getClassName());
                iRecord.setClassGroupId(form.getClassGroupId());
                iRecord.setMemberGroupNum(form.getMemberGroupNum());
                iRecord.setAddress(form.getAddress());
                iRecord.setCompanyNameAndJobTitle(form.getCompanyNameAndJobTitle());
                iRecord.setSchoolNameAndMajor(form.getSchoolNameAndMajor());

                List<ClassAttendance> attendances = new ArrayList<>();
                iRecord.setAttendRecordList(attendances);
                List<ClassAttendRecord> attends = records.stream().filter(r -> r.getMemberId().equals(form.getMemberId())).collect(Collectors.toList());

                int fullyAttendCount = 0;
                int shouldAttendCount = 0;

                if (attends.size() > 0) {
                    for (ClassAttendRecord attend : attends) {
                        ClassAttendance attendance = new ClassAttendance(
                                String.valueOf(attend.getClassWeeksNum()),
                                formatter.format(attend.getClassDate()),
                                attend.getAttendMark());

                        attendances.add(attendance);

                        if (attend.getClassDate().compareTo(Calendar.getInstance().getTime()) <= 0) {
                            shouldAttendCount ++;

                            if (fullyAttendMarks.contains(attend.getAttendMark())) {
                                fullyAttendCount++;
                            }
                        }
                    }
                }

                iRecord.setDroppedClass(form.getIsDroppedClass());
                iRecord.setWeekTotal(shouldAttendCount);
                iRecord.setWeek(fullyAttendCount);

                int percent = shouldAttendCount == 0 ? 0 : fullyAttendCount * 100 / shouldAttendCount;

                iRecord.setPercent(percent <= 100 ? percent : 100);

                iRecord.setPreviousClassName(form.getPreviousClassName());
                iRecord.setBirthDate(formatterBirth.format(form.getBirthDate()));
                iRecord.setTwIdNum(form.getTwIdNum());
                iRecord.setDsaJobNameList(form.getDsaJobNameList());
                iRecord.setHomePhoneNum1(form.getHomePhoneNum1());
                iRecord.setCurrentClassIntroducerName(form.getCurrentClassIntroducerName());
                iRecord.setCurrentClassIntroducerRelationship(form.getCurrentClassIntroducerRelationship());
                iRecord.setClassEnrollFormNote(form.getClassEnrollFormNote());
                iRecord.setChangeAndWaitPrintMark(form.getChangeAndWaitPrintMark());
                iRecord.setClassTypeName(form.getClassTypeName());
                iRecord.setClassJobTitleList(form.getClassJobTitleList());
                iRecord.setMobileNum1(form.getMobileNum1());
                iRecord.setAge(form.getAge());

                iRecord.setDayOfWeek(classInfo.getDayOfWeek());
                iRecord.setDayOrNight(classInfo.getClassDayOrNight());

                iRecord.setFullAttended(form.getIsFullAttended());
                iRecord.setGraduated(form.getIsGraduated());
                iRecord.setHomeClass(form.getIsHomeClass());
                iRecord.setHomeClassNote(form.getHomeClassNote());

                if (includeMember) {
                    List<CtMemberInfo> infos = members.stream().filter(m -> m.getMemberId().equals(form.getMemberId())).collect(Collectors.toList());
                    if (infos.size() > 0) {
                        iRecord.setMemberInfo(infos.get(0));
                    }
                }

                iRecords.add(iRecord);
            }
        }

//        long endTime   = System.nanoTime();
//        long totalTime = endTime - startTime;
//        logger.debug("getClassEnrollFormsAttendRecordsByClassId run time " + totalTime);

        return iRecords;
    }

    // class - report - clear print mark
    public void setPrintMark(String classId, List<String> memberIds, boolean onOff) {
        classEnrollFormRepo.SetPrintMark(classId, memberIds, onOff);
    }

    public ClassEnrollForm save(ClassEnrollForm form) {
        return classEnrollFormRepo.save(form);
    }

    public List<ClassEnrollForm> save(List<ClassEnrollForm> forms) {
        return classEnrollFormRepo.saveAll(forms);
    }

    // 依取得學員的修業統計
    public List<ClassStats003Response.ClassStats003Object> getClassStats003List(HttpSession session, String classId,
                                                                                String startDate, String endDate) {
        long startTime = System.nanoTime();

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<ClassStats003Response.ClassStats003Object> classStats003ObjectList = new ArrayList<>();

        List<ClassStats003_1Object> classStats003_1ObjectList =
                classEnrollFormRepo.GetEnrollFormByClassIdAndUnitId(classId, unitId);

        Date date = java.sql.Date.valueOf(endDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        if (classStats003_1ObjectList.size() != 0) {

            List<String> isGraduatedList = classAttendMarkRepo.GetIsGraduatedList(classId);
            List<String> isMakeupClassList = classAttendMarkRepo.GetIsMakeupClassList(classId);

            ClassStats003_2Object classInfo = classInfoRepo.GetClassStats003_2ObjectByClassId(classId);

            List<ClassAttendRecord> allClassAttendRecordList =
                    classAttendRecordRepo.GetClassAttendRecordListByClassIdAndEndDate(classId, calendar.getTime());

//            List<String> graduatedMemberIds = new ArrayList<>();
//            List<String> stayMemberIds = new ArrayList<>();

            for (ClassStats003_1Object classStats003_1Object : classStats003_1ObjectList) {
                
                boolean calcFullAttended = false; // 電腦計算全勤
                boolean isFullAttended = classStats003_1Object.getIsFullAttended(); // 最後是否全勤
                boolean calcGraduated = false; // 電腦計算結業
                boolean isGraduated = classStats003_1Object.getIsGraduated(); // 最後是否結業
                boolean isMakeupToFullAttended = false; // 補後全勤
                String alertFullAttended = ""; // 補全勤提醒
                String alertGraduated = ""; // 補結業提醒
                boolean isDiligentAward = false; // 精進獎
                String calcClassAttendedResult = ""; // 電腦計算修業狀況
                String classAttendedResult = Null2Empty(classStats003_1Object.getClassAttendedResult()); // 實際修業狀況

                int total = 0;          // 應上週數
                int attended = 0;       // 實上週數
                int absent = 0;         // 未上
                int absentAllowedFull = classInfo.getAbsenceCountToFullAttend(); // 容許缺課全勤
                int absentAllowed = classInfo.getAbsenceCountToGraduate();  // 容許缺課畢業
                int makeupAllowed = classInfo.getMakeupCountToGraduate();  // 容許補課畢業
                int absentAllowedAward = classInfo.getAbsenceCountToDiligentAward();
//                int makeupAllowedAward = classInfo.getMakeupCountToDiligentAward();
                int minMakeupCountToDiligentAward = classInfo.getMinMakeupCountToDiligentAward();
                int maxMakeupCountToDiligentAward = classInfo.getMaxMakeupCountToDiligentAward();
                int makeup = 0;         // 補課

                List<ClassAttendRecord> classAttendRecordList = allClassAttendRecordList.stream().filter(r -> r.getMemberId().equals(classStats003_1Object.getMemberId())).collect(Collectors.toList());

                for (ClassAttendRecord classAttendRecord : classAttendRecordList) {
                    // 應上週數
                    if (classAttendRecord.getClassDate().compareTo(Calendar.getInstance().getTime()) <= 0) {
                        total++;
                        // 實上週數++
                        if (isGraduatedList.contains(classAttendRecord.getAttendMark())) {
                            attended++;
                        }
                    }
                }

                absent = total - attended;

                makeup = (int)classAttendRecordList.stream().filter(r -> isMakeupClassList.contains(r.getAttendMark())).count();

                // 電腦計算修業狀況
                if (makeup > makeupAllowed
                        || absent > (absentAllowed + makeupAllowed)
                        || makeup + (absent - absentAllowed) > makeupAllowed) {
                    calcClassAttendedResult = "留原班";
                }

//                // 電腦計算全勤
//                if (absent == 0 && makeup == 0) {
//                    calcFullAttended = true;
//                }
//
//                // 電腦計算全勤、最後是否全勤、補後全勤、電腦計算修業狀況
//                if (absent == 0 && makeup <= absentAllowedFull) {
//                    calcFullAttended = true;
//                    isMakeupToFullAttended = true;
//                    calcClassAttendedResult = "全勤";
//                }

                // 補全勤提醒、電腦計算修業狀況
                if (absent > 0 && (absent + makeup) <= absentAllowedFull) {
                    alertFullAttended = "補" + absent + "可全勤";
                    calcClassAttendedResult = "補" + absent + "可全勤";
                }

                // 補結業提醒、電腦計算修業狀況
                if (absent > absentAllowed
                        && makeup + (absent - absentAllowed) <= makeupAllowed) {
                    alertGraduated = "補" + (absent - absentAllowed) + "可結業";
                    calcClassAttendedResult = "補" + (absent - absentAllowed) + "可結業";
                }

                // 精進獎
//                logger.debug(classStats003_1Object.getMemberId() + ":" + absent + "/" + absentAllowedAward + "/" + makeup + "/" + minMakeupCountToDiligentAward + "/" + maxMakeupCountToDiligentAward);
                if (absent <= absentAllowedAward
                        && makeup >= minMakeupCountToDiligentAward
                        && makeup <= maxMakeupCountToDiligentAward) {
                    isDiligentAward = true;
                    calcClassAttendedResult = "精進獎";
                }

                // 電腦計算結業、電腦計算修業狀況
                if (absent <= absentAllowed
                        && makeup <= makeupAllowed) {
                    calcGraduated = true;
                    calcClassAttendedResult = "結業";
                }

                // 電腦計算全勤
                if (absent == 0) {
                    if (makeup == 0) {
                        calcFullAttended = true;
                        calcClassAttendedResult = "全勤";
                    } else if (makeup <= absentAllowedFull) {
                        isMakeupToFullAttended = true;
                        calcFullAttended = true;
                        calcClassAttendedResult = "全勤";
                    }
                }

                classStats003ObjectList.add(new ClassStats003Response.ClassStats003Object(
                        classStats003_1Object.getClassEnrollFormId(),
                        classStats003_1Object.getMemberId(),
                        classStats003_1Object.getAliasName(),
                        Null2Empty(classStats003_1Object.getCtDharmaName()),
                        classStats003_1Object.getGender(),
                        classStats003_1Object.getClassPeriodNum(),
                        classStats003_1Object.getClassName(),
                        classStats003_1Object.getClassGroupId(),
                        classStats003_1Object.getMemberGroupNum(),
                        Null2Empty(classStats003_1Object.getClassJobTitleList()),
                        total,
                        attended,
                        calcFullAttended,
                        isFullAttended,
                        calcGraduated,
                        isGraduated,
                        isMakeupToFullAttended,
                        alertFullAttended,
                        alertGraduated,
                        isDiligentAward,
                        calcClassAttendedResult,
                        classAttendedResult,
                        classStats003_1Object.getAge()
                ));

//                logger.debug(classStats003_1Object.getAliasName() + "/" + makeup + "/" + makeupAllowed);
            }

//            if (stayMemberIds.size() > 0) {
//                classEnrollFormRepo.UpdateIsStayInOriginalClass(classId, stayMemberIds);
//            }
//
//            if (graduatedMemberIds.size() > 0) {
//                classEnrollFormRepo.UpdateIsGraduated(classId, graduatedMemberIds);
//            }
        }

        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        logger.debug("getClassStats003List run time " + totalTime);

        return classStats003ObjectList;
    }

    // 更新學員的修業狀態
    public String updateClassEnrollForm(HttpSession session, String classId, ClassStats004Request classStats004Request) {

        if (classStats004Request.getMemberList().size() > 0) {

            for (ClassStats004Request.ClassStats004Object classStats004Object : classStats004Request.getMemberList()) {

                ClassEnrollForm classEnrollForm = classEnrollFormRepo
                        .GetClassEnrollFormByClassEnrollFormId(classStats004Object.getClassEnrollFormId());

                classEnrollForm.setIsFullAttended(classStats004Object.getIsFullAttended());
                classEnrollForm.setIsGraduated(classStats004Object.getIsGraduated());

                if (classStats004Request.getIsOverWrite()) {
                    classEnrollForm.setClassAttendedResult(classStats004Object.getClassAttendedResult());
                }else {
                    if (classEnrollForm.getClassAttendedResult().isEmpty()) {
                        classEnrollForm.setClassAttendedResult(classStats004Object.getClassAttendedResult());
                    }
                }
                classEnrollForm.setUpdater(session);

                classEnrollFormRepo.save(classEnrollForm);
            }
        }

        return "success";
    }

    // 匯出證書套印資料，勾選以及全勤結業由前端傳送id列表過去
    public List<ClassStats005Object> excelCert(String ids) {

        String[] idArray = ids.split("\\,");

        List<String> idList = new ArrayList<>();

        for (String id : idArray) {

            idList.add(id);
        }

        List<ClassStats005Object> classStats005ObjectList = classEnrollFormRepo.GetExcelCertListByIds(idList);

        return classStats005ObjectList;
    }

    // 匯出統計表
    public List<ClassStats006Object> excelStats(HttpSession session, String classId,
                                                String startDate, String endDate) {

        List<ClassStats006Object> classStats006ObjectList = new ArrayList<>();

        List<ClassStats003Response.ClassStats003Object> classStats003ObjectList =
                getClassStats003List(session, classId, startDate, endDate);

        if (classStats003ObjectList.size() != 0) {

            for (ClassStats003Response.ClassStats003Object classStats003Object : classStats003ObjectList) {

                classStats006ObjectList.add(new ClassStats006Object(
                        classStats003Object.getMemberId(),
                        classStats003Object.getClassJobTitleList(),
                        classStats003Object.getGender(),
                        classStats003Object.getAliasName(),
                        classStats003Object.getCtDharmaName(),
                        classStats003Object.getAge(),
                        classStats003Object.getClassPeriodNum(),
                        classStats003Object.getClassName(),
                        classStats003Object.getClassGroupId(),
                        classStats003Object.getWeekTotal(),
                        classStats003Object.getWeek(),
                        classStats003Object.getIsFullAttended(),
                        classStats003Object.getIsGraduated(),
                        classStats003Object.getIsMakeupToFullAttended(),
                        classStats003Object.getClassAttendedResult(),
                        ""
                ));
            }
        }

        return classStats006ObjectList;
    }

    // 匯出資料組
    public List<ClassStats007Object> excelData(String classId) {

        logger.debug("classId " + classId);

        List<ClassStats007Object> classStats007ObjectList =
                classEnrollFormRepo.GetExcelDataListByClassId(classId);

        return classStats007ObjectList;
    }

    // 匯出補課名單
    public List<ClassStats008Response.ClassStats008_1Object> excelMakeUpClasses(String classId, String startDate,
                                                                                String endDate) {

        List<ClassStats008Response.ClassStats008_1Object> items = new ArrayList<>();

        List<ClassStats008_1Object> classStats008_1ObjectList = classEnrollFormRepo.GetExcelMakeUpClassesListByClassId(classId);

        if (classStats008_1ObjectList.size() != 0) {

            Date date = java.sql.Date.valueOf(endDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);

            for (ClassStats008_1Object classStats008_1Object : classStats008_1ObjectList) {

                List<ClassStats008Response.ClassStats008_2Object> attendRecordList = new ArrayList<>();

                List<ClassStats008_2Object> classStats008_2ObjectList =
                        classAttendRecordRepo.GetExcelMakeUpClassesListByClassId(
                                classStats008_1Object.getMemberId(), classId, calendar.getTime());

                if (classStats008_2ObjectList.size() != 0) {

                    for (ClassStats008_2Object classStats008_2Object : classStats008_2ObjectList) {

                        String classDate = classStats008_2Object.getClassDate().toString().split(" ")[0].substring(5, 10).replace("-", "/");
                        String attendMark = classStats008_2Object.getAttendMark();
                        String attendStatus = "";
                        String color = "B";

                        if (attendMark == null) {

                            // 缺課
                            attendStatus = classDate;
                            color = "R";
                        } else {

                            if (attendMark.equals("M")) {

                                // 補課
                                attendStatus = classDate + "-M";
                                color = "B";
                            } else {

                                attendStatus = attendMark;
                                color = "B";
                            }

                        }

                        attendRecordList.add(new ClassStats008Response.ClassStats008_2Object(attendStatus, color));
                    }
                }

                items.add(new ClassStats008Response.ClassStats008_1Object(
                        classStats008_1Object.getMemberId(),
                        classStats008_1Object.getClassPeriodNum(),
                        classStats008_1Object.getClassName(),
                        classStats008_1Object.getGender(),
                        classStats008_1Object.getAliasName(),
                        classStats008_1Object.getCtDharmaName(),
                        classStats008_1Object.getClassGroupId(),
                        classStats008_1Object.getMemberGroupNum(),
                        startDate,
                        endDate,
                        classStats008_1Object.getClassAttendedResult(),
                        classStats008_1Object.getIsFullAttended(),
                        attendRecordList
                ));
            }
        }

        return items;
    }

    // 取得班級續升的學員列表 (左右表格皆取這API), isAttend欄位用來判斷該學員是否有上課記錄
    public List<ClassUpgrade003Response.ClassUpgrade003Object> getClassEnrollFormListByClassId(HttpSession session, String classId) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<ClassUpgrade003Response.ClassUpgrade003Object> items = new ArrayList<>();

        List<ClassUpgrade003Object> classUpgrade003ObjectList =
                classEnrollFormRepo.GetClassEnrollFormListByClassId(classId, unitId);

        if (classUpgrade003ObjectList.size() != 0) {

            for (ClassUpgrade003Object classUpgrade003Object : classUpgrade003ObjectList) {

                // 該學員是否有上課記錄
                boolean isAttend = false;

                List<String> attendList = classAttendRecordRepo
                        .GetAttendMarkListByMemberIdAndClassId(classUpgrade003Object.getMemberId(), classId);

                for (String attend : attendList) {

                    if (attend != null) {

                        isAttend = true;

                        break;
                    }
                }

                items.add(new ClassUpgrade003Response.ClassUpgrade003Object(
                        classUpgrade003Object.getClassEnrollFormId(),
                        classUpgrade003Object.getMemberId(),
                        classUpgrade003Object.getGender(),
                        classUpgrade003Object.getAliasName(),
                        Null2Empty(classUpgrade003Object.getCtDharmaName()),
                        classUpgrade003Object.getClassPeriodNum(),
                        classUpgrade003Object.getClassName(),
                        classUpgrade003Object.getClassGroupId(),
                        classUpgrade003Object.getMemberGroupNum(),
                        Null2Empty(classUpgrade003Object.getClassJobTitleList()),
                        classUpgrade003Object.getIsNotComeBack(),
                        classUpgrade003Object.getIsCompleteNextNewClassEnroll(),
                        classUpgrade003Object.getIsEnrollToNewUpperClass(),
                        classUpgrade003Object.getIsFullAttended(),
                        classUpgrade003Object.getIsGraduated(),
                        Null2Empty(classUpgrade003Object.getPreviousClassName()),
                        Null2Empty(classUpgrade003Object.getPreviousClassGroupName()),
                        Null2Empty(classUpgrade003Object.getNextNewClassName()),
                        Null2Empty(classUpgrade003Object.getNextNewClassNote()),
                        isAttend,
                        Null2Empty(classUpgrade003Object.getClassJobContentList()),
                        classUpgrade003Object.getIsLeadGroup()
                ));
            }
        }

        return items;
    }

    // 左Grid，更新升班備註
    public String updateNextClassNote(HttpSession session, String classId, String memberId, String nextNewClassNote) {

        ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(memberId, classId);

        classEnrollForm.setNextNewClassNote(nextNewClassNote);
        classEnrollForm.setUpdater(session);

        return "success ClassEnrollFormId : " + classEnrollFormRepo.save(classEnrollForm).getClassEnrollFormId();
    }

    // 右Grid、修改
    public BaseResponse updateClassEnrollFormJobTitle(HttpSession session, String classId, String memberId,
                                                      ClassUpgrade005Request classUpgrade005Request) {

        int errCode = 0;
        String errMsg = "";

        ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(memberId, classId);

        String classJobHighestNum = null;

        String classJobTitleList = null;
        if (classUpgrade005Request.getClassJobTitleList().size() != 0) {

            for (int x = 0; x < classUpgrade005Request.getClassJobTitleList().size(); x++) {

                if (x == 0) {
                    classJobTitleList = classUpgrade005Request.getClassJobTitleList().get(x);

                    classJobHighestNum = codeDefRepo.GetClassCodeDefCodeId(
                            "學員長職事", classUpgrade005Request.getClassJobTitleList().get(x));
                } else {
                    classJobTitleList = classJobTitleList + "/" + classUpgrade005Request.getClassJobTitleList().get(x);
                }
            }
        }

        String classJobContentList = null;
        if (classUpgrade005Request.getClassJobContentList().size() != 0) {

            for (int x = 0; x < classUpgrade005Request.getClassJobContentList().size(); x++) {

                if (x == 0) {
                    classJobContentList = classUpgrade005Request.getClassJobContentList().get(x);
                } else {
                    classJobContentList = classJobContentList + "/" + classUpgrade005Request.getClassJobContentList().get(x);
                }
            }
        }

        if (classUpgrade005Request.getClassJobTitleList().size() != 0) {
            // 有執事

            if (classUpgrade005Request.getIsLeadGroup()) {
                // 有帶班
                classEnrollForm.setMemberGroupNum("0");
            } else {
                // 無帶班
                classEnrollForm.setMemberGroupNum("1");
            }

            errCode = 200;
        } else {
            // 無執事

            if (classEnrollForm.getMemberGroupNum().equals("0") || classEnrollForm.getMemberGroupNum().equals("1")) {
                // 原本是擔任執事

                // 取得該組別所有缺號與
                // 最大號+1
                List<String> availableMemberGroupNumList =
                        availableMemberGroupNumListGenerator(classId, classUpgrade005Request.getClassGroupId());

                if (availableMemberGroupNumList.contains(classUpgrade005Request.getMemberGroupNum())) {

                    classEnrollForm.setMemberGroupNum(classUpgrade005Request.getMemberGroupNum());

                    errCode = 200;
                } else {

                    if (classUpgrade005Request.getMemberGroupNum().equals("-1")) {

                        classEnrollForm.setMemberGroupNum(availableMemberGroupNumList.get(0));

                        errCode = 200;
                    } else {

                        errCode = 400;
                        errMsg = "您輸入的組號已存在，目前可用組號為 ";

                        for (int s = 0; s < availableMemberGroupNumList.size(); s++) {

                            if (s == 0) {

                                errMsg = errMsg + availableMemberGroupNumList.get(s);
                            } else {

                                errMsg = errMsg + "," + availableMemberGroupNumList.get(s);
                            }
                        }
                    }
                }
            } else {
                // 原本不是擔任執事

                if (!classEnrollForm.getClassGroupId().equals(classUpgrade005Request.getClassGroupId()) ||
                        !classEnrollForm.getMemberGroupNum().equals(classUpgrade005Request.getMemberGroupNum())) {
                    // 有變更組別或組號

                    // 取得該組別所有缺號與最大號+1
                    List<String> availableMemberGroupNumList =
                            availableMemberGroupNumListGenerator(classId, classUpgrade005Request.getClassGroupId());

                    if (availableMemberGroupNumList.contains(classUpgrade005Request.getMemberGroupNum())) {

                        classEnrollForm.setMemberGroupNum(classUpgrade005Request.getMemberGroupNum());

                        errCode = 200;
                    } else {

                        if (classUpgrade005Request.getMemberGroupNum().equals("-1")) {

                            classEnrollForm.setMemberGroupNum(availableMemberGroupNumList.get(0));

                            errCode = 200;
                        } else {

                            errCode = 400;
                            errMsg = "您輸入的組號已存在，目前可用組號為 ";

                            for (int s = 0; s < availableMemberGroupNumList.size(); s++) {

                                if (s == 0) {

                                    errMsg = errMsg + availableMemberGroupNumList.get(s);
                                } else {

                                    errMsg = errMsg + "," + availableMemberGroupNumList.get(s);
                                }
                            }
                        }
                    }
                } else {

                    errCode = 200;
                }
            }
        }

        classEnrollForm.setClassGroupId(Empty2Null(classUpgrade005Request.getClassGroupId()));
        classEnrollForm.setClassJobHighestNum(classJobHighestNum);
        classEnrollForm.setClassJobTitleList(classJobTitleList);
        classEnrollForm.setClassJobContentList(classJobContentList);
        classEnrollForm.setIsLeadGroup(classUpgrade005Request.getClassJobTitleList().size() == 0 ? false : classUpgrade005Request.getIsLeadGroup());
        classEnrollForm.setUpdater(session);

        BaseResponse baseResponse = null;

        switch (errCode) {
            case 200:

                baseResponse = new BaseResponse(200, "success ClassEnrollFormId : " + classEnrollFormRepo.save(classEnrollForm).getClassEnrollFormId());
                break;
            case 400:

                baseResponse = new BaseResponse(400, errMsg);
                break;
        }

        return baseResponse;
    }

    // 設定下期不再上襌修班
    public String setMemberSetNotBack(HttpSession session, String classId, ClassUpgrade006Request classUpgrade006Request) {

        if (classUpgrade006Request.getMemberIdList().size() != 0) {

            for (String memberId : classUpgrade006Request.getMemberIdList()) {

                ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(memberId, classId);

                if (classUpgrade006Request.getIsNotComeBack()) {

                    classEnrollForm.setIsNotComeBack(true);
                    classEnrollForm.setIsCompleteNextNewClassEnroll(true);
                } else {

                    classEnrollForm.setIsNotComeBack(false);
                    classEnrollForm.setIsCompleteNextNewClassEnroll(false);
                }

                classEnrollForm.setUpdater(session);

                classEnrollFormRepo.save(classEnrollForm);
            }
        }

        return "success";
    }

    // 班級續升，左邊表格續升
    public String membersUpgrade(HttpSession session, String classId, ClassUpgrade007Request classUpgrade007Request) {

        if (classUpgrade007Request.getMemberIdList().size() != 0) {

            for (String memberId : classUpgrade007Request.getMemberIdList()) {

                String[] classRegisterStatus = getClassRegisterStatus(
                        memberId,
                        classUpgrade007Request.getNextClassId(),
                        (String) session.getAttribute(UserSession.Keys.UnitId.getValue())).split(";");

                switch (classRegisterStatus[0]) {
                    case "已取消":

                        break;
                    case "已報名":

                        break;
                    case "未報名":

                        // 續升報名
                        upgradeClassEnrollForm(session, memberId, classId, classUpgrade007Request.getNextClassId());
                        break;
                }
            }
        }

        return "success";
    }

    // 續升報名
    public void upgradeClassEnrollForm(HttpSession session, String memberId, String prevClassId, String nextClassId) {

//        ClassCtMemberInfo classCtMemberInfo = classCtMemberInfoRepo.GetClassCtMemberInfoByMemberId(memberId);
        MembersObject classCtMemberInfo = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId2(memberId);
        ClassInfo classInfo = classInfoRepo.GetClassInfoByClassId(nextClassId);
//        String timeZone = classUnitInfoRepo.GetClassUnitTimeZoneByUnitId(classInfo.getUnitId());
        String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());

        List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetClassDateInfoByClassId(nextClassId);

        String gender = "男";
        switch (classCtMemberInfo.getGender()) {
            case "F":
                gender = "女";
                break;
            default:
                gender = "男";
                break;
        }

        ClassEnrollForm prevClassEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(memberId, prevClassId);

        ClassEnrollForm classEnrollForm = new ClassEnrollForm();
        classEnrollForm.setUnitId((String) session.getAttribute(UserSession.Keys.UnitId.getValue()));
        classEnrollForm.setUnitName((String) session.getAttribute(UserSession.Keys.UnitName.getValue()));
        classEnrollForm.setClassEnrollFormId(classEnrollFormIdGenerator());
        classEnrollForm.setMemberId(classCtMemberInfo.getMemberId());
        classEnrollForm.setTwIdNum(classCtMemberInfo.getTwIdNum());
        classEnrollForm.setGender(gender);
        classEnrollForm.setAliasName(Empty2Null(classCtMemberInfo.getAliasName()));
        classEnrollForm.setCtDharmaName(Empty2Null(classCtMemberInfo.getCtDharmaName()));
        classEnrollForm.setAge(classCtMemberInfo.getAge());
        classEnrollForm.setHomePhoneNum1(Empty2Null(classCtMemberInfo.getHomePhoneNum1()));
        classEnrollForm.setMobileNum1(Empty2Null(classCtMemberInfo.getMobileNum1()));
        classEnrollForm.setDsaJobNameList(Empty2Null(classCtMemberInfo.getDsaJobNameList()));
        classEnrollForm.setAddress(Empty2Null(
                classCtMemberInfo.getMailingCountry() + classCtMemberInfo.getMailingState() +
                        classCtMemberInfo.getMailingCity() + classCtMemberInfo.getMailingStreet()));
        classEnrollForm.setCompanyNameAndJobTitle(Empty2Null(classCtMemberInfo.getCompanyJobTitle()));
        classEnrollForm.setSchoolNameAndMajor(Empty2Null(
                classCtMemberInfo.getSchoolName() + classCtMemberInfo.getSchoolMajor()));
        classEnrollForm.setBirthDate(classCtMemberInfo.getBirthDate().equals("") ? null : java.sql.Date.valueOf(classCtMemberInfo.getBirthDate()));
        classEnrollForm.setClassId(classInfo.getClassId());
        classEnrollForm.setClassTypeNum(classInfo.getClassTypeNum());
        classEnrollForm.setClassTypeName(classInfo.getClassTypeName());
        classEnrollForm.setYear(classInfo.getYear());
        classEnrollForm.setClassStartDate(classInfo.getClassStartDate());
        classEnrollForm.setClassEndDate(classInfo.getClassEndDate());
        classEnrollForm.setClassPeriodNum(classInfo.getClassPeriodNum());
        classEnrollForm.setClassName(classInfo.getClassName());
        classEnrollForm.setDayOfWeek(classInfo.getDayOfWeek());
        classEnrollForm.setClassDesc(classInfo.getClassDesc());
        classEnrollForm.setClassFullName(classInfo.getClassFullName());
        classEnrollForm.setClassGroupId(prevClassEnrollForm.getClassGroupId());
        classEnrollForm.setMemberGroupNum(memberGroupNumGenerator(nextClassId, prevClassEnrollForm.getClassGroupId())); // 學號
        classEnrollForm.setSameGroupMemberName(null);
        classEnrollForm.setCurrentClassIntroducerName(null);
        classEnrollForm.setCurrentClassIntroducerRelationship(null);
        classEnrollForm.setChildContactPersonName(null);
        classEnrollForm.setChildContactPersonPhoneNum(null);
//        classEnrollForm.setNewReturnChangeMark("新報名");
//        classEnrollForm.setNewReturnChangeDate(getLocalDate());
        classEnrollForm.setChangeAndWaitPrintMark(true);
        classEnrollForm.setUserDefinedLabel1(false);
        classEnrollForm.setUserDefinedLabel2(false);
        classEnrollForm.setAsistantMasterName(classInfo.getAssistantMasterName());
        classEnrollForm.setPreviousClassName(prevClassEnrollForm.getClassName());
        classEnrollForm.setPreviousClassGroupName(prevClassEnrollForm.getClassGroupId());
        classEnrollForm.setIsPreviousGraduated(prevClassEnrollForm.getIsGraduated());
        classEnrollForm.setIsPreviousFullAttend(prevClassEnrollForm.getIsFullAttended());
        classEnrollForm.setIsDroppedClass(false);
        classEnrollForm.setCreatorAndUpdater(session);

        classEnrollFormRepo.save(classEnrollForm);

        if (classDateInfoList.size() != 0) {

            for (ClassDateInfo classDateInfo : classDateInfoList) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));
                Date classDate = null;
                try {
                    classDate = dateFormat.parse(classDateInfo.getClassDate().toString().split(" ")[0] + " " + classDateInfo.getClassStartTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                ClassAttendRecord classAttendRecord = new ClassAttendRecord();
                classAttendRecord.setUnitId(classDateInfo.getUnitId());
                classAttendRecord.setUnitName(classDateInfo.getUnitName());
                classAttendRecord.setClassEnrollFormId(classEnrollForm.getClassEnrollFormId());
                classAttendRecord.setMemberId(classEnrollForm.getMemberId());
                classAttendRecord.setGender(classEnrollForm.getGender());
                classAttendRecord.setAliasName(classEnrollForm.getAliasName());
                classAttendRecord.setCtDharmaName(classEnrollForm.getCtDharmaName());
                classAttendRecord.setClassId(classEnrollForm.getClassId());
                classAttendRecord.setClassPeriodNum(classEnrollForm.getClassPeriodNum());
                classAttendRecord.setClassName(classEnrollForm.getClassName());
                classAttendRecord.setClassGroupId(classEnrollForm.getClassGroupId());
                classAttendRecord.setClassWeeksNum(classDateInfo.getClassWeeksNum());
                classAttendRecord.setClassDate(classDate);
//                classAttendRecord.setClassStartDtTm(null);
//                classAttendRecord.setAttendMark(null);
//                classAttendRecord.setAttendCheckinDate(null);
//                classAttendRecord.setAttendCheckinTime(null);
                classAttendRecord.setCreatorAndUpdater(session);

                classAttendRecordRepo.save(classAttendRecord);
            }
        }

        // 更新原班報名表
        int nextClassTypeNum = classEnrollForm.getClassTypeNum();
        int prevClassTypeNum = prevClassEnrollForm.getClassTypeNum();

        boolean isEnrollToNewUpperClass = nextClassTypeNum - prevClassTypeNum == 1 ? true : false;

        prevClassEnrollForm.setIsEnrollToNewUpperClass(isEnrollToNewUpperClass);
        prevClassEnrollForm.setNextNewClassId(nextClassId);
        prevClassEnrollForm.setNextNewClassTypeNum(classEnrollForm.getClassTypeNum());
        prevClassEnrollForm.setNextNewClassTypeName(classEnrollForm.getClassTypeName());
        prevClassEnrollForm.setNextNewClassName(classEnrollForm.getClassName());
        prevClassEnrollForm.setIsCompleteNextNewClassEnroll(true);
        prevClassEnrollForm.setUpdater(session);

        classEnrollFormRepo.save(prevClassEnrollForm);
    }

    // 班級續升，右邊表格移除
    public String membersRemove(HttpSession session, String classId, ClassUpgrade008Request classUpgrade008Request) {

        if (classUpgrade008Request.getMemberIdList().size() != 0) {

            for (String memberId : classUpgrade008Request.getMemberIdList()) {

                // 刪除上課紀錄
                classAttendRecordRepo.DeleteClassAttendRecordByMemberIdAndClassId(memberId, classId);

                // 刪除報名表
                classEnrollFormRepo.DeleteClassEnrollFormByMemberIdAndClassId(memberId, classId);

                // 更新續升前的報名表
                ClassEnrollForm prevClassEnrollForm = classEnrollFormRepo.GetPrevClassEnrollFormByMemberIdAndClassId(memberId, classId);

                if (prevClassEnrollForm != null) {

                    // 有續升前的報名表
                    prevClassEnrollForm.setIsEnrollToNewUpperClass(false);
                    prevClassEnrollForm.setIsNotComeBack(false);
                    prevClassEnrollForm.setNextNewClassTypeNum(0);
                    prevClassEnrollForm.setNextNewClassTypeName(null);
                    prevClassEnrollForm.setNextNewClassId(null);
                    prevClassEnrollForm.setNextNewClassName(null);
                    prevClassEnrollForm.setIsCompleteNextNewClassEnroll(false);
                    prevClassEnrollForm.setNextNewClassNote(null);
                    prevClassEnrollForm.setUpdater(session);

                    classEnrollFormRepo.save(prevClassEnrollForm);
                }
            }
        }

        return "success";
    }

    // 人數統計
    public List<DataReport001Response.DataReport001Object> getMemberCount(String reportType, String year, boolean isMeditation) {

        List<DataReport001Response.DataReport001Object> dataReport001ObjectList = new ArrayList<>();

        List<DataReport001Object> dataReport001Objects = new ArrayList<>();

        if (reportType.equals("當期禪修班人數")) {
            // 當期禪修班人數
            List<String> yearList = classEnrollFormRepo.GetYearList();

            if (!isMeditation) {
                // 未勾選僅限禪修班 (撈所有班別的資料)
                dataReport001Objects = classEnrollFormRepo.GetDataReportByYear1(yearList);
            } else {
                // 勾選僅限禪修班 (撈班別1~5的資料)
                dataReport001Objects = classEnrollFormRepo.GetDataReportByYear2(yearList);
            }
        } else {
            // 各期禪修班人數
            List<String> yearList = new ArrayList<>();

            if (year.equals("")) {
                yearList = classEnrollFormRepo.GetYearList();
            }else {
                yearList.add(year);
            }

            if (!isMeditation) {
                // 未勾選僅限禪修班 (撈所有班別的資料)
                dataReport001Objects = classEnrollFormRepo.GetDataReportByYear3(yearList);
            } else {
                // 勾選僅限禪修班 (撈班別1~5的資料)
                dataReport001Objects = classEnrollFormRepo.GetDataReportByYear4(yearList);
            }
        }

        if (dataReport001Objects.size() != 0) {

            String mClassId = "";

            List<String> classIdList = new ArrayList<>();

            for (DataReport001Object dataReport001Object : dataReport001Objects) {

                if (!classIdList.contains(dataReport001Object.getClassId())) {

                    mClassId = dataReport001Object.getClassId();

                    classIdList.add(mClassId);
                }
            }

            for (String classId : classIdList) {

                List<DataReport001Object> classList = dataReport001Objects.stream().filter(
                        a -> a.getClassId().equals(classId)).collect(Collectors.toList());

                // 報名(男)
                int registerMNumber = (int) classList.stream().filter(p -> p.getGender().equals("男")).count();

                // 報名(女)
                int registerFNumber = (int) classList.stream().filter(p -> p.getGender().equals("女")).count();

                // 取消(男)
                int cancelMNumber = (int) classList.stream().filter(p -> p.getGender().equals("男") && p.getIsDroppedClass()).count();

                // 取消(女)
                int cancelFNumber = (int) classList.stream().filter(p -> p.getGender().equals("女") && p.getIsDroppedClass()).count();

                // 結業(男)
                int graduatedMNumber = (int) classList.stream().filter(p -> p.getGender().equals("男") && p.getIsGraduated()).count();

                // 結業(女)
                int graduatedFNumber = (int) classList.stream().filter(p -> p.getGender().equals("女") && p.getIsGraduated()).count();

                String className = "";

//                switch (classList.get(0).getClassDayOrNight()) {
//                    case "日間":
//                        className = "日";
//                        break;
//                    case "夜間":
//                        className = "夜";
//                        break;
//                }
//
//                switch (classList.get(0).getClassTypeName()) {
//                    case "初級班":
//                        className = className + "初";
//                        break;
//                    case "中級班":
//                        className = className + "中";
//                        break;
//                    case "高級班":
//                        className = className + "高";
//                        break;
//                    case "研一班":
//                        className = className + "研一";
//                        break;
//                    case "研二班":
//                        className = className + "研二";
//                        break;
//                }

                className = classList.get(0).getClassName();

                dataReport001ObjectList.add(new DataReport001Response.DataReport001Object(
                        classList.get(0).getUnitName(),
                        classList.get(0).getYear(),
                        classList.get(0).getClassPeriodNum(),
                        className,
                        classList.get(0).getClassId(),
                        registerMNumber,
                        registerFNumber,
                        cancelMNumber,
                        cancelFNumber,
                        graduatedMNumber,
                        graduatedFNumber
                ));
            }
        }

        return dataReport001ObjectList;
    }

    // 即時統計
    public List<DataReport003Response.DataReport003_1Object> getRealTimeCount(String reportType) {

        List<DataReport003Response.DataReport003_1Object> dataReport002_1ObjectList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date startDate = calendar.getTime();

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date endDate = calendar.getTime();

        List<DataReport003Object> dataReport003ObjectList = classEnrollFormRepo.getRealTimeCount(startDate, endDate);

//        List<String> unitNameList = new ArrayList<>();
//
//        for (DataReport003Object dataReport003Object : dataReport003ObjectList) {
//            if (!unitNameList.contains(dataReport003Object.getUnitName())) {
//                unitNameList.add(dataReport003Object.getUnitName());
//            }
//        }

        if (dataReport003ObjectList.size() > 0) {
            List<String> unitNames = dataReport003ObjectList.stream().map(d -> d.getUnitName()).distinct().collect(Collectors.toList());
            List<String> classIds = dataReport003ObjectList.stream().map(d -> d.getClassId()).distinct().collect(Collectors.toList());

            List<ClassInfo> classInfos = classInfoRepo.GetClassListByClassIdList(classIds);
//        }
//
//
//        if (unitNameList.size() != 0) {

            List<DataReport003Response.DataReport003_2Object> dataReport002_2ObjectList = new ArrayList<>();

            String[] titleList = new String[]{
                    "日間初級班", "夜間初級班", "日間中級班", "夜間中級班", "日間高級班", "夜間高級班", "日間研一班", "夜間研一班",
                    "日間研二班", "夜間研二班", "其他"};

            for (String unitName : unitNames) {

                dataReport002_2ObjectList = new ArrayList<>();

                int expectedAttendance = 0; // 應到人數
                int actualAttendance = 0; // 實到人數
                int totalExpectedAttendance = 0; // 班別1~5的應到人數
                int totalActualAttendance = 0; // 班別1~5的實到人數
                String output = "";

                for (String title : titleList) {

                    if (title.length() == 5) {

                        String classDayOrNight = title.substring(0, 2);
                        String classTypeName = title.substring(2, 5);

                        List<String> matchedClassIds = classInfos.stream().filter(c -> c.getUnitName().equals(unitName) && classDayOrNight.equals(c.getClassDayOrNight()) && c.getClassTypeName().equals(classTypeName)).map(c -> c.getClassId()).collect(Collectors.toList());
                        expectedAttendance = (int)dataReport003ObjectList.stream().filter(d -> matchedClassIds.contains(d.getClassId())).count();
                        actualAttendance = (int)dataReport003ObjectList.stream().filter(d -> matchedClassIds.contains(d.getClassId()) && d.getAttendMark() != null).count();
//                        expectedAttendance = (int) dataReport003ObjectList.stream().filter(
//                                p -> p.getUnitName().equals(unitName) &&
//                                        (p.getClassName().substring(0, 1) + "間").equals(classDayOrNight) &&
//                                        p.getClassTypeName().equals(classTypeName)).count();

//                        actualAttendance = (int) dataReport003ObjectList.stream().filter(
//                                p -> p.getUnitName().equals(unitName) &&
//                                        (p.getClassName().substring(0, 1) + "間").equals(classDayOrNight) &&
//                                        p.getClassTypeName().equals(classTypeName) &&
//                                        p.getAttendMark() != null).count();

                        totalExpectedAttendance = totalExpectedAttendance + expectedAttendance;
                        totalActualAttendance = totalActualAttendance + actualAttendance;

                    } else {
                        expectedAttendance = (int) dataReport003ObjectList.stream().filter(
                                p -> p.getUnitName().equals(unitName)).count();

                        actualAttendance = (int) dataReport003ObjectList.stream().filter(
                                p -> p.getUnitName().equals(unitName) && p.getAttendMark() != null).count();

                        expectedAttendance = expectedAttendance - totalExpectedAttendance;
                        actualAttendance = actualAttendance - totalActualAttendance;
                    }

                    if (expectedAttendance == 0) {
                        output = "";
                    } else {
                        switch (reportType) {
                            case "人數":
                                output = String.valueOf(actualAttendance);
                                break;
                            case "出席率":
                                output = CommonUtils.Percentage(actualAttendance, expectedAttendance) + "%";
                                break;
                        }
                    }

                    dataReport002_2ObjectList.add(new DataReport003Response.DataReport003_2Object(title, output));
                }

                dataReport002_1ObjectList.add(new DataReport003Response.DataReport003_1Object(
                        unitName, dataReport002_2ObjectList
                ));
            }

        }
        return dataReport002_1ObjectList;
    }

    // 禪修班報到統計表(區間統計) 舊版
//    public List<DataReport005Response.DataReport005_1Object> getCheckinCountByRange(
//            String startDate, String endDate, boolean isMeditation, String reportType) {
//
//        List<DataReport005Response.DataReport005_1Object> dataReport005_1ObjectList = new ArrayList<>();
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(java.sql.Date.valueOf(startDate));
//        calendar.set(Calendar.HOUR_OF_DAY,0);
//        calendar.set(Calendar.MINUTE,0);
//        calendar.set(Calendar.SECOND,0);
//        Date mStartDate = calendar.getTime();
//
//        calendar.setTime(java.sql.Date.valueOf(endDate));
//        calendar.set(Calendar.HOUR_OF_DAY,23);
//        calendar.set(Calendar.MINUTE,59);
//        calendar.set(Calendar.SECOND,59);
//        Date mEndDate = calendar.getTime();
//
//        List<String> classDateList = new ArrayList<>();
//
//        for(Date classDate : classAttendRecordRepo.GetClassAttendRecordClassDateListByClassDate(mStartDate, mEndDate)) {
//
//            if (!classDateList.contains(classDate.toString().substring(0, 10))) {
//
//                classDateList.add(classDate.toString().substring(0, 10));
//            }
//        }
//
//        List<DataReport005Object> dataReport005ObjectList = classEnrollFormRepo.getCheckinCountByRange(mStartDate, mEndDate);
//
//        List<String> classPeriodNumList = new ArrayList<>();
//
//        for (DataReport005Object dataReport005Object : dataReport005ObjectList) {
//
//            if (!classPeriodNumList.contains(dataReport005Object.getClassPeriodNum())) {
//
//                classPeriodNumList.add(dataReport005Object.getClassPeriodNum());
//            }
//        }
//
//        if (classPeriodNumList.size() != 0) {
//
//            List<DataReport005Response.DataReport005_2Object> dataReport005_2ObjectList;
//
//            for (String classPeriodNum : classPeriodNumList) {
//
//                List<DataReport005Object> dataReport005ObjectsClassIdList = dataReport005ObjectList.stream()
//                        .filter(p -> p.getClassPeriodNum().equals(classPeriodNum)).collect(Collectors.toList());
//
//                List<String> classIdList = new ArrayList<>();
//
//                for (DataReport005Object dataReport005Object : dataReport005ObjectsClassIdList) {
//
//                    if (!classIdList.contains(dataReport005Object.getClassId())) {
//
//                        if (isMeditation) {
//
//                            // 撈班別1~5
//                            if (dataReport005Object.getClassTypeNum() < 6) {
//
//                                classIdList.add(dataReport005Object.getClassId());
//                            }
//                        }else {
//
//                            // 撈所有班別
//                            classIdList.add(dataReport005Object.getClassId());
//                        }
//
//                    }
//                }
//
//                String unitName = "", className = "";
//
//                for (String classId : classIdList) {
//
//                    int expectedAttendance = 0; // 應到人數
//                    int actualAttendance = 0; // 實到人數
//                    String number = "";
//
//                    List<DataReport005Object> objects = dataReport005ObjectList.stream()
//                            .filter(p -> p.getClassId().equals(classId))
//                            .collect(Collectors.toList());
//
//                    unitName = objects.get(0).getUnitName();
//                    className = objects.get(0).getClassName();
//
//                    dataReport005_2ObjectList = new ArrayList<>();
//
//                    for (String classDate : classDateList) {
//
//                        List<DataReport005Object> objects2 = dataReport005ObjectList.stream()
//                                .filter(p -> p.getClassId().equals(classId) &&
//                                        p.getClassDate().toString().substring(0, 10).equals(classDate))
//                                .collect(Collectors.toList());
//
//                        switch (reportType) {
//                            case "人數":
//
//                                if (objects2.size() != 0) {
//
//                                    actualAttendance = (int) dataReport005ObjectList.stream()
//                                            .filter(p -> p.getClassId().equals(classId) &&
//                                                    p.getClassDate().toString().substring(0, 10).equals(classDate) &&
//                                                    p.getAttendMark() != null)
//                                            .count();
//
//                                    number = String.valueOf(actualAttendance);
//                                }else {
//
//                                    number = "";
//                                }
//                                break;
//                            case "出席率":
//
//                                DecimalFormat df = new DecimalFormat("##0.00");
//
//                                if (objects2.size() != 0) {
//
//                                    actualAttendance = (int) dataReport005ObjectList.stream()
//                                            .filter(p -> p.getClassId().equals(classId) &&
//                                                    p.getClassDate().toString().substring(0, 10).equals(classDate.toString().substring(0, 10)) &&
//                                                    p.getAttendMark() != null)
//                                            .count();
//
//                                    expectedAttendance = (int) dataReport005ObjectList.stream()
//                                            .filter(p -> p.getClassId().equals(classId) &&
//                                                    p.getClassDate().toString().substring(0, 10).equals(classDate.toString().substring(0, 10)))
//                                            .count();
//
//                                    number = df.format((actualAttendance / expectedAttendance) * 100) + "%";
//                                }else {
//
//                                    number = "";
//                                }
//                                break;
//                        }
//
//                        dataReport005_2ObjectList.add(new DataReport005Response.DataReport005_2Object(
//                                classDate.toString().substring(5, 10).replace("-","/").toString(),
//                                number
//                        ));
//                    }
//
//                    dataReport005_1ObjectList.add(new DataReport005Response.DataReport005_1Object(
//                            unitName, classPeriodNum, className, dataReport005_2ObjectList
//                    ));
//                }
//            }
//        }
//
//        return dataReport005_1ObjectList;
//    }

    // 禪修班報到統計表(區間統計) 新版
    public List<DataReport005Response.DataReport005_1Object> getCheckinCountByRange(
            String startDate, String endDate, boolean isMeditation, String reportType) {

        List<DataReport005Response.DataReport005_1Object> dataReport005_1ObjectList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(java.sql.Date.valueOf(startDate));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date mStartDate = calendar.getTime();

        calendar.setTime(java.sql.Date.valueOf(endDate));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date mEndDate = calendar.getTime();

//        List<String> recordClassIdList = classAttendRecordRepo.GetClassAttendRecordClassIdListByClassDate(mStartDate, mEndDate);
//
//        List<String> enrollClassIdList = classEnrollFormRepo.GetClassEnrollFormClassIdListByClassIdList(recordClassIdList);
//
//        List<String> enrollClassIdList2 = new ArrayList<>();
//
//        for (String enrollClassId : enrollClassIdList) {
//            if (!enrollClassIdList2.contains(enrollClassId)) {
//                enrollClassIdList2.add(enrollClassId);
//            }
//        }
//
//        List<String> classIds = classInfoRepo.GetClassIdListByClassIdList(enrollClassIdList2);

        List<String> classIds = classAttendRecordRepo.GetClassAttendRecordClassIdListByClassDate(mStartDate, mEndDate);

        classIds = classIds.stream().filter(distinctByKey(s -> s)).collect(Collectors.toList());

        // include canceled
        List<DataReport005Object> forms = classEnrollFormRepo.GetClassEnrollFormListByClassIdList(classIds);

        List<Date> dateList = classAttendRecordRepo.GetClassAttendRecordClassDateListByClassDateAndClassIdList(mStartDate, mEndDate, classIds);

        List<String> classDateList = new ArrayList<>();

        for (Date date : dateList) {
            if (!classDateList.contains(date.toString().substring(0, 10))) {
                classDateList.add(date.toString().substring(0, 10));
            }
        }

        // get canceled Form Ids
        List<String> canceledFormIds = forms.stream().filter(f -> f.getIsDroppedClass() == true).map(f -> f.getClassEnrollFormId()).collect(Collectors.toList());

        List<DataReport005Object> records = classAttendRecordRepo.GetClassAttendRecordListByClassDate(mStartDate, mEndDate);

        // remove canceled
        records = records.stream().filter(r -> !canceledFormIds.contains(r.getClassEnrollFormId())).collect(Collectors.toList());

        List<String> classPeriodNumList = new ArrayList<>();

        for (DataReport005Object form : forms) {
            if (!classPeriodNumList.contains(form.getClassPeriodNum())) {
                classPeriodNumList.add(form.getClassPeriodNum());
            }
        }

        if (classPeriodNumList.size() != 0) {

            List<DataReport005Response.DataReport005_2Object> dataReport005_2ObjectList;

            for (String classPeriodNum : classPeriodNumList) {

                List<DataReport005Object> dataReport005ObjectsClassIdList = forms.stream()
                        .filter(p -> p.getClassPeriodNum().equals(classPeriodNum)).collect(Collectors.toList());

                List<String> classIdList = new ArrayList<>();

                for (DataReport005Object dataReport005Object : dataReport005ObjectsClassIdList) {

                    if (!classIdList.contains(dataReport005Object.getClassId())) {

                        if (isMeditation) {

                            // 撈班別1~5
                            if (dataReport005Object.getClassTypeNum() < 6) {

                                classIdList.add(dataReport005Object.getClassId());
                            }
                        } else {

                            // 撈所有班別
                            classIdList.add(dataReport005Object.getClassId());
                        }

                    }
                }

                String unitName = "", className = "", classTypeNum = "", classDayOrNight = "";

                for (String classId : classIdList) {

                    int expectedAttendance = 0; // 應到人數
                    int actualAttendance = 0; // 實到人數
                    String number = "";

                    List<DataReport005Object> objects = forms.stream()
                            .filter(p -> p.getClassId().equals(classId))
                            .collect(Collectors.toList());

                    unitName = objects.get(0).getUnitName();
                    className = objects.get(0).getClassName();
//                    classTypeNum = String.valueOf(objects.get(0).getClassTypeNum());
//                    classDayOrNight = objects.get(0).getClassDayOrNight();

//                    logger.debug("TEST : " + classId + ";" + unitName + ";" + className + ";" + classPeriodNum + ";" + classTypeNum + ";" + classDayOrNight);

                    dataReport005_2ObjectList = new ArrayList<>();

                    for (String classDate : classDateList) {

                        List<DataReport005Object> objects2 = records.stream()
                                .filter(p -> p.getClassId().equals(classId) &&
                                        p.getClassDate().toString().substring(0, 10).equals(classDate))
                                .collect(Collectors.toList());

                        switch (reportType) {
                            case "人數":

                                if (objects2.size() != 0) {

                                    actualAttendance = (int) records.stream()
                                            .filter(p -> p.getClassId().equals(classId) &&
                                                    p.getClassDate().toString().substring(0, 10).equals(classDate) &&
                                                    p.getAttendMark() != null)
                                            .count();

                                    number = String.valueOf(actualAttendance);
                                } else {

                                    number = "";
                                }
                                break;
                            case "出席率":
                                if (objects2.size() != 0) {

                                    actualAttendance = (int) records.stream()
                                            .filter(p -> p.getClassId().equals(classId) &&
                                                    p.getClassDate().toString().substring(0, 10).equals(classDate.toString().substring(0, 10)) &&
                                                    p.getAttendMark() != null)
                                            .count();

                                    expectedAttendance = (int) records.stream()
                                            .filter(p -> p.getClassId().equals(classId) &&
                                                    p.getClassDate().toString().substring(0, 10).equals(classDate.toString().substring(0, 10)))
                                            .count();

                                    number = CommonUtils.Percentage(actualAttendance, expectedAttendance) + "%";
                                } else {
                                    number = "";
                                }
                                break;
                        }

                        dataReport005_2ObjectList.add(new DataReport005Response.DataReport005_2Object(
                                classDate.toString().substring(5, 10).replace("-", "/").toString(),
                                number
                        ));
                    }

                    dataReport005_1ObjectList.add(new DataReport005Response.DataReport005_1Object(
                            unitName, classPeriodNum, className, dataReport005_2ObjectList
                    ));
                }
            }
        }

        return dataReport005_1ObjectList;
    }

    // TEST 自動報名系統
    public String register(HttpSession session) {

        String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());

        List<String> classIdList = classInfoRepo.GetClassIdList(unitName);

        for (String classId : classIdList) {

            ClassInfo classInfo = classInfoRepo.GetClassInfoByClassId(classId);

            List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetClassDateInfoByClassId(classId);

            Integer[] limitList = new Integer[]{50, 85, 100, 110, 125, 130, 135, 140, 145, 150};

            int maxLimit = limitList[(int) (Math.random() * 10)];

            List<String> memberIdList = ctMemberInfoRepo.GetMemberIdList();

//            List<ClassEnrollForm> classEnrollFormList = new ArrayList<>();
            List<ClassAttendRecord> classAttendRecordList = new ArrayList<>();

            for (int x = 0; x < maxLimit; x++) {

                String memberId = memberIdList.get(x);
                logger.error("ClassId : " + classId + " " + (x + 1) + ":" + memberId);

//                ClassCtMemberInfo classCtMemberInfo = classCtMemberInfoRepo.GetClassCtMemberInfoByMemberId(memberId);
                MembersObject classCtMemberInfo = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId2(memberId);

                String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());

                String gender = "男";
                switch (classCtMemberInfo.getGender()) {
                    case "F":
                        gender = "女";
                        break;
                    default:
                        gender = "男";
                        break;
                }

                ClassEnrollForm classEnrollForm = new ClassEnrollForm();
                classEnrollForm.setUnitId((String) session.getAttribute(UserSession.Keys.UnitId.getValue()));
                classEnrollForm.setUnitName((String) session.getAttribute(UserSession.Keys.UnitName.getValue()));
                classEnrollForm.setClassEnrollFormId(classEnrollFormIdGenerator());
                classEnrollForm.setMemberId(classCtMemberInfo.getMemberId());
                classEnrollForm.setTwIdNum(classCtMemberInfo.getTwIdNum());
                classEnrollForm.setGender(gender);
                classEnrollForm.setAliasName(Empty2Null(classCtMemberInfo.getAliasName()));
                classEnrollForm.setCtDharmaName(Empty2Null(classCtMemberInfo.getCtDharmaName()));
                classEnrollForm.setAge(classCtMemberInfo.getAge());
                classEnrollForm.setHomePhoneNum1(Empty2Null(classCtMemberInfo.getHomePhoneNum1()));
                classEnrollForm.setMobileNum1(Empty2Null(classCtMemberInfo.getMobileNum1()));
                classEnrollForm.setDsaJobNameList(Empty2Null(classCtMemberInfo.getDsaJobNameList()));
                classEnrollForm.setAddress(Empty2Null(
                        classCtMemberInfo.getMailingCountry() + classCtMemberInfo.getMailingState() +
                                classCtMemberInfo.getMailingCity() + classCtMemberInfo.getMailingStreet()));
                classEnrollForm.setCompanyNameAndJobTitle(Empty2Null(classCtMemberInfo.getCompanyJobTitle()));
                classEnrollForm.setSchoolNameAndMajor(Empty2Null(
                        classCtMemberInfo.getSchoolName() + classCtMemberInfo.getSchoolMajor()));
                classEnrollForm.setBirthDate(Empty2Null(classCtMemberInfo.getBirthDate()).equals("") ? null : java.sql.Date.valueOf(classCtMemberInfo.getBirthDate().substring(0, 4) + "-" + classCtMemberInfo.getBirthDate().substring(4, 6) + "-" + classCtMemberInfo.getBirthDate().substring(6, 8)));
                classEnrollForm.setClassId(classInfo.getClassId());
                classEnrollForm.setClassTypeNum(classInfo.getClassTypeNum());
                classEnrollForm.setClassTypeName(classInfo.getClassTypeName());
                classEnrollForm.setYear(classInfo.getYear());
                classEnrollForm.setClassStartDate(classInfo.getClassStartDate());
                classEnrollForm.setClassEndDate(classInfo.getClassEndDate());
                classEnrollForm.setClassPeriodNum(classInfo.getClassPeriodNum());
                classEnrollForm.setClassName(classInfo.getClassName());
                classEnrollForm.setDayOfWeek(classInfo.getDayOfWeek());
                classEnrollForm.setClassDesc(classInfo.getClassDesc());
                classEnrollForm.setClassFullName(classInfo.getClassFullName());
                classEnrollForm.setClassGroupId("1");
                classEnrollForm.setMemberGroupNum(memberGroupNumGenerator(classId, "1")); // 學號
                classEnrollForm.setNewReturnChangeMark("新報名");
                classEnrollForm.setNewReturnChangeDate(getLocalDate());
                classEnrollForm.setChangeAndWaitPrintMark(true);
                classEnrollForm.setUserDefinedLabel1(false);
                classEnrollForm.setUserDefinedLabel2(false);
                classEnrollForm.setAsistantMasterName(classInfo.getAssistantMasterName());
                classEnrollForm.setCreatorAndUpdater(session);

//                classEnrollFormList.add(classEnrollForm);
                classEnrollFormRepo.save(classEnrollForm);

                if (classDateInfoList.size() != 0) {

                    for (ClassDateInfo classDateInfo : classDateInfoList) {

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));
                        Date classDate = null;
                        try {
                            classDate = dateFormat.parse(classDateInfo.getClassDate().toString().split(" ")[0] + " " + classDateInfo.getClassStartTime());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        ClassAttendRecord classAttendRecord = new ClassAttendRecord(
                                classDateInfo.getUnitId(),
                                classDateInfo.getUnitName(),
                                classEnrollForm.getClassEnrollFormId(),
                                classEnrollForm.getMemberId(),
                                classEnrollForm.getGender(),
                                classEnrollForm.getAliasName(),
                                classEnrollForm.getCtDharmaName(),
                                classEnrollForm.getClassId(),
                                classEnrollForm.getClassPeriodNum(),
                                classEnrollForm.getClassName(),
                                classEnrollForm.getClassGroupId(),
                                classDateInfo.getClassWeeksNum(),
                                classDate,
                                null,
                                "V",
                                null,
                                null
                        );

                        classAttendRecord.setCreatorAndUpdater(session);

                        classAttendRecordList.add(classAttendRecord);
//                        classAttendRecordRepo.save(classAttendRecord);
                    }
                }
            }

//            classEnrollFormRepo.saveAll(classEnrollFormList);
            classAttendRecordRepo.saveAll(classAttendRecordList);
        }

        return "success";
    }

    // 取得學員資料列表
    public List<Members001Object> getClassEnrollFormListByRequestParam(String unitId,
                                                                       String classStatus,
                                                                       String classPeriodNum,
                                                                       String className) {

        List<Members001Object> classEnrollFormLists = new ArrayList<>();

        if (classStatus.equals("")) {

            classEnrollFormLists = classEnrollFormRepo.GetClassEnrollFormListByRequestParam(unitId);

        }else {

            List<String> classIdList = classInfoRepo.GetClassIdByClassStatus(classStatus);

            if (classIdList.size() > 0) {
                logger.debug(unitId + "/" + classIdList.size());
                classEnrollFormLists = classEnrollFormRepo.GetClassEnrollFormListByRequestParam(unitId, classIdList);
            }
        }

        String mClassPeriodNum = classPeriodNum.equals("") ? "0" : "1";
        String mClassName = className.equals("") ? "0" : "1";
        String key = mClassPeriodNum + mClassName;

        List<Members001Object> classEnrollFormList = new ArrayList<>();

        switch (key) {
            case "00":

                classEnrollFormList = classEnrollFormLists;
                break;
            case "01":

                classEnrollFormList = classEnrollFormLists.stream()
                        .filter(w -> w.getClassName().equals(className))
                        .collect(Collectors.toList());
                break;
            case "10":

                classEnrollFormList = classEnrollFormLists.stream()
                        .filter(w -> w.getClassPeriodNum().equals(classPeriodNum))
                        .collect(Collectors.toList());
                break;
            case "11":

                classEnrollFormList = classEnrollFormLists.stream()
                        .filter(w -> w.getClassPeriodNum().equals(classPeriodNum) &&
                                w.getClassName().equals(className))
                        .collect(Collectors.toList());
                break;
        }

        List<Members001Object> MembersObjectList = new ArrayList<>();

        for (int x = 0; x < classEnrollFormList.size(); x++) {
            MembersObjectList.add(new Members001Object(
                    classEnrollFormList.get(x).getMemberId(),
                    classEnrollFormList.get(x).getGender(),
                    Null2Empty(classEnrollFormList.get(x).getAliasName()),
                    Null2Empty(classEnrollFormList.get(x).getCtDharmaName()),
                    Null2Empty(classEnrollFormList.get(x).getClassPeriodNum()),
                    Null2Empty(classEnrollFormList.get(x).getClassName()),
                    classEnrollFormList.get(x).getAge(),
                    Null2Empty(classEnrollFormList.get(x).getClassGroupId()),
                    Null2Empty(classEnrollFormList.get(x).getHomePhoneNum1()),
                    Null2Empty(classEnrollFormList.get(x).getMobileNum1()),
                    Null2Empty(classEnrollFormList.get(x).getDsaJobNameList()),
                    classEnrollFormList.get(x).getIsDroppedClass()));
        }

        return MembersObjectList;
    }

    // 取得組別
    public Common012Response getClassGroupListByClassId(String classId) {

        List<Common012_2Object> classGroupList = classEnrollFormRepo.GetClassGroupListByClassId(classId);

        List<String> groupIdList = new ArrayList<>();

        for (Common012_2Object classGroup : classGroupList) {

            if (!groupIdList.contains(classGroup.getClassGroupId())) {

                groupIdList.add(classGroup.getClassGroupId());
            }
        }

        List<Common012_1Object> items = new ArrayList<>();

        for (String groupId : groupIdList) {

            int f = classGroupList.stream()
                    .filter(a -> a.getClassGroupId().equals(groupId) && a.getGender().equals("女"))
                    .collect(Collectors.toList())
                    .size();

            int m = classGroupList.stream()
                    .filter(a -> a.getClassGroupId().equals(groupId) && a.getGender().equals("男"))
                    .collect(Collectors.toList())
                    .size();

            items.add(new Common012_1Object(groupId, f, m));
        }

        if (items.size() == 0) {

            items.add(new Common012_1Object("1", 0, 0));
        }

        Common012Response common012Response = new Common012Response(
                200,
                "success",
                items
        );

        return common012Response;
    }

    // 取得學員班級列表
    public List<Anonymous009_1Object> getMemberClassListByMemberId(HttpSession session, String memberId) {

        List<Anonymous009_1Object> items = new ArrayList<>();

        List<ClassEnrollForm> classEnrollFormList = classEnrollFormRepo.GetClassEnrollFormListByMemberMemberId(memberId);

        if (classEnrollFormList.size() > 0) {

            List<String> classIdList = new ArrayList<>();

            for (ClassEnrollForm classEnrollForm : classEnrollFormList) {

                classIdList.add(classEnrollForm.getClassId());
            }

//            String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());
//
//            SimpleDateFormat dateFormatInput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            dateFormatInput.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));
//
//            Date localTime = null;
//
            SimpleDateFormat dateFormatOutput = new SimpleDateFormat("yyyy-MM-dd");
//            try {
//                localTime = dateFormatOutput.parse(dateFormatInput.format(new Date()));
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }

            List<ClassAttendRecord> classAttendRecordList =
                    classAttendRecordRepo.GetClassAttendRecordListByMemberIdAndClassId(memberId, classIdList);

            for (String classId : classIdList) {

                List<Anonymous009_2Object> classDates = new ArrayList<>();

                ClassEnrollForm classEnrollForm = classEnrollFormList.stream()
                        .filter(s -> s.getClassId().equals(classId))
                        .collect(Collectors.toList()).get(0);

                List<ClassAttendRecord> mClassAttendRecordList = classAttendRecordList.stream()
                        .filter(s -> s.getClassId().equals(classId))
                        .collect(Collectors.toList());

                if (mClassAttendRecordList.size() > 0) {

                    for (ClassAttendRecord classAttendRecord : mClassAttendRecordList) {

                        String note = "";

                        if (classAttendRecord.getAttendMark() != null) {

//                            switch (classAttendRecord.getAttendMark()) {
//                                case "V":
//                                    note = "出席";
//                                    break;
//                                case "O":
//                                    note = "請假";
//                                    break;
//                                case "A":
//                                    note = "曠課";
//                                    break;
//                                case "X":
//                                    note = "中輟";
//                                    break;
//                                case "D":
//                                    note = "日補";
//                                    break;
//                                case "N":
//                                    note = "夜補";
//                                    break;
//                                case "W":
//                                    note = "公假";
//                                    break;
//                                case "F":
//                                    note = "放香";
//                                    break;
//                                case "M":
//                                    note = "補課";
//                                    break;
//                                case "L":
//                                    note = "遲到";
//                                    break;
//                                case "E":
//                                    note = "早退";
//                                    break;
//                                case "S1":
//                                    note = "特殊";
//                                    break;
//                                case "S2":
//                                    note = "特殊";
//                                    break;
//                                case "S3":
//                                    note = "特殊";
//                                    break;
//                            }

                            note = Null2Empty(classAttendRecord.getNote());
                        }

                        classDates.add(new Anonymous009_2Object(
                                dateFormatOutput.format(classAttendRecord.getClassDate()),
                                classAttendRecord.getClassWeeksNum(),
                                classAttendRecord.getAttendMark() == null ? "" : classAttendRecord.getAttendMark(),
                                note
                        ));
                    }

                    items.add(new Anonymous009_1Object(
                            classEnrollForm.getClassId(),
                            classEnrollForm.getClassName(),
                            classEnrollForm.getDayOfWeek(),
                            classDates
                    ));
                }
            }
        }

        return items;
    }

    // 取得學員班級列表，詳細點開
    public List<AnonymousMakeupClass> getClassEnrollFormListByUnitIdAndId(String unitId, String memberId) {

//        logger.debug(unitId + "/" + memberId);

        // all opening classed of an unit
        List<AnonymousMakeupClass> classInfoList = classInfoRepo.GetOpeningClassListByClassStatusUnitId("開課中", unitId);

//        logger.debug("class info count :" + classInfoList.size());

        List<String> classIds = classInfoList.stream().map(c -> c.getClassId()).distinct().collect(Collectors.toList());

//        logger.debug("class count :" + classIds.size());

        // all attend record of opening classes by member
        List<ClassAttendRecord> classAttendRecordList = classAttendRecordRepo.GetRecordsByClassIdsMemberId(classIds, memberId);

        // all class date of opening classes
        List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetClassDateByClassIdList(classIds);

//        logger.debug("all class date count :" + classDateInfoList.size());

        // all attend marks of opening classes
        List<ClassAttendMark> classAttendMarkList = classAttendMarkRepo.GetIsGraduatedListByClassIds(classIds);

//        logger.debug("all attend marks count :" + classAttendMarkList.size());

        // all makeup records of classes
        List<ClassMakeupRecord> classMakeupRecordList = classMakeupRecordRepo.GetRecordByClassIdListMemberId(classIds, memberId);

        Date now = new Date();

        for (AnonymousMakeupClass classItem : classInfoList) {

//            logger.debug("class id: " + classItem.getClassId());

            // all dates of class
            List<ClassDateInfo> dateInfoList = classDateInfoList.stream().filter(c -> c.getClassId().equals(classItem.getClassId())).collect(Collectors.toList());

//            logger.debug("date count: " + dateInfoList.size());
//            long classPassedNum = dateInfoList.stream().filter(d -> d.getClassDate().compareTo(now) < 0).count();

            // all counted attend mark of class
            List<String> attendMarkList = classAttendMarkList.stream().filter(m -> m.getClassId().equals(classItem.getClassId())).map(m -> m.getAttendMark()).collect(Collectors.toList());

//            logger.debug("attend marks count :" + attendMarkList.size());

            // all attend record of member
            List<ClassAttendRecord> memberAttend = classAttendRecordList.stream().filter(r -> r.getClassId().equals(classItem.getClassId())).collect(Collectors.toList());

            if (memberAttend.size() > 0) {
                classItem.setAttend(true);
            }

            classItem.setClassDates(new ArrayList<>());

            int absentCount = 0;
            int passClassCount = 0;

            for (ClassDateInfo dateItem: dateInfoList) {

//                logger.debug("date: " + dateItem.getClassDate());

                int latestListenFlag = 0;
                boolean needMakeup = false;
                boolean complete = false;

                if (dateItem.getClassDate().compareTo(now) < 0) {
                    passClassCount++;

                    long noMakeupNum = memberAttend.stream().filter(a -> a.getClassWeeksNum() == dateItem.getClassWeeksNum() && attendMarkList.contains(a.getAttendMark())).count();

//                    logger.debug("no need make up num: " + noMakeupNum);

                    if (noMakeupNum == 0) {
                        needMakeup = true;
                        absentCount++;
                    }
                }

                List<ClassMakeupRecord> memberMakeupRecord = classMakeupRecordList.stream().filter(m -> m.getClassId().equals(classItem.getClassId()) && m.getClassWeeksNum() == dateItem.getClassWeeksNum()).collect(Collectors.toList());

                if (memberMakeupRecord.size() > 0) {
                    latestListenFlag = memberMakeupRecord.get(0).getLatestListenFlag();
                    complete = memberMakeupRecord.get(0).isMakeUpClassCompleted();
                }

                AnonymousMakeupClassDate date = new AnonymousMakeupClassDate(dateItem.getClassWeeksNum(), CommonUtils.DateTime2DateString(dateItem.getClassDate()), dateItem.getClassContent(), dateItem.getClassDayOfWeek(), dateItem.getMakeupFileName(), dateItem.getFileTimeTotalLength(), latestListenFlag, needMakeup, complete);

                if (dateItem.getMakeupFileProcessStatus() != null && !dateItem.getMakeupFileProcessStatus().equals("轉檔成功")) {
                    date.setMakeupFileName(null);
                    date.setFileTimeTotalLength(0);
                }

                classItem.getClassDates().add(date);
            }

//            logger.debug("pass: " + passClassCount + "/absent: " + absentCount);

            classItem.setAbsentCount(absentCount);

            if (passClassCount > 0) {
                classItem.setAttendRate((passClassCount - absentCount) * 100 / passClassCount);
            }
        }

        return classInfoList;
    }

    // 取得報名表資料
    public Printer011Response getClassEnrollFormList(String classId) {

        int errCode = 200;
        String errMsg = "success";
        List<ClassEnrollForm> items = classEnrollFormRepo.GetClassEnrollFormListByClassId2(classId);

        if (items.size() == 0) {

            errCode = 400;
            errMsg = "List is empty";
        }

        return new Printer011Response(
                errCode,
                errMsg,
                items
        );
    }

    // 依classId取得學員報名表
    public List<ClassGroup002Object> getMemberListByClassId(String classId) {

        return classEnrollFormRepo.GetClassEnrollFormListByClassId3(classId);
    }

    // 更新組別
    public BaseResponse changeClassGroupId(ClassGroup003Request classGroup003Request) {

        int errCode = 200;
        String errMsg = "Success";

        List<ClassEnrollForm> classEnrollFormList =
                classEnrollFormRepo.GetClassEnrollFormListByClassEnrollFormIdList(classGroup003Request.getEnrollFormIds());

        if (classEnrollFormList.size() > 0) {
            // sort as input
            Collections.sort(classEnrollFormList, Comparator.comparing(item -> classGroup003Request.getEnrollFormIds().indexOf(item.getClassEnrollFormId())));

//            logger.debug(classEnrollFormList.get(0).getClassId() + "/" + classGroup003Request.getClassGroupId());
            String max = classEnrollFormRepo.GetMaxMemberGroupNum(classEnrollFormList.get(0).getClassId(), classGroup003Request.getClassGroupId());

            if (max == null) {
                max = "0";
            }

//            logger.debug("max: " + max);

            int current = Integer.parseInt(max) + 1;

            for (ClassEnrollForm classEnrollForm : classEnrollFormList) {
//                logger.debug("group num: " + current);
                classEnrollForm.setClassGroupId(String.valueOf(classGroup003Request.getClassGroupId()));
                classEnrollForm.setMemberGroupNum(String.valueOf(current));
                current++;
            }

            classEnrollFormRepo.saveAll(classEnrollFormList);
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 移出組別, 將classGroupId清空
    public BaseResponse clearClassGroupId(ClassGroup004Request classGroup004Request) {

        int errCode = 200;
        String errMsg = "Success";

        List<ClassEnrollForm> classEnrollFormList =
                classEnrollFormRepo.GetClassEnrollFormListByClassEnrollFormIdList(classGroup004Request.getEnrollFormIds());

        for (ClassEnrollForm classEnrollForm : classEnrollFormList) {
            classEnrollForm.setClassGroupId(null);
            classEnrollForm.setMemberGroupNum(null);
        }

        classEnrollFormRepo.saveAll(classEnrollFormList);

        return new BaseResponse(errCode, errMsg);
    }


    public List<ClassGroup001Object> getOpeningClassByMemberId(String unitId, String memberId) {

        return classEnrollFormRepo.GetCurrentOpeningClassEnrolled(unitId, memberId);
    }
}
