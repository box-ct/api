package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.EventAbbotSign.SignUnit;
import tw.org.ctworld.meditation.beans.EventAbbotSign.UpdateEventUnitDateRequest;
import tw.org.ctworld.meditation.beans.EventEnrollMember.EnrollSigned;
import tw.org.ctworld.meditation.beans.EventEnrollMember.ModLeaderRequest;
import tw.org.ctworld.meditation.beans.Makeup.Unit;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain002Object;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain006Object;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EventMainService {
    private static final Logger logger = LoggerFactory.getLogger(EventMainService.class);

    @Autowired
    private EventMainRepo eventMainRepo;

    @Autowired
    private EventPgmOcaRepo eventPgmOcaRepo;

    @Autowired
    private EventPgmDefRepo eventPgmDefRepo;

    @Autowired
    private EventUnitInfoRepo eventUnitInfoRepo;

    @Autowired
    private EventEnrollMainRepo eventEnrollMainRepo;


    public List<UnitEventsMaintain002Object> getEventsByAttendUnit(String unitId) {

        List<UnitEventsMaintain002Object> events = eventMainRepo.GetEventMainAttendByUnitId(unitId);

        if (events.size() > 0) {
            List<String> eventIds = events.stream().map(e -> e.getEventId()).collect(Collectors.toList());

            List<EventUnitInfo> units = eventUnitInfoRepo.GetByEventIdList(eventIds, unitId);

            List<EnrollSigned> signed = eventEnrollMainRepo.GetSigned(eventIds, unitId);
//            logger.debug("signed: " + signed.size());
            for (UnitEventsMaintain002Object event : events) {
//                logger.debug("event id: " + event.getEventId());
                Integer sum = units.stream().filter(u -> u.getEventId().equals(event.getEventId())).mapToInt(u -> u.getPeopleForecast()).sum();
                event.setPeopleForecast(sum);

                if (event.getIsNeedAbbotSigned()) {
//                    logger.debug("need abbot signed");
                    long signedCount = signed.stream().filter(s -> s.getEventId().equals(event.getEventId()) && s.getIsAbbotSigned() == true).count();
                    long unsignedCount = signed.stream().filter(s -> s.getEventId().equals(event.getEventId()) && s.getIsAbbotSigned() == false).count();

                    event.setAbbotSignedCount((int) signedCount);
                    event.setNotAbbotSignedCount((int) unsignedCount);
                }

                long waitForCtApproved = signed.stream().filter(s -> s.getEventId().equals(event.getEventId()) && !s.getIsCtApproved()).count();
//                logger.debug("wait for ct: " + waitForCtApproved);
                event.setWaitCtApprovedCount((int) waitForCtApproved);
            }
        }

        return events;
    }

    public tw.org.ctworld.meditation.beans.EventEnrollMember.EventMain getEventMain(String eventId) {

        // get event
        EventMain eventMainObj = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMainObj != null) {
            // get pgm oca
            List<EventPgmOca> pgmOcaList = eventPgmOcaRepo.GetEventPgmOcaByEventId(eventId);

            List<String> pgmIds = pgmOcaList.stream().map(e -> e.getPgmId()).distinct().collect(Collectors.toList());

            //get pgm def
            List<EventPgmDef> pgmDefList = eventPgmDefRepo.GetEventPgmDefListByPgmIdList(pgmIds);

            // packaging
            tw.org.ctworld.meditation.beans.EventEnrollMember.EventMain eventMain = new tw.org.ctworld.meditation.beans.EventEnrollMember.EventMain(eventMainObj);

//            pgmOcaList.sort(new Comparator<EventPgmOca>() {
//                @Override
//                public int compare(EventPgmOca eventPgmOca, EventPgmOca t1) {
//                    try {
//                        return eventPgmOca.getPgmUiOrderNum() - t1.getPgmUiOrderNum();
//                    } catch (Exception e) {
//                        return 0;
//                    }
//                }
//            });

            eventMain.setPgmList(new ArrayList<>());

            for (EventPgmOca oca : pgmOcaList) {
                Optional<EventPgmDef> opt = pgmDefList.stream().filter(e -> e.getPgmId().equals(oca.getPgmId())).findAny();

                if (opt.isPresent()) {
                    eventMain.getPgmList().add(new tw.org.ctworld.meditation.beans.EventEnrollMember.EventPgm(oca, opt.get()));
                } else {
                    eventMain.getPgmList().add(new tw.org.ctworld.meditation.beans.EventEnrollMember.EventPgm(oca, null));
                }
            }

            return eventMain;
        } else {
            return null;
        }
    }

    public void updateEventStatus() {
        Date date = new Date();
        eventMainRepo.setEnrollEndDayDue(date);
        eventMainRepo.setChangeEndDayDue(date);
        eventMainRepo.setFinalDayDue(date);
    }

    public List<SignUnit> getUnitsByEvent(String eventId) throws NotFoundException {

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) throw new NotFoundException();

        List<String> eventIds = new ArrayList<>();
        eventIds.add(eventMain.getEventId());

        List<EventUnitInfo> unitInfoList = eventUnitInfoRepo.GetByEventIdList(eventIds, null);

        List<EnrollSigned> signed = eventEnrollMainRepo.GetSigned(eventIds, null);

        logger.debug("total enroll: " + signed.size());

        List<SignUnit> unitList = new ArrayList<>();
        for (EventUnitInfo unitInfo : unitInfoList) {
            SignUnit unit = new SignUnit(unitInfo);
            unitList.add(unit);

            if (eventMain.getIsNeedAbbotSigned() != null && eventMain.getIsNeedAbbotSigned() == true) {
                unit.setIsNeedAbbotSigned(true);
            } else {
                unit.setIsNeedAbbotSigned(false);
            }

            if (unit.getIsNeedAbbotSigned()) {
                long signedCount = signed.stream().filter(s -> s.getUnitId().equals(unit.getUnitId()) && s.getIsAbbotSigned() == true).count();
                long unsignedCount = signed.stream().filter(s -> s.getUnitId().equals(unit.getUnitId()) && s.getIsAbbotSigned() == false).count();

                unit.setAbbotSignedCount((int) signedCount);
                unit.setNotAbbotSignedCount((int) unsignedCount);
            }

            long masterCount = signed.stream().filter(s -> s.getUnitId().equals(unit.getUnitId()) && s.getIsMaster() == true).count();
            long notMasterCount = signed.stream().filter(s -> s.getUnitId().equals(unit.getUnitId()) && s.getIsMaster() == false).count();

            unit.setMasterEnrollCount((int) masterCount);
            unit.setNotMasterEnrollCount((int) notMasterCount);

            logger.debug(unit.getUnitId() + ":" + unit.getAbbotSignedCount() + "," + unit.getNotAbbotSignedCount() + "," + unit.getMasterEnrollCount() + "," + unit.getNotMasterEnrollCount());
        }

        return unitList;
    }

    public SignUnit getUnitByEvent(String eventId, String unitId) throws NotFoundException {

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) throw new NotFoundException("活動不存在");

        List<String> eventIds = new ArrayList<>();
        eventIds.add(eventMain.getEventId());

        List<EventUnitInfo> unitInfoList = eventUnitInfoRepo.GetByEventIdList(eventIds, unitId);

        List<EnrollSigned> signed = eventEnrollMainRepo.GetSigned(eventIds, unitId);

        logger.debug("total enroll: " + signed.size());

        if (unitInfoList.size() <= 0) throw new NotFoundException("活動單位不存在");

        EventUnitInfo unitInfo = unitInfoList.get(0);

        SignUnit unit = new SignUnit(unitInfo);

        if (eventMain.getIsNeedAbbotSigned() != null && eventMain.getIsNeedAbbotSigned() == true) {
            unit.setIsNeedAbbotSigned(true);
        } else {
            unit.setIsNeedAbbotSigned(false);
        }

        if (unit.getIsNeedAbbotSigned()) {
            long signedCount = signed.stream().filter(s -> s.getIsAbbotSigned() == true).count();
            long unsignedCount = signed.stream().filter(s -> s.getIsAbbotSigned() == false).count();

            unit.setAbbotSignedCount((int) signedCount);
            unit.setNotAbbotSignedCount((int) unsignedCount);
        }

        long masterCount = signed.stream().filter(s -> s.getIsMaster() == true).count();
        long notMasterCount = signed.stream().filter(s -> s.getIsMaster() == false).count();

        unit.setMasterEnrollCount((int) masterCount);
        unit.setNotMasterEnrollCount((int) notMasterCount);

        logger.debug(unit.getUnitId() + ":" + unit.getAbbotSignedCount() + "," + unit.getNotAbbotSignedCount() + "," + unit.getMasterEnrollCount() + "," + unit.getNotMasterEnrollCount());

        return unit;
    }

    public void updateEventUnitDate(HttpSession session, UpdateEventUnitDateRequest request) throws NotFoundException {
        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(request.getEventId());

        if (eventMain == null) throw new NotFoundException();

        List<EventUnitInfo> unitInfoList = eventUnitInfoRepo.GetByEventIdUnitIds(request.getEventId(), request.getUnitIds());

        if (unitInfoList.size() > 0) {
            Date changeEndDate = CommonUtils.ParseDate(request.getChangeEndDate());
            Date enrollEndDate = CommonUtils.ParseDate(request.getEnrollEndDate());

            for (EventUnitInfo eventUnitInfo : unitInfoList) {
                eventUnitInfo.setChangeEndDate(changeEndDate);
                eventUnitInfo.setEnrollEndDate(enrollEndDate);

                eventUnitInfo.setUpdater(session);
            }

            eventUnitInfoRepo.saveAll(unitInfoList);
        }
    }
}
