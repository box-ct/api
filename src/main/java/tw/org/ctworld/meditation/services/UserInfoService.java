package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.Auth.Auth001Response;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.ManagedClassRequest;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.models.ClassInfo;
import tw.org.ctworld.meditation.models.UserInfo;
import tw.org.ctworld.meditation.models.UnitInfo;
import tw.org.ctworld.meditation.repositories.UserInfoRepo;
import tw.org.ctworld.meditation.repositories.UnitInfoRepo;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserInfoService {

    private static final Logger logger = LoggerFactory.getLogger(UserInfoService.class);

    @Autowired
    private UserInfoRepo userInfoRepo;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UnitInfoRepo unitInfoRepo;

    @Autowired
    private ClassInfoService classInfoService;

    // log in
    public UserInfo findLoginUserInfo(String userId, String unitId) {

        UserInfo info = userInfoRepo.FindLoginUserInfo(userId, unitId);

        return info;
    }

    // auth001
    public Auth001Response auth001(HttpSession session) {

        String userId = (String) session.getAttribute(UserSession.Keys.UserId.getValue());
        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        UserInfo info = userInfoRepo.FindLoginUserInfo(userId, unitId);
        UnitInfo unit = unitInfoRepo.GetClassUnitInfoByUnitId(unitId);

        boolean isProxy = false;
        String xUserName = "";

        if ((String) session.getAttribute(UserSession.Keys.XUserName.getValue()) != null) {

            isProxy = true;
            xUserName = (String) session.getAttribute(UserSession.Keys.XUserName.getValue());
        }

        Auth001Response auth001Response = new Auth001Response(
                200,
                "success",
                info.getUserName(),
                info.getUserId(),
                unit,
                (String) session.getAttribute(UserSession.Keys.UserIP.getValue()),
                new Auth001Response.Auth001Object(
                        info.getRoleType(),
                        info.getIsSeeMemberData(),
                        info.getIsSeeCreateClass(),
                        info.getIsSeeBarcodeCheckIn(),
                        info.getIsSeeClassAttendRecord(),
                        info.getIsSeeClassGraduateStats(),
                        info.getIsSeeClassUpGrade(),
                        info.getIsSeePrintReport(),
                        info.getIsSeeHomeClass(),
                        info.getIsSeeIDCheck(),
                        info.getIsSeeCurrentClassMembers(),
                        info.getIsSeePreviousClassMembers(),
                        info.getIsSeeCurrentClassLeaders(),
                        info.getIsSeeMultpleClassMembersAndLeaders(),
                        info.getIsSeeEventRecord(),
                        info.getIsSeeClassStats(),
                        info.getIsSeeClassCurrentCheckInStats(),
                        info.getIsSeeClassPeriodCheckInStats(),
                        info.getIsSeeClassStatsByClassName(),
                        info.getIsSeeClassStatsWithLinechart(),
                        info.getIsSeeOnlineUser(),
                        info.getIsSeeNews(),
                        info.getIsSeeAuthorize(),
                        info.getIsSeeUnitInfo(),
                        info.getIsSeeImportClassRecord(),
                        info.getIsClassLeaderSetting(),
                        info.getIsClassMemberGroupSetting(),
                        info.getIsCertPrint(),
                        info.getIsMakeupFilesManage(),
                        info.getIsSysCode(),
                        info.getIsMemberManageByCT(),
                        info.getIsMakeupFileManageByCT()
                ),
                isProxy,
                new Auth001Response.Auth001Object2(xUserName)
        );

        return auth001Response;
    }

    // auth004
    public BaseResponse auth004(HttpSession session, String id) {

        int errCode = 200;
        String errMsg = "success";

        UserInfo loginUserInfo = userInfoRepo.FindLoginUserInfo(Long.valueOf(id));

        UnitInfo unitInfo = unitInfoRepo.GetClassUnitInfoByUnitId(loginUserInfo.getUnitId());

        if (loginUserInfo != null && unitInfo != null) {

            session.setAttribute(UserSession.Keys.XUserName.getValue(), session.getAttribute(UserSession.Keys.UserName.getValue()));

            session.setAttribute(UserSession.Keys.UnitId.getValue(), loginUserInfo.getUnitId());
            session.setAttribute(UserSession.Keys.UnitName.getValue(), loginUserInfo.getUnitName());
            session.setAttribute(UserSession.Keys.RoleType.getValue(), loginUserInfo.getRoleType());
            session.setAttribute(UserSession.Keys.UserId.getValue(), loginUserInfo.getUserId());
            session.setAttribute(UserSession.Keys.UserName.getValue(), loginUserInfo.getUserName());
            session.setAttribute(UserSession.Keys.UserJobTitle.getValue(), loginUserInfo.getUserJobTitle());
            session.setAttribute(UserSession.Keys.MobileNum.getValue(), loginUserInfo.getMobileNum());
            session.setAttribute(UserSession.Keys.IsDisabled.getValue(), loginUserInfo.getIsDisabled());
//            session.setAttribute(UserSession.Keys.Group.getValue(), unitGroup);
//            session.setAttribute(UserSession.Keys.UserIP.getValue(), request.getRemoteAddr());
            session.setAttribute(UserSession.Keys.LogInTime.getValue(), LocalDateTime.now());
            session.setAttribute(UserSession.Keys.LoginUserInfo.getValue(), loginUserInfo);
            session.setAttribute(UserSession.Keys.TimeZone.getValue(), unitInfo.getTimezone());
            session.setAttribute(UserSession.Keys.MemberId.getValue(), loginUserInfo.getMemberId());
            logger.debug("UnitId : " + session.getAttribute(UserSession.Keys.UnitId.getValue()));
            logger.debug("UnitName : " + session.getAttribute(UserSession.Keys.UnitName.getValue()));
        }else {

            errCode = 400;
            errMsg = "代登單位資料短缺!!";
        }

        return new BaseResponse(errCode, errMsg);
    }

    // sys - users - get users by role type
    public List<UserInfo> getAllUserInfo(HttpSession session) {
        String roleType = (String) session.getAttribute(UserSession.Keys.RoleType.getValue());

        List<UserInfo> result;

        switch (roleType) {
            case "系統管理員":
            case "本山管理單位":
                result = userInfoRepo.findAuthInfoByAll();
                break;
            case "精舍使用者":
                String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
                 result = userInfoRepo.findAuthInfoByUnitIdForRecord(unitId);
                break;
            default:
                result = new ArrayList<>();
                break;

        }

        return result;
    }

    // sys - users - create new user (return [true: success, false: failed/already exists])
    public boolean createUser(HttpSession session, UserInfo newUser) {
        if (userInfoRepo.FindLoginUserInfo(newUser.getUserId(), newUser.getUnitId()) != null) {
            return false;
        } else {
            newUser.setCreatorAndUpdater(session);
            userInfoRepo.save(newUser);

            return true;
        }
    }

    // sys - users - update user info (return [true: success, false: failed/not exists])
    public boolean updateUser(HttpSession session, UserInfo user) {
        UserInfo iUser = userInfoRepo.FindLoginUserInfo(user.getId());
        if (iUser != null) {
            iUser.copy(user);
            iUser.setUpdater(session);
            userInfoRepo.save(iUser);
            return true;
        } else {
            return false;
        }
    }

    // sys - users - delete user info (return [true: success, false: failed/not exists])
    public boolean deleteUser(UserInfo user) {
        UserInfo iUser = userInfoRepo.FindLoginUserInfo(user.getId());
        if (iUser != null) {
            userInfoRepo.delete(iUser);
            return true;
        } else {
            return false;
        }
    }

    // class records
    public List<String> getManagedClassIds(String userId, String unitId) {
//        logger.debug(userId + "|" + unitId);
        UserInfo user = userInfoRepo.FindLoginUserInfo(userId, unitId);

        List<String> classIds = new ArrayList<>();

        if (user != null && user.getManagedClassList() != null && !user.getManagedClassList().isEmpty()) {

//            logger.debug(user.getManagedClassList());

            String[] classes =  user.getManagedClassList().split("\\/");

            for (int i = 0; i < classes.length; i++) {

//                logger.debug(classes[i]);
                if (classes[i].contains(":")) {
                    classIds.add(classes[i].substring(0, classes[i].indexOf(":")));
                }
            }
        }

        return classIds;
    }

    public boolean updateManagedClasses(ManagedClassRequest request, HttpSession session) {

        if (request.getId() != null && !request.getId().isEmpty()) {

            UserInfo userInfo = userInfoRepo.FindLoginUserInfo(request.getId());

            if (userInfo != null) {
                if (request.getClassIds() != null && request.getClassIds().size() > 0) {
                    List<ClassInfo> classInfoList = classInfoService.getClassesByClassIdList(request.getClassIds());

                    StringBuilder sb = new StringBuilder();

                    if (classInfoList.size() > 0) {
                        for (ClassInfo classInfo : classInfoList) {
                            sb.append(classInfo.getClassId() + ":" + classInfo.getClassName() + "-第" + classInfo.getClassPeriodNum() + "期|");
                        }

                        userInfo.setManagedClassList(sb.toString());
                    } else {
                        userInfo.setManagedClassList(null);
                    }

                    userInfo.setUpdater(session);

                    userInfoRepo.save(userInfo);

                    return true;
                }
            }
        }

        return false;
    }

}
