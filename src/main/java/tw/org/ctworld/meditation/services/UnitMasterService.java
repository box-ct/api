package tw.org.ctworld.meditation.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort;
import tw.org.ctworld.meditation.beans.UnitMaster.UnitMaster002Data;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.libs.RedundantException;
import tw.org.ctworld.meditation.models.UnitInfo;
import tw.org.ctworld.meditation.models.CtMasterInfo;
import tw.org.ctworld.meditation.models.UnitMasterInfo;
import tw.org.ctworld.meditation.repositories.UnitInfoRepo;
import tw.org.ctworld.meditation.repositories.CtMasterInfoRepo;
import tw.org.ctworld.meditation.repositories.UnitMasterInfoRepo;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service
public class UnitMasterService {
    private static final Logger logger = LoggerFactory.getLogger(ClassInfoService.class);

    @Autowired
    private UnitMasterInfoRepo unitMasterInfoRepo;

    @Autowired
    private UnitInfoRepo unitInfoRepo;

    @Autowired
    private CtMasterInfoRepo ctMasterInfoRepo;

    public List<UnitMasterInfo>
    getUnitMasterByUnitid(String unitid) {
        List<UnitMasterInfo> unitMaster001ObjectList = unitMasterInfoRepo.GetUnitMasterByUnitId(unitid);
        return unitMaster001ObjectList;
    }

//    public UnitMaster001Object getUnitMasterByAliasname(String aliasname){
//        UnitMaster001Object unitMaster001ObjectList= unitMasterInfoRepo.GetUnitMasterByAliasname(aliasname);
//        return unitMaster001ObjectList;
//    }

    public UnitMasterInfo getCtMasterByAliasname(String aliasname) {
        UnitMasterInfo unitMaster = new UnitMasterInfo();
        List<CtMasterInfo> ctMasterInfo = ctMasterInfoRepo.GetCtMasterByAliasname(aliasname);
        if (ctMasterInfo.size() > 0) {

            unitMaster.setJobOrder(ctMasterInfo.get(0).getJobOrder());
            unitMaster.setPreceptOrder(ctMasterInfo.get(0).getPreceptOrder());
            unitMaster.setMobileNum(ctMasterInfo.get(0).getMobileNum());
            unitMaster.setJobTitle(ctMasterInfo.get(0).getJobTitle());
            unitMaster.setIsTreasurer(ctMasterInfo.get(0).getIsTreasurer());
            unitMaster.setIsAbbot(ctMasterInfo.get(0).getIsAbbot());
            unitMaster.setMasterPreceptTypeName(ctMasterInfo.get(0).getMasterPreceptTypeName());
            unitMaster.setMasterName(ctMasterInfo.get(0).getMasterName());
            unitMaster.setMasterId(ctMasterInfo.get(0).getMasterId());
        }
        return unitMaster;
    }

    public void UpdateUnitMasterInfos(HttpSession session, String unitid, UnitMaster002Data umobj) {
        unitMasterInfoRepo.DeleteUnitMastersByUnitId(unitid);
        UnitInfo cf = unitInfoRepo.GetClassUnitInfoByUnitId(unitid);
        cf.setUpdater(session);
        cf.setFundUnitId(umobj.getData().getFundUnitId());
        cf.setUnitName(umobj.getData().getUnitName());
        cf.setCertUnitName(umobj.getData().getCertUnitName());
        cf.setCertUnitEngName(umobj.getData().getCertUnitEngName());
        cf.setUnitPhoneNum(umobj.getData().getUnitPhoneNum());
        cf.setUnitFaxNum(umobj.getData().getUnitFaxNum());
//        cf.setTimezone(umobj.getData().getTimezone());
        cf.setUnitPhoneNumCode(umobj.getData().getUnitPhoneNumCode());
        cf.setUnitMobileNum(umobj.getData().getUnitMobileNum());
        cf.setUnitEmail(umobj.getData().getUnitEmail());
        cf.setDistrict(umobj.getData().getDistrict());
        cf.setUnitMailingAddress(umobj.getData().getUnitMailingAddress());
        cf.setStateName(umobj.getData().getStateName());
        cf.setAbbotName(umobj.getData().getAbbotName());
        cf.setAbbotId(umobj.getData().getAbbotId());
        cf.setAbbotEngName(umobj.getData().getAbbotEngName());
        unitInfoRepo.save(cf);
        //
        if (umobj.getData().getUnitMasterInfos().size() > 0) {
            List<UnitMasterInfo> infos = new ArrayList<>();
            for (UnitMasterInfo ele : umobj.getData().getUnitMasterInfos()) {
                UnitMasterInfo info = new UnitMasterInfo();
                info.setMasterId(ele.getMasterId());
                info.setMasterName(ele.getMasterName());
                info.setMasterPreceptTypeName(ele.getMasterPreceptTypeName());
                info.setIsAbbot(ele.getIsAbbot());
                info.setIsTreasurer(ele.getIsTreasurer());
                info.setJobTitle(ele.getJobTitle());
                info.setMobileNum(ele.getMobileNum());
                info.setPreceptOrder(ele.getPreceptOrder());
                info.setJobOrder(ele.getJobOrder());
                info.setUnitId(unitid);
                info.setCreatorAndUpdater(session);
                infos.add(info);
            }
            unitMasterInfoRepo.saveAll(infos);
        }
    }

//    public void LogObjJsonstr(Object obj) {
//        ObjectMapper mapperObj = new ObjectMapper();
//        try {
//            String jsonStr = mapperObj.writeValueAsString(obj);
//            logger.debug("**** json result: " + jsonStr);
//        } catch (Exception e) {
//            logger.debug("--- LogObjJsonstr error : " + e.getMessage());
//        }
//    }

    public List<MasterInfoShort> getUnitMasterByMasterId(String unitId, String masterId) {
        List<MasterInfoShort> masterInfoShorts = unitMasterInfoRepo.GetUnitMasterByMasterId(unitId, masterId);

        if (masterInfoShorts.size() <= 0) {
            masterInfoShorts = ctMasterInfoRepo.GetCtMasterShortByMasterId(masterId);
        }

        return masterInfoShorts;
    }

    public void addUnitMasterByMasterId(String unitId, String masterId, MasterInfoShort masterInfoShort, HttpSession session) throws RedundantException {
        List<UnitMasterInfo> unitMasterInfoList = unitMasterInfoRepo.GetByMasterId(unitId, masterId);

        if (unitMasterInfoList.size() <= 0) {
            masterInfoShort.setMasterId(masterId);
            UnitMasterInfo unitMasterInfo = new UnitMasterInfo();

            unitMasterInfo.copy(masterInfoShort);
            unitMasterInfo.setCreatorAndUpdater(session);

            unitMasterInfoRepo.save(unitMasterInfo);
        } else {
            throw new RedundantException();
        }
    }


    public void editUnitMasterByMasterId(String unitId, String masterId, MasterInfoShort masterInfoShort, HttpSession session) throws NotFoundException {
        List<UnitMasterInfo> unitMasterInfoList = unitMasterInfoRepo.GetByMasterId(unitId, masterId);

        if (unitMasterInfoList.size() > 0) {
            masterInfoShort.setMasterId(masterId);
            UnitMasterInfo unitMasterInfo = unitMasterInfoList.get(0);

            unitMasterInfo.copy(masterInfoShort);
            unitMasterInfo.setUpdater(session);

            unitMasterInfoRepo.save(unitMasterInfo);
        } else {
            throw new NotFoundException();
        }
    }

    public void delUnitMasterByMasterId(String unitId, String masterId) {
        List<UnitMasterInfo> unitMasterInfoList = unitMasterInfoRepo.GetByMasterId(unitId, masterId);

        if (unitMasterInfoList.size() > 0) {
            unitMasterInfoRepo.deleteAll(unitMasterInfoList);
        }
    }
}
