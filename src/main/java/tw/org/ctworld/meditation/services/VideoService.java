package tw.org.ctworld.meditation.services;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.xmlbeans.impl.jam.internal.DirectoryScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.models.SysCode;

import java.io.*;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class VideoService {

    private static final Logger logger = LoggerFactory.getLogger(VideoService.class);

    final static String[] SUPPORTED_FORMAT = new String[]{"mp3", "wav", "aac", "wma", "flac", "mp4", "ogg", "webm", "avi", "mov", "flv", "wmv", "mk4", "3gp", "rmvb", "mkv", "mts"};
    final static int VIDEO_START_INDEX = 5;

    @Autowired
    private SysCodeService sysCodeService;

    public boolean isSupported(String fileExt) {
        if (fileExt != null && !fileExt.isEmpty()) {
            String low = fileExt.toLowerCase();

            for(int i = 0; i < SUPPORTED_FORMAT.length; i++) {
                if (low.equals(SUPPORTED_FORMAT[i])) {
                    return true;
                }
            }
        }

        return false;
    }


    public boolean isVideo(String fileExt) {
        if (fileExt != null && !fileExt.isEmpty()) {
            String low = fileExt.toLowerCase();

            for(int i = 0; i < SUPPORTED_FORMAT.length; i++) {
                if (low.equals(SUPPORTED_FORMAT[i])) {
                    if (i >= VIDEO_START_INDEX) {
                        return true;
                    }
                }
            }
        }

        return false;
    }


    public int joinVideos(List<String> fileNames, String fileName) {

        try {
            StringBuilder sb = new StringBuilder();
            // -y: yes to overwrite output file, -v quiet: no warning
            sb.append("ffmpeg -y -v quiet");

            for (int i = 0; i < fileNames.size(); i++) {
                sb.append(" -i");
                sb.append(" " + sysCodeService.getOriginalMakeupFilePath() + "/" + fileNames.get(i));
            }

            sb.append(" -filter_complex \"");

            for (int i = 0; i < fileNames.size(); i++) {
                sb.append("[" + i + ":v]scale=720x480,setsar=1[v" + i + "];");
            }

            for (int i = 0; i < fileNames.size(); i++) {
                sb.append("[v" + i + "][" + i + ":a]");
            }

            sb.append(" concat=n=" + fileNames.size() + ":v=1:a=1 [v] [a]\" -map \"[v]\" -map \"[a]\"");

            sb.append(" -vcodec libx264 -acodec aac -strict -2 " + sysCodeService.getMakeupFilePath() + "/" + fileName);

            logger.debug(sb.toString());

            Process proc = Runtime.getRuntime().exec(new String[]{"bash", "-c", sb.toString()});
            proc.waitFor();

            logger.debug("finish video join, exit value: " + proc.exitValue());

            return proc.exitValue();

        } catch (Exception e) {
            e.printStackTrace();

            return 1;
        }

    }


    public int joinAudios(List<String> fileNames, String fileName) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("ffmpeg -y -v quiet");

            for (int i = 0; i < fileNames.size(); i++) {
                sb.append(" -i");
                sb.append(" " + sysCodeService.getOriginalMakeupFilePath() + "/" + fileNames.get(i));
            }

            sb.append(" -filter_complex \"");

            for (int i = 0; i < fileNames.size(); i++) {
                sb.append("[" + i + ":a]");
            }

            sb.append(" concat=n=" + fileNames.size() + ":v=0:a=1 [a]\" -map \"[a]\"");
            sb.append(" -metadata artist=\"中台禪修\"");
            sb.append(" -metadata album=\"中台禪修\"");
            sb.append(" -metadata year=\"" +  Calendar.getInstance().get(Calendar.YEAR) + "\"");
            sb.append(" -metadata track=\"1\"");
            sb.append(" -metadata title=\"" + fileName + "\"");
            sb.append(" -codec:a libmp3lame -q:a 9 ");
            sb.append(sysCodeService.getMakeupFilePath() + "/" + fileName);

            logger.debug(sb.toString());

            Process proc = Runtime.getRuntime().exec(new String[]{"bash", "-c", sb.toString()});
            proc.waitFor();

            logger.debug("finish audio join, exit value: " + proc.exitValue());

            return proc.exitValue();

        } catch (Exception e) {
            e.printStackTrace();

            return 1;
        }
    }


    // delete temp files
//    public void deleteTempFilesOverYear(Date date) {
//        File file = new File(sysCodeService.getOriginalMakeupFilePath());
//        File[] files = file.listFiles();
//
//        for (int i = 0; i < files.length; i++) {
//            File temp = files[i];
//            if (temp.lastModified() <= date.getTime()) {
//                logger.debug("delete " + temp.getName());
//
//                temp.delete();
//            }
//        }
//    }


    // delete files
    public void deleteFile(String fileName) {
        try {
                logger.debug("delete " + fileName);
                File file = new File(fileName);
                Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
            logger.debug(e.getMessage());
        }
    }

    public void deleteFilesLike(String path, String filename) {
        logger.debug(path +"/" + filename);
        final String name = filename.contains(".") ? filename.substring(0, filename.indexOf(".")) : filename;

        File dir = new File(path);
        File[] files = dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.getName().contains(name)) {
                    return true;
                } else {
                    return false;
                }
            }
        });

        for (int i = 0; i < files.length; i++) {
            try {
                Files.deleteIfExists(files[i].toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // free disk space
    public String getFreeSpace() {
        logger.debug("get free space");

        File file = new File(sysCodeService.getMakeupFilePath());
        long usableSpace = file.getUsableSpace();

        logger.debug("usable space: " + usableSpace);

        return displayFileSize(usableSpace);
    }


    public String displayFileSize(long fileSize) {
        if (fileSize <= 0) {
            return "0";
        } else {
            String[] fileUnit = new String[] {"B","KB","MB","GB","TB"};
            int group = (int)(Math.log10(fileSize) / Math.log10(1024));
            return new DecimalFormat("#,##0.#").format(fileSize / Math.pow(1024, group)) + " " + fileUnit[group];
        }
    }


    // get all video/audio files
    public File[] getFiles() {
        logger.debug("get all video/audio files");

        File file = new File(sysCodeService.getMakeupFilePath());

        return file.listFiles();
    }

    public int getFileSizeMB(String fileName) {
        try {
            File file = new File(sysCodeService.getMakeupFilePath() + "/" + fileName);
            logger.debug(fileName);
            logger.debug("length: " + file.length());
            return (int) (file.length() / (1024 * 1024));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getMediaDuration(String fileName) {
        try {
            StringBuilder sb = new StringBuilder();

            sb.append("ffprobe -i");

            sb.append(" " + sysCodeService.getMakeupFilePath() + "/" + fileName);

            sb.append(" -show_entries format=duration -v quiet -of csv=\"p=0\"");

            logger.debug(sb.toString());

            Process proc = Runtime.getRuntime().exec(new String[]{"bash", "-c", sb.toString()});

            StringBuilder sbOut = new StringBuilder();

            InputStream in = proc.getInputStream();

            int c;

            while ((c = in.read()) != -1) {
                sbOut.append((char)c);
            }

            in.close();

            String s = sbOut.toString();

            logger.debug("output: " + s);

            return (int)Double.parseDouble(s);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


}

