package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.models.EventEnrollMain;
import tw.org.ctworld.meditation.models.EventEnrollPgm;
import tw.org.ctworld.meditation.models.EventFormForCt;
import tw.org.ctworld.meditation.repositories.EventEnrollMainRepo;
import tw.org.ctworld.meditation.repositories.EventEnrollPgmRepo;
import tw.org.ctworld.meditation.repositories.EventFormForCtRepo;
import tw.org.ctworld.meditation.repositories.EventMainRepo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class CtTableSyncService {

    private static final Logger logger = LoggerFactory.getLogger(CtTableSyncService.class);

    @Autowired
    private EventMainRepo eventMainRepo;

    @Autowired
    private EventEnrollMainRepo eventEnrollMainRepo;

    @Autowired
    private EventEnrollPgmRepo eventEnrollPgmRepo;

    @Autowired
    private EventFormForCtRepo eventFormForCtRepo;


    public void updateEventFormForCt(boolean isAll) {
        logger.debug("updateEventFormForCt");

        List<String> eventIds = eventMainRepo.GetEventIdsForSync();

        logger.debug("event size:" + eventIds.size());
        if (eventIds.size() > 0) {
            List<EventEnrollMain> enrollMains = eventEnrollMainRepo.GetWaitForSync(eventIds, isAll);

            logger.debug("enroll size:" + enrollMains.size());
            if (enrollMains.size() > 0) {
                List<String> formAutoAddNums = enrollMains.stream().map(e -> e.getFormAutoAddNum()).collect(Collectors.toList());

                if (formAutoAddNums.size() > 0) {
                    List<EventEnrollPgm> eventEnrollPgms = eventEnrollPgmRepo.GetByFormAutoAddNums(formAutoAddNums);

                    List<EventFormForCt> eventFormForCtList = eventFormForCtRepo.GetEventFormForCtListByFormAutoAddNumList(formAutoAddNums);

                    List<EventFormForCt> ctUpdateList = new ArrayList<>();

                    for (EventEnrollMain enrollMain : enrollMains) {
                        Optional<EventFormForCt> optional = eventFormForCtList.stream().filter(e -> e.getFormAutoAddNum().equals(enrollMain.getFormAutoAddNum())).findAny();

                        List<EventEnrollPgm> pgms = eventEnrollPgms.stream().filter(p -> p.getFormAutoAddNum().equals(enrollMain.getFormAutoAddNum())).collect(Collectors.toList());

                        if (optional.isPresent()) {
                            if (optional.get().getEventFormForCtUpdateDtTm() == null || optional.get().getEventFormForCtUpdateDtTm().compareTo(enrollMain.getUpdateDtTm()) != 0) {
                                optional.get().copyEventEnrollMainAndEventEnrollPgm(enrollMain, pgms);
                                optional.get().setEventFormForCtUpdateDtTm(enrollMain.getUpdateDtTm());

                                ctUpdateList.add(optional.get());
                            }
                        } else {
                            EventFormForCt eventFormForCt = new EventFormForCt();
                            eventFormForCt.copyEventEnrollMainAndEventEnrollPgm(enrollMain, pgms);
                            eventFormForCt.setEventFormForCtCreateDtTm(enrollMain.getCreateDtTm());
                            eventFormForCt.setEventFormForCtUpdateDtTm(enrollMain.getUpdateDtTm());
                            eventFormForCtList.add(eventFormForCt);

                            ctUpdateList.add(eventFormForCt);
                        }
                    }

                    if (enrollMains.size() > 0) {
                        eventEnrollMainRepo.saveAll(enrollMains);
                    }

                    if (ctUpdateList.size() > 0) {
                        eventFormForCtRepo.saveAll(ctUpdateList);
                    }
                }
            }
        }
    }

}
