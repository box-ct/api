package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.EventEnrollMember.SummerDonationRequest;
import tw.org.ctworld.meditation.models.EventSummerDonationRecord;
import tw.org.ctworld.meditation.repositories.EventSummerDonationRecordRepo;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class EventSummerDonationService {

    private static final Logger logger = LoggerFactory.getLogger(EventSummerDonationService.class);

    @Autowired
    EventSummerDonationRecordRepo eventSummerDonationRecordRepo;

    public List<EventSummerDonationRecord> getByMemberId(String memberId) {
        return eventSummerDonationRecordRepo.GetByMemberId(memberId);
    }

    public void addOrUpdateCurrentYear(HttpSession session, SummerDonationRequest request) {

        List<EventSummerDonationRecord> records = eventSummerDonationRecordRepo.GetByMemberIdYear(request.getEnrollUserId(), request.getSummerDonationYear());

        EventSummerDonationRecord record;

        if (records.size() > 0) {
            record = records.get(0);
            record.setUpdater(session);
        } else {
            record = new EventSummerDonationRecord();
            record.setCreatorAndUpdater(session);
        }

        record.copyRequest(request);

        eventSummerDonationRecordRepo.save(record);
    }

    public void delCurrentYear(String memberId) {
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        String currentYear = String.valueOf(localDate.getYear());

        eventSummerDonationRecordRepo.DelByMemberIdYear(memberId, currentYear);
    }

}
