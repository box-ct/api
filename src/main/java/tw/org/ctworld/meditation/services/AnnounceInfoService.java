package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.models.AnnounceInfo;
import tw.org.ctworld.meditation.repositories.AnnounceInfoRepo;
import tw.org.ctworld.meditation.repositories.AnnounceIsReadRecordRepo;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
public class AnnounceInfoService {

    private static final Logger logger = LoggerFactory.getLogger(AnnounceInfoService.class);

    @Autowired
    private AnnounceInfoRepo announceInfoRepo;

    @Autowired
    private AnnounceIsReadRecordRepo announceIsReadRecordRepo;


    // setting - admin - message - get all message in desc order
    public List<AnnounceInfo> query() {
        return announceInfoRepo.findAll(new Sort(Sort.Direction.DESC, "announceDt"));
    }

    // setting - admin - message - create
    public AnnounceInfo create(HttpSession session, AnnounceInfo announceInfo) {

        announceInfo.setMessageId(messageIdGenerator());

        announceInfo.setCreatorAndUpdater(session);

        announceInfo.setAnnounceDt(new Date());

        return announceInfoRepo.save(announceInfo);
    }

    // setting - admin - message - update
    public void update(HttpSession session, AnnounceInfo announceInfo) {

        AnnounceInfo info = announceInfoRepo.findById(announceInfo.getId()).get();

        info.copy(announceInfo);

        info.setUpdater(session);

        announceInfoRepo.save(info);
    }

    // setting - admin - message - delete
    public void delete(int id) {
        announceInfoRepo.deleteById(id);
    }

    // main - get unread message for an unit
    public List<AnnounceInfo> getAnnouncements(String userId) {

        List<String> readList = announceIsReadRecordRepo.FindClassAnnounceIsReadRecordByUserId(userId);

        List<AnnounceInfo> announcements;

        if (readList.isEmpty()) {
            announcements = announceInfoRepo.GetAllMsg();
        } else {
            announcements = announceInfoRepo.GetUnReadMsg(readList);
        }

        return announcements;
    }

    public boolean hasMessageId(String messageId) {
        return announceInfoRepo.HasMessageId(messageId);
    }

    public AnnounceInfo getInfoByMessageId(String messageId) {

        List<AnnounceInfo> results= announceInfoRepo.GetInfoByMessageId(messageId);

        if (results != null && results.size() >= 1)
            return results.get(0);
        else {
            return null;
        }
    }

    // generate message id
    public String messageIdGenerator() {
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int mingoYear = localDate.getYear() - 1911;

        String criteria = "M" + String.valueOf(mingoYear) + String.format("%02d", localDate.getMonthValue());

        String currentMaxValue = announceInfoRepo.GetMaxAssetId(criteria + '%');

        logger.debug("criteria: " + criteria);
        logger.debug("current max: " + currentMaxValue);

        long tempId = 0;

        if (currentMaxValue != null) {

            currentMaxValue = currentMaxValue.substring(1);

            tempId = Long.parseLong(currentMaxValue);
        } else {
            tempId = Long.parseLong(criteria.substring(1)) * 1000;
        }

        tempId++;

        return "M" + Long.toString(tempId);
    }
}
