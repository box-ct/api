package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.EventCategoryDef.*;
import tw.org.ctworld.meditation.models.EventCategoryDef;
import tw.org.ctworld.meditation.models.EventCategorySpecifiedPgmRecord;
import tw.org.ctworld.meditation.repositories.EventCategoryDefRepo;
import tw.org.ctworld.meditation.repositories.EventCategorySpecifiedPgmRecordRepo;

import javax.servlet.http.HttpSession;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static tw.org.ctworld.meditation.libs.CommonUtils.ParseDateTime2TimeZoneDT;

@Service
public class EventCategoryDefService {

    private static final Logger logger = LoggerFactory.getLogger(EventCategoryDefService.class);

    @Autowired
    private EventCategoryDefRepo eventCategoryDefRepo;

    @Autowired
    private EventCategorySpecifiedPgmRecordRepo eventCategorySpecifiedPgmRecordRepo;


    // 取得category_def列表
    public EventCategoryDef002Response getCategoryList(HttpSession session) {

        List<EventCategoryDef002Object> items = eventCategoryDefRepo.GetCategoryList();

        return new EventCategoryDef002Response(200, "success", items);
    }

    // 新增category_def
    public EventCategoryDef003Response addCategory(HttpSession session, EventCategoryDef003Request eventCategoryDef003Request) {

        int errCode = 200;
        String errMsg = "success";
        EventCategoryDef003Object data = null;

        int count = eventCategoryDefRepo.GetEventCategoryDefCountByEventCategoryName(
                eventCategoryDef003Request.getEventCategoryName());

        if (count == 0) {

            String currentMaxValue = eventCategoryDefRepo.GetMaxCategoryId();

            String categoryId = "";

            long tempId = 0;

            if (currentMaxValue == null) {

                categoryId = "EVNT01";
            }else {

                DecimalFormat df = new DecimalFormat("00");

                currentMaxValue = currentMaxValue.substring(4);
                tempId = Long.parseLong(currentMaxValue);
                tempId++;

                categoryId = "EVNT" + df.format(tempId);
            }

            EventCategoryDef eventCategoryDef = new EventCategoryDef();
            eventCategoryDef.setEventCategoryId(categoryId);
            eventCategoryDef.setEventCategoryName(eventCategoryDef003Request.getEventCategoryName());
            eventCategoryDef.setIsCenterAvailable(eventCategoryDef003Request.getIsCenterAvailable());
            eventCategoryDef.setCreatorAndUpdater(session);

            EventCategoryDef mEventCategoryDef = eventCategoryDefRepo.save(eventCategoryDef);

            data = new EventCategoryDef003Object(
                    mEventCategoryDef.getId(),
                    mEventCategoryDef.getEventCategoryId(),
                    mEventCategoryDef.getEventCategoryName(),
                    mEventCategoryDef.getIsCenterAvailable()
            );
        }else {

            errCode = 400;
            errMsg = "活動類別名稱 {" + eventCategoryDef003Request.getEventCategoryName() + "} 已經存在，請重新輸入!";
        }

        return new EventCategoryDef003Response(errCode, errMsg, data);
    }

    // 取得 event_category_def 的詳細資料
    public EventCategoryDef004Response getCategory(HttpSession session, String eventCategoryId) {

        int errCode = 200;
        String errMsg = "success";

        EventCategoryDef eventCategoryDef = eventCategoryDefRepo.GetEventCategoryDefByEventCategoryId(eventCategoryId);

        if (eventCategoryDef == null) {

            errCode = 400;
            errMsg = "找不到event category id :" + eventCategoryId;
            return new EventCategoryDef004Response(
                    errCode, errMsg, "", "", false);
        }else {

            return new EventCategoryDef004Response(errCode, errMsg, eventCategoryDef.getEventCategoryId(),
                    eventCategoryDef.getEventCategoryName(), eventCategoryDef.getIsCenterAvailable());
        }
    }


    // 編輯
    public BaseResponse editCategory(HttpSession session, String eventCategoryId,
                                     EventCategoryDef003Request eventCategoryDef003Request) {

        int errCode = 200;
        String errMsg = "success";

        EventCategoryDef eventCategoryDef = eventCategoryDefRepo.GetEventCategoryDefByEventCategoryId(eventCategoryId);

        if (eventCategoryDef == null) {

            errCode = 400;
            errMsg = "找不到event category id :" + eventCategoryId;
        }else {

            List<EventCategoryDef005Object> eventCategoryDef005ObjectList =
                    eventCategoryDefRepo.GetCategoryListByEventCategoryName(
                            eventCategoryDef003Request.getEventCategoryName());

            switch (eventCategoryDef005ObjectList.size()) {
                case 0:

                    // EventCategoryName沒有重複
                    eventCategoryDef.setEventCategoryName(eventCategoryDef003Request.getEventCategoryName());
                    eventCategoryDef.setIsCenterAvailable(eventCategoryDef003Request.getIsCenterAvailable());

                    eventCategoryDefRepo.save(eventCategoryDef);
                    break;
                case 1:

                    // EventCategoryName重複，判斷是不是同EventCategoryId
                    if (eventCategoryDef005ObjectList.get(0).getEventCategoryId().equals(eventCategoryId)) {

                        eventCategoryDef.setEventCategoryName(eventCategoryDef003Request.getEventCategoryName());
                        eventCategoryDef.setIsCenterAvailable(eventCategoryDef003Request.getIsCenterAvailable());

                        eventCategoryDefRepo.save(eventCategoryDef);
                    }else {

                        errCode = 400;
                        errMsg = "event category name 重複";
                    }
                    break;
                default:

                    errCode = 400;
                    errMsg = "event category name 多筆重複";
                    break;
            }
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 刪除
    public BaseResponse deleteCategory(HttpSession session, String eventCategoryId) {

        int errCode = 200;
        String errMsg = "success";

        EventCategoryDef eventCategoryDef = eventCategoryDefRepo.GetEventCategoryDefByEventCategoryId(eventCategoryId);

        if (eventCategoryDef == null) {

            errCode = 400;
            errMsg = "找不到event category id :" + eventCategoryId;
        }else {

            eventCategorySpecifiedPgmRecordRepo.DeleteEventCategorySpecifiedPgmRecordByEventCategoryId(eventCategoryId);

            eventCategoryDefRepo.DeleteEventCategoryDefByEventCategoryId(eventCategoryId);
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 依eventCategoryId取得event_category_specified_pgm_record中的pgm_def
    public EventCategoryDef007Response getCategorySpecified(HttpSession session, String eventCategoryId) {

        List<EventCategoryDef007Object> items =
                eventCategorySpecifiedPgmRecordRepo.GetEventCategorySpecifiedListByEventCategoryId(eventCategoryId);

        if (items.size() != 0) {

            for (EventCategoryDef007Object item : items) {

                item.setUpdateDtTm(ParseDateTime2TimeZoneDT(session, item.getmUpdateDtTm()));
            }
        }

        return new EventCategoryDef007Response(200, "success", items);
    }


    // 新增pgm_def到pgm_category中 ( api 只取pgmId 應該就夠了)
    public BaseResponse editCategorySpecified(HttpSession session, String eventCategoryId,
                                     EventCategoryDef008Request eventCategoryDef008Request) {

        int errCode = 200;
        String errMsg = "success";

//        eventCategorySpecifiedPgmRecordRepo.DeleteEventCategorySpecifiedPgmRecordByEventCategoryId(eventCategoryId);

        EventCategoryDef eventCategoryDef = eventCategoryDefRepo.GetEventCategoryDefByEventCategoryId(eventCategoryId);

        if (eventCategoryDef == null) {

            errCode = 400;
            errMsg = "找不到event category id :" + eventCategoryId;
        }else {

            List<EventCategorySpecifiedPgmRecord> eventCategorySpecifiedPgmRecordList = new ArrayList<>();

            for (EventCategoryDef008Object eventCategoryDef008Object : eventCategoryDef008Request.getItems()) {

                eventCategorySpecifiedPgmRecordList.add(new EventCategorySpecifiedPgmRecord(
                        eventCategoryDef.getEventCategoryId(),
                        eventCategoryDef.getEventCategoryName(),
                        eventCategoryDef008Object.getPgmCategoryId(),
                        eventCategoryDef008Object.getPgmCategoryName(),
                        eventCategoryDef008Object.getPgmId(),
                        eventCategoryDef008Object.getPgmName()
                ));
            }

            for (EventCategorySpecifiedPgmRecord eventCategorySpecifiedPgmRecord : eventCategorySpecifiedPgmRecordList) {
                eventCategorySpecifiedPgmRecord.setCreatorAndUpdater(session);
            }

            eventCategorySpecifiedPgmRecordRepo.saveAll(eventCategorySpecifiedPgmRecordList);
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 依eventCategoryId取得event_category_specified_pgm_record中的pgm_def
    public BaseResponse deleteCategorySpecified(HttpSession session, String eventCategoryId, String id) {

        eventCategorySpecifiedPgmRecordRepo.DeleteEventCategorySpecifiedPgmRecordById(Long.parseLong(id));

        return new BaseResponse(200, "success");
    }
}
