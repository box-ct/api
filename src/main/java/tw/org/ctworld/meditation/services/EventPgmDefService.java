package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.EventPgmDef.*;
import tw.org.ctworld.meditation.models.EventPgmDef;
import tw.org.ctworld.meditation.repositories.EventPgmDefRepo;

import javax.servlet.http.HttpSession;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateTimeString;
import static tw.org.ctworld.meditation.libs.CommonUtils.ParseDateTime2TimeZoneDT;
import static tw.org.ctworld.meditation.libs.CommonUtils.ParseTime;

@Service
public class EventPgmDefService {

    private static final Logger logger = LoggerFactory.getLogger(EventPgmDefService.class);

    @Autowired
    private EventPgmDefRepo eventPgmDefRepo;


    // 取得pgm_def列表
    public EventPgmDef001Response getPGMList(HttpSession session) {

        List<EventPgmDef001Object> items = eventPgmDefRepo.GetPgmList();

        if (items.size() != 0) {

            for (EventPgmDef001Object item : items) {

                item.setUpdateDtTm(ParseDateTime2TimeZoneDT(session, item.getmUpdateDtTm()));
            }
        }

        return new EventPgmDef001Response(200, "success", items);
    }

    // 新增pgm
    public EventPgmDef002Response addPGM(HttpSession session, EventPgmDef002Request eventPgmDef002Request) {

        int errCode = 200;
        String errMsg = "success";
        String pgmCategoryId = eventPgmDef002Request.getPgmCategoryId();

        EventPgmDef002Object data = null;

        String currentMaxValue = eventPgmDefRepo.GetMaxPgmId(pgmCategoryId + "%");

        String pgmId = "";

        long tempId = 0;

        if (currentMaxValue == null) {

            errCode = 400;
            errMsg = "找不到類別代碼:" + pgmCategoryId;
        }else {

            DecimalFormat df = new DecimalFormat("000");

            currentMaxValue = currentMaxValue.substring(5);
            tempId = Long.parseLong(currentMaxValue);
            tempId++;

            pgmId = pgmCategoryId + df.format(tempId);

            EventPgmDef eventPgmDef = new EventPgmDef();
            eventPgmDef.setPgmCategoryId(eventPgmDef002Request.getPgmCategoryId());
            eventPgmDef.setPgmCategoryName(eventPgmDef002Request.getPgmCategoryName());
            eventPgmDef.setPgmId(pgmId);
            eventPgmDef.setPgmName(eventPgmDef002Request.getPgmName());
            eventPgmDef.setPgmDesc(eventPgmDef002Request.getPgmDesc());
            eventPgmDef.setIsGenPgm(eventPgmDef002Request.getIsGenPgm());
            eventPgmDef.setGenPgmUiType(eventPgmDef002Request.getGenPgmUiType());
            eventPgmDef.setGenPgmInputOption(eventPgmDef002Request.getGenPgmInputOption());
            eventPgmDef.setPgmStartTime(ParseTime(eventPgmDef002Request.getPgmStartTime()));
            eventPgmDef.setPgmEndTime(ParseTime(eventPgmDef002Request.getPgmEndTime()));
            eventPgmDef.setIsCenterAvailable(eventPgmDef002Request.getIsCenterAvailable());
            eventPgmDef.setPgmNote(eventPgmDef002Request.getPgmNote());
            eventPgmDef.setCreatorAndUpdater(session);

            EventPgmDef mEventPgmDef = eventPgmDefRepo.save(eventPgmDef);

            data = new EventPgmDef002Object(
                    mEventPgmDef.getId(),
                    mEventPgmDef.getPgmCategoryId(),
                    mEventPgmDef.getPgmCategoryName(),
                    mEventPgmDef.getPgmId(),
                    mEventPgmDef.getPgmName(),
                    mEventPgmDef.getPgmDesc(),
                    mEventPgmDef.getIsGenPgm(),
                    mEventPgmDef.getGenPgmUiType(),
                    mEventPgmDef.getGenPgmInputOption(),
                    mEventPgmDef.getPgmStartTime(),
                    mEventPgmDef.getPgmEndTime(),
                    mEventPgmDef.getIsCenterAvailable(),
                    mEventPgmDef.getPgmNote()
            );
        }

        return new EventPgmDef002Response(errCode, errMsg, data);
    }

    // 編輯pgm
    public BaseResponse editPGM(HttpSession session, String id, EventPgmDef002Request eventPgmDef002Request) {

        int errCode = 200;
        String errMsg = "success";

        EventPgmDef eventPgmDef = eventPgmDefRepo.GetPgmDefById(Long.valueOf(id));

        if (eventPgmDef == null) {

            errCode = 400;
            errMsg = "找不到event pgm id : " + id;
        }else {

            eventPgmDef.setPgmCategoryId(eventPgmDef002Request.getPgmCategoryId());
            eventPgmDef.setPgmCategoryName(eventPgmDef002Request.getPgmCategoryName());
            eventPgmDef.setPgmId(eventPgmDef002Request.getPgmId());
            eventPgmDef.setPgmName(eventPgmDef002Request.getPgmName());
            eventPgmDef.setPgmDesc(eventPgmDef002Request.getPgmDesc());
            eventPgmDef.setIsGenPgm(eventPgmDef002Request.getIsGenPgm());
            eventPgmDef.setGenPgmUiType(eventPgmDef002Request.getGenPgmUiType());
            eventPgmDef.setGenPgmInputOption(eventPgmDef002Request.getGenPgmInputOption());
            eventPgmDef.setPgmStartTime(ParseTime(eventPgmDef002Request.getPgmStartTime()));
            eventPgmDef.setPgmEndTime(ParseTime(eventPgmDef002Request.getPgmEndTime()));
            eventPgmDef.setIsCenterAvailable(eventPgmDef002Request.getIsCenterAvailable());
            eventPgmDef.setPgmNote(eventPgmDef002Request.getPgmNote());
            eventPgmDef.setUpdater(session);

            Long mId = eventPgmDefRepo.save(eventPgmDef).getId();

            errMsg = "Update EventPgmDef id : " + mId;
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 刪除pgm
    public BaseResponse deletePGM(String id) {

        eventPgmDefRepo.DeletePgmById(Long.valueOf(id));

        return new BaseResponse(200, "success");
    }
}
