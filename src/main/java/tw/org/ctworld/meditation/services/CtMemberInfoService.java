package tw.org.ctworld.meditation.services;

import org.jboss.jandex.ClassInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.Anonymous.Anonymous003Request;
import tw.org.ctworld.meditation.beans.Anonymous.Anonymous004Response;
import tw.org.ctworld.meditation.beans.Anonymous.Anonymous005_1Object;
import tw.org.ctworld.meditation.beans.Anonymous.Anonymous005_2Object;
import tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort;
import tw.org.ctworld.meditation.beans.EventEnrollMember.*;
import tw.org.ctworld.meditation.beans.Member.*;
import tw.org.ctworld.meditation.beans.MemberInfos.*;
import tw.org.ctworld.meditation.beans.Sys.SearchMember;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.libs.UnacceptableException;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.libs.CommonUtils.*;

@Service
public class CtMemberInfoService {

    private static final Logger logger = LoggerFactory.getLogger(CtMemberInfoService.class);

    @Autowired
    private CtMemberInfoRepo ctMemberInfoRepo;

    @Autowired
    private CtMemberNoteRepo ctMemberNoteRepo;

    @Autowired
    private ClassEnrollFormRepo classEnrollFormRepo;

    @Autowired
    private EventEnrollMainRepo eventEnrollMainRepo;

    @Autowired
    private ClassCommonFilepathRepo classCommonFilepathRepo;

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private SysCodeService sysCodeService;

    @Autowired
    private UnitDsaMemberRecordRepo unitDsaMemberRecordRepo;

    @Autowired
    private EventEnrollMainService eventEnrollMainService;

    @Autowired
    private UnitMasterInfoRepo unitMasterInfoRepo;

    @Autowired
    private CtMasterInfoRepo ctMasterInfoRepo;


    public Members002Response getClassCtMemberInfoByMemberId(String memberId, HttpSession session) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        CtMemberInfo ctMemberInfo = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId(memberId);

        Members002ProfileObject profileObject;
        List<Members002DuplicateNameObject> duplicateNameObjectList = new ArrayList<>();
        List<Members002DuplicateAddressObject> duplicateAddressObjectList = new ArrayList<>();
        List<Members002DuplicateFamilyObject> duplicateFamilyObjectList = new ArrayList<>();
        List<Members002MeditationRecordObject> meditationRecordObjectList = new ArrayList<>();
        List<Members002ActivityRecordObject> activityRecordObjectList = new ArrayList<>();
        List<Members002RegistrationRecordObject> registrationRecordObjectList = new ArrayList<>();
        List<Members002ClassCtMemberNoteObject> classCtMemberNoteObjectList = new ArrayList<>();

        String firstDt = "";

        List<ClassEnrollForm> classEnrollFormList = classEnrollFormRepo.GetClassEnrollFormListByMemberIdAndUnitId(memberId, unitId);

        if (classEnrollFormList.size() != 0) {

            ClassEnrollForm classEnrollForm = classEnrollFormList.get(0);

            firstDt = classEnrollForm.getCreateDtTm().toString().split(" ")[0] + " " +
                    classEnrollForm.getClassPeriodNum() + "期 " + classEnrollForm.getClassName();
        }

        String thisPersonStatus = null;
        if (ctMemberInfo.getThisPersonStatus() != null) {

            switch (ctMemberInfo.getThisPersonStatus()) {
                case "0":
                    thisPersonStatus = "正常";
                    break;
                case "1":
                    thisPersonStatus = "往生";
                    break;
                case "3":
                    thisPersonStatus = "出家";
                    break;
                case "99":
                    thisPersonStatus = "已刪除";
                    break;
            }
        } else {

            thisPersonStatus = "";
        }

        String thisSchoolDegree = null;
        if (ctMemberInfo.getSchoolDegree() != null) {
            switch (ctMemberInfo.getSchoolDegree()) {
                case "1":
                    thisSchoolDegree = "博士";
                    break;
                case "2":
                    thisSchoolDegree = "碩士";
                    break;
                case "3":
                    thisSchoolDegree = "大學(專)";
                    break;
                case "4":
                    thisSchoolDegree = "高中(職)";
                    break;
                case "5":
                    thisSchoolDegree = "國中";
                    break;
                case "6":
                    thisSchoolDegree = "國小";
                    break;
                default:
                    thisSchoolDegree = "";
                    break;
            }
        } else {
            thisSchoolDegree = "";
        }

        profileObject = new Members002ProfileObject(
                Null2Empty(ctMemberInfo.getFullName()),
                Null2Empty(ctMemberInfo.getAliasName()),
                Null2Empty(ctMemberInfo.getCtDharmaName()),
                Null2Empty(ctMemberInfo.getMemberId()),
                Null2Empty(ctMemberInfo.getGender()),
                Null2Empty(ctMemberInfo.getTwIdNum()),
                Null2Empty(ctMemberInfo.getEmail1()),
                ctMemberInfo.getAge(),
                //Null2Empty(classCtMemberInfo.getSchoolDegree()),
                thisSchoolDegree,
                Null2Empty(ctMemberInfo.getSchoolName()),
                Null2Empty(ctMemberInfo.getSchoolMajor()),
                ctMemberInfo.getOkSendMessage() == null ? false : ctMemberInfo.getOkSendMessage(),
                ctMemberInfo.getOkSendMagazine() == null ? false : ctMemberInfo.getOkSendMagazine(),
                ctMemberInfo.getPhotoCompleted() == null ? false : ctMemberInfo.getPhotoCompleted(),
                Null2Empty(ctMemberInfo.getMailingCountry()),
                Null2Empty(ctMemberInfo.getMailingState()),
                Null2Empty(ctMemberInfo.getMailingCity()),
                Null2Empty(ctMemberInfo.getMailingStreet()),
                Null2Empty(ctMemberInfo.getCompanyName()),
                Null2Empty(ctMemberInfo.getCompanyJobTitle()),
                Null2Empty(ctMemberInfo.getOfficePhoneNum1()),
                Null2Empty(ctMemberInfo.getFamilyLeaderName()),
                Null2Empty(ctMemberInfo.getFamilyLeaderRelationship()),
                Null2Empty(ctMemberInfo.getUrgentContactPersonName1()),
                Null2Empty(ctMemberInfo.getUrgentContactPersonPhoneNum1()),
                Null2Empty(ctMemberInfo.getUrgentContactPersonRelationship1()),
                Null2Empty(ctMemberInfo.getIntroducerName()),
                Null2Empty(ctMemberInfo.getIntroducerPhoneNum()),
                Null2Empty(ctMemberInfo.getIntroducerRelationship()),
                Null2Empty(ctMemberInfo.getComingReason()),
                ctMemberInfo.getIsTakeRefuge() == null ? false : ctMemberInfo.getIsTakeRefuge(),
                Null2Empty(ctMemberInfo.getRefugeDate()),
                "",
                ctMemberInfo.getIsTakePrecept5() == null ? false : ctMemberInfo.getIsTakePrecept5(),
                Null2Empty(ctMemberInfo.getPrecept5Date()),
                "",
                ctMemberInfo.getIsTakeBodhiPrecept() == null ? false : ctMemberInfo.getIsTakeBodhiPrecept(),
                Null2Empty(ctMemberInfo.getBodhiPreceptDate()),
                "",
                Null2Empty(ctMemberInfo.getDataUnitMemberId()),
                DateTime2DateString(ctMemberInfo.getDataSentToCTDate()),
                Null2Empty(ctMemberInfo.getCreatorName()),
                DateTime2DateTimeString(ctMemberInfo.getCreateDtTm()),
                firstDt,
                Null2Empty(ctMemberInfo.getHealthList()),
                Null2Empty(ctMemberInfo.getSkillList()),
                thisPersonStatus,
                Null2Empty(ctMemberInfo.getDsaJobNameList()),
                DateTime2DateString(ctMemberInfo.getBirthDate()),
                Null2Empty(ctMemberInfo.getBirthCountry()),
                Null2Empty(ctMemberInfo.getBirthCity()),
                Null2Empty(ctMemberInfo.getHomePhoneNum1()),
                Null2Empty(ctMemberInfo.getMobileNum1())
        );

        // 同姓名
        List<CtMemberInfo> duplicateNameList = ctMemberInfoRepo.GetClassCtMemberInfoDuplicateName(
                ctMemberInfo.getAliasName(),
                ctMemberInfo.getFullName(),
                unitId,
                false
        );

        if (duplicateNameList.size() != 0) {

            for (CtMemberInfo duplicateName : duplicateNameList) {
                duplicateNameObjectList.add(new Members002DuplicateNameObject(
                        Null2Empty(duplicateName.getMemberId()),
                        Null2Empty(duplicateName.getTwIdNum()),
                        Null2Empty(duplicateName.getFullName()),
                        Null2Empty(duplicateName.getAliasName()),
                        Null2Empty(duplicateName.getCtDharmaName()),
                        Null2Empty(duplicateName.getGender()),
                        DateTime2DateString(duplicateName.getBirthDate()),
                        Null2Empty(duplicateName.getMailingCountry() +
                                duplicateName.getMailingState() +
                                duplicateName.getMailingCity() +
                                duplicateName.getMailingStreet())
                ));
            }
        }

        // 同地址
        List<CtMemberInfo> duplicateAddressList = new ArrayList<>();

        if (ctMemberInfo.getMailingCity() != null && !ctMemberInfo.getMailingCity().equals("")) {

            if (!ctMemberInfo.getMailingStreet().equals("")) {

                duplicateAddressList = ctMemberInfoRepo.GetClassCtMemberInfoDuplicateAddress1(
                        ctMemberInfo.getMailingCity(),
                        ctMemberInfo.getMailingStreet(),
                        unitId,
                        false
                );
            } else {

                duplicateAddressList = ctMemberInfoRepo.GetClassCtMemberInfoDuplicateAddress2(
                        ctMemberInfo.getMailingCity(),
                        unitId,
                        false
                );
            }
        }

        if (duplicateAddressList.size() != 0) {

            for (CtMemberInfo duplicateAddress : duplicateAddressList) {

                duplicateAddressObjectList.add(new Members002DuplicateAddressObject(
                        Null2Empty(duplicateAddress.getMemberId()),
                        Null2Empty(duplicateAddress.getTwIdNum()),
                        Null2Empty(duplicateAddress.getAliasName()),
                        Null2Empty(duplicateAddress.getCtDharmaName()),
                        Null2Empty(duplicateAddress.getGender()),
                        DateTime2DateString(duplicateAddress.getBirthDate()),
                        Null2Empty(duplicateAddress.getMailingCountry() +
                                duplicateAddress.getMailingState() +
                                duplicateAddress.getMailingCity() +
                                duplicateAddress.getMailingStreet())
                ));
            }
        }

        // 同家屬碼
        List<CtMemberInfo> duplicateFamilyList = new ArrayList<>();

        if (ctMemberInfo.getFamilyLeaderMemberId() != null && !ctMemberInfo.getFamilyLeaderMemberId().equals("")) {

            duplicateFamilyList = ctMemberInfoRepo.GetClassCtMemberInfoDuplicateFamily(
                    ctMemberInfo.getFamilyLeaderMemberId(),
                    unitId,
                    false
            );
        }

        if (duplicateFamilyList.size() != 0) {

            for (CtMemberInfo duplicateFamily : duplicateFamilyList) {

                duplicateFamilyObjectList.add(new Members002DuplicateFamilyObject(
                        Null2Empty(duplicateFamily.getMemberId()),
                        Null2Empty(duplicateFamily.getAliasName()),
                        Null2Empty(duplicateFamily.getCtDharmaName()),
                        Null2Empty(duplicateFamily.getGender()),
                        DateTime2DateString(duplicateFamily.getBirthDate()),
                        Null2Empty(duplicateFamily.getFamilyLeaderName()),
                        Null2Empty(duplicateFamily.getFamilyLeaderMemberId()),
                        Null2Empty(duplicateFamily.getFamilyLeaderRelationship()),
                        Null2Empty(duplicateFamily.getHomePhoneNum1()),
                        Null2Empty(duplicateFamily.getMobileNum1())
                ));
            }
        }

        // 禪修紀錄-個人禪修紀錄 MeditationRecord
        List<Members002MeditationRecordObject> MeditationRecordList = classEnrollFormRepo.GetClassCtMemberInfoMeditationRecord1(
                ctMemberInfo.getMemberId(),
                unitId
        );

        if (MeditationRecordList.size() != 0) {

            for (Members002MeditationRecordObject MeditationRecord : MeditationRecordList) {

                boolean isInClass = false;

                if (MeditationRecord.getClassStatus().equals("開課中") && MeditationRecord.getIsDroppedClass() == false) {
                    isInClass = true;
                }

                meditationRecordObjectList.add(new Members002MeditationRecordObject(
                        Null2Empty(MeditationRecord.getMemberId()),
                        Null2Empty(MeditationRecord.getClassPeriodNum()),
                        Null2Empty(MeditationRecord.getClassName()),
                        Null2Empty(MeditationRecord.getGender()),
                        Null2Empty(MeditationRecord.getClassGroupId()),
                        Null2Empty(MeditationRecord.getAliasName()),
                        Null2Empty(MeditationRecord.getClassJobTitleList()),
                        isInClass,
                        MeditationRecord.getIsDroppedClass(),
                        MeditationRecord.getIsFullAttended(),
                        MeditationRecord.getIsGraduated(),
                        MeditationRecord.getIsNotComeBack(),
                        MeditationRecord.getIsEnrollToNewUpperClass(),
                        Null2Empty(MeditationRecord.getNextNewClassName()),
                        Null2Empty(MeditationRecord.getClassStatus()),
                        Null2Empty(MeditationRecord.getUnitName())
                ));
            }
        }

        // 禪修紀錄-個人活動紀錄 ActivityRecord
        List<EventEnrollMain> activityRecordList = eventEnrollMainRepo.GetClassEventEnrollMainActivityRecord(
                ctMemberInfo.getMemberId(),
                unitId
        );

        if (activityRecordList.size() != 0) {

            for (EventEnrollMain activityRecord : activityRecordList) {

                activityRecordObjectList.add(new Members002ActivityRecordObject(
                        Null2Empty(activityRecord.getEnrollUserId()),
                        Null2Empty(activityRecord.getEnrollUserName()),
                        Null2Empty(activityRecord.getEnrollUserCtDharmaName()),
                        activityRecord.getClassPeriodNum(),
                        Null2Empty(activityRecord.getClassName()),
                        activityRecord.getClassGroupId(),
                        Null2Empty(activityRecord.getEventName()),
                        DateTime2DateString(activityRecord.getArriveCtDate()),
                        DateTime2DateString(activityRecord.getLeaveCtDate()),
                        activityRecord.getIdType1(),
                        activityRecord.getIdType2()
                ));
            }
        }

        // 禪修紀錄-條碼報到紀錄 RegistrationRecord
        List<Members002RegistrationRecordObject> registrationRecordList = classEnrollFormRepo.GetClassEventEnrollMainRegistrationRecord(
                ctMemberInfo.getMemberId(),
                unitId,
                "開課中"
        );

        if (registrationRecordList.size() != 0) {

            int loginUnitTimeZone = Integer.parseInt(
                    session.getAttribute(UserSession.Keys.TimeZone.getValue()).toString().replace(".0", ""));

            String timeZone = String.valueOf(loginUnitTimeZone <= 0 ? loginUnitTimeZone : "+" + loginUnitTimeZone);

            for (Members002RegistrationRecordObject registrationRecord : registrationRecordList) {

                String classDate = "";
                String attendCheckinDate = "";
                String attendCheckinTime = "";

                Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT" + timeZone));
                calendar.setTime(registrationRecord.getClassDateD());

                classDate = calendar.get(Calendar.YEAR) + "-" +
                        ((calendar.get(Calendar.MONTH) + 1) < 10 ? "0" + (calendar.get(Calendar.MONTH) + 1) : (calendar.get(Calendar.MONTH) + 1)) + "-" +
                        (calendar.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendar.get(Calendar.DAY_OF_MONTH) : calendar.get(Calendar.DAY_OF_MONTH));

                if (registrationRecord.getAttendCheckinDateD() != null) {

                    calendar.setTime(registrationRecord.getAttendCheckinDateD());

                    attendCheckinDate = calendar.get(Calendar.YEAR) + "-" +
                            ((calendar.get(Calendar.MONTH) + 1) < 10 ? "0" + (calendar.get(Calendar.MONTH) + 1) : (calendar.get(Calendar.MONTH) + 1)) + "-" +
                            (calendar.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendar.get(Calendar.DAY_OF_MONTH) : calendar.get(Calendar.DAY_OF_MONTH));

                    attendCheckinTime = (calendar.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + calendar.get(Calendar.HOUR_OF_DAY) : calendar.get(Calendar.HOUR_OF_DAY)) + ":" +
                            (calendar.get(Calendar.MINUTE) < 10 ? "0" + calendar.get(Calendar.MINUTE) : calendar.get(Calendar.MINUTE));
                }

                registrationRecordObjectList.add(new Members002RegistrationRecordObject(
                        Null2Empty(registrationRecord.getMemberId()),
                        Null2Empty(registrationRecord.getAliasName()),
                        Null2Empty(registrationRecord.getCtDharmaName()),
                        Null2Empty(registrationRecord.getClassPeriodNum()),
                        Null2Empty(registrationRecord.getClassName()),
                        Null2Empty(registrationRecord.getClassGroupId()),
                        Null2Empty(registrationRecord.getMemberGroupNum()),
                        classDate,
                        attendCheckinDate,
                        registrationRecord.getClassWeeksNum(),
                        attendCheckinTime,
                        Null2Empty(registrationRecord.getAttendMark())
                ));
            }
        }

        // 個人註記
        List<CtMemberNote> classCtMemberNoteList = ctMemberNoteRepo.GetClassCtMemberInfoClassEnrollForm(
                ctMemberInfo.getFamilyLeaderMemberId(),
                unitId
        );

        if (classCtMemberNoteList.size() != 0) {

            for (CtMemberNote classCtMemberNote : classCtMemberNoteList) {

                classCtMemberNoteObjectList.add(new Members002ClassCtMemberNoteObject(
//                        Null2Empty(classCtMemberNote.getNoteDate()),
                        DateTime2DateString(classCtMemberNote.getUpdateDtTm()),
                        Null2Empty(classCtMemberNote.getNote()),
                        Null2Empty(classCtMemberNote.getNoteType())
                ));
            }
        }

        Members002ImagesObject images = getPhotosByMemberId(memberId);

        Members002Response members002Response = new Members002Response(
                200,
                "success",
                profileObject,
                duplicateNameObjectList,
                duplicateAddressObjectList,
                duplicateFamilyObjectList,
                meditationRecordObjectList,
                activityRecordObjectList,
                registrationRecordObjectList,
                classCtMemberNoteObjectList,
                images
        );

        return members002Response;
    }


    // sys - users
    public List<SearchMember> searchAliasName(String aliasName) {
        return ctMemberInfoRepo.SearchByAliasName(aliasName);
    }

    // sys - import data -
    public CtMemberInfo getMemberIdByNameTwId(String name, String twId, String unitId) {
        List<CtMemberInfo> members = ctMemberInfoRepo.GetMemberIdByNameTwId(name, twId, unitId);
        if (members.size() > 0) {
            return members.get(0);
        } else {
            return null;
        }
    }

    // sys - import data -
    public CtMemberInfo getMemberIdByNameBirthday(String name, String birthday, String unitId) {
        List<CtMemberInfo> members = ctMemberInfoRepo.GetMemberIdByNameBirthday(name, birthday, unitId);
        if (members.size() > 0) {
            return members.get(0);
        } else {
            return null;
        }
    }


    // test only
    public void setMemberTwId() {
        List<CtMemberInfo> list = ctMemberInfoRepo.GetMemberNoTwId(PageRequest.of(0, 100));

        for (CtMemberInfo item : list) {
            item.setTwIdNum(CommonUtils.TwIdMaker());
        }

        ctMemberInfoRepo.saveAll(list);
    }

//    public Members002ImagesObject getPhotosByMemberId(String memberId) {
//
//        List<Members002Object> members002Objects = classCommonFilepathRepo.GetFilePathByMemberId(memberId);
//
//        String photo = "", id1 = "", id2 = "", data = "", agree = "";
//
//        List<Members002Object> photoList = members002Objects.stream()
//                .filter(s -> s.getFilePathKind().equals("PHOTO")).collect(Collectors.toList());
//
//        if (photoList.size() > 0) {
//            photo = photoList.get(0).getFilePathRoot() + photoList.get(0).getFilePath() + "/" + photoList.get(0).getFileName();
//        }
//
//        List<Members002Object> twIdList = members002Objects.stream()
//                .filter(s -> s.getFilePathKind().equals("TWID")).collect(Collectors.toList());
//
//        if (twIdList.size() != 0) {
//            List<Members002Object> twId1 = twIdList.stream()
//                    .filter(s -> s.getFileName().substring(0, 12).equals(memberId + ".D1"))
//                    .collect(Collectors.toList());
//
//            if (twId1.size() > 0) {
//                id1 = twId1.get(0).getFilePathRoot() + twId1.get(0).getFilePath() + "/" + twId1.get(0).getFileName();
//            }
//
//            List<Members002Object> twId2 = twIdList.stream()
//                    .filter(s -> s.getFileName().substring(0, 12).equals(memberId + ".D2"))
//                    .collect(Collectors.toList());
//
//            if (twId2.size() > 0) {
//                id2 = twId2.get(0).getFilePathRoot() + twId2.get(0).getFilePath() + "/" + twId2.get(0).getFileName();
//            }
//        }
//
//        List<Members002Object> pidList = members002Objects.stream()
//                .filter(s -> s.getFilePathKind().equals("PID")).collect(Collectors.toList());
//
//        if (pidList.size() > 0) {
//            data = pidList.get(0).getFilePathRoot() + pidList.get(0).getFilePath() + "/" + pidList.get(0).getFileName();
//        }
//
//        List<Members002Object> p_consentList = members002Objects.stream()
//                .filter(s -> s.getFilePathKind().equals("P_CONSENT")).collect(Collectors.toList());
//
//        if (p_consentList.size() > 0) {
//            agree = p_consentList.get(0).getFilePathRoot() + p_consentList.get(0).getFilePath() + "/" + p_consentList.get(0).getFileName();
//        }
//
//        return new Members002ImagesObject(photo, id1, id2, data, agree);
//    }

    public Members002ImagesObject getPhotosByMemberId(String memberId) {

        String photo = "", id1 = "", id2 = "", data = "", agree = "";

        String path = sysCodeService.getMemberImagePath();

        File dir = new File(path);

        try {
            if (dir.listFiles().length > 0) {

                File[] matches = dir.listFiles((sdir, name) -> name.startsWith(memberId));

                for (File mFile : matches) {
                    switch (mFile.getName().substring(10).split("\\.")[0]) {
                        case "Photo":
                            photo = "/" + mFile.getName();
                            break;
                        case "IdFront":
                            id1 = "/" + mFile.getName();
                            break;
                        case "IdBack":
                            id2 = "/" + mFile.getName();
                            break;
                        case "PersonalForm":
                            data = "/" + mFile.getName();
                            break;
                        case "ParentAgreeForm":
                            agree = "/" + mFile.getName();
                            break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Members002ImagesObject(photo, id1, id2, data, agree);
    }

//    public List<Members002ImagesObject> getPhotosByMemberIdList(List<String> memberIds) {
//        return getPhotosByMemberIdList(memberIds, false);
//    }

    public List<Members002ImagesObject> getPhotosByMemberIdList(List<String> memberIds, boolean photoOnly) {

        String path = sysCodeService.getMemberImagePath();

        File dir = new File(path);

        List<Members002ImagesObject> result = new ArrayList<>();

        try {
            if (dir.listFiles().length > 0) {

                for (String memberId : memberIds) {
                    File[] matches = dir.listFiles((sdir, name) -> name.startsWith(memberId) && (!photoOnly || name.contains("Photo")));

                    String photo = "", id1 = "", id2 = "", data = "", agree = "";

                    for (File mFile : matches) {
                        switch (mFile.getName().substring(10).split("\\.")[0]) {
                            case "Photo":
                                photo = "/" + mFile.getName();
                                break;
                            case "IdFront":
                                id1 = "/" + mFile.getName();
                                break;
                            case "IdBack":
                                id2 = "/" + mFile.getName();
                                break;
                            case "PersonalForm":
                                data = "/" + mFile.getName();
                                break;
                            case "ParentAgreeForm":
                                agree = "/" + mFile.getName();
                                break;
                        }
                    }

                    if (matches.length > 0) {
                        result.add(new Members002ImagesObject(memberId, photo, id1, id2, data, agree));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public void updateIsDataComplete(String memberId) {

        CtMemberInfo ctMemberInfo = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId(memberId);

        Members002ImagesObject images = getPhotosByMemberId(memberId);

        if (images.getPhoto().equals("") ||
                images.getId1().equals("") ||
                images.getId2().equals("") ||
                images.getAgree().equals("") ||
                images.getData().equals("")) {

            ctMemberInfo.setPhotoCompleted(false);
        } else {

            ctMemberInfo.setPhotoCompleted(true);
        }

        ctMemberInfoRepo.save(ctMemberInfo);
    }

//    public List<Members002ImagesObject> getPhotosByMemberIdList(List<String> memberIdList) {
//
//        List<Members002Object> members002ObjectList = new ArrayList<>();
//        List<String> filePathRootList = new ArrayList<>();
//        List<Members002ImagesObject> members002ImagesObjectList = new ArrayList<>();

//        for (String mMemberId : memberIdList) {
//
//            String filePathRoot = mMemberId.substring(0,3) + "/" + mMemberId + "/PHOTO";
//
//            filePathRootList.add(filePathRoot);
//        }

//        members002ObjectList = classCommonFilepathRepo.GetFilePathByFilePathRootList(filePathRootList);

//        for (String mMemberId : memberIdList) {

//            List<Members002Object> photoList = members002ObjectList.stream()
//                    .filter(s -> s.getFileName().substring(0,9).equals(mMemberId))
//                    .collect(Collectors.toList());
//
//            String photo = "";
//
//            if (photoList.size() > 0) {
//                photo = photoList.get(0).getFilePathRoot() + photoList.get(0).getFilePath() + "/" + photoList.get(0).getFileName();
//            }

//            Members002ImagesObject images = getPhotosByMemberId(mMemberId);
//
//            members002ImagesObjectList.add(new Members002ImagesObject(
//                    mMemberId,
//                    images.getPhoto()
//            ));
//        }
//
//        return members002ImagesObjectList;
//    }

    // 取得學員資料列表(className & classPeriodNum帶上本期的資料)
    public MemberInfos001Response getAllMemberInfoListByUnitId(String unitId) {

        List<MemberInfos001_1Object> memberInfoList = ctMemberInfoRepo.GetAllMemberInfoListByUnitId(unitId);

        List<String> memberIdList = new ArrayList<>();

        for (MemberInfos001_1Object memberInfo : memberInfoList) {

            memberIdList.add(memberInfo.getMemberId());
        }

        List<MemberInfos001_1Object> memberClassList = new ArrayList<>();

        if (memberIdList.size() != 0) {

            memberClassList = classEnrollFormRepo.getMemberClassListByMemberIdList(memberIdList);
        }

        List<MemberInfos001_1Object> memberInfos001_1ObjectList = new ArrayList<>();

        for (MemberInfos001_1Object memberInfo : memberInfoList) {

            String mClassName = "";

            List<MemberInfos001_1Object> classNameList = memberClassList.stream()
                    .filter(s -> s.getMemberId().equals(memberInfo.getMemberId()))
                    .collect(Collectors.toList());

            for (MemberInfos001_1Object className : classNameList) {

                if (mClassName.length() == 0) {

                    mClassName = className.getClassName();
                } else {

                    mClassName = mClassName + "、" + className.getClassName();
                }
            }

            memberInfos001_1ObjectList.add(new MemberInfos001_1Object(
                    memberInfo.getMemberId(),
                    memberInfo.getGender(),
                    memberInfo.getAliasName(),
                    memberInfo.getCtDharmaName(),
                    mClassName,
                    memberInfo.getAge(),
                    memberInfo.getHomePhoneNum1(),
                    memberInfo.getMobileNum1(),
                    memberInfo.getDsaJobNameList()
            ));
        }

        MemberInfos001Response memberInfos001Response = new MemberInfos001Response(
                200,
                "success",
                memberInfos001_1ObjectList
        );

        return memberInfos001Response;
    }

    // 取得學員列表
    public List<Anonymous005_2Object> getMemberInfoLikeKeyword(String unitId, String keyword) {
        logger.debug(unitId + "/" + keyword);

        List<Anonymous005_1Object> anonymous0051ObjectList;

        Pattern pattern = Pattern.compile("[0-9]*");

        if (keyword.length() >= 4 && pattern.matcher(keyword.substring(0, 2)).matches() && pattern.matcher(keyword.substring(2, 4)).matches()) {

            int birthDateMonth = Integer.parseInt(keyword.substring(0, 2));
            int birthDateDay = Integer.parseInt(keyword.substring(2, 4));

            anonymous0051ObjectList = ctMemberInfoRepo.GetMemberInfoLikeKeyword(unitId, keyword, birthDateMonth, birthDateDay);
        } else {

            anonymous0051ObjectList = ctMemberInfoRepo.GetMemberInfoLikeKeyword(unitId, keyword);
        }

        List<Anonymous005_2Object> items = new ArrayList<>();

        if (anonymous0051ObjectList.size() != 0) {

            List<String> memberIdList = new ArrayList<>();

            for (Anonymous005_1Object object : anonymous0051ObjectList) {
                memberIdList.add(object.getMemberId());
            }

            List<Members002ImagesObject> photoList = getPhotosByMemberIdList(memberIdList, true);

            for (Anonymous005_1Object object : anonymous0051ObjectList) {

                String unEmptyKey = "";
                int keyLength = 0;
                boolean isContinue = false;

                if (object.getTwIdNum().isEmpty()) {

                    isContinue = true;
                } else {

                    keyLength = object.getTwIdNum().length();

                    if (object.getTwIdNum().substring((keyLength - 4), keyLength).equals(keyword)) {

                        isContinue = true;
                    } else {

                        isContinue = false;
                        unEmptyKey = "twIdNum";
                    }
                }

                if (isContinue) {

                    if (object.getBirthDate().isEmpty()) {

                        isContinue = true;
                    } else {

                        keyLength = object.getBirthDate().length();

                        if (object.getBirthDate().substring((keyLength - 4), keyLength).equals(keyword)) {

                            isContinue = true;
                        } else {

                            isContinue = false;
                            unEmptyKey = "birthDate";
                        }
                    }
                }

                if (isContinue) {

                    if (object.getMobileNum1().isEmpty()) {

                        isContinue = true;
                    } else {

                        keyLength = object.getMobileNum1().length();

                        if (object.getMobileNum1().substring((keyLength - 4), keyLength).equals(keyword)) {

                            isContinue = true;
                        } else {

                            isContinue = false;
                            unEmptyKey = "mobileNum1";
                        }
                    }
                }

                if (isContinue) {

                    if (object.getMobileNum2().isEmpty()) {

                        isContinue = true;
                    } else {

                        keyLength = object.getMobileNum2().length();

                        if (object.getMobileNum2().substring((keyLength - 4), keyLength).equals(keyword)) {

                            isContinue = true;
                        } else {

                            isContinue = false;
                            unEmptyKey = "mobileNum2";
                        }
                    }
                }

                if (isContinue) {

                    if (object.getHomePhoneNum1().isEmpty()) {

                        isContinue = true;
                    } else {

                        keyLength = object.getHomePhoneNum1().length();

                        if (object.getHomePhoneNum1().substring((keyLength - 4), keyLength).equals(keyword)) {

                            isContinue = true;
                        } else {

                            isContinue = false;
                            unEmptyKey = "homePhoneNum1";
                        }
                    }
                }

                if (isContinue) {

                    if (object.getHomePhoneNum2().isEmpty()) {

                        isContinue = true;
                    } else {

                        keyLength = object.getHomePhoneNum2().length();

                        if (object.getHomePhoneNum2().substring((keyLength - 4), keyLength).equals(keyword)) {

                            isContinue = true;
                        } else {

                            isContinue = false;
                            unEmptyKey = "homePhoneNum2";
                        }
                    }
                }

                Optional<Members002ImagesObject> photo = photoList.stream().filter(s -> s.getMemberId().equals(object.getMemberId())).findAny();

                String photoStr = null;

                if (photo.isPresent()) {
                    photoStr = photo.get().getPhoto();
                }

                items.add(new Anonymous005_2Object(
                        object.getMemberId(),
                        photoStr,
                        object.getAliasName(),
                        object.getCtDharmaName(),
                        unEmptyKey
                ));
            }
        }

        return items;
    }

    // 二重驗證學員資料
    public BaseResponse getMemberCtInfoDoubleCheck(Anonymous003Request anonymous003Request) {

        String memberId = anonymous003Request.getMemberId();
        String verifyKey = anonymous003Request.getVerifyKey();
        String verifyText = anonymous003Request.getVerifyText();
        boolean isCorrect = false;

        switch (verifyKey) {
            case "twIdNum":
                isCorrect = ctMemberInfoRepo.GetClassCtMemberInfoByTwIdNum(memberId, verifyText);
                break;
            case "birthDate":
                int month = Integer.parseInt(verifyText.substring(0, 2));
                int day = Integer.parseInt(verifyText.substring(2, 4));
                isCorrect = ctMemberInfoRepo.GetClassCtMemberInfoByBirthDate(memberId, month, day);
                break;
            case "mobileNum1":
                isCorrect = ctMemberInfoRepo.GetClassCtMemberInfoByMobileNum1(memberId, verifyText);
                break;
            case "mobileNum2":
                isCorrect = ctMemberInfoRepo.GetClassCtMemberInfoByMobileNum2(memberId, verifyText);
                break;
//            case "mobileNum3":
//                isCorrect = classCtMemberInfoRepo.GetClassCtMemberInfoByMobileNum3(memberId, verifyText);
//                break;
            case "homePhoneNum1":
                isCorrect = ctMemberInfoRepo.GetClassCtMemberInfoByHomePhoneNum1(memberId, verifyText);
                break;
            case "homePhoneNum2":
                isCorrect = ctMemberInfoRepo.GetClassCtMemberInfoByHomePhoneNum2(memberId, verifyText);
                break;
        }

        int errCode;
        String errMsg;

        if (isCorrect == true) {
            errCode = 200;
            errMsg = "success";
        } else {
            errCode = 400;
            errMsg = "fail";
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 依卡號取得學員資料
    public Anonymous004Response getMemberCtInfoByMemberId(String unitId, String memberId) {

        Members002ImagesObject photo = getPhotosByMemberId(memberId);

        String photoStr = null;

        if (photo != null) {
            photoStr = photo.getPhoto();
        }

        List<Anonymous005_2Object> anonymous0051ObjectList = ctMemberInfoRepo.GetCtMemberInfoListByUnitIdAndMemberId(unitId, memberId);

        if (anonymous0051ObjectList.size() == 0) {
            return new Anonymous004Response(400, "無相關資料", "", "", "");
        } else {
            return new Anonymous004Response(200, "success", photoStr, anonymous0051ObjectList.get(0).getAliasName(), anonymous0051ObjectList.get(0).getCtDharmaName());
        }
    }

    public void updateMemberAge() {
        Calendar today = Calendar.getInstance();
        int todayYear = today.get(Calendar.YEAR);
        int todayMonth = today.get(Calendar.MONTH) + 1;
        int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);

        updateAgeByDay(todayYear, todayMonth, todayDayOfMonth);

        // update age for birthday of 2/29 on 2/28 at non-leap year
        if (todayMonth == 2 && todayDayOfMonth == 28 && !Year.isLeap(todayYear)) {
            updateAgeByDay(todayYear, todayMonth, todayDayOfMonth + 1);
        }
    }

    private void updateAgeByDay(int todayYear, int todayMonth, int todayDayOfMonth) {
        List<CtMemberInfo> ctMemberInfoList = ctMemberInfoRepo.GetMemberByBirthDate(todayMonth, todayDayOfMonth);

        if (ctMemberInfoList.size() > 0) {
            for (CtMemberInfo member : ctMemberInfoList) {
                member.updateAge();
            }

            ctMemberInfoRepo.saveAll(ctMemberInfoList);
        }
    }

    public List<MemberInfoItem> findSameName(String name) {

        List<MemberInfoItem> memberInfoItemList = ctMemberInfoRepo.FindSameName(name, null);

        if (memberInfoItemList.size() > 0) {

            List<String> memberIds = memberInfoItemList.stream().map(m -> m.getMemberId()).collect(Collectors.toList());

            List<Members002ImagesObject> photos = getPhotosByMemberIdList(memberIds, true);

            for (MemberInfoItem memberInfoItem : memberInfoItemList) {
                Optional<Members002ImagesObject> photo = photos.stream().filter(p -> p.getMemberId().equals(memberInfoItem.getMemberId())).findAny();

                if (photo.isPresent()) {
                    memberInfoItem.setPhoto(photo.get().getPhoto());
                }
            }
        }

        return memberInfoItemList;
    }

    public List<MemberInfoItem> searchName(String name, String unitId) {

        List<MemberInfoItem> memberInfoItemList = ctMemberInfoRepo.SearchName(name, unitId);

        if (memberInfoItemList.size() > 0) {

            List<String> memberIds = memberInfoItemList.stream().map(m -> m.getMemberId()).collect(Collectors.toList());

            List<Members002ImagesObject> photos = getPhotosByMemberIdList(memberIds, true);

            for (MemberInfoItem memberInfoItem : memberInfoItemList) {
                Optional<Members002ImagesObject> photo = photos.stream().filter(p -> p.getMemberId().equals(memberInfoItem.getMemberId())).findAny();

                if (photo.isPresent()) {
                    memberInfoItem.setPhoto(photo.get().getPhoto());
                }
            }
        }

        return memberInfoItemList;
    }


    public List<MemberInfoItem> findSameNameByUnit(String unitId, String name) {

        List<MemberInfoItem> memberInfoItemList = ctMemberInfoRepo.FindSameName(name, unitId);

        List<String> memberIds = memberInfoItemList.stream().map(m -> m.getMemberId()).collect(Collectors.toList());

        if (memberIds.size() > 0) {
            List<Members002ImagesObject> photos = getPhotosByMemberIdList(memberIds, true);

            for (MemberInfoItem member : memberInfoItemList) {
                Optional<Members002ImagesObject> photo = photos.stream().filter(p -> p.getMemberId().equals(member.getMemberId())).findAny();

                if (photo.isPresent()) {
                    member.setPhoto(photo.get().getPhoto());
                }
            }
        }

        return memberInfoItemList;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public CtMemberInfo newMember(NewMemberRequest request, HttpSession session, boolean keepUnit) {
        CtMemberInfo memberInfo = new CtMemberInfo();
        memberInfo.setFullName(request.getName());
        memberInfo.setAliasName(request.getName());
        memberInfo.setBirthDate(CommonUtils.ParseDate(request.getBirthDate()));
        memberInfo.updateAge();
        memberInfo.setGender(request.getGender().equals("女") ? "F" : "M");
        memberInfo.setCreatorAndUpdater(session);

        if (!keepUnit) {
            String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
            String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());
            memberInfo.setUnitId(unitId);
            memberInfo.setUnitName(unitName);
        } else {
            memberInfo.setUnitId(request.getUnitId());
            memberInfo.setUnitName(request.getUnitName());
        }

        memberInfo.setId(ctMemberInfoRepo.GetMaxId() + 1);
        memberInfo.setMemberId(genMemberId());

        memberInfo.setDeleteByUnit(false);

        return ctMemberInfoRepo.save(memberInfo);
    }

    private String genMemberId() {
        int cYear = Calendar.getInstance().get(Calendar.YEAR) - 1911;

        String criteria = String.valueOf(cYear);

        String currentMaxValue = ctMemberInfoRepo.GetMaxMemberId(criteria + '%');
        int currentMaxInt = 0;
        if (currentMaxValue != null) {
            currentMaxInt = Integer.parseInt(currentMaxValue.substring(3, currentMaxValue.length()));
        }

        currentMaxInt++;

        return String.format("%03d", cYear) + String.format("%06d", currentMaxInt);
    }

    public List<SearchMemberInfo> searchMember(String name, String ctDharmaName, String twIdNum, String memberId, String birthDate, String dsaJobNameList, String email, String phone, String address, String skill, String unitId) {

        logger.debug("unitId: " + unitId + "/" + name + "/" + ctDharmaName);
        List<SearchMemberInfo> memberInfos;

        Date bDay = null;

        try {
            bDay = new SimpleDateFormat("yyyyMMdd").parse(birthDate);
        } catch (ParseException e) {
//            e.printStackTrace();
        }

        if (bDay == null) {

            memberInfos = ctMemberInfoRepo.SearchMember(
                    CommonUtils.Empty2Null(name),
                    CommonUtils.Empty2Null(ctDharmaName),
                    CommonUtils.Empty2Null(twIdNum),
                    CommonUtils.Empty2Null(memberId),
                    CommonUtils.Empty2Null(dsaJobNameList),
                    CommonUtils.Empty2Null(email),
                    CommonUtils.Empty2Null(phone),
                    CommonUtils.Empty2Null(address),
                    CommonUtils.Empty2Null(skill),
                    CommonUtils.Empty2Null(unitId));
        } else {

            int year = Integer.parseInt(birthDate.substring(0, 4));
            int month = Integer.parseInt(birthDate.substring(4, 6));
            int day = Integer.parseInt(birthDate.substring(6, 8));

            memberInfos = ctMemberInfoRepo.SearchMember(
                    CommonUtils.Empty2Null(name),
                    CommonUtils.Empty2Null(ctDharmaName),
                    CommonUtils.Empty2Null(twIdNum),
                    CommonUtils.Empty2Null(memberId),
                    CommonUtils.Empty2Null(dsaJobNameList),
                    CommonUtils.Empty2Null(email),
                    CommonUtils.Empty2Null(phone),
                    CommonUtils.Empty2Null(address),
                    CommonUtils.Empty2Null(skill),
                    CommonUtils.Empty2Null(unitId),
                    year,
                    month,
                    day);
        }

        logger.debug("sizeL " + memberInfos.size());

        List<String> memberIds = memberInfos.stream().map(m -> m.getMemberId()).collect(Collectors.toList());

        List<Members002ImagesObject> memberImages = getPhotosByMemberIdList(memberIds, true);

        if (memberIds.size() > 0) {

            List<SearchMemberInfoEnrollForm> enrollForms = classEnrollFormRepo.GetCurrentEnrollFormByMemberIds(memberIds);

            for (SearchMemberInfo memberInfo : memberInfos) {

                String iMemberId = memberInfo.getMemberId();

                if (enrollForms.size() > 0) {
                    List<SearchMemberInfoEnrollForm> iEnrollForms = enrollForms.stream().filter(e -> e.getMemberId().equals(iMemberId)).collect(Collectors.toList());
                    memberInfo.setCurrentEnrollForms(iEnrollForms);

                    long dropCount = iEnrollForms.stream().filter(e -> e.getIsDroppedClass() == true).count();
                    if (dropCount > 0) {
                        memberInfo.setDroppedClass(true);
                    } else {
                        memberInfo.setDroppedClass(false);
                    }
                }

                Optional<Members002ImagesObject> image = memberImages.stream().filter(i -> i.getMemberId().equals(memberInfo.getMemberId())).findFirst();

                if (image.isPresent()) {
                    memberInfo.setPhoto(image.get().getPhoto());
                }
            }
        }

        return memberInfos;
    }

    public CtMemberInfo getProfile(HttpSession session, String memberId) {
        boolean master = classInfoService.isMaster(session);

        CtMemberInfo memberInfo = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId(memberId);

        Members002ImagesObject images = getPhotosByMemberId(memberId);

        memberInfo.setPhoto(images.getPhoto());

        if (!master && memberInfo != null) {
            memberInfo.setDonationNameList("");
            memberInfo.setHealthList("");
            memberInfo.setNote("");
        }

        return memberInfo;
    }

    public boolean saveProfile(HttpSession session, String memberId, CtMemberInfo memberInfo) {
        boolean master = classInfoService.isMaster(session);

        CtMemberInfo iMemberInfo = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId(memberId);

        if (iMemberInfo != null) {
            iMemberInfo.copy(memberInfo, master);
            iMemberInfo.setUpdater(session);
            iMemberInfo.setRelativeMeritList(memberInfo.getRelativeMeritList());
            iMemberInfo.setVlntrGroupNameIdList(memberInfo.getVlntrGroupNameIdList());
            iMemberInfo.setParentsName(memberInfo.getParentsName());
            iMemberInfo.setParentsPhoneNum(memberInfo.getParentsPhoneNum());
            iMemberInfo.setParentsJobTitle(memberInfo.getParentsJobTitle());

            iMemberInfo.updateAge();

            ctMemberInfoRepo.save(iMemberInfo);

            return true;
        } else {
            return false;
        }
    }


    public ClassCtMemberInfoOtherResponse getOther(String unitId, String memberId) {
        ClassCtMemberInfoOtherResponse response = new ClassCtMemberInfoOtherResponse();

        CtMemberInfo memberInfo = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId(memberId);

        if (unitId != null) {
            List<DuplicateMember> duplicateNameList = ctMemberInfoRepo.GetClassCtMemberInfoDuplicateNameV2(memberInfo.getAliasName(), memberInfo.getFullName(), unitId, false);

            response.setDuplicateNameList(duplicateNameList);

            if (CommonUtils.Empty2Null(memberInfo.getMailingCity()) != null || CommonUtils.Empty2Null(memberInfo.getMailingStreet()) != null) {
                List<DuplicateMember> duplicateAddressList = ctMemberInfoRepo.GetClassCtMemberInfoDuplicateAddressV2(CommonUtils.Empty2Null(memberInfo.getMailingCity()), CommonUtils.Empty2Null(memberInfo.getMailingStreet()), unitId, false);

                response.setDuplicateAddressList(duplicateAddressList);
            }

            if (memberInfo.getFamilyLeaderMemberId() != null && !memberInfo.getFamilyLeaderMemberId().isEmpty()) {
                List<DuplicateMember> duplicateFamilyList = ctMemberInfoRepo.GetClassCtMemberInfoDuplicateFamilyV2(memberInfo.getFamilyLeaderMemberId(), unitId, false);

                response.setDuplicateFamilyList(duplicateFamilyList);
            }
        }

//        List<Members002MeditationRecordObject> enrollFormList = classEnrollFormRepo.GetClassCtMemberInfoMeditationRecord1(memberInfo.getMemberId(), unitId);
//        response.setEnrollFormList(enrollFormList);
        List<Members002MeditationRecordObject> enrollFormList = new ArrayList<>();
        List<Members002MeditationRecordObject> meditationRecordObjectList =
                classEnrollFormRepo.GetClassCtMemberInfoMeditationRecord1(memberInfo.getMemberId(), unitId);

        if (meditationRecordObjectList.size() != 0) {

            for (Members002MeditationRecordObject MeditationRecord : meditationRecordObjectList) {

                boolean isInClass = false;

                if (MeditationRecord.getClassStatus().equals("開課中") && MeditationRecord.getIsDroppedClass() == false) {
                    isInClass = true;
                }

                enrollFormList.add(new Members002MeditationRecordObject(
                        Null2Empty(MeditationRecord.getMemberId()),
                        Null2Empty(MeditationRecord.getClassPeriodNum()),
                        Null2Empty(MeditationRecord.getClassName()),
                        Null2Empty(MeditationRecord.getGender()),
                        Null2Empty(MeditationRecord.getClassGroupId()),
                        Null2Empty(MeditationRecord.getAliasName()),
                        Null2Empty(MeditationRecord.getClassJobTitleList()),
                        isInClass,
                        MeditationRecord.getIsDroppedClass(),
                        MeditationRecord.getIsFullAttended(),
                        MeditationRecord.getIsGraduated(),
                        MeditationRecord.getIsNotComeBack(),
                        MeditationRecord.getIsEnrollToNewUpperClass(),
                        Null2Empty(MeditationRecord.getNextNewClassName()),
                        Null2Empty(MeditationRecord.getClassStatus()),
                        Null2Empty(MeditationRecord.getUnitName())
                ));
            }
        }
        response.setEnrollFormList(enrollFormList);

        List<EventItem> eventList = eventEnrollMainRepo.GetClassEventEnrollMainRecord(memberInfo.getMemberId(), unitId);
        response.setEventList(eventList);

        List<Members002RegistrationRecordObject> barCodeList = classEnrollFormRepo.GetClassEventEnrollMainRegistrationRecord(memberInfo.getMemberId(), unitId, "開課中");
        response.setBarCodeList(barCodeList);

        response.setDsaMemberList(unitDsaMemberRecordRepo.GetRecordsByMemberId(memberId));

        Members002ImagesObject images = getPhotosByMemberId(memberId);
        response.setImages(images);

        response.setErrCode(200);
        response.setErrMsg("success");

        return response;
    }


    public boolean deleteMembersById(MemberIds memberIds, HttpSession session) {
        try {
            if (memberIds != null && memberIds.getMemberIds() != null && memberIds.getMemberIds().size() > 0) {

                List<CtMemberInfo> memberInfoList = ctMemberInfoRepo.GetMemberInfoByMemberIds(memberIds.getMemberIds());

                for (CtMemberInfo memberInfo : memberInfoList) {
                    memberInfo.setDeleteByUnit(true);
                    memberInfo.setUpdater(session);
                }

                ctMemberInfoRepo.saveAll(memberInfoList);
            }

            return true;
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return false;
        }
    }

    public BaseResponse imagesTransfer() {

        String pathOld = "/Users/Box-Henry/Desktop/test file old";
        String pathNew = "/Users/Box-Henry/Desktop/test file new";

        File yearFile = new File(pathOld);

        if (yearFile.listFiles().length > 0) {

            for (File year : yearFile.listFiles()) {

                logger.debug("Year : " + year.getName());

                File memberIdFile = new File(pathOld + "/" + year.getName());

                if (memberIdFile.listFiles().length > 0) {

                    for (File memberId : memberIdFile.listFiles()) {

                        logger.debug("MemberId : " + memberId.getName());

                        File folderFile = new File(pathOld + "/" + year.getName() + "/" + memberId.getName());

                        if (folderFile.listFiles().length > 0) {

                            for (File folder : folderFile.listFiles()) {

//                                logger.debug("Folder : " + folder.getName());

                                File mFile = new File(pathOld + "/" + year.getName() + "/" + memberId.getName() + "/" + folder.getName());

                                if (mFile.listFiles().length > 0) {

                                    for (File file : mFile.listFiles()) {

                                        logger.debug(folder.getName() + ":" + file.getName());

                                        String mPathOld = pathOld + "/" + year.getName() + "/" + memberId.getName() + "/" + folder.getName() + "/" + file.getName();
                                        String mPathNew = pathNew + "/" + file.getName();

                                        String key = file.getName().split("\\.")[1];

                                        switch (key) {
                                            case "M":
                                                mPathNew = pathNew + "/" +
                                                        file.getName().replace(".M", "_Photo");
                                                break;
                                            case "D1":
                                                mPathNew = pathNew + "/" +
                                                        file.getName().replace(".D1", "_IdFront");
                                                break;
                                            case "D2":
                                                mPathNew = pathNew + "/" +
                                                        file.getName().replace(".D2", "_IdBack");
                                                break;
                                            case "P1":
                                                mPathNew = pathNew + "/" +
                                                        file.getName().replace(".P1", "_PersonalForm");
                                                break;
                                            case "P2":
                                                mPathNew = pathNew + "/" +
                                                        file.getName().replace(".P2", "_ParentAgreeForm");
                                                break;
                                        }

                                        try {

                                            int cursor;

                                            FileInputStream fileInputStreamI = new FileInputStream(new File(mPathOld));

                                            FileOutputStream fileOutputStreamO =
                                                    new FileOutputStream(mPathNew, false);
                                            while ((cursor = fileInputStreamI.read()) != -1) {
                                                fileOutputStreamO.write(cursor);
                                            }
                                            fileOutputStreamO.flush();
                                            fileOutputStreamO.close();
                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return new BaseResponse(200, "success");
    }

    public BaseResponse imagesTransfer2() {

        String pathOld = "/Users/Box-Henry/Desktop/test file old";
        String pathNew = sysCodeService.getMemberImagePath();

        List<ClassCommonFilepath> classCommonFilepathList = classCommonFilepathRepo.GetAllFilePathByMemberId();

        for (ClassCommonFilepath classCommonFilepath : classCommonFilepathList) {

            String mPathOld = classCommonFilepath.getFilePathRoot() + classCommonFilepath.getFilePath() + "/" + classCommonFilepath.getFileName();
//            String mPathOld = pathOld + "/" + classCommonFilepath.getFilePath() + "/" + classCommonFilepath.getFileName();
            String mPathNew = pathNew + "/" + classCommonFilepath.getFileName();

            String key = classCommonFilepath.getFileName().split("\\.")[1];

            switch (key) {
                case "M":
                    mPathNew = pathNew + "/" +
                            classCommonFilepath.getFileName().replace(".M", "_Photo");
                    break;
                case "D1":
                    mPathNew = pathNew + "/" +
                            classCommonFilepath.getFileName().replace(".D1", "_IdFront");
                    break;
                case "D2":
                    mPathNew = pathNew + "/" +
                            classCommonFilepath.getFileName().replace(".D2", "_IdBack");
                    break;
                case "P1":
                    mPathNew = pathNew + "/" +
                            classCommonFilepath.getFileName().replace(".P1", "_PersonalForm");
                    break;
                case "P2":
                    mPathNew = pathNew + "/" +
                            classCommonFilepath.getFileName().replace(".P2", "_ParentAgreeForm");
                    break;
            }

            try {

                int cursor;

                FileInputStream fileInputStreamI = new FileInputStream(new File(mPathOld));

                FileOutputStream fileOutputStreamO =
                        new FileOutputStream(mPathNew, false);
                while ((cursor = fileInputStreamI.read()) != -1) {
                    fileOutputStreamO.write(cursor);
                }
                fileOutputStreamO.flush();
                fileOutputStreamO.close();
            } catch (FileNotFoundException e) {
//                e.printStackTrace();
                logger.error("系統找不到指定的檔案 : " + mPathOld);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return new BaseResponse(200, "success");
    }

    public List<MemberShort> getCurrentClassList(List<String> memberIds) {

        if (memberIds.size() > 0) {
            List<ClassEnrollFormShort> enrollFormList = classEnrollFormRepo.GetCurrentOpeningClassEnrolled(memberIds);
            List<MemberShort> members = ctMemberInfoRepo.GetMemberShortByMemberIds(memberIds);
            List<Members002ImagesObject> photos = getPhotosByMemberIdList(memberIds, true);

            for (MemberShort member : members) {
                List<ClassEnrollFormShort> classList = enrollFormList.stream().filter(f -> f.getMemberId().equals(member.getMemberId())).collect(Collectors.toList());
                Optional<Members002ImagesObject> photo = photos.stream().filter(p -> p.getMemberId().equals(member.getMemberId())).findAny();
                member.setClassList(classList);

                if (photo.isPresent()) {
                    member.setPhoto(photo.get().getPhoto());
                }
            }

            return members;
        } else {
            return new ArrayList<>();
        }
    }

    public void updateUrgentContact(HttpSession session, String memberId, UrgentContactRequest request) throws NotFoundException {
        CtMemberInfo member = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId(memberId);

        if (member != null) {
            member.setUrgentContactPersonName1(CommonUtils.Empty2Null(request.getUrgentContactPersonName1()));
            member.setUrgentContactPersonRelationship1(CommonUtils.Empty2Null(request.getUrgentContactPersonRelationship1()));
            member.setUrgentContactPersonPhoneNum1(CommonUtils.Empty2Null(request.getUrgentContactPersonPhoneNum1()));

            member.setUpdater(session);
            ctMemberInfoRepo.save(member);
        } else {
            throw new NotFoundException();
        }
    }

    public MemberInfoNotEnrolled getMemberInfo4EventEnroll(String memberId) {
        List<MemberInfoNotEnrolled> items = ctMemberInfoRepo.Get4EventEnrollByMemberIds(memberId);

        if (items.size() > 0) {
            Members002ImagesObject photos = getPhotosByMemberId(items.get(0).getMemberId());

            if (photos != null && photos.getPhoto() != null) {
                items.get(0).setPhoto(photos.getPhoto());
            }

            return items.get(0);
        } else {
            return null;
        }
    }

    public void updateMemberInfo4EventEnroll(HttpSession session, MemberInfoNotEnrolled member) throws NotFoundException, UnacceptableException {

        CtMemberInfo memberInfo = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId(member.getMemberId());

        if (memberInfo == null) {
            throw new NotFoundException();
        } else if (!CommonUtils.TwIdChecker(member.getTwIdNum())) {
            throw new UnacceptableException();
        } else {
            memberInfo.setAliasName(CommonUtils.Empty2Null(member.getAliasName()));
            memberInfo.setCtDharmaName(CommonUtils.Empty2Null(member.getCtDharmaName()));
            memberInfo.setGender(CommonUtils.Empty2Null(member.getGender()));
            memberInfo.setBirthDate(member.getBirthDate());
            memberInfo.setAge(member.getAge());
            memberInfo.setTwIdNum(CommonUtils.Empty2Null(member.getTwIdNum()));
            memberInfo.setClassList(CommonUtils.Empty2Null(member.getClassList()));
            memberInfo.setHealthList(CommonUtils.Empty2Null(member.getHealthList()));
            memberInfo.setCarLicenseNum1(CommonUtils.Empty2Null(member.getCarLicenseNum1()));
            memberInfo.setPassportNum(CommonUtils.Empty2Null(member.getPassportNum()));
            memberInfo.setUrgentContactPersonName1(CommonUtils.Empty2Null(member.getUrgentContactPersonName1()));
            memberInfo.setUrgentContactPersonPhoneNum1(CommonUtils.Empty2Null(member.getUrgentContactPersonPhoneNum1()));
            memberInfo.setUrgentContactPersonRelationship1(CommonUtils.Empty2Null(member.getUrgentContactPersonRelationship1()));
            memberInfo.setDonationNameList(CommonUtils.Empty2Null(member.getDonationNameList()));
            memberInfo.setRelativeMeritList(CommonUtils.Empty2Null(member.getRelativeMeritList()));
            memberInfo.setMobileNum1(CommonUtils.Empty2Null(member.getMobileNum1()));
            memberInfo.setUpdater(session);

            ctMemberInfoRepo.save(memberInfo);
        }
    }

    public ExcelCheckResponse searchMembers(String unitId, List<ExcelMember> excelMembers) {

        List<ExcelMember> unitMembers = ctMemberInfoRepo.GetAllMemberInfoByUnitId(unitId);

        List<ExcelMember> allMatched = new ArrayList<>();
        List<ExcelMember> notFound = new ArrayList<>();

        for (ExcelMember item : excelMembers) {

            int check = 0;

            if (CommonUtils.Empty2Null(item.getName()) != null) {
                check = 1;
            } else {
                item.setName("空白");
            }

            if (CommonUtils.Empty2Null(item.getDharmaName()) != null) {
                check += 2;
            } else {
                item.setDharmaName("空白");
            }

            if (CommonUtils.Empty2Null(item.getTwIdNum()) != null) {
                check += 4;
            } else {
                item.setTwIdNum("空白");
            }

            List<ExcelMember> matched = new ArrayList<>();


            switch (check) {
                case 0: // none: do nothing
                    break;
                case 1: // aliasName
                    matched = unitMembers.stream().filter(m -> m.getName() != null && m.getName().equals(item.getName())).collect(Collectors.toList());
                    break;
                case 2: // dharmaName
                    matched = unitMembers.stream().filter(m -> m.getDharmaName() != null && m.getDharmaName().equals(item.getDharmaName())).collect(Collectors.toList());
                    break;
                case 3: // aliasName & dharmaName
                    matched = unitMembers.stream().filter(m -> m.getName() != null && m.getName().equals(item.getName()) && m.getDharmaName() != null && m.getDharmaName().equals(item.getDharmaName())).collect(Collectors.toList());
                    break;
                case 4: // twIdNum
                    matched = unitMembers.stream().filter(m -> m.getTwIdNum() != null && m.getTwIdNum().equals(item.getTwIdNum())).collect(Collectors.toList());
                    break;
                case 5: // aliasName & twIdNum
                    matched = unitMembers.stream().filter(m -> m.getName() != null && m.getName().equals(item.getName()) && m.getTwIdNum() != null && m.getTwIdNum().equals(item.getTwIdNum())).collect(Collectors.toList());
                    break;
                case 6: // dharmaName & twIdNum
                    matched = unitMembers.stream().filter(m -> m.getDharmaName() != null && m.getDharmaName().equals(item.getDharmaName()) && m.getTwIdNum() != null && m.getTwIdNum().equals(item.getTwIdNum())).collect(Collectors.toList());
                    break;
                case 7: // all
                    matched = unitMembers.stream().filter(m -> m.getName() != null && m.getName().equals(item.getName()) && m.getDharmaName() != null && m.getDharmaName().equals(item.getDharmaName()) && m.getTwIdNum() != null && m.getTwIdNum().equals(item.getTwIdNum())).collect(Collectors.toList());
                    break;
            }

            if (matched.size() > 0) {
                allMatched.addAll(matched);
            } else {
                notFound.add(item);
            }
        }

        List<String> notFounds = new ArrayList<>();

        for (ExcelMember item : notFound) {

            StringBuilder notFoundSB = new StringBuilder();

            if (item.getName() != null) {
                notFoundSB.append(item.getName() + "/");
            }

            if (item.getDharmaName() != null) {
                notFoundSB.append(item.getDharmaName() + "/");
            }

            if (item.getTwIdNum() != null) {
                notFoundSB.append(item.getTwIdNum());
            }

            notFounds.add(notFoundSB.toString());
        }

        StringBuilder resultSB = new StringBuilder();
        resultSB.append("篩選學員名單有{" + excelMembers.size() + "}筆，其中{" + notFound.size() +
                "}筆找不到學員基本資料，成功找到{" + allMatched.size() + "}筆學員基本資料!");

        List<String> matchedMemberId = allMatched.stream().map(m -> m.getMemberId()).collect(Collectors.toList());

        List<MemberInfoNotEnrolled> memberInfoList = new ArrayList<>();

        if (matchedMemberId.size() > 0) {
            memberInfoList = ctMemberInfoRepo.GetMembersByMemberIds(unitId, matchedMemberId);
            eventEnrollMainService.packMemberInfo(unitId, memberInfoList);
        }

        ExcelCheckResponse response = new ExcelCheckResponse(200, resultSB.toString(), notFounds, memberInfoList);

        return response;
    }

    public ExcelCheckResponse searchMasters(String type, boolean isDeleteOriginalData, String unitId, List<ExcelMember> excelMembers, HttpSession session) {
        if (type.equals("CT")) {
            return searchMasters4Ct(excelMembers);
        } else {
            return searchMasters4Unit(isDeleteOriginalData, unitId, excelMembers, session);
        }
    }

    // SA16 ct master
    private ExcelCheckResponse searchMasters4Ct(List<ExcelMember> excelMembers) {
        List<ExcelMember> ctMasters = ctMasterInfoRepo.GetAllMasterInfo();
        List<ExcelMember> allMatched = new ArrayList<>();
        List<ExcelMember> notFound = new ArrayList<>();

        for (ExcelMember item : excelMembers) {
            int check = 0;

            // masterId
            if (CommonUtils.Empty2Null(item.getName()) != null) {
                check = 1;
            } else {
                item.setName(null);
            }

            // masterName
            if (CommonUtils.Empty2Null(item.getDharmaName()) != null) {
                check += 2;
            } else {
                item.setDharmaName(null);
            }

            List<ExcelMember> ctMatched = new ArrayList<>();

            switch (check) {
                case 0: // none: do nothing
                    break;
                case 1: // masterId
                    ctMatched = ctMasters.stream().filter(m -> m.getName() != null && m.getName().equals(item.getName())).collect(Collectors.toList());
                    break;
                case 2: // masterName
                    ctMatched = ctMasters.stream().filter(m -> m.getDharmaName() != null && m.getDharmaName().contains(item.getDharmaName())).collect(Collectors.toList());
                    break;
                case 3: // masterId & masterName
                    ctMatched = ctMasters.stream().filter(m -> m.getName() != null && m.getName().contains(item.getName()) && m.getDharmaName() != null && m.getDharmaName().equals(item.getDharmaName())).collect(Collectors.toList());
                    break;
            }

            if (ctMatched.size() > 0) {
                allMatched.addAll(ctMatched);
            } else {
                notFound.add(item);
            }
        }

        List<String> notFounds = new ArrayList<>();

        for (ExcelMember item : notFound) {

            StringBuilder notFoundSB = new StringBuilder();

            if (item.getName() != null) {
                notFoundSB.append(item.getName() + "/");
            }

            if (item.getDharmaName() != null) {
                notFoundSB.append(item.getDharmaName());
            }

            String buffer = notFoundSB.toString();

            if (buffer.endsWith("/")) {
                buffer = buffer.substring(0, buffer.length() - 1);
            }

            notFounds.add(buffer);
        }

        List<String> matchedMasterId = allMatched.stream().map(m -> m.getName()).distinct().collect(Collectors.toList());

        List<MasterInfoShort> masterInfoList = new ArrayList<>();

        if (matchedMasterId.size() > 0) {
            masterInfoList = ctMasterInfoRepo.GetAllMasterInfoByMasterIds(matchedMasterId);
        }

        StringBuilder resultSB = new StringBuilder();
        resultSB.append("篩選法師名單有{" + excelMembers.size() + "}筆，其中{" + notFound.size() +
                "}筆找不到法師資料，成功找到{" + masterInfoList.size() + "}筆法師資料!");

        ExcelCheckResponse response = new ExcelCheckResponse<>(200, resultSB.toString(), notFounds, masterInfoList);

        return response;
    }

    // SA13 unit master
    private ExcelCheckResponse searchMasters4Unit(boolean isDeleteOriginalData, String unitId, List<ExcelMember> excelMembers, HttpSession session) {
        if (isDeleteOriginalData) {
            deleteUnitMasters(unitId);
        }

        List<ExcelMember> unitMasters = unitMasterInfoRepo.GetAllMasterInfoByUnitId(unitId);
        List<ExcelMember> ctMasters = ctMasterInfoRepo.GetAllMasterInfo();
        
        List<ExcelMember> allMatched = new ArrayList<>();
        List<ExcelMember> allCtMatched = new ArrayList<>();
        List<ExcelMember> notFound = new ArrayList<>();

        for (ExcelMember item : excelMembers) {
            int check = 0;

            // masterId
            if (CommonUtils.Empty2Null(item.getName()) != null) {
                check = 1;
            } else {
                item.setName(null);
            }

            // masterName
            if (CommonUtils.Empty2Null(item.getDharmaName()) != null) {
                check += 2;
            } else {
                item.setDharmaName(null);
            }

            List<ExcelMember> unitMatched = new ArrayList<>();

            switch (check) {
                case 0: // none: do nothing
                    break;
                case 1: // masterId
                    unitMatched = unitMasters.stream().filter(m -> m.getName() != null && m.getName().equals(item.getName())).collect(Collectors.toList());
                    break;
                case 2: // masterName
                    unitMatched = unitMasters.stream().filter(m -> m.getDharmaName() != null && m.getDharmaName().contains(item.getDharmaName())).collect(Collectors.toList());
                    break;
                case 3: // masterId & masterName
                    unitMatched = unitMasters.stream().filter(m -> m.getName() != null && m.getName().contains(item.getName()) && m.getDharmaName() != null && m.getDharmaName().equals(item.getDharmaName())).collect(Collectors.toList());
                    break;
            }

            if (unitMatched.size() > 0) {
                allMatched.addAll(unitMatched);
            } else {
                List<ExcelMember> ctMatched = new ArrayList<>();

                switch (check) {
                    case 0: // none: do nothing
                        break;
                    case 1: // masterId
                        ctMatched = ctMasters.stream().filter(m -> m.getName() != null && m.getName().equals(item.getName())).collect(Collectors.toList());
                        break;
                    case 2: // masterName
                        ctMatched = ctMasters.stream().filter(m -> m.getDharmaName() != null && m.getDharmaName().contains(item.getDharmaName())).collect(Collectors.toList());
                        break;
                    case 3: // masterId & masterName
                        ctMatched = ctMasters.stream().filter(m -> m.getName() != null && m.getName().contains(item.getName()) && m.getDharmaName() != null && m.getDharmaName().equals(item.getDharmaName())).collect(Collectors.toList());
                        break;
                }

                if (ctMatched.size() > 0) {
                    allMatched.addAll(ctMatched);
                    allCtMatched.addAll(ctMatched);
                } else {
                    notFound.add(item);
                }
            }
        }

        // copy masters found in ct to unit
        if (allCtMatched.size() > 0) {
            copyCtMater2UnitMaster(allCtMatched, session);
        }

        // build for response
        List<String> notFounds = new ArrayList<>();

        for (ExcelMember item : notFound) {

            StringBuilder notFoundSB = new StringBuilder();

            if (item.getName() != null) {
                notFoundSB.append(item.getName() + "/");
            }

            if (item.getDharmaName() != null) {
                notFoundSB.append(item.getDharmaName());
            }

            String buffer = notFoundSB.toString();

            if (buffer.endsWith("/")) {
                buffer = buffer.substring(0, buffer.length() - 1);
            }

            notFounds.add(buffer);
        }

        List<String> matchedMasterId = allMatched.stream().map(m -> m.getName()).distinct().collect(Collectors.toList());

        List<MasterInfoShort> masterInfoList = new ArrayList<>();

        if (matchedMasterId.size() > 0) {
            masterInfoList = unitMasterInfoRepo.GetAllMasterInfoByMasterIds(unitId, matchedMasterId);
        }

        StringBuilder resultSB = new StringBuilder();
        resultSB.append("篩選法師名單有{" + excelMembers.size() + "}筆，其中{" + notFound.size() +
                "}筆找不到法師資料，成功找到{" + masterInfoList.size() + "}筆法師資料!");

        ExcelCheckResponse response = new ExcelCheckResponse<>(200, resultSB.toString(), notFounds, masterInfoList);

        return response;
    }

    private void deleteUnitMasters(String unitId) {
        List<UnitMasterInfo> unitMasterInfoList = unitMasterInfoRepo.GetUnitMasterByUnitId(unitId);

        if (unitMasterInfoList.size() > 0) {
            unitMasterInfoRepo.deleteAll(unitMasterInfoList);
        }
    }

    private void copyCtMater2UnitMaster(List<ExcelMember> ctMasters, HttpSession session) {
        List<String> ctMasterIds = ctMasters.stream().map(m -> m.getName()).distinct().collect(Collectors.toList());

        List<CtMasterInfo> ctMemberInfoList = ctMasterInfoRepo.findByMemberId(ctMasterIds);

        List<UnitMasterInfo> unitMasterInfoList = new ArrayList<>();

        for (CtMasterInfo ctMasterInfo : ctMemberInfoList) {
            UnitMasterInfo unitMasterInfo = new UnitMasterInfo(ctMasterInfo);
            unitMasterInfo.setCreatorAndUpdater(session);
            unitMasterInfo.setUnitId(unitMasterInfo.getCreatorUnitId());
            unitMasterInfo.setUnitName(unitMasterInfo.getCreatorUnitName());
            unitMasterInfoList.add(unitMasterInfo);
        }

        unitMasterInfoRepo.saveAll(unitMasterInfoList);
    }

    public List<String> getMemberIds(String unitId) {
        return ctMemberInfoRepo.GetMemberIdListByUnitId(unitId);
    }
}
