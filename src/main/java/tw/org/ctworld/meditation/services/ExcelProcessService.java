package tw.org.ctworld.meditation.services;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tw.org.ctworld.meditation.beans.DataImport.ExcelMemberEnroll;
import tw.org.ctworld.meditation.beans.EventEnrollMember.ExcelMember;
import tw.org.ctworld.meditation.beans.Sys.ImportResult;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.UnitInfoRepo;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.libs.CommonUtils.Empty2Null;

@Service
public class ExcelProcessService {

    @Autowired
    UnitInfoRepo unitInfoRepo;

    @Autowired
    UnitInfoService unitInfoService;

    @Autowired
    CtMemberInfoService ctMemberInfoService;

    @Autowired
    ClassEnrollFormService classEnrollFormService;

    @Autowired
    ClassInfoService classInfoService;

    @Autowired
    RecordImportLogService recordImportLogService;


    private static final Logger logger = LoggerFactory.getLogger(ExcelProcessService.class);

    // process excel file of unit info data
    public void processUnitInfoData() {

        try {
            File inFile = new ClassPathResource("UnitInfoDataV2.xlsx").getFile();

            InputStream stream = new FileInputStream(inFile);

            XSSFWorkbook workbook = new XSSFWorkbook(stream);

            for (int sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++) {

                XSSFSheet sheet = workbook.getSheetAt(sheetIndex);

                Iterator<Row> rowIterator = sheet.iterator();

                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();

                    // Skip read heading
                    if (row.getRowNum() == 0) {
                        continue;
                    }

                    UnitInfo unitInfo = new UnitInfo();

                    for (int i = 0; i < 10; i++) {

                        if (row.getCell(i) != null) {
                            Cell cell = row.getCell(i);
                            String temp;
                            if (cell.getCellTypeEnum() == CellType.FORMULA) {
                                temp = cell.getStringCellValue();
                            } else {
                                temp = cell.toString();
                            }

                            switch (i) {
                                case 0:
                                    unitInfo.setUnitId(temp);
                                    break;
                                case 1:
                                    unitInfo.setUnitName(temp);
                                    break;
                                case 2:
                                    unitInfo.setGroupName(temp);
                                    break;
                                case 3:
                                    unitInfo.setDistrict(CommonUtils.Empty2Null(temp));
                                    break;
                                case 4:
                                    unitInfo.setIsOverseasUnit(temp.equals("") ? false : true);
                                case 5:
                                    unitInfo.setSortNum(temp);
                                    break;
                                case 6:
                                    unitInfo.setTimezone(temp);
                                    break;
                                case 7:
                                    unitInfo.setUnitStartIp(temp);
                                    break;
                                case 8:
                                    unitInfo.setUnitEndIp(temp);
                                    break;
                                case 9:
                                    unitInfo.setIsNoIpLimit(temp.equals("") ? false : true);
                                    break;
                            }
                        }
                    }

                    if (unitInfo.getUnitId() != null && !unitInfo.getUnitId().isEmpty() && !unitInfoRepo.IsUnitIdExist(unitInfo.getUnitId())) {
                        unitInfoService.createUnitInfo(null, unitInfo);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    // process class excel file imported from users
    public ImportResult processClassDataImport(HttpSession session, String classId, MultipartFile file) {

        ClassInfo classInfo = classInfoService.getClassInfoByClassId4Import(classId);

        List<ExcelMemberEnroll> result = readExcelMemberEnroll(file);

        return validateMembers(result, session, classInfo);
    }

    // read excel
    private List<ExcelMemberEnroll> readExcelMemberEnroll(MultipartFile file) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT+8"));

        List<ExcelMemberEnroll> enrolls = new ArrayList<>();

        try {
            InputStream stream = file.getInputStream();

            XSSFWorkbook workbook = new XSSFWorkbook(stream);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();

            // rows
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();

                // Skip 2 rows
                if (row.getRowNum() < 2) {
                    continue;
                }

                ExcelMemberEnroll enroll = new ExcelMemberEnroll();

                // columns
                for (int i = 0; i < 11; i++) {

                    if (row.getCell(i) != null) {
                        row.getCell(i).setCellType(CellType.STRING);
                        String temp = row.getCell(i).toString();
                        logger.debug(temp);

                        switch (i) {
                            case 0:
                                enroll.setName(temp);
                                break;
                            case 1:
                                enroll.setBirthday(temp);
                                enroll.setBirthdayParsed(dateStrProcess(temp));
                                break;
                            case 2:
                                enroll.setGender(temp);
                                enroll.setGenderParsed(genderProcess(temp));
                                break;
                            case 3:
                                enroll.setTwId(temp);
                                temp = temp.trim();
                                enroll.setTwIdParsed(twIdProcess(temp));
                                break;
                            case 4:
                                enroll.setCtName(temp);
                                break;
                            case 5:
                                enroll.setGroupNum(temp);
                                enroll.setGroupNumParsed(numberProcess(temp));
                                break;
                            case 6:
                                enroll.setMemberNum(temp);
                                enroll.setMemberNumParsed(numberProcess(temp));
                                break;
                            case 7:
                                enroll.setJobList(temp);
                                break;
                            case 8:
                                enroll.setCanceled(temp);
                                enroll.setCanceledParsed(boolProcess(temp));
                                break;
                            case 9:
                                enroll.setGraduated(temp);
                                enroll.setGraduatedParsed(boolProcess(temp));
                                break;
                            case 10:
                                enroll.setFullyAttended(temp);
                                enroll.setFullyAttendedParsed(boolProcess(temp));
                                break;
                        }
                    }
                }

                // all fields, stop reading
                if (enroll.getName() == null && enroll.getBirthday() == null
                        && enroll.getGender() == null && enroll.getTwId() == null && enroll.getCtName() == null
                        && enroll.getGroupNum() == null && enroll.getMemberNum() == null
                        && enroll.getJobList() == null && enroll.getCanceled() == null
                        && enroll.getFullyAttended() == null && enroll.getGraduated() == null) {
                    break;
                }

                enrolls.add(enroll);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return enrolls;
    }

    // parsing date string
    private String dateStrProcess(String input) {
        String result;
        try {
            String date[];

            if (input.contains("/")) {
                date = input.split("/");
            } else if (input.contains(".")) {
                date = input.split("\\.");
            } else if (input.contains("-")) {
                date = input.split("-");
            } else {
                date = new String[3];
                if (input.length() == 6) {
                    date[0] = input.substring(0, 2);
                    date[1] = input.substring(2, 4);
                    date[2] = input.substring(4, 6);
                } else if (input.length() == 7) {
                    date[0] = input.substring(0, 3);
                    date[1] = input.substring(3, 5);
                    date[2] = input.substring(5, 7);
                } else if (input.length() == 8) {
                    date[0] = input.substring(0, 4);
                    date[1] = input.substring(4, 6);
                    date[2] = input.substring(6, 8);
                }
            }

            if (date[0].length() <= 3) {
                date[0] = String.valueOf(Integer.parseInt(date[0]) + 1911);
            }

            logger.debug(date[0]);

            for (int i = 1; i < date.length; i++) {
                if (date[i].length() == 0) {
                    date[i] = "00";
                } else if (date[i].length() == 1) {
                    date[i] = "0" + date[i];
                } else if (date[i].length() > 2) {
                    date[i] = date[i].substring(0, 2);
                }
                logger.debug(date[i]);
            }

            result = date[0] + date[1] + date[2];
        } catch (Exception e) {
            result = null;
        }

        return result;
    }

    // parsing gender
    private String genderProcess(String input) {
        if (input.equals("M") || input.equals("m") || input.equals("男")) {
            return "男";
        } else if (input.equals("F") || input.equals("f") || input.equals("女")) {
            return "女";
        } else {
            return null;
        }
    }

    // parsing tw id num
    private String twIdProcess(String input) {
        String twIdRegex = "^[a-zA-Z]{1}[1-2]{1}[0-9]{8}$";
        Pattern pattern = Pattern.compile(twIdRegex);
        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            return matcher.group(0);
        } else {
            return null;
        }
    }

    private int numberProcess(String input) {
        try {
            return Integer.parseInt(input);
        } catch (Exception e) {
            return -1;
        }
    }

    private boolean boolProcess(String input) {
        if (input.equals("Y") || input.equals("y")) {
            return true;
        } else {
            return false;
        }
    }

    private ImportResult validateMembers(List<ExcelMemberEnroll> enrolls, HttpSession session, ClassInfo classInfo) {

        ImportResult result = new ImportResult();
        List<RecordImportLog> logs = new ArrayList<>();
        List<ExcelMemberEnroll> newMembers = new ArrayList<>();
        result.setEnrolls(newMembers);
        result.setLogs(logs);
        result.setClassInfo(classInfo);

        List<ClassEnrollForm> newEnrollForms = new ArrayList<>();

        for (ExcelMemberEnroll enroll : enrolls) {

            logger.debug(enroll.getName());
            logger.debug("TwIdParsed : " + enroll.getTwIdParsed());

            if (classInfo != null) {

                enroll.setClassId(classInfo.getClassId());
                enroll.setUnitId(classInfo.getUnitId());
                enroll.setUnitName(classInfo.getUnitName());
            }

            if (enroll.getName() == null || enroll.getBirthday() == null || enroll.getGender() == null || enroll.getBirthdayParsed() == null || enroll.getGenderParsed() == null) {

                //A1
                enroll.setValid(false);
                enroll.setData(null, null, "匯入失敗", null, "錯誤紀錄");
            } else {

                if (enroll.getGroupNumParsed() == -1 || enroll.getMemberNumParsed() == -1) {

                    //A2
                    enroll.setData(null, null, "匯入失敗", null, "錯誤紀錄");
                }else {

                    if (enroll.getTwIdParsed() != null) {

                        ClassEnrollForm form = classEnrollFormService.getEnrollFormByNameTwIdClassId(enroll.getClassId(), enroll.getName(), enroll.getTwIdParsed());

                        if (form != null) {
                            //A3
                            String updateResult = updateEnrollForm(form, enroll, session);

                            enroll.setData(form.getMemberId(), form.getClassEnrollFormId(), "匯入更新成功(姓名+身分證比對報名表成功)", updateResult, null);
                        } else {

                            CtMemberInfo member = ctMemberInfoService.getMemberIdByNameTwId(enroll.getName(), enroll.getTwIdParsed(), enroll.getUnitId());
                            if (member != null) {
                                //A4
                                // add new enroll form
                                newEnrollForms.add(createEnrollForm(classInfo, member, enroll, session));

                                enroll.setData(member.getMemberId(), null, "匯入新增成功(姓名+身分證比對學員資料成功)", null, null);

                            } else {
                                ClassEnrollForm formB = classEnrollFormService.getEnrollFormByNameBirthdayClassId(enroll.getClassId(), enroll.getName(), enroll.getBirthDate());

                                if (formB != null) {
                                    //A5
                                    String updateResult = updateEnrollForm(formB, enroll, session);

                                    enroll.setData(formB.getMemberId(), formB.getClassEnrollFormId(), "匯入更新成功(姓名+生日比對報名表成功)", updateResult, null);

                                } else {
                                    CtMemberInfo memberB = ctMemberInfoService.getMemberIdByNameBirthday(enroll.getName(), enroll.getBirthdayParsed(), enroll.getUnitId());
                                    if (memberB != null) {
                                        //A6
                                        //add new enroll form
                                        newEnrollForms.add(createEnrollForm(classInfo, memberB, enroll, session));

                                        enroll.setData(memberB.getMemberId(), null, "匯入新增成功(姓名+生日比對學員資料成功)", null, null);

                                    } else {
                                        //A7
                                        enroll.setData(null, null, "匯入失敗", null, "比對不到此學員，另產生新增學員EXCEL/匯入禪修紀錄EXCEL");

                                        // write to new member excel
                                        newMembers.add(enroll);
                                    }
                                }
                            }
                        }
                    } else { // no tw id or not match format
                        ClassEnrollForm formC = classEnrollFormService.getEnrollFormByNameBirthdayClassId(enroll.getClassId(), enroll.getName(), enroll.getBirthDate());

                        if (formC != null) {
                            //A8
                            String updateResult = updateEnrollForm(formC, enroll, session);

                            enroll.setData(formC.getMemberId(), formC.getClassEnrollFormId(), "匯入更新成功(姓名+生日比對報名表成功)", updateResult, null);

                        } else {
                            CtMemberInfo memberB = ctMemberInfoService.getMemberIdByNameBirthday(enroll.getName(), enroll.getBirthdayParsed(), enroll.getUnitId());
                            if (memberB != null) {
                                //A9
                                //add new enroll form
                                newEnrollForms.add(createEnrollForm(classInfo, memberB, enroll, session));

                                enroll.setData(memberB.getMemberId(), null, "匯入新增成功(姓名+生日比對學員資料成功)", null, null);

                            } else {
                                //A10
                                enroll.setData(null, null, "匯入失敗", null, "比對不到此學員，另產生新增學員EXCEL / 匯入禪修紀錄EXCEL");

                                // write to new member excel
                                newMembers.add(enroll);
                            }
                        }
                    }
                }
            }

            //write enroll to log
            RecordImportLog log = recordImportLogService.add(session, enroll);
            logs.add(log);
        }

        // save all enroll forms at once
        String newEnrollFormId = classEnrollFormService.classEnrollFormIdGenerator();
        String idPrefix = newEnrollFormId.substring(0, 6);
        int intId = Integer.parseInt(newEnrollFormId.substring(6, 11));

//        logger.debug("new id " + newEnrollFormId + "/" + idPrefix + "/" + intId);

        for (ClassEnrollForm form: newEnrollForms) {
            form.setClassEnrollFormId(idPrefix + String.format("%05d", intId));
//            logger.debug(form.getClassEnrollFormId());
            intId++;
        }

        if (newEnrollForms.size() > 0) {
            classEnrollFormService.save(newEnrollForms);
        }

        for (ClassEnrollForm form: newEnrollForms) {
            List<ExcelMemberEnroll> matchedEnrolls = result.getEnrolls().stream().filter( e -> e.getMemberId() != null && e.getMemberId().equals(form.getMemberId())).collect(Collectors.toList());

            for (ExcelMemberEnroll excelMemberEnroll: matchedEnrolls) {
                excelMemberEnroll.setFormId(form.getClassEnrollFormId());
            }
        }

        return result;
    }

    private String updateEnrollForm(ClassEnrollForm form, ExcelMemberEnroll enroll, HttpSession session) {

        StringBuilder sb = new StringBuilder();

        if (enroll.getBirthDate().compareTo(form.getBirthDate()) != 0) {
            sb.append("生日:" + form.getBirthDate() + " => " + enroll.getBirthDate());
        }

        if (!CommonUtils.compare(enroll.getGenderParsed(), form.getGender())) {
            if (sb.length() > 0) {
                sb.append("，");
            }
            sb.append("性別:" + form.getGender() + " => " + enroll.getGenderParsed());
        }

        logger.debug("1>" + enroll.getTwIdParsed());
        logger.debug("2>" + form.getTwIdNum());
        if (!CommonUtils.compare(enroll.getTwIdParsed(), form.getTwIdNum())) {
            if (sb.length() > 0) {
                sb.append("，");
            }
            sb.append("身份證字號:" + form.getTwIdNum() + " => " + enroll.getTwIdParsed());
        }

        if (!CommonUtils.compare(enroll.getCtName(), form.getCtDharmaName())) {
            if (sb.length() > 0) {
                sb.append("，");
            }
            sb.append("法名:" + form.getCtDharmaName() + " => " + enroll.getCtName());
        }

        if (!CommonUtils.compare(enroll.getGroupNum(), form.getClassGroupId())) {
            if (sb.length() > 0) {
                sb.append("，");
            }
            sb.append("組別:" + form.getClassGroupId() + " => " + enroll.getGroupNum());
        }

        if (!CommonUtils.compare(enroll.getMemberNum(), form.getMemberGroupNum())) {
            if (sb.length() > 0) {
                sb.append("，");
            }
            sb.append("組號:" + form.getMemberGroupNum() + " => " + enroll.getMemberNum());
        }

        if (!CommonUtils.compare(enroll.getJobList(), form.getClassJobTitleList())) {
            if (sb.length() > 0) {
                sb.append("，");
            }
            sb.append("禪修班職稱:" + form.getClassJobTitleList() + " => " + enroll.getJobList());
        }

        if (enroll.isCanceledParsed() != form.getIsDroppedClass()) {
            if (sb.length() > 0) {
                sb.append("，");
            }
            sb.append("取消:" + form.getIsDroppedClass() + " => " + enroll.isCanceledParsed());
        }

        if (enroll.isGraduatedParsed() != form.getIsGraduated()) {
            if (sb.length() > 0) {
                sb.append("，");
            }
            sb.append("結業:" + form.getIsGraduated() + " => " + enroll.isGraduatedParsed());
        }

        if (enroll.isFullyAttendedParsed() != form.getIsFullAttended()) {
            if (sb.length() > 0) {
                sb.append("，");
            }
            sb.append("全勤:" + form.getIsFullAttended() + " => " + enroll.isFullyAttendedParsed());
        }

        if (sb.length() > 0) {
            sb.insert(0, "更新");
        }

        setClassEnrollFormFields(form, enroll, false);

        form.setUpdater(session);

        classEnrollFormService.save(form);

        if (sb.length() > 0) {
            return sb.toString();
        } else {
            return null;
        }
    }

    private ClassEnrollForm createEnrollForm(ClassInfo info, CtMemberInfo member, ExcelMemberEnroll enroll, HttpSession session) {

        ClassEnrollForm classEnrollForm = new ClassEnrollForm();

//        classEnrollForm.setClassEnrollFormId(classEnrollFormService.classEnrollFormIdGenerator());

        //from class info
        classEnrollForm.setUnitId(info.getUnitId());
        classEnrollForm.setUnitName(info.getUnitName());
        classEnrollForm.setClassId(info.getClassId());
        classEnrollForm.setClassTypeNum(info.getClassTypeNum());
        classEnrollForm.setClassTypeName(info.getClassTypeName());
        classEnrollForm.setYear(info.getYear());
        classEnrollForm.setClassStartDate(info.getClassStartDate());
        classEnrollForm.setClassEndDate(info.getClassEndDate());
        classEnrollForm.setClassPeriodNum(info.getClassPeriodNum());
        classEnrollForm.setClassName(info.getClassName());
        classEnrollForm.setDayOfWeek(info.getDayOfWeek());
        classEnrollForm.setClassDesc(info.getClassDesc());
        classEnrollForm.setClassFullName(info.getClassFullName());
        classEnrollForm.setAsistantMasterName(info.getAssistantMasterName());

        // from excel
        setClassEnrollFormFields(classEnrollForm, enroll, true);

        // from member info

        classEnrollForm.setMemberId(member.getMemberId());
        classEnrollForm.setAliasName(member.getAliasName());
        classEnrollForm.setAge(member.getAge());
        classEnrollForm.setHomePhoneNum1(Empty2Null(member.getHomePhoneNum1()));
        classEnrollForm.setMobileNum1(Empty2Null(member.getMobileNum1()));
        classEnrollForm.setAddress(Empty2Null(
                member.getMailingCountry() + member.getMailingState() +
                        member.getMailingCity() + member.getMailingStreet()));
        classEnrollForm.setCompanyNameAndJobTitle(Empty2Null(member.getCompanyJobTitle()));
        classEnrollForm.setSchoolNameAndMajor(Empty2Null(
                member.getSchoolName() + member.getSchoolMajor()));
//        classEnrollForm.setBirthDate(Empty2Null(member.getBirthDate()).equals("") ? null : java.sql.Date.valueOf(member.getBirthDate().substring(0, 4) + "-" + member.getBirthDate().substring(4, 6) + "-" + member.getBirthDate().substring(6, 8)));

        classEnrollForm.setCreatorAndUpdater(session);

//        classEnrollFormService.save(classEnrollForm);

        return classEnrollForm;
    }

    private void setClassEnrollFormFields(ClassEnrollForm form, ExcelMemberEnroll enroll, boolean updateDefault) {

        if (updateDefault) {
            form.setAliasName(enroll.getName());

            if (enroll.getTwIdParsed() != null) {
                form.setTwIdNum(enroll.getTwIdParsed());
            }

            if (enroll.getGenderParsed() != null) {
                form.setGender(enroll.getGenderParsed());
            }
        }

        if (enroll.getCtName() != null && !enroll.getCtName().isEmpty()) {
            form.setCtDharmaName(enroll.getCtName());
        }

        if (enroll.getGroupNumParsed() >= 0) {
            form.setClassGroupId(enroll.getGroupNum());
        }

        if (enroll.getMemberNumParsed() >= 0) {
            form.setMemberGroupNum(enroll.getMemberNum());
        }

        if (enroll.getJobList() != null && !enroll.getJobList().isEmpty()) {
            form.setClassJobTitleList(enroll.getJobList());
        }

        if (enroll.getBirthDate() != null) {
            form.setBirthDate(enroll.getBirthDate());
        }

        form.setIsDroppedClass(enroll.isCanceledParsed());


        form.setIsGraduated(enroll.isGraduatedParsed());


        form.setIsFullAttended(enroll.isFullyAttendedParsed());
    }


    // process class excel file imported from users
    public List<ExcelMember> processMemberFilterExcel(MultipartFile file) {

        List<ExcelMember> result = new ArrayList<>();

        try {
            InputStream stream = file.getInputStream();

            XSSFWorkbook workbook = new XSSFWorkbook(stream);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();

            // rows
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();

                // Skip 1st rows
                if (row.getRowNum() < 1) {
                    continue;
                }

                ExcelMember member = new ExcelMember();

                // columns
                for (int i = 0; i < 3; i++) {
                    if (row.getCell(i) != null) {

                        try {
                            String temp = row.getCell(i).toString();

                            switch (i) {
                                case 0:
                                    member.setName(temp);
                                    break;
                                case 1:
                                    member.setDharmaName(temp);
                                    break;
                                case 2:
                                    member.setTwIdNum(temp);
                            }
                        } catch (Exception e) {
                            logger.debug(e.getMessage());
                        }
                    }
                }

                result.add(member);
            }
        } catch (IOException i) {
            logger.error("excel process error!");
        }

        return result;

    }

    // process class excel file imported from users
    public List<ExcelMember> processMasterFilterExcel(MultipartFile file) {

        List<ExcelMember> result = new ArrayList<>();

        try {
            InputStream stream = file.getInputStream();

            XSSFWorkbook workbook = new XSSFWorkbook(stream);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();

            // rows
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();

                // Skip 1st rows
                if (row.getRowNum() < 1) {
                    continue;
                }

                ExcelMember member = new ExcelMember();

                // columns
                for (int i = 0; i < 3; i++) {
                    if (row.getCell(i) != null) {

                        try {
                            String temp = row.getCell(i).toString();

                            switch (i) {
                                case 0:
                                    member.setName(temp);
                                    break;
                                case 1:
                                    member.setDharmaName(temp);
                                    break;
                            }
                        } catch (Exception e) {
                            logger.debug(e.getMessage());
                        }
                    }
                }

                result.add(member);
            }
        } catch (IOException i) {
            logger.error("excel process error!");
        }

        return result;

    }

    public List<CodeDef> processCodeDefExcel() {

        List<CodeDef> codeDefList = new ArrayList<>();

        try {
            File inFile = new ClassPathResource("CodeDef20190919.xlsx").getFile();

            InputStream stream = new FileInputStream(inFile);

            XSSFWorkbook workbook = new XSSFWorkbook(stream);

            Date now = new Date();

            for (int sheetIndex = 0; sheetIndex < 1; sheetIndex++) {

                XSSFSheet sheet = workbook.getSheetAt(sheetIndex);

                Iterator<Row> rowIterator = sheet.iterator();

                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();

                    // Skip read heading
                    if (row.getRowNum() == 0) {
                        continue;
                    }

                    CodeDef codeDef = new CodeDef();

                    for (int i = 1; i < 8; i++) {
                        Cell cell = row.getCell(i);
                        String temp = cell.toString();

                        try {
                            switch (i) {
                                case 1:
                                    codeDef.setCodeCategory(CommonUtils.Empty2Null(temp));
                                    break;
                                case 2:
                                    codeDef.setCodeContentValue(CommonUtils.Empty2Null(temp));
                                    break;
                                case 3:
                                    codeDef.setCodeGroup(CommonUtils.Empty2Null(temp));
                                    break;
                                case 4:
                                    codeDef.setCodeGroupId(CommonUtils.Empty2Null(temp));
                                    break;
                                case 5:
                                    if (temp != null && temp.contains(".")) {
                                        temp = temp.substring(0, temp.lastIndexOf("."));
                                    }
                                    codeDef.setCodeId(CommonUtils.Empty2Null(temp));
                                    break;
                                case 6:
                                    codeDef.setCodeName(CommonUtils.Empty2Null(temp));
                                    break;
                                case 7:
                                    if (temp != null && temp.contains(".")) {
                                        temp = temp.substring(0, temp.lastIndexOf("."));
                                    }
                                    codeDef.setOrderNum(Integer.valueOf(temp));
                                    break;
                            }
                        } catch (Exception e) {
                            logger.error(e.getMessage());
                        }
                    }

                    if (codeDef.getCodeCategory() != null && codeDef.getCodeName() != null) {
                        codeDef.setCreateDtTm(now);
                        codeDefList.add(codeDef);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return codeDefList;
    }
}
