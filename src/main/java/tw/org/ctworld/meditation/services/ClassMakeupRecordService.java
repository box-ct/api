package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.Anonymous.AnonymousMakeupRequest;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Service
public class ClassMakeupRecordService {

    private static final Logger logger = LoggerFactory.getLogger(ClassMakeupRecordService.class);

    @Autowired
    private ClassMakeupRecordRepo classMakeupRecordRepo;

    @Autowired
    private ClassDateInfoRepo classDateInfoRepo;

    @Autowired
    private CtMemberInfoRepo ctMemberInfoRepo;

    @Autowired
    private ClassInfoRepo classInfoRepo;

    @Autowired
    private ClassAttendRecordRepo classAttendRecordRepo;

    @Autowired
    private ClassEnrollFormRepo classEnrollFormRepo;


    public String CreateOrUpdateMakeupRecord(AnonymousMakeupRequest request, HttpSession session) {
        logger.debug(request.getClassId() + "/" + request.getMemberId() + "/" + request.getClassWeeksNum() + "/" + request.getLatestListenFlag());

        ClassAttendRecord classAttendRecord = classAttendRecordRepo.GetClassAttendRecordByMemberIdAndClassIdAndClassWeeksNum(request.getMemberId(), request.getClassId(), request.getClassWeeksNum());

        if (classAttendRecord.getAttendMark() == null || !classAttendRecord.getAttendMark().equals("M")) {

            String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());

            SimpleDateFormat dateFormatInput = new SimpleDateFormat("yyyy-MM-dd");
            dateFormatInput.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));

            ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(request.getMemberId(), request.getClassId());

            List<ClassDateInfo> classDateInfos = classDateInfoRepo.GetClassDateByClassIdWeekNum(request.getClassId(), request.getClassWeeksNum());

            if (classDateInfos.size() > 0) {
                ClassDateInfo dateInfo = classDateInfos.get(0);

                List<ClassMakeupRecord> makeupRecords = classMakeupRecordRepo.GetRecordByClassIdMemberIdWeekNum(request.getClassId(), request.getMemberId(), request.getClassWeeksNum());

                ClassMakeupRecord record;

                if (makeupRecords.size() > 0) {
                    record = makeupRecords.get(0);

                    record.setUpdater(session);
                } else {
                    record = new ClassMakeupRecord();

                    record.setUnitId(dateInfo.getUnitId());
                    record.setUnitName(dateInfo.getUnitName());
                    record.setMemberId(request.getMemberId());

                    List<String> names = ctMemberInfoRepo.GetAliasName(request.getMemberId());

                    if (names.size() > 0) {
                        record.setAliasName(names.get(0));
                    }

                    record.setClassId(dateInfo.getClassId());

                    List<String> classNames = classInfoRepo.GetClassName(dateInfo.getClassId());

                    if (classNames.size() > 0) {
                        record.setClassName(classNames.get(0));
                    }

                    record.setClassDate(dateInfo.getClassDate());
                    record.setClassWeeksNum(dateInfo.getClassWeeksNum());
                    record.setStartListenDtTm(new Date());


                    record.setCreatorAndUpdater(session);
                }

                if (record.getLatestListenFlag() < request.getLatestListenFlag()) {
                    record.setLatestListenFlag(request.getLatestListenFlag());
                    double complete = (double) request.getLatestListenFlag() / dateInfo.getFileTimeTotalLength();
                    record.setListenCompleteRatio(complete);

                    if (complete > 0.95d) {
                        record.setListenCompleteRatio(1d);
                        record.setMakeUpClassCompleted(true);
                        record.setEndListenDtTm(new Date());


                        if (classAttendRecord != null) {

                            // update class attend mark "M"
                            classAttendRecord.setAttendMark("M");

                            if (classAttendRecord.getNote() == null) {
                                classAttendRecord.setNote(dateFormatInput.format(new Date()) + ":影音補課完成");
                            }else {
                                classAttendRecord.setNote(classAttendRecord.getNote() + "/" + dateFormatInput.format(new Date()) + ":影音補課完成");
                            }
                            classAttendRecordRepo.save(classAttendRecord);
                        }

                        if (classEnrollForm.getClassEnrollFormNote() == null) {
                            classEnrollForm.setClassEnrollFormNote(dateFormatInput.format(new Date()) + ":影音補課完成");
                        }else {
                            classEnrollForm.setClassEnrollFormNote(classEnrollForm.getClassEnrollFormNote() + "/" + dateFormatInput.format(new Date()) + ":影音補課完成");
                        }
                        classEnrollFormRepo.save(classEnrollForm);
                    }
                }

                classMakeupRecordRepo.save(record);

                return null;
            } else {
                return "ClassDateInfo not found";
            }
        }else {
            return "Has made up the class";
        }
    }
}
