package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.MemberImport.*;
import tw.org.ctworld.meditation.models.CodeDef;
import tw.org.ctworld.meditation.models.CtMemberInfo;
import tw.org.ctworld.meditation.models.CtMemberImportLog;
import tw.org.ctworld.meditation.repositories.CodeDefRepo;
import tw.org.ctworld.meditation.repositories.CtMemberInfoRepo;
import tw.org.ctworld.meditation.repositories.ClassEnrollFormRepo;
import tw.org.ctworld.meditation.repositories.CtMemberImportLogRepo;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.libs.CommonUtils.TwIdChecker;

@Service
public class MemberImportService {

    @Autowired
    private CtMemberInfoRepo ctMemberInfoRepo;

    @Autowired
    private ClassEnrollFormRepo classEnrollFormRepo;

    @Autowired
    private CodeDefRepo codeDefRepo;

    @Autowired
    private CtMemberImportLogRepo ctMemberImportLogRepo;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;


    private static final Logger logger = LoggerFactory.getLogger(ExcelProcessService.class);

    public static List<String> fieldIds = Arrays.asList("memberId", "gender", "aliasName", "engLastName", "engFirstName", "ctDharmaName", "ctRefugeMaster", "birthDate", "twIdNum", "passportNum", "homePhoneNum1", "mobileNum1", "officePhoneNum1", "email", "skillList", "healthList", "mailingZip", "mailingCountry", "mailingState", "mailingCity", "mailingStreet", "birthCountry", "nationality", "urgentContactPersonName1", "urgentContactPersonRelationship1", "urgentContactPersonPhoneNum1", "introducerName", "introducerRelationship", "introducerPhoneNum", "okSendEletter", "okSendMagazine", "okSendMessage", "companyName", "companyJobTitle", "schoolDegree", "schoolName", "schoolMajor", "clubName", "clubJobTitleNow", "clubJobTitlePrev", "parentsName", "parentsPhoneNum", "parentsJobTitle", "parentsEmployer", "dataUnitMemberId", "dataSentToCTDate", "classList", "isTakeRefuge", "refugeDate", "isTakePrecept5", "precept5Date", "isTakeBodhiPrecept", "bodhiPreceptDate", "donationNameList", "dsaJobNameList");
    public static List<String> fieldNames = Arrays.asList("學員編號", "性別", "姓名", "英文姓", "英文名", "法名", "皈依師德號", "出生日期", "身分證字號", "護照及其它證件號碼", "住宅電話", "行動電話", "公司電話", "Email", "專長", "身心狀況", "郵遞區號", "通訊國家", "通訊縣市", "通訊鄉鎮市區", "通訊街道名稱", "出生地(國家)", "國籍", "緊急連絡人", "緊急連絡人關係", "緊急連絡人電話", "介紹人", "介紹人關係", "介紹人電話", "可寄通啟", "可寄月刊", "傳簡訊", "工作單位", "職稱", "學位", "學校名稱", "系所", "參加社團", "現任社團職稱", "曾任社團職稱", "家長姓名", "家長連絡電話", "家長職業", "家長工作單位", "中台信眾編號", "資料回山日", "學佛經歷", "三皈依", "三皈依日期", "五戒", "五戒日期", "菩薩戒", "菩薩戒日期", "本山發心", "護法會職稱");

    public List<MemberImportLog> GetMemberImportLog(String unitId, String startDate, String endDate, String name) {
        Date sd = GetDate(startDate);
        Date ed = AddDay(GetDate(endDate), 1);
        if (sd == null) sd = new Date(Long.MIN_VALUE);
        if (ed == null) ed = new Date(Long.MAX_VALUE);
        List<MemberImportLog> memberImportLogs = ctMemberImportLogRepo.GetMemberImportLog(unitId, sd, ed);
        if (name != null && !name.equals("")) {
            memberImportLogs = memberImportLogs.stream().filter(item -> item.getCtDharmaName().indexOf(name) > -1 || item.getAliasName().indexOf(name) > -1).collect(Collectors.toList());
        }
        return memberImportLogs;
    }

    public List<MemberListItem> GetMemberListItem(String unitId) {
        List<MemberListItem> res = ctMemberInfoRepo.GetMemberListItem(unitId);
        List<String> ids = new ArrayList<>();
        for (MemberListItem ele : res) {
            ids.add(ele.getMemberId());
        }
        if (ids.size() > 0) {
            List<MemberEnrollInfo> classinfos = classEnrollFormRepo.GetCurrentEnrollInfosByMemberIds(ids);
            if (classinfos.size() > 0) {
                for (MemberListItem ele : res) {
                    String classJobTitle = "", homeClassNote = "";
                    //篩選報名表  lambda範例: myProducts.stream().filter(prod -> prod.price>10).collect(Collectors.toList())
                    List<MemberEnrollInfo> infos = classinfos.stream().
                            filter(info -> info.getMemberId().equals(ele.getMemberId())).
                            sorted(Comparator.comparingDouble(MemberEnrollInfo::getClassTypeNum).reversed()).
                            collect(Collectors.toList());
                    if (infos.size() > 0) {
                        for (MemberEnrollInfo cl : infos) {
                            classJobTitle += cl.getClassName();
                            if (cl.getClassJobTitleList() != null && !cl.getClassJobTitleList().equals("")) {
                                classJobTitle += "(" + cl.getClassJobTitleList() + ")";
                            }
                            classJobTitle += "/";

                            if (cl.getHomeClassNote() != null && !cl.getHomeClassNote().equals("")) {
                                homeClassNote += cl.getHomeClassNote() + ";";
                            }
                        }
                    }
                    if (classJobTitle.length() > 0)
                        classJobTitle = classJobTitle.substring(0, classJobTitle.length() - 1);
                    if (homeClassNote.length() > 0)
                        homeClassNote = homeClassNote.substring(0, homeClassNote.length() - 1);
                    ele.setCurrentClassJobTitle(classJobTitle);
                    ele.setCurrentHomeClassNote(homeClassNote);
                }
            }
        }

        return res;
    }

    public List<MemberTemplateItem> GetExportDataByMemberIds(List<String> memberIds) {
        List<MemberTemplateItem> res = ctMemberInfoRepo.GetMemberDataByMemberIds(memberIds);
        for (MemberTemplateItem ele : res) {
            ele.setGender(ele.getGender().equals("F") ? "女" : ele.getGender().equals("M") ? "男" : ele.getGender());
        }
        return res;
    }

    public void MemberImport(HttpSession session, String unitId, String unitName, MemberImportRequest memberImportRequest, List<MemberTemplateItem> memberTemplateItems) {
        List<CodeDef> codeDefs = codeDefRepo.GetClassCodeDefByCodeCategory("禪修班別");
        List<CtMemberImportLog> ctMemberImportLogList = new ArrayList<>();
        List<CtMemberInfo> ctMemberInfos = new ArrayList<>();
        int genid = Integer.parseInt(genMemberId());
        int newid = ctMemberInfoRepo.GetMaxDocId();
        for (MemberImportResult ele : memberImportRequest.getItems()) {
            if (ele.getCheck() && !ele.getStatus().equals("ERROR") && !ele.getStatus().equals("CONFIRM")) {
                String status = (ele.getStatus().equals("ADD")) ? "新增" : (ele.getStatus().equals("UPDATE")) ? "更新" : (ele.getStatus().equals("CONFIRM")) ? "確認" : (ele.getStatus().equals("ERROR")) ? "錯誤" : "";
                List<MemberTemplateItem> mbrs = memberTemplateItems.stream().filter(m -> m.getIdx() == ele.getIdx()).collect(Collectors.toList());
                if (mbrs.size() > 0) {
                    boolean chkerr = false;
                    if (ele.getStatus().equals("ADD")) {
                        //檢查同單位同資料
                        String gender = (mbrs.get(0).getGender().equals("M") || mbrs.get(0).getGender().equals("男")) ? "M" : (mbrs.get(0).getGender().equals("F") || mbrs.get(0).getGender().equals("女")) ? "F" : "";
                        boolean cnt = ctMemberInfoRepo.CountMemberByInfo(unitId, gender, mbrs.get(0).getAliasName(), mbrs.get(0).getTwIdNum());
                        if (cnt) {
                            chkerr = true;
                            ctMemberImportLogList.add(AddImportLog(session, unitId, unitName, ele.getMemberId(), ele.getAliasName(), ele.getCtDharmaName(), status + "失敗", "系統" + status + " 學員資料重複"));
                        } else {
                            ++newid;
                            ctMemberInfos.add(AddMember(session, mbrs.get(0), unitId, String.valueOf(genid), newid,unitName));
                            ++genid;
                        }
                    } else {
                        if (mbrs.get(0).getMemberId().equals(""))
                            mbrs.get(0).setMemberId(memberImportRequest.getItems().get(mbrs.get(0).getIdx()).getMemberId());
                        if (mbrs.get(0).getMemberId().equals("")) {
                            String mid = getMemberId(mbrs.get(0));
                            mbrs.get(0).setMemberId(mid);
                        }
                        CtMemberInfo updmbr = UpdateMember(session, mbrs.get(0), memberImportRequest.getUpdateType().equals("OVERWRITE"), unitId,unitName);
                        if (updmbr != null) {
                            ctMemberInfos.add(updmbr);
                        } else {
                            chkerr = true;
                            ctMemberImportLogList.add(AddImportLog(session, unitId, unitName, ele.getMemberId(), ele.getAliasName(), ele.getCtDharmaName(), status + "失敗", "系統" + status + " 取得學員資料失敗"));
                        }
                    }
                    if (!chkerr)
                        ctMemberImportLogList.add(AddImportLog(session, unitId, unitName, mbrs.get(0).getMemberId(), ele.getAliasName(), ele.getCtDharmaName(), status + "成功", "系統" + status));
                } else { //寫入log
                    ctMemberImportLogList.add(AddImportLog(session, unitId, unitName, ele.getMemberId(), ele.getAliasName(), ele.getCtDharmaName(), status + "失敗", "比對不成功"));
                }
            }
        }
        ctMemberImportLogRepo.saveAll(ctMemberImportLogList);
//        ctMemberInfoRepo.saveAll(ctMemberInfos);
    }

    private String getMemberId(MemberTemplateItem inpmbr) {
        String res = "";
        //比對資料
        List<MemberCompareItem> allmbrs = ctMemberInfoRepo.GetAllMemberByNameList(Arrays.asList(inpmbr.getAliasName()));
        if (allmbrs.size() > 0) {
            for (MemberCompareItem mbr : allmbrs) {
                //比對二: 姓名+身分證字號
                if (inpmbr.getAliasName().equals(mbr.getAliasName()) && inpmbr.getTwIdNum().equals(mbr.getTwIdNum())) {
                    res = mbr.getMemberId();
                    break;
                }
                //比對三: 姓名+生日
                if (inpmbr.getAliasName().equals(mbr.getAliasName()) && CompareBirthdate(inpmbr.getBirthDate(), mbr.getBirthDate())) {
                    res = mbr.getMemberId();
                    break;
                }
                //比對同名
                if (inpmbr.getAliasName().equals(mbr.getAliasName())) {
                    res = mbr.getMemberId();
                    break;
                }
            }
        }
        return res;
    }

    private CtMemberImportLog AddImportLog(HttpSession session, String unitId, String unitName, String memberId, String aliasName, String ctDharmaName, String importResultType, String importContent) {
        CtMemberImportLog log = new CtMemberImportLog();
        log.setCreatorAndUpdater(session);
        log.setUnitId(unitId);
        log.setUnitName(unitName);
        log.setMemberId(memberId);
        log.setAliasName(aliasName);
        log.setCtDharmaName(ctDharmaName);
        log.setImportResultType(importResultType);
        log.setImportContent(importContent);
        return log;
    }

    private CtMemberInfo UpdateMember(HttpSession session, MemberTemplateItem data, boolean overwrite, String unitId,String unitName) {
        CtMemberInfo mbr = ctMemberInfoRepo.GetClassCtMemberInfoByMemberId(data.getMemberId());
        if (mbr != null) {
            mbr.setUpdater(session);
            return SetData(mbr, data, overwrite, unitId,unitName);
        } else
            return null;
    }

    private String genMemberId() {
        int cYear = Calendar.getInstance().get(Calendar.YEAR) - 1911;

        String criteria = String.valueOf(cYear);

        String currentMaxValue = ctMemberInfoRepo.GetMaxMemberId(criteria + '%');
        int currentMaxInt = 0;
        if (currentMaxValue != null) {
            currentMaxInt = Integer.parseInt(currentMaxValue.substring(3, currentMaxValue.length()));
        }

        currentMaxInt++;

        return String.format("%03d", cYear) + String.format("%06d", currentMaxInt);
    }

    private CtMemberInfo AddMember(HttpSession session, MemberTemplateItem data, String unidId, String memid, Integer newid,String unitName) {
        CtMemberInfo mbr = new CtMemberInfo();
        mbr.setCreatorAndUpdater(session);
        data.setMemberId(memid);
        mbr.setMemberId(memid);
        mbr.setId(newid);
        mbr.setDeleteByUnit(false);
        return SetData(mbr, data, true, unidId,unitName);
    }

    private CtMemberInfo SetData(CtMemberInfo mbr, MemberTemplateItem data, boolean overwrite, String unitId, String unitName) {
        mbr.setUnitId(unitId);
        mbr.setUnitName(unitName);
        Date birthdate = GetDate(data.getBirthDate());
        Date ctdate = GetDate(data.getDataSentToCTDate());
        String gender = (data.getGender().equals("M") || data.getGender().equals("男")) ? "M" : (data.getGender().equals("F") || data.getGender().equals("女")) ? "F" : "";
        mbr.setGender((overwrite) ? gender : (gender.equals("")) ? mbr.getGender() : gender);
        mbr.setAliasName((overwrite) ? data.getAliasName() : (data.getAliasName().equals("")) ? mbr.getAliasName() : data.getAliasName());
        mbr.setEngLastName((overwrite) ? data.getEngLastName() : (data.getEngLastName().equals("")) ? mbr.getEngFirstName() : data.getEngLastName());
        mbr.setEngFirstName((overwrite) ? data.getEngFirstName() : (data.getEngFirstName().equals("")) ? mbr.getEngFirstName() : data.getEngFirstName());
        mbr.setCtDharmaName((overwrite) ? data.getCtDharmaName() : (data.getCtDharmaName().equals("")) ? mbr.getCtDharmaName() : data.getCtDharmaName());
        mbr.setCtRefugeMaster((overwrite) ? data.getCtRefugeMaster() : (data.getCtRefugeMaster().equals("")) ? mbr.getCtRefugeMaster() : data.getCtRefugeMaster());
        mbr.setBirthDate((overwrite) ? birthdate : (birthdate == null) ? mbr.getBirthDate() : birthdate);
        mbr.setTwIdNum((overwrite) ? data.getTwIdNum() : (data.getTwIdNum().equals("")) ? mbr.getTwIdNum() : data.getTwIdNum());
        mbr.setPassportNum((overwrite) ? data.getPassportNum() : (data.getPassportNum().equals("")) ? mbr.getPassportNum() : data.getPassportNum());
        mbr.setHomePhoneNum1((overwrite) ? data.getHomePhoneNum1() : (data.getHomePhoneNum1().equals("")) ? mbr.getHomePhoneNum1() : data.getHomePhoneNum1());
        mbr.setMobileNum1((overwrite) ? data.getMobileNum1() : (data.getMobileNum1().equals("")) ? mbr.getMobileNum1() : data.getMobileNum1());
        mbr.setOfficePhoneNum1((overwrite) ? data.getOfficePhoneNum1() : (data.getOfficePhoneNum1().equals("")) ? mbr.getOfficePhoneNum1() : data.getOfficePhoneNum1());
        mbr.setEmail1((overwrite) ? data.getEmail() : (data.getEmail().equals("")) ? mbr.getEmail1() : data.getEmail());
        mbr.setSkillList((overwrite) ? data.getSkillList() : (data.getSkillList().equals("")) ? mbr.getSkillList() : data.getSkillList());
        mbr.setHealthList((overwrite) ? data.getHealthList() : (data.getHealthList().equals("")) ? mbr.getHealthList() : data.getHealthList());
        mbr.setMailingZip((overwrite) ? data.getMailingZip() : (data.getMailingZip().equals("")) ? mbr.getMailingZip() : data.getMailingZip());
        mbr.setMailingCountry((overwrite) ? data.getMailingCountry() : (data.getMailingCountry().equals("")) ? mbr.getMailingCountry() : data.getMailingCountry());
        mbr.setMailingCity((overwrite) ? data.getMailingCity() : (data.getMailingCity().equals("")) ? mbr.getMailingCity() : data.getMailingCity());
        mbr.setMailingStreet((overwrite) ? data.getMailingStreet() : (data.getMailingStreet().equals("")) ? mbr.getMailingStreet() : data.getMailingStreet());
        mbr.setMailingState((overwrite) ? data.getMailingState() : (data.getMailingState().equals("")) ? mbr.getMailingState() : data.getMailingState());
        mbr.setBirthCountry((overwrite) ? data.getBirthCountry() : (data.getBirthCountry().equals("")) ? mbr.getBirthCountry() : data.getBirthCountry());
        mbr.setNationality((overwrite) ? data.getNationality() : (data.getNationality().equals("")) ? mbr.getNationality() : data.getNationality());
        mbr.setUrgentContactPersonName1((overwrite) ? data.getUrgentContactPersonName1() : (data.getUrgentContactPersonName1().equals("")) ? mbr.getUrgentContactPersonName1() : data.getUrgentContactPersonName1());
        mbr.setUrgentContactPersonRelationship1((overwrite) ? data.getUrgentContactPersonRelationship1() : (data.getUrgentContactPersonRelationship1().equals("")) ? mbr.getUrgentContactPersonRelationship1() : data.getUrgentContactPersonRelationship1());
        mbr.setUrgentContactPersonPhoneNum1((overwrite) ? data.getUrgentContactPersonPhoneNum1() : (data.getUrgentContactPersonPhoneNum1().equals("")) ? mbr.getUrgentContactPersonPhoneNum1() : data.getUrgentContactPersonPhoneNum1());
        mbr.setIntroducerName((overwrite) ? data.getIntroducerName() : (data.getIntroducerName().equals("")) ? mbr.getIntroducerName() : data.getIntroducerName());
        mbr.setIntroducerRelationship((overwrite) ? data.getIntroducerRelationship() : (data.getIntroducerRelationship().equals("")) ? mbr.getIntroducerRelationship() : data.getIntroducerRelationship());
        mbr.setIntroducerPhoneNum((overwrite) ? data.getIntroducerPhoneNum() : (data.getIntroducerPhoneNum().equals("")) ? mbr.getIntroducerPhoneNum() : data.getIntroducerPhoneNum());
        mbr.setOkSendEletter((overwrite) ? data.getOkSendEletter().equals("Y") : (data.getOkSendEletter().equals("")) ? mbr.getOkSendEletter() : data.getOkSendEletter().equals("Y"));
        mbr.setOkSendMagazine((overwrite) ? data.getOkSendMagazine().equals("Y") : (data.getOkSendMagazine().equals("")) ? mbr.getOkSendMagazine() : data.getOkSendMagazine().equals("Y"));
        mbr.setOkSendMessage((overwrite) ? data.getOkSendMessage().equals("Y") : (data.getOkSendMessage().equals("")) ? mbr.getOkSendMessage() : data.getOkSendMessage().equals("Y"));
        mbr.setCompanyName((overwrite) ? data.getCompanyName() : (data.getCompanyName().equals("")) ? mbr.getCompanyName() : data.getCompanyName());
        mbr.setCompanyJobTitle((overwrite) ? data.getCompanyJobTitle() : (data.getCompanyJobTitle().equals("")) ? mbr.getCompanyJobTitle() : data.getCompanyJobTitle());
        mbr.setSchoolDegree((overwrite) ? data.getSchoolDegree() : (data.getSchoolDegree().equals("")) ? mbr.getSchoolDegree() : data.getSchoolDegree());
        mbr.setSchoolName((overwrite) ? data.getSchoolName() : (data.getSchoolName().equals("")) ? mbr.getSchoolName() : data.getSchoolName());
        mbr.setSchoolMajor((overwrite) ? data.getSchoolMajor() : (data.getSchoolMajor().equals("")) ? mbr.getSchoolMajor() : data.getSchoolMajor());
        mbr.setClubName((overwrite) ? data.getClubName() : (data.getClubName().equals("")) ? mbr.getClubName() : data.getClubName());
        mbr.setClubJobTitleNow((overwrite) ? data.getClubJobTitleNow() : (data.getClubJobTitleNow().equals("")) ? mbr.getClubJobTitleNow() : data.getClubJobTitleNow());
        mbr.setClubJobTitlePrev((overwrite) ? data.getClubJobTitlePrev() : (data.getClubJobTitlePrev().equals("")) ? mbr.getClubJobTitlePrev() : data.getClubJobTitlePrev());
        mbr.setParentsName((overwrite) ? data.getParentsName() : (data.getParentsName().equals("")) ? mbr.getParentsName() : data.getParentsName());
        mbr.setParentsPhoneNum((overwrite) ? data.getParentsPhoneNum() : (data.getParentsPhoneNum().equals("")) ? mbr.getParentsPhoneNum() : data.getParentsPhoneNum());
        mbr.setParentsJobTitle((overwrite) ? data.getParentsJobTitle() : (data.getParentsJobTitle().equals("")) ? mbr.getParentsJobTitle() : data.getParentsJobTitle());
        mbr.setParentsEmployer((overwrite) ? data.getParentsEmployer() : (data.getParentsEmployer().equals("")) ? mbr.getParentsEmployer() : data.getParentsEmployer());
        mbr.setDataUnitMemberId((overwrite) ? data.getDataUnitMemberId() : (data.getDataUnitMemberId().equals("")) ? mbr.getDataUnitMemberId() : data.getDataUnitMemberId());
        mbr.setDataSentToCTDate((overwrite) ? ctdate : (ctdate == null) ? mbr.getDataSentToCTDate() : ctdate);
        mbr.setClassList((overwrite) ? data.getClassList() : (data.getClassList().equals("")) ? mbr.getClassList() : data.getClassList());
        mbr.setIsTakeRefuge((overwrite) ? data.getIsTakeRefuge().equals("Y") : (data.getIsTakeRefuge().equals("")) ? mbr.getIsTakeRefuge() : data.getIsTakeRefuge().equals("Y"));
        mbr.setRefugeDate((overwrite) ? data.getRefugeDate() : (data.getRefugeDate().equals("")) ? mbr.getRefugeDate() : data.getRefugeDate());
        mbr.setIsTakePrecept5((overwrite) ? data.getIsTakePrecept5().equals("Y") : (data.getIsTakePrecept5().equals("")) ? mbr.getIsTakePrecept5() : data.getIsTakePrecept5().equals("Y"));
        mbr.setPrecept5Date((overwrite) ? data.getPrecept5Date() : (data.getPrecept5Date().equals("")) ? mbr.getPrecept5Date() : data.getPrecept5Date());
        mbr.setIsTakeBodhiPrecept((overwrite) ? data.getIsTakeBodhiPrecept().equals("Y") : (data.getIsTakeBodhiPrecept().equals("")) ? mbr.getIsTakeBodhiPrecept() : data.getIsTakeBodhiPrecept().equals("Y"));
        mbr.setBodhiPreceptDate((overwrite) ? data.getBodhiPreceptDate() : (data.getBodhiPreceptDate().equals("")) ? mbr.getBodhiPreceptDate() : data.getBodhiPreceptDate());
        mbr.setDonationNameList((overwrite) ? data.getDonationNameList() : (data.getDonationNameList().equals("")) ? mbr.getDonationNameList() : data.getDonationNameList());

        ctMemberInfoRepo.saveAndFlush(mbr);

//        ObjectMapper Obj = new ObjectMapper();
//        try {
//            String mbrStr = Obj.writeValueAsString(mbr);
//            String dataStr=Obj.writeValueAsString(data);
//            logger.debug(mbrStr);
//            logger.debug(dataStr);
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
        return mbr;
    }

    public List<MemberImportResult> CheckMemberImport(List<MemberTemplateItem> memberTemplateItems) {
        List<CodeDef> codeDefs = codeDefRepo.GetClassCodeDefByCodeCategory("禪修班別");
        List<MemberImportResult> results = new ArrayList<>();
        if (memberTemplateItems.size() > 0) {
            List<String> names = memberTemplateItems.stream().map(item -> item.getAliasName()).collect(Collectors.toList());
            //建立匯入結果
            int idx = 0;
            for (MemberTemplateItem ele : memberTemplateItems) {
                MemberImportResult importResult = new MemberImportResult("", "", ele.getMemberId(), ele.getGender(), "", ele.getAliasName(), ele.getEngFirstName(),
                        ele.getEngLastName(), ele.getCtDharmaName(), ele.getBirthDate(), ele.getTwIdNum(), ele.getPassportNum(), false, idx);
                results.add(importResult);
                ele.setIdx(idx);
                ++idx;
            }
            //比對資料
            List<MemberCompareItem> allmbrs = ctMemberInfoRepo.GetAllMemberByNameList(names);
            for (MemberTemplateItem ele : memberTemplateItems) {
                String status = "ADD";
                //檢查資料
                String errmsg = CheckData(ele, codeDefs);
                if (!errmsg.equals("")) {
                    //mantis id:0000673
                    errmsg = errmsg.substring(0, errmsg.length() - 1);
                    //
                    results.get(ele.getIdx()).setStatus("ERROR");
                    results.get(ele.getIdx()).setErrorResult(errmsg);
                } else {
                    if (allmbrs.size() > 0) {
                        for (MemberCompareItem mbr : allmbrs) {
                            //比對一: 學員編號+姓名 或 全名
                            if (ele.getMemberId().equals(mbr.getMemberId()) && (ele.getAliasName().equals(mbr.getAliasName()) || ele.getAliasName().equals(mbr.getFullName()))) {
                                status = "UPDATE";
                                break;
                            }
                            //比對二: 姓名+身分證字號
                            if (ele.getAliasName().equals(mbr.getAliasName()) && ele.getTwIdNum().equals(mbr.getTwIdNum())) {
                                status = "UPDATE";
                                break;
                            }
                            //比對三: 姓名+生日
                            if (ele.getAliasName().equals(mbr.getAliasName()) && CompareBirthdate(ele.getBirthDate(), mbr.getBirthDate())) {
                                status = "UPDATE";
                                break;
                            }
                            //比對同名
                            if (ele.getAliasName().equals(mbr.getAliasName())) {
                                status = "CONFIRM";
//                                break;
                            }
                        }
                    }
                    results.get(ele.getIdx()).setStatus(status);
                }

            }

//            //檢查資料
//            for(MemberTemplateItem ele:memberTemplateItems){
//                String errmsg= CheckData(ele, classCodeDefs);
//                if (errmsg!=""){
//                    results.get(ele.getIdx()).setStatus("ERROR");
//                    results.get(ele.getIdx()).setErrorResult(errmsg);
//                }
//            }
        }
        return results;
    }

    private String CheckData(MemberTemplateItem ele, List<CodeDef> codeDefs) {
        String errmsg = "";
        if (!ele.getMemberId().equals("")) {
            if (!ctMemberInfoRepo.CheckMemberId(ele.getMemberId())) {
                errmsg += "會員ID不存在，";
            }
        }
        if (ele.getGender().equals("") || ele.getGender() == null) {
            errmsg += "性別不得為空，";
        } else if (ValidGenre.indexOf(ele.getGender()) < 0) {
            errmsg += "性別格式不符，";
        }
        if (ele.getAliasName().equals("") || ele.getAliasName() == null) {
            errmsg += "姓名不得為空，";
        }
        if (ele.getBirthDate().equals("") || ele.getBirthDate() == null) {
            errmsg += "出生日期不得為空，";
        } else if (GetDate_1(ele.getBirthDate()) == null) {
            errmsg += "出生日期格式不符，";
        }
        if (!ele.getTwIdNum().equals("") && !TwIdChecker(ele.getTwIdNum())) {
            errmsg += "身分證字號格式錯誤，";
        }
        if (ValidYN.indexOf(ele.getOkSendEletter()) < 0) {
            errmsg += "可寄通啟格式不符，";
        }
        if (ValidYN.indexOf(ele.getOkSendMagazine()) < 0) {
            errmsg += "可寄月刊格式不符，";
        }
        if (ValidYN.indexOf(ele.getOkSendMessage()) < 0) {
            errmsg += "傳簡訊格式不符，";
        }
        if (!ele.getDataSentToCTDate().equals("") && GetDate_1(ele.getDataSentToCTDate()) == null) {
            errmsg += "資料回山日格式不符，";
        }
        List<CodeDef> tmp = codeDefs.stream().filter(c -> c.getCodeName().equals(ele.getClassList())).collect(Collectors.toList());
        if (!ele.getClassList().equals("") && tmp.size() < 1) {
            errmsg += "學佛經歷格式不符，";
        }
        if (ValidYN.indexOf(ele.getIsTakeRefuge()) < 0) {
            errmsg += "三皈依格式不符，";
        }
        if (!ele.getRefugeDate().equals("") && GetDate_1(ele.getRefugeDate()) == null) {
            errmsg += "三皈依日期格式不符，";
        }
        if (ValidYN.indexOf(ele.getIsTakePrecept5()) < 0) {
            errmsg += "五戒格式不符，";
        }
        if (!ele.getPrecept5Date().equals("") && GetDate_1(ele.getPrecept5Date()) == null) {
            errmsg += "五戒日期格式不符，";
        }
        if (ValidYN.indexOf(ele.getIsTakeBodhiPrecept()) < 0) {
            errmsg += "菩薩戒格式不符，";
        }
        if (!ele.getBodhiPreceptDate().equals("") && GetDate_1(ele.getBodhiPreceptDate()) == null) {
            errmsg += "菩薩戒日期格式不符，";
        }
        return errmsg;
    }

    private List<String> ValidGenre = Arrays.asList("M", "F", "男", "女");
    private List<String> ValidYN = Arrays.asList("Y", "");

    private boolean CompareBirthdate(String date1, String date2) {
        Date d1 = GetDate(date1);
        Date d2 = GetDate(date2);
        if (d1 != null && d2 != null) {
            return sdf.format(d1).equals(sdf.format(d2));
        } else
            return false;
    }

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat sd_f = new SimpleDateFormat("yyyy-MM-dd");

    public Date AddDay(Date dt, Integer days) {
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DATE, days);
            return c.getTime();
        } catch (Exception ex) {
            return dt;
        }
    }

    public Date GetDate(String datestr) {
        try {
            if (datestr.indexOf(".") > -1) {
                String[] tmp = datestr.split("\\.");
                String str = String.valueOf(Integer.parseInt(tmp[0]) + 1911) + "-" + tmp[1] + "-" + tmp[2];
                return sd_f.parse(str);
            }
            if (datestr.indexOf("-") > -1)
                return sd_f.parse(datestr);
            else
                return sdf.parse(datestr);
        } catch (Exception ex) {
            return null;
        }
    }

    public Date GetDate_1(String datestr) {
        try {
            if (datestr.indexOf(".") > -1) {
                String[] tmp = datestr.split("\\.");
                String str = String.valueOf(Integer.parseInt(tmp[0]) + 1911) + "-" + tmp[1] + "-" + tmp[2];
                if (isValidDateFormat("yyyy-MM-dd", str)) {
                    return sd_f.parse(str);
                } else
                    return null;
            }
            if (datestr.indexOf("-") > -1)
                return null;// sd_f.parse(datestr);
            else {
                if (datestr.length() != 8) {
                    return null;
                } else {
                    if (isValidDateFormat("yyyyMMdd", datestr)) {
                        return sdf.parse(datestr);
                    } else
                        return null;
                }

            }
        } catch (Exception ex) {
            logger.debug(ex.getMessage());
            return null;
        }
    }

    private boolean isValidDateFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (Exception ex) {

        }
        return date != null;
    }
}
