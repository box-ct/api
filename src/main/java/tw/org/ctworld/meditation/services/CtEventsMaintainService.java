package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.CtEventMaintain.*;
import tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef002Object;
import tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef002Response;
import tw.org.ctworld.meditation.beans.EventPgmDef.EventPgmDef001Object;
import tw.org.ctworld.meditation.beans.EventPgmDef.EventPgmDef001Response;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.libs.CommonUtils.*;

@Service
public class CtEventsMaintainService {

    private static final Logger logger = LoggerFactory.getLogger(CtEventsMaintainService.class);

    @Autowired
    private EventMainRepo eventMainRepo;

    @Autowired
    private EventCtSpecialInputRepo eventCtSpecialInputRepo;

    @Autowired
    private EventUnitSpecialInputRepo eventUnitSpecialInputRepo;

    @Autowired
    private CodeDefRepo codeDefRepo;

    @Autowired
    private EventEnrollMainRepo eventEnrollMainRepo;

    @Autowired
    private EventEnrollPgmRepo eventEnrollPgmRepo;

    @Autowired
    private EventFormForCtRepo eventFormForCtRepo;

    @Autowired
    private EventCategorySpecifiedPgmRecordRepo eventCategorySpecifiedPgmRecordRepo;

    @Autowired
    private EventPgmDefRepo eventPgmDefRepo;

    @Autowired
    private EventPgmOcaRepo eventPgmOcaRepo;

    @Autowired
    private EventPgmUnitRepo eventPgmUnitRepo;

    @Autowired
    private EventCategoryDefRepo eventCategoryDefRepo;

    @Autowired
    private EventUnitInfoRepo eventUnitInfoRepo;

    @Autowired
    private EventPgmMappingForCtRepo eventPgmMappingForCtRepo;

    @Autowired
    private EventUnitTemplateRepo eventUnitTemplateRepo;

    @Autowired
    private EventUnitTemplatePgmRepo eventUnitTemplatePgmRepo;

    @Autowired
    private EventUnitVehicleInfoRepo eventUnitVehicleInfoRepo;


    // DataSearch get event list
    public List<String> getEventsByUnitDates(String startDate, String endDate) {

        if (startDate != null && !startDate.isEmpty()) {
            startDate = startDate.replace("-", "").replace("/", "");
        } else {
            startDate = "19700101";
        }

        if (endDate != null && !endDate.isEmpty()) {
            endDate = endDate.replace("-", "").replace("/", "");
        } else {
            endDate = "29991231";
        }

        return eventMainRepo.GetEventsByUnitDates(startDate, endDate);
    }

    // 取得category_def列表
    public EventCategoryDef002Response getCategoryList(HttpSession session) {

        List<EventCategoryDef002Object> items = eventCategoryDefRepo.GetCtEventsMainCategoryList();

        return new EventCategoryDef002Response(200, "success", items);
    }

    // 取得pgm_def列表
    public EventPgmDef001Response getPGMList(HttpSession session) {

        List<EventPgmDef001Object> items = eventPgmDefRepo.GetPgmList();

        for (EventPgmDef001Object item : items) {

            item.setUpdateDtTm(ParseDateTime2TimeZoneDT(session, item.getmUpdateDtTm()));
        }

        return new EventPgmDef001Response(200, "success", items);
    }

    // 本山所有活動列表
    public CtEventMaintain003Response getCtEventMain(HttpSession session) {

        List<CtEventMaintain003_1Object> ctEventMaintain0031ObjectList = eventMainRepo.GetCtEventsMainList();

        List<CtEventMaintain003_1Object> items = new ArrayList<>();

        if (ctEventMaintain0031ObjectList != null) {

            List<String> eventIdList = ctEventMaintain0031ObjectList.stream().map(e -> e.getEventId())
                    .collect(Collectors.toList());

            List<CtEventMaintain003_2Object> eventEnrollMainRecords =
                    eventEnrollMainRepo.GetRecordsByEventIdList(eventIdList);

            List<CtEventMaintain003_2Object> eventUnitInfoRecords =
                    eventUnitInfoRepo.GetRecordsByEventIdList(eventIdList);

            List<CodeDef> codeDefList = codeDefRepo.GetClassCodeDefByCodeCategory("活動狀態");

            for (CtEventMaintain003_1Object c : ctEventMaintain0031ObjectList) {

                int masterEnrollCount = eventEnrollMainRecords.stream()
                        .filter(e -> e.getEventIs().equals(c.getEventId()) && e.getIsMaster() == true)
                        .collect(Collectors.toList()).size();

                int notMasterEnrollCount = eventEnrollMainRecords.stream()
                        .filter(e -> e.getEventIs().equals(c.getEventId()) && e.getIsMaster() == false)
                        .collect(Collectors.toList()).size();

                int peopleForecast = eventUnitInfoRecords.stream()
                        .filter(u -> u.getEventIs().equals(c.getEventId()))
                        .collect(Collectors.toList()).size();

                String status = codeDefList.stream()
                        .filter(n -> n.getCodeId().equals(c.getStatus())).collect(Collectors.toList())
                        .get(0).getCodeName();

                items.add(new CtEventMaintain003_1Object(
                        c.getEventYear(),
                        c.getSponsorUnitName(),
                        c.getEventCategoryName(),
                        c.getEventName(),
                        c.getEventStartDate(),
                        c.getEventEndDate(),
                        masterEnrollCount,
                        notMasterEnrollCount,
                        peopleForecast,
                        status,
                        c.getEventNote(),
                        c.getEventId(),
                        c.getUpdatorName(),
                        ParseDateTime2TimeZoneDT(session, c.getmUpdateDtTm())
                ));
            }
        }

        return new CtEventMaintain003Response(200, "success", items);
    }

    // 新增/複製活動 (如果 copyId 有值，為複製模式)
    public CtEventMaintain004Response addMainEvent(
            HttpSession session, CtEventMaintain004Request ctEventMaintain004Request, String department) throws NotFoundException {

        boolean isCt = false;

        if (department.equals("C")) {
            isCt = true;
        }

        String copyId = ctEventMaintain004Request.getCopyId();
        if (copyId != null && !copyId.isEmpty()) {
            EventMain copyEventMain = eventMainRepo.GetClassEventMainByEventId(ctEventMaintain004Request.getCopyId());

            if (copyEventMain == null) {
                throw new NotFoundException();
            }
        }

        String errMsg = "";

        CtEventMaintain004_2Object result = null;

        CtEventMaintain004_1Object data = ctEventMaintain004Request.getData();

        String eventId = eventMainEventIdGenerator(data.getEventCategoryId());

        EventMain eventMain = new EventMain();
        eventMain.copyData(data);
        eventMain.setEventId(eventId);
        eventMain.setIsCreatedByCt(isCt);
        eventMain.setIsCreatedByUnit(!isCt);
        eventMain.setIsHeldInUnit(!isCt);
        eventMain.setCreatorAndUpdater(session);

        eventMain = eventMainRepo.save(eventMain);

        // event unit info
        List<String> UnitList = Arrays.asList(eventMain.getAttendUnitList().split("/"));
        List<EventUnitInfo> eventUnitInfoList = new ArrayList<>();

        for (String unitStr : UnitList) {
            String unitId = unitStr.substring(0, 9);
            String unitName = unitStr.substring(10);

            logger.debug("add:" + unitId);
            EventUnitInfo eventUnitInfo = new EventUnitInfo();
            eventUnitInfo.copyEventMain(unitId, unitName, eventMain);
            eventUnitInfo.setCreatorAndUpdater(session);

            logger.debug("eId:" + eventUnitInfo.getEventId());
            eventUnitInfoList.add(eventUnitInfo);
        }

        eventUnitInfoRepo.saveAll(eventUnitInfoList);

        if (copyId == null || copyId.isEmpty()) {
            // 新增
            errMsg = "成功新增 {" + eventMain.getEventName() + "} 活動，請設定活動項目";

            // 新增 本山掛單PGM10014 和 搭車PGM10017
            List<EventPgmDef> eventPgmDefList = eventPgmDefRepo.GetEventPgmDefListByPgmIdList(Arrays.asList("PGM10014", "PGM10017"));

            createPgmOcaAndPgmUnit(eventMain, eventPgmDefList, department, session);
        } else {
            // 複製
            EventMain copyEventMain = eventMainRepo.GetClassEventMainByEventId(ctEventMaintain004Request.getCopyId());

            errMsg = "成功複製 {" + eventMain.getEventName() + "} 活動";

            copyPgmOcaAndPgmUnit(copyEventMain, eventMain, department, session);
            copySpecialInput(copyEventMain, eventMain, department, session);
        }

        result = new CtEventMaintain004_2Object(eventMain);

        return new CtEventMaintain004Response(200, errMsg, result);
    }


    private void createPgmOcaAndPgmUnit(EventMain eventMain, List<EventPgmDef> eventPgmDefList, String department, HttpSession session) {

        List<EventPgmOca> eventPgmOcaList = new ArrayList<>();
        List<EventPgmUnit> eventPgmUnitList = new ArrayList<>();

        List<String> pgmUniqueIdList = EventPgmOcaPgmUniqueIdListGenerate(department, eventPgmDefList.size());

        List<String> units = eventMain.getAttendUnitIds();

        for (int d = 0; d < eventPgmDefList.size(); d++) {

            EventPgmOca eventPgmOca = new EventPgmOca();
            eventPgmOca.copyEventMainAndEventPgmDef(
                    eventMain, eventPgmDefList.get(d), pgmUniqueIdList.get(d), session);

            eventPgmOcaList.add(eventPgmOca);

            for (String unit : units) {
                String unitId = unit.substring(0, 9);
                String unitName = unit.substring(10);

                EventPgmUnit eventPgmUnit = new EventPgmUnit();
                eventPgmUnit.copyEventMainAndEventPgmDef(
                        eventMain, eventPgmDefList.get(d), pgmUniqueIdList.get(d), unitId, unitName, session);

                eventPgmUnitList.add(eventPgmUnit);
            }
        }

        eventPgmOcaRepo.saveAll(eventPgmOcaList);
        eventPgmUnitRepo.saveAll(eventPgmUnitList);
    }

    private void copyPgmOcaAndPgmUnit(EventMain sourceEvent, EventMain targetEvent, String department, HttpSession session) {
        List<EventPgmOca> ocaList = eventPgmOcaRepo.GetEventPgmOcaByEventId(sourceEvent.getEventId());

        if (ocaList.size() > 0) {
            List<String> pgmUniqueIdList = EventPgmOcaPgmUniqueIdListGenerate(department, ocaList.size());

            List<EventPgmOca> newOcaList = new ArrayList<>();
            List<EventPgmUnit> newUnitList = new ArrayList<>();

            List<String> units = targetEvent.getAttendUnitIds();

            int index = 0;

            for (EventPgmOca oca : ocaList) {
                EventPgmOca newOca = new EventPgmOca();
                newOca.copy(oca, targetEvent);
                newOca.setPgmUniqueId(pgmUniqueIdList.get(index++));
                newOca.setCreatorAndUpdater(session);

                newOcaList.add(newOca);

                for (String unit : units) {
                    String unitId = unit.substring(0, 9);
                    String unitName = unit.substring(10);

                    EventPgmUnit newUnit = new EventPgmUnit();
                    newUnit.setUnitId(unitId);
                    newUnit.setUnitName(unitName);
                    newUnit.copyEventPgmOca(newOca);
                    newUnit.setCreatorAndUpdater(session);

                    newUnitList.add(newUnit);
                }
            }

            eventPgmOcaRepo.saveAll(newOcaList);
            eventPgmUnitRepo.saveAll(newUnitList);
        }
    }

    private void copySpecialInput(EventMain sourceEvent, EventMain targetEvent, String department, HttpSession session) {
        List<EventCtSpecialInput> specialList = eventCtSpecialInputRepo.GetByEventId(sourceEvent.getEventId());

        if (specialList.size() > 0) {
            List<String> sInputIdList = EventCtSpecialInputSInputIdGenerator(specialList.size());

            List<EventCtSpecialInput> newCtSpecialList = new ArrayList<>();
            List<EventUnitSpecialInput> newUnitSpecialList = new ArrayList<>();

            List<String> units = targetEvent.getAttendUnitIds();

            int index = 0;
            for (EventCtSpecialInput special : specialList) {
                EventCtSpecialInput newSpecial = new EventCtSpecialInput();
                newSpecial.copy(special, targetEvent);
                newSpecial.setSInputId(sInputIdList.get(index++));
                newSpecial.setCreatorAndUpdater(session);

                newCtSpecialList.add(newSpecial);

                for (String unit : units) {
                    String unitId = unit.substring(0, 9);
                    String unitName = unit.substring(10);

                    EventUnitSpecialInput unitSpecialInput = new EventUnitSpecialInput();
                    unitSpecialInput.setUnitId(unitId);
                    unitSpecialInput.setUnitName(unitName);
                    unitSpecialInput.copyCtSpecialInput(newSpecial);
                    unitSpecialInput.setCreatorAndUpdater(session);

                    newUnitSpecialList.add(unitSpecialInput);
                }
            }

            eventCtSpecialInputRepo.saveAll(newCtSpecialList);
            eventUnitSpecialInputRepo.saveAll(newUnitSpecialList);
        }
    }

    // 取得單取活動資料
    public CtEventMaintain005Response getMainEventByEventId(HttpSession session, String eventId) {

        int errCode = 200;
        String errMsg = "success";

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) {

            errCode = 400;
            errMsg = "找不到event main id : " + eventId;
        }

        return new CtEventMaintain005Response(errCode, errMsg, eventMain);
    }

    // 修改單筆活動資料
    public CtEventMaintain005Response editMainEventByEventId(HttpSession session, String eventId,
                                                             CtEventMaintain006Request ctEventMaintain006Request,
                                                             String department) {

        int errCode = 200;
        String errMsg = "success";

        EventMain mEventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (mEventMain == null) {

            errCode = 400;
            errMsg = "找不到event main id : " + eventId;
        } else {

            // 依據活動報名10 P.12

            // 刪除、新增event_unit_info
            List<String> oldAttendUnitList = Arrays.asList(mEventMain.getAttendUnitList().split("/"));
            List<String> newAttendUnitList = Arrays.asList(ctEventMaintain006Request.getAttendUnitList().split("/"));
            List<String> updateAttendUnitIdList = new ArrayList<>();
            List<String> deleteAttendUnitIdList = new ArrayList<>();
            List<String> addAttendUnitIdList = new ArrayList<>();
            List<EventUnitInfo> addEventUnitInfoList = new ArrayList<>();
            List<EventUnitInfo> updateEventUnitInfoList = new ArrayList<>();

            List<String> diffList = compareTwoArrayListDiff(oldAttendUnitList, newAttendUnitList);


            for (String diff : diffList) {
                String unitId = diff.substring(0, 9);
                String unitName = diff.substring(10);
                logger.debug(diff + "/" + unitId + "/" + unitName);
                if (oldAttendUnitList.contains(diff)) {

                    deleteAttendUnitIdList.add(unitId);
                    logger.debug("del:" + unitId);
                } else {
                    logger.debug("add:" + unitId);
                    EventUnitInfo eventUnitInfo = new EventUnitInfo();
                    eventUnitInfo.copyEventMain(unitId, unitName, ctEventMaintain006Request);
                    eventUnitInfo.setCreatorAndUpdater(session);

                    logger.debug("eId:" + eventUnitInfo.getEventId());
                    addEventUnitInfoList.add(eventUnitInfo);

                    addAttendUnitIdList.add(diff);
                }
            }

            for (int s = 0; s < oldAttendUnitList.size(); s++) {
                if (!newAttendUnitList.contains(oldAttendUnitList.get(s))) {
                    String unitId = oldAttendUnitList.get(s).substring(0, 9);
                    updateAttendUnitIdList.add(unitId);
                }
            }

            // del event_unit_info
            if (deleteAttendUnitIdList.size() != 0) {
                eventUnitInfoRepo.DeleteEventUnitInfoByEventIdAndUnitIdList(eventId, deleteAttendUnitIdList);
            }

            // add event_unit_info
            if (addEventUnitInfoList.size() > 0) {
                eventUnitInfoRepo.saveAll(addEventUnitInfoList);
            }

            // update event_unit_info
            if (updateAttendUnitIdList.size() != 0) {
                updateEventUnitInfoList = eventUnitInfoRepo
                        .GetEventUnitInfoListByEventIdAndUnitIdList(eventId, updateAttendUnitIdList);

                for (EventUnitInfo eventUnitInfo : updateEventUnitInfoList) {

                    eventUnitInfo.copyEventMain(eventUnitInfo.getUnitId(), eventUnitInfo.getUnitName(), ctEventMaintain006Request);
                    eventUnitInfo.setUpdater(session);
                }

                eventUnitInfoRepo.saveAll(updateEventUnitInfoList);
            }

            // del event_pgm_unit
            if (deleteAttendUnitIdList.size() != 0) {
                eventPgmUnitRepo.DeleteEventPgmUnitByEventIdAndUnitIdList(eventId, deleteAttendUnitIdList);
            }

            // add event_pgm_unit
            List<EventCategorySpecifiedPgmRecord> eventCategorySpecifiedPgmRecordList =
                    eventCategorySpecifiedPgmRecordRepo.GetEventCategorySpecifiedPgmRecordListByEventCategoryId(
                            ctEventMaintain006Request.getEventCategoryId());

            List<String> pgmIdList = eventCategorySpecifiedPgmRecordList.stream()
                    .map(p -> p.getPgmId()).collect(Collectors.toList());

            List<EventPgmOca> eventPgmOcaList = new ArrayList<>();
            List<EventPgmUnit> eventPgmUnitList = new ArrayList<>();

            if (pgmIdList.size() > 0) {
                List<EventPgmDef> eventPgmDefList = eventPgmDefRepo.GetEventPgmDefListByPgmIdList(pgmIdList);

                List<String> pgmUniqueIdList = EventPgmOcaPgmUniqueIdListGenerate(department, eventPgmDefList.size());

                for (int d = 0; d < eventPgmDefList.size(); d++) {
                    EventPgmOca eventPgmOca = new EventPgmOca();
                    eventPgmOca.copyEventMainAndEventPgmDef(
                            ctEventMaintain006Request, eventPgmDefList.get(d), pgmUniqueIdList.get(d), session);

                    eventPgmOcaList.add(eventPgmOca);

                    for (String unit : addAttendUnitIdList) {
                        String unitId = unit.substring(0, 9);
                        String unitName = unit.substring(10);

                        EventPgmUnit eventPgmUnit = new EventPgmUnit();
                        eventPgmUnit.copyEventMainAndEventPgmDef(
                                ctEventMaintain006Request, eventPgmDefList.get(d), pgmUniqueIdList.get(d), unitId, unitName, session);

                        eventPgmUnitList.add(eventPgmUnit);
                    }
                }

                if (eventPgmUnitList.size() > 0) {
                    eventPgmUnitRepo.saveAll(eventPgmUnitList);
                }
            }

            // 刪除 event_enroll_main
            if (deleteAttendUnitIdList.size() != 0) {
                eventEnrollMainRepo.DeleteEventEnrollMainByEventIdAndUnitIdList(eventId, deleteAttendUnitIdList);
            }

            // 更新 event_enroll_main
            if (updateAttendUnitIdList.size() != 0) {

                List<EventEnrollMain> eventEnrollMainList =
                        eventEnrollMainRepo.GetEventEnrollMainListByEventIdAndUntIdList(eventId, updateAttendUnitIdList);

                if (eventEnrollMainList.size() != 0) {

                    for (EventEnrollMain eventEnrollMain : eventEnrollMainList) {

                        eventEnrollMain.copyEventMain(ctEventMaintain006Request);
                        eventEnrollMain.setUpdater(session);
                    }

                    eventEnrollMainRepo.saveAll(eventEnrollMainList);
                }
            }

            // 新增、更新、刪除 event_pgm_mapping_for_ct
            EventPgmMappingForCt eventPgmMappingForCt =
                    eventPgmMappingForCtRepo.GetEventPgmMappingForCtByEventId(eventId);

            if (ctEventMaintain006Request.getIsSyncCtTable()) {

                // 新增 or 更新
                if (eventPgmMappingForCt == null) {
                    eventPgmMappingForCt = new EventPgmMappingForCt();
                    eventPgmMappingForCt.createEventPgmMappingForCt(
                            eventId, ctEventMaintain006Request.getEventName(), eventPgmOcaList);
                    eventPgmMappingForCt.setCreatorAndUpdater(session);
                } else {
                    eventPgmMappingForCt.createEventPgmMappingForCt(
                            eventId, ctEventMaintain006Request.getEventName(), eventPgmOcaList);
                    eventPgmMappingForCt.setUpdater(session);
                }

                eventPgmMappingForCtRepo.save(eventPgmMappingForCt);
            } else {

                // 刪除
                eventPgmMappingForCtRepo.DeleteEventPgmMappingForCtByEventId(eventId);
            }

            if (deleteAttendUnitIdList.size() != 0) {

                // 刪除 event_enroll_pgm
                eventEnrollPgmRepo.DeleteEventEnrollPgmByEventIdAndUnitIdList(eventId, deleteAttendUnitIdList);

                // 刪除 event_unit_template
                eventUnitTemplateRepo.DeleteEventUnitTemplateByEventIdAndUnitIdList(eventId, deleteAttendUnitIdList);

                // 刪除 event_unit_template_pgm
                eventUnitTemplatePgmRepo.DeleteEventUnitTemplatePgmByEventIdAndUnitIdList(eventId, deleteAttendUnitIdList);

                // 刪除 event_form_for_ct
                eventFormForCtRepo.DeleteEventFormForCtByEventIdAndUnitIdList(eventId, deleteAttendUnitIdList);

                // 刪除 event_unit_special_input
                eventUnitSpecialInputRepo.DeleteEventUnitSpecialInputByEventIdAndUnitIdList(eventId, deleteAttendUnitIdList);

                // 刪除 event_unit_vehicle_info
                eventUnitVehicleInfoRepo.DeleteEventUnitVehicleInfoByEventIdAndUnitIdList(eventId, deleteAttendUnitIdList);
            }

            // 修改event_main
            mEventMain.copyEventMain(ctEventMaintain006Request);
            mEventMain.setUpdater(session);

            eventMainRepo.save(mEventMain);

            // add unit special inputs
            List<EventCtSpecialInput> ctSpecialInputList = eventCtSpecialInputRepo.GetByEventId(eventId);

            if (ctSpecialInputList.size() > 0) {
                List<EventUnitSpecialInput> newUnitSpecialInputList = new ArrayList<>();

                for (String unit : addAttendUnitIdList) {
                    String unitId = unit.substring(0, 9);
                    String unitName = unit.substring(10);

                    for (EventCtSpecialInput ctSpecialInput : ctSpecialInputList) {
                        EventUnitSpecialInput sInput = new EventUnitSpecialInput();
                        sInput.setUnitId(unitId);
                        sInput.setUnitName(unitName);
                        sInput.copyCtSpecialInput(ctSpecialInput);
                        sInput.setCreatorAndUpdater(session);

                        newUnitSpecialInputList.add(sInput);
                    }
                }

                eventUnitSpecialInputRepo.saveAll(newUnitSpecialInputList);
            }
        }

        return new CtEventMaintain005Response(errCode, errMsg, mEventMain);
    }

    // 刪除單筆活動資料
    public BaseResponse deleteMainEventByEventId(HttpSession session, String eventId) {

        // Delete event_enroll_main
        eventEnrollMainRepo.DeleteEventEnrollMainByEventId(eventId);

        // Delete event_enroll_pgm
        eventEnrollPgmRepo.DeleteEventEnrollPgmByEventId(eventId);

        // Delete event_unit_info
        eventUnitInfoRepo.DeleteEventUnitInfoByEventId(eventId);

        // Delete event_pgm_unit
        eventPgmUnitRepo.DeleteEventPgmUnitByEventId(eventId);

        // Delete event_unit_template
        eventUnitTemplateRepo.DeleteEventUnitTemplateByEventId(eventId);

        // Delete event_unit_template_pgm
        eventUnitTemplatePgmRepo.DeleteEventUnitTemplatePgmByEventId(eventId);

        // Delete event_form_for_ct
        eventFormForCtRepo.DeleteEventFormForCtByEventId(eventId);

        // Delete event_unit_special_input
        eventUnitSpecialInputRepo.DeleteEventUnitSpecialInputByEventId(eventId);

        // Delete event_unit_vehicle_info
        eventUnitVehicleInfoRepo.DeleteEventUnitVehicleInfoByEventId(eventId);

        // Delete event_pgm_mapping_for_ct
        eventPgmMappingForCtRepo.DeleteEventPgmMappingForCtByEventId(eventId);

        // Delete event_main
        eventMainRepo.DeleteEventMainByEventId(eventId);

        return new BaseResponse(200, "success");
    }

    // 同步報名表到中台資料表btn Step1
    public BaseResponse syncCtTable(HttpSession session, String eventId) {

        int errCode = 200;
        String errMsg = "success";

        List<EventEnrollMain> eventEnrollMainList = eventEnrollMainRepo.GetClassEventEnrollMainListByEventId(eventId);

        List<EventEnrollPgm> eventEnrollPgmList = eventEnrollPgmRepo.GetClassEventEnrollPgmListByEventId(eventId);

        if (eventEnrollMainList.size() == 0) {

            errCode = 400;
            errMsg = "目前沒有資料可以同步";
        } else {

            // 所有要同步的formAutoAddNum
            List<String> formAutoAddNumList = eventEnrollMainList.stream()
                    .map(f -> f.getFormAutoAddNum()).collect(Collectors.toList());

            // 同步過
            // 同步過的EventFormForCt
            List<EventFormForCt> ctSyncList =
                    eventFormForCtRepo.GetEventFormForCtListByFormAutoAddNumList(formAutoAddNumList);

            if (ctSyncList.size() != 0) {

                for (EventFormForCt ct : ctSyncList) {

                    EventEnrollMain mEventEnrollMain = eventEnrollMainList.stream()
                            .filter(m -> m.getFormAutoAddNum().equals(ct.getFormAutoAddNum()))
                            .collect(Collectors.toList()).get(0);

                    List<EventEnrollPgm> mEventEnrollPgmList = eventEnrollPgmList.stream()
                            .filter(p -> p.getFormAutoAddNum().equals(ct.getFormAutoAddNum()))
                            .collect(Collectors.toList());

                    ct.copyEventEnrollMainAndEventEnrollPgm(mEventEnrollMain, mEventEnrollPgmList);
                    ct.setEventFormForCtUpdateDtTm(new Date());
                    ct.setUpdater(session);

                    // 從formAutoAddNumList中，移除同步過的formAutoAddNum
                    for (int x = 0; x < formAutoAddNumList.size(); x++) {

                        if (ct.getFormAutoAddNum().equals(formAutoAddNumList.get(x))) {

                            formAutoAddNumList.remove(x);
                        }
                    }
                }

                eventFormForCtRepo.saveAll(ctSyncList);
            }

            // 未同步過
            List<EventFormForCt> ctNotSyncList = new ArrayList<>();

            for (String formAutoAddNum : formAutoAddNumList) {

                EventEnrollMain mEventEnrollMain = eventEnrollMainList.stream()
                        .filter(m -> m.getFormAutoAddNum().equals(formAutoAddNum))
                        .collect(Collectors.toList()).get(0);

                List<EventEnrollPgm> mEventEnrollPgmList = eventEnrollPgmList.stream()
                        .filter(p -> p.getFormAutoAddNum().equals(formAutoAddNum))
                        .collect(Collectors.toList());

                EventFormForCt ct = new EventFormForCt();
                ct.copyEventEnrollMainAndEventEnrollPgm(mEventEnrollMain, mEventEnrollPgmList);
                ct.setEventFormForCtCreateDtTm(new Date());
                ct.setCreator(session);

                ctNotSyncList.add(ct);
            }

            eventFormForCtRepo.saveAll(ctNotSyncList);
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 刪除中台資料表的報名表btn
    public BaseResponse deleteSyncCtTable(HttpSession session, String eventId) {

        String eventName = eventMainRepo.GetEventNameByEventId(eventId);
        int count = eventFormForCtRepo.GetEventFormForCtCountByEventId(eventId);

        if (count != 0) {

            eventFormForCtRepo.DeleteEventFormForCtByEventId(eventId);
        }

        return new BaseResponse(200, "成功刪除 " + eventName + " 活動{" + count + "}筆中台資料表的報名表");
    }

    // 匯入活動類別預定項目
    public CtEventMaintain010Response importEventCategoryItem(HttpSession session, String eventId) {

        int errCode = 200;
        String errMsg = "success";
        List<CtEventMaintain010Object> importedItems = new ArrayList<>();

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) {
            errCode = 400;
            errMsg = "找不到event main id : " + eventId;
        } else {
            List<EventCategorySpecifiedPgmRecord> eventCategorySpecifiedPgmRecordList =
                    eventCategorySpecifiedPgmRecordRepo.GetEventCategorySpecifiedPgmRecordListByEventCategoryId(
                            eventMain.getEventCategoryId());

            String[] units = eventMain.getAttendUnitList().split("/");

            List<String> pgmIdList = new ArrayList<>();

            for (EventCategorySpecifiedPgmRecord c : eventCategorySpecifiedPgmRecordList) {
                pgmIdList.add(c.getPgmId());
            }

            if (pgmIdList.size() > 0) {
                List<EventPgmOca> mEventPgmOcaList = new ArrayList<>();
                List<EventPgmUnit> eventPgmUnitList = new ArrayList<>();

                List<EventPgmDef> eventPgmDefList = eventPgmDefRepo.GetEventPgmDefListByPgmIdList(pgmIdList);

                List<String> pgmUniqueIdList = EventPgmOcaPgmUniqueIdListGenerate("C", eventPgmDefList.size());

                for (String pgmUniqueId : pgmUniqueIdList) {

                    EventPgmOca eventPgmOca = new EventPgmOca();
                    eventPgmOca.setPgmUniqueId(pgmUniqueId);

                    mEventPgmOcaList.add(eventPgmOca);
                }

                if (mEventPgmOcaList.size() > 0) {
                    eventPgmOcaRepo.saveAll(mEventPgmOcaList);
                }

                List<EventPgmOca> eventPgmOcaList = eventPgmOcaRepo.GetEventPgmOcaListByPgmUniqueIdList(pgmUniqueIdList);

                int temp = 0;

                // dorm, trans pgm(s) will not be imported
                final List<String> PGM_AVOID_LIST = Arrays.asList(new String[]{"PGM10014", "PGM10017"});

                for (EventPgmDef p : eventPgmDefList) {
                    String pgmUniqueId = pgmUniqueIdList.get(temp);
                    temp++;

                    Date pgmDate = null;

                    if (p.getIsGenPgm()) {
                        pgmDate = null;
                    } else {
                        if (p.getPgmId().equals("PGM10017")) {
                            pgmDate = null;
                        } else {
                            pgmDate = eventMain.getEventStartDate();
                        }
                    }

                    EventPgmOca eventPgmOca = eventPgmOcaList.stream()
                            .filter(w -> w.getPgmUniqueId().equals(pgmUniqueId)).collect(Collectors.toList()).get(0);

                    // not import dorm & trans pgm(s) again
                    if (!PGM_AVOID_LIST.contains(p.getPgmId())) {
                        eventPgmOca.setEventId(eventMain.getEventId());
                        eventPgmOca.setEventName(eventMain.getEventName());
                        eventPgmOca.setEventCreatorUnitId(eventMain.getEventCreatorUnitId());
                        eventPgmOca.setEventCreatorUnitName(eventMain.getEventCreatorUnitName());
                        eventPgmOca.setSponsorUnitId(eventMain.getSponsorUnitId());
                        eventPgmOca.setSponsorUnitName(eventMain.getSponsorUnitName());
                        eventPgmOca.setEventCategoryId(eventMain.getEventCategoryId());
                        eventPgmOca.setEventCategoryName(eventMain.getEventCategoryName());
                        eventPgmOca.setPgmId(p.getPgmId());
                        eventPgmOca.setPgmName(p.getPgmName());
                        eventPgmOca.setIsGenPgm(p.getIsGenPgm());
                        eventPgmOca.setPgmCategoryId(p.getPgmCategoryId());
                        eventPgmOca.setPgmCategoryName(p.getPgmCategoryName());
                        eventPgmOca.setPgmDesc(p.getPgmDesc());
                        eventPgmOca.setPgmNote(p.getPgmNote());
                        eventPgmOca.setPgmEndTime(p.getPgmEndTime());
                        eventPgmOca.setPgmStartTime(p.getPgmStartTime());
                        eventPgmOca.setIsRequired(false);
                        eventPgmOca.setIsDisabledByOCA(false);
                        eventPgmOca.setPgmCtOrderNum(null);
                        eventPgmOca.setPgmUiOrderNum(null);
                        eventPgmOca.setPgmDate(null);
                        eventPgmOca.setGoTransDate(null);
                        eventPgmOca.setGoTransType(null);
                        eventPgmOca.setReturnTransDate(null);
                        eventPgmOca.setReturnTransType(null);
                        eventPgmOca.setPgmStartTime(p.getPgmStartTime());
                        eventPgmOca.setPgmEndTime(p.getPgmEndTime());
                        eventPgmOca.setPgmDate(pgmDate);
                        eventPgmOca.setCreator(session);

                        for (String unit : units) {

                            String unitId = unit.substring(0, 9);
                            String unitName = unit.substring(10);

                            EventPgmUnit eventPgmUnit = new EventPgmUnit();
                            eventPgmUnit.setEventId(eventMain.getEventId());
                            eventPgmUnit.setEventName(eventMain.getEventName());
                            eventPgmUnit.setEventCreatorUnitId(eventMain.getEventCreatorUnitId());
                            eventPgmUnit.setEventCreatorUnitName(eventMain.getEventCreatorUnitName());
                            eventPgmUnit.setSponsorUnitId(eventMain.getSponsorUnitId());
                            eventPgmUnit.setSponsorUnitName(eventMain.getSponsorUnitName());
                            eventPgmUnit.setEventCategoryId(eventMain.getEventCategoryId());
                            eventPgmUnit.setEventCategoryName(eventMain.getEventCategoryName());
                            eventPgmUnit.setPgmId(p.getPgmId());
                            eventPgmUnit.setPgmName(p.getPgmName());
                            eventPgmUnit.setIsGenPgm(p.getIsGenPgm());
                            eventPgmUnit.setPgmCategoryId(p.getPgmCategoryId());
                            eventPgmUnit.setPgmCategoryName(p.getPgmCategoryName());
                            eventPgmUnit.setPgmDesc(p.getPgmDesc());
                            eventPgmUnit.setPgmNote(p.getPgmNote());
                            eventPgmUnit.setPgmEndTime(p.getPgmEndTime());
                            eventPgmUnit.setPgmStartTime(p.getPgmStartTime());
                            eventPgmUnit.setPgmUniqueId(pgmUniqueId);
                            eventPgmUnit.setIsRequired(false);
                            eventPgmUnit.setIsDisabledByOCA(false);
                            eventPgmUnit.setUnitId(unitId);
                            eventPgmUnit.setUnitName(unitName);
                            eventPgmUnit.setPgmDate(pgmDate);
                            eventPgmUnit.setPgmStartTime(p.getPgmStartTime());
                            eventPgmUnit.setPgmEndTime(p.getPgmEndTime());
                            eventPgmUnit.setCreator(session);

                            eventPgmUnitList.add(eventPgmUnit);
                        }
                    }
                }

                if (eventPgmOcaList.size() > 0) {
                    eventPgmOcaRepo.saveAll(eventPgmOcaList);
                }
                if (eventPgmUnitList.size() > 0) {
                    eventPgmUnitRepo.saveAll(eventPgmUnitList);
                }

                for (EventPgmOca eventPgmOca : eventPgmOcaList) {

                    CtEventMaintain010Object ctEventMaintain010Object = new CtEventMaintain010Object(
                            eventPgmOca.getPgmUniqueId(),
                            eventPgmOca.getIsDisabledByOCA(),
                            eventPgmOca.getIsRequired(),
                            eventPgmOca.getPgmUiOrderNum(),
                            DateTime2DateString(eventPgmOca.getPgmDate()),
                            DateTime2TimeString(eventPgmOca.getPgmStartTime()),
                            DateTime2TimeString(eventPgmOca.getPgmEndTime()),
                            eventPgmOca.getPgmCtOrderNum(),
                            eventPgmOca.getPgmName(),
                            eventPgmOca.getPgmNote(),
                            eventPgmOca.getPgmDesc(),
                            DateTime2DateString(eventPgmOca.getGoTransDate()),
                            eventPgmOca.getGoTransType(),
                            DateTime2DateString(eventPgmOca.getReturnTransDate()),
                            eventPgmOca.getReturnTransType()
                    );

                    importedItems.add(ctEventMaintain010Object);
                }
            }
        }

        return new CtEventMaintain010Response(errCode, errMsg, importedItems);
    }

    // 取得活動pgm_oca列表
    public CtEventMaintain011Response getEventPgmOcaList(HttpSession session, String eventId) {

        List<CtEventMaintain011Object> ctEventMaintain011ObjectList =
                eventPgmOcaRepo.GetCtEventPgmOcaListByEventId(eventId);

        return new CtEventMaintain011Response(200, "success", ctEventMaintain011ObjectList);
    }

    // 批次新增 pgm_ocas
    public CtEventMaintain012Response addEventPgmOcaList(HttpSession session, String eventId,
                                                         CtEventMaintain012Request ctEventMaintain012Request) {

        int errCode = 200;
        String errMsg = "success";
        List<CtEventMaintain012_2Object> importedItems = new ArrayList<>();

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) {

            errCode = 400;
            errMsg = "找不到event main id : " + eventId;
        } else {

            String[] units = eventMain.getAttendUnitList().split("/");

            List<String> pgmIdList = new ArrayList<>();

            for (CtEventMaintain012_1Object c : ctEventMaintain012Request.getItems()) {

                pgmIdList.add(c.getPgmId());
            }

            List<EventPgmOca> mEventPgmOcaList = new ArrayList<>();
            List<EventPgmUnit> eventPgmUnitList = new ArrayList<>();

            List<EventPgmDef> eventPgmDefList = eventPgmDefRepo.GetEventPgmDefListByPgmIdList(pgmIdList);

            List<String> pgmUniqueIdList = EventPgmOcaPgmUniqueIdListGenerate("C", eventPgmDefList.size());

            for (String pgmUniqueId : pgmUniqueIdList) {

                EventPgmOca eventPgmOca = new EventPgmOca();
                eventPgmOca.setPgmUniqueId(pgmUniqueId);

                mEventPgmOcaList.add(eventPgmOca);
            }

            eventPgmOcaRepo.saveAll(mEventPgmOcaList);

            List<EventPgmOca> eventPgmOcaList = eventPgmOcaRepo.GetEventPgmOcaListByPgmUniqueIdList(pgmUniqueIdList);

            int temp = 0;

            for (EventPgmDef p : eventPgmDefList) {

                String pgmUniqueId = pgmUniqueIdList.get(temp);
                temp++;

                Date pgmDate = null;

                if (p.getIsGenPgm()) {

                    pgmDate = null;
                } else {

                    if (p.getPgmId().equals("PGM10017")) {

                        pgmDate = null;
                    } else {

                        pgmDate = eventMain.getEventStartDate();
                    }
                }

                EventPgmOca eventPgmOca = eventPgmOcaList.stream()
                        .filter(w -> w.getPgmUniqueId().equals(pgmUniqueId)).collect(Collectors.toList()).get(0);

                eventPgmOca.setEventId(eventMain.getEventId());
                eventPgmOca.setEventName(eventMain.getEventName());
                eventPgmOca.setEventCreatorUnitId(eventMain.getEventCreatorUnitId());
                eventPgmOca.setEventCreatorUnitName(eventMain.getEventCreatorUnitName());
                eventPgmOca.setSponsorUnitId(eventMain.getSponsorUnitId());
                eventPgmOca.setSponsorUnitName(eventMain.getSponsorUnitName());
                eventPgmOca.setEventCategoryId(eventMain.getEventCategoryId());
                eventPgmOca.setEventCategoryName(eventMain.getEventCategoryName());
                eventPgmOca.setPgmId(p.getPgmId());
                eventPgmOca.setPgmName(p.getPgmName());
                eventPgmOca.setIsGenPgm(p.getIsGenPgm());
                eventPgmOca.setPgmCategoryId(p.getPgmCategoryId());
                eventPgmOca.setPgmCategoryName(p.getPgmCategoryName());
                eventPgmOca.setPgmDesc(p.getPgmDesc());
                eventPgmOca.setPgmNote(p.getPgmNote());
                eventPgmOca.setPgmEndTime(p.getPgmEndTime());
                eventPgmOca.setPgmStartTime(p.getPgmStartTime());
                eventPgmOca.setIsRequired(false);
                eventPgmOca.setIsDisabledByOCA(false);
                eventPgmOca.setPgmCtOrderNum(null);
                eventPgmOca.setPgmUiOrderNum(null);
                eventPgmOca.setGoTransDate(null);
                eventPgmOca.setGoTransType(null);
                eventPgmOca.setReturnTransDate(null);
                eventPgmOca.setReturnTransType(null);
                eventPgmOca.setPgmStartTime(p.getPgmStartTime());
                eventPgmOca.setPgmEndTime(p.getPgmEndTime());
                eventPgmOca.setPgmDate(pgmDate);
                eventPgmOca.setCreator(session);


                for (String unit : units) {

                    String unitId = unit.substring(0, 9);
                    String unitName = unit.substring(10);

                    EventPgmUnit eventPgmUnit = new EventPgmUnit();
                    eventPgmUnit.setEventId(eventMain.getEventId());
                    eventPgmUnit.setEventName(eventMain.getEventName());
                    eventPgmUnit.setEventCreatorUnitId(eventMain.getEventCreatorUnitId());
                    eventPgmUnit.setEventCreatorUnitName(eventMain.getEventCreatorUnitName());
                    eventPgmUnit.setSponsorUnitId(eventMain.getSponsorUnitId());
                    eventPgmUnit.setSponsorUnitName(eventMain.getSponsorUnitName());
                    eventPgmUnit.setEventCategoryId(eventMain.getEventCategoryId());
                    eventPgmUnit.setEventCategoryName(eventMain.getEventCategoryName());
                    eventPgmUnit.setPgmId(p.getPgmId());
                    eventPgmUnit.setPgmName(p.getPgmName());
                    eventPgmUnit.setIsGenPgm(p.getIsGenPgm());
                    eventPgmUnit.setPgmCategoryId(p.getPgmCategoryId());
                    eventPgmUnit.setPgmCategoryName(p.getPgmCategoryName());
                    eventPgmUnit.setPgmDesc(p.getPgmDesc());
                    eventPgmUnit.setPgmNote(p.getPgmNote());
                    eventPgmUnit.setPgmEndTime(p.getPgmEndTime());
                    eventPgmUnit.setPgmStartTime(p.getPgmStartTime());
                    eventPgmUnit.setPgmUniqueId(pgmUniqueId);
                    eventPgmUnit.setIsRequired(false);
                    eventPgmUnit.setIsDisabledByOCA(false);
                    eventPgmUnit.setUnitId(unitId);
                    eventPgmUnit.setUnitName(unitName);
                    eventPgmUnit.setPgmDate(pgmDate);
                    eventPgmUnit.setPgmStartTime(p.getPgmStartTime());
                    eventPgmUnit.setPgmEndTime(p.getPgmEndTime());
                    eventPgmUnit.setCreator(session);

                    eventPgmUnitList.add(eventPgmUnit);
                }
            }

            if (eventPgmOcaList.size() > 0) {
                eventPgmOcaRepo.saveAll(eventPgmOcaList);
            }
            if (eventPgmUnitList.size() > 0) {
                eventPgmUnitRepo.saveAll(eventPgmUnitList);
            }

            for (EventPgmOca eventPgmOca : eventPgmOcaList) {

                CtEventMaintain012_2Object ctEventMaintain012_2Object = new CtEventMaintain012_2Object(
                        eventPgmOca.getPgmUniqueId(),
                        eventPgmOca.getIsDisabledByOCA(),
                        eventPgmOca.getIsRequired(),
                        eventPgmOca.getPgmUiOrderNum(),
                        DateTime2DateString(eventPgmOca.getPgmDate()),
                        DateTime2TimeString(eventPgmOca.getPgmStartTime()),
                        DateTime2TimeString(eventPgmOca.getPgmEndTime()),
                        eventPgmOca.getPgmCtOrderNum(),
                        eventPgmOca.getPgmName(),
                        eventPgmOca.getPgmNote(),
                        eventPgmOca.getPgmDesc(),
                        DateTime2DateString(eventPgmOca.getGoTransDate()),
                        eventPgmOca.getGoTransType(),
                        DateTime2DateString(eventPgmOca.getReturnTransDate()),
                        eventPgmOca.getReturnTransType()
                );

                importedItems.add(ctEventMaintain012_2Object);
            }
        }

        return new CtEventMaintain012Response(errCode, errMsg, importedItems);
    }

    // 修改單筆event_pgm_oca
    public BaseResponse editEventPgmOca(HttpSession session, String eventId, String pgmUniqueId,
                                        CtEventMaintain013Request request) {

        int errCode = 200;
        String errMsg = "success";

        EventPgmOca eventPgmOca = eventPgmOcaRepo.GetEventPgmOcaByEventIdAndPgmUniqueId(pgmUniqueId);

        int pgmCtOrderNumCount = 0;
        int pgmUiOrderNumCount = 0;

        if (request.getPgmCtOrderNum() != null) {
            pgmCtOrderNumCount = eventPgmOcaRepo.CheckIfPgmCtOrderNumExist(eventId, pgmUniqueId, request.getPgmCtOrderNum());
        }

        if (request.getPgmUiOrderNum() != null) {
            pgmUiOrderNumCount = eventPgmOcaRepo.CheckIfPgmUiOrderNumExist(eventId, pgmUniqueId, request.getPgmUiOrderNum(), ParseDate(request.getPgmDate()));
        }

        if (eventPgmOca == null) {
            errCode = 400;
            errMsg = "找不到eventPgmOca pgmUniqueId : " + pgmUniqueId;
        } else if (pgmCtOrderNumCount > 0) {
            errCode = 400;
            errMsg = "中台項目順序{" + request.getPgmCtOrderNum() + "}已經存在，請更換順序後再繼續。";
        } else if (pgmUiOrderNumCount > 0) {
            errCode = 400;
            errMsg = "排序碼{" + request.getPgmUiOrderNum() + "}已經存在，請更換順序後再繼續。";
        } else {
            eventPgmOca.setIsDisabledByOCA(request.getIsDisabledByOCA());
            eventPgmOca.setIsRequired(request.getIsRequired());
            eventPgmOca.setPgmUiOrderNum(request.getPgmUiOrderNum());
            eventPgmOca.setPgmDate(ParseDate(request.getPgmDate()));
            eventPgmOca.setPgmStartTime(ParseTime(request.getPgmStartTime()));
            eventPgmOca.setPgmEndTime(ParseTime(request.getPgmEndTime()));
            eventPgmOca.setPgmCtOrderNum(request.getPgmCtOrderNum());
            eventPgmOca.setPgmName(request.getPgmName());
            eventPgmOca.setPgmNote(request.getPgmNote());
            eventPgmOca.setPgmDesc(request.getPgmDesc());
            eventPgmOca.setGoTransDate(ParseDate(request.getGoTransDate()));
            eventPgmOca.setGoTransType(request.getGoTransType());
            eventPgmOca.setReturnTransDate(ParseDate(request.getReturnTransDate()));
            eventPgmOca.setReturnTransType(request.getReturnTransType());
            eventPgmOca.setUpdater(session);

            eventPgmOcaRepo.save(eventPgmOca);

            // update event_pgm_unit list
            List<EventPgmUnit> eventPgmUnitList = eventPgmUnitRepo.GetEventPgmUnitListByPgmUniqueId(pgmUniqueId);

            for (EventPgmUnit eventPgmUnit : eventPgmUnitList) {

                eventPgmUnit.setIsDisabledByOCA(request.getIsDisabledByOCA());
                eventPgmUnit.setIsRequired(request.getIsRequired());
                eventPgmUnit.setPgmUiOrderNum(request.getPgmUiOrderNum());
                eventPgmUnit.setPgmDate(ParseDate(request.getPgmDate()));
                eventPgmUnit.setPgmStartTime(ParseTime(request.getPgmStartTime()));
                eventPgmUnit.setPgmEndTime(ParseTime(request.getPgmEndTime()));
                eventPgmUnit.setPgmCtOrderNum(request.getPgmCtOrderNum());
                eventPgmUnit.setPgmName(request.getPgmName());
                eventPgmUnit.setPgmNote(request.getPgmNote());
                eventPgmUnit.setPgmDesc(request.getPgmDesc());
                eventPgmUnit.setGoTransDate(ParseDate(request.getGoTransDate()));
                eventPgmUnit.setGoTransType(request.getGoTransType());
                eventPgmUnit.setReturnTransDate(ParseDate(request.getReturnTransDate()));
                eventPgmUnit.setReturnTransType(request.getReturnTransType());
                eventPgmUnit.setUpdater(session);
            }

            eventPgmUnitRepo.saveAll(eventPgmUnitList);

            // update event_enroll_pgm
            List<EventEnrollPgm> eventEnrollPgmList =
                    eventEnrollPgmRepo.GetClassEventEnrollPgmListByPgmUniqueId(pgmUniqueId);

            if (eventEnrollPgmList.size() != 0) {

                for (EventEnrollPgm eventEnrollPgm : eventEnrollPgmList) {
                    eventEnrollPgm.setIsDisabledByOCA(request.getIsDisabledByOCA());
                    eventEnrollPgm.setPgmDate(ParseDate(request.getPgmDate()));
                    eventEnrollPgm.setPgmStartTime(ParseTime(request.getPgmStartTime()));
                    eventEnrollPgm.setPgmEndTime(ParseTime(request.getPgmEndTime()));
                    eventEnrollPgm.setPgmDesc(request.getPgmDesc());
                    eventEnrollPgm.setPgmNote(request.getPgmNote());
                    eventEnrollPgm.setIsRequired(request.getIsRequired());
                    eventEnrollPgm.setUpdater(session);
                }

                eventEnrollPgmRepo.saveAll(eventEnrollPgmList);
            }

            // update event_unit_template_pgm
            List<EventUnitTemplatePgm> eventUnitTemplatePgmList =
                    eventUnitTemplatePgmRepo.GetEventUnitTemplatePgmListByPgmUniqueId(pgmUniqueId);

            if (eventUnitTemplatePgmList.size() != 0) {

                for (EventUnitTemplatePgm eventUnitTemplatePgm : eventUnitTemplatePgmList) {
                    eventUnitTemplatePgm.setIsDisabledByOCA(request.getIsDisabledByOCA());
                    eventUnitTemplatePgm.setPgmDate(ParseDate(request.getPgmDate()));
                    eventUnitTemplatePgm.setPgmStartTime(ParseTime(request.getPgmStartTime()));
                    eventUnitTemplatePgm.setPgmEndTime(ParseTime(request.getPgmEndTime()));
                    eventUnitTemplatePgm.setPgmDesc(request.getPgmDesc());
                    eventUnitTemplatePgm.setPgmNote(request.getPgmNote());
                    eventUnitTemplatePgm.setIsRequired(request.getIsRequired());
                    eventUnitTemplatePgm.setUpdater(session);
                }

                eventUnitTemplatePgmRepo.saveAll(eventUnitTemplatePgmList);
            }

            // update event_pgm_mapping_for_ct
            if (request.getPgmCtOrderNum() != null) {

                EventPgmMappingForCt eventPgmMappingForCt =
                        eventPgmMappingForCtRepo.GetEventPgmMappingForCtByEventId(eventId);

                if (eventPgmMappingForCt != null) {

                    if (eventPgmOca.getPgmCtOrderNum() != null) {

                        String value = null;

                        if (eventPgmOca.getIsDisabledByOCA()) {

                            value = "(取消)" + eventPgmOca.getPgmDate() + eventPgmOca.getPgmName();
                        } else {

                            value = eventPgmOca.getPgmDate() + eventPgmOca.getPgmName();
                        }

                        if (eventPgmOca.getPgmCtOrderNum() != null && eventPgmOca.getPgmCtOrderNum() >= 1 && eventPgmOca.getPgmCtOrderNum() <= 50) {

                            try {
                                Method method = eventPgmMappingForCt.getClass().getMethod("setCtPgm" + String.format("%02d", eventPgmOca.getPgmCtOrderNum()), value.getClass());
                                method.invoke(eventPgmMappingForCt, value);
                            } catch (Exception e) {
                                logger.debug(e.getMessage());
                            }
                        }
                    }
                    eventPgmMappingForCtRepo.save(eventPgmMappingForCt);
                }
            }
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 刪除單筆event_pgm_oca
    public BaseResponse deleteEventPgmOca(HttpSession session, String eventId, String pgmUniqueId) {

        EventPgmOca eventPgmOca = eventPgmOcaRepo.GetEventPgmOcaByEventIdAndPgmUniqueId(pgmUniqueId);

        if (eventPgmOca.getPgmCtOrderNum() != null) {

            // Update event_pgm_mapping_for_ct
            EventPgmMappingForCt eventPgmMappingForCt =
                    eventPgmMappingForCtRepo.GetEventPgmMappingForCtByEventId(eventId);

            if (eventPgmMappingForCt != null) {

                String value = null;

                try {
                    Method method = eventPgmMappingForCt.getClass().getMethod("setCtPgm" + String.format("%02d", eventPgmOca.getPgmCtOrderNum()), value.getClass());
                    method.invoke(eventPgmMappingForCt, value);
                } catch (Exception e) {
                    logger.debug(e.getMessage());
                }

                eventPgmMappingForCt.setUpdater(session);

                eventPgmMappingForCtRepo.save(eventPgmMappingForCt);

                // Update event_form_for_ct
                List<EventFormForCt> eventFormForCtList = eventFormForCtRepo.GetEventFormForCtListByEventId(eventId);

                if (eventFormForCtList.size() != 0) {

                    for (EventFormForCt eventFormForCt : eventFormForCtList) {

                        try {
                            Method method = eventFormForCt.getClass().getMethod("setCtPgm" + String.format("%02d", eventPgmOca.getPgmCtOrderNum()), value.getClass());
                            method.invoke(eventPgmMappingForCt, value);
                        } catch (Exception e) {
                            logger.debug(e.getMessage());
                        }

                        eventFormForCt.setUpdater(session);
                    }

                    eventFormForCtRepo.saveAll(eventFormForCtList);
                }
            }
        }

        // Delete event_pgm_oca
        eventPgmOcaRepo.DeleteEventPgmOcaByPgmUniqueId(pgmUniqueId);

        // Delete event_pgm_unit
        eventPgmUnitRepo.DeleteEventPgmUnitByPgmUniqueId(pgmUniqueId);

        // Delete event_enroll_pgm
        eventEnrollPgmRepo.DeleteEventEnrollPgmByPgmUniqueId(pgmUniqueId);

        return new BaseResponse(200, "success");
    }

    // 取得活動特殊輸入項目列表
    public CtEventMaintain016Response getSpecialInputs(HttpSession session, String eventId) {

        List<CtEventMaintain016Object> items =
                eventCtSpecialInputRepo.GetEventUnitSpecialInputListByEventId(eventId);

        return new CtEventMaintain016Response(200, "success", items);
    }

    // 新增特殊輸入項目
    public CtEventMaintain017Response addSpecialInput(HttpSession session, String eventId,
                                                      CtEventMaintain017Request ctEventMaintain017Request) {

        int errCode = 200;
        String errMsg = "success";

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) {

            errCode = 400;
            errMsg = "找不到event main id : " + eventId;
        } else {

            int count = 0;

            if (ctEventMaintain017Request.getsInputOrderNum() != null) {

                count = eventCtSpecialInputRepo.GetEventCtSpecialInputCountByEventIdAndsInputOrderNum(
                        eventId, ctEventMaintain017Request.getsInputOrderNum());
            }

            if (count != 0 && ctEventMaintain017Request.getsInputOrderNum() != null) {

                errCode = 400;
                errMsg = "排序碼{" + ctEventMaintain017Request.getsInputOrderNum() + "}已經存在，請重新選擇排序碼再存檔";
            } else {

                EventCtSpecialInput eventCtSpecialInput = new EventCtSpecialInput();
                eventCtSpecialInput.setEventId(eventMain.getEventId());
                eventCtSpecialInput.setEventName(eventMain.getEventName());
                eventCtSpecialInput.setIsRequired(ctEventMaintain017Request.getIsRequired());
                eventCtSpecialInput.setSInputId(EventCtSpecialInputSInputIdGenerator(1).get(0));

                ctEventMaintain017Request.setsInputId(eventCtSpecialInput.getSInputId());

                eventCtSpecialInput.setSInputName(ctEventMaintain017Request.getsInputName());
                eventCtSpecialInput.setSInputCategory(Empty2Null(ctEventMaintain017Request.getsInputCategory()));
                eventCtSpecialInput.setSInputOrderNum(ctEventMaintain017Request.getsInputOrderNum());
                eventCtSpecialInput.setSInputWriteType(ctEventMaintain017Request.getsInputWriteType());
                eventCtSpecialInput.setSInputExample(ctEventMaintain017Request.getsInputExample());
                eventCtSpecialInput.setCreatorAndUpdater(session);

                eventCtSpecialInputRepo.save(eventCtSpecialInput);

                List<EventUnitSpecialInput> eventUnitSpecialInputList = new ArrayList<>();

                String[] units = eventMain.getAttendUnitList().split("/");

                for (String unit : units) {

                    String unitId = unit.substring(0, 9);
                    String unitName = unit.substring(10);

                    EventUnitSpecialInput eventUnitSpecialInput = new EventUnitSpecialInput();
                    eventUnitSpecialInput.setEventId(eventCtSpecialInput.getEventId());
                    eventUnitSpecialInput.setEventName(eventCtSpecialInput.getEventName());
                    eventUnitSpecialInput.setUnitId(unitId);
                    eventUnitSpecialInput.setUnitName(unitName);
                    eventUnitSpecialInput.setIsRequired(ctEventMaintain017Request.getIsRequired());
                    eventUnitSpecialInput.setSInputId(eventCtSpecialInput.getSInputId());
                    eventUnitSpecialInput.setSInputName(eventCtSpecialInput.getSInputName());
                    eventUnitSpecialInput.setSInputValue(null);
                    eventUnitSpecialInput.setSInputCategory(eventCtSpecialInput.getSInputCategory());
                    eventUnitSpecialInput.setSInputOrderNum(eventCtSpecialInput.getSInputOrderNum());
                    eventUnitSpecialInput.setSInputWriteType(eventCtSpecialInput.getSInputWriteType());
                    eventUnitSpecialInput.setSInputExample(eventCtSpecialInput.getSInputExample());
                    eventUnitSpecialInput.setCreatorAndUpdater(session);

                    eventUnitSpecialInputList.add(eventUnitSpecialInput);
                }

                if (eventUnitSpecialInputList.size() > 0) {
                    eventUnitSpecialInputRepo.saveAll(eventUnitSpecialInputList);
                }
            }
        }

        return new CtEventMaintain017Response(errCode, errMsg, ctEventMaintain017Request);
    }

    // 修改單筆特殊輸入項目 Step1
    public BaseResponse editSpecialInput(HttpSession session, String eventId, String sInputId,
                                         CtEventMaintain017Request ctEventMaintain017Request) {

        int errCode = 200;
        String errMsg = "success";

        List<EventCtSpecialInput> eventCtSpecialInputs =
                eventCtSpecialInputRepo.GetEventCtSpecialInputByEventIdAndsInputId(eventId, sInputId);

        if (eventCtSpecialInputs.size() <= 0) {

            errCode = 400;
            errMsg = "找不到classEventCtSpecialInput eventId : " + eventId + " 、 sInputId : " + sInputId;
        } else {
            EventCtSpecialInput eventCtSpecialInput = eventCtSpecialInputs.get(0);

            if (ctEventMaintain017Request.getsInputOrderNum() == null ||
                    eventCtSpecialInput.getSInputOrderNum() == ctEventMaintain017Request.getsInputOrderNum()) {

                // save
                saveEdit(session, eventCtSpecialInput, ctEventMaintain017Request);

            } else {

                // check sInputOrderNum
                if (!checkSInputOrderNum(eventId, eventCtSpecialInput, ctEventMaintain017Request.getsInputOrderNum())) {

                    errCode = 400;
                    errMsg = "排序碼{" + ctEventMaintain017Request.getsInputOrderNum() + "}已經存在，請重新選擇排序碼再存檔";
                } else {

                    saveEdit(session, eventCtSpecialInput, ctEventMaintain017Request);
                }
            }
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 修改單筆特殊輸入項目 Step2
    public void saveEdit(HttpSession session, EventCtSpecialInput eventCtSpecialInput,
                         CtEventMaintain017Request ctEventMaintain017Request) {

        eventCtSpecialInput.setSInputName(ctEventMaintain017Request.getsInputName());
        eventCtSpecialInput.setSInputExample(ctEventMaintain017Request.getsInputExample());
        eventCtSpecialInput.setSInputCategory(ctEventMaintain017Request.getsInputCategory());
        eventCtSpecialInput.setSInputWriteType(ctEventMaintain017Request.getsInputWriteType());
        eventCtSpecialInput.setSInputOrderNum(ctEventMaintain017Request.getsInputOrderNum());
        eventCtSpecialInput.setIsRequired(ctEventMaintain017Request.getIsRequired());
        eventCtSpecialInput.setUpdater(session);

        eventCtSpecialInputRepo.save(eventCtSpecialInput);

        List<EventUnitSpecialInput> eventUnitSpecialInputList =
                eventUnitSpecialInputRepo.GetEventUnitSpecialInputListByEventIdAndsInputId(
                        eventCtSpecialInput.getEventId(), eventCtSpecialInput.getSInputId());

        for (EventUnitSpecialInput e : eventUnitSpecialInputList) {
            e.setSInputName(eventCtSpecialInput.getSInputName());
            e.setSInputExample(eventCtSpecialInput.getSInputExample());
            e.setSInputCategory(eventCtSpecialInput.getSInputCategory());
            e.setSInputWriteType(eventCtSpecialInput.getSInputWriteType());
            e.setSInputOrderNum(eventCtSpecialInput.getSInputOrderNum());
            e.setIsRequired(eventCtSpecialInput.getIsRequired());
            e.setUpdater(session);
        }

        eventUnitSpecialInputRepo.saveAll(eventUnitSpecialInputList);
    }

    // 刪除單筆特殊輸入項目
    public BaseResponse deleteSpecialInput(HttpSession session, String eventId, String sInputId) {

        eventCtSpecialInputRepo.DeleteEventCtSpecialInputByEventIdAndsInputId(eventId, sInputId);

        eventUnitSpecialInputRepo.DeleteEventUnitSpecialInputListByEventIdAndsInputId(eventId, sInputId);

        return new BaseResponse(200, "success");
    }

    // 排序碼
    // check pgmUiOrderNum
    public boolean checkPgmUiOrderNum(String eventId, EventPgmOca eventPgmOca, Integer pgmUiOrderNum) {

        boolean save = false;
        int count = 0;

        if (eventPgmOca.getPgmUiOrderNum() == null && pgmUiOrderNum == null) {

            save = true;
        }

        if (eventPgmOca.getPgmUiOrderNum() == null && pgmUiOrderNum != null) {

            count = eventPgmOcaRepo.GetEventPgmOcaCountByEventIdAndPgmUiOrderNum(eventId, pgmUiOrderNum);

            if (count == 0) {
                save = true;
            } else {
                save = false;
            }
        }

        if (eventPgmOca.getPgmUiOrderNum() != null && pgmUiOrderNum == null) {

            save = true;
        }

        if (eventPgmOca.getPgmUiOrderNum() != null && pgmUiOrderNum != null) {

            if (eventPgmOca.getPgmUiOrderNum().equals(pgmUiOrderNum)) {

                save = true;
            } else {
                if (pgmUiOrderNum == null) {

                    save = true;
                } else {

                    count = eventPgmOcaRepo.GetEventPgmOcaCountByEventIdAndPgmUiOrderNum(eventId, pgmUiOrderNum);

                    if (count == 0) {
                        save = true;
                    } else {
                        save = false;
                    }
                }
            }
        }

        return save;
    }

    // 中台項目排序碼
    // check pgmCtOrderNum
    public boolean checkPgmCtOrderNum(String eventId, EventPgmOca eventPgmOca, Integer pgmCtOrderNum) {

        boolean save = false;
        int count = 0;

        if (eventPgmOca.getPgmCtOrderNum() == null && pgmCtOrderNum == null) {

            save = true;
        }

        if (eventPgmOca.getPgmCtOrderNum() == null && pgmCtOrderNum != null) {

            count = eventPgmOcaRepo.GetEventPgmOcaCountByEventIdAndPgmCtOrderNum(eventId, pgmCtOrderNum);

            if (count == 0) {
                save = true;
            } else {
                save = false;
            }
        }

        if (eventPgmOca.getPgmCtOrderNum() != null && pgmCtOrderNum == null) {

            save = true;
        }

        if (eventPgmOca.getPgmCtOrderNum() != null && pgmCtOrderNum != null) {

            if (eventPgmOca.getPgmCtOrderNum().equals(pgmCtOrderNum)) {

                save = true;
            } else {

                count = eventPgmOcaRepo.GetEventPgmOcaCountByEventIdAndPgmCtOrderNum(eventId, pgmCtOrderNum);

                if (count == 0) {
                    save = true;
                } else {
                    save = false;
                }
            }
        }

        return save;
    }

    // check sInputOrderNum
    public boolean checkSInputOrderNum(String eventId, EventCtSpecialInput eventCtSpecialInput, Integer sInputOrderNum) {

        boolean save = false;
        int count = 0;

        if (eventCtSpecialInput.getSInputOrderNum() == null && sInputOrderNum == null) {
            save = true;
        }

        if (eventCtSpecialInput.getSInputOrderNum() == null && sInputOrderNum != null) {

            count = eventCtSpecialInputRepo.GetEventCtSpecialInputCountByEventIdAndsInputOrderNum(
                    eventId, sInputOrderNum);

            if (count == 0) {
                save = true;
            } else {
                save = false;
            }
        }

        if (eventCtSpecialInput.getSInputOrderNum() != null && sInputOrderNum == null) {
            save = true;
        }

        if (eventCtSpecialInput.getSInputOrderNum() != null && sInputOrderNum != null) {


            if (eventCtSpecialInput.getSInputOrderNum().equals(sInputOrderNum)) {
                save = true;
            } else {

                count = eventCtSpecialInputRepo.GetEventCtSpecialInputCountByEventIdAndsInputOrderNum(
                        eventId, sInputOrderNum);

                if (count == 0) {
                    save = true;
                } else {
                    save = false;
                }
            }
        }

        return save;
    }

    // Compare two arrayList differences
    public List<String> compareTwoArrayListDiff(List<String> list1, List<String> list2) {

        long st = System.nanoTime();

        List<String> diff = new ArrayList<>();
        List<String> maxList = list1;
        List<String> minList = list2;

        if (list2.size() > list1.size()) {

            maxList = list2;
            minList = list1;
        }

        Map<String, Integer> map = new HashMap<>(maxList.size());

        for (String string : maxList) {

            map.put(string, 1);
        }

        for (String string : minList) {

            if (map.get(string) != null) {

                map.put(string, 2);
                continue;
            }

            diff.add(string);
        }

        for (Map.Entry<String, Integer> entry : map.entrySet()) {

            if (entry.getValue() == 1) {

                diff.add(entry.getKey());
            }
        }

//        logger.debug("Total times : " + (System.nanoTime() - st));
//        print2JSON("diff", diff);
        return diff;
    }

    // generate EventMain eventId
    public String eventMainEventIdGenerator(String eventCategoryId) {

        String criteria = eventCategoryId + CommonUtils.GetMinGoDateStr();

        String currentMaxValue = eventMainRepo.GetMaxEventId(criteria + '%');

        String eventId = "";

        long tempId = 0;

        if (currentMaxValue == null) {

            eventId = criteria + "0001";
        } else {

            DecimalFormat df = new DecimalFormat("0000");

            currentMaxValue = currentMaxValue.substring(13);
            tempId = Long.parseLong(currentMaxValue);
            tempId++;

            eventId = criteria + df.format(tempId);
        }

        return eventId;
    }

    // generate EventCtSpecialInput sInputId
    public List<String> EventCtSpecialInputSInputIdGenerator(int setNum) {

        String criteria = "SID" + CommonUtils.GetMinGoYearStr();

        String currentMaxValue = eventCtSpecialInputRepo.GetMaxSInputId(criteria + '%');

        long tempId = 0;

        List<String> result = new ArrayList<>();

        if (currentMaxValue != null) {
            tempId = Long.parseLong(currentMaxValue.substring(6));
        }


        logger.debug("" + tempId);

        tempId++;
        logger.debug("" + tempId);
        for (int i = 0; i < setNum; i++) {
            String id = criteria + String.format("%07d", tempId++);
            result.add(id);
            logger.debug(id);
        }

        return result;
    }

    // generate EventPgmOca pgmUniqueId
    // unit = "C" or "U"
    // setNum = 要產生幾組pgmUniqueId
    public List<String> EventPgmOcaPgmUniqueIdListGenerate(String department, int setNum) {

        String criteria = "PUD" + department + CommonUtils.GetMinGoYearStr();

        String currentMaxValue = eventPgmOcaRepo.GetMaxPgmUniqueId(criteria + '%');

        String pgmUniqueId = "";

        long tempId = 0;

        DecimalFormat df = new DecimalFormat("0000000");

        if (currentMaxValue == null) {

            pgmUniqueId = criteria + "0000001";
        } else {

            currentMaxValue = currentMaxValue.substring(7);
            tempId = Long.parseLong(currentMaxValue);
            tempId++;

            pgmUniqueId = criteria + df.format(tempId);
        }

        currentMaxValue = pgmUniqueId.substring(7);
        tempId = Long.parseLong(currentMaxValue);

        List<String> pgmUniqueIdList = new ArrayList<>();

        for (int s = 0; s < setNum; s++) {

            if (s == 0) {

                pgmUniqueIdList.add(pgmUniqueId);
            } else {

                tempId++;
                pgmUniqueId = criteria + df.format(tempId);
                pgmUniqueIdList.add(pgmUniqueId);
            }
        }

        return pgmUniqueIdList;
    }
}
