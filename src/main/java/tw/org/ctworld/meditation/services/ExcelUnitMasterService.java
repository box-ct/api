package tw.org.ctworld.meditation.services;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tw.org.ctworld.meditation.beans.DataImport.ExcelMasterInfo;
import tw.org.ctworld.meditation.beans.MemberImport.MemberTemplateItem;
import tw.org.ctworld.meditation.beans.UnitMaster.MasterImportResultObj;
import tw.org.ctworld.meditation.beans.UnitMaster.MasterInfo;
import tw.org.ctworld.meditation.models.CtMasterInfo;
import tw.org.ctworld.meditation.models.UnitMasterInfo;
import tw.org.ctworld.meditation.repositories.UnitMasterInfoRepo;

import javax.servlet.ServletContext;
import java.io.InputStream;
import java.util.*;

@Service
public class ExcelUnitMasterService {
    @Autowired
    private UnitMasterInfoRepo unitMasterInfoRepo;

    @Autowired
    private MemberImportService memberImportService;



    private ServletContext context;

    @Autowired
    public ExcelUnitMasterService(ServletContext context) {
        this.context = context;
    }

    private static final Logger logger = LoggerFactory.getLogger(ExcelProcessService.class);
    // process class excel file imported from users
    public MasterImportResultObj processUnitMasterImport(String unitid, MultipartFile file) {

        List<ExcelMasterInfo> inpfile = readExcelUnitMaster(file);

        List<MasterInfo> result=new ArrayList<>();

        List<UnitMasterInfo> unitMasters=unitMasterInfoRepo.GetUnitMasterByUnitId(unitid); //目前單位法師

        List<String> failed=new ArrayList<>();
        //比對ctmasterinfo 總表
        for (ExcelMasterInfo ele:inpfile){
            if (!ele.getMasterName().equals("") || !ele.getMasterId().equals("")){
                List<CtMasterInfo> ctMasterInfos=unitMasterInfoRepo.GetCtMasterByCond(ele.getMasterId(),ele.getMasterName(),unitid);
                if (ctMasterInfos!=null){
                    CtMasterInfo ctMasterInfo=ctMasterInfos.get(0);
                    boolean chkadd=true;
                    //比對目前單位法師
//                    for (int i=0;i<unitMasters.size();++i){
//                        if (unitMasters.get(i).getMasterId().equals(ele.getMasterId())){
//                            chkadd=false;
//                            break;
//                        }
//                    }
                    if (chkadd){
                        MasterInfo masterInfo=new MasterInfo(ctMasterInfo.getMasterId(),ctMasterInfo.getMasterName(),ctMasterInfo.getMasterPreceptTypeName(),ctMasterInfo.getIsAbbot(),
                                ctMasterInfo.getIsTreasurer(),ctMasterInfo.getJobTitle(),ctMasterInfo.getMobileNum(),ctMasterInfo.getPreceptOrder(),ctMasterInfo.getJobOrder());
                        result.add(masterInfo);
                    }
                }
            }
        }

        MasterImportResultObj masterImportResultObj=new MasterImportResultObj(result,failed);
        return masterImportResultObj;
    }

    // read excel
    private List<ExcelMasterInfo> readExcelUnitMaster(MultipartFile file) {
        List<ExcelMasterInfo> masterInfos=new ArrayList<>();
        try {
            InputStream stream = file.getInputStream();

            XSSFWorkbook workbook = new XSSFWorkbook(stream);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();

            // rows
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();

                // Skip 1 rows
                if (row.getRowNum() < 1) {
                    continue;
                }

                ExcelMasterInfo enroll = new ExcelMasterInfo();

                // columns
                for (int i = 0; i < 2; i++) {

                    if (row.getCell(i) != null) {
                        row.getCell(i).setCellType(CellType.STRING);
                        String temp = row.getCell(i).toString();
//                        logger.debug(temp);

                        switch (i) {
                            case 0:
                                enroll.setMasterId(temp);
                                break;
                            case 1:
                                enroll.setMasterName(temp);
                                break;

                        }
                    }
                }

                // all fields, stop reading
                if (enroll.getMasterId() == null && enroll.getMasterName() == null) {
                    break;
                }

                masterInfos.add(enroll);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return masterInfos;
    }


//    public List<MemberImportResult> processCheckMemberImport(MultipartFile file){
//        List<MemberTemplateItem> inpfile = readExcelMemberImport(file);
//        List<MemberImportResult> results=memberImportService.CheckMemberImport(inpfile);
//        return results;
//    }


    //學員資料-01-精舍學員資料上傳
    public List<MemberTemplateItem> readExcelMemberImport(MultipartFile file) {

        List<MemberTemplateItem> memberListItems = new ArrayList<>();

        List<String> colids = memberImportService.fieldIds;

        try {
            InputStream stream = file.getInputStream();

            XSSFWorkbook workbook = new XSSFWorkbook(stream);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();

            // rows
            int idx=0;
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();

                // Skip 1 rows
                if (row.getRowNum() < 1) {
                    continue;
                }

                memberListItems.add( MemberTemplateItemReadRow( row,  colids,idx));
                ++idx;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return  memberListItems;

    }

    private  MemberTemplateItem MemberTemplateItemReadRow(Row row, List<String> colids,int idx){
        List<String> impRow=new ArrayList<>(colids.size());
        // columns
        for (int i = 0; i < colids.size(); i++) {
            String temp="";
            try {
                if (row.getCell(i) != null) {
                    row.getCell(i).setCellType(CellType.STRING);
                    temp = row.getCell(i).toString();
                }
            }
            catch (Exception ex){

            }
            impRow.add(temp);
        }
        MemberTemplateItem memberListItem = new MemberTemplateItem(
                impRow.get(0),impRow.get(1),impRow.get(2),impRow.get(3),impRow.get(4),impRow.get(5),impRow.get(6),impRow.get(7),impRow.get(8),impRow.get(9),
                impRow.get(10),impRow.get(11),impRow.get(12),impRow.get(13),impRow.get(14),impRow.get(15),impRow.get(16),impRow.get(17),impRow.get(18),impRow.get(19),
                impRow.get(20),impRow.get(21),impRow.get(22),impRow.get(23),impRow.get(24),impRow.get(25),impRow.get(26),impRow.get(27),impRow.get(28),impRow.get(29),
                impRow.get(30),impRow.get(31),impRow.get(32),impRow.get(33),impRow.get(34),impRow.get(35),impRow.get(36),impRow.get(37),impRow.get(38),impRow.get(39),
                impRow.get(40),impRow.get(41),impRow.get(42),impRow.get(43),impRow.get(44),impRow.get(45),impRow.get(46),impRow.get(47),impRow.get(48),impRow.get(49),
                impRow.get(50),impRow.get(51),impRow.get(52),impRow.get(53),impRow.get(54));
        memberListItem.setIdx(idx);
        return memberListItem;
    }

    //學員資料-01-精舍學員資料上傳-- for 舊版EXCEL
    public List<MemberTemplateItem> readExcelMemberImport_XLS(MultipartFile file) {
        List<MemberTemplateItem> memberListItems=new ArrayList<>();
        List<String> colids=memberImportService.fieldIds;
        try {
            POIFSFileSystem pfs = new POIFSFileSystem(file.getInputStream());

            Workbook workbook = new HSSFWorkbook(pfs);

            Sheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();

            // rows
            int idx=0;
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();

                // Skip 1 rows
                if (row.getRowNum() < 1) {
                    continue;
                }

                memberListItems.add( MemberTemplateItemReadRow( row,  colids,idx));
                ++idx;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return  memberListItems;

    }

}
