package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.EventEnrollMember.*;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.libs.RedundantException;
import tw.org.ctworld.meditation.libs.UnacceptableException;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.models.EventMain;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EventUnitTemplateService {

    private static final Logger logger = LoggerFactory.getLogger(EventUnitTemplateService.class);

    @Autowired
    private EventMainRepo eventMainRepo;

    @Autowired
    private EventUnitTemplateRepo eventUnitTemplateRepo;

    @Autowired
    private EventUnitTemplatePgmRepo eventUnitTemplatePgmRepo;

    @Autowired
    private EventPgmUnitRepo eventPgmUnitRepo;

    @Autowired
    private EventEnrollMainRepo eventEnrollMainRepo;

    @Autowired
    private EventEnrollPgmRepo eventEnrollPgmRepo;


    public List<TemplateShort> getTemplatesByEventIdUnitId(String eventId, String unitId, boolean isMaster) {
        return eventUnitTemplateRepo.GetByEventIdUnitId(eventId, unitId, isMaster);
    }

    public void newTemplate(HttpSession session, EventEnrollRequest request) throws UnacceptableException, RedundantException{

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
        String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());

        // check
        if (request.getTemplateName() == null || request.getTemplateName().isEmpty()) {
            throw new UnacceptableException();
        }

        int sameNameCount = eventUnitTemplateRepo.CheckTemplateName(request.getEventId(), unitId, request.getTemplateName());

        if (sameNameCount > 0) {
            throw new RedundantException();
        }

        // new template
        EventUnitTemplate template = new EventUnitTemplate();
        template.copyRequest(request);
        template.setUnitId(unitId);
        template.setUnitName(unitName);
        template.setCreatorAndUpdater(session);
        template.setTemplateUniqueId(genTemplateUniqueId());

        eventUnitTemplateRepo.save(template);

        // add template pgm(s)
        List<EventPgmShort> pgms = request.getGenPgmList();
        pgms.addAll(request.getPgmList());

        // get all pgmUniqueIds
        List<String> pgmUniqueIds = pgms.stream().map(p -> p.getPgmUniqueId()).collect(Collectors.toList());

        // get all pgmIds of all event_pgm_unit where unitId matched and in pgmUniqueIds
        List<EventPgmUnit> eventPgmUnits = null;

        if (pgmUniqueIds.size() > 0) {
            eventPgmUnits = eventPgmUnitRepo.GetByUnitIdInPgmUniqueIds(unitId, pgmUniqueIds);
        } else {
            eventPgmUnits = new ArrayList<>();
        }

        // create event_enroll_pgm
        List<EventUnitTemplatePgm> templatePgmList = new ArrayList<>();

        for (EventPgmShort item : pgms) {
            logger.debug(item.getPgmUniqueId() + "/" + item.getGenPgmUserInputValue());

            Optional<EventPgmUnit> eventPgmUnit = eventPgmUnits.stream().filter(e -> e.getPgmUniqueId().equals(item.getPgmUniqueId())).findAny();

            if (eventPgmUnit.isPresent()) {
                EventUnitTemplatePgm templatePgm = createEventUnitTemplatePgm(template, eventPgmUnit.get(), CommonUtils.Empty2Null(item.getGenPgmUserInputValue()), session);
                templatePgmList.add(templatePgm);
            } else {
                logger.error("pgm unit not found! pgmUniqueId: " + item.getPgmUniqueId());
            }
        }

        // need dorm
        if (template.getIsCtDorm()) {
            EventPgmUnit eventPgmUnitDorm = eventPgmUnitRepo.GetByUnitIdEventIdDorm(unitId, template.getEventId());

            if (eventPgmUnitDorm != null) {
                EventUnitTemplatePgm templatePgm = createEventUnitTemplatePgm(template, eventPgmUnitDorm, null, session);
                templatePgm.setPgmDate(template.getCtDormStartDate());
                templatePgm.setCtDormStartDate(template.getCtDormStartDate());
                templatePgm.setCtDormEndDate(template.getCtDormEndDate());
                templatePgmList.add(templatePgm);
            } else {
                logger.error("pgm unit not found! dorm unitId:" + unitId + " eventId: " + template.getEventId());
            }
        }

        // need trans
        if (!template.getIsGoCtBySelf()) {
            EventPgmUnit eventPgmUnitTrans = eventPgmUnitRepo.GetByUnitIdEventIdTrans(unitId, template.getEventId());

            if (eventPgmUnitTrans != null) {
                EventUnitTemplatePgm templatePgm = createEventUnitTemplatePgm(template, eventPgmUnitTrans, null, session);
                templatePgm.setGoTransDate(CommonUtils.ParseDate(request.getGoTransDate()));
                templatePgm.setGoTransType(CommonUtils.Empty2Null(request.getGoTransType()));
                templatePgm.setReturnTransDate(CommonUtils.ParseDate(request.getReturnTransDate()));
                templatePgm.setReturnTransType(CommonUtils.Empty2Null(request.getReturnTransType()));
                templatePgm.setSameGoTransMemberName(CommonUtils.Empty2Null(request.getSameGoTransMemberName()));
                templatePgm.setSameReturnTransMemberName(CommonUtils.Empty2Null(request.getSameGoTransMemberName()));
                templatePgmList.add(templatePgm);
            } else {
                logger.error("pgm unit not found! trans unitId:" + unitId + " eventId: " + template.getEventId());
            }
        }

        if (templatePgmList.size() > 0) {
            eventUnitTemplatePgmRepo.saveAll(templatePgmList);
        }
    }

    // generate TemplateUniqueId
    private String genTemplateUniqueId() {
        String prefix = "TUD" + CommonUtils.GetMinGoYearStr();

        String maxTemplateUniqueId = eventUnitTemplateRepo.GetMaxTemplateUniqueId(prefix);

        int max = 0;

        if (maxTemplateUniqueId != null) {
            max = Integer.parseInt(maxTemplateUniqueId.substring(maxTemplateUniqueId.length() - 7, maxTemplateUniqueId.length()));
        }

        max++;

        return prefix + String.format("%07d", max);
    }

    // create new event enroll pgm (not saved)
    private EventUnitTemplatePgm createEventUnitTemplatePgm(EventUnitTemplate template, EventPgmUnit eventPgmUnit, String genPgmUserInputValue, HttpSession session) {

        EventUnitTemplatePgm pgm = new EventUnitTemplatePgm();
        pgm.setUnitId(template.getUnitId());
        pgm.setUnitName(template.getUnitName());
        pgm.setEventId(template.getEventId());
        pgm.setEventName(template.getEventName());
        pgm.setEventCreatorUnitId(template.getEventCreatorUnitId());
        pgm.setEventCreatorUnitName(template.getEventCreatorUnitName());
        pgm.setSponsorUnitId(template.getSponsorUnitId());
        pgm.setSponsorUnitName(template.getSponsorUnitName());
//        pgm.setEnrollUserId(userId);
//        pgm.setEnrollUserName(userName);
//        pgm.setFormAutoAddNum(template.getFormAutoAddNum());
        pgm.setTemplateUniqueId(template.getTemplateUniqueId());
        pgm.setTemplateName(template.getTemplateName());
        pgm.setGenPgmUserInputValue(genPgmUserInputValue);

        pgm.setIsDisabledByOCA(eventPgmUnit.getIsDisabledByOCA() != null ? eventPgmUnit.getIsDisabledByOCA() : false);
        pgm.setIsRequired(eventPgmUnit.getIsRequired() != null ? eventPgmUnit.getIsRequired() : false);
        pgm.setIsGenPgm(eventPgmUnit.getIsGenPgm() != null ? eventPgmUnit.getIsGenPgm() : false);
        pgm.setPgmCategoryId(eventPgmUnit.getPgmCategoryId());
        pgm.setPgmCategoryName(eventPgmUnit.getPgmCategoryName());
        pgm.setPgmId(eventPgmUnit.getPgmId());
        pgm.setPgmName(eventPgmUnit.getPgmName());
        pgm.setPgmUniqueId(eventPgmUnit.getPgmUniqueId());
        pgm.setPgmCtOrderNum(eventPgmUnit.getPgmCtOrderNum());
        pgm.setPgmDate(eventPgmUnit.getPgmDate());
        pgm.setPgmDesc(eventPgmUnit.getPgmDesc());
        pgm.setPgmNote(eventPgmUnit.getPgmNote());
        pgm.setPgmStartTime(eventPgmUnit.getPgmStartTime());
        pgm.setPgmEndTime(eventPgmUnit.getPgmEndTime());
        pgm.setPgmUiOrderNum(eventPgmUnit.getPgmUiOrderNum());

        pgm.setCreatorAndUpdater(session);

        return pgm;
    }


    public EventUnitTemplatePgmResponse getTemplate(String eventId, String templateUniqueId) {
        EventUnitTemplatePgmResponse response = new EventUnitTemplatePgmResponse();
        response.setErrCode(200);
        response.setErrMsg("success");

        // get unit template
        List<EventUnitTemplate> eventUnitTemplates = eventUnitTemplateRepo.GetByEventIdTemplateUniqueId(eventId, templateUniqueId);

        if (eventUnitTemplates.size() > 0) {
            response.setResult(eventUnitTemplates.get(0));

            // get unit template pgm
            List<EventUnitTemplatePgm> unitTemplatePgmList = eventUnitTemplatePgmRepo.GetByEventIdTemplateUniqueId(eventId, templateUniqueId);

            List<EventPgmShort> genPgmList = new ArrayList<>();
            List<EventPgmShort> pgmList = new ArrayList<>();

            response.setGenPgmList(genPgmList);
            response.setPgmList(pgmList);

            for (EventUnitTemplatePgm pgm : unitTemplatePgmList) {
                EventPgmShort pgmShort = new EventPgmShort();
                pgmShort.setPgmId(pgm.getPgmId());
                pgmShort.setPgmUniqueId(pgm.getPgmUniqueId());
                pgmShort.setGenPgmUserInputValue(pgm.getGenPgmUserInputValue());

                if (pgm.getIsGenPgm()) {
                    genPgmList.add(pgmShort);
                } else {
                    pgmList.add(pgmShort);
                }
            }

        } else {
            response.setErrCode(400);
            response.setErrMsg("範本不存在");
        }

        return response;
    }

    public void updateTemplate(HttpSession session, EventEnrollRequest request) throws NotFoundException, RedundantException {

        // 1.update template
        List<EventUnitTemplate> templates = eventUnitTemplateRepo.GetByEventIdTemplateUniqueId(request.getEventId(), request.getTemplateUniqueId());

        if (templates.size() <= 0) {
            throw new NotFoundException();
        }

        EventUnitTemplate template = templates.get(0);

        logger.debug(request.getEventId() + "/" + request.getUnitId() + "/" + request.getTemplateName() + "/" + template.getId());
        int sameNameCount = eventUnitTemplateRepo.CheckTemplateNameExclude(request.getEventId(), request.getUnitId(), request.getTemplateName(), template.getId());
        logger.debug("count:" + sameNameCount);
        if (sameNameCount > 0) {
            throw new RedundantException();
        }

        template.copyRequest(request);
        template.setUpdater(session);

        eventUnitTemplateRepo.save(template);

        // 2. update pgms
        // exist pgm(s)
        List<EventUnitTemplatePgm> existPgmList = eventUnitTemplatePgmRepo.GetByEventIdTemplateUniqueId(request.getEventId(), request.getTemplateUniqueId());

        // request pgm(s)
        List<EventPgmShort> requestPgmList = request.getGenPgmList();
        requestPgmList.addAll(request.getPgmList());

        // get all pgmUniqueIds
        List<String> pgmUniqueIds = requestPgmList.stream().map(p -> p.getPgmUniqueId()).collect(Collectors.toList());

        // dorm
        if (request.getIsCtDorm()) {
            EventPgmUnit eventPgmUnitDorm = eventPgmUnitRepo.GetByUnitIdEventIdDorm(request.getUnitId(), request.getEventId());
            if (eventPgmUnitDorm != null) pgmUniqueIds.add(eventPgmUnitDorm.getPgmUniqueId());
        }
        //trans
        if (!request.getIsGoCtBySelf()) {
            EventPgmUnit eventPgmUnitTrans = eventPgmUnitRepo.GetByUnitIdEventIdTrans(request.getUnitId(), request.getEventId());
            if (eventPgmUnitTrans != null) pgmUniqueIds.add(eventPgmUnitTrans.getPgmUniqueId());
        }

        // 2.1 update exist pgm(s)
        Date dormStartDate = CommonUtils.ParseDate(request.getCtDormStartDate());
        Date dormEndDate = CommonUtils.ParseDate(request.getCtDormStartDate());
        Date goTransDate = CommonUtils.ParseDate(request.getGoTransDate());
        Date returnTransDate = CommonUtils.ParseDate(request.getReturnTransDate());

        List<EventUnitTemplatePgm> updPgmList = new ArrayList<>();
        List<EventUnitTemplatePgm> newPgmList = new ArrayList<>();
        List<EventUnitTemplatePgm> delPgmList;

        List<EventUnitTemplatePgm> updateCheckPgmList = existPgmList.stream().filter(e -> pgmUniqueIds.contains(e.getPgmUniqueId())).collect(Collectors.toList());

        for (EventUnitTemplatePgm item : updateCheckPgmList) {
            boolean change = false;
            if (item.getPgmUniqueId().equals("PGM10014")) {

                if (item.getPgmDate().compareTo(dormStartDate) != 0) {
                    item.setPgmDate(dormStartDate);
                    change = true;
                }
                if (item.getCtDormStartDate().compareTo(dormStartDate) != 0) {
                    item.setCtDormStartDate(dormStartDate);
                    change = true;
                }
                if (item.getCtDormEndDate().compareTo(dormEndDate) != 0) {
                    item.setCtDormEndDate(dormEndDate);
                    change = true;
                }
            } else if (item.getPgmUniqueId().equals("PGM10017")) {

                if (item.getGoTransDate().compareTo(goTransDate) != 0) {
                    item.setGoTransDate(goTransDate);
                    change = true;
                }

                if (!item.getGoTransType().equals(request.getGoTransType())) {
                    item.setGoTransType(request.getGoTransType());
                    change = true;
                }

                if (item.getReturnTransDate().compareTo(returnTransDate) != 0) {
                    item.setReturnTransDate(returnTransDate);
                    change = true;
                }

                if (!item.getReturnTransType().equals(request.getReturnTransType())) {
                    item.setReturnTransType(request.getReturnTransType());
                    change = true;
                }
            } else {
                if (item.getIsGenPgm()) {
                    Optional<EventUnitTemplatePgm> target = existPgmList.stream().filter(p -> p.getPgmUniqueId().equals(item.getPgmUniqueId())).findAny();

                    if (target.isPresent()) {
                        item.setGenPgmUserInputValue(target.get().getGenPgmUserInputValue());
                    }
                }
            }

            if (change) {
                item.setUpdater(session);
                updPgmList.add(item);
            }
        }

        // 2.2 add new pgm(s)
        List<String> existPgmUniqueIds = updateCheckPgmList.stream().map(i -> i.getPgmUniqueId()).collect(Collectors.toList());

        List<String> newPgmUniqueIds;

        if (existPgmUniqueIds.size() > 0) {
            newPgmUniqueIds = pgmUniqueIds.stream().filter(i -> !existPgmUniqueIds.contains(i)).collect(Collectors.toList());
        } else {
            newPgmUniqueIds = pgmUniqueIds;
        }

        // has new pgm
        if (newPgmUniqueIds.size() > 0) {
            // get all pgmIds of all event_pgm_unit where unitId matched and in pgmUniqueIds
            List<EventPgmUnit> eventPgmUnits = eventPgmUnitRepo.GetByUnitIdInPgmUniqueIds(request.getUnitId(), newPgmUniqueIds);

            // create event_enroll_pgm
            List<EventPgmShort> newPgms = requestPgmList.stream().filter(p -> newPgmUniqueIds.contains(p.getPgmUniqueId())).collect(Collectors.toList());

            for (EventPgmShort item : newPgms) {
                logger.debug(item.getPgmUniqueId() + "/" + item.getGenPgmUserInputValue());

                Optional<EventPgmUnit> eventPgmUnit = eventPgmUnits.stream().filter(e -> e.getPgmUniqueId().equals(item.getPgmUniqueId())).findAny();

                if (eventPgmUnit.isPresent()) {
                    EventUnitTemplatePgm eventUnitTemplatePgm = createEventUnitTemplatePgm(template, eventPgmUnit.get(), CommonUtils.Empty2Null(item.getGenPgmUserInputValue()), session);

                    if (eventPgmUnit.get().getPgmUniqueId().equals("PGM10014")) {
                        eventUnitTemplatePgm.setPgmDate(dormStartDate);
                        eventUnitTemplatePgm.setCtDormStartDate(dormStartDate);
                        eventUnitTemplatePgm.setCtDormEndDate(dormEndDate);
                    } else if (eventPgmUnit.get().getPgmUniqueId().equals("PGM10017")) {
                        eventUnitTemplatePgm.setGoTransDate(goTransDate);
                        eventUnitTemplatePgm.setGoTransType(CommonUtils.Empty2Null(request.getGoTransType()));
                        eventUnitTemplatePgm.setReturnTransDate(returnTransDate);
                        eventUnitTemplatePgm.setReturnTransType(CommonUtils.Empty2Null(request.getReturnTransType()));
                    }

                    newPgmList.add(eventUnitTemplatePgm);
                } else {
                    logger.error("pgm unit not found! pgmUniqueId: " + item.getPgmUniqueId());
                }
            }
        }

        // 3. delete enroll pgm(s)
        delPgmList = existPgmList.stream().filter(e -> !pgmUniqueIds.contains(e.getPgmUniqueId())).collect(Collectors.toList());

        if (updPgmList.size() > 0) {
            eventUnitTemplatePgmRepo.saveAll(updPgmList);
        }

        if (newPgmList.size() > 0) {
            eventUnitTemplatePgmRepo.saveAll(newPgmList);
        }

        if (delPgmList.size() > 0) {
            eventUnitTemplatePgmRepo.deleteAll(delPgmList);
        }
    }

    public int reApplyTemplate(HttpSession session, String eventId, String templateId) throws NotFoundException{

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) {
            throw new NotFoundException("活動不存在。");
        }

        List<EventUnitTemplate> templates = eventUnitTemplateRepo.GetByEventIdTemplateUniqueId(eventId, templateId);

        if (templates.size() <= 0) {
            throw new NotFoundException("範本不存在。");
        }

        // query data
        EventUnitTemplate template = templates.get(0);

        List<EventUnitTemplatePgm> pgmList = eventUnitTemplatePgmRepo.GetByEventIdTemplateUniqueId(eventId, templateId);
        List<String> pgmUniqueIdList = pgmList.stream().map(p -> p.getPgmUniqueId()).collect(Collectors.toList());

        List<EventEnrollMain> eventEnrollMainList = eventEnrollMainRepo.GetEnrolledByApplyTemplateId(eventId, templateId);

        List<String> formAutoAddNumList = eventEnrollMainList.stream().map(e -> e.getFormAutoAddNum()).collect(Collectors.toList());

        logger.debug("# enroll with template:" + formAutoAddNumList.size());

        if (formAutoAddNumList.size() > 0) {
            List<EventEnrollPgm> eventEnrollPgmList = eventEnrollPgmRepo.GetByEventIdFormAutoAddNum(eventId, formAutoAddNumList);

            List<EventEnrollPgm> updatePgmList = new ArrayList<>();
            List<EventEnrollPgm> newPgmList = new ArrayList<>();
            List<EventEnrollPgm> delPgmList;

            // update enroll main(s)
            for (EventEnrollMain eventEnrollMain : eventEnrollMainList) {
                // 1.apply template to event enroll main
                eventEnrollMain.applyTemplate(template);
                eventEnrollMain.setIsWaitForCtSync(true);
                eventEnrollMain.setUpdater(session);

                // 2. apply template pgm(s) to event enroll pgm
                List<EventEnrollPgm> existPgmList = eventEnrollPgmList.stream().filter(p -> p.getFormAutoAddNum().equals(eventEnrollMain.getFormAutoAddNum())).collect(Collectors.toList());

                // 2.1.update pgm(s)
                if (existPgmList != null && existPgmList.size() > 0) {
                    List<EventEnrollPgm> checkPgmList = existPgmList.stream().filter(p -> pgmUniqueIdList.contains(p.getPgmUniqueId())).collect(Collectors.toList());

                    for (EventEnrollPgm item : checkPgmList) {
                        Optional<EventUnitTemplatePgm> templatePgm = pgmList.stream().filter(p -> p.getPgmUniqueId().equals(item.getPgmUniqueId())).findAny();

                        if (templatePgm.isPresent()) {
                            item.applyTemplatePgm(template, templatePgm.get(), eventEnrollMain);
                            item.setUpdater(session);
                            updatePgmList.add(item);
                        } else {
                            logger.debug("template pgm not found!");
                        }
                    }
                }

                // 2.2.add new pgm(s) if any
                List<String> existPgmUniqueIds = existPgmList.stream().map(p -> p.getPgmUniqueId()).collect(Collectors.toList());
                List<EventUnitTemplatePgm> newTemplatePgmList;

                if (existPgmUniqueIds.size() > 0) {
                    newTemplatePgmList = pgmList.stream().filter(i -> !existPgmUniqueIds.contains(i)).collect(Collectors.toList());
                } else {
                    newTemplatePgmList = pgmList;
                }

                if (newTemplatePgmList.size() > 0) {
                    for (EventUnitTemplatePgm item : newTemplatePgmList) {
                        EventEnrollPgm pgm = new EventEnrollPgm();
                        pgm.applyTemplatePgm(template, item, eventEnrollMain);
                        pgm.setCreatorAndUpdater(session);

                        newPgmList.add(pgm);
                    }
                }
            }

            eventEnrollMainRepo.saveAll(eventEnrollMainList);

            if (updatePgmList.size() > 0) {
                eventEnrollPgmRepo.saveAll(updatePgmList);
            }

            if (newPgmList.size() > 0) {
                eventEnrollPgmRepo.saveAll(newPgmList);
            }

            // 2.3.delete pgm(s) if any
            delPgmList = eventEnrollPgmList.stream().filter(p -> !pgmUniqueIdList.contains(p.getPgmUniqueId())).collect(Collectors.toList());

            if (delPgmList.size() > 0) {
                eventEnrollPgmRepo.deleteAll(delPgmList);
            }
        }

        return formAutoAddNumList.size();
    }

}
