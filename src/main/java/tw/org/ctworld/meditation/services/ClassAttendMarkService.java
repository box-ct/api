package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord007Object;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord008Request;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.models.ClassAttendMark;
import tw.org.ctworld.meditation.repositories.ClassAttendMarkRepo;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class ClassAttendMarkService {

    private static final Logger logger = LoggerFactory.getLogger(ClassAttendMarkService.class);

    @Autowired
    private ClassAttendMarkRepo classAttendMarkRepo;


    public void addClassAttendMark(String classId, int classTypeNum, String classTypeName, String classPeriodNum,
                                   String className, HttpSession session) {

        String[] attendMarkList = new String[]{"V", "O", "A", "X", "D", "N", "W", "F", "M", "L", "E", "S1", "S2", "S3"};

        for (int x = 0 ; x < attendMarkList.length ; x++) {

            boolean isFullAttended = false, isGraduated = false, isDiligentAward = false, isMakeupClass = false;

            ClassAttendMark classAttendMark = new ClassAttendMark();
            classAttendMark.setUnitId((String) session.getAttribute(UserSession.Keys.UnitId.getValue()));
            classAttendMark.setUnitName((String) session.getAttribute(UserSession.Keys.UnitName.getValue()));
            classAttendMark.setClassId(classId);
            classAttendMark.setClassTypeNum(classTypeNum);
            classAttendMark.setClassTypeName(classTypeName);
            classAttendMark.setClassPeriodNum(classPeriodNum);
            classAttendMark.setClassName(className);
            classAttendMark.setAttendMark(attendMarkList[x]);

            switch (attendMarkList[x]) {
                case "V":
                    isFullAttended = true; isGraduated = true; isDiligentAward = true; isMakeupClass = false;
                    break;
                case "O":
                    isFullAttended = false; isGraduated = false; isDiligentAward = false; isMakeupClass = false;
                    break;
                case "A":
                    isFullAttended = false; isGraduated = false; isDiligentAward = false; isMakeupClass = false;
                    break;
                case "X":
                    isFullAttended = false; isGraduated = false; isDiligentAward = false; isMakeupClass = false;
                    break;
                case "D":
                    isFullAttended = true; isGraduated = true; isDiligentAward = true; isMakeupClass = true;
                    break;
                case "N":
                    isFullAttended = true; isGraduated = true; isDiligentAward = true; isMakeupClass = true;
                    break;
                case "W":
                    isFullAttended = true; isGraduated = true; isDiligentAward = true; isMakeupClass = false;
                    break;
                case "F":
                    isFullAttended = true; isGraduated = true; isDiligentAward = true; isMakeupClass = false;
                    break;
                case "M":
                    isFullAttended = false; isGraduated = false; isDiligentAward = true; isMakeupClass = true;
                    break;
                case "L":
                    isFullAttended = false; isGraduated = true; isDiligentAward = true; isMakeupClass = false;
                    break;
                case "E":
                    isFullAttended = false; isGraduated = true; isDiligentAward = true; isMakeupClass = false;
                    break;
                case "S1":
                    isFullAttended = false; isGraduated = false; isDiligentAward = false; isMakeupClass = false;
                    break;
                case "S2":
                    isFullAttended = false; isGraduated = false; isDiligentAward = false; isMakeupClass = false;
                    break;
                case "S3":
                    isFullAttended = false; isGraduated = false; isDiligentAward = false; isMakeupClass = false;
                    break;
            }
            classAttendMark.setIsFullAttended(isFullAttended);
            classAttendMark.setIsGraduated(isGraduated);
            classAttendMark.setIsDiligentAward(isDiligentAward);
            classAttendMark.setIsMakeupClass(isMakeupClass);
            classAttendMark.setCreatorAndUpdater(session);

            classAttendMarkRepo.save(classAttendMark);
        }
    }

    // 取得上課記錄標記
    public List<ClassRecord007Object> getClassAttendMark(HttpSession session, String classId) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<ClassRecord007Object> classRecord007ObjectList = classAttendMarkRepo.GetClassAttendMarkList(classId, unitId);

        return classRecord007ObjectList;
    }

    // 更新上課標記
    public String editClassAttendMark(HttpSession session, String classId, ClassRecord008Request classRecord008Request) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        if (classRecord008Request.getMarkList().size() != 0) {

            for (ClassRecord008Request.ClassRecord006Object classRecord006Object : classRecord008Request.getMarkList()) {

                ClassAttendMark classAttendMark = classAttendMarkRepo.GetClassAttendMark(classId, classRecord006Object.getAttendMark(), unitId);

                classAttendMark.setIsFullAttended(classRecord006Object.getIsFullAttended());
                classAttendMark.setIsGraduated(classRecord006Object.getIsGraduated());
                classAttendMark.setIsDiligentAward(classRecord006Object.getIsDiligentAward());
                classAttendMark.setIsMakeupClass(classRecord006Object.getIsMakeupClass());
                classAttendMark.setUpdater(session);

                classAttendMarkRepo.save(classAttendMark);
            }
        }

        return "success";
    }


}
