package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tw.org.ctworld.meditation.beans.Public.TrainingFile;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.libs.NotFoundException;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
public class FileStorageService {
    private ServletContext context;

    private static final Logger logger = LoggerFactory.getLogger(FileStorageService.class);


    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    public FileStorageService(ServletContext context) {
        this.context = context;
    }

    @Autowired
    private SysCodeService sysCodeService;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;


    // load file from resource path
    public Resource loadFileAsResource(String filename) throws NotFoundException {
        try {
            File inFile = new ClassPathResource(filename).getFile();
            Resource resource = new UrlResource(inFile.toURI());
            if(resource.exists()) {
                return resource;
            } else {
                throw new NotFoundException("File not found " + filename);
            }
        } catch (Exception ex) {
            throw new NotFoundException("File not found " + filename);
        }
    }

    // delete temp files, for user download zip images
    public void cleanTempFiles(boolean checkTime) {
        logger.debug("clean temp files");

        String absolutePath = context.getRealPath("temp");

        File index = new File(absolutePath);
        String[]entries = index.list();

        Date yesterday = CommonUtils.GetDateFromToday(-1);

        if (entries != null) {

            for(String s: entries){
                // skip readme.txt
                if (s.length() > 10) {

                    boolean delete = true;

                    // existing more than 1 day
                    if (checkTime) {
                        logger.debug("check file " + s);
                        String time = s.substring(0, s.length() - 5);
                        logger.debug("time " + time);

                        if (yesterday.getTime() < Long.parseLong(time)) {
                            delete = false;
                        }
                    }

                    if (delete) {
                        logger.debug("delete file " + s);
                        File currentFile = new File(index.getPath(), s);
                        currentFile.delete();
                    }
                }
            }
        }
    }


    public File loadFile(String filename){
        try {
            return resourceLoader.getResource("file:" + sysCodeService.getMemberImagePath() + "/" + filename).getFile();
        } catch (IOException e) {
            return null;
        }
    }

    // load file from absolute path
    public Resource loadFile2Resource(String path) throws NotFoundException {
        try {
            File inFile = resourceLoader.getResource("file:" + path).getFile();
            Resource resource = new UrlResource(inFile.toURI());
            if(resource.exists()) {
                return resource;
            } else {
                throw new NotFoundException("File not found " + path);
            }
        } catch (Exception ex) {
            throw new NotFoundException("File not found " + path);
        }
    }


    public Resource loadFile(int fileType, String filename) throws NotFoundException{
        String path = "";

        switch (fileType) {
            case 0:
                path = sysCodeService.getMemberImagePath();
                break;
            case 1:
                path = sysCodeService.getMakeupFilePath();
                break;
        }

        try {
            File inFile = resourceLoader.getResource("file:" + path + "/" + filename).getFile();
            Resource resource = new UrlResource(inFile.toURI());
            if(resource.exists()) {
                return resource;
            } else {
                throw new NotFoundException("File not found " + filename);
            }
        } catch (Exception ex) {
            throw new NotFoundException("File not found " + filename);
        }
    }

    public void saveVideo(MultipartFile uploadFile, String filename) throws Exception{
        String filePath = Paths.get(sysCodeService.getOriginalMakeupFilePath() + "/", filename).toString();

        saveFile(uploadFile, filePath);
    }

    public void saveImage(MultipartFile uploadFile, String filename, String memberId) throws Exception{
        String filePath = Paths.get(sysCodeService.getMemberImagePath() + "/", filename).toString();

        boolean exist = true;
        String backupPath = filePath;
        int count = 1;
        String firstExistPath = null;
        String existPath = null;

        while (exist) {
            if (firstExistPath == null && existPath != null) {
                firstExistPath = existPath;
            }

            existPath = imageExist(backupPath);

            if (existPath != null) {
                backupPath = filePath.substring(0, filePath.lastIndexOf(".")) +
                        "_" + String.format("%03d", count) +
                        (firstExistPath == null ? existPath.substring(existPath.lastIndexOf(".")) : firstExistPath.substring(firstExistPath.lastIndexOf(".")));

                count ++;
            } else {
                exist = false;
            }

//            logger.debug("backupPath: " + backupPath);
        }

//        logger.debug("firstExistPath: " + firstExistPath);

        if (firstExistPath != null && !filePath.equals(backupPath)) {
//            logger.debug("move file from: " + firstExistPath + " to " + backupPath);
            Path source = Paths.get(firstExistPath);
            Path dist = Paths.get(backupPath);
            Files.move(source, dist, REPLACE_EXISTING);
        }

        saveFile(uploadFile, filePath);

        ctMemberInfoService.updateIsDataComplete(memberId);
    }

    public void removeImage(String filename, String memberId)  throws Exception{
        String filePath = Paths.get(sysCodeService.getMemberImagePath() + "/", filename).toString();

        boolean exist = true;
        String backupPath = filePath;
        int count = 1;
        String firstExistPath = null;
        String existPath = null;

        while (exist) {
            if (firstExistPath == null && existPath != null) {
                firstExistPath = existPath;
            }

            existPath = imageExist(backupPath);

            if (existPath != null) {
                backupPath = filePath.substring(0, filePath.lastIndexOf(".")) +
                        "_" + String.format("%03d", count) +
                        (firstExistPath == null ? existPath.substring(existPath.lastIndexOf(".")) : firstExistPath.substring(firstExistPath.lastIndexOf(".")));

                count ++;
            } else {
                exist = false;
            }

        }

        if (firstExistPath != null && !filePath.equals(backupPath)) {
//            logger.debug("move file from: " + firstExistPath + " to " + backupPath);
            Path source = Paths.get(firstExistPath);
            Path dist = Paths.get(backupPath);
            Files.move(source, dist, REPLACE_EXISTING);
        }

        ctMemberInfoService.updateIsDataComplete(memberId);
    }

    private String imageExist(String path) {

        String[] fileExts = {"jpg", "png", "gif", "jpeg", "JPG", "PNG", "GIF", "JPEG"};

        for (int i = 0; i < fileExts.length; i++) {
            String tempPath = path.substring(0, path.lastIndexOf(".")) + "." + fileExts[i];

//            logger.debug("check: " + tempPath);
            File tempFile = new File(tempPath);

            if (tempFile.exists()) {
                return tempPath;
            }
        }

        return null;
    }

    private void saveFile(MultipartFile uploadFile, String filePath) throws Exception{
        FileOutputStream fileOutputStreamO = new FileOutputStream(filePath, false);
        fileOutputStreamO.write(uploadFile.getBytes());
        fileOutputStreamO.flush();
        fileOutputStreamO.close();
    }

    public void createPath(String pathStr) {
        try {
            Files.createDirectories(Paths.get(pathStr));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void rearrangeMemberImage() {
        logger.debug("rearrangeMemberImage");


    }

    public List<TrainingFile> getTrainingFiles() {
        String videoPath = sysCodeService.getTrainingVideosPath();
        String docPath = sysCodeService.getTrainingDocsPath();

        List<TrainingFile> trainingFileNameList = new ArrayList<>();

        File videoFile = new File(videoPath);

        for (File item: videoFile.listFiles()) {
            trainingFileNameList.add(new TrainingFile(item.getName(), CommonUtils.FormatLong2DateTime(item.lastModified()), "VIDEO", item.getAbsolutePath()));
        }

        File docFile = new File(docPath);

        for (File item: docFile.listFiles()) {
            trainingFileNameList.add(new TrainingFile(item.getName(), CommonUtils.FormatLong2DateTime(item.lastModified()), "DOCUMENT", item.getAbsolutePath()));
        }

        return trainingFileNameList;

    }
}
