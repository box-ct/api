package tw.org.ctworld.meditation.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.models.SysCode;
import tw.org.ctworld.meditation.repositories.SysCodeRepo;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class SysCodeService {

    @Autowired
    private SysCodeRepo sysCodeRepo;

    public List<SysCode> getAllSysCode() {
        return  sysCodeRepo.findAll();
    }

    public SysCode saveSysCode(SysCode sysCode) {
        return sysCodeRepo.save(sysCode);
    }

    public List<SysCode> saveSysCode(List<SysCode> sysCode) {
        return sysCodeRepo.saveAll(sysCode);
    }

    public SysCode addSysCode(String codeDesc, String codeKey, String codeValue, String codeCat, HttpSession session) {
        SysCode code = new SysCode();
        code.setCode_category(codeCat);
        code.setCode_key(codeKey);
        code.setCode_value(codeValue);
        code.setCode_desc(codeDesc);

        code.setCreatorAndUpdater(session);

        return sysCodeRepo.save(code);
    }

    public void deleteSysCodeById(long id) {
        sysCodeRepo.deleteById(id);
    }

    public void deleteSysCode(SysCode sysCode) {
        sysCodeRepo.delete(sysCode);
    }

    public SysCode getSysCodeByKey(String key) {
        if (key != null && !key.isEmpty()) {
            List<SysCode> sysCodes = sysCodeRepo.GetSysCodeByKey(key);

            if (sysCodes.size() > 0) {
                return sysCodes.get(0);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public String getMakeupFilePath() {
        SysCode makeupFilePathCode = getSysCodeByKey("class_converted_makeupfiles_path");

        if (makeupFilePathCode != null) {
            return makeupFilePathCode.getCode_value();
        } else {
            return "/opt/class_ converted _makeupfiles";
        }
    }

    public String getOriginalMakeupFilePath() {
        SysCode tempFilePathCode = getSysCodeByKey("class_original_makeupfiles_path");

        if (tempFilePathCode != null) {
            return tempFilePathCode.getCode_value();
        } else {
            return "/opt/class_ original_makeupfiles";
        }
    }

    public String getMemberImagePath() {
        SysCode tempFilePathCode = getSysCodeByKey("class_member_image_path");

        if (tempFilePathCode != null) {
            return tempFilePathCode.getCode_value();
        } else {
            return "/opt/class_member_images";
        }
    }

    public String getTrainingDocsPath() {
        SysCode tempFilePathCode = getSysCodeByKey("training_docs_path");

        if (tempFilePathCode != null) {
            return tempFilePathCode.getCode_value();
        } else {
            return "/opt/class_training_docs";
        }
    }

    public String getTrainingVideosPath() {
        SysCode tempFilePathCode = getSysCodeByKey("training_videos_path");

        if (tempFilePathCode != null) {
            return tempFilePathCode.getCode_value();
        } else {
            return "/opt/class_training_docs";
        }
    }

    public int getUpdateAgeHour() {

        List<SysCode> sysCodes = sysCodeRepo.GetSysCodeByKey("class_update_age_hour");

        if (sysCodes.size() > 0) {
            return Integer.parseInt(sysCodes.get(0).getCode_value());
        } else {
            return 2;
        }
    }
}
