package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain003_2Object;
import tw.org.ctworld.meditation.beans.UnitStartUp.*;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.libs.CommonUtils.ParseDateTime2TimeZoneDT;

@Service
public class UnitStartUpService {

    private static final Logger logger = LoggerFactory.getLogger(UnitStartUpService.class);

    @Autowired
    private EventUnitInfoRepo eventUnitInfoRepo;

    @Autowired
    private EventMainRepo eventMainRepo;

    @Autowired
    private EventPgmUnitRepo eventPgmUnitRepo;

    @Autowired
    private EventEnrollMainRepo eventEnrollMainRepo;

    @Autowired
    private EventUnitSpecialInputRepo eventUnitSpecialInputRepo;

    @Autowired
    private UnitMasterInfoRepo unitMasterInfoRepo;

    @Autowired
    private CtMasterInfoRepo ctMasterInfoRepo;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;


    // 取得 event_category_def 列表
    public UnitStartUp001Response getLastEventEnrollGroupList(HttpSession session, String eventId) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<String> eventEnrollGroupNameLists = eventUnitInfoRepo.GetLastEventEnrollGroupNameListByUnitId(unitId, eventId);

        String eventEnrollGroupNameList = "";

        if (eventEnrollGroupNameLists.size() != 0) {

            eventEnrollGroupNameList = eventEnrollGroupNameLists.get(0);
        }

        return new UnitStartUp001Response(200, "success", eventEnrollGroupNameList);
    }

    // 取得精舍啟動活動設定列表
    public UnitStartUp002Response getList(HttpSession session) {

        List<UnitStartUp002_1Object> items = new ArrayList<>();

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<UnitStartUp002_1Object> eventMainList = eventMainRepo.GetEventNameListByUnitId(unitId);

        if (eventMainList.size() > 0) {
            List<String> eventIdList = eventMainList.stream().map(e -> e.getEventId()).collect(Collectors.toList());

            List<CtEventMaintain003_2Object> eventEnrollMainRecords =
                    eventEnrollMainRepo.GetRecordsByEventIdList(eventIdList);

            List<Forecast> forecastList = eventUnitInfoRepo.GetForecastByEventIdList(eventIdList, unitId);

            for (UnitStartUp002_1Object e : eventMainList) {

                int masterEnrollCount = eventEnrollMainRecords.stream()
                        .filter(r -> r.getEventIs().equals(e.getEventId()) && r.getIsMaster() == true)
                        .collect(Collectors.toList()).size();

                int notMasterEnrollCount = eventEnrollMainRecords.stream()
                        .filter(r -> r.getEventIs().equals(e.getEventId()) && (r.getIsMaster() == null ||r.getIsMaster() == false))
                        .collect(Collectors.toList()).size();

                int notAbbotSignedCount = eventEnrollMainRecords.stream()
                        .filter(r -> r.getEventIs().equals(e.getEventId()) && (r.getIsAbbotSigned() == null || r.getIsAbbotSigned() == false))
                        .collect(Collectors.toList()).size();

                int peopleForecast = 0;

                Optional<Forecast> forecast = forecastList.stream().filter(u -> u.getEventId().equals(e.getEventId())).findAny();

                if (forecast.isPresent()) {
                    peopleForecast = forecast.get().getForecast() != null ? forecast.get().getForecast() : 0;
                }

                items.add(new UnitStartUp002_1Object(
                        e.getEventYear(),
                        e.getSponsorUnitName(),
                        e.getEventName(),
                        e.getEventStartDate(),
                        e.getEventEndDate(),
                        masterEnrollCount,
                        notMasterEnrollCount,
                        e.getIsNeedAbbotSigned(),
                        notAbbotSignedCount,
                        peopleForecast,
                        e.getStatus(),
                        e.getEventNote(),
                        e.getEventId()
                ));
            }
        }

        return new UnitStartUp002Response(200, "success", items);
    }

    // 取得精舍啟動活動單筆資料
    public UnitStartUp003Response getDetail(HttpSession session, String eventId) {

        int errCode = 200;
        String errMsg = "success";

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        UnitStartUp003_1Object eventMain = eventMainRepo.GetEventNameListByEventIdList(eventId);

        UnitStartUp003_2Object eventUnitInfo = null;

        List<UnitStartUp003_3Object> eventUnitSpecialInputs = new ArrayList<>();

        if (eventMain == null) {

            errCode = 400;
            errMsg = "找不到event main id : " + eventId;
        } else {

            List<CtEventMaintain003_2Object> eventEnrollMainRecords =
                    eventEnrollMainRepo.GetRecordsByEventIdList(Arrays.asList(eventId));

            List<CtEventMaintain003_2Object> eventUnitInfoRecords =
                    eventUnitInfoRepo.GetRecordsByEventIdList(Arrays.asList(eventId));

            int masterEnrollCount = eventEnrollMainRecords.stream()
                    .filter(r -> r.getEventIs().equals(eventId) && r.getIsMaster() == true)
                    .collect(Collectors.toList()).size();

            int notMasterEnrollCount = eventEnrollMainRecords.stream()
                    .filter(r -> r.getEventIs().equals(eventId) && r.getIsMaster() == false)
                    .collect(Collectors.toList()).size();

            int notAbbotSignedCount = eventEnrollMainRecords.stream()
                    .filter(r -> r.getEventIs().equals(eventId) && r.getIsAbbotSigned() == false)
                    .collect(Collectors.toList()).size();

            int peopleForecast = eventUnitInfoRecords.stream()
                    .filter(u -> u.getEventIs().equals(eventId))
                    .collect(Collectors.toList()).size();

            eventMain.setMasterEnrollCount(masterEnrollCount);
            eventMain.setNotMasterEnrollCount(notMasterEnrollCount);
            eventMain.setNotAbbotSignedCount(notAbbotSignedCount);
            eventMain.setPeopleForecast(peopleForecast);

            eventUnitSpecialInputs =
                    eventUnitSpecialInputRepo.GetEventUnitSpecialInputListByEventIdAndUnitId(eventId, unitId);

            logger.debug(unitId + "/" + eventId);

            eventUnitInfo = eventUnitInfoRepo.GetEventUnitInfoByEventIdAndUnitId(eventId, unitId);

            if (eventUnitInfo != null) {
                eventUnitInfo.setEventUnitSpecialInputs(eventUnitSpecialInputs);
            } else {
                logger.debug("no event unit info");
            }
        }

        return new UnitStartUp003Response(errCode, errMsg, eventMain, eventUnitInfo);
    }

    // 更新單筆資料
    public BaseResponse putDetail(HttpSession session, String eventId, UnitStartUp004Request unitStartUp004Request) {

        int errCode = 200;
        String errMsg = "success";

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        EventUnitInfo eventUnitInfo = eventUnitInfoRepo.GetEventUnitInfoDetailByEventIdAndUnitId(eventId, unitId);

        if (eventUnitInfo == null) {

            errCode = 400;
            errMsg = "找不到event main id : " + eventId;
        } else {

            List<Long> ids = new ArrayList<>();

            for (UnitStartUp004Object o : unitStartUp004Request.getEventUnitSpecialInputs()) {

                ids.add((long) o.getId());
            }

            if (ids.size() != 0) {

                List<EventUnitSpecialInput> eventUnitSpecialInputList =
                        eventUnitSpecialInputRepo.GetEventUnitSpecialInputListByEventIdAndsInputIdList(eventId, ids);

                for (EventUnitSpecialInput eventUnitSpecialInput : eventUnitSpecialInputList) {

                    UnitStartUp004Object unitStartUp004Object = unitStartUp004Request.getEventUnitSpecialInputs().stream()
                            .filter(w -> w.getId() == eventUnitSpecialInput.getId())
                            .collect(Collectors.toList()).get(0);

                    eventUnitSpecialInput.copyUnitStartUp004Object(unitStartUp004Object);
                }

                eventUnitSpecialInputRepo.saveAll(eventUnitSpecialInputList);
            }

            eventUnitInfo.copyUnitStartUp004Request(unitStartUp004Request);

            eventUnitInfoRepo.save(eventUnitInfo);
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 取得法師資訊
    public UnitStartUp005Response getUnitMasterInfo(HttpSession session, String eventId) {

        int errCode = 200;
        String errMsg = "success";

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<UnitStartUp005Object> items = unitMasterInfoRepo.GetUnitMasterInfoListByUnitId(unitId);

        if (items.size() != 0) {

            List<String> masterIdList = items.stream().map(s -> s.getMasterId()).collect(Collectors.toList());

            List<String> enrollUserIdList =
                    eventEnrollMainRepo.GetEnrollUserIdListByEventIdAndEnrollUserIdList(eventId, masterIdList);

            for (UnitStartUp005Object item : items) {

                if (enrollUserIdList.contains(item.getMasterId())) {

                    item.setIsEnroll(true);
                }
            }
        }

        return new UnitStartUp005Response(errCode, errMsg, items);
    }

    // 新增單位法師
    public UnitStartUp006Response addUnitMasterInfo(HttpSession session, String eventId,
                                                    UnitStartUp006Request request) {

        int errCode = 200;
        String errMsg = "success";
        List<UnitStartUp006Object> items = new ArrayList<>();

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
        String unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());

        String masterId = request.getMasterId();

        CtMasterInfo ctMasterInfo = ctMasterInfoRepo.GetCtMasterInfoByMasterId(masterId);

        if (ctMasterInfo == null) {

            errCode = 400;
            errMsg = "找不到masterId : " + request.getMasterId();
        } else {

            UnitMasterInfo unitMasterInfoRecord = unitMasterInfoRepo.GetUnitMasterInfoByUnitIdAndMasterId(unitId, masterId);

            if (unitMasterInfoRecord != null) {

                errCode = 400;
                errMsg = "{" + ctMasterInfo.getMasterName() + "}法師已在單位法師名單中";
            } else {

                UnitMasterInfo unitMasterInfo = new UnitMasterInfo();
                unitMasterInfo.setUnitId(unitId);
                unitMasterInfo.setUnitName(unitName);
                unitMasterInfo.setMasterId(ctMasterInfo.getMasterId());
                unitMasterInfo.setMasterName(ctMasterInfo.getMasterName());
                unitMasterInfo.setMasterPreceptTypeName(ctMasterInfo.getMasterPreceptTypeName());
                unitMasterInfo.setIsAbbot(request.getIsAbbot());
                unitMasterInfo.setIsTreasurer(request.getIsTreasurer());
                unitMasterInfo.setPreceptOrder(request.getPreceptOrder());
                unitMasterInfo.setJobOrder(request.getJobOrder());
                unitMasterInfo.setJobTitle(request.getJobTitle());
                unitMasterInfo.setMobileNum(request.getMobileNum());
                unitMasterInfo.setUpdater(session);

                unitMasterInfoRepo.save(unitMasterInfo);


                Boolean isEnroll = false;

                String enrollUserId = eventEnrollMainRepo.GetEnrollUserIdByEventIdAndEnrollUserId(
                        eventId, request.getMasterId());

                if (enrollUserId != null) {

                    isEnroll = true;
                }

                items.add(new UnitStartUp006Object(
                        isEnroll,
                        unitMasterInfo.getMasterId(),
                        unitMasterInfo.getMasterName(),
                        unitMasterInfo.getJobTitle(),
                        unitMasterInfo.getIsAbbot(),
                        unitMasterInfo.getIsTreasurer(),
                        unitMasterInfo.getMobileNum()
                ));
            }
        }

        if (errCode == 200) {

            errMsg = "成功新増{" + ctMasterInfo.getMasterName() + "}法師";
        }

        return new UnitStartUp006Response(errCode, errMsg, items);
    }

    // 取得已報名的學員資料
    public UnitStartUp007Response getEventEnrollMain(HttpSession session, String eventId) {

        List<UnitStartUp007Object> items =
                eventEnrollMainRepo.GetClassEventEnrollMainListByEventId2(eventId);

        if (items.size() != 0) {

            for (UnitStartUp007Object item : items) {
                String photo = ctMemberInfoService.getPhotosByMemberId(item.getEnrollUserId()).getPhoto();

                item.setPhoto(photo);
            }
        }

        return new UnitStartUp007Response(200, "success", items);
    }
}
