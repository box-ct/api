package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.EventEnrollMember.ModLeaderRequest;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.models.EventUnitInfo;
import tw.org.ctworld.meditation.repositories.EventUnitInfoRepo;

import javax.servlet.http.HttpSession;

@Service
public class EventUnitInfoService {

    private static final Logger logger = LoggerFactory.getLogger(EventUnitInfoService.class);

    @Autowired
    private EventUnitInfoRepo eventUnitInfoRepo;

    public boolean modifyLeader(HttpSession session, String eventId, ModLeaderRequest request) {
        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        // get event unit
        EventUnitInfo eventUnitInfo = eventUnitInfoRepo.GetEventUnitInfoDetailByEventIdAndUnitId(eventId, unitId);

        if (eventUnitInfo != null) {
            boolean changed = false;

            if (request.getLeaderMasterName() != null && !request.getLeaderMasterName().isEmpty()) {
                eventUnitInfo.setLeaderMasterName(request.getLeaderMasterName());
                changed = true;
            }

            if (request.getLeaderMasterId() != null && !request.getLeaderMasterId().isEmpty()) {
                eventUnitInfo.setLeaderMasterId(request.getLeaderMasterId());
                changed = true;
            }

            if (request.getLeaderMasterPhoneNum () != null && !request.getLeaderMasterPhoneNum ().isEmpty()) {
                eventUnitInfo.setLeaderMasterPhoneNum (request.getLeaderMasterPhoneNum ());
                changed = true;
            }

            if (request.getEventLeader() != null && !request.getEventLeader().isEmpty()) {
                eventUnitInfo.setEventLeader(request.getEventLeader());
                changed = true;
            }

            if (request.getEventLeaderMemberId() != null && !request.getEventLeaderMemberId().isEmpty()) {
                eventUnitInfo.setEventLeaderMemberId(request.getEventLeaderMemberId());
                changed = true;
            }

            if (request.getEventLeaderPhone() != null && !request.getEventLeaderPhone().isEmpty()) {
                eventUnitInfo.setEventLeaderPhone(request.getEventLeaderPhone());
                changed = true;
            }

            if (request.getViceEventLeader1() != null && !request.getViceEventLeader1().isEmpty()) {
                eventUnitInfo.setViceEventLeader1(request.getViceEventLeader1());
                changed = true;
            }

            if (request.getViceEventLeader1MemberId() != null && !request.getViceEventLeader1MemberId().isEmpty()) {
                eventUnitInfo.setViceEventLeader1MemberId(request.getViceEventLeader1MemberId());
                changed = true;
            }

            if (request.getViceEventLeader1Phone() != null && !request.getViceEventLeader1Phone().isEmpty()) {
                eventUnitInfo.setViceEventLeader1Phone(request.getViceEventLeader1Phone());
                changed = true;
            }

            if (request.getViceEventLeader2() != null && !request.getViceEventLeader2().isEmpty()) {
                eventUnitInfo.setViceEventLeader2(request.getViceEventLeader2());
                changed = true;
            }

            if (request.getViceEventLeader2MemberId() != null && !request.getViceEventLeader2MemberId().isEmpty()) {
                eventUnitInfo.setViceEventLeader2MemberId(request.getViceEventLeader2MemberId());
                changed = true;
            }

            if (request.getViceEventLeader2Phone() != null && !request.getViceEventLeader2Phone().isEmpty()) {
                eventUnitInfo.setViceEventLeader2Phone(request.getViceEventLeader2Phone());
                changed = true;
            }

            if (request.getEventEnrollGroupNameList() != null && !request.getEventEnrollGroupNameList().isEmpty()) {
                eventUnitInfo.setEventEnrollGroupNameList(request.getEventEnrollGroupNameList());
                changed = true;
            }

            if (changed) {
                eventUnitInfo.setUpdater(session);
                eventUnitInfoRepo.save(eventUnitInfo);
            }

            return true;
        } else {
            return false;
        }
    }

    public void createUnitSpecialInputbyEventId(String eventId) {

    }
}
