package tw.org.ctworld.meditation.services;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.DataReport.DataReport007Object;
import tw.org.ctworld.meditation.beans.DataReport.DataReport009Object;
import tw.org.ctworld.meditation.beans.DataReport.MonthlyTotal;
import tw.org.ctworld.meditation.models.OldClassMonthStats;
import tw.org.ctworld.meditation.repositories.UnitInfoRepo;
import tw.org.ctworld.meditation.repositories.OldClassMonthStatsRepo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.services.ClassAttendRecordService.getMonthList;

@Service
public class OldClassMonthStatsService {

    @Value("${meditation.oldClassMonthStats.lastYearMonth}")
    private String lastYearMonth;

    @Autowired
    OldClassMonthStatsRepo oldClassMonthStatsRepo;

    @Autowired
    ResourcePatternResolver resourcePatternResolver;

    @Autowired
    UnitInfoService unitInfoService;

    @Autowired
    private UnitInfoRepo unitInfoRepo;


    private static final Logger logger = LoggerFactory.getLogger(OldClassMonthStatsService.class);


    // process all xlsx file in ./resources/oldClassStats/
    public void importOldClassStats() {
        // if table is empty
        if (oldClassMonthStatsRepo.count() <= 0) {
            URL url = getClass().getClassLoader().getResource("oldClassStats");

            File[] files = new File(url.getPath()).listFiles();

            for(File file : files) {
                logger.debug(file.getName());
                if (file.getName().contains(".xlsx") || file.getName().contains(".xls")) {
                    importFile(file);
                }
            }
        }
    }

    private void importFile(File file) {

        try {
            InputStream stream = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(stream);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.iterator();

            List<OldClassMonthStats> results = new ArrayList<>();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();

                // Skip read heading
                if (row.getRowNum() == 0) {
                    continue;
                }

                OldClassMonthStats result = new OldClassMonthStats();

                for (int i = 0; i < 26; i++) {
                    Cell cell = row.getCell(i);
                    String temp = cell.toString();
                    int tempNum = 0;

//                    logger.debug(temp + "/" + cell.getCellTypeEnum().toString());
                    if (i >= 4) {
                        try {
                            switch (cell.getCellTypeEnum()) {
                                case NUMERIC:
                                    tempNum = (int) cell.getNumericCellValue();
                                    break;
                                case STRING:
                                    tempNum = Integer.valueOf(temp);
                                    break;
                                case BLANK:
                                default:
                                    tempNum = 0;
                            }

                        } catch (Exception e) {
                            tempNum = 0;
                        }
                    }

                    switch (i) {
                        case 0:
                            result.setYear(temp);
                            break;
                        case 1:
                            result.setMonth(temp);
                            break;
                        case 2:
                            result.setUnitName(temp);
                            break;
                        case 3:
                            result.setUnitId(temp);
                            break;
                        case 4:
                            result.setDayClass1Stats(tempNum);
                            break;
                        case 5:
                            result.setDayClass2Stats(tempNum);
                            break;
                        case 6:
                            result.setDayClass3Stats(tempNum);
                            break;
                        case 7:
                            result.setDayClass4Stats(tempNum);
                            break;
                        case 8:
                            result.setDayClass5Stats(tempNum);
                            break;
                        case 9:
                            result.setNightClass1Stats(tempNum);
                            break;
                        case 10:
                            result.setNightClass2Stats(tempNum);
                            break;
                        case 11:
                            result.setNightClass3Stats(tempNum);
                            break;
                        case 12:
                            result.setNightClass4Stats(tempNum);
                            break;
                        case 13:
                            result.setNightClass5Stats(tempNum);
                            break;
                        case 14:
                            result.setChildrenClass(tempNum);
                            break;
                        case 15:
                            result.setSeniorClass(tempNum);
                            break;
                        case 16:
                            result.setOtherClass(tempNum);
                            break;
                        case 17:
                            result.setEngDayClass1Stats(tempNum);
                            break;
                        case 18:
                            result.setEngDayClass2Stats(tempNum);
                            break;
                        case 19:
                            result.setEngDayClass3Stats(tempNum);
                            break;
                        case 20:
                            result.setEngDayClass4Stats(tempNum);
                            break;
                        case 21:
                            result.setEngNightClass1Stats(tempNum);
                            break;
                        case 22:
                            result.setEngNightClass2Stats(tempNum);
                            break;
                        case 23:
                            result.setEngNightClass3Stats(tempNum);
                            break;
                        case 24:
                            result.setEngNightClass4Stats(tempNum);
                            break;
                        case 25:
                            result.setJapDayClass1Stats(tempNum);
                            break;
                    }
                }

                results.add(result);
            }

            oldClassMonthStatsRepo.saveAll(results);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    // return last year/month of old class statistics
    public String getOldClassStatsYearMonth() {
        String dateStr[] = lastYearMonth.split("-");
        return dateStr[0] + dateStr[1];
    }

    public void avg_by_range(String start, String end) {
        String startStr[] = start.split("-");
        String endStr[] = end.split("-");

        List<String> domesticAbodeId = unitInfoService.getDomesticAbodeUnitId();
        List<String> overseaAbodeId = unitInfoService.getOverseaAbodeUnitId();

        int startDate = Integer.parseInt(startStr[0] + startStr[1]);
        int endDate = Integer.parseInt(endStr[0] + endStr[1]);

        logger.debug(startDate + "/" + endDate);

        List<MonthlyTotal> domesticResults = oldClassMonthStatsRepo.getMonthlyCountByStartEndUnits(domesticAbodeId, startDate - 191100, endDate - 191100);
        List<MonthlyTotal> overseaResults = oldClassMonthStatsRepo.getMonthlyCountByStartEndUnits(overseaAbodeId, startDate - 191100, endDate - 191100);

        for (MonthlyTotal result: domesticResults) {
            logger.debug(result.getTotal() + "/" + result.getYear() + "/" + result.getMonth());
        }

        for (MonthlyTotal result: overseaResults) {
            logger.debug(result.getTotal() + "/" + result.getYear() + "/" + result.getMonth());
        }
    }

    // 海內外精舍週平均(直接輸出Google Chart格式) (舊資料)
    public List<String>[] getAvgByRange(String startDate, String endDate) {

        List<String>[] dataReport007ResponseList;

        List<String> monthList = getMonthList(startDate, endDate);

        dataReport007ResponseList = new List[monthList.size() + 1];

        List<String> titleList = new ArrayList<>();
        titleList.add("月份");
        titleList.add("全球");
        titleList.add("海外");
        titleList.add("國內");

        dataReport007ResponseList[0] = titleList;

        String startStr[] = startDate.substring(0,7).split("-");
        String endStr[] = endDate.substring(0,7).split("-");

        List<String> domesticAbodeId = unitInfoService.getDomesticAbodeUnitId();
        List<String> overseaAbodeId = unitInfoService.getOverseaAbodeUnitId();

        //logger.debug("d size: " +  domesticAbodeId.size());
        //logger.debug("o size: " +  overseaAbodeId.size());

        int mStartDate = Integer.parseInt(startStr[0] + startStr[1]);
        int mEndDate = Integer.parseInt(endStr[0] + endStr[1]);

        //logger.debug("s date " + (mStartDate - 191100));
        //logger.debug("e date " + (mEndDate- 191100));

        List<OldClassMonthStats> dList = oldClassMonthStatsRepo.getMonthlyCountByStartEndUnitsDebug(domesticAbodeId, mStartDate - 191100, mEndDate - 191100);
        List<OldClassMonthStats> oList = oldClassMonthStatsRepo.getMonthlyCountByStartEndUnitsDebug(overseaAbodeId, mStartDate - 191100, mEndDate - 191100);

        //logger.debug("d m size " + dList.size());
        //logger.debug("o m size " + oList.size());

        //for (OldClassMonthStats state: oList) {
        //    logger.debug(state.getUnitId() + "/" + state.getUnitName() + "/" + state.getTotal());
        //}
//        List<String> dUnitIds = dList.stream().map(d -> d.getUnitId()).collect(Collectors.toList());
//        List<String> noRecord = domesticAbodeId.stream().filter(d -> !dUnitIds.contains(d)).collect(Collectors.toList());
//
//        for (String id: noRecord) {
//            logger.debug(id);
//        }

        List<MonthlyTotal> domesticResults = oldClassMonthStatsRepo
                .getMonthlyCountByStartEndUnits(domesticAbodeId, mStartDate - 191100, mEndDate - 191100);

        List<MonthlyTotal> overseaResults = oldClassMonthStatsRepo
                .getMonthlyCountByStartEndUnits(overseaAbodeId, mStartDate - 191100, mEndDate - 191100);

        for (int x = 0 ; x < monthList.size() ; x++) {

            String month = (monthList.get(x).substring(5).length() == 1 ? monthList.get(x).substring(0,5) + "0" +  monthList.get(x).substring(5) : monthList.get(x));

            long domestic = 0;

            List<MonthlyTotal> domesticResult = domesticResults.stream()
                    .filter(s -> ((Integer.parseInt(s.getYear()) + 1911) + "/" + s.getMonth()).equals(month))
                    .collect(Collectors.toList());

            if (domesticResult.size() != 0) {

                domestic = domesticResult.get(0).getTotal();
            }

            long oversea = 0;

            List<MonthlyTotal> overseaResult = overseaResults.stream()
                    .filter(s -> ((Integer.parseInt(s.getYear()) + 1911) + "/" + s.getMonth()).equals(month))
                    .collect(Collectors.toList());

            if (overseaResult.size() != 0) {

                oversea = overseaResult.get(0).getTotal();
            }

            List<String> contentList = new ArrayList<>();
            contentList.add(monthList.get(x));
            contentList.add(String.valueOf(domestic + oversea));
            contentList.add(String.valueOf(oversea));
            contentList.add(String.valueOf(domestic));

            dataReport007ResponseList[x + 1] = contentList;
        }

        return dataReport007ResponseList;
    }

    // 海內外精舍依班別(直接輸出Google Chart格式) (舊資料)
    public List<String>[] getAvgByClass(String unitId, String startDate, String endDate, String language, String classes) {

        List<String> unitIdList = getUnitIdList(unitId);

        String startStr[] = startDate.substring(0,7).split("-");
        String endStr[] = endDate.substring(0,7).split("-");

        int mStartDate = Integer.parseInt(startStr[0] + startStr[1]);
        int mEndDate = Integer.parseInt(endStr[0] + endStr[1]);

        String[] classList = classes.split(",");

        List<String> classNameList = new ArrayList<>();

        List<DataReport009Object> results = new ArrayList<>();

//        List<String> languageList = new ArrayList<>();

        switch (language) {
            case "中文":

                results = oldClassMonthStatsRepo
                        .getLang1ByStartEndUnits(unitIdList, mStartDate - 191100, mEndDate - 191100);

                for (String c : classList) {

                    switch (c.length()) {
                        case 5:

                            List<Integer> results2 = new ArrayList<>();

                            String className = "";

                            switch (c) {
                                case "日間初級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getDayClass1Stats).collect(Collectors.toList());

                                    className = "中日初";
                                    break;
                                case "日間中級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getDayClass2Stats).collect(Collectors.toList());

                                    className = "中日中";
                                    break;
                                case "日間高級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getDayClass3Stats).collect(Collectors.toList());

                                    className = "中日高";
                                    break;
                                case "日間研ㄧ班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getDayClass4Stats).collect(Collectors.toList());

                                    className = "中日研一";
                                    break;
                                case "日間研二班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getDayClass5Stats).collect(Collectors.toList());

                                    className = "中日研二";
                                    break;
                                case "夜間初級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getNightClass1Stats).collect(Collectors.toList());

                                    className = "中夜初";
                                    break;
                                case "夜間中級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getNightClass2Stats).collect(Collectors.toList());

                                    className = "中夜中";
                                    break;
                                case "夜間高級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getNightClass3Stats).collect(Collectors.toList());

                                    className = "中夜高";
                                    break;
                                case "夜間研一班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getNightClass4Stats).collect(Collectors.toList());

                                    className = "中夜研一";
                                    break;
                                case "夜間研二班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getNightClass5Stats).collect(Collectors.toList());

                                    className = "中夜研二";
                                    break;
                            }

                            if (results2.size() != 0 && !classNameList.contains(className)) {

                                classNameList.add(className);
                            }
                            break;
                        default:

                            classNameList.add(c);
                            break;
                    }
                }
                break;
            case "外文":

                results = oldClassMonthStatsRepo
                        .getLang2ByStartEndUnits(unitIdList, mStartDate - 191100, mEndDate - 191100);

                for (String c : classList) {

                    switch (c.length()) {
                        case 5:

                            List<Integer> results2 = new ArrayList<>();

                            String className = "";

                            switch (c) {
                                case "日間初級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getJapDayClass1Stats).collect(Collectors.toList());

                                    className = "日日初";

                                    if (results2.size() != 0 && !classNameList.contains(className)) {

                                        classNameList.add(className);
                                    }

                                    results2 = results.stream()
                                            .map(DataReport009Object::getEngDayClass1Stats).collect(Collectors.toList());

                                    className = "英日初";

                                    if (results2.size() != 0 && !classNameList.contains(className)) {

                                        classNameList.add(className);
                                    }
                                    break;
                                case "日間中級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getEngDayClass2Stats).collect(Collectors.toList());

                                    className = "英日中";

                                    if (results2.size() != 0 && !classNameList.contains(className)) {

                                        classNameList.add(className);
                                    }
                                    break;
                                case "日間高級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getEngDayClass3Stats).collect(Collectors.toList());

                                    className = "英日高";

                                    if (results2.size() != 0 && !classNameList.contains(className)) {

                                        classNameList.add(className);
                                    }
                                    break;
                                case "日間研一班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getEngDayClass4Stats).collect(Collectors.toList());

                                    className = "英日研一";

                                    if (results2.size() != 0 && !classNameList.contains(className)) {

                                        classNameList.add(className);
                                    }
                                    break;
                                case "夜間初級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getEngNightClass1Stats).collect(Collectors.toList());

                                    className = "英夜初";

                                    if (results2.size() != 0 && !classNameList.contains(className)) {

                                        classNameList.add(className);
                                    }
                                    break;
                                case "夜間中級班":

                                    results2 = results.stream()
                                            .map(DataReport009Object::getEngNightClass2Stats).collect(Collectors.toList());

                                    className = "英夜中";

                                    if (results2.size() != 0 && !classNameList.contains(className)) {

                                        classNameList.add(className);
                                    }
                                    break;
                                case "夜間高級班":
                                    results2 = results.stream()
                                            .map(DataReport009Object::getEngNightClass3Stats).collect(Collectors.toList());

                                    className = "英夜高";

                                    if (results2.size() != 0 && !classNameList.contains(className)) {

                                        classNameList.add(className);
                                    }
                                    break;
                                case "夜間研一班":
                                    results2 = results.stream()
                                            .map(DataReport009Object::getEngNightClass4Stats).collect(Collectors.toList());

                                    className = "英夜研一";

                                    if (results2.size() != 0 && !classNameList.contains(className)) {

                                        classNameList.add(className);
                                    }
                                    break;
                            }
                            break;
                        default:

                            classNameList.add(c);
                            break;
                    }
                }
                break;
        }

        List<String> monthList = getMonthList(startDate, endDate);

        List<String>[] dataReport009ResponseList = new List[monthList.size() + 1];

        List<String> titleList = new ArrayList<>();

        titleList.add("月份");

        for (String className : classNameList) {

            titleList.add(className);
        }

        dataReport009ResponseList[0] = titleList;

        for (int x = 0 ; x < monthList.size() ; x++) {

            String year = String.valueOf(Integer.parseInt(monthList.get(x).substring(0,4)) - 1911);

            String month = (monthList.get(x).substring(5).length() == 1 ? "0" +  monthList.get(x).substring(5) : monthList.get(x).substring(5));

            List<String> contentList = new ArrayList<>();

            contentList.add(monthList.get(x));

            List<DataReport009Object> thisMonthClassAttendRecordList = results.stream()
                    .filter(s -> s.getYear().equals(year) && s.getMonth().equals(month))
                    .collect(Collectors.toList());

            for (String className : classNameList) {

                if (thisMonthClassAttendRecordList.size() != 0) {

                    int count = 0;

                    switch (className) {
                        case "中日初":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getDayClass1Stats();
                            }
                            break;
                        case "中日中":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getDayClass2Stats();
                            }
                            break;
                        case "中日高":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getDayClass3Stats();
                            }
                            break;
                        case "中日研一":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getDayClass4Stats();
                            }
                            break;
                        case "中日研二":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getDayClass5Stats();
                            }
                            break;
                        case "中夜初":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getNightClass1Stats();
                            }
                            break;
                        case "中夜中":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getNightClass2Stats();
                            }
                            break;
                        case "中夜高":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getNightClass3Stats();
                            }
                            break;
                        case "中夜研一":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getNightClass4Stats();
                            }
                            break;
                        case "中夜研二":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getNightClass5Stats();
                            }
                            break;
                        case "日日初":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getJapDayClass1Stats();
                            }
                            break;
                        case "英日初":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getEngDayClass1Stats();
                            }
                            break;
                        case "英日中":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getEngDayClass2Stats();
                            }
                            break;
                        case "英日高":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getEngDayClass3Stats();
                            }
                            break;
                        case "英日研一":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getEngDayClass4Stats();
                            }
                            break;
                        case "英夜初":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getEngNightClass1Stats();
                            }
                            break;
                        case "英夜中":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getEngNightClass2Stats();
                            }
                            break;
                        case "英夜高":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getEngNightClass3Stats();
                            }
                            break;
                        case "英夜研一":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getEngNightClass4Stats();
                            }
                            break;
                        case "兒童班":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getChildrenClass();
                            }
                            break;
                        case "長青班":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getSeniorClass();
                            }
                            break;
                        case "其他":

                            for (DataReport009Object record : thisMonthClassAttendRecordList) {

                                count = count + record.getOtherClass();
                            }
                            break;
                    }

                    contentList.add(String.valueOf(count));
                }else {

                    contentList.add("0");
                }
            }

            dataReport009ResponseList[x + 1] = contentList;
        }

        return dataReport009ResponseList;
    }

    private List<String> getUnitIdList(String unitId) {

        List<String> unitIdList = new ArrayList<>();

        if (unitId.equals("0") || unitId.equals("1") || unitId.equals("2")) {

            List<DataReport007Object> unitObjectList = unitInfoRepo.GetUnitObjectList();

            List<DataReport007Object> dataReport007Objects = new ArrayList<>();

            switch (unitId) {
                case "0": // 全部單位

                    dataReport007Objects = unitObjectList.stream()
                            .collect(Collectors.toList());
                    break;
                case "1": // 國內單位

                    dataReport007Objects = unitObjectList.stream().filter(p -> p.getOverseasUnit() == false)
                            .collect(Collectors.toList());

                    break;
                case "2": // 海外單位

                    dataReport007Objects = unitObjectList.stream().filter(p -> p.getOverseasUnit() == true)
                            .collect(Collectors.toList());
                    break;
            }

            for (DataReport007Object dataReport007Object : dataReport007Objects) {

                unitIdList.add(dataReport007Object.getUnitId());
            }
        }else {

            unitIdList.add(unitId);
        }

        return unitIdList;
    }
}
