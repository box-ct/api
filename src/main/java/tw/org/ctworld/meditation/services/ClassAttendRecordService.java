package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.Anonymous.*;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.ClassBarCode.ClassBarCode001Object;
import tw.org.ctworld.meditation.beans.ClassBarCode.ClassBarCode001Response;
import tw.org.ctworld.meditation.beans.ClassBarCode.ClassBarCode002Response;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord004Request;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord006Request;
import tw.org.ctworld.meditation.beans.DataReport.DataReport007Object;
import tw.org.ctworld.meditation.beans.DataReport.DataReport011_1Object;
import tw.org.ctworld.meditation.beans.DataReport.DataReport011_2Object;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.ClassAttendRecord;
import tw.org.ctworld.meditation.models.ClassEnrollForm;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.libs.CommonUtils.Empty2Null;
import static tw.org.ctworld.meditation.libs.CommonUtils.Null2Empty;

@Service
public class ClassAttendRecordService {

    private static final Logger logger = LoggerFactory.getLogger(ClassAttendRecordService.class);

    @Autowired
    private ClassAttendRecordRepo classAttendRecordRepo;

    @Autowired
    private ClassInfoRepo classInfoRepo;

    @Autowired
    private UnitInfoRepo unitInfoRepo;

    @Autowired
    private ClassEnrollFormRepo classEnrollFormRepo;

    @Autowired
    private ClassDateInfoRepo classDateInfoRepo;


//    @Value("${meditation.old_class_month_stats.lastYearMonth}")
//    private String LAST_YEAR_MONTH;


    // 取得指定日期的報到記錄
    public List<ClassBarCode001Response.ClassBarCode001Object> getClassAttendRecordList(HttpSession session, String classId, String date) {

        List<ClassBarCode001Response.ClassBarCode001Object> mClassBarCode001ObjectList = new ArrayList<>();

        String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());

        Date mDate = java.sql.Date.valueOf(date);

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT" + timeZone));
        calendar.setTime(mDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date startDate = calendar.getTime();

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date endDate = calendar.getTime();

        List<ClassBarCode001Object> classBarCode001ObjectList =
                classAttendRecordRepo.GetClassAttendRecordListByClassIdAndDate(classId, startDate, endDate);

        if (classBarCode001ObjectList.size() != 0) {

            int loginUnitTimeZone = Integer.parseInt(
                    session.getAttribute(UserSession.Keys.TimeZone.getValue()).toString().replace(".0", ""));

            for (ClassBarCode001Object classBarCode001Object : classBarCode001ObjectList) {

                String attendCheckinDate = "";
                String attendCheckinTime = "";

                if (classBarCode001Object.getAttendCheckinDtTm() != null) {

                    Calendar calendar2 = Calendar.getInstance(TimeZone.getTimeZone("GMT" + timeZone));
                    calendar2.setTime(classBarCode001Object.getAttendCheckinDtTm());

                    attendCheckinDate = calendar2.get(Calendar.YEAR) + "-" +
                            ((calendar2.get(Calendar.MONTH) + 1) < 10 ? "0" + (calendar2.get(Calendar.MONTH) + 1) : (calendar2.get(Calendar.MONTH) + 1)) + "-" +
                            (calendar2.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendar2.get(Calendar.DAY_OF_MONTH) : calendar2.get(Calendar.DAY_OF_MONTH));

                    attendCheckinTime = (calendar2.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + calendar2.get(Calendar.HOUR_OF_DAY) : calendar2.get(Calendar.HOUR_OF_DAY)) + ":" +
                            (calendar2.get(Calendar.MINUTE) < 10 ? "0" + calendar2.get(Calendar.MINUTE) : calendar2.get(Calendar.MINUTE));
                }

                mClassBarCode001ObjectList.add(new ClassBarCode001Response.ClassBarCode001Object(
                        String.valueOf(classBarCode001Object.getId()),
                        classBarCode001Object.getMemberId(),
                        classBarCode001Object.getGender(),
                        classBarCode001Object.getAliasName(),
                        Null2Empty(classBarCode001Object.getCtDharmaName()),
                        classBarCode001Object.getClassPeriodNum(),
                        classBarCode001Object.getClassName(),
                        classBarCode001Object.getClassGroupId(),
                        classBarCode001Object.getMemberGroupNum(),
                        classBarCode001Object.getClassWeeksNum(),
                        attendCheckinDate,
                        attendCheckinTime,
                        Null2Empty(classBarCode001Object.getAttendMark())
                ));
            }
        }

        return mClassBarCode001ObjectList;
    }

    // 學員報到
    public ClassBarCode002Response enrollment(HttpSession session, String classId, String attendRecordId) {

        String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());

        int minutesBeLate = classInfoRepo.GetMinutesBeLateByClassId(classId);

        ClassAttendRecord classAttendRecord = classAttendRecordRepo.GetClassAttendRecordById(Long.parseLong(attendRecordId));

        ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(classAttendRecord.getMemberId(), classId);

        String attendMark = "V";

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat dateFormatInput = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        dateFormatInput.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));
        Date chickInDate = null;

        SimpleDateFormat dateFormatOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            chickInDate = dateFormatOutput.parse(dateFormatInput.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        logger.error("Server ChickIn Date : " + chickInDate);


        int nowSec = (int) (chickInDate.getTime() / 1000);
        int classSec = (int) (classAttendRecord.getClassDate().getTime() / 1000);
        int diff = nowSec - (classSec + (minutesBeLate * 60));
        logger.debug("Now Sec : " + nowSec);
        logger.debug("Class Sec : " + classSec);
        logger.debug("minutesBeLate : " + minutesBeLate * 60);
        logger.debug("diff : " + diff);

        if (diff > 0) {
            attendMark = "L";
        }

        classAttendRecord.setAttendCheckinDtTm(chickInDate);
        classAttendRecord.setAttendMark(attendMark);
        classAttendRecord.setUpdater(session);

        classAttendRecordRepo.save(classAttendRecord);


        int loginUnitTimeZone = Integer.parseInt(
                session.getAttribute(UserSession.Keys.TimeZone.getValue()).toString().replace(".0", ""));

//        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT" + timeZone));
        calendar.setTime(classAttendRecord.getAttendCheckinDtTm());

        String AttendCheckinDate = "";
        String AttendCheckinTime = "";

        AttendCheckinDate = calendar.get(Calendar.YEAR) + "-" +
                ((calendar.get(Calendar.MONTH) + 1) < 10 ? "0" + (calendar.get(Calendar.MONTH) + 1) : (calendar.get(Calendar.MONTH) + 1)) + "-" +
                (calendar.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendar.get(Calendar.DAY_OF_MONTH) : calendar.get(Calendar.DAY_OF_MONTH));
        AttendCheckinTime = (calendar.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + calendar.get(Calendar.HOUR_OF_DAY) : calendar.get(Calendar.HOUR_OF_DAY)) + ":" +
                (calendar.get(Calendar.MINUTE) < 10 ? "0" + calendar.get(Calendar.MINUTE) : calendar.get(Calendar.MINUTE));

        ClassBarCode002Response classBarCode002Response = new ClassBarCode002Response(
                200,
                "success",
                attendRecordId,
                classAttendRecord.getMemberId(),
                classAttendRecord.getGender(),
                classAttendRecord.getAliasName(),
                Null2Empty(classAttendRecord.getCtDharmaName()),
                classAttendRecord.getClassPeriodNum(),
                classAttendRecord.getClassName(),
                classAttendRecord.getClassGroupId(),
                classEnrollForm.getMemberGroupNum(),
                classAttendRecord.getClassWeeksNum(),
                AttendCheckinDate,
                AttendCheckinTime,
                classAttendRecord.getAttendMark()
        );

        return classBarCode002Response;
    }

    // 學員取消報到
    public ClassBarCode002Response cancelEnrollment(HttpSession session, String classId, String attendRecordId) {

        ClassAttendRecord classAttendRecord = classAttendRecordRepo.GetClassAttendRecordById(Long.parseLong(attendRecordId));

        ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(classAttendRecord.getMemberId(), classId);

        classAttendRecord.setAttendCheckinDtTm(null);
        classAttendRecord.setAttendMark(null);
        classAttendRecord.setUpdater(session);

        classAttendRecordRepo.save(classAttendRecord);


        ClassBarCode002Response classBarCode002Response = new ClassBarCode002Response(
                200,
                "success",
                attendRecordId,
                classAttendRecord.getMemberId(),
                classAttendRecord.getGender(),
                classAttendRecord.getAliasName(),
                Null2Empty(classAttendRecord.getCtDharmaName()),
                classAttendRecord.getClassPeriodNum(),
                classAttendRecord.getClassName(),
                classAttendRecord.getClassGroupId(),
                classEnrollForm.getMemberGroupNum(),
                classAttendRecord.getClassWeeksNum(),
                "",
                "",
                Null2Empty(classAttendRecord.getAttendMark())
        );

        return classBarCode002Response;
    }

    // 儲存上課記錄
    public BaseResponse saveClassAttendRecordList(HttpSession session, String classId, ClassRecord004Request classRecord004Request) {

        boolean isBreak = false;

        int errCode = 200;
        String errMsg = "success";

        String[] attendMarkList = {"V", "O", "A", "X", "D", "N", "W", "F", "M", "L", "E", "S1", "S2", "S3", ""};

        if (classRecord004Request.getMemberList().size() != 0) {

            for (ClassRecord004Request.memberObject memberObject : classRecord004Request.getMemberList()) {

                if (memberObject.getAttendRecordList().size() != 0) {

                    for (ClassRecord004Request.attendRecordObject attendRecordObject : memberObject.getAttendRecordList()) {

                        String attendMark = attendRecordObject.getAttendMark().toUpperCase();

                        if (!Arrays.asList(attendMarkList).contains(attendMark)) {

                            errCode = 400;
                            errMsg = "輸入資料錯誤，僅可輸入: V, O, A, X, D, N, W, F, M, L, E, S1, S2, S3，或空值";

                            isBreak = true;

                            break;
                        }
                    }
                }

                if (isBreak) {
                    break;
                }
            }

            if (!isBreak) {

                for (ClassRecord004Request.memberObject memberObject : classRecord004Request.getMemberList()) {

                    String memberId = memberObject.getMemberId();

                    if (memberObject.getAttendRecordList().size() != 0) {

                        for (ClassRecord004Request.attendRecordObject attendRecordObject : memberObject.getAttendRecordList()) {

                            String attendMark = attendRecordObject.getAttendMark().toUpperCase();

                            ClassAttendRecord classAttendRecord =
                                    classAttendRecordRepo.GetClassAttendRecordByMemberIdAndClassIdAndAttendMark(
                                            memberId, classId, attendRecordObject.getClassWeeksNum());

                            classAttendRecord.setAttendMark(Empty2Null(attendMark));
                            classAttendRecord.setUpdater(session);

                            classAttendRecordRepo.save(classAttendRecord);


                            ClassEnrollForm classEnrollForm =
                                    classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(memberId, classId);

                            if (attendMark.equals("X")) {

                                Date date = new Date();
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(date);
                                calendar.set(Calendar.HOUR_OF_DAY, 0);
                                calendar.set(Calendar.MINUTE, 0);
                                calendar.set(Calendar.SECOND, 0);
                                calendar.set(Calendar.MILLISECOND, 0);
                                Date classDroppedDate = calendar.getTime();

                                classEnrollForm.setIsDroppedClass(true);
                                classEnrollForm.setClassDroppedDate(classDroppedDate);
                                classEnrollForm.setUpdater(session);

                                classEnrollFormRepo.save(classEnrollForm);
                            }
                        }
                    }
                }
            }
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 刪除學員的上課記錄
    public String deleteClassAttendRecord(String classId, String memberId) {

        classAttendRecordRepo.DeleteClassAttendRecordByMemberIdAndClassId(memberId, classId);

        classEnrollFormRepo.DeleteClassEnrollFormByMemberIdAndClassId(memberId, classId);

        return "success";
    }

    // 單筆編輯
    public String editClassAttendRecord(String classId, String memberId, ClassRecord006Request classRecord006Request) {

        ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(memberId, classId);

        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        classEnrollForm.setChangeAndWaitPrintMark(classRecord006Request.getChangeAndWaitPrintMark());

        //  檢查DB isDroppedClass
        if (classEnrollForm.getIsDroppedClass() == true) {
            // isDroppedClass原本是true

            if (!classRecord006Request.getIsDroppedClass()) {
                // isDroppedClass is false 沒有被勾選

                classEnrollForm.setIsDroppedClass(false);
                classEnrollForm.setNewReturnChangeMark("中輟復學");
                classEnrollForm.setNewReturnChangeDate(calendar.getTime());
                classEnrollForm.setChangeAndWaitPrintMark(true);
                classEnrollForm.setClassDroppedDate(null);
            }
        } else {
            // isDroppedClass原本是false

            if (classRecord006Request.getIsDroppedClass()) {
                // isDroppedClass is true 被勾選

                classEnrollForm.setIsDroppedClass(true);
                classEnrollForm.setClassDroppedDate(calendar.getTime());
            }
        }
        classEnrollForm.setUserDefinedLabel1(classRecord006Request.getUserDefinedLabel1());
        classEnrollForm.setUserDefinedLabel2(classRecord006Request.getUserDefinedLabel2());
        classEnrollForm.setClassEnrollFormNote(Empty2Null(classRecord006Request.getClassEnrollFormNote()));

        return "success ClassEnrollFormId : " + classEnrollFormRepo.save(classEnrollForm).getClassEnrollFormId();
    }

    // 海內外精舍週平均(直接輸出Google Chart格式)
    public List<String>[] getAvgByRange(String startDate, String endDate) {

        List<String>[] dataReport007ResponseList;

        List<DataReport007Object> unitObjectList = unitInfoRepo.GetUnitObjectList();

        // 國內精舍
        List<DataReport007Object> domesticDataReport007Objects = unitObjectList.stream().filter(p -> p.getOverseasUnit() == false)
                .collect(Collectors.toList());

        List<String> domesticUnitIdList = new ArrayList<>();

        for (DataReport007Object dataReport007Object : domesticDataReport007Objects) {

            domesticUnitIdList.add(dataReport007Object.getUnitId());
        }
        //logger.error("國內精舍 : " + domesticUnitIdList);

        // 海外精舍
        List<DataReport007Object> overseasDataReport007Objects = unitObjectList.stream().filter(p -> p.getOverseasUnit() == true)
                .collect(Collectors.toList());

        List<String> overseasUnitIdList = new ArrayList<>();

        for (DataReport007Object dataReport007Object : overseasDataReport007Objects) {

            overseasUnitIdList.add(dataReport007Object.getUnitId());
        }
        //logger.error("海外精舍 : " + overseasUnitIdList);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(java.sql.Date.valueOf(startDate));
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date mStartDate = calendar.getTime();

        calendar.setTime(java.sql.Date.valueOf(endDate));
        calendar.set(Calendar.MONTH, 31);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date mEndDate = calendar.getTime();

        List<DataReport007Object> domesticClassAttendRecordList =
                classAttendRecordRepo.GetClassAttendRecordListByClassDateAndUnitIds(mStartDate, mEndDate, domesticUnitIdList);

        List<DataReport007Object> overseasClassAttendRecordList =
                classAttendRecordRepo.GetClassAttendRecordListByClassDateAndUnitIds(mStartDate, mEndDate, overseasUnitIdList);

        List<String> monthList = getMonthList(startDate, endDate);

        dataReport007ResponseList = new List[monthList.size() + 1];

        List<String> titleList = new ArrayList<>();
        titleList.add("月份");
        titleList.add("全球");
        titleList.add("海外");
        titleList.add("國內");

        dataReport007ResponseList[0] = titleList;

        for (int x = 0; x < monthList.size(); x++) {

            String month = (monthList.get(x).substring(5).length() == 1 ? monthList.get(x).substring(0, 5) + "0" + monthList.get(x).substring(5) : monthList.get(x))
                    .replace("/", "-");

            List<DataReport007Object> domesticThisMonthClassAttendRecordList = domesticClassAttendRecordList.stream()
                    .filter(p -> p.getClassDate().toString().substring(0, 7).equals(month)).collect(Collectors.toList());

            int domesticThisMonthAverage = getThisMonthAverage(domesticThisMonthClassAttendRecordList, month);

            List<DataReport007Object> overseasThisMonthClassAttendRecordList = overseasClassAttendRecordList.stream()
                    .filter(p -> p.getClassDate().toString().substring(0, 7).equals(month)).collect(Collectors.toList());

            int overseasThisMonthAverage = getThisMonthAverage(overseasThisMonthClassAttendRecordList, month);

            List<String> contentList = new ArrayList<>();
            contentList.add(monthList.get(x));
            contentList.add(String.valueOf(domesticThisMonthAverage + overseasThisMonthAverage));
            contentList.add(String.valueOf(overseasThisMonthAverage));
            contentList.add(String.valueOf(domesticThisMonthAverage));

            dataReport007ResponseList[x + 1] = contentList;
        }

        return dataReport007ResponseList;
    }

    // 海內外精舍依班別(直接輸出Google Chart格式)
    public List<String>[] getAvgByClass(String unitId, String startDate, String endDate, String language, String classes) {

        List<String> unitIdList = getUnitIdList(unitId);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(java.sql.Date.valueOf(startDate));
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date mStartDate = calendar.getTime();

        calendar.setTime(java.sql.Date.valueOf(endDate));
        calendar.set(Calendar.MONTH, 31);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date mEndDate = calendar.getTime();

        List<String> languageList = new ArrayList<>();

        switch (language) {
            case "中文":

                languageList.add("國語");
                languageList.add("台語");
                languageList.add("客語");
                break;
            case "外文":

                languageList.add("英語");
                languageList.add("日語");
                languageList.add("德語");
                languageList.add("粵語");
                languageList.add("義大利語");
                break;
        }

        List<DataReport007Object> classAttendRecordList =
                classAttendRecordRepo.GetClassAttendRecordListByClassDateAndUnitIdsAndLanguages(mStartDate, mEndDate, unitIdList, languageList);

        List<String> monthList = getMonthList(startDate, endDate);

        List<String>[] dataReport009ResponseList = new List[monthList.size() + 1];

        String[] classList = classes.split(",");

        List<String> classNameList = new ArrayList<>();

        for (String c : classList) {

            List<DataReport007Object> classAttendRecordList2 = new ArrayList<>();

            if (c.length() == 5) { // 日間初級班~夜間研二班

                classAttendRecordList2 = classAttendRecordList.stream()
                        .filter(s -> s.getClassDayOrNight().equals(c.substring(0, 2)) &&
                                s.getClassTypeName().equals(c.substring(2, 5))).collect(Collectors.toList());

                if (classAttendRecordList2.size() != 0) {

                    for (DataReport007Object dataReport007Object : classAttendRecordList2) {

                        int s = dataReport007Object.getClassTypeName().substring(0, 1).equals("研") ? 2 : 1;

                        String temp = dataReport007Object.getClassLanguage().substring(0, 1) +
                                dataReport007Object.getClassDayOrNight().substring(0, 1) +
                                dataReport007Object.getClassTypeName().substring(0, s);

                        if (!classNameList.contains(temp)) {

                            classNameList.add(temp);
                        }
                    }
                }
            } else {
                long count = classAttendRecordList.stream().filter(r -> r.getClassTypeName().equals(c)).count();

                if (count > 0) {
                    classNameList.add(c);
                }
            }
        }

        List<String> titleList = new ArrayList<>();

        titleList.add("月份");

        for (String className : classNameList) {

            titleList.add(className);
        }

        dataReport009ResponseList[0] = titleList;

        for (int x = 0; x < monthList.size(); x++) {

            String month = (monthList.get(x).substring(5).length() == 1 ? monthList.get(x).substring(0, 5) + "0" + monthList.get(x).substring(5) : monthList.get(x))
                    .replace("/", "-");

            List<String> contentList = new ArrayList<>();

            contentList.add(monthList.get(x));

            List<DataReport007Object> thisMonthClassAttendRecordList = null;

            for (String className : classNameList) {

                //logger.error(month + className);

                int thisMonthAverage = 0;

                switch (className.length()) {
                    case 4: // 研一班、研二班

                        thisMonthClassAttendRecordList = classAttendRecordList.stream()
                                .filter(p -> p.getClassDate().toString().substring(0, 7).equals(month) &&
                                        p.getClassLanguage().substring(0, 1).equals(className.substring(0, 1)) &&
                                        p.getClassDayOrNight().substring(0, 1).equals(className.substring(1, 2)) &&
                                        p.getClassTypeName().substring(0, 2).equals(className.substring(2)))
                                .collect(Collectors.toList());
                        //print2JSON("thisMonthClassAttendRecordList",thisMonthClassAttendRecordList);

                        thisMonthAverage = getThisMonthAverage(thisMonthClassAttendRecordList, month);
                        break;
                    case 3: // 中夜初、義日高、日日中......兒童班、長青班

                        if (className.equals("兒童班") || className.equals("長青班")) {

                            thisMonthClassAttendRecordList = classAttendRecordList.stream()
                                    .filter(p -> p.getClassDate().toString().substring(0, 7).equals(month) &&
                                            p.getClassTypeName().equals(className))
                                    .collect(Collectors.toList());
                        } else {

                            thisMonthClassAttendRecordList = classAttendRecordList.stream()
                                    .filter(p -> p.getClassDate().toString().substring(0, 7).equals(month) &&
                                            p.getClassLanguage().substring(0, 1).equals(className.substring(0, 1)) &&
                                            p.getClassDayOrNight().substring(0, 1).equals(className.substring(1, 2)) &&
                                            p.getClassTypeName().substring(0, 1).equals(className.substring(2)))
                                    .collect(Collectors.toList());
                        }
                        //print2JSON("thisMonthClassAttendRecordList",thisMonthClassAttendRecordList);

                        thisMonthAverage = getThisMonthAverage(thisMonthClassAttendRecordList, month);
                        break;
                    case 2: // 其他(青少年班、念佛班、梵唄班、才藝班、共修班)

                        String[] classTypeNameList = new String[]{"青少年班", "念佛班", "梵唄班", "才藝班", "共修班"};

                        for (String classTypeName : classTypeNameList) {

                            thisMonthClassAttendRecordList = classAttendRecordList.stream()
                                    .filter(p -> p.getClassDate().toString().substring(0, 7).equals(month) &&
                                            p.getClassTypeName().equals(classTypeName))
                                    .collect(Collectors.toList());
                            //print2JSON("thisMonthClassAttendRecordList",thisMonthClassAttendRecordList);

                            thisMonthAverage = thisMonthAverage + getThisMonthAverage(thisMonthClassAttendRecordList, month);
                        }

                        break;
                }

                contentList.add(String.valueOf(thisMonthAverage));
            }

            dataReport009ResponseList[x + 1] = contentList;
        }

        return dataReport009ResponseList;
    }

    // 海內外精舍依班別(直接輸出Google Chart格式)
    public List<String>[] getAvgByWeek(String unitId, String startDate, String endDate, String weeks) {

        List<String> unitIdList = getUnitIdList(unitId);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(java.sql.Date.valueOf(startDate));
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date mStartDate = calendar.getTime();

        calendar.setTime(java.sql.Date.valueOf(endDate));
        calendar.set(Calendar.MONTH, 31);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date mEndDate = calendar.getTime();

        List<String> monthList = getMonthList(startDate, endDate);

        List<String>[] dataReport011ResponseList = new List[monthList.size() + 1];

        String[] weekList = weeks.split(",");

        List<String> titleList = new ArrayList<>();

        titleList.add("月份");

        List<String> mWeeks = new ArrayList<>();

        for (String w : weekList) {

            switch (w) {
                case "星期一":

                    w = "Mon";
                    mWeeks.add("一");
                    break;
                case "星期二":

                    w = "Tue";
                    mWeeks.add("二");
                    break;
                case "星期三":

                    w = "Wed";
                    mWeeks.add("三");
                    break;
                case "星期四":

                    w = "Thur";
                    mWeeks.add("四");
                    break;
                case "星期五":

                    w = "Fri";
                    mWeeks.add("五");
                    break;
                case "星期六":

                    w = "Sat";
                    mWeeks.add("六");
                    break;
                case "星期日":

                    w = "Sun";
                    mWeeks.add("日");
                    break;
            }
            titleList.add(w);
        }

        dataReport011ResponseList[0] = titleList;

        List<DataReport011_1Object> classAttendRecordList =
                classAttendRecordRepo.GetClassAttendRecordListByClassDateAndUnitIds2(mStartDate, mEndDate, unitIdList);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String mStartDate2 = format.format(mStartDate);
        String mEndDate2 = format.format(mEndDate);

        mStartDate = java.sql.Date.valueOf(mStartDate2);
        mEndDate = java.sql.Date.valueOf(mEndDate2);

        List<DataReport011_2Object> classDateInfoList =
                classDateInfoRepo.GetClassDateInfoByClassDateAndUnitIds(mStartDate, mEndDate, unitIdList, mWeeks);

        for (int x = 0; x < monthList.size(); x++) {

            String month = (monthList.get(x).substring(5).length() == 1 ? monthList.get(x).substring(0, 5) + "0" + monthList.get(x).substring(5) : monthList.get(x))
                    .replace("/", "-");

            List<String> contentList = new ArrayList<>();

            contentList.add(monthList.get(x));

            // 這個月所有上課紀錄
            List<DataReport011_1Object> thisMonthClassAttendRecordList = classAttendRecordList.stream()
                    .filter(p -> p.getClassDate().toString().substring(0, 7).equals(month))
                    .collect(Collectors.toList());

            // 這個月所有上課的ClassId
            List<String> classIdList = new ArrayList<>();

            for (DataReport011_1Object dataReport011_1Object : thisMonthClassAttendRecordList) {

                if (!classIdList.contains(dataReport011_1Object.getClassId())) {

                    classIdList.add(dataReport011_1Object.getClassId());
                }
            }

            int week1Average = 0, week2Average = 0, week3Average = 0, week4Average = 0, week5Average = 0,
                    week6Average = 0, week7Average = 0;

            for (String classId : classIdList) {

                for (String week : mWeeks) {

                    List<DataReport011_2Object> thisClassThisMonthThisWeekDateInfoList = classDateInfoList.stream()
                            .filter(s -> s.getClassId().equals(classId) &&
                                    s.getClassDate().toString().substring(0, 7).equals(month) &&
                                    s.getClassDayOfWeek().equals(week))
                            .collect(Collectors.toList());

                    // classId這個月，各個禮拜的開課出席人數
                    int thisClassThisMonthAttendCount = 0;

                    List<Integer> classWeeksNumList = new ArrayList<>();

                    for (DataReport011_2Object dataReport011_2Object : thisClassThisMonthThisWeekDateInfoList) {

                        if (!classWeeksNumList.contains(dataReport011_2Object.getClassWeeksNum())) {

                            classWeeksNumList.add(dataReport011_2Object.getClassWeeksNum());
                        }

                        thisClassThisMonthAttendCount = thisClassThisMonthAttendCount +
                                thisMonthClassAttendRecordList.stream()
                                        .filter(s -> s.getClassId().equals(classId) &&
                                                s.getClassWeeksNum() == dataReport011_2Object.getClassWeeksNum() &&
                                                s.getAttendMark() != null &&
                                                (s.getAttendMark().equals("V") || s.getAttendMark().equals("L") || s.getAttendMark().equals("E")))
                                        .collect(Collectors.toList())
                                        .size();
                    }

                    // classId這個月，各個禮拜的開課次數
                    int thisClassThisMonthThisWeekCount = classWeeksNumList.size();

                    //logger.error(classId + " 星期" + week + " 開課次數 : " + thisClassThisMonthThisWeekCount);
                    //logger.error(classId + " 星期" + week + " 出席人數 : " + thisClassThisMonthAttendCount);

                    // classId這個月，週平均人數
                    int thisWeekAverage = 0;

                    if (thisClassThisMonthThisWeekCount != 0) {

                        thisWeekAverage = thisClassThisMonthAttendCount / thisClassThisMonthThisWeekCount;
                        //logger.error(monthList.get(x) + " ClassId : " + classId + " 星期 : " + week + " 週平均人數 : " + thisClassThisMonthAttendCount + "/" + thisClassThisMonthThisWeekCount + "=" + thisWeekAverage);

                        switch (week) {
                            case "一": // 星期一週平均人數

                                week1Average = week1Average + thisWeekAverage;
                                break;
                            case "二": // 星期二週平均人數

                                week2Average = week2Average + thisWeekAverage;
                                break;
                            case "三": // 星期三週平均人數

                                week3Average = week3Average + thisWeekAverage;
                                break;
                            case "四": // 星期四週平均人數

                                week4Average = week4Average + thisWeekAverage;
                                break;
                            case "五": // 星期五週平均人數

                                week5Average = week5Average + thisWeekAverage;
                                break;
                            case "六": // 星期六週平均人數

                                week6Average = week6Average + thisWeekAverage;
                                break;
                            case "日": // 星期日週平均人數

                                week7Average = week7Average + thisWeekAverage;
                                break;
                        }
                    }
                }
            }

            for (String week : mWeeks) {

                switch (week) {
                    case "一": // 星期一週平均人數

                        contentList.add(String.valueOf(week1Average));
                        break;
                    case "二": // 星期二週平均人數

                        contentList.add(String.valueOf(week2Average));
                        break;
                    case "三": // 星期三週平均人數

                        contentList.add(String.valueOf(week3Average));
                        break;
                    case "四": // 星期四週平均人數

                        contentList.add(String.valueOf(week4Average));
                        break;
                    case "五": // 星期五週平均人數

                        contentList.add(String.valueOf(week5Average));
                        break;
                    case "六": // 星期六週平均人數

                        contentList.add(String.valueOf(week6Average));
                        break;
                    case "日": // 星期日週平均人數

                        contentList.add(String.valueOf(week7Average));
                        break;
                }
            }

            dataReport011ResponseList[x + 1] = contentList;
        }

        return dataReport011ResponseList;
    }

    private List<String> getUnitIdList(String unitId) {

        List<String> unitIdList = new ArrayList<>();

        if (unitId.equals("0") || unitId.equals("1") || unitId.equals("2")) {

            List<DataReport007Object> unitObjectList = unitInfoRepo.GetUnitObjectList();

            List<DataReport007Object> dataReport007Objects = new ArrayList<>();

            switch (unitId) {
                case "0": // 全部單位

                    dataReport007Objects = unitObjectList.stream()
                            .collect(Collectors.toList());
                    break;
                case "1": // 國內單位

                    dataReport007Objects = unitObjectList.stream().filter(p -> p.getOverseasUnit() == false)
                            .collect(Collectors.toList());

                    break;
                case "2": // 海外單位

                    dataReport007Objects = unitObjectList.stream().filter(p -> p.getOverseasUnit() == true)
                            .collect(Collectors.toList());
                    break;
            }

            for (DataReport007Object dataReport007Object : dataReport007Objects) {

                unitIdList.add(dataReport007Object.getUnitId());
            }
        } else {

            unitIdList.add(unitId);
        }

        return unitIdList;
    }

    public static List<String> getMonthList(String startDate, String endDate) {

        int startYear = Integer.parseInt(startDate.substring(0, 4));
        int endYear = Integer.parseInt(endDate.substring(0, 4));
        int startMonth = Integer.parseInt(startDate.substring(5, 7));
        int endMonth = Integer.parseInt(endDate.substring(5, 7));

        List<String> monthList = new ArrayList<>();

        if (startYear < endYear) {

            if (endYear - startYear >= 2) {

                for (int s = 0; s <= endYear - startYear; s++) {

                    if (s == 0) {

                        for (int x = startMonth; x <= 12; x++) {

                            monthList.add(startYear + "/" + x);
                        }
                    }

                    if (s == endYear - startYear) {

                        for (int x = 1; x <= endMonth; x++) {

                            monthList.add((startYear + s) + "/" + x);
                        }
                    }

                    if (s > 0 && s < endYear - startYear) {

                        for (int x = 1; x <= 12; x++) {

                            monthList.add((startYear + s) + "/" + x);
                        }
                    }
                }
            } else {

                for (int x = startMonth; x <= endMonth + 12; x++) {

                    if (x > 12) {
                        monthList.add(endYear + "/" + (x > 12 ? x - 12 : x));
                    } else {
                        monthList.add(startYear + "/" + x);
                    }
                }
            }
        } else if (startYear == endYear) {

            for (int x = startMonth; x <= endMonth; x++) {

                monthList.add(startYear + "/" + x);
            }
        }

        return monthList;
    }

    private int getThisMonthAverage(List<DataReport007Object> thisMonthClassAttendRecordList, String month) {

        // 這個月有幾門課
        List<String> thisMonthClassIdList = new ArrayList<>();

        for (DataReport007Object dataReport007Object : thisMonthClassAttendRecordList) {

            if (!thisMonthClassIdList.contains(dataReport007Object.getClassId())) {

                thisMonthClassIdList.add(dataReport007Object.getClassId());
            }
        }
        //print2JSON("thisMonthClassIdList",thisMonthClassIdList);

        int thisMonthAverage = 0;

        for (String classId : thisMonthClassIdList) {

            List<DataReport007Object> thisMonthCountList = thisMonthClassAttendRecordList.stream()
                    .filter(p -> p.getClassId().equals(classId)).collect(Collectors.toList());

            // 這門課(classId)這個月上幾堂
            int thisMonthCount = (int) thisMonthCountList.stream()
                    .filter(p ->
                            p.getClassEnrollFormId().equals(thisMonthCountList.get(0).getClassEnrollFormId()))
                    .count();
            //logger.error("這門課這個月上幾堂 : " + thisMonthCount);

            // 這門課(classId)這個月出席人數
            int thisMonthAttendCount = (int) thisMonthClassAttendRecordList.stream()
                    .filter(p -> p.getClassId().equals(classId) &&
                            p.getClassDate().toString().substring(0, 7).equals(month) && p.getAttendMark() != null &&
                            (p.getAttendMark().equals("V") || p.getAttendMark().equals("L") || p.getAttendMark().equals("E")))
                    .count();
            //logger.error("這門課這個月出席人數 : " + thisMonthAttendCount);

            // 這門課(classId)這個月平均出席人數
            thisMonthAverage = thisMonthAverage + (thisMonthAttendCount / thisMonthCount);
            //logger.error("這個月平均值 : " + thisMonthAverage);
        }

        return thisMonthAverage;
    }

    private int getThisMonthAverage2(List<DataReport011_1Object> thisMonthClassAttendRecordList, String classId) {

        int thisClassThisMonthAverage = 0;

        List<DataReport011_1Object> thisMonthCountList = thisMonthClassAttendRecordList.stream()
                .filter(p -> p.getClassId().equals(classId)).collect(Collectors.toList());

        // 這門課(classId)這個月上幾堂
        int thisMonthCount = (int) thisMonthCountList.stream()
                .filter(p ->
                        p.getClassEnrollFormId().equals(thisMonthCountList.get(0).getClassEnrollFormId()))
                .count();
        //logger.error("這門課這個月上幾堂 : " + thisMonthCount);

        // 這門課(classId)這個月出席人數
        int thisMonthAttendCount = (int) thisMonthClassAttendRecordList.stream()
                .filter(p -> p.getClassId().equals(classId) &&
                        p.getAttendMark() != null &&
                        (p.getAttendMark().equals("V") || p.getAttendMark().equals("L") || p.getAttendMark().equals("E")))
                .count();
        //logger.error("這門課這個月出席人數 : " + thisMonthAttendCount);

        // 這個月平均值
        thisClassThisMonthAverage = thisMonthAttendCount / thisMonthCount;
        //logger.error("這個月平均值 : " + thisMonthAverage);

        return thisClassThisMonthAverage;
    }

    // 取得已報到 & 未報到學員列表
    public List<Anonymous007Object> getMemberAttendByClassIdAndClassWeeksNum(String classId, int classWeeksNum) {

        List<Anonymous007Object> items = new ArrayList<>();

        List<String> memberIdList = classEnrollFormRepo.GetMemberIdByClassId(classId);

        if (memberIdList.size() > 0) {

            List<ClassAttendRecord> classAttendRecordList = classAttendRecordRepo
                    .GetClassAttendRecordListByClassIdAndClassWeeksNum2(classId, classWeeksNum, memberIdList);

            List<String> classEnrollFormIdList = new ArrayList<>();

            for (ClassAttendRecord classAttendRecord : classAttendRecordList) {

                classEnrollFormIdList.add(classAttendRecord.getClassEnrollFormId());
            }

            List<ClassEnrollForm> classEnrollFormList =
                    classEnrollFormRepo.getMemberClassListByClassEnrollFormIdList(classEnrollFormIdList);

            // 出席率標準
            List<String> attendMarkList = new ArrayList<>();
            attendMarkList.add("V");
            attendMarkList.add("L");
            attendMarkList.add("E");

            for (ClassEnrollForm classEnrollForm : classEnrollFormList) {

                ClassAttendRecord mClassAttendRecord = classAttendRecordList.stream()
                        .filter(a -> a.getClassEnrollFormId().equals(classEnrollForm.getClassEnrollFormId()))
                        .collect(Collectors.toList()).get(0);

                Boolean isAttend = false;

                if (mClassAttendRecord.getAttendMark() != null) {

                    isAttend = attendMarkList.contains(mClassAttendRecord.getAttendMark());
                }

                items.add(new Anonymous007Object(
                        classEnrollForm.getMemberId(),
                        classEnrollForm.getClassEnrollFormId(),
                        classEnrollForm.getAliasName(),
                        classEnrollForm.getCtDharmaName(),
                        classEnrollForm.getMobileNum1(),
                        isAttend
                ));
            }
        }

        return items;
    }

    // 學員報到(不經過登入)
    public Anonymous008Response getAttendRecordIdAndEnrollment(HttpSession session, Anonymous008Request request) {

        String memberId = request.getMemberId();
        String classId = request.getClassId();
        int classWeeksNum = request.getClassWeeksNum();
        String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());
//        String updatorId = request.getUserName();
        String updatorIp = (String) session.getAttribute(UserSession.Keys.UserIP.getValue());
//        String updatorName = request.getUserName();
        String updatorUnitName = (String) session.getAttribute(UserSession.Keys.UnitName2.getValue());
        int attendRate = 0;
        String attendDt = "";

        String errMsg = "success";
        int errCode = 200;

        ClassAttendRecord classAttendRecord = classAttendRecordRepo
                .GetClassAttendRecordByMemberIdAndClassIdAndClassWeeksNum(memberId, classId, classWeeksNum);

        if (classAttendRecord != null) {

            int minutesBeLate = classInfoRepo.GetMinutesBeLateByClassId(classId);

            String attendMark = "V";

            SimpleDateFormat dateFormatInput = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            dateFormatInput.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));
            Date chickInDate = null;

            SimpleDateFormat dateFormatOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            try {
                chickInDate = dateFormatOutput.parse(dateFormatInput.format(new Date()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int nowSec = (int) (chickInDate.getTime() / 1000);
            int classSec = (int) (classAttendRecord.getClassDate().getTime() / 1000);
            int diff = nowSec - (classSec + (minutesBeLate * 60));
//            logger.debug("Now Sec : " + nowSec);
//            logger.debug("Class Sec : " + classSec);
//            logger.debug("minutesBeLate : " + minutesBeLate * 60);
//            logger.debug("diff : " + diff);

            if (diff > 0) {
                attendMark = "L";
            }

            classAttendRecord.setAttendCheckinDtTm(chickInDate);
            classAttendRecord.setAttendMark(attendMark);
            classAttendRecord.setUpdateDtTm(new Date());
//            classAttendRecord.setUpdatorId(updatorId);
            classAttendRecord.setUpdatorIp(updatorIp);
//            classAttendRecord.setUpdatorName(updatorName);
            classAttendRecord.setUpdatorUnitName(updatorUnitName);

            classAttendRecordRepo.save(classAttendRecord);

            // 出席率

            List<String> attendMarkList = classAttendRecordRepo.GetClassAttendMarkListByClassIdAndMemberId(classId, memberId);

            if (attendMarkList.size() > 0) {

                // 出席率標準
                List<String> mAttendMarkList = new ArrayList<>();
                mAttendMarkList.add("V");
                mAttendMarkList.add("L");
                mAttendMarkList.add("E");

                int total = attendMarkList.size();
                int classEnroll = attendMarkList.stream()
                        .filter(a -> mAttendMarkList.contains(a))
                        .collect(Collectors.toList())
                        .size();

                if (total > 0) {
                    attendRate = CommonUtils.PercentageInt(classEnroll, total);
                }
            }

            // 報到時間
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(classAttendRecord.getAttendCheckinDtTm());

            String AttendCheckinDate = "";
            String AttendCheckinTime = "";

            AttendCheckinDate = calendar.get(Calendar.YEAR) + "-" +
                    ((calendar.get(Calendar.MONTH) + 1) < 10 ? "0" + (calendar.get(Calendar.MONTH) + 1) : (calendar.get(Calendar.MONTH) + 1)) + "-" +
                    (calendar.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendar.get(Calendar.DAY_OF_MONTH) : calendar.get(Calendar.DAY_OF_MONTH));
            AttendCheckinTime = (calendar.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + calendar.get(Calendar.HOUR_OF_DAY) : calendar.get(Calendar.HOUR_OF_DAY)) + ":" +
                    (calendar.get(Calendar.MINUTE) < 10 ? "0" + calendar.get(Calendar.MINUTE) : calendar.get(Calendar.MINUTE));

            attendDt = AttendCheckinTime;
        } else {

            errCode = 400;
            errMsg = "memberId、classId與classWeeksNum無法匹配";
            attendRate = 0;
            attendDt = "";
        }

        return new Anonymous008Response(errCode, errMsg, attendRate, attendDt);
    }

    // 取消報到
    public BaseResponse deleteEnrollment(HttpSession session, Anonymous013Request anonymous013Request) {

        String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());
        String updatorIp = (String) session.getAttribute(UserSession.Keys.UserIP.getValue());
        String updatorUnitName = (String) session.getAttribute(UserSession.Keys.UnitName2.getValue());

        String errMsg = "success";
        int errCode = 200;

        ClassAttendRecord classAttendRecord = classAttendRecordRepo
                .GetClassAttendRecordByClassEnrollFormIdAndClassWeeksNum(
                        anonymous013Request.getEnrollFormId(), anonymous013Request.getClassWeeksNum());

        if (classAttendRecord != null) {

            classAttendRecord.setAttendCheckinDtTm(null);
            classAttendRecord.setAttendMark(null);
            classAttendRecord.setUpdateDtTm(new Date());
//            classAttendRecord.setUpdatorId(updatorId);
            classAttendRecord.setUpdatorIp(updatorIp);
//            classAttendRecord.setUpdatorName(updatorName);
            classAttendRecord.setUpdatorUnitName(updatorUnitName);

            classAttendRecordRepo.save(classAttendRecord);
        } else {

            errCode = 400;
            errMsg = "enrollFormId與classWeeksNum無法匹配";
        }

        return new BaseResponse(errCode, errMsg);
    }

    // 請假
    public String takeOff(HttpSession session, Anonymous010Request request) {

        String memberId = request.getMemberId();
        String classId = request.getClassId();
        String note = request.getNote();
        List<Integer> classWeeksNumList = request.getClassWeeksNums();
        String timeZone = (String) session.getAttribute(UserSession.Keys.TimeZone.getValue());
        String updatorId = request.getUserName();
        String updatorIp = (String) session.getAttribute(UserSession.Keys.UserIP.getValue());
        String updatorName = request.getUserName();
        String updatorUnitName = (String) session.getAttribute(UserSession.Keys.UnitName2.getValue());
        String result = null;

        List<ClassAttendRecord> classAttendRecordList = classAttendRecordRepo
                .GetClassAttendRecordListByMemberIdAndClassIdAndClassWeekNumList(memberId, classId, classWeeksNumList);

        boolean update = true;

        for (ClassAttendRecord classAttendRecord : classAttendRecordList) {

            if (classAttendRecord.getAttendMark() != null) {
                update = false;
                result = "上課紀錄重複標記";
                break;
            }
        }

        if (update) {

            SimpleDateFormat dateFormatInput = new SimpleDateFormat("yyyy-MM-dd");
            dateFormatInput.setTimeZone(TimeZone.getTimeZone("GMT" + timeZone));

            ClassEnrollForm classEnrollForm = classEnrollFormRepo.GetClassEnrollFormByMemberIdAndClassId(memberId, classId);

            String mNote = null;

            if (classEnrollForm.getClassEnrollFormNote() == null) {

                mNote = dateFormatInput.format(new Date()) + ":" + note;
            } else {

                mNote = classEnrollForm.getClassEnrollFormNote() + " / " + dateFormatInput.format(new Date()) + ":" + note;
            }

            classEnrollForm.setClassEnrollFormNote(mNote);
            classEnrollForm.setUpdateDtTm(new Date());
            classEnrollForm.setUpdatorId(updatorId);
            classEnrollForm.setUpdatorIp(updatorIp);
            classEnrollForm.setUpdatorName(updatorName);
            classEnrollForm.setUpdatorUnitName(updatorUnitName);

            classEnrollFormRepo.save(classEnrollForm);

            String mAttendMark = null;

            if (note.equals("公假")) {

                mAttendMark = "W";
            } else {

                mAttendMark = "O";
            }

            for (ClassAttendRecord classAttendRecord : classAttendRecordList) {

                classAttendRecord.setAttendMark(mAttendMark);
                classAttendRecord.setUpdateDtTm(new Date());
                classAttendRecord.setUpdatorId(updatorId);
                classAttendRecord.setUpdatorIp(updatorIp);
                classAttendRecord.setUpdatorName(updatorName);
                classAttendRecord.setUpdatorUnitName(updatorUnitName);
                classAttendRecord.setNote(dateFormatInput.format(new Date()) + ":" + note);
            }

            classAttendRecordRepo.saveAll(classAttendRecordList);

            result = "success";
        }

        return result;
    }
}
