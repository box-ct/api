package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain012_1Object;
import tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef002Object;
import tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef002Response;
import tw.org.ctworld.meditation.beans.ListResponse;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.*;
import tw.org.ctworld.meditation.beans.UnitStartUp.Forecast;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

import static tw.org.ctworld.meditation.libs.CommonUtils.*;
import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;
import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2TimeString;

@Service
public class UnitEventsMaintainService {

    private static final Logger logger = LoggerFactory.getLogger(UnitEventsMaintainService.class);

    @Autowired
    private CtEventsMaintainService ctEventsMaintainService;

    @Autowired
    private EventCategoryDefRepo eventCategoryDefRepo;

    @Autowired
    private EventMainRepo eventMainRepo;

    @Autowired
    private EventPgmOcaRepo eventPgmOcaRepo;

    @Autowired
    private EventCategorySpecifiedPgmRecordRepo eventCategorySpecifiedPgmRecordRepo;

    @Autowired
    private EventPgmDefRepo eventPgmDefRepo;

    @Autowired
    private EventPgmUnitRepo eventPgmUnitRepo;

    @Autowired
    private EventUnitInfoRepo eventUnitInfoRepo;

    @Autowired
    private CodeDefRepo codeDefRepo;


    // 取得category_def列表
    public EventCategoryDef002Response getCategoryList(HttpSession session) {

        List<EventCategoryDef002Object> items = eventCategoryDefRepo.GetUnitEventsMainCategoryList();

        return new EventCategoryDef002Response(200, "success", items);
    }

    // 取得精舍 event_main
    public UnitEventsMaintain002Response getEventMainList(HttpSession session) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<UnitEventsMaintain002Object> unitEventsMaintain002ObjectList = eventMainRepo.GetEventMainByUnitId(unitId);

        List<String> eventIds = unitEventsMaintain002ObjectList.stream().map(e -> e.getEventId()).collect(Collectors.toList());

        List<Forecast> forecasts = eventUnitInfoRepo.GetForecastByEventIdList(eventIds);

        for (UnitEventsMaintain002Object item : unitEventsMaintain002ObjectList) {
            Optional<Forecast> forecast = forecasts.stream().filter(f -> f.getEventId().equals(item.getEventId())).findAny();

            if (forecast.isPresent()) {
                item.setPeopleForecast(forecast.get().getForecast());
            }
        }

        return new UnitEventsMaintain002Response(200, "success", unitEventsMaintain002ObjectList);
    }

    // 新增/複製活動 (如果copyId有值，為複製模式)
//    public UnitEventsMaintain003Response addEventMain(HttpSession session,
//                                                      UnitEventsMaintain003Request unitEventsMaintain003Request) {
//
//
//
//        return null;
//    }

    // 取得單取活動資料
    public UnitEventsMaintain004Response getMainEventByEventId(HttpSession session, String eventId) {

        int errCode = 200;
        String errMsg = "success";

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        if (eventMain == null) {

            errCode = 400;
            errMsg = "找不到event main id : " + eventId;
        }

        return new UnitEventsMaintain004Response(errCode, errMsg, eventMain);
    }

    // 修改單筆活動資料 in ct
//    public UnitEventsMaintain005Response editMainEventByEventId(HttpSession session, String eventId,
//                                                                EventMain eventMain) {
//
//        return null;
//    }

    // 刪除單筆活動資料 in ct
//    public BaseResponse deleteMainEventByEventId(HttpSession session, String eventId) {
//
//        return null;
//    }

    // 取得活動pgm_oca列表
    public ListResponse getEventPgmOcaList(String eventId) {

        List<UnitEventsMaintain008Object> ctEventMaintain011ObjectList =
                eventPgmOcaRepo.GetUnitEventPgmOcaListByEventId(eventId);

        return new ListResponse(200, "success", ctEventMaintain011ObjectList);
    }

    // 單筆新增pgm_ocas
    public UnitEventsMaintain008Response addEventPgmOca(HttpSession session, String eventId,
                                                        UnitEventsMaintain008Request request) {

        int errCode = 200;
        String errMsg = "success";

        UnitEventsMaintain008Object importedItem = null;

        EventMain eventMain = eventMainRepo.GetClassEventMainByEventId(eventId);

        int pgmUiOrderNumCount = 0;

        if (request.getPgmUiOrderNum() != null) {
            pgmUiOrderNumCount = eventPgmOcaRepo.CheckIfPgmUiOrderNumExist(eventId, "", request.getPgmUiOrderNum(), ParseDate(request.getPgmDate()));
        }

        if (eventMain == null) {
            errCode = 400;
            errMsg = "找不到event main id : " + eventId;
        } else if (pgmUiOrderNumCount > 0) {
            errCode = 400;
            errMsg = "排序碼{" + request.getPgmUiOrderNum() + "}已經存在，請更換順序後再繼續。";
        } else {
            String[] units = eventMain.getAttendUnitList().split("/");

            String pgmId = request.getPgmId();

            List<EventPgmUnit> eventPgmUnitList = new ArrayList<>();

            List<CodeDef> codeDefs = codeDefRepo.GetUnitEventPgms(pgmId);

            logger.debug(pgmId + ":" + codeDefs.size());

            if (codeDefs.size() <= 0) {
                errCode = 400;
                errMsg = "找不到pgm id : " + pgmId;
            } else {

                CodeDef codeDef = codeDefs.get(0);

                String pgmUniqueId = ctEventsMaintainService.EventPgmOcaPgmUniqueIdListGenerate("U", 1).get(0);

                Date pgmDate = ParseDate(request.getPgmDate());

                EventPgmOca eventPgmOca = new EventPgmOca();
                eventPgmOca.setPgmUniqueId(pgmUniqueId);
                eventPgmOca.setEventId(eventMain.getEventId());
                eventPgmOca.setEventName(eventMain.getEventName());
                eventPgmOca.setEventCreatorUnitId(eventMain.getEventCreatorUnitId());
                eventPgmOca.setEventCreatorUnitName(eventMain.getEventCreatorUnitName());
                eventPgmOca.setSponsorUnitId(eventMain.getSponsorUnitId());
                eventPgmOca.setSponsorUnitName(eventMain.getSponsorUnitName());
                eventPgmOca.setEventCategoryId(eventMain.getEventCategoryId());
                eventPgmOca.setEventCategoryName(eventMain.getEventCategoryName());

                eventPgmOca.setPgmId(codeDef.getCodeId());
                eventPgmOca.setPgmCategoryName(codeDef.getCodeName());
                eventPgmOca.setPgmName(request.getPgmName());
                eventPgmOca.setPgmNote(request.getPgmNote());
                eventPgmOca.setPgmEndTime(CommonUtils.ParseTime(request.getPgmEndTime()));
                eventPgmOca.setPgmStartTime(CommonUtils.ParseTime(request.getPgmStartTime()));
                eventPgmOca.setPgmUiOrderNum(request.getPgmUiOrderNum());
                eventPgmOca.setPgmDate(ParseDate(request.getPgmDate()));

                eventPgmOca.setIsRequired(false);
                eventPgmOca.setIsDisabledByOCA(false);
                eventPgmOca.setPgmDate(pgmDate);
                eventPgmOca.setCreatorAndUpdater(session);

                eventPgmOcaRepo.save(eventPgmOca);

                for (String unit : units) {

                    try {
                        String unitId = unit.substring(0, 9);
                        String unitName = unit.substring(10);

                        EventPgmUnit eventPgmUnit = new EventPgmUnit();
                        eventPgmUnit.setEventId(eventMain.getEventId());
                        eventPgmUnit.setEventName(eventMain.getEventName());
                        eventPgmUnit.setEventCreatorUnitId(eventMain.getEventCreatorUnitId());
                        eventPgmUnit.setEventCreatorUnitName(eventMain.getEventCreatorUnitName());
                        eventPgmUnit.setSponsorUnitId(eventMain.getSponsorUnitId());
                        eventPgmUnit.setSponsorUnitName(eventMain.getSponsorUnitName());
                        eventPgmUnit.setEventCategoryId(eventMain.getEventCategoryId());
                        eventPgmUnit.setEventCategoryName(eventMain.getEventCategoryName());
                        eventPgmUnit.setPgmId(eventPgmOca.getPgmId());
                        eventPgmUnit.setPgmName(eventPgmOca.getPgmName());
                        eventPgmUnit.setPgmDate(eventPgmOca.getPgmDate());
                        eventPgmUnit.setPgmCategoryName(eventPgmOca.getPgmCategoryName());
                        eventPgmUnit.setPgmNote(eventPgmOca.getPgmNote());
                        eventPgmUnit.setPgmEndTime(eventPgmOca.getPgmEndTime());
                        eventPgmUnit.setPgmStartTime(eventPgmOca.getPgmStartTime());

                        eventPgmUnit.setPgmUniqueId(eventPgmOca.getPgmUniqueId());
                        eventPgmUnit.setIsRequired(false);
                        eventPgmUnit.setIsDisabledByOCA(false);
                        eventPgmUnit.setUnitId(unitId);
                        eventPgmUnit.setUnitName(unitName);
                        eventPgmUnit.setCreatorAndUpdater(session);

                        eventPgmUnitList.add(eventPgmUnit);
                    } catch (Exception e) {
                        logger.debug(e.getMessage());
                    }
                }

                eventPgmUnitRepo.saveAll(eventPgmUnitList);

                importedItem = new UnitEventsMaintain008Object(
                        eventPgmOca.getPgmId(),
                        eventPgmOca.getPgmName(),
                        eventPgmOca.getPgmUniqueId(),
                        eventPgmOca.getPgmCategoryName(),
                        eventPgmOca.getPgmDate(),
                        eventPgmOca.getPgmStartTime(),
                        eventPgmOca.getPgmEndTime(),
                        eventPgmOca.getPgmNote(),
                        eventPgmOca.getPgmUiOrderNum()
                );
            }
        }

        return new UnitEventsMaintain008Response(errCode, errMsg, importedItem);
    }

}
