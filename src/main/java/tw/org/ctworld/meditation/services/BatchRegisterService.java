package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.BatchRegister.BatchRegister001Request;
import tw.org.ctworld.meditation.beans.BatchRegister.BatchRegister001Response;
import tw.org.ctworld.meditation.beans.BatchRegister.BatchRegister001_1Object;
import tw.org.ctworld.meditation.beans.Member.MembersObject;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.ClassAttendRecord;
import tw.org.ctworld.meditation.models.ClassDateInfo;
import tw.org.ctworld.meditation.models.ClassEnrollForm;
import tw.org.ctworld.meditation.models.ClassInfo;
import tw.org.ctworld.meditation.repositories.*;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import static tw.org.ctworld.meditation.libs.CommonUtils.Empty2Null;

@Service
public class BatchRegisterService {

    private static final Logger logger = LoggerFactory.getLogger(BatchRegisterService.class);

    @Autowired
    private ClassEnrollFormRepo classEnrollFormRepo;

    @Autowired
    private ClassEnrollFormService classEnrollFormService;

    @Autowired
    private ClassInfoRepo classInfoRepo;

    @Autowired
    private ClassDateInfoRepo classDateInfoRepo;

    @Autowired
    private CtMemberInfoRepo ctMemberInfoRepo;

    @Autowired
    private ClassAttendRecordRepo classAttendRecordRepo;


    // 批次註冊報名
    public BatchRegister001Response batchRegister(HttpSession session, BatchRegister001Request batchRegister001Request) {

        String unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());

        List<BatchRegister001_1Object> registerList = new ArrayList<>();

        List<BatchRegister001_1Object> members = new ArrayList<>();

        // 檢查
        for (BatchRegister001_1Object member : batchRegister001Request.getMembers()) {

            // 報名狀態 status true = 可報名，false = 不可報名
            boolean status = getClassRegisterStatus(member.getMemberId(), member.getClassId(), unitId);

            members.add(new BatchRegister001_1Object(member.getMemberId(), status));

            if (status) {

                registerList.add(new BatchRegister001_1Object(
                        member.getMemberId(), member.getClassId(), member.getClassGroupId(), status));
            }
        }

        if (!batchRegister001Request.getSimulation()) {

            // 報名
            if (registerList.size() != 0) {

                String classId = registerList.get(0).getClassId();

                String classGroupId = registerList.get(0).getClassGroupId();

                ClassInfo classInfo = classInfoRepo.GetClassInfoByClassId(classId);

                List<ClassDateInfo> classDateInfoList = classDateInfoRepo.GetClassDateInfoByClassId(classId);

                for (BatchRegister001_1Object register : registerList) {

                    MembersObject classCtMemberInfo =
                            ctMemberInfoRepo.GetClassCtMemberInfoByMemberId2(register.getMemberId());

                    String gender = classCtMemberInfo.getGender().equals("F") ? "女" : "男";

                    ClassEnrollForm classEnrollForm = new ClassEnrollForm();
                    classEnrollForm.setUnitId((String) session.getAttribute(UserSession.Keys.UnitId.getValue()));
                    classEnrollForm.setUnitName((String) session.getAttribute(UserSession.Keys.UnitName.getValue()));
                    classEnrollForm.setClassEnrollFormId(classEnrollFormService.classEnrollFormIdGenerator());
                    classEnrollForm.setMemberId(classCtMemberInfo.getMemberId());
                    classEnrollForm.setTwIdNum(classCtMemberInfo.getTwIdNum());
                    classEnrollForm.setGender(gender);
                    classEnrollForm.setAliasName(Empty2Null(classCtMemberInfo.getAliasName()));
                    classEnrollForm.setCtDharmaName(Empty2Null(classCtMemberInfo.getCtDharmaName()));
                    classEnrollForm.setAge(classCtMemberInfo.getAge());
                    classEnrollForm.setHomePhoneNum1(Empty2Null(classCtMemberInfo.getHomePhoneNum1()));
                    classEnrollForm.setMobileNum1(Empty2Null(classCtMemberInfo.getMobileNum1()));
                    classEnrollForm.setDsaJobNameList(Empty2Null(classCtMemberInfo.getDsaJobNameList()));
                    classEnrollForm.setAddress(Empty2Null(
                            classCtMemberInfo.getMailingCountry() + classCtMemberInfo.getMailingState() +
                                    classCtMemberInfo.getMailingCity() + classCtMemberInfo.getMailingStreet()));
                    classEnrollForm.setCompanyNameAndJobTitle(Empty2Null(classCtMemberInfo.getCompanyJobTitle()));
                    classEnrollForm.setSchoolNameAndMajor(Empty2Null(
                            classCtMemberInfo.getSchoolName() + classCtMemberInfo.getSchoolMajor()));
                    if (classCtMemberInfo.getBirthDate() == null || classCtMemberInfo.getBirthDate().equals("")) {
                        classEnrollForm.setBirthDate(null);
                    }else {
                        classEnrollForm.setBirthDate(CommonUtils.ParseDate(classCtMemberInfo.getBirthDate()));
                    }
                    classEnrollForm.setClassId(classInfo.getClassId());
                    classEnrollForm.setClassTypeNum(classInfo.getClassTypeNum());
                    classEnrollForm.setClassTypeName(classInfo.getClassTypeName());
                    classEnrollForm.setYear(classInfo.getYear());
                    classEnrollForm.setClassStartDate(classInfo.getClassStartDate());
                    classEnrollForm.setClassEndDate(classInfo.getClassEndDate());
                    classEnrollForm.setClassPeriodNum(classInfo.getClassPeriodNum());
                    classEnrollForm.setClassName(classInfo.getClassName());
                    classEnrollForm.setDayOfWeek(classInfo.getDayOfWeek());
                    classEnrollForm.setClassDesc(classInfo.getClassDesc());
                    classEnrollForm.setClassFullName(classInfo.getClassFullName());
                    classEnrollForm.setClassGroupId(Empty2Null(register.getClassGroupId()));
                    classEnrollForm.setMemberGroupNum(classEnrollFormService.memberGroupNumGenerator(classId, classGroupId)); // 學號
//                    classEnrollForm.setSameGroupMemberName(Empty2Null(members003Request.getSameGroupMemberName()));
//                    classEnrollForm.setCurrentClassIntroducerName(Empty2Null(members003Request.getCurrentClassIntroducerName()));
//                    classEnrollForm.setCurrentClassIntroducerRelationship(Empty2Null(members003Request.getCurrentClassIntroducerRelationship()));
//                    classEnrollForm.setChildContactPersonName(Empty2Null(members003Request.getChildContactPersonName()));
//                    classEnrollForm.setChildContactPersonPhoneNum(Empty2Null(members003Request.getChildContactPersonPhoneNum()));
                    classEnrollForm.setNewReturnChangeMark("新報名");
                    classEnrollForm.setNewReturnChangeDate(classEnrollFormService.getLocalDate());
                    classEnrollForm.setChangeAndWaitPrintMark(true);
                    classEnrollForm.setUserDefinedLabel1(false);
                    classEnrollForm.setUserDefinedLabel2(false);
                    classEnrollForm.setAsistantMasterName(classInfo.getAssistantMasterName());
                    classEnrollForm.setCreatorAndUpdater(session);

                    if (classDateInfoList.size() != 0) {

                        for (ClassDateInfo classDateInfo : classDateInfoList) {

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                            Date classDate = null;
                            try {
                                classDate = dateFormat.parse(classDateInfo.getClassDate().toString().split(" ")[0] + " " + classDateInfo.getClassStartTime());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            ClassAttendRecord classAttendRecord = new ClassAttendRecord(
                                    classDateInfo.getUnitId(),
                                    classDateInfo.getUnitName(),
                                    classEnrollForm.getClassEnrollFormId(),
                                    classEnrollForm.getMemberId(),
                                    classEnrollForm.getGender(),
                                    classEnrollForm.getAliasName(),
                                    classEnrollForm.getCtDharmaName(),
                                    classEnrollForm.getClassId(),
                                    classEnrollForm.getClassPeriodNum(),
                                    classEnrollForm.getClassName(),
                                    classEnrollForm.getClassGroupId(),
                                    classDateInfo.getClassWeeksNum(),
                                    classDate,
                                    null,
                                    null,
                                    null,
                                    null
                            );

                            classAttendRecord.setCreatorAndUpdater(session);

                            classAttendRecordRepo.save(classAttendRecord);
                        }
                    }

                    classEnrollFormRepo.save(classEnrollForm);
//                    logger.error("classEnrollFormId : " + classEnrollFormRepo.save(classEnrollForm).getClassEnrollFormId());
                }
            }
        }

        BatchRegister001Response batchRegister001Response = new BatchRegister001Response(
                200,
                "success",
                members
        );

        return batchRegister001Response;
    }

    // 檢查報名狀態 status true = 可報名，false = 不可報名
    public boolean getClassRegisterStatus(String memberId, String classId, String unitId) {

        boolean status;

        List<ClassEnrollForm> classEnrollFormList = classEnrollFormRepo.GetClassRegisterStatus(memberId, classId, unitId);

        if (classEnrollFormList.size() != 0) {

            status = false;
        } else {

            status = true;
        }

        return status;
    }

}
