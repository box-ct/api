package tw.org.ctworld.meditation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.org.ctworld.meditation.beans.Classes.Classes005Request;
import tw.org.ctworld.meditation.beans.Unit.Unit003_2Object;
import tw.org.ctworld.meditation.beans.Unit.Unit003Object;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.libs.RedundantException;
import tw.org.ctworld.meditation.models.UnitInfo;
import tw.org.ctworld.meditation.repositories.UnitInfoRepo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service
public class UnitInfoService {

    private static final Logger logger = LoggerFactory.getLogger(UnitInfoService.class);

    @Autowired
    UnitInfoRepo unitInfoRepo;

    public Unit003_2Object getLoginUnits() {

        Unit003_2Object rootUnit003_2Object = new Unit003_2Object("", "root");

        List<String> groups = unitInfoRepo.GetGroups();

        for (String group: groups) {
//            logger.debug(group + "/" + group.length());

            Unit003_2Object unit003_2Object = new Unit003_2Object("", group);

            rootUnit003_2Object.addSubUnit(unit003_2Object);

            List<UnitInfo> units = unitInfoRepo.GetUnitsByGroup(group);

            if (!group.equals(UnitInfo.Group.ABODE.getValue())) {
                for (UnitInfo unit : units) {
//                    logger.debug(unit.getUnitName());

                    unit003_2Object.addSubUnit(new Unit003_2Object(unit.getUnitId(), unit.getUnitName()));

                }

            } else {
                List<String> districts = unitInfoRepo.GetDistricts();


                for (String district : districts) {
//                    logger.debug(district);

                    Unit003_2Object distUnit0032Object = new Unit003_2Object("", district);

                    unit003_2Object.addSubUnit(distUnit0032Object);

                    List<UnitInfo> dUnits = unitInfoRepo.GetUnitsByDistrict(district);

                    for (UnitInfo dUnit : dUnits) {
//                        logger.debug(dUnit.getUnitName());

                        Unit003_2Object subUnit0032Object = new Unit003_2Object(dUnit.getUnitId(), dUnit.getUnitName());

                        distUnit0032Object.addSubUnit(subUnit0032Object);
                    }
                }
            }
        }

        return rootUnit003_2Object;
    }

    public String findUnitGroupByName(String unitName) {
        return unitInfoRepo.FindUnitGroupByName(unitName);
    }

    // setting get unit info order by sort num
    public List<UnitInfo> getAllUnitsSorted() {
        return unitInfoRepo.GetUnits();
    }

    // setting create unit info
    public UnitInfo createUnitInfo(HttpSession session, UnitInfo unitInfo) throws RedundantException {
        if (unitInfoRepo.FindUnitInfoByUnitId(unitInfo.getUnitId()).size() > 0) {
            throw new RedundantException();
        }

        if (session != null) {
            unitInfo.setCreatorAndUpdater(session);
        }

        UnitInfo newInfo = unitInfoRepo.save(unitInfo);

        return newInfo;
    }

    // setting update unit info
    public void updateUnitInfo(HttpSession session, UnitInfo unitInfo) throws NotFoundException {

        List<UnitInfo> infos = unitInfoRepo.FindUnitInfoByUnitId(unitInfo.getUnitId());

        if (infos.size() > 0) {
            UnitInfo info = infos.get(0);
            info.copy(unitInfo);

            info.setUpdater(session);

            unitInfoRepo.save(info);
        } else {
            throw new NotFoundException();
        }
    }

    // setting delete unit info
//    public void deleteUnitInfo(ClassUnitInfo classUnitInfo) {
//        classUnitInfoRepo.delete(classUnitInfo);
//    }

    // setting check if unit id exists
    public boolean isUnitIdUsed(String unitId) {
        if (unitInfoRepo.FindUnitInfoByUnitId(unitId).size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public String getUnitNameById(String unitId) {
        return unitInfoRepo.GetUnitNameById(unitId);
    }

    public List<UnitInfo> getLoginUnitList() {

//        List<Unit003Object> unit003ObjectList = new ArrayList<>();

        List<UnitInfo> unitInfoList = unitInfoRepo.GetLoginUnitList();

        return unitInfoList;
        
//        if (unitInfoList.size() != 0) {
//
//            for (int x = 0; x < unitInfoList.size() ; x++) {
//                UnitInfo unitInfo = unitInfoList.get(x);
//                unit003ObjectList.add(new Unit003Object(
//                        unitInfo.getUnitId(),
//                        unitInfo.getUnitName(),
//                        unitInfo.getGroupName(),
//                        unitInfo.getDistrict(),
//                        unitInfo.getSortNum(),
//                        unitInfo.getTimezone(),
//                        unitInfo.getUnitStartIp(),
//                        unitInfo.getUnitEndIp(),
//                        unitInfo.getIsNoIpLimit(),
//                        unitInfo.getCreateDtTm() == null ? null : unitInfo.getCreateDtTm().toString(),
//                        unitInfo.getCreatorName(),
//                        unitInfo.getCreatorId(),
//                        unitInfo.getCreatorIp(),
//                        unitInfo.getCreatorUnitName(),
//                        unitInfo.getUpdateDtTm() == null ? null : unitInfo.getUpdateDtTm().toString(),
//                        unitInfo.getUpdatorName(),
//                        unitInfo.getUpdatorId(),
//                        unitInfo.getUpdatorIp(),
//                        unitInfo.getUpdatorUnitName(),
//                        unitInfo.getIsOverseasUnit(),
//                        unitInfo.getAbbotName(),
//                        unitInfo.getAbbotEngName(),
//                        unitInfo.getCertUnitEngName(),
//                        unitInfo.getStateName(),
//                        unitInfo.getCertUnitName()));
//            }
//        }
//
//        return unit003ObjectList;
    }


    public List<String> getUnitDistrict() {

        List<String> unitDistrictList = new ArrayList<>();

//        if (classUnitInfoRepo.GetUnitDistrictList().size() != 0) {
//            unitDistrictList = classUnitInfoRepo.GetUnitDistrictList();
//        }

        unitDistrictList.add("基隆市");
        unitDistrictList.add("台北市");
        unitDistrictList.add("新北市");
        unitDistrictList.add("桃竹苗");
        unitDistrictList.add("台中市");
        unitDistrictList.add("彰投地區");
        unitDistrictList.add("雲嘉地區");
        unitDistrictList.add("台南市");
        unitDistrictList.add("高屏地區");
        unitDistrictList.add("東部地區");
        unitDistrictList.add("美國");
        unitDistrictList.add("奧地利");
        unitDistrictList.add("義大利");
        unitDistrictList.add("澳洲");
        unitDistrictList.add("日本");
        unitDistrictList.add("菲律賓");
        unitDistrictList.add("香港");
        unitDistrictList.add("泰國");
        unitDistrictList.add("其他");

        return unitDistrictList;
    }

    public List<String> getUnitGroupName() {

        List<String> unitGroupList = new ArrayList<>();

        if (unitInfoRepo.GetUnitGroupNameList().size() != 0) {
            unitGroupList = unitInfoRepo.GetUnitGroupNameList();
        }

        return unitGroupList;
    }

    public boolean deleteUnit(UnitInfo unit) {
        UnitInfo info = unitInfoRepo.GetClassUnitInfoByUnitId(unit.getUnitId());

        if (info == null) {
            return false;
        } else {
            unitInfoRepo.delete(info);
            return true;
        }
    }

    public List<String> getAllAbodeUnitId() {
        return unitInfoRepo.GetAllAbodeId();
    }

    public List<String> getDomesticAbodeUnitId() {
        return unitInfoRepo.GetDomesticAbodeId();
    }

    public List<String> getOverseaAbodeUnitId() {
        return unitInfoRepo.GetOverseaAbodeId();
    }

    public HttpSession getCurrentlyIp(HttpServletRequest request) {

        String unitId = null;
        String unitName = null;
        String timeZone = null;

        String ip[] = request.getRemoteAddr().split("\\.");

        List<UnitInfo> unitInfoList = unitInfoRepo.GetClassUnitInfoLikeIp(ip[0] + "." + ip[1] + "." + ip[2]);

        for (UnitInfo unitInfo : unitInfoList) {

            String[] unitStartIp = unitInfo.getUnitStartIp().toString().split("\\.");
            String[] unitEndIp = unitInfo.getUnitEndIp().split("\\.");

            if (Integer.parseInt(unitStartIp[3]) <= Integer.parseInt(ip[3]) &&
                    Integer.parseInt(unitEndIp[3]) >= Integer.parseInt(ip[3])) {

                // ip通過
                unitId = unitInfo.getUnitId();
                unitName = unitInfo.getUnitName();
                timeZone = unitInfo.getTimezone();
                break;
            }
        }

        HttpSession session = request.getSession();
        session.setAttribute(UserSession.Keys.UserIP.getValue(), request.getRemoteAddr());
        session.setAttribute(UserSession.Keys.UnitId2.getValue(), unitId);
        session.setAttribute(UserSession.Keys.UnitName2.getValue(), unitName);
        session.setAttribute(UserSession.Keys.TimeZone.getValue(), timeZone);

        return session;
    }

    public void updateUnit(String unitId, Classes005Request request, HttpSession session) {
        try {
            UnitInfo unitInfo = unitInfoRepo.GetClassUnitInfoByUnitId(unitId);

            if (unitInfo != null) {
                unitInfo.setStateName(request.getStateName());
                unitInfo.setCertUnitName(request.getCertUnitName());
                unitInfo.setCertUnitEngName(request.getCertUnitEngName());
                unitInfo.setUpdater(session);
                unitInfoRepo.save(unitInfo);
            }
        } catch (Exception e) {
            logger.debug(e.getMessage());
            e.printStackTrace();
        }
    }

    public String getUnitStateName(String unitId) {
        try {
            UnitInfo unitInfo = unitInfoRepo.GetClassUnitInfoByUnitId(unitId);

            return unitInfo.getStateName();
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return null;
        }
    }

    public UnitInfo getUnit(String unitId) {
        try {
            return unitInfoRepo.GetClassUnitInfoByUnitId(unitId);
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return null;
        }
    }
}
