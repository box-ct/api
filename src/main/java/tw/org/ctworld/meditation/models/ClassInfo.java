package tw.org.ctworld.meditation.models;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name="class_info")
public class ClassInfo extends BaseEntity {

    public enum Status {
        Initial(1, "未啟動"), Appliable(2, "可報名"), Opening(3, "開課中"), Finishing(4, "待結業"), Finished(2, "已結業");

        private final String value;
        private final int index;

        Status(int index, String value) {
            this.index = index;
            this.value = value;
        }

        public String getValue() { return value; }
        public int getIndex() { return index; }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 9)
    private String unitId;
    @Column(length = 30)
    private String unitName;

    @Column(length = 10)
    private String classStatus;
    @Column(length = 13, unique = true)
    private String classId;
    @Column(length = 4)
    private String year;
    private Date classStartDate;
    private Date classEndDate;
    @Column(length = 1)
    private String dayOfWeek;
    @Column(length = 10)
    private String classDayOrNight;
    @Column(columnDefinition = "INT(3)")
    private int classTypeNum;
    @Column(length = 20)
    private String classTypeName;
    @Column(length = 4)
    private String classPeriodNum;
    @Column(length = 30)
    private String className;
    @Column(length = 20)
    private String classLanguage;
    @Column(length = 20)
    private String teachMasterName;
    @Column(length = 30)
    private String assistantMasterName;
    @Column(length = 20)
    private String teachMemberName;
    @Column(length = 20)
    private String classPlace;
    @Column(length = 4)
    private String classTotalNum;
    @Column(columnDefinition = "INT(2)")
    private int absenceCountToFullAttend;
    @Column(columnDefinition = "INT(2)")
    private int absenceCountToGraduate;
    @Column(columnDefinition = "INT(2)")
    private int makeupCountToGraduate;
    @Column(columnDefinition = "INT(2)")
    private int absenceCountToDiligentAward;
    @Column(columnDefinition = "INT(2)")
    private int minMakeupCountToDiligentAward;
    @Column(columnDefinition = "INT(2)")
    private int maxMakeupCountToDiligentAward;
    @Column(columnDefinition = "INT(3)")
    private int minutesBeLate;
    @Column(length = 60)
    private String classFullName;
    @Column(length = 50)
    private String abbotName;
    @Column(length = 50)
    private String abbotEngName;
    @Column(length = 50)
    private String certificateName;
    @Column(length = 50)
    private String certificateEngName;

    private Time classStartTime;
    private Time classEndTime;

    @Column(length = 300)
    private String classDesc;
    @Column(columnDefinition = "TEXT")
    private String classNote;

    public ClassInfo() {
    }

    public ClassInfo(String classId, String classPeriodNum, String className) {
        this.classId = classId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
    }

    public ClassInfo(String classId, String className, String dayOfWeek, String classPeriodNum, String classStatus) {
        this.classId = classId;
        this.className = className;
        this.dayOfWeek = dayOfWeek;
        this.classPeriodNum = classPeriodNum;
        this.classStatus = classStatus;
    }

    public ClassInfo(String unitName, String classDayOrNight, String classTypeName, int classTypeNum, String classPeriodNum,
                     String certificateName, String certificateEngName, String classDesc) {
        this.unitName = unitName;
        this.classDayOrNight = classDayOrNight;
        this.classTypeName = classTypeName;
        this.classTypeNum = classTypeNum;
        this.classPeriodNum = classPeriodNum;
        this.certificateName = certificateName;
        this.certificateEngName = certificateEngName;
        this.classDesc = classDesc;
    }

    public ClassInfo(String classStatus, String classId, String year, Date classStartDate, Date classEndDate,
                     String dayOfWeek, String classDayOrNight, String classTypeName, String classPeriodNum,
                     String className, String classTotalNum, String classDesc) {
        this.classStatus = classStatus;
        this.classId = classId;
        this.year = year;
        this.classStartDate = classStartDate;
        this.classEndDate = classEndDate;
        this.dayOfWeek = dayOfWeek;
        this.classDayOrNight = classDayOrNight;
        this.classTypeName = classTypeName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classTotalNum = classTotalNum;
        this.classDesc = classDesc;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getClassStatus() {
        return classStatus;
    }

    public void setClassStatus(String classStatus) {
        this.classStatus = classStatus;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Date getClassStartDate() {
        return classStartDate;
    }

    public void setClassStartDate(Date classStartDate) {
        this.classStartDate = classStartDate;
    }

    public Date getClassEndDate() {
        return classEndDate;
    }

    public void setClassEndDate(Date classEndDate) {
        this.classEndDate = classEndDate;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getClassDayOrNight() {
        return classDayOrNight;
    }

    public void setClassDayOrNight(String classDayOrNight) {
        this.classDayOrNight = classDayOrNight;
    }

    public int getClassTypeNum() {
        return classTypeNum;
    }

    public void setClassTypeNum(int classTypeNum) {
        this.classTypeNum = classTypeNum;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassLanguage() {
        return classLanguage;
    }

    public void setClassLanguage(String classLanguage) {
        this.classLanguage = classLanguage;
    }

    public String getTeachMasterName() {
        return teachMasterName;
    }

    public void setTeachMasterName(String teachMasterName) {
        this.teachMasterName = teachMasterName;
    }

    public String getAssistantMasterName() {
        return assistantMasterName;
    }

    public void setAssistantMasterName(String assistantMasterName) {
        this.assistantMasterName = assistantMasterName;
    }

    public String getTeachMemberName() {
        return teachMemberName;
    }

    public void setTeachMemberName(String teachMemberName) {
        this.teachMemberName = teachMemberName;
    }

    public String getClassPlace() {
        return classPlace;
    }

    public void setClassPlace(String classPlace) {
        this.classPlace = classPlace;
    }

    public String getClassTotalNum() {
        return classTotalNum;
    }

    public void setClassTotalNum(String classTotalNum) {
        this.classTotalNum = classTotalNum;
    }

    public int getAbsenceCountToFullAttend() {
        return absenceCountToFullAttend;
    }

    public void setAbsenceCountToFullAttend(int absenceCountToFullAttend) {
        this.absenceCountToFullAttend = absenceCountToFullAttend;
    }

    public int getAbsenceCountToGraduate() {
        return absenceCountToGraduate;
    }

    public void setAbsenceCountToGraduate(int absenceCountToGraduate) {
        this.absenceCountToGraduate = absenceCountToGraduate;
    }

    public int getMakeupCountToGraduate() {
        return makeupCountToGraduate;
    }

    public void setMakeupCountToGraduate(int makeupCountToGraduate) {
        this.makeupCountToGraduate = makeupCountToGraduate;
    }

    public int getAbsenceCountToDiligentAward() {
        return absenceCountToDiligentAward;
    }

    public void setAbsenceCountToDiligentAward(int absenceCountToDiligentAward) {
        this.absenceCountToDiligentAward = absenceCountToDiligentAward;
    }

    public int getMinMakeupCountToDiligentAward() {
        return minMakeupCountToDiligentAward;
    }

    public void setMinMakeupCountToDiligentAward(int minMakeupCountToDiligentAward) {
        this.minMakeupCountToDiligentAward = minMakeupCountToDiligentAward;
    }

    public int getMaxMakeupCountToDiligentAward() {
        return maxMakeupCountToDiligentAward;
    }

    public void setMaxMakeupCountToDiligentAward(int maxMakeupCountToDiligentAward) {
        this.maxMakeupCountToDiligentAward = maxMakeupCountToDiligentAward;
    }

    public int getMinutesBeLate() {
        return minutesBeLate;
    }

    public void setMinutesBeLate(int minutesBeLate) {
        this.minutesBeLate = minutesBeLate;
    }

    public String getClassFullName() {
        return classFullName;
    }

    public void setClassFullName(String classFullName) {
        this.classFullName = classFullName;
    }

    public String getAbbotName() {
        return abbotName;
    }

    public void setAbbotName(String abbotName) {
        this.abbotName = abbotName;
    }

    public String getAbbotEngName() {
        return abbotEngName;
    }

    public void setAbbotEngName(String abbotEngName) {
        this.abbotEngName = abbotEngName;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public String getCertificateEngName() {
        return certificateEngName;
    }

    public void setCertificateEngName(String certificateEngName) {
        this.certificateEngName = certificateEngName;
    }

    public Time getClassStartTime() {
        return classStartTime;
    }

    public void setClassStartTime(Time classStartTime) {
        this.classStartTime = classStartTime;
    }

    public Time getClassEndTime() {
        return classEndTime;
    }

    public void setClassEndTime(Time classEndTime) {
        this.classEndTime = classEndTime;
    }

    public String getClassDesc() {
        return classDesc;
    }

    public void setClassDesc(String classDesc) {
        this.classDesc = classDesc;
    }

    public String getClassNote() {
        return classNote;
    }

    public void setClassNote(String classNote) {
        this.classNote = classNote;
    }
}
