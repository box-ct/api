package tw.org.ctworld.meditation.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name="class_attend_mark")
public class ClassAttendMark extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 9)
    private String unitId;
    @Column(length = 30)
    private String unitName;

    @Column(length = 12)
    private String classId;
    @Column(columnDefinition = "INT(3)")
    private int classTypeNum;
    @Column(length = 20)
    private String classTypeName;
    @Column(length = 2)
    private String classPeriodNum;
    @Column(length = 30)
    private String className;
    @Column(length = 10)
    private String attendMark;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isFullAttended;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isGraduated;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isDiligentAward;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isMakeupClass;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public int getClassTypeNum() {
        return classTypeNum;
    }

    public void setClassTypeNum(int classTypeNum) {
        this.classTypeNum = classTypeNum;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }

    public Boolean getIsFullAttended() {
        return isFullAttended == null ? false : isFullAttended;
    }

    public void setIsFullAttended(Boolean fullAttended) {
        isFullAttended = fullAttended;
    }

    public Boolean getIsGraduated() {
        return isGraduated == null ? false : isGraduated;
    }

    public void setIsGraduated(Boolean graduated) {
        isGraduated = graduated;
    }

    public Boolean getIsDiligentAward() {
        return isDiligentAward == null ? false : isDiligentAward;
    }

    public void setIsDiligentAward(Boolean diligentAward) {
        isDiligentAward = diligentAward;
    }

    public Boolean getIsMakeupClass() {
        return isMakeupClass == null ? false : isMakeupClass;
    }

    public void setIsMakeupClass(Boolean makeupClass) {
        isMakeupClass = makeupClass;
    }
}
