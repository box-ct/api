/*
 * Auto Generated
 * Based on name: event_enroll_main
 * Date: 2019-08-15 15:45:46
 */

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain006Request;
import tw.org.ctworld.meditation.beans.EventEnrollMember.ClassEnrollFormShort;
import tw.org.ctworld.meditation.beans.EventEnrollMember.EventEnrollRequest;
import tw.org.ctworld.meditation.libs.CommonUtils;

@Entity
@Table(name = "event_enroll_main")
public class EventEnrollMain extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(columnDefinition = "TEXT")
    private String address;

    private Integer age;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date arriveCtDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date arriveCtTime;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date birthDate;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date bodhiPreceptDate;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date cancelledDate;

    @Column(length = 50)
    private String carLicenseNum1;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date changeEndDate;

    @Column(columnDefinition = "TEXT")
    private String changeNote;

    private Integer classGroupId;

    @Column(length = 50)
    private String classId;

    @Column(length = 50)
    private String classList;

    @Column(length = 50)
    private String className;

    @Column(columnDefinition = "TEXT")
    private String classNameAndJobList;

    private Integer classPeriodNum;

    @Column(length = 50)
    private String classTypeName;

    @Column(length = 50)
    private String classTypeNum;

    @Column(length = 50)
    private String clubJobTitleNow;

    @Column(length = 50)
    private String clubJobTitlePrev;

    @Column(length = 50)
    private String clubName;

    @Column(length = 50)
    private String companyJobTitle;

    @Column(length = 150)
    private String companyName;

    @Column(length = 200)
    private String ctApprovedNote;

    @Column(columnDefinition = "TEXT")
    private String ctDormChangeHistory;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date ctDormEndDate;

    @Column(columnDefinition = "TEXT")
    private String ctDormLatestChange;

    @Column(length = 50)
    private String ctDormPurpose;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date ctDormStartDate;

    @Column(length = 200)
    private String ctNote1;

    @Column(length = 200)
    private String ctNote2;

    @Column(length = 200)
    private String ctNote3;

    @Column(length = 200)
    private String ctNote4;

    @Column(length = 200)
    private String ctNote5;

    @Column(length = 50)
    private String ctRefugeMaster;

    @Column(columnDefinition = "TEXT")
    private String degreeInfo;

    @Column(length = 200)
    private String donationNameList;

    @Column(length = 100)
    private String dormNote;

    @Column(length = 100)
    private String dsaJobNameList;

    @Column(length = 50)
    private String email1;

    @Column(columnDefinition = "TEXT")
    private String enrollChangeHistory;

    @Column(length = 100)
    private String enrollGroupName;

    @Column(columnDefinition = "TEXT")
    private String enrollLatestChange;

    @Column(columnDefinition = "TEXT")
    private String enrollNote;

    private Integer enrollOrderNum;

    @Column(length = 50)
    private String enrollUnitId;

    @Column(length = 50)
    private String enrollUnitName;

    @Column(length = 50)
    private String enrollUserCtDharmaName;

    @Column(length = 50)
    private String enrollUserId;

    @Column(length = 150)
    private String enrollUserName;

    @Column(length = 50)
    private String eventCategoryId;

    @Column(length = 50)
    private String eventCreatorUnitId;

    @Column(length = 50)
    private String eventCreatorUnitName;

    @Column(length = 50, unique = true)
    private String eventFormUid;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @Column(length = 50)
    private String familyLeaderMemberId;

    @Column(length = 150)
    private String familyLeaderName;

    @Column(length = 50, unique = true)
    private String formAutoAddNum;

    @Column(length = 50)
    private String gender;

    @Column(columnDefinition = "TEXT")
    private String healthList;

    @Column(columnDefinition = "DOUBLE(5,2)")
    private Double height;

    @Column(length = 50)
    private String highestPreceptName;

    private Integer homeClassGroupId;

    @Column(length = 50)
    private String homeClassId;

    @Column(length = 50)
    private String homeClassName;

    @Column(length = 100)
    private String homeClassNote;

    private Integer homeClassPeriodNum;

    @Column(length = 50)
    private String homeClassTypeName;

    @Column(length = 50)
    private String homeClassTypeNum;

    @Column(length = 50)
    private String homePhoneNum1;

    @Column(length = 50)
    private String idType1;

    @Column(length = 50)
    private String idType2;

    @Column(length = 150)
    private String introducerName;

    @Column(length = 50)
    private String introducerPhoneNum;

    @Column(length = 50)
    private String introducerRelationship;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isAbbot;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isAbbotSigned;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date isAbbotSignedDt;

    @Column(length = 150)
    private String isAbbotSignedByUserName;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isBodyHealthy;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCancelPrecept5ThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCancelRefugeThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCanceled;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCtDorm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCtApproved;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date isCtApprovedDt;

    @Column(length = 150)
    private String isCtApprovedByUserName;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isEarlyBookCtDorm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isGoCtBySelf;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isMaster;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isMentalHealthy;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedExtraPrecept5CertThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedParkingPass;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedPrecept5RobeThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedTranslator;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNotDorm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isRelativePreceptOrZen7Vlntr;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isRelativeTakePreceptOrZen7;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isSentAfterEnrollEndDate;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeBodhiPrecept;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeExtraPrecept5ThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakePrecept5;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakePrecept5ThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeRefuge;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeRefugeThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTreasurer;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isWaitForCtSync;

    @Column(length = 50)
    private String jobOrder;

    @Column(length = 200)
    private String jobTitle;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date leaveCtDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date leaveCtTime;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date leaveUnitDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date leaveUnitTime;

    @Column(length = 50)
    private String mailingCity;

    @Column(length = 50)
    private String mailingCountry;

    @Column(length = 50)
    private String mailingState;

    @Column(columnDefinition = "TEXT")
    private String mailingStreet;

    @Column(length = 50)
    private String mailingZip;

    @Column(length = 100)
    private String masterDormNote;

    @Column(length = 50)
    private String masterPreceptTypeName;

    @Column(length = 50)
    private String mobileNum1;

    @Column(length = 50)
    private String officePhoneNum1;

    @Column(length = 100)
    private String onThatDayDonation;

    @Column(length = 50)
    private String parentsJobTitle;

    @Column(length = 150)
    private String parentsName;

    @Column(length = 50)
    private String parentsPhoneNum;

    @Column(length = 100)
    private String passportNum;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date precept5Date;

    @Column(length = 50)
    private String preceptOrder;

    private Integer preceptVlntrCount;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date refugeDate;

    private Integer relativeAge;

    @Column(columnDefinition = "TEXT")
    private String relativeMeritList;

    @Column(length = 150)
    private String relativeName;

    @Column(length = 50)
    private String relativeRelationship;

    @Column(length = 100)
    private String relativeTwId;

    @Column(length = 50)
    private String schoolDegree;

    @Column(length = 50)
    private String schoolMajor;

    @Column(length = 50)
    private String schoolName;

    @Column(columnDefinition = "TEXT")
    private String skillList;

    @Column(columnDefinition = "TEXT")
    private String socialTitle;

    @Column(length = 100)
    private String specialDonation;

    @Column(length = 100)
    private String specialDonationCompleteNote;

    private Integer specialDonationSum;

    @Column(length = 50)
    private String sponsorUnitId;

    @Column(length = 50)
    private String sponsorUnitName;

    @Column(length = 50)
    private String summerDonation1;

    private Integer summerDonation1Count;

    private Integer summerDonation1and2Sum;

    @Column(length = 100)
    private String summerDonation2;

    @Column(length = 50)
    private String summerDonationType;

    @Column(length = 100)
    private String summerRelativeMeritList;

    private Integer summerVlntrCount;

    @Column(length = 50)
    private String templateName;

    @Column(columnDefinition = "TEXT")
    private String templateNote;

    @Column(length = 50)
    private String templateUniqueId;

    @Column(length = 50)
    private String translateLanguage;

    @Column(length = 100)
    private String twIdNum;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;

    @Column(length = 150)
    private String urgentContactPersonName1;

    @Column(length = 50)
    private String urgentContactPersonPhoneNum1;

    @Column(length = 50)
    private String urgentContactPersonRelationship1;

    @Column(columnDefinition = "TEXT")
    private String vehicleNumJobTitleList;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date vlntrEndDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date vlntrEndTime;

    @Column(length = 100)
    private String vlntrGroupNameIdList;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date vlntrStartDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date vlntrStartTime;

    @Column(columnDefinition = "DOUBLE(5,2)")
    private Double weight;

    private Integer zen7Count;

    private Integer zen7VlntrCount;


    // not in db
    @Transient
    private String leaderText;
    @Transient
    private String photo;


    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAge() {
        return this.age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getArriveCtDate() {
        return this.arriveCtDate;
    }

    public void setArriveCtDate(Date arriveCtDate) {
        this.arriveCtDate = arriveCtDate;
    }

    public Date getArriveCtTime() {
        return this.arriveCtTime;
    }

    public void setArriveCtTime(Date arriveCtTime) {
        this.arriveCtTime = arriveCtTime;
    }

    public Date getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getBodhiPreceptDate() {
        return this.bodhiPreceptDate;
    }

    public void setBodhiPreceptDate(Date bodhiPreceptDate) {
        this.bodhiPreceptDate = bodhiPreceptDate;
    }

    public Date getCancelledDate() {
        return this.cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getCarLicenseNum1() {
        return this.carLicenseNum1;
    }

    public void setCarLicenseNum1(String carLicenseNum1) {
        this.carLicenseNum1 = carLicenseNum1;
    }

    public Date getChangeEndDate() {
        return this.changeEndDate;
    }

    public void setChangeEndDate(Date changeEndDate) {
        this.changeEndDate = changeEndDate;
    }

    public Integer getClassGroupId() {
        return this.classGroupId;
    }

    public void setClassGroupId(Integer classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getClassId() {
        return this.classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassList() {
        return this.classList;
    }

    public void setClassList(String classList) {
        this.classList = classList;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassNameAndJobList() {
        return this.classNameAndJobList;
    }

    public void setClassNameAndJobList(String classNameAndJobList) {
        this.classNameAndJobList = classNameAndJobList;
    }

    public Integer getClassPeriodNum() {
        return this.classPeriodNum;
    }

    public void setClassPeriodNum(Integer classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassTypeName() {
        return this.classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassTypeNum() {
        return this.classTypeNum;
    }

    public void setClassTypeNum(String classTypeNum) {
        this.classTypeNum = classTypeNum;
    }

    public String getClubJobTitleNow() {
        return this.clubJobTitleNow;
    }

    public void setClubJobTitleNow(String clubJobTitleNow) {
        this.clubJobTitleNow = clubJobTitleNow;
    }

    public String getClubJobTitlePrev() {
        return this.clubJobTitlePrev;
    }

    public void setClubJobTitlePrev(String clubJobTitlePrev) {
        this.clubJobTitlePrev = clubJobTitlePrev;
    }

    public String getClubName() {
        return this.clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getCompanyJobTitle() {
        return this.companyJobTitle;
    }

    public void setCompanyJobTitle(String companyJobTitle) {
        this.companyJobTitle = companyJobTitle;
    }

    public String getCompanyName() {
        return this.companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCtDormChangeHistory() {
        return this.ctDormChangeHistory;
    }

    public void setCtDormChangeHistory(String ctDormChangeHistory) {
        this.ctDormChangeHistory = ctDormChangeHistory;
    }

    public Date getCtDormEndDate() {
        return this.ctDormEndDate;
    }

    public void setCtDormEndDate(Date ctDormEndDate) {
        this.ctDormEndDate = ctDormEndDate;
    }

    public String getCtDormLatestChange() {
        return this.ctDormLatestChange;
    }

    public void setCtDormLatestChange(String ctDormLatestChange) {
        this.ctDormLatestChange = ctDormLatestChange;
    }

    public String getCtDormPurpose() {
        return this.ctDormPurpose;
    }

    public void setCtDormPurpose(String ctDormPurpose) {
        this.ctDormPurpose = ctDormPurpose;
    }

    public Date getCtDormStartDate() {
        return this.ctDormStartDate;
    }

    public void setCtDormStartDate(Date ctDormStartDate) {
        this.ctDormStartDate = ctDormStartDate;
    }

    public String getCtNote1() {
        return this.ctNote1;
    }

    public void setCtNote1(String ctNote1) {
        this.ctNote1 = ctNote1;
    }

    public String getCtNote2() {
        return this.ctNote2;
    }

    public void setCtNote2(String ctNote2) {
        this.ctNote2 = ctNote2;
    }

    public String getCtNote3() {
        return this.ctNote3;
    }

    public void setCtNote3(String ctNote3) {
        this.ctNote3 = ctNote3;
    }

    public String getCtNote4() {
        return this.ctNote4;
    }

    public void setCtNote4(String ctNote4) {
        this.ctNote4 = ctNote4;
    }

    public String getCtNote5() {
        return this.ctNote5;
    }

    public void setCtNote5(String ctNote5) {
        this.ctNote5 = ctNote5;
    }

    public String getCtRefugeMaster() {
        return this.ctRefugeMaster;
    }

    public void setCtRefugeMaster(String ctRefugeMaster) {
        this.ctRefugeMaster = ctRefugeMaster;
    }

    public String getDegreeInfo() {
        return this.degreeInfo;
    }

    public void setDegreeInfo(String degreeInfo) {
        this.degreeInfo = degreeInfo;
    }

    public String getDonationNameList() {
        return this.donationNameList;
    }

    public void setDonationNameList(String donationNameList) {
        this.donationNameList = donationNameList;
    }

    public String getDormNote() {
        return this.dormNote;
    }

    public void setDormNote(String dormNote) {
        this.dormNote = dormNote;
    }

    public String getDsaJobNameList() {
        return this.dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getEmail1() {
        return this.email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEnrollChangeHistory() {
        return this.enrollChangeHistory;
    }

    public void setEnrollChangeHistory(String enrollChangeHistory) {
        this.enrollChangeHistory = enrollChangeHistory;
    }

    public String getEnrollGroupName() {
        return this.enrollGroupName;
    }

    public void setEnrollGroupName(String enrollGroupName) {
        this.enrollGroupName = enrollGroupName;
    }

    public String getEnrollLatestChange() {
        return this.enrollLatestChange;
    }

    public void setEnrollLatestChange(String enrollLatestChange) {
        this.enrollLatestChange = enrollLatestChange;
    }

    public String getEnrollNote() {
        return this.enrollNote;
    }

    public void setEnrollNote(String enrollNote) {
        this.enrollNote = enrollNote;
    }

    public Integer getEnrollOrderNum() {
        return this.enrollOrderNum;
    }

    public void setEnrollOrderNum(Integer enrollOrderNum) {
        this.enrollOrderNum = enrollOrderNum;
    }

    public String getEnrollUnitId() {
        return this.enrollUnitId;
    }

    public void setEnrollUnitId(String enrollUnitId) {
        this.enrollUnitId = enrollUnitId;
    }

    public String getEnrollUnitName() {
        return this.enrollUnitName;
    }

    public void setEnrollUnitName(String enrollUnitName) {
        this.enrollUnitName = enrollUnitName;
    }

    public String getEnrollUserCtDharmaName() {
        return this.enrollUserCtDharmaName;
    }

    public void setEnrollUserCtDharmaName(String enrollUserCtDharmaName) {
        this.enrollUserCtDharmaName = enrollUserCtDharmaName;
    }

    public String getEnrollUserId() {
        return this.enrollUserId;
    }

    public void setEnrollUserId(String enrollUserId) {
        this.enrollUserId = enrollUserId;
    }

    public String getEnrollUserName() {
        return this.enrollUserName;
    }

    public void setEnrollUserName(String enrollUserName) {
        this.enrollUserName = enrollUserName;
    }

    public String getEventCategoryId() {
        return this.eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCreatorUnitId() {
        return this.eventCreatorUnitId;
    }

    public void setEventCreatorUnitId(String eventCreatorUnitId) {
        this.eventCreatorUnitId = eventCreatorUnitId;
    }

    public String getEventCreatorUnitName() {
        return this.eventCreatorUnitName;
    }

    public void setEventCreatorUnitName(String eventCreatorUnitName) {
        this.eventCreatorUnitName = eventCreatorUnitName;
    }

    public String getEventFormUid() {
        return this.eventFormUid;
    }

    public void setEventFormUid(String eventFormUid) {
        this.eventFormUid = eventFormUid;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getFamilyLeaderMemberId() {
        return this.familyLeaderMemberId;
    }

    public void setFamilyLeaderMemberId(String familyLeaderMemberId) {
        this.familyLeaderMemberId = familyLeaderMemberId;
    }

    public String getFamilyLeaderName() {
        return this.familyLeaderName;
    }

    public void setFamilyLeaderName(String familyLeaderName) {
        this.familyLeaderName = familyLeaderName;
    }

    public String getFormAutoAddNum() {
        return this.formAutoAddNum;
    }

    public void setFormAutoAddNum(String formAutoAddNum) {
        this.formAutoAddNum = formAutoAddNum;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHealthList() {
        return this.healthList;
    }

    public void setHealthList(String healthList) {
        this.healthList = healthList;
    }

    public Double getHeight() {
        return this.height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getHighestPreceptName() {
        return this.highestPreceptName;
    }

    public void setHighestPreceptName(String highestPreceptName) {
        this.highestPreceptName = highestPreceptName;
    }

    public Integer getHomeClassGroupId() {
        return this.homeClassGroupId;
    }

    public void setHomeClassGroupId(Integer homeClassGroupId) {
        this.homeClassGroupId = homeClassGroupId;
    }

    public String getHomeClassId() {
        return this.homeClassId;
    }

    public void setHomeClassId(String homeClassId) {
        this.homeClassId = homeClassId;
    }

    public String getHomeClassName() {
        return this.homeClassName;
    }

    public void setHomeClassName(String homeClassName) {
        this.homeClassName = homeClassName;
    }

    public String getHomeClassNote() {
        return this.homeClassNote;
    }

    public void setHomeClassNote(String homeClassNote) {
        this.homeClassNote = homeClassNote;
    }

    public Integer getHomeClassPeriodNum() {
        return this.homeClassPeriodNum;
    }

    public void setHomeClassPeriodNum(Integer homeClassPeriodNum) {
        this.homeClassPeriodNum = homeClassPeriodNum;
    }

    public String getHomeClassTypeName() {
        return this.homeClassTypeName;
    }

    public void setHomeClassTypeName(String homeClassTypeName) {
        this.homeClassTypeName = homeClassTypeName;
    }

    public String getHomeClassTypeNum() {
        return this.homeClassTypeNum;
    }

    public void setHomeClassTypeNum(String homeClassTypeNum) {
        this.homeClassTypeNum = homeClassTypeNum;
    }

    public String getHomePhoneNum1() {
        return this.homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getIdType1() {
        return this.idType1;
    }

    public void setIdType1(String idType1) {
        this.idType1 = idType1;
    }

    public String getIdType2() {
        return this.idType2;
    }

    public void setIdType2(String idType2) {
        this.idType2 = idType2;
    }

    public String getIntroducerName() {
        return this.introducerName;
    }

    public void setIntroducerName(String introducerName) {
        this.introducerName = introducerName;
    }

    public String getIntroducerPhoneNum() {
        return this.introducerPhoneNum;
    }

    public void setIntroducerPhoneNum(String introducerPhoneNum) {
        this.introducerPhoneNum = introducerPhoneNum;
    }

    public String getIntroducerRelationship() {
        return this.introducerRelationship;
    }

    public void setIntroducerRelationship(String introducerRelationship) {
        this.introducerRelationship = introducerRelationship;
    }

    public Boolean getIsAbbot() {
        return this.isAbbot;
    }

    public void setIsAbbot(Boolean isAbbot) {
        this.isAbbot = isAbbot;
    }

    public Boolean getIsAbbotSigned() {
        return this.isAbbotSigned;
    }

    public void setIsAbbotSigned(Boolean isAbbotSigned) {
        this.isAbbotSigned = isAbbotSigned;
    }

    public Boolean getIsBodyHealthy() {
        return this.isBodyHealthy;
    }

    public void setIsBodyHealthy(Boolean isBodyHealthy) {
        this.isBodyHealthy = isBodyHealthy;
    }

    public Boolean getIsCancelPrecept5ThisTime() {
        return this.isCancelPrecept5ThisTime;
    }

    public void setIsCancelPrecept5ThisTime(Boolean isCancelPrecept5ThisTime) {
        this.isCancelPrecept5ThisTime = isCancelPrecept5ThisTime;
    }

    public Boolean getIsCancelRefugeThisTime() {
        return this.isCancelRefugeThisTime;
    }

    public void setIsCancelRefugeThisTime(Boolean isCancelRefugeThisTime) {
        this.isCancelRefugeThisTime = isCancelRefugeThisTime;
    }

    public Boolean getIsCanceled() {
        return this.isCanceled;
    }

    public void setIsCanceled(Boolean isCanceled) {
        this.isCanceled = isCanceled;
    }

    public Boolean getIsCtDorm() {
        return this.isCtDorm;
    }

    public void setIsCtDorm(Boolean isCtDorm) {
        this.isCtDorm = isCtDorm;
    }

    public Boolean getIsCtApproved() {
        return this.isCtApproved;
    }

    public void setIsCtApproved(Boolean isCtApproved) {
        this.isCtApproved = isCtApproved;
    }

    public Boolean getIsEarlyBookCtDorm() {
        return this.isEarlyBookCtDorm;
    }

    public void setIsEarlyBookCtDorm(Boolean isEarlyBookCtDorm) {
        this.isEarlyBookCtDorm = isEarlyBookCtDorm;
    }

    public Boolean getIsGoCtBySelf() {
        return this.isGoCtBySelf;
    }

    public void setIsGoCtBySelf(Boolean isGoCtBySelf) {
        this.isGoCtBySelf = isGoCtBySelf;
    }

    public Boolean getIsMaster() {
        return this.isMaster;
    }

    public void setIsMaster(Boolean isMaster) {
        this.isMaster = isMaster;
    }

    public Boolean getIsMentalHealthy() {
        return this.isMentalHealthy;
    }

    public void setIsMentalHealthy(Boolean isMentalHealthy) {
        this.isMentalHealthy = isMentalHealthy;
    }

    public Boolean getIsNeedExtraPrecept5CertThisTime() {
        return this.isNeedExtraPrecept5CertThisTime;
    }

    public void setIsNeedExtraPrecept5CertThisTime(Boolean isNeedExtraPrecept5CertThisTime) {
        this.isNeedExtraPrecept5CertThisTime = isNeedExtraPrecept5CertThisTime;
    }

    public Boolean getIsNeedParkingPass() {
        return this.isNeedParkingPass;
    }

    public void setIsNeedParkingPass(Boolean isNeedParkingPass) {
        this.isNeedParkingPass = isNeedParkingPass;
    }

    public Boolean getIsNeedPrecept5RobeThisTime() {
        return this.isNeedPrecept5RobeThisTime;
    }

    public void setIsNeedPrecept5RobeThisTime(Boolean isNeedPrecept5RobeThisTime) {
        this.isNeedPrecept5RobeThisTime = isNeedPrecept5RobeThisTime;
    }

    public Boolean getIsNeedTranslator() {
        return this.isNeedTranslator;
    }

    public void setIsNeedTranslator(Boolean isNeedTranslator) {
        this.isNeedTranslator = isNeedTranslator;
    }

    public Boolean getIsNotDorm() {
        return this.isNotDorm;
    }

    public void setIsNotDorm(Boolean isNotDorm) {
        this.isNotDorm = isNotDorm;
    }

    public Boolean getIsRelativePreceptOrZen7Vlntr() {
        return this.isRelativePreceptOrZen7Vlntr;
    }

    public void setIsRelativePreceptOrZen7Vlntr(Boolean isRelativePreceptOrZen7Vlntr) {
        this.isRelativePreceptOrZen7Vlntr = isRelativePreceptOrZen7Vlntr;
    }

    public Boolean getIsRelativeTakePreceptOrZen7() {
        return this.isRelativeTakePreceptOrZen7;
    }

    public void setIsRelativeTakePreceptOrZen7(Boolean isRelativeTakePreceptOrZen7) {
        this.isRelativeTakePreceptOrZen7 = isRelativeTakePreceptOrZen7;
    }

    public Boolean getIsSentAfterEnrollEndDate() {
        return this.isSentAfterEnrollEndDate;
    }

    public void setIsSentAfterEnrollEndDate(Boolean isSentAfterEnrollEndDate) {
        this.isSentAfterEnrollEndDate = isSentAfterEnrollEndDate;
    }

    public Boolean getIsTakeBodhiPrecept() {
        return this.isTakeBodhiPrecept;
    }

    public void setIsTakeBodhiPrecept(Boolean isTakeBodhiPrecept) {
        this.isTakeBodhiPrecept = isTakeBodhiPrecept;
    }

    public Boolean getIsTakeExtraPrecept5ThisTime() {
        return this.isTakeExtraPrecept5ThisTime;
    }

    public void setIsTakeExtraPrecept5ThisTime(Boolean isTakeExtraPrecept5ThisTime) {
        this.isTakeExtraPrecept5ThisTime = isTakeExtraPrecept5ThisTime;
    }

    public Boolean getIsTakePrecept5() {
        return this.isTakePrecept5;
    }

    public void setIsTakePrecept5(Boolean isTakePrecept5) {
        this.isTakePrecept5 = isTakePrecept5;
    }

    public Boolean getIsTakePrecept5ThisTime() {
        return this.isTakePrecept5ThisTime;
    }

    public void setIsTakePrecept5ThisTime(Boolean isTakePrecept5ThisTime) {
        this.isTakePrecept5ThisTime = isTakePrecept5ThisTime;
    }

    public Boolean getIsTakeRefuge() {
        return this.isTakeRefuge;
    }

    public void setIsTakeRefuge(Boolean isTakeRefuge) {
        this.isTakeRefuge = isTakeRefuge;
    }

    public Boolean getIsTakeRefugeThisTime() {
        return this.isTakeRefugeThisTime;
    }

    public void setIsTakeRefugeThisTime(Boolean isTakeRefugeThisTime) {
        this.isTakeRefugeThisTime = isTakeRefugeThisTime;
    }

    public Boolean getIsTreasurer() {
        return this.isTreasurer;
    }

    public void setIsTreasurer(Boolean isTreasurer) {
        this.isTreasurer = isTreasurer;
    }

    public Boolean getIsWaitForCtSync() {
        return this.isWaitForCtSync;
    }

    public void setIsWaitForCtSync(Boolean isWaitForCtSync) {
        this.isWaitForCtSync = isWaitForCtSync;
    }

    public String getJobOrder() {
        return this.jobOrder;
    }

    public void setJobOrder(String jobOrder) {
        this.jobOrder = jobOrder;
    }

    public String getJobTitle() {
        return this.jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Date getLeaveCtDate() {
        return this.leaveCtDate;
    }

    public void setLeaveCtDate(Date leaveCtDate) {
        this.leaveCtDate = leaveCtDate;
    }

    public Date getLeaveCtTime() {
        return this.leaveCtTime;
    }

    public void setLeaveCtTime(Date leaveCtTime) {
        this.leaveCtTime = leaveCtTime;
    }

    public Date getLeaveUnitDate() {
        return this.leaveUnitDate;
    }

    public void setLeaveUnitDate(Date leaveUnitDate) {
        this.leaveUnitDate = leaveUnitDate;
    }

    public Date getLeaveUnitTime() {
        return this.leaveUnitTime;
    }

    public void setLeaveUnitTime(Date leaveUnitTime) {
        this.leaveUnitTime = leaveUnitTime;
    }

    public String getMailingCity() {
        return this.mailingCity;
    }

    public void setMailingCity(String mailingCity) {
        this.mailingCity = mailingCity;
    }

    public String getMailingCountry() {
        return this.mailingCountry;
    }

    public void setMailingCountry(String mailingCountry) {
        this.mailingCountry = mailingCountry;
    }

    public String getMailingState() {
        return this.mailingState;
    }

    public void setMailingState(String mailingState) {
        this.mailingState = mailingState;
    }

    public String getMailingStreet() {
        return this.mailingStreet;
    }

    public void setMailingStreet(String mailingStreet) {
        this.mailingStreet = mailingStreet;
    }

    public String getMailingZip() {
        return this.mailingZip;
    }

    public void setMailingZip(String mailingZip) {
        this.mailingZip = mailingZip;
    }

    public String getMasterDormNote() {
        return this.masterDormNote;
    }

    public void setMasterDormNote(String masterDormNote) {
        this.masterDormNote = masterDormNote;
    }

    public String getMasterPreceptTypeName() {
        return this.masterPreceptTypeName;
    }

    public void setMasterPreceptTypeName(String masterPreceptTypeName) {
        this.masterPreceptTypeName = masterPreceptTypeName;
    }

    public String getMobileNum1() {
        return this.mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getOfficePhoneNum1() {
        return this.officePhoneNum1;
    }

    public void setOfficePhoneNum1(String officePhoneNum1) {
        this.officePhoneNum1 = officePhoneNum1;
    }

    public String getOnThatDayDonation() {
        return this.onThatDayDonation;
    }

    public void setOnThatDayDonation(String onThatDayDonation) {
        this.onThatDayDonation = onThatDayDonation;
    }

    public String getParentsJobTitle() {
        return this.parentsJobTitle;
    }

    public void setParentsJobTitle(String parentsJobTitle) {
        this.parentsJobTitle = parentsJobTitle;
    }

    public String getParentsName() {
        return this.parentsName;
    }

    public void setParentsName(String parentsName) {
        this.parentsName = parentsName;
    }

    public String getParentsPhoneNum() {
        return this.parentsPhoneNum;
    }

    public void setParentsPhoneNum(String parentsPhoneNum) {
        this.parentsPhoneNum = parentsPhoneNum;
    }

    public String getPassportNum() {
        return this.passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public Date getPrecept5Date() {
        return this.precept5Date;
    }

    public void setPrecept5Date(Date precept5Date) {
        this.precept5Date = precept5Date;
    }

    public String getPreceptOrder() {
        return this.preceptOrder;
    }

    public void setPreceptOrder(String preceptOrder) {
        this.preceptOrder = preceptOrder;
    }

    public Integer getPreceptVlntrCount() {
        return this.preceptVlntrCount;
    }

    public void setPreceptVlntrCount(Integer preceptVlntrCount) {
        this.preceptVlntrCount = preceptVlntrCount;
    }

    public Date getRefugeDate() {
        return this.refugeDate;
    }

    public void setRefugeDate(Date refugeDate) {
        this.refugeDate = refugeDate;
    }

    public Integer getRelativeAge() {
        return this.relativeAge;
    }

    public void setRelativeAge(Integer relativeAge) {
        this.relativeAge = relativeAge;
    }

    public String getRelativeMeritList() {
        return this.relativeMeritList;
    }

    public void setRelativeMeritList(String relativeMeritList) {
        this.relativeMeritList = relativeMeritList;
    }

    public String getRelativeName() {
        return this.relativeName;
    }

    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName;
    }

    public String getRelativeRelationship() {
        return this.relativeRelationship;
    }

    public void setRelativeRelationship(String relativeRelationship) {
        this.relativeRelationship = relativeRelationship;
    }

    public String getRelativeTwId() {
        return this.relativeTwId;
    }

    public void setRelativeTwId(String relativeTwId) {
        this.relativeTwId = relativeTwId;
    }

    public String getSchoolDegree() {
        return this.schoolDegree;
    }

    public void setSchoolDegree(String schoolDegree) {
        this.schoolDegree = schoolDegree;
    }

    public String getSchoolMajor() {
        return this.schoolMajor;
    }

    public void setSchoolMajor(String schoolMajor) {
        this.schoolMajor = schoolMajor;
    }

    public String getSchoolName() {
        return this.schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSkillList() {
        return this.skillList;
    }

    public void setSkillList(String skillList) {
        this.skillList = skillList;
    }

    public String getSocialTitle() {
        return this.socialTitle;
    }

    public void setSocialTitle(String socialTitle) {
        this.socialTitle = socialTitle;
    }

    public String getSpecialDonation() {
        return this.specialDonation;
    }

    public void setSpecialDonation(String specialDonation) {
        this.specialDonation = specialDonation;
    }

    public String getSpecialDonationCompleteNote() {
        return this.specialDonationCompleteNote;
    }

    public void setSpecialDonationCompleteNote(String specialDonationCompleteNote) {
        this.specialDonationCompleteNote = specialDonationCompleteNote;
    }

    public Integer getSpecialDonationSum() {
        return this.specialDonationSum;
    }

    public void setSpecialDonationSum(Integer specialDonationSum) {
        this.specialDonationSum = specialDonationSum;
    }

    public String getSponsorUnitId() {
        return this.sponsorUnitId;
    }

    public void setSponsorUnitId(String sponsorUnitId) {
        this.sponsorUnitId = sponsorUnitId;
    }

    public String getSponsorUnitName() {
        return this.sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getSummerDonation1() {
        return this.summerDonation1;
    }

    public void setSummerDonation1(String summerDonation1) {
        this.summerDonation1 = summerDonation1;
    }

    public Integer getSummerDonation1Count() {
        return this.summerDonation1Count;
    }

    public void setSummerDonation1Count(Integer summerDonation1Count) {
        this.summerDonation1Count = summerDonation1Count;
    }

    public Integer getSummerDonation1and2Sum() {
        return this.summerDonation1and2Sum;
    }

    public void setSummerDonation1and2Sum(Integer summerDonation1and2Sum) {
        this.summerDonation1and2Sum = summerDonation1and2Sum;
    }

    public String getSummerDonation2() {
        return this.summerDonation2;
    }

    public void setSummerDonation2(String summerDonation2) {
        this.summerDonation2 = summerDonation2;
    }

    public String getSummerDonationType() {
        return this.summerDonationType;
    }

    public void setSummerDonationType(String summerDonationType) {
        this.summerDonationType = summerDonationType;
    }

    public String getSummerRelativeMeritList() {
        return this.summerRelativeMeritList;
    }

    public void setSummerRelativeMeritList(String summerRelativeMeritList) {
        this.summerRelativeMeritList = summerRelativeMeritList;
    }

    public Integer getSummerVlntrCount() {
        return this.summerVlntrCount;
    }

    public void setSummerVlntrCount(Integer summerVlntrCount) {
        this.summerVlntrCount = summerVlntrCount;
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateNote() {
        return templateNote;
    }

    public void setTemplateNote(String templateNote) {
        this.templateNote = templateNote;
    }

    public String getTemplateUniqueId() {
        return this.templateUniqueId;
    }

    public void setTemplateUniqueId(String templateUniqueId) {
        this.templateUniqueId = templateUniqueId;
    }

    public String getTranslateLanguage() {
        return this.translateLanguage;
    }

    public void setTranslateLanguage(String translateLanguage) {
        this.translateLanguage = translateLanguage;
    }

    public String getTwIdNum() {
        return this.twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getUnitId() {
        return this.unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return this.unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUrgentContactPersonName1() {
        return this.urgentContactPersonName1;
    }

    public void setUrgentContactPersonName1(String urgentContactPersonName1) {
        this.urgentContactPersonName1 = urgentContactPersonName1;
    }

    public String getUrgentContactPersonPhoneNum1() {
        return this.urgentContactPersonPhoneNum1;
    }

    public void setUrgentContactPersonPhoneNum1(String urgentContactPersonPhoneNum1) {
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
    }

    public String getUrgentContactPersonRelationship1() {
        return this.urgentContactPersonRelationship1;
    }

    public void setUrgentContactPersonRelationship1(String urgentContactPersonRelationship1) {
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
    }

    public String getVehicleNumJobTitleList() {
        return this.vehicleNumJobTitleList;
    }

    public void setVehicleNumJobTitleList(String vehicleNumJobTitleList) {
        this.vehicleNumJobTitleList = vehicleNumJobTitleList;
    }

    public Date getVlntrEndDate() {
        return this.vlntrEndDate;
    }

    public void setVlntrEndDate(Date vlntrEndDate) {
        this.vlntrEndDate = vlntrEndDate;
    }

    public Date getVlntrEndTime() {
        return this.vlntrEndTime;
    }

    public void setVlntrEndTime(Date vlntrEndTime) {
        this.vlntrEndTime = vlntrEndTime;
    }

    public String getVlntrGroupNameIdList() {
        return this.vlntrGroupNameIdList;
    }

    public void setVlntrGroupNameIdList(String vlntrGroupNameIdList) {
        this.vlntrGroupNameIdList = vlntrGroupNameIdList;
    }

    public Date getVlntrStartDate() {
        return this.vlntrStartDate;
    }

    public void setVlntrStartDate(Date vlntrStartDate) {
        this.vlntrStartDate = vlntrStartDate;
    }

    public Date getVlntrStartTime() {
        return this.vlntrStartTime;
    }

    public void setVlntrStartTime(Date vlntrStartTime) {
        this.vlntrStartTime = vlntrStartTime;
    }

    public Double getWeight() {
        return this.weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getZen7Count() {
        return this.zen7Count;
    }

    public void setZen7Count(Integer zen7Count) {
        this.zen7Count = zen7Count;
    }

    public Integer getZen7VlntrCount() {
        return this.zen7VlntrCount;
    }

    public void setZen7VlntrCount(Integer zen7VlntrCount) {
        this.zen7VlntrCount = zen7VlntrCount;
    }

    public String getChangeNote() {
        return changeNote;
    }

    public void setChangeNote(String changeNote) {
        this.changeNote = changeNote;
    }

    public String getLeaderText() {
        return leaderText;
    }

    public void setLeaderText(String leaderText) {
        this.leaderText = leaderText;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCtApprovedNote() {
        return ctApprovedNote;
    }

    public void setCtApprovedNote(String ctApprovedNote) {
        this.ctApprovedNote = ctApprovedNote;
    }

    public Date getIsAbbotSignedDt() {
        return isAbbotSignedDt;
    }

    public void setIsAbbotSignedDt(Date isAbbotSignedDt) {
        this.isAbbotSignedDt = isAbbotSignedDt;
    }

    public String getIsAbbotSignedByUserName() {
        return isAbbotSignedByUserName;
    }

    public void setIsAbbotSignedByUserName(String isAbbotSignedByUserName) {
        this.isAbbotSignedByUserName = isAbbotSignedByUserName;
    }

    public Date getIsCtApprovedDt() {
        return isCtApprovedDt;
    }

    public void setIsCtApprovedDt(Date isCtApprovedDt) {
        this.isCtApprovedDt = isCtApprovedDt;
    }

    public String getIsCtApprovedByUserName() {
        return isCtApprovedByUserName;
    }

    public void setIsCtApprovedByUserName(String isCtApprovedByUserName) {
        this.isCtApprovedByUserName = isCtApprovedByUserName;
    }

    // copy functions
    public void copyEventMain(CtEventMaintain006Request ctEventMaintain006Request) {
        this.eventCategoryId = ctEventMaintain006Request.getEventCategoryId();
        this.eventCreatorUnitId = ctEventMaintain006Request.getEventCreatorUnitId();
        this.eventCreatorUnitName = ctEventMaintain006Request.getEventCreatorUnitName();
        this.eventId = ctEventMaintain006Request.getEventId();
        this.eventName = ctEventMaintain006Request.getEventName();
        this.sponsorUnitId = ctEventMaintain006Request.getSponsorUnitId();
        this.sponsorUnitName = ctEventMaintain006Request.getSponsorUnitName();
    }

    public void copyEnroll(EventEnrollRequest request, boolean isBatch) {

        if (!isBatch) {
            this.address = CommonUtils.Empty2Null(request.getAddress()); //*
            this.age = request.getAge(); //*
            this.birthDate = CommonUtils.ParseDate(request.getBirthDate()); //*
            this.bodhiPreceptDate = CommonUtils.ParseDate(request.getBodhiPreceptDate());
            this.carLicenseNum1 = CommonUtils.Empty2Null(request.getCarLicenseNum1()); //*
            this.classGroupId = CommonUtils.String2Integer(request.getClassGroupId()); //*
            this.classId = CommonUtils.Empty2Null(request.getClassId()); //*
            this.classList = CommonUtils.Empty2Null(request.getClassList());
            this.className = CommonUtils.Empty2Null(request.getClassName()); //*
            this.classNameAndJobList = CommonUtils.Empty2Null(request.getClassNameAndJobList()); //*
            this.classPeriodNum = request.getClassPeriodNum(); //*
            this.classTypeName = CommonUtils.Empty2Null(request.getClassTypeName()); //*
            this.classTypeNum = String.valueOf(request.getClassTypeNum()); //*
            this.clubJobTitleNow = CommonUtils.Empty2Null(request.getClubJobTitleNow());
            this.clubJobTitlePrev = CommonUtils.Empty2Null(request.getClubJobTitlePrev());
            this.clubName = CommonUtils.Empty2Null(request.getClubName());
            this.companyJobTitle = CommonUtils.Empty2Null(request.getCompanyJobTitle());
            this.companyName = CommonUtils.Empty2Null(request.getCompanyName());
            this.ctDormChangeHistory = CommonUtils.Empty2Null(request.getCtDormChangeHistory());
            this.ctRefugeMaster = CommonUtils.Empty2Null(request.getCtRefugeMaster());
            this.degreeInfo = CommonUtils.Empty2Null(request.getDegreeInfo()); //*
            this.donationNameList = CommonUtils.Empty2Null(request.getDonationNameList()); //*
            this.dsaJobNameList = CommonUtils.Empty2Null(request.getDsaJobNameList()); //*
            this.email1 = CommonUtils.Empty2Null(request.getEmail1());
            this.familyLeaderMemberId = CommonUtils.Empty2Null(request.getFamilyLeaderMemberId()); //*
            this.familyLeaderName = CommonUtils.Empty2Null(request.getFamilyLeaderName()); //*
            this.gender = CommonUtils.Empty2Null(request.getGender()); //*
            this.healthList = CommonUtils.Empty2Null(request.getHealthList()); //*
            this.height = request.getHeight(); //*
            this.highestPreceptName = CommonUtils.Empty2Null(request.getHighestPreceptName()); //*
            this.homeClassId = CommonUtils.Empty2Null(request.getHomeClassId()); //*
            this.homeClassName = CommonUtils.Empty2Null(request.getClassName()); //*
            this.homeClassNote = CommonUtils.Empty2Null(request.getHomeClassNote()); //*

            this.homePhoneNum1 = CommonUtils.Empty2Null(request.getHomePhoneNum1());
            this.introducerName = CommonUtils.Empty2Null(request.getIntroducerName());
            this.introducerPhoneNum = request.getIntroducerPhoneNum();
            this.introducerRelationship = CommonUtils.Empty2Null(request.getIntroducerRelationship());
            this.isAbbot = request.getIsAbbot();
            this.mailingCity = CommonUtils.Empty2Null(request.getMailingCity());
            this.mailingCountry = CommonUtils.Empty2Null(request.getMailingCountry());
            this.mailingState = CommonUtils.Empty2Null(request.getMailingState());
            this.mailingStreet = CommonUtils.Empty2Null(request.getMailingStreet());
            this.mailingZip = CommonUtils.Empty2Null(request.getMailingZip());
            this.mobileNum1 = CommonUtils.Empty2Null(request.getMobileNum1());
            this.officePhoneNum1 = CommonUtils.Empty2Null(request.getOfficePhoneNum1());
            this.parentsJobTitle = CommonUtils.Empty2Null(request.getParentsJobTitle()); //*
            this.parentsName = CommonUtils.Empty2Null(request.getParentsName()); //*
            this.parentsPhoneNum = CommonUtils.Empty2Null(request.getParentsPhoneNum()); //*
            this.passportNum = CommonUtils.Empty2Null(request.getPassportNum()); //*
            this.precept5Date = CommonUtils.ParseDate(request.getPrecept5Date());
            this.preceptVlntrCount = request.getPreceptVlntrCount();
            this.refugeDate = CommonUtils.ParseDate(request.getRefugeDate());
            this.relativeMeritList = CommonUtils.Empty2Null(request.getRelativeMeritList()); //*
            this.schoolDegree = CommonUtils.Empty2Null(request.getSchoolDegree());
            this.schoolMajor = CommonUtils.Empty2Null(request.getSchoolMajor());
            this.schoolName = CommonUtils.Empty2Null(request.getSchoolName());
            this.skillList = CommonUtils.Empty2Null(request.getSkillList());
            this.summerVlntrCount = request.getSummerVlntrCount();
            this.twIdNum = CommonUtils.Empty2Null(request.getTwIdNum()); //*
            this.urgentContactPersonName1 = CommonUtils.Empty2Null(request.getUrgentContactPersonName1()); //*
            this.urgentContactPersonPhoneNum1 = CommonUtils.Empty2Null(request.getUrgentContactPersonPhoneNum1()); //*
            this.urgentContactPersonRelationship1 = CommonUtils.Empty2Null(request.getUrgentContactPersonRelationship1()); //*
            this.vlntrGroupNameIdList = CommonUtils.Empty2Null(request.getVlntrGroupNameIdList()); //*
            this.weight = request.getWeight(); //*
            this.zen7Count = request.getZen7Count();
            this.zen7VlntrCount = request.getZen7VlntrCount();

            this.enrollUserCtDharmaName = CommonUtils.Empty2Null(request.getEnrollUserCtDharmaName()); //*
            this.enrollUserId = CommonUtils.Empty2Null(request.getEnrollUserId()); //*
            this.enrollUserName = CommonUtils.Empty2Null(request.getEnrollUserName()); //*
        }

        this.arriveCtDate = CommonUtils.ParseDate(request.getArriveCtDate());
        this.arriveCtTime = CommonUtils.ParseTime(request.getArriveCtTime());
        this.cancelledDate = CommonUtils.ParseDate(request.getCancelledDate());
        this.ctDormEndDate = CommonUtils.ParseDate(request.getCtDormEndDate());
        this.ctDormLatestChange = CommonUtils.Empty2Null(request.getCtDormLatestChange());
        this.ctDormPurpose = CommonUtils.Empty2Null(request.getCtDormPurpose());
        this.ctDormStartDate = CommonUtils.ParseDate(request.getCtDormStartDate());
        this.ctNote1 = CommonUtils.Empty2Null(request.getCtNote1());
        this.ctNote2 = CommonUtils.Empty2Null(request.getCtNote2());
        this.ctNote3 = CommonUtils.Empty2Null(request.getCtNote3());
        this.ctNote4 = CommonUtils.Empty2Null(request.getCtNote4());
        this.ctNote5 = CommonUtils.Empty2Null(request.getCtNote5());
        this.dormNote = CommonUtils.Empty2Null(request.getDormNote());
        this.enrollChangeHistory = CommonUtils.Empty2Null(request.getEnrollChangeHistory());
        this.enrollGroupName = CommonUtils.Empty2Null(request.getEnrollGroupName());
        this.enrollLatestChange = CommonUtils.Empty2Null(request.getEnrollLatestChange());
        this.enrollNote = CommonUtils.Empty2Null(request.getEnrollNote());
        this.enrollOrderNum = CommonUtils.String2Integer(request.getEnrollOrderNum()); //*

        this.enrollUnitId = CommonUtils.Empty2Null(request.getEnrollUnitId());//*
        this.enrollUnitName = CommonUtils.Empty2Null(request.getEnrollUnitName());//*

        this.eventCategoryId = CommonUtils.Empty2Null(request.getEventCategoryId()); //*
        this.eventCreatorUnitId = CommonUtils.Empty2Null(request.getEventCreatorUnitId()); //*
        this.eventCreatorUnitName = CommonUtils.Empty2Null(request.getEventCreatorUnitName());  //*
//        this.eventFormUid = CommonUtils.Empty2Null(request.getEventFormUid());
        this.eventId = CommonUtils.Empty2Null(request.getEventId()); //*
        this.eventName = CommonUtils.Empty2Null(request.getEventName()); //*
//        this.formAutoAddNum = request.getFormAutoAddNum();
        this.idType1 = CommonUtils.Empty2Null(request.getIdType1());
        this.idType2 = CommonUtils.Empty2Null(request.getIdType2());
        this.isAbbot = request.getIsAbbot();
        this.isAbbotSigned = request.getIsAbbotSigned(); //*
        this.isCanceled = request.getIsCanceled();
        this.isCancelPrecept5ThisTime = request.getIsCancelPrecept5ThisTime();
        this.isCancelRefugeThisTime = request.getIsCancelRefugeThisTime();
        this.isCtDorm = request.getIsCtDorm();
        this.isEarlyBookCtDorm = request.getIsEarlyBookCtDorm();
        this.isGoCtBySelf = request.getIsGoCtBySelf();
        this.isMaster = request.getIsMaster();
        this.isNeedExtraPrecept5CertThisTime = request.getIsNeedExtraPrecept5CertThisTime();
        this.isNeedParkingPass = request.getIsNeedParkingPass();
        this.isNeedPrecept5RobeThisTime = request.getIsNeedPrecept5RobeThisTime();
        this.isNeedTranslator = request.getIsNeedTranslator();
        this.isNotDorm = request.getIsNotDorm();
        this.isRelativePreceptOrZen7Vlntr = request.getIsRelativePreceptOrZen7Vlntr();
        this.isRelativeTakePreceptOrZen7 = request.getIsRelativeTakePreceptOrZen7();
        this.isSentAfterEnrollEndDate = request.getIsIsSentAfterEnrollEndDate(); //*
        this.isTakeBodhiPrecept = request.getIsTakeBodhiPrecept();
        this.isTakeExtraPrecept5ThisTime = request.getIsTakeExtraPrecept5ThisTime();
        this.isTakePrecept5 = request.getIsTakePrecept5();
        this.isTakePrecept5ThisTime = request.getIsTakePrecept5ThisTime();
        this.isTakeRefuge = request.getIsTakeRefuge();
        this.isTakeRefugeThisTime = request.getIsTakeRefugeThisTime();
        this.isTreasurer = request.getIsTreasurer();
        this.isWaitForCtSync = request.getIsWaitForCtSync();
        this.jobOrder = request.getJobOrder();
        this.jobTitle = CommonUtils.Empty2Null(request.getJobTitle());
        this.leaveCtDate = CommonUtils.ParseDate(request.getLeaveCtDate());
        this.leaveCtTime = CommonUtils.ParseTime(request.getLeaveCtTime());
        this.leaveUnitDate = CommonUtils.ParseDate(request.getLeaveUnitDate());
        this.leaveUnitTime = CommonUtils.ParseTime(request.getLeaveUnitTime());
        this.masterDormNote = CommonUtils.Empty2Null(request.getMasterDormNote()); //*
        this.masterPreceptTypeName = CommonUtils.Empty2Null(request.getMasterPreceptTypeName());
        this.onThatDayDonation = CommonUtils.Empty2Null(request.getOnThatDayDonation()); //*
        this.preceptOrder = request.getPreceptOrder();
        this.relativeAge = request.getRelativeAge(); //*
        this.relativeName = CommonUtils.Empty2Null(request.getRelativeName()); //*
        this.relativeRelationship = CommonUtils.Empty2Null(request.getRelativeRelationship()); //*
        this.relativeTwId = CommonUtils.Empty2Null(request.getRelativeTwId()); //*
        this.socialTitle = CommonUtils.Empty2Null(request.getSocialTitle()); //*
        this.specialDonation = CommonUtils.Empty2Null(request.getSpecialDonation()); //*
        this.specialDonationCompleteNote = CommonUtils.Empty2Null(request.getSpecialDonationCompleteNote()); //*
        this.specialDonationSum = request.getSpecialDonationSum();
        this.sponsorUnitId = CommonUtils.Empty2Null(request.getSponsorUnitId()); //*
        this.sponsorUnitName = CommonUtils.Empty2Null(request.getSponsorUnitName()); //*
        this.summerDonation1 = CommonUtils.Empty2Null(request.getSummerDonation1()); //*
        this.summerDonation1and2Sum = request.getSummerDonation1and2Sum(); //*
        this.summerDonation1Count = request.getSummerDonation1Count(); //*
        this.summerDonation2 = CommonUtils.Empty2Null(request.getSummerDonation2()); //*
        this.summerDonationType = CommonUtils.Empty2Null(request.getSummerDonationType()); //*
        this.summerRelativeMeritList = CommonUtils.Empty2Null(request.getSummerRelativeMeritList()); //*
        this.templateName = CommonUtils.Empty2Null(request.getTemplateName()); //*
        this.templateUniqueId = CommonUtils.Empty2Null(request.getTemplateUniqueId()); //*
        this.templateNote = CommonUtils.Empty2Null(request.getTemplateNote());
        this.translateLanguage = CommonUtils.Empty2Null(request.getTranslateLanguage());

        this.unitId = CommonUtils.Empty2Null(request.getUnitId()); //*
        this.unitName = CommonUtils.Empty2Null(request.getUnitName()); //*

        this.vehicleNumJobTitleList = CommonUtils.Empty2Null(request.getVehicleNumJobTitleList());
        this.vlntrEndDate = CommonUtils.ParseDate(request.getVlntrEndDate());
        this.vlntrEndTime = CommonUtils.ParseTime(request.getVlntrEndTime());
        this.vlntrStartDate = CommonUtils.ParseDate(request.getVlntrStartDate());
        this.vlntrStartTime = CommonUtils.ParseTime(request.getVlntrStartTime());

        this.changeNote = request.getChangeNote();
    }

    public void copyEnroll4BatchUpdate(EventEnrollRequest request) {

        this.templateUniqueId = CommonUtils.Empty2Null(request.getTemplateUniqueId());
        this.templateName = CommonUtils.Empty2Null(request.getTemplateName());

        this.isCanceled = request.getIsCanceled();
        this.cancelledDate = new Date();

        if (request.getLeaveUnitDate() != null) {
            this.leaveUnitDate = CommonUtils.ParseDate(request.getLeaveUnitDate());
        }
        if (request.getLeaveUnitTime() != null) {
            this.leaveUnitTime = CommonUtils.ParseTime(request.getLeaveUnitTime());
        }
        if (request.getArriveCtDate() != null) {
            this.arriveCtDate = CommonUtils.ParseDate(request.getArriveCtDate());
        }
        if (request.getArriveCtTime() != null) {
            this.arriveCtTime = CommonUtils.ParseTime(request.getArriveCtTime());
        }
        if (request.getLeaveCtDate() != null) {
            this.leaveCtDate = CommonUtils.ParseDate(request.getLeaveCtDate());
        }
        if (request.getLeaveCtTime() != null) {
            this.leaveCtTime = CommonUtils.ParseTime(request.getLeaveCtTime());
        }
        if (request.getEnrollGroupName() != null) {
            this.enrollGroupName = request.getEnrollGroupName();
        }
        if (request.getIdType1() != null) {
            this.idType1 = request.getIdType1();
        }
        if (request.getIdType2() != null) {
            this.idType2 = request.getIdType2();
        }
        if (request.getIsGoCtBySelf() != null) {
            this.isGoCtBySelf = request.getIsGoCtBySelf();
        }
        if (request.getVlntrEndDate() != null) {
            this.vlntrEndDate = CommonUtils.ParseDate(request.getVlntrEndDate());
        }
        if (request.getVlntrEndTime() != null) {
            this.vlntrEndTime = CommonUtils.ParseTime(request.getVlntrEndTime());
        }
        if (request.getVlntrStartDate() != null) {
            this.vlntrStartDate = CommonUtils.ParseDate(request.getVlntrStartDate());
        }
        if (request.getVlntrStartTime() != null) {
            this.vlntrStartTime = CommonUtils.ParseTime(request.getVlntrStartTime());
        }
        if (request.getEnrollNote() != null) {
            this.enrollNote = request.getEnrollNote();
        }
        if (request.getChangeNote() != null) {
            this.changeNote = request.getChangeNote();
        }

        this.isNotDorm = request.getIsNotDorm();

        this.isCtDorm = request.getIsCtDorm();

        if (request.getCtDormStartDate() != null) {
            this.ctDormStartDate = CommonUtils.ParseDate(request.getCtDormStartDate());
        }
        if (request.getCtDormEndDate() != null) {
            this.ctDormEndDate = CommonUtils.ParseDate(request.getCtDormEndDate());
        }
        if (request.getDormNote() != null) {
            this.dormNote = request.getDormNote();
        }

        this.isNeedParkingPass = request.getIsNeedParkingPass();

        this.isNeedTranslator = request.getIsNeedTranslator();

        if (request.getTranslateLanguage() != null) {
            this.translateLanguage = request.getTranslateLanguage();
        }

        this.isTakeRefugeThisTime = request.getIsTakeRefugeThisTime();
        this.isCancelRefugeThisTime = request.getIsCancelRefugeThisTime();
        this.isTakePrecept5ThisTime = request.getIsTakePrecept5ThisTime();
        this.isCancelPrecept5ThisTime = request.getIsCancelPrecept5ThisTime();
        this.isTakeExtraPrecept5ThisTime = request.getIsTakeExtraPrecept5ThisTime();
        this.isNeedPrecept5RobeThisTime = request.getIsNeedPrecept5RobeThisTime();
        this.isNeedExtraPrecept5CertThisTime = request.getIsNeedExtraPrecept5CertThisTime();
        this.isEarlyBookCtDorm = request.getIsEarlyBookCtDorm();

        if (request.getCtDormPurpose() != null) {
            this.ctDormPurpose = request.getCtDormPurpose();
        }

        this.isRelativeTakePreceptOrZen7 = request.getIsRelativeTakePreceptOrZen7();
        this.isRelativePreceptOrZen7Vlntr = request.getIsRelativePreceptOrZen7Vlntr();
    }

    public void copyMemberInfo(CtMemberInfo memberInfo) {
//        this.address = memberInfo.getMailingCity() + memberInfo.getMailingStreet();
        this.age = memberInfo.getAge();
        this.birthDate = memberInfo.getBirthDate();
        this.bodhiPreceptDate = CommonUtils.ParseDate(memberInfo.getBodhiPreceptDate());
        this.carLicenseNum1 = memberInfo.getCarLicenseNum1();
        this.classList = memberInfo.getClassList();
        this.clubJobTitleNow = memberInfo.getClubJobTitleNow();
        this.clubJobTitlePrev = memberInfo.getClubJobTitlePrev();
        this.clubName = memberInfo.getClubName();
        this.companyJobTitle = CommonUtils.Empty2Null(memberInfo.getCompanyJobTitle());
        this.companyName = CommonUtils.Empty2Null(memberInfo.getCompanyName());
        this.ctRefugeMaster = memberInfo.getCtRefugeMaster();
        this.donationNameList = memberInfo.getDonationNameList();
        this.email1 = memberInfo.getEmail1();
        this.enrollUserCtDharmaName = CommonUtils.Empty2Null(memberInfo.getCtDharmaName());
        this.enrollUserId = memberInfo.getMemberId();
        this.enrollUserName = memberInfo.getAliasName();
        this.gender = memberInfo.getGender();
        this.healthList = CommonUtils.Empty2Null(memberInfo.getHealthList());
        this.height = memberInfo.getHeight();
        this.homePhoneNum1 = memberInfo.getHomePhoneNum1();
        this.introducerName = memberInfo.getIntroducerName();
        this.introducerPhoneNum = memberInfo.getIntroducerPhoneNum();
        this.introducerRelationship = memberInfo.getIntroducerRelationship();
        this.isBodyHealthy = memberInfo.getIsBodyHealthy();
        this.isMentalHealthy = memberInfo.getIsMentalHealthy();
        this.isTakeBodhiPrecept = memberInfo.getIsTakeBodhiPrecept();
        this.isTakePrecept5 = memberInfo.getIsTakePrecept5();
        this.isTakeRefuge = memberInfo.getIsTakeRefuge();
        this.mailingCity = CommonUtils.Empty2Null(memberInfo.getMailingCity());
        this.mailingCountry = CommonUtils.Empty2Null(memberInfo.getMailingCountry());
        this.mailingState = CommonUtils.Empty2Null(memberInfo.getMailingState());
        this.mailingStreet = CommonUtils.Empty2Null(memberInfo.getMailingStreet());
        this.mailingZip = CommonUtils.Empty2Null(memberInfo.getMailingZip());
        this.mobileNum1 = CommonUtils.Empty2Null(memberInfo.getMobileNum1());
        this.officePhoneNum1 = CommonUtils.Empty2Null(memberInfo.getOfficePhoneNum1());
        this.parentsJobTitle = memberInfo.getParentsJobTitle();
        this.parentsName = memberInfo.getParentsName();
        this.parentsPhoneNum = memberInfo.getParentsPhoneNum();
        this.passportNum = memberInfo.getPassportNum();
        this.precept5Date = CommonUtils.ParseDate(memberInfo.getPrecept5Date());
        this.preceptVlntrCount = memberInfo.getPreceptVlntrCount();
        this.refugeDate = CommonUtils.ParseDate(memberInfo.getRefugeDate());
        this.relativeMeritList = memberInfo.getRelativeMeritList();
        this.schoolDegree = CommonUtils.Empty2Null(memberInfo.getSchoolDegree());
        this.schoolMajor = CommonUtils.Empty2Null(memberInfo.getSchoolMajor());
        this.schoolName = CommonUtils.Empty2Null(memberInfo.getSchoolName());
        this.skillList = CommonUtils.Empty2Null(memberInfo.getSkillList());
        this.summerVlntrCount = memberInfo.getSummerVlntrCount();
        this.twIdNum = memberInfo.getTwIdNum();
        this.urgentContactPersonName1 = CommonUtils.Empty2Null(memberInfo.getUrgentContactPersonName1());
        this.urgentContactPersonPhoneNum1 = CommonUtils.Empty2Null(memberInfo.getUrgentContactPersonPhoneNum1());
        this.urgentContactPersonRelationship1 = CommonUtils.Empty2Null(memberInfo.getUrgentContactPersonRelationship1());
        this.vlntrGroupNameIdList = memberInfo.getVlntrGroupNameIdList();
        this.weight = memberInfo.getWeight();
        this.zen7Count = memberInfo.getZen7Count();
        this.zen7VlntrCount = memberInfo.getZen7VlntrCount();

        this.unitId = memberInfo.getUnitId();
        this.unitName = memberInfo.getUnitName();
    }

    public void copyMasterInfo(UnitMasterInfo masterInfo) {
        this.enrollUserId = masterInfo.getMasterId();
        this.enrollUserName = masterInfo.getMasterName();
        this.isAbbot = masterInfo.getIsAbbot();
        this.isTreasurer = masterInfo.getIsTreasurer();
        this.jobOrder = masterInfo.getJobOrder();
        this.jobTitle = masterInfo.getJobTitle();
        this.masterPreceptTypeName = masterInfo.getMasterPreceptTypeName();
        this.preceptOrder = masterInfo.getPreceptOrder();

        this.unitId = masterInfo.getUnitId();
        this.unitName = masterInfo.getUnitName();
        this.mobileNum1 = masterInfo.getMobileNum();
    }

    public void applyTemplate(EventUnitTemplate template) {
        // initial columns, not changed on reapply

//        this.eventCategoryId = template.getEventCategoryId();
//        this.eventCreatorUnitId = template.getEventCreatorUnitId();
//        this.eventCreatorUnitName = template.getEventCreatorUnitName();
//        this.eventId = template.getEventId();
//        this.eventName = template.getEventName();
//        this.sponsorUnitId = template.getSponsorUnitId();
//        this.sponsorUnitName = template.getSponsorUnitName();
//        this.templateUniqueId = template.getTemplateUniqueId();
//        this.unitId = template.getUnitId();
//        this.unitName = template.getUnitName();

        this.arriveCtDate = template.getArriveCtDate();
        this.arriveCtTime = template.getArriveCtTime();
        this.ctDormEndDate = template.getCtDormEndDate();
        this.ctDormPurpose = template.getCtDormPurpose();
        this.ctDormStartDate = template.getCtDormStartDate();
        this.dormNote = template.getDormNote();
        this.enrollGroupName = template.getEnrollGroupName();
        this.enrollNote = template.getEnrollNote();
        this.idType1 = template.getIdType1();
        this.idType2 = template.getIdType2();
        this.isCancelPrecept5ThisTime = template.getIsCancelPrecept5ThisTime();
        this.isCancelRefugeThisTime = template.getIsCancelRefugeThisTime();
        this.isCtDorm = template.getIsCtDorm();
        this.isEarlyBookCtDorm = template.getIsEarlyBookCtDorm();
        this.isGoCtBySelf = template.getIsGoCtBySelf();
        this.isNeedExtraPrecept5CertThisTime = template.getIsNeedExtraPrecept5CertThisTime();
        this.isNeedPrecept5RobeThisTime = template.getIsNeedPrecept5RobeThisTime();
        this.isNeedTranslator = template.getIsNeedTranslator();
        this.isNotDorm = template.getIsNotDorm();
        this.isRelativePreceptOrZen7Vlntr = template.getIsRelativePreceptOrZen7Vlntr();
        this.isRelativeTakePreceptOrZen7 = template.getIsRelativeTakePreceptOrZen7();
        this.isTakeExtraPrecept5ThisTime = template.getIsTakeExtraPrecept5ThisTime();
        this.isTakePrecept5ThisTime = template.getIsTakePrecept5ThisTime();
        this.isTakeRefugeThisTime = template.getIsTakeRefugeThisTime();
        this.leaveCtDate = template.getLeaveCtDate();
        this.leaveCtTime = template.getLeaveCtTime();
        this.leaveUnitDate = template.getLeaveUnitDate();
        this.leaveUnitTime = template.getLeaveUnitTime();
        this.templateName = template.getTemplateName();
        this.templateNote = template.getTemplateNote();
        this.translateLanguage = template.getTranslateLanguage();
        this.vlntrEndDate = template.getVlntrEndDate();
        this.vlntrEndTime = template.getVlntrEndTime();
        this.vlntrStartDate = template.getVlntrStartDate();
        this.vlntrStartTime = template.getVlntrStartTime();
    }

    public void copyHomeClassInfo(ClassEnrollFormShort enroll) {
        this.homeClassId = CommonUtils.Empty2Null(enroll.getClassId());
        this.homeClassName = CommonUtils.Empty2Null(enroll.getClassName());
        try {
            this.homeClassPeriodNum = enroll.getClassTypeNum() != null ? Integer.valueOf(enroll.getClassTypeNum()) : null;

            this.homeClassGroupId = enroll.getClassGroupId() != null ? Integer.valueOf(enroll.getClassGroupId()) : null;
        } catch (NumberFormatException n) {

        }
        this.homeClassTypeNum = enroll.getClassTypeNum() != null ? String.valueOf(enroll.getClassTypeNum()) : null;

        this.homeClassTypeName = enroll.getClassTypeName();

        this.homeClassNote = this.homeClassName + "-" + this.homeClassPeriodNum + "期";
    }

    public void copyClassInfo(ClassInfo classInfo) {
        this.classId = classInfo.getClassId();
        this.className = classInfo.getClassName();
        this.classPeriodNum = Integer.parseInt(classInfo.getClassPeriodNum());
        this.classTypeNum = String.valueOf(classInfo.getClassTypeNum());
        this.classTypeName = classInfo.getClassTypeName();
    }

    public void copyClassEnrollForm(ClassEnrollForm enrollForm) {
        this.classGroupId = Integer.parseInt(enrollForm.getClassGroupId());
    }

    public void copyEnroll4Master(EventEnrollRequest request, boolean isBatch) {
        //        this.eventFormUid = CommonUtils.Empty2Null(request.getEventFormUid());
        //        this.formAutoAddNum = request.getFormAutoAddNum();

        if (!isBatch) {
            this.enrollUserId = CommonUtils.Empty2Null(request.getEnrollUserId());//*
            this.enrollUserName = CommonUtils.Empty2Null(request.getEnrollUserName());//*
            this.isAbbot = request.getIsAbbot();//*
            this.isTreasurer = request.getIsTreasurer();//*
            this.jobOrder = request.getJobOrder();//*
            this.jobTitle = CommonUtils.Empty2Null(request.getJobTitle());//*
            this.masterPreceptTypeName = CommonUtils.Empty2Null(request.getMasterPreceptTypeName());//*
            this.preceptOrder = request.getPreceptOrder();//*

            this.unitId = CommonUtils.Empty2Null(request.getUnitId());//*
            this.unitName = CommonUtils.Empty2Null(request.getUnitName());//*

            this.mobileNum1 = CommonUtils.Empty2Null(request.getMobileNum1());
        }

        this.arriveCtDate = CommonUtils.ParseDate(request.getArriveCtDate());//*
        this.arriveCtTime = CommonUtils.ParseTime(request.getArriveCtTime());//*
        this.cancelledDate = CommonUtils.ParseDate(request.getCancelledDate());//*
        this.carLicenseNum1 = CommonUtils.Empty2Null(request.getCarLicenseNum1());//*
        this.ctDormEndDate = CommonUtils.ParseDate(request.getCtDormEndDate());//*
        this.ctDormStartDate = CommonUtils.ParseDate(request.getCtDormStartDate());//*
        this.dormNote = CommonUtils.Empty2Null(request.getDormNote());//*
        this.enrollGroupName = CommonUtils.Empty2Null(request.getEnrollGroupName());//*
        this.enrollLatestChange = CommonUtils.Empty2Null(request.getEnrollLatestChange());//*
        this.enrollNote = CommonUtils.Empty2Null(request.getEnrollNote());//*
        this.enrollOrderNum = CommonUtils.String2Integer(request.getEnrollOrderNum());//*
        this.enrollUnitId = CommonUtils.Empty2Null(request.getEnrollUnitId());//*
        this.enrollUnitName = CommonUtils.Empty2Null(request.getEnrollUnitName());//*
        this.eventCategoryId = CommonUtils.Empty2Null(request.getEventCategoryId());//*
        this.eventCreatorUnitId = CommonUtils.Empty2Null(request.getEventCreatorUnitId());//*
        this.eventCreatorUnitName = CommonUtils.Empty2Null(request.getEventCreatorUnitName());//*
        this.eventId = CommonUtils.Empty2Null(request.getEventId());//*
        this.eventName = CommonUtils.Empty2Null(request.getEventName());//*
        this.idType1 = CommonUtils.Empty2Null(request.getIdType1());//*
        this.idType2 = CommonUtils.Empty2Null(request.getIdType2());//*
        this.isAbbotSigned = request.getIsAbbotSigned();//*
        this.isCanceled = request.getIsCanceled();//*
        this.isCtDorm = request.getIsCtDorm();//*
        this.isGoCtBySelf = request.getIsGoCtBySelf();//*
        this.isMaster = true; //request.getIsMaster();//*
        this.isNeedParkingPass = request.getIsNeedParkingPass();//*
        this.isNotDorm = request.getIsNotDorm();//*
        this.isSentAfterEnrollEndDate = request.getIsIsSentAfterEnrollEndDate();//*
        this.leaveCtDate = CommonUtils.ParseDate(request.getLeaveCtDate());//*
        this.leaveCtTime = CommonUtils.ParseTime(request.getLeaveCtTime());//*
        this.leaveUnitDate = CommonUtils.ParseDate(request.getLeaveUnitDate());//*
        this.leaveUnitTime = CommonUtils.ParseTime(request.getLeaveUnitTime());//*
        this.masterDormNote = CommonUtils.Empty2Null(request.getMasterDormNote());//*
        this.preceptOrder = request.getPreceptOrder();//*
        this.sponsorUnitId = CommonUtils.Empty2Null(request.getSponsorUnitId());//*
        this.sponsorUnitName = CommonUtils.Empty2Null(request.getSponsorUnitName());//*
        this.templateName = CommonUtils.Empty2Null(request.getTemplateName());//*
        this.templateUniqueId = CommonUtils.Empty2Null(request.getTemplateUniqueId());//*
    }
}
