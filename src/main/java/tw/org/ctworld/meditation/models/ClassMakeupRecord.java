package tw.org.ctworld.meditation.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="class_makeup_record")
public class ClassMakeupRecord extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String unitName;
    @Column(length = 50)
    private String unitId;
    @Column(length = 50)
    private String memberId;
    @Column(length = 50)
    private String aliasName;
    @Column(length = 50)
    private String classId;
    @Column(length = 50)
    private String className;
    private Date classDate;
    @Column(columnDefinition = "INT(3)")
    private int classWeeksNum;
    private Date startListenDtTm;
    private int latestListenFlag;
    private Date endListenDtTm;
    @Column(columnDefinition = "DOUBLE(3, 2)")
    private double listenCompleteRatio;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isMakeUpClassCompleted;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public Date getStartListenDtTm() {
        return startListenDtTm;
    }

    public void setStartListenDtTm(Date startListenDtTm) {
        this.startListenDtTm = startListenDtTm;
    }

    public int getLatestListenFlag() {
        return latestListenFlag;
    }

    public void setLatestListenFlag(int latestListenFlag) {
        this.latestListenFlag = latestListenFlag;
    }

    public Date getEndListenDtTm() {
        return endListenDtTm;
    }

    public void setEndListenDtTm(Date endListenDtTm) {
        this.endListenDtTm = endListenDtTm;
    }

//    public String getMakeupFileName() {
//        return makeupFileName;
//    }
//
//    public void setMakeupFileName(String makeupFileName) {
//        this.makeupFileName = makeupFileName;
//    }
//
//    public String getMakeupFileId() {
//        return makeupFileId;
//    }
//
//    public void setMakeupFileId(String makeupFileId) {
//        this.makeupFileId = makeupFileId;
//    }

    public double getListenCompleteRatio() {
        return listenCompleteRatio;
    }

    public void setListenCompleteRatio(double listenCompleteRatio) {
        this.listenCompleteRatio = listenCompleteRatio;
    }

    public Boolean isMakeUpClassCompleted() {
        return isMakeUpClassCompleted == null ? false : isMakeUpClassCompleted;
    }

    public void setMakeUpClassCompleted(Boolean makeUpClassCompleted) {
        isMakeUpClassCompleted = makeUpClassCompleted;
    }
}
