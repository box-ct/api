package tw.org.ctworld.meditation.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import tw.org.ctworld.meditation.beans.Sys.ClassUnitRequest;

import javax.persistence.*;

@Entity
@Table(name="unit_info")
public class UnitInfo extends BaseEntity{

    public final static String[] DISTRICTS = {"基隆市", "台北市", "新北市", "桃竹苗", "台中市", "彰化南投", "雲嘉地區", "台南市", "高屏地區",
            "美國", "日本", "香港", "菲律賓", "泰國", "歐洲", "澳洲", "亞洲", "美洲", "其他"};

    public enum Group {
        PERMANENT("本山"), ABODE("精舍");

        private final String value;

        Group(String value) {
            this.value = value;
        }

        public String getValue() { return value; }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String abbotEngName;

    @Column(length = 50)
    private String abbotId;

    @Column(length = 50)
    private String abbotName;

    @Column(length = 50)
    private String certUnitEngName;

    @Column(length = 50)
    private String certUnitName;

    @Column(length = 50)
    private String district;

    @Column(length = 50)
    private String fundUnitId;

    @Column(length = 50)
    private String groupName;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNoIpLimit;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isOverseasUnit;

    @Column(length = 50)
    private String sortNum;

    @Column(length = 50)
    private String stateName;

    @Column(length = 50)
    private String timezone;

    @Column(length = 50)
    private String unitEmail;

    @Column(length = 50)
    private String unitEndIp;

    @Column(length = 50)
    private String unitFaxNum;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitMailingAddress;

    @Column(length = 50)
    private String unitMobileNum;

    @Column(length = 50)
    private String unitName;

    @Column(length = 50)
    private String unitPhoneNum;

    @Column(length = 50)
    private String unitPhoneNumCode;

    @Column(length = 50)
    private String unitStartIp;

    public UnitInfo() {
    }

    public UnitInfo(String unitId, String unitName, String groupName, String district, String sortNum,
                    String timezone, String unitStartIp, String unitEndIp, boolean isNoIpLimit) {
        this.unitId = unitId;
        this.unitName = unitName;
        this.groupName = groupName;
        this.district = district;
        this.sortNum = sortNum;
        this.timezone = timezone;
        this.unitStartIp = unitStartIp;
        this.unitEndIp = unitEndIp;
        this.isNoIpLimit = isNoIpLimit;
    }

    public UnitInfo(ClassUnitRequest request) {
        this.unitId = request.getUnitId();
        this.unitName = request.getUnitName();
        this.groupName = request.getGroupName();
        this.district = request.getDistrict();
        this.sortNum = request.getSortNum();
        this.timezone = request.getTimezone();
        this.unitStartIp = request.getUnitStartIp();
        this.unitEndIp = request.getUnitEndIp();
        this.isNoIpLimit = request.isNoIpLimit();
        this.isOverseasUnit = request.isOverseasUnit();
        this.abbotId = request.getAbbotId();
        this.abbotName = request.getAbbotName();
        this.abbotEngName = request.getAbbotEngName();
        this.certUnitName = request.getCertUnitName();
        this.certUnitEngName = request.getCertUnitEngName();
        this.stateName = request.getStateName();
        this.unitPhoneNum = request.getUnitPhoneNum();
        this.unitFaxNum = request.getUnitFaxNum();
        this.unitEmail = request.getUnitEmail();
        this.unitPhoneNumCode = request.getUnitPhoneNumCode();
        this.unitMobileNum = request.getUnitMobileNum();
        this.unitMailingAddress = request.getUnitMailingAddress();
        this.fundUnitId = request.getFundUnitId();
    }

    public UnitInfo(String unitId, String unitName, String unitStartIp, String unitEndIp, String timezone) {
        this.unitId = unitId;
        this.unitName = unitName;
        this.unitStartIp = unitStartIp;
        this.unitEndIp = unitEndIp;
        this.timezone = timezone;
    }

    public UnitInfo(String unitId, String abbotName, String abbotEngName, String certUnitEngName, String stateName, String certUnitName) {
        this.unitId = unitId;
        this.abbotName = abbotName;
        this.abbotEngName = abbotEngName;
        this.certUnitEngName = certUnitEngName;
        this.stateName = stateName;
        this.certUnitName = certUnitName;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getUnitStartIp() {
        return unitStartIp;
    }

    public void setUnitStartIp(String unitStartIp) {
        this.unitStartIp = unitStartIp;
    }

    public String getUnitEndIp() {
        return unitEndIp;
    }

    public void setUnitEndIp(String unitEndIp) {
        this.unitEndIp = unitEndIp;
    }

    public boolean getIsNoIpLimit() {
        return isNoIpLimit != null ? isNoIpLimit : false;
    }

    public void setIsNoIpLimit(boolean noIpLimit) {
        isNoIpLimit = noIpLimit;
    }

    public String getSortNum() {
        return sortNum;
    }

    public void setSortNum(String sortNum) {
        this.sortNum = sortNum;
    }

    public boolean getIsOverseasUnit() {
        return isOverseasUnit;
    }

    public void setIsOverseasUnit(boolean overseasUnit) {
        isOverseasUnit = overseasUnit;
    }

    public String getAbbotName() {
        return abbotName;
    }

    public void setAbbotName(String abbotName) {
        this.abbotName = abbotName;
    }

    public String getAbbotEngName() {
        return abbotEngName;
    }

    public void setAbbotEngName(String abbotEngName) {
        this.abbotEngName = abbotEngName;
    }

    public String getCertUnitEngName() {
        return certUnitEngName;
    }

    public void setCertUnitEngName(String certUnitEngName) {
        this.certUnitEngName = certUnitEngName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCertUnitName() {
        return certUnitName;
    }

    public void setCertUnitName(String certUnitName) {
        this.certUnitName = certUnitName;
    }

    public String getFundUnitId() {
        return fundUnitId;
    }

    public void setFundUnitId(String fundUnitId) {
        this.fundUnitId = fundUnitId;
    }

    public String getUnitFaxNum() {
        return unitFaxNum;
    }

    public void setUnitFaxNum(String unitFaxNum) {
        this.unitFaxNum = unitFaxNum;
    }

    public String getUnitPhoneNumCode() {
        return unitPhoneNumCode;
    }

    public void setUnitPhoneNumCode(String unitPhoneNumCode) {
        this.unitPhoneNumCode = unitPhoneNumCode;
    }

    public String getUnitMobileNum() {
        return unitMobileNum;
    }

    public void setUnitMobileNum(String unitMobileNum) {
        this.unitMobileNum = unitMobileNum;
    }

    public String getUnitEmail() {
        return unitEmail;
    }

    public void setUnitEmail(String unitEmail) {
        this.unitEmail = unitEmail;
    }

    public String getUnitMailingAddress() {
        return unitMailingAddress;
    }

    public void setUnitMailingAddress(String unitMailingAddress) {
        this.unitMailingAddress = unitMailingAddress;
    }

    public String getUnitPhoneNum() {
        return unitPhoneNum;
    }

    public void setUnitPhoneNum(String unitPhoneNum) {
        this.unitPhoneNum = unitPhoneNum;
    }

    public String getAbbotId() {
        return abbotId;
    }

    public void setAbbotId(String abbotId) {
        this.abbotId = abbotId;
    }

    public void copy(UnitInfo unitInfo) {
        setUnitId(unitInfo.getUnitId());
        setUnitName(unitInfo.getUnitName());
        setGroupName(unitInfo.getGroupName());
        setDistrict(unitInfo.getDistrict());
        setSortNum(unitInfo.getSortNum());
        setTimezone(unitInfo.getTimezone());
        setUnitStartIp(unitInfo.getUnitStartIp());
        setUnitEndIp(unitInfo.getUnitEndIp());
        setIsNoIpLimit(unitInfo.getIsNoIpLimit());
        setIsOverseasUnit(unitInfo.getIsOverseasUnit());
        setAbbotId(unitInfo.getAbbotId());
        setAbbotName(unitInfo.getAbbotName());
        setAbbotEngName(unitInfo.getAbbotEngName());
        setCertUnitEngName(unitInfo.getCertUnitEngName());
        setStateName(unitInfo.getStateName());
        setCertUnitName(unitInfo.getCertUnitName());
        setFundUnitId(unitInfo.getFundUnitId());
        setUnitFaxNum(unitInfo.getUnitFaxNum());
        setUnitPhoneNumCode(unitInfo.getUnitPhoneNumCode());
        setUnitMobileNum(unitInfo.getUnitMobileNum());
        setUnitEmail(unitInfo.getUnitEmail());
        setUnitMailingAddress(unitInfo.getUnitMailingAddress());
        setAbbotId(unitInfo.getAbbotId());
        setUnitPhoneNum(unitInfo.getUnitPhoneNum());
    }
}
