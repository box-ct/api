/*
* Auto Generated
* Based on name: event_unit_tour_info
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="event_unit_tour_info")
public class EventUnitTourInfo extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCanceled;

    @Column(columnDefinition = "Date")
    private Date tourDate;

    @Column(columnDefinition = "Time")
    private Date tourEndTime;

    @Column(columnDefinition = "TEXT")
    private String tourNote;

    @Column(length = 50)
    private String tourPeopleCount;

    @Column(columnDefinition = "Time")
    private Date tourStartTime;

    @Column(length = 50)
    private String tourType;

    private Integer unitId;

    @Column(length = 50)
    private String unitName;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEventid() {
        return this.eventId;
    }

    public void setEventid(String eventId) {
        this.eventId = eventId;
    }

    public String getEventname() {
        return this.eventName;
    }

    public void setEventname(String eventName) {
        this.eventName = eventName;
    }

    public Boolean getIscanceled() {
        return this.isCanceled == null ? false : isCanceled;
    }

    public void setIscanceled(Boolean isCanceled) {
        this.isCanceled = isCanceled;
    }

    public Date getTourdate() {
        return this.tourDate;
    }

    public void setTourdate(Date tourDate) {
        this.tourDate = tourDate;
    }

    public Date getTourendtime() {
        return this.tourEndTime;
    }

    public void setTourendtime(Date tourEndTime) {
        this.tourEndTime = tourEndTime;
    }

    public String getTournote() {
        return this.tourNote;
    }

    public void setTournote(String tourNote) {
        this.tourNote = tourNote;
    }

    public String getTourpeoplecount() {
        return this.tourPeopleCount;
    }

    public void setTourpeoplecount(String tourPeopleCount) {
        this.tourPeopleCount = tourPeopleCount;
    }

    public Date getTourstarttime() {
        return this.tourStartTime;
    }

    public void setTourstarttime(Date tourStartTime) {
        this.tourStartTime = tourStartTime;
    }

    public String getTourtype() {
        return this.tourType;
    }

    public void setTourtype(String tourType) {
        this.tourType = tourType;
    }

    public int getUnitid() {
        return this.unitId;
    }

    public void setUnitid(int unitId) {
        this.unitId = unitId;
    }

    public String getUnitname() {
        return this.unitName;
    }

    public void setUnitname(String unitName) {
        this.unitName = unitName;
    }

}
