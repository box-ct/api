/*
* Auto Generated
* Based on name: event_category_specified_pgm_record
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="event_category_specified_pgm_record")
public class EventCategorySpecifiedPgmRecord extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String eventCategoryId;

    @Column(length = 50)
    private String eventCategoryName;

    @Column(length = 50)
    private String pgmCategoryId;

    @Column(length = 50)
    private String pgmCategoryName;

    @Column(length = 50)
    private String pgmId;

    @Column(length = 50)
    private String pgmName;

    public EventCategorySpecifiedPgmRecord() {

    }

    public EventCategorySpecifiedPgmRecord(String eventCategoryId, String eventCategoryName,
                                           String pgmCategoryId, String pgmCategoryName, String pgmId, String pgmName) {
        this.eventCategoryId = eventCategoryId;
        this.eventCategoryName = eventCategoryName;
        this.pgmCategoryId = pgmCategoryId;
        this.pgmCategoryName = pgmCategoryName;
        this.pgmId = pgmId;
        this.pgmName = pgmName;
    }

    public EventCategorySpecifiedPgmRecord(String eventCategoryId, String pgmId, String pgmName) {
        this.eventCategoryId = eventCategoryId;
        this.pgmId = pgmId;
        this.pgmName = pgmName;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEventCategoryId() {
        return this.eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return this.eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public String getPgmCategoryId() {
        return this.pgmCategoryId;
    }

    public void setPgmCategoryId(String pgmCategoryId) {
        this.pgmCategoryId = pgmCategoryId;
    }

    public String getPgmCategoryName() {
        return this.pgmCategoryName;
    }

    public void setPgmCategoryName(String pgmCategoryName) {
        this.pgmCategoryName = pgmCategoryName;
    }

    public String getPgmId() {
        return this.pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }

    public String getPgmName() {
        return this.pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

}
