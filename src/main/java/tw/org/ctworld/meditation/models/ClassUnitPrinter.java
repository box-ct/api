package tw.org.ctworld.meditation.models;

import javax.persistence.*;

@Entity
@Table(name="class_unit_printer")
public class ClassUnitPrinter extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String unitName;
    @Column(length = 50)
    private String unitId;
    @Column(length = 50)
    private String printerId;
    @Column(length = 50)
    private String printerName;
    @Column(columnDefinition = "TEXT")
    private String note;

    public ClassUnitPrinter() {
    }

    public ClassUnitPrinter(long id, String printerId, String printerName, String note) {
        this.id = id;
        this.printerId = printerId;
        this.printerName = printerName;
        this.note = note;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getPrinterId() {
        return printerId;
    }

    public void setPrinterId(String printerId) {
        this.printerId = printerId;
    }

    public String getPrinterName() {
        return printerName;
    }

    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
