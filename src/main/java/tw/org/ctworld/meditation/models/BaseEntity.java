package tw.org.ctworld.meditation.models;

import tw.org.ctworld.meditation.beans.UserSession;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.servlet.http.HttpSession;
import java.util.Date;

@MappedSuperclass
public abstract class BaseEntity {

    private Date createDtTm;
    @Column(length = 50)
    private String creatorName;
    @Column(length = 50)
    private String creatorId;
    @Column(length = 50)
    private String creatorIp;
    @Column(length = 50)
    private String creatorUnitName;
    @Column(length = 50)
    private String creatorUnitId;

    private Date updateDtTm;
    @Column(length = 50)
    private String updatorName;
    @Column(length = 50)
    private String updatorId;
    @Column(length = 50)
    private String updatorIp;
    @Column(length = 50)
    private String updatorUnitName;
    @Column(length = 50)
    private String updatorUnitId;

    protected BaseEntity() {
    }

    public BaseEntity(BaseEntity baseEntity) {
        this.createDtTm = baseEntity.createDtTm;
        this.creatorName = baseEntity.creatorName;
        this.creatorId = baseEntity.creatorId;
        this.creatorIp = baseEntity.creatorIp;
        this.creatorUnitName = baseEntity.creatorUnitName;
        this.creatorUnitId = baseEntity.creatorUnitId;
        this.updateDtTm = baseEntity.updateDtTm;
        this.updatorName = baseEntity.updatorName;
        this.updatorId = baseEntity.updatorId;
        this.updatorIp = baseEntity.updatorIp;
        this.updatorUnitName = baseEntity.updatorUnitName;
        this.updatorUnitId = baseEntity.updatorUnitId;
    }

    public void setCreatorAndUpdater(HttpSession session) {
        setCreator(session);
        setUpdater(session);
    }

    public void setCreator(HttpSession session) {
        this.createDtTm = new Date();
        this.creatorId = (String)session.getAttribute(UserSession.Keys.UserId.getValue());
        this.creatorIp = (String)session.getAttribute(UserSession.Keys.UserIP.getValue());
        this.creatorName = (String)session.getAttribute(UserSession.Keys.UserName.getValue());
        this.creatorUnitName = (String)session.getAttribute(UserSession.Keys.UnitName.getValue());
        this.creatorUnitId = (String)session.getAttribute(UserSession.Keys.UnitId.getValue());
    }

    public void setUpdater(HttpSession session) {
        this.updateDtTm = new Date();
        this.updatorId = (String)session.getAttribute(UserSession.Keys.UserId.getValue());
        this.updatorIp = (String)session.getAttribute(UserSession.Keys.UserIP.getValue());
        this.updatorName = (String)session.getAttribute(UserSession.Keys.UserName.getValue());
        this.updatorUnitName = (String)session.getAttribute(UserSession.Keys.UnitName.getValue());
        this.updatorUnitId = (String)session.getAttribute(UserSession.Keys.UnitId.getValue());
    }

    public Date getCreateDtTm() {
        return createDtTm;
    }

    public void setCreateDtTm(Date createDtTm) {
        this.createDtTm = createDtTm;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorIp() {
        return creatorIp;
    }

    public void setCreatorIp(String creatorIp) {
        this.creatorIp = creatorIp;
    }

    public String getCreatorUnitName() {
        return creatorUnitName;
    }

    public void setCreatorUnitName(String creatorUnitName) {
        this.creatorUnitName = creatorUnitName;
    }

    public Date getUpdateDtTm() {
        return updateDtTm;
    }

    public void setUpdateDtTm(Date updateDtTm) {
        this.updateDtTm = updateDtTm;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId;
    }

    public String getUpdatorIp() {
        return updatorIp;
    }

    public void setUpdatorIp(String updatorIp) {
        this.updatorIp = updatorIp;
    }

    public String getUpdatorUnitName() {
        return updatorUnitName;
    }

    public void setUpdatorUnitName(String updatorUnitName) {
        this.updatorUnitName = updatorUnitName;
    }

    public String getCreatorUnitId() {
        return creatorUnitId;
    }

    public void setCreatorUnitId(String creatorUnitId) {
        this.creatorUnitId = creatorUnitId;
    }

    public String getUpdatorUnitId() {
        return updatorUnitId;
    }

    public void setUpdatorUnitId(String updatorUnitId) {
        this.updatorUnitId = updatorUnitId;
    }
}
