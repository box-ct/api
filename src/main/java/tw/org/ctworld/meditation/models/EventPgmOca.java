/*
* Auto Generated
* Based on name: event_pgm_oca
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.internal.util.Cloneable;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain006Request;

import static tw.org.ctworld.meditation.libs.CommonUtils.ParseDate;

@Entity
@Table(name="event_pgm_oca")
public class EventPgmOca extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String eventCategoryId;

    @Column(length = 50)
    private String eventCategoryName;

    @Column(length = 50)
    private String eventCreatorUnitId;

    @Column(length = 50)
    private String eventCreatorUnitName;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @Column(columnDefinition = "Date")
    private Date goTransDate;

    @Column(length = 50)
    private String goTransType;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isDisabledByOCA;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isGenPgm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isRequired;

    @Column(length = 50)
    private String pgmCategoryId;

    @Column(length = 50)
    private String pgmCategoryName;

    private Integer pgmCtOrderNum;

    @Column(columnDefinition = "Date")
    private Date pgmDate;

    @Column(length = 50)
    private String pgmDesc;

    @Column(columnDefinition = "Time")
    private Date pgmEndTime;

    @Column(length = 50)
    private String pgmId;

    @Column(length = 50)
    private String pgmName;

    @Column(length = 50)
    private String pgmNote;

    @Column(columnDefinition = "Time")
    private Date pgmStartTime;

    private Integer pgmUiOrderNum;

    @Column(length = 50, unique = true)
    private String pgmUniqueId;

    @Column(columnDefinition = "Date")
    private Date returnTransDate;

    @Column(length = 50)
    private String returnTransType;

    @Column(length = 50)
    private String sponsorUnitId;

    @Column(length = 50)
    private String sponsorUnitName;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEventCategoryId() {
        return this.eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return this.eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public String getEventCreatorUnitId() {
        return this.eventCreatorUnitId;
    }

    public void setEventCreatorUnitId(String eventCreatorUnitId) {
        this.eventCreatorUnitId = eventCreatorUnitId;
    }

    public String getEventCreatorUnitName() {
        return this.eventCreatorUnitName;
    }

    public void setEventCreatorUnitName(String eventCreatorUnitName) {
        this.eventCreatorUnitName = eventCreatorUnitName;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getGoTransDate() {
        return this.goTransDate;
    }

    public void setGoTransDate(Date goTransDate) {
        this.goTransDate = goTransDate;
    }

    public String getGoTransType() {
        return this.goTransType;
    }

    public void setGoTransType(String goTransType) {
        this.goTransType = goTransType;
    }

    public Boolean getIsDisabledByOCA() {
        return this.isDisabledByOCA == null ? false : isDisabledByOCA;
    }

    public void setIsDisabledByOCA(Boolean isDisabledByOCA) {
        this.isDisabledByOCA = isDisabledByOCA;
    }

    public Boolean getIsGenPgm() {
        return this.isGenPgm == null ? false : isGenPgm;
    }

    public void setIsGenPgm(Boolean isGenPgm) {
        this.isGenPgm = isGenPgm;
    }

    public Boolean getIsRequired() {
        return this.isRequired == null ? false : isRequired;
    }

    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public String getPgmCategoryId() {
        return this.pgmCategoryId;
    }

    public void setPgmCategoryId(String pgmCategoryId) {
        this.pgmCategoryId = pgmCategoryId;
    }

    public String getPgmCategoryName() {
        return this.pgmCategoryName;
    }

    public void setPgmCategoryName(String pgmCategoryName) {
        this.pgmCategoryName = pgmCategoryName;
    }

    public Integer getPgmCtOrderNum() {
        return this.pgmCtOrderNum;
    }

    public void setPgmCtOrderNum(Integer pgmCtOrderNum) {
        this.pgmCtOrderNum = pgmCtOrderNum;
    }

    public Date getPgmDate() {
        return this.pgmDate;
    }

    public void setPgmDate(Date pgmDate) {
        this.pgmDate = pgmDate;
    }

    public String getPgmDesc() {
        return this.pgmDesc;
    }

    public void setPgmDesc(String pgmDesc) {
        this.pgmDesc = pgmDesc;
    }

    public Date getPgmEndTime() {
        return this.pgmEndTime;
    }

    public void setPgmEndTime(Date pgmEndTime) {
        this.pgmEndTime = pgmEndTime;
    }

    public String getPgmId() {
        return this.pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }

    public String getPgmName() {
        return this.pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

    public String getPgmNote() {
        return this.pgmNote;
    }

    public void setPgmNote(String pgmNote) {
        this.pgmNote = pgmNote;
    }

    public Date getPgmStartTime() {
        return this.pgmStartTime;
    }

    public void setPgmStartTime(Date pgmStartTime) {
        this.pgmStartTime = pgmStartTime;
    }

    public Integer getPgmUiOrderNum() {
        return this.pgmUiOrderNum;
    }

    public void setPgmUiOrderNum(Integer pgmUiOrderNum) {
        this.pgmUiOrderNum = pgmUiOrderNum;
    }

    public String getPgmUniqueId() {
        return this.pgmUniqueId;
    }

    public void setPgmUniqueId(String pgmUniqueId) {
        this.pgmUniqueId = pgmUniqueId;
    }

    public Date getReturnTransDate() {
        return this.returnTransDate;
    }

    public void setReturnTransDate(Date returnTransDate) {
        this.returnTransDate = returnTransDate;
    }

    public String getReturnTransType() {
        return this.returnTransType;
    }

    public void setReturnTransType(String returnTransType) {
        this.returnTransType = returnTransType;
    }

    public String getSponsorUnitId() {
        return this.sponsorUnitId;
    }

    public void setSponsorUnitId(String sponsorUnitId) {
        this.sponsorUnitId = sponsorUnitId;
    }

    public String getSponsorUnitName() {
        return this.sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }


    public void copyEventMainAndEventPgmDef(EventMain m, EventPgmDef p, String pgmUniqueId, HttpSession session) {
        this.eventCategoryId = m.getEventCategoryId();
        this.eventCategoryName = m.getEventCategoryName();
        this.eventCreatorUnitId = m.getEventCreatorUnitId();
        this.eventCreatorUnitName = m.getEventCreatorUnitName();
        this.eventId = m.getEventId();
        this.eventName = m.getEventName();
        this.goTransDate = null;
        this.goTransType = null;
        this.isDisabledByOCA = false;
        this.isGenPgm = p.getIsGenPgm();
        this.isRequired = false;
        this.pgmCategoryId = p.getPgmCategoryId();
        this.pgmCategoryName = p.getPgmCategoryName();
        this.pgmCtOrderNum = null;
        this.pgmDate = m.getEventStartDate();
        this.pgmDesc = p.getPgmDesc();
        this.pgmEndTime = p.getPgmEndTime();
        this.pgmId = p.getPgmId();
        this.pgmName = p.getPgmName();
        this.pgmNote = p.getPgmNote();
        this.pgmStartTime = p.getPgmStartTime();
        this.pgmUiOrderNum = null;
        this.pgmUniqueId = pgmUniqueId;
        this.returnTransDate = null;
        this.returnTransType = null;
        this.sponsorUnitId = m.getSponsorUnitId();
        this.sponsorUnitName = m.getSponsorUnitName();
        this.setCreator(session);
    }

    public void copyEventMainAndEventPgmDef(CtEventMaintain006Request m, EventPgmDef p, String pgmUniqueId, HttpSession session) {
        this.eventCategoryId = m.getEventCategoryId();
        this.eventCategoryName = m.getEventCategoryName();
        this.eventCreatorUnitId = m.getEventCreatorUnitId();
        this.eventCreatorUnitName = m.getEventCreatorUnitName();
        this.eventId = m.getEventId();
        this.eventName = m.getEventName();
        this.goTransDate = null;
        this.goTransType = null;
        this.isDisabledByOCA = false;
        this.isGenPgm = p.getIsGenPgm();
        this.isRequired = false;
        this.pgmCategoryId = p.getPgmCategoryId();
        this.pgmCategoryName = p.getPgmCategoryName();
        this.pgmCtOrderNum = null;
        this.pgmDate = ParseDate(m.getEventStartDate());
        this.pgmDesc = p.getPgmDesc();
        this.pgmEndTime = p.getPgmEndTime();
        this.pgmId = p.getPgmId();
        this.pgmName = p.getPgmName();
        this.pgmNote = p.getPgmNote();
        this.pgmStartTime = p.getPgmStartTime();
        this.pgmUiOrderNum = null;
        this.pgmUniqueId = pgmUniqueId;
        this.returnTransDate = null;
        this.returnTransType = null;
        this.sponsorUnitId = m.getSponsorUnitId();
        this.sponsorUnitName = m.getSponsorUnitName();
        this.setCreator(session);
    }
    
    public void copy(EventPgmOca oca, EventMain eventMain) {
        
        this.eventCategoryId = eventMain.getEventCategoryId();
        this.eventCategoryName = eventMain.getEventCategoryName();
        this.eventCreatorUnitId = eventMain.getEventCreatorUnitId();
        this.eventCreatorUnitName = eventMain.getEventCreatorUnitName();
        this.eventId = eventMain.getEventId();
        this.eventName = eventMain.getEventName();
        this.goTransDate = eventMain.getEventStartDate();        
        this.goTransType = oca.goTransType;
        this.isDisabledByOCA = oca.isDisabledByOCA;
        this.isGenPgm = oca.isGenPgm;
        this.isRequired = oca.isRequired;
        this.pgmCategoryId = oca.pgmCategoryId;
        this.pgmCategoryName = oca.pgmCategoryName;
        this.pgmCtOrderNum = oca.pgmCtOrderNum;
        this.pgmDesc = oca.pgmDesc;
        this.pgmEndTime = oca.pgmEndTime;
        this.pgmId = oca.pgmId;
        this.pgmName = oca.pgmName;
        this.pgmNote = oca.pgmNote;
        this.pgmStartTime = oca.pgmStartTime;
        this.pgmUiOrderNum = oca.pgmUiOrderNum;
        this.returnTransDate = eventMain.getEventEndDate();
        this.returnTransType = oca.returnTransType;
        this.sponsorUnitId = eventMain.getSponsorUnitId();
        this.sponsorUnitName = eventMain.getSponsorUnitName();

        if (this.isGenPgm == null || !this.isGenPgm) {
            this.pgmDate = eventMain.getEventStartDate();
        }

        if (this.pgmId.equals("PGM10017")) {
            this.pgmDate = null;
        }
    }
}
