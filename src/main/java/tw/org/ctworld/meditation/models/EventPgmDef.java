/*
* Auto Generated
* Based on name: event_pgm_def
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="event_pgm_def")
public class EventPgmDef extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(columnDefinition = "TEXT")
    private String genPgmInputOption;

    @Column(length = 50)
    private String genPgmUiType;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCenterAvailable;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isGenPgm;

    @Column(length = 50)
    private String pgmCategoryId;

    @Column(length = 50)
    private String pgmCategoryName;

    @Column(length = 50)
    private String pgmDesc;

    @Column(columnDefinition = "Time")
    private Date pgmEndTime;

    @Column(length = 50)
    private String pgmId;

    @Column(length = 50)
    private String pgmName;

    @Column(columnDefinition = "TEXT")
    private String pgmNote;

    @Column(columnDefinition = "Time")
    private Date pgmStartTime;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGenPgmInputOption() {
        return this.genPgmInputOption;
    }

    public void setGenPgmInputOption(String genPgmInputOption) {
        this.genPgmInputOption = genPgmInputOption;
    }

    public String getGenPgmUiType() {
        return this.genPgmUiType;
    }

    public void setGenPgmUiType(String genPgmUiType) {
        this.genPgmUiType = genPgmUiType;
    }

    public Boolean getIsCenterAvailable() {
        return this.isCenterAvailable == null ? false : isCenterAvailable;
    }

    public void setIsCenterAvailable(Boolean isCenterAvailable) {
        this.isCenterAvailable = isCenterAvailable;
    }

    public Boolean getIsGenPgm() {
        return this.isGenPgm == null ? false : isGenPgm;
    }

    public void setIsGenPgm(Boolean isGenPgm) {
        this.isGenPgm = isGenPgm;
    }

    public String getPgmCategoryId() {
        return this.pgmCategoryId;
    }

    public void setPgmCategoryId(String pgmCategoryId) {
        this.pgmCategoryId = pgmCategoryId;
    }

    public String getPgmCategoryName() {
        return this.pgmCategoryName;
    }

    public void setPgmCategoryName(String pgmCategoryName) {
        this.pgmCategoryName = pgmCategoryName;
    }

    public String getPgmDesc() {
        return this.pgmDesc;
    }

    public void setPgmDesc(String pgmDesc) {
        this.pgmDesc = pgmDesc;
    }

    public Date getPgmEndTime() {
        return this.pgmEndTime;
    }

    public void setPgmEndTime(Date pgmEndTime) {
        this.pgmEndTime = pgmEndTime;
    }

    public String getPgmId() {
        return this.pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }

    public String getPgmName() {
        return this.pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

    public String getPgmNote() {
        return this.pgmNote;
    }

    public void setPgmNote(String pgmNote) {
        this.pgmNote = pgmNote;
    }

    public Date getPgmStartTime() {
        return this.pgmStartTime;
    }

    public void setPgmStartTime(Date pgmStartTime) {
        this.pgmStartTime = pgmStartTime;
    }

    

    public void copy(EventPgmDef input) {
        this.genPgmInputOption = input.genPgmInputOption;
        this.genPgmUiType = input.genPgmUiType;
        this.isCenterAvailable = input.isCenterAvailable == null ? false : input.isCenterAvailable;
        this.isGenPgm = input.isGenPgm == null ? false : input.isGenPgm;
        this.pgmCategoryId = input.pgmCategoryId;
        this.pgmCategoryName = input.pgmCategoryName;
        this.pgmDesc = input.pgmDesc;
        this.pgmEndTime = input.pgmEndTime;
        this.pgmId = input.pgmId;
        this.pgmName = input.pgmName;
        this.pgmNote = input.pgmNote;
        this.pgmStartTime = input.pgmStartTime;
    }
}
