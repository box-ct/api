package tw.org.ctworld.meditation.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name="class_cert_field_setting")
public class ClassCertFieldSetting extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 50)
    private String unitId;
    @Column(length = 50)
    private String unitName;
    @Column(length = 50)
    private String certTypeName;
    @Column(length = 50)
    private String certTemplateId;
    @Column(length = 50)
    private String certTemplateName;
    @Column(length = 50)
    private String fieldPositionNum;
    @Column(length = 50)
    private String fieldName;
    @Column(length = 50)
    private String fontName;
    private int fontSize;// SA(double)
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isBold;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isItalic;
    @Column(length = 50)
    private String horizontalAlignType;
    @Column(length = 50)
    private String verticalAlignType;
    @Column(length = 50)
    private String textDirection;
    private int width;// SA(double)
    private int height;// SA(double)
    private int yDistance;// SA(double)
    private int xDistance;// SA(double)
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isEnabled;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getCertTypeName() {
        return certTypeName;
    }

    public void setCertTypeName(String certTypeName) {
        this.certTypeName = certTypeName;
    }

    public String getCertTemplateId() {
        return certTemplateId;
    }

    public void setCertTemplateId(String certTemplateId) {
        this.certTemplateId = certTemplateId;
    }

    public String getCertTemplateName() {
        return certTemplateName;
    }

    public void setCertTemplateName(String certTemplateName) {
        this.certTemplateName = certTemplateName;
    }

    public String getFieldPositionNum() {
        return fieldPositionNum;
    }

    public void setFieldPositionNum(String fieldPositionNum) {
        this.fieldPositionNum = fieldPositionNum;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public Boolean getIsBold() {
        return isBold == null ? false : isBold;
    }

    public void setIsBold(Boolean bold) {
        isBold = bold;
    }

    public Boolean getIsItalic() {
        return isItalic == null ? false : isItalic;
    }

    public void setIsItalic(Boolean italic) {
        isItalic = italic;
    }

    public String getHorizontalAlignType() {
        return horizontalAlignType;
    }

    public void setHorizontalAlignType(String horizontalAlignType) {
        this.horizontalAlignType = horizontalAlignType;
    }

    public String getVerticalAlignType() {
        return verticalAlignType;
    }

    public void setVerticalAlignType(String verticalAlignType) {
        this.verticalAlignType = verticalAlignType;
    }

    public String getTextDirection() {
        return textDirection;
    }

    public void setTextDirection(String textDirection) {
        this.textDirection = textDirection;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getyDistance() {
        return yDistance;
    }

    public void setyDistance(int yDistance) {
        this.yDistance = yDistance;
    }

    public int getxDistance() {
        return xDistance;
    }

    public void setxDistance(int xDistance) {
        this.xDistance = xDistance;
    }

    public Boolean getIsEnabled() {
        return isEnabled == null ? false : isEnabled;
    }

    public void setIsEnabled(Boolean enabled) {
        isEnabled = enabled;
    }
}
