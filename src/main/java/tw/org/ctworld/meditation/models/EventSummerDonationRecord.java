/*
* Auto Generated
* Based on name: event_summer_donation_record
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import tw.org.ctworld.meditation.beans.EventEnrollMember.SummerDonationRequest;
import tw.org.ctworld.meditation.libs.CommonUtils;

@Entity
@Table(name="event_summer_donation_record")
public class EventSummerDonationRecord extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String enrollUserId;

    @Column(length = 50)
    private String enrollUserName;

    @Column(length = 200)
    private String museumDonation;

    @Column(length = 50)
    private String museumDonationStatus;

    @Column(length = 200)
    private String onThatDayDonation;

    @Column(length = 50)
    private String otherSpecialDonation;

    @Column(length = 200)
    private String specialDonation;

    @Column(columnDefinition = "TEXT")
    private String specialDonationCompleteNote;

    private Integer specialDonationSum;

    @Column(length = 50)
    private String summerDonation1;

    private Integer summerDonation1Count;

    private Integer summerDonation1and2Sum;

    @Column(length = 200)
    private String summerDonation2;

    @Column(length = 50)
    private String summerDonationType;

    @Column(length = 50)
    private String summerDonationYear;

    @Column(columnDefinition = "TEXT")
    private String summerRelativeMeritList;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEnrollUserId() {
        return enrollUserId;
    }

    public void setEnrollUserId(String enrollUserId) {
        this.enrollUserId = enrollUserId;
    }

    public String getEnrollUserName() {
        return enrollUserName;
    }

    public void setEnrollUserName(String enrollUserName) {
        this.enrollUserName = enrollUserName;
    }

    public String getMuseumDonation() {
        return museumDonation;
    }

    public void setMuseumDonation(String museumDonation) {
        this.museumDonation = museumDonation;
    }

    public String getMuseumDonationStatus() {
        return museumDonationStatus;
    }

    public void setMuseumDonationStatus(String museumDonationStatus) {
        this.museumDonationStatus = museumDonationStatus;
    }

    public String getOnThatDayDonation() {
        return onThatDayDonation;
    }

    public void setOnThatDayDonation(String onThatDayDonation) {
        this.onThatDayDonation = onThatDayDonation;
    }

    public String getOtherSpecialDonation() {
        return otherSpecialDonation;
    }

    public void setOtherSpecialDonation(String otherSpecialDonation) {
        this.otherSpecialDonation = otherSpecialDonation;
    }

    public String getSpecialDonation() {
        return specialDonation;
    }

    public void setSpecialDonation(String specialDonation) {
        this.specialDonation = specialDonation;
    }

    public String getSpecialDonationCompleteNote() {
        return specialDonationCompleteNote;
    }

    public void setSpecialDonationCompleteNote(String specialDonationCompleteNote) {
        this.specialDonationCompleteNote = specialDonationCompleteNote;
    }

    public Integer getSpecialDonationSum() {
        return specialDonationSum;
    }

    public void setSpecialDonationSum(Integer specialDonationSum) {
        this.specialDonationSum = specialDonationSum;
    }

    public String getSummerDonation1() {
        return summerDonation1;
    }

    public void setSummerDonation1(String summerDonation1) {
        this.summerDonation1 = summerDonation1;
    }

    public Integer getSummerDonation1Count() {
        return summerDonation1Count;
    }

    public void setSummerDonation1Count(Integer summerDonation1Count) {
        this.summerDonation1Count = summerDonation1Count;
    }

    public Integer getSummerDonation1and2Sum() {
        return summerDonation1and2Sum;
    }

    public void setSummerDonation1and2Sum(Integer summerDonation1and2Sum) {
        this.summerDonation1and2Sum = summerDonation1and2Sum;
    }

    public String getSummerDonation2() {
        return summerDonation2;
    }

    public void setSummerDonation2(String summerDonation2) {
        this.summerDonation2 = summerDonation2;
    }

    public String getSummerDonationType() {
        return summerDonationType;
    }

    public void setSummerDonationType(String summerDonationType) {
        this.summerDonationType = summerDonationType;
    }

    public String getSummerDonationYear() {
        return summerDonationYear;
    }

    public void setSummerDonationYear(String summerDonationYear) {
        this.summerDonationYear = summerDonationYear;
    }

    public String getSummerRelativeMeritList() {
        return summerRelativeMeritList;
    }

    public void setSummerRelativeMeritList(String summerRelativeMeritList) {
        this.summerRelativeMeritList = summerRelativeMeritList;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public void copyRequest(SummerDonationRequest request) {
        this.unitId = CommonUtils.Empty2Null(request.getUnitId());
        this.unitName = CommonUtils.Empty2Null(request.getUnitName());
        this.enrollUserId = CommonUtils.Empty2Null(request.getEnrollUserId());
        this.enrollUserName = CommonUtils.Empty2Null(request.getEnrollUserName());
        this.summerDonationYear = CommonUtils.Empty2Null(request.getSummerDonationYear());
        this.summerDonationType = CommonUtils.Empty2Null(request.getSummerDonationType());
        this.summerDonation1 = CommonUtils.Empty2Null(request.getSummerDonation1());
        this.summerDonation1Count = request.getSummerDonation1Count();
        this.summerDonation2 = CommonUtils.Empty2Null(request.getSummerDonation2());
        this.summerDonation1and2Sum = request.getSummerDonation1and2Sum();
        this.specialDonation = CommonUtils.Empty2Null(request.getSpecialDonation());
        this.specialDonationSum = request.getSpecialDonationSum();
        this.specialDonationCompleteNote = CommonUtils.Empty2Null(request.getSpecialDonationCompleteNote());
        this.onThatDayDonation = CommonUtils.Empty2Null(request.getOnThatDayDonation());
        this.summerRelativeMeritList = CommonUtils.Empty2Null(request.getSummerRelativeMeritList());
    }
}
