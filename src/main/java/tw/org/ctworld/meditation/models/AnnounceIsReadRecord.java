package tw.org.ctworld.meditation.models;


import javax.persistence.*;

@Entity
@Table(name = "announce_isread_record")
public class AnnounceIsReadRecord extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    @Column(length = 9)
    private String unitId;
    @Column(length = 30)
    private String unitName;
    @Column(length = 9)
    private String userId;
    @Column(length = 60)
    private String userName;
    @Column(length = 9)
    private String messageId;
    @Column(length = 50)
    private String announceSubject;


    public AnnounceIsReadRecord() {
    }

    public AnnounceIsReadRecord(String unitId, String unitName, String userId, String userName, String messageId) {
        this.unitId = unitId;
        this.unitName = unitName;
        this.userId = userId;
        this.userName = userName;
        this.messageId = messageId;
//        this.announceSubject = announceSubject;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getAnnounceSubject() {
        return announceSubject;
    }

    public void setAnnounceSubject(String announceSubject) {
        this.announceSubject = announceSubject;
    }
}
