package tw.org.ctworld.meditation.models;

import javax.persistence.*;

@Entity
@Table(name = "sys_code")
public class SysCode extends BaseEntity{
    @Id
    @GeneratedValue
    private long Id;

    @Column(length = 50)
    private String code_desc;
    @Column(length = 50)
    private String code_key;
    @Column(length = 200)
    private String code_value;
    @Column(length = 50)
    private String code_category;

    public SysCode() {
    }

    public SysCode(String code_key, String code_value, String code_category,String code_desc) {
        this.code_key = code_key;
        this.code_value = code_value;
        this.code_category = code_category;
        this.code_desc = code_desc;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getCode_desc() {
        return code_desc;
    }

    public void setCode_desc(String code_desc) {
        this.code_desc = code_desc;
    }

    public String getCode_key() {
        return code_key;
    }

    public void setCode_key(String code_key) {
        this.code_key = code_key;
    }

    public String getCode_value() {
        return code_value;
    }

    public void setCode_value(String code_value) {
        this.code_value = code_value;
    }

    public String getCode_category() {
        return code_category;
    }

    public void setCode_category(String code_category) {
        this.code_category = code_category;
    }
}
