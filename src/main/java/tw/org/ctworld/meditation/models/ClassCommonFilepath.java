package tw.org.ctworld.meditation.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "class_common_filepath")
//@Immutable
//@Subselect("select * from class_common_filepath")
public class ClassCommonFilepath extends BaseEntity{

    @Id
    private int id;
    @Column(length = 10)
    private String filePathKind;
    @Column(length = 100)
    private String filePathRoot;
    @Column(length = 100)
    private String filePath;
    @Column(length = 100)
    private String fileName;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isEncryption;

    public ClassCommonFilepath() {
    }

    public ClassCommonFilepath(String filePathRoot, String filePath, String fileName) {
        this.filePathRoot = filePathRoot;
        this.filePath = filePath;
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePathKind() {
        return filePathKind;
    }

    public void setFilePathKind(String filePathKind) {
        this.filePathKind = filePathKind;
    }

    public String getFilePathRoot() {
        return filePathRoot;
    }

    public void setFilePathRoot(String filePathRoot) {
        this.filePathRoot = filePathRoot;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Boolean isEncryption() {
        return isEncryption == null ? false : isEncryption;
    }

    public void setEncryption(Boolean encryption) {

        if (encryption == null) {
            isEncryption = false;
        }else {
            isEncryption = encryption;
        }

    }
}
