/*
* Auto Generated
* Based on name: ct_member_import_log
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="ct_member_import_log")
public class CtMemberImportLog extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 150)
    private String aliasName;

    @Column(length = 50)
    private String ctDharmaName;

    @Column(columnDefinition = "TEXT")
    private String importContent;

    @Column(length = 50)
    private String importResultType;

    @Column(length = 50)
    private String memberId;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAliasName() {
        return this.aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return this.ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getImportContent() {
        return this.importContent;
    }

    public void setImportContent(String importContent) {
        this.importContent = importContent;
    }

    public String getImportResultType() {
        return this.importResultType;
    }

    public void setImportResultType(String importResultType) {
        this.importResultType = importResultType;
    }

    public String getMemberId() {
        return this.memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getUnitId() {
        return this.unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return this.unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

}
