/*
* Auto Generated
* Based on name: event_unit_info
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.annotation.JsonProperty;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain006Request;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp004Request;

import static tw.org.ctworld.meditation.libs.CommonUtils.DateTime2DateString;
import static tw.org.ctworld.meditation.libs.CommonUtils.ParseDate;
import static tw.org.ctworld.meditation.libs.CommonUtils.ParseTime;

@Entity
@Table(name="event_unit_info")
public class EventUnitInfo extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(columnDefinition = "Date")
    private Date arriveCtDate;

    @Column(columnDefinition = "Time")
    private Date arriveCtTime;

    @Column(length = 50)
    private String eventCategoryId;

    @Column(length = 50)
    private String eventCategoryName;

    @Column(length = 50)
    private String eventCategoryColorCode;

    @Column(columnDefinition = "Date")
    private Date changeEndDate;

    @Column(columnDefinition = "Date")
    private Date enrollEndDate;

    @Column(columnDefinition = "Date")
    private Date eventEndDate;

    @Column(columnDefinition = "Time")
    private Date eventEndTime;

    @Column(columnDefinition = "TEXT")
    private String eventEnrollGroupNameList;

    @Column(length = 50)
    private String eventId;

    @Column(length = 150)
    private String eventLeader;

    @Column(length = 50)
    private String eventLeaderMemberId;

    @Column(length = 50)
    private String eventLeaderPhone;

    @Column(length = 100)
    private String eventName;

    @Column(columnDefinition = "Date")
    private Date eventStartDate;

    @Column(columnDefinition = "Time")
    private Date eventStartTime;

    @Column(columnDefinition = "TEXT")
    private String eventUnitNote;

    @Column(length = 50)
    private String eventYear;

    @Column(columnDefinition = "TEXT")
    private String idType1List;

    @Column(columnDefinition = "TEXT")
    private String idType2List;

    @Column(length = 50)
    private String leaderMasterId;

    @Column(length = 50)
    private String leaderMasterName;

    @Column(length = 50)
    private String leaderMasterPhoneNum;

    @Column(columnDefinition = "Date")
    private Date leaveCtDate;

    @Column(columnDefinition = "Time")
    private Date leaveCtTime;

    @Column(columnDefinition = "Date")
    private Date leaveUnitDate;

    @Column(columnDefinition = "Time")
    private Date leaveUnitTime;

    private Integer peopleForecast;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;

    @Column(length = 150)
    private String viceEventLeader1;

    @Column(length = 50)
    private String viceEventLeader1MemberId;

    @Column(length = 50)
    private String viceEventLeader1Phone;

    @Column(length = 150)
    private String viceEventLeader2;

    @Column(length = 50)
    private String viceEventLeader2MemberId;

    @Column(length = 50)
    private String viceEventLeader2Phone;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getArriveCtDate() {
        return this.arriveCtDate;
    }

    public void setArriveCtDate(Date arriveCtDate) {
        this.arriveCtDate = arriveCtDate;
    }

    public Date getArriveCtTime() {
        return this.arriveCtTime;
    }

    public void setArriveCtTime(Date arriveCtTime) {
        this.arriveCtTime = arriveCtTime;
    }

    public String getEventCategoryId() {
        return this.eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return this.eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public Date getEventEndDate() {
        return this.eventEndDate;
    }

    public void setEventEndDate(Date eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public Date getEventEndTime() {
        return this.eventEndTime;
    }

    public void setEventEndTime(Date eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventEnrollGroupNameList () {
        return this.eventEnrollGroupNameList ;
    }

    public void setEventEnrollGroupNameList (String eventEnrollGroupNameList ) {
        this.eventEnrollGroupNameList  = eventEnrollGroupNameList ;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventLeader() {
        return this.eventLeader;
    }

    public void setEventLeader(String eventLeader) {
        this.eventLeader = eventLeader;
    }

    public String getEventLeaderMemberId() {
        return this.eventLeaderMemberId;
    }

    public void setEventLeaderMemberId(String eventLeaderMemberId) {
        this.eventLeaderMemberId = eventLeaderMemberId;
    }

    public String getEventLeaderPhone() {
        return this.eventLeaderPhone;
    }

    public void setEventLeaderPhone(String eventLeaderPhone) {
        this.eventLeaderPhone = eventLeaderPhone;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getEventStartDate() {
        return this.eventStartDate;
    }

    public void setEventStartDate(Date eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public Date getEventStartTime() {
        return this.eventStartTime;
    }

    public void setEventStartTime(Date eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getEventUnitNote () {
        return this.eventUnitNote ;
    }

    public void setEventUnitNote (String eventUnitNote ) {
        this.eventUnitNote  = eventUnitNote ;
    }

    public String getEventYear() {
        return this.eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public String getIdType1List() {
        return this.idType1List;
    }

    public void setIdType1List(String idType1List) {
        this.idType1List = idType1List;
    }

    public String getIdType2List() {
        return this.idType2List;
    }

    public void setIdType2List(String idType2List) {
        this.idType2List = idType2List;
    }

    public String getLeaderMasterId () {
        return this.leaderMasterId ;
    }

    public void setLeaderMasterId (String leaderMasterId ) {
        this.leaderMasterId  = leaderMasterId ;
    }

    public String getLeaderMasterName() {
        return this.leaderMasterName;
    }

    public void setLeaderMasterName(String leaderMasterName) {
        this.leaderMasterName = leaderMasterName;
    }

    public String getLeaderMasterPhoneNum() {
        return this.leaderMasterPhoneNum;
    }

    public void setLeaderMasterPhoneNum(String leaderMasterPhoneNum) {
        this.leaderMasterPhoneNum = leaderMasterPhoneNum;
    }

    public Date getLeaveCtDate() {
        return this.leaveCtDate;
    }

    public void setLeaveCtDate(Date leaveCtDate) {
        this.leaveCtDate = leaveCtDate;
    }

    public Date getLeaveCtTime() {
        return this.leaveCtTime;
    }

    public void setLeaveCtTime(Date leaveCtTime) {
        this.leaveCtTime = leaveCtTime;
    }

    public Date getLeaveUnitDate() {
        return this.leaveUnitDate;
    }

    public void setLeaveUnitDate(Date leaveUnitDate) {
        this.leaveUnitDate = leaveUnitDate;
    }

    public Date getLeaveUnitTime() {
        return this.leaveUnitTime;
    }

    public void setLeaveUnitTime(Date leaveUnitTime) {
        this.leaveUnitTime = leaveUnitTime;
    }

    public Integer getPeopleForecast() {
        return this.peopleForecast != null ? this.peopleForecast : 0;
    }

    public void setPeopleForecast(Integer peopleForecast) {
        this.peopleForecast = peopleForecast;
    }

    public String getUnitId() {
        return this.unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return this.unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getViceEventLeader1() {
        return this.viceEventLeader1;
    }

    public void setViceEventLeader1(String viceEventLeader1) {
        this.viceEventLeader1 = viceEventLeader1;
    }

    public String getViceEventLeader1MemberId() {
        return this.viceEventLeader1MemberId;
    }

    public void setViceEventLeader1MemberId(String viceEventLeader1MemberId) {
        this.viceEventLeader1MemberId = viceEventLeader1MemberId;
    }

    public String getViceEventLeader1Phone() {
        return this.viceEventLeader1Phone;
    }

    public void setViceEventLeader1Phone(String viceEventLeader1Phone) {
        this.viceEventLeader1Phone = viceEventLeader1Phone;
    }

    public String getViceEventLeader2() {
        return this.viceEventLeader2;
    }

    public void setViceEventLeader2(String viceEventLeader2) {
        this.viceEventLeader2 = viceEventLeader2;
    }

    public String getViceEventLeader2MemberId() {
        return this.viceEventLeader2MemberId;
    }

    public void setViceEventLeader2MemberId(String viceEventLeader2MemberId) {
        this.viceEventLeader2MemberId = viceEventLeader2MemberId;
    }

    public String getViceEventLeader2Phone() {
        return this.viceEventLeader2Phone;
    }

    public void setViceEventLeader2Phone(String viceEventLeader2Phone) {
        this.viceEventLeader2Phone = viceEventLeader2Phone;
    }

    public String getEventCategoryColorCode() {
        return eventCategoryColorCode;
    }

    public void setEventCategoryColorCode(String eventCategoryColorCode) {
        this.eventCategoryColorCode = eventCategoryColorCode;
    }

    public Date getChangeEndDate() {
        return changeEndDate;
    }

    public void setChangeEndDate(Date eventChangeEndDate) {
        this.changeEndDate = eventChangeEndDate;
    }

    public Date getEnrollEndDate() {
        return enrollEndDate;
    }

    public void setEnrollEndDate(Date enrollEndDate) {
        this.enrollEndDate = enrollEndDate;
    }

    public void copyEventMain(String unitId, String unitName, EventMain eventMain) {
        this.arriveCtDate = eventMain.getEventStartDate();
        this.arriveCtTime = eventMain.getEventStartTime();
        this.eventCategoryId = eventMain.getEventCategoryId();
        this.eventCategoryName = eventMain.getEventCategoryName();
        this.eventEndDate = eventMain.getEventEndDate();
        this.eventEndTime = eventMain.getEventEndTime();
        this.eventEnrollGroupNameList = null;
        this.eventId = eventMain.getEventId();
        this.eventLeader = null;
        this.eventLeaderMemberId = null;
        this.eventLeaderPhone = null;
        this.eventName = eventMain.getEventName();
        this.eventStartDate = eventMain.getEventStartDate();
        this.eventStartTime = eventMain.getEventStartTime();
        this.eventUnitNote = null;
        this.eventYear = eventMain.getEventYear();
        this.idType1List = eventMain.getIdType1List();
        this.idType2List = eventMain.getIdType2List();
        this.leaderMasterId = null;
        this.leaderMasterName = null;
        this.leaderMasterPhoneNum = null;
        this.leaveCtDate = eventMain.getEventEndDate();
        this.leaveCtTime = eventMain.getEventEndTime();
        this.leaveUnitDate = eventMain.getEventStartDate();
        this.leaveUnitTime = eventMain.getEventStartTime();
        this.peopleForecast = 0;
        this.unitId = unitId;
        this.unitName = unitName;
        this.viceEventLeader1 = null;
        this.viceEventLeader1MemberId = null;
        this.viceEventLeader1Phone = null;
        this.viceEventLeader2 = null;
        this.viceEventLeader2MemberId = null;
        this.viceEventLeader2Phone = null;

        this.eventCategoryColorCode = eventMain.getEventCategoryColorCode();
        this.changeEndDate = eventMain.getChangeEndDate();
        this.enrollEndDate = eventMain.getEnrollEndDate();
    }

    public void copyEventMain(String unitId, String unitName, CtEventMaintain006Request ctEventMaintain006Request) {
        this.eventStartDate = ParseDate(ctEventMaintain006Request.getEventStartDate());
        this.eventStartTime = ParseTime(ctEventMaintain006Request.getEventStartTime());

        this.arriveCtDate = this.eventStartDate;
        this.arriveCtTime = this.eventStartTime;
        this.eventCategoryId = ctEventMaintain006Request.getEventCategoryId();
        this.eventCategoryName = ctEventMaintain006Request.getEventCategoryName();
        this.eventEndDate = ParseDate(ctEventMaintain006Request.getEventEndDate());
        this.eventEndTime = ParseTime(ctEventMaintain006Request.getEventEndTime());
        this.eventEnrollGroupNameList = null;
        this.eventId = ctEventMaintain006Request.getEventId();
        this.eventLeader = null;
        this.eventLeaderMemberId = null;
        this.eventLeaderPhone = null;
        this.eventName = ctEventMaintain006Request.getEventName();

        this.eventUnitNote = null;
        this.eventYear = ctEventMaintain006Request.getEventYear();
        this.idType1List = ctEventMaintain006Request.getIdType1List();
        this.idType2List = ctEventMaintain006Request.getIdType2List();
        this.leaderMasterId = null;
        this.leaderMasterName = null;
        this.leaderMasterPhoneNum = null;
        this.leaveCtDate = this.eventEndDate;
        this.leaveCtTime = this.eventEndTime;
        this.leaveUnitDate = this.eventStartDate;
        this.leaveUnitTime = this.eventStartTime;
        this.peopleForecast = 0;
        this.unitId = unitId;
        this.unitName = unitName;
        this.viceEventLeader1 = null;
        this.viceEventLeader1MemberId = null;
        this.viceEventLeader1Phone = null;
        this.viceEventLeader2 = null;
        this.viceEventLeader2MemberId = null;
        this.viceEventLeader2Phone = null;

        this.enrollEndDate = ParseDate(ctEventMaintain006Request.getEnrollEndDate());
    }

    public void copyUnitStartUp004Request(UnitStartUp004Request unitStartUp004Request) {
        this.peopleForecast = unitStartUp004Request.getPeopleForecast();
        this.leaveUnitDate = unitStartUp004Request.getLeaveUnitDate().isEmpty() ? null : ParseDate(unitStartUp004Request.getLeaveUnitDate());
        this.leaveUnitTime = unitStartUp004Request.getLeaveUnitTime().isEmpty() ? null : ParseTime(unitStartUp004Request.getLeaveUnitTime());
        this.arriveCtDate = unitStartUp004Request.getArriveCtDate().isEmpty() ? null : ParseDate(unitStartUp004Request.getArriveCtDate());
        this.arriveCtTime = unitStartUp004Request.getArriveCtTime().isEmpty() ? null : ParseTime(unitStartUp004Request.getArriveCtTime());
        this.leaveCtDate = unitStartUp004Request.getLeaveCtDate().isEmpty() ? null : ParseDate(unitStartUp004Request.getLeaveCtDate());
        this.leaveCtTime = unitStartUp004Request.getLeaveCtTime().isEmpty() ? null : ParseTime(unitStartUp004Request.getLeaveCtTime());
        this.leaderMasterName = unitStartUp004Request.getLeaderMasterName();
        this.leaderMasterId = unitStartUp004Request.getLeaderMasterId();
        this.leaderMasterPhoneNum = unitStartUp004Request.getLeaderMasterPhoneNum();
        this.eventLeader = unitStartUp004Request.getEventLeader();
        this.eventLeaderMemberId = unitStartUp004Request.getEventLeaderMemberId();
        this.eventLeaderPhone = unitStartUp004Request.getEventLeaderPhone();
        this.viceEventLeader1 = unitStartUp004Request.getViceEventLeader1();
        this.viceEventLeader1MemberId = unitStartUp004Request.getViceEventLeader1MemberId();
        this.viceEventLeader1Phone = unitStartUp004Request.getViceEventLeader1Phone();
        this.viceEventLeader2 = unitStartUp004Request.getViceEventLeader2();
        this.viceEventLeader2MemberId = unitStartUp004Request.getViceEventLeader2MemberId();
        this.viceEventLeader2Phone = unitStartUp004Request.getViceEventLeader2Phone();
        this.eventEnrollGroupNameList = unitStartUp004Request.getEventEnrollGroupNameList();
        this.eventUnitNote = unitStartUp004Request.getEventUnitNote();

    }
}
