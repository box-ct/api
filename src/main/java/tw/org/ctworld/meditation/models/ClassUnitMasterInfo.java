package tw.org.ctworld.meditation.models;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "class_unit_master_info")
@Immutable
@Subselect("select * from class_unit_master_info")
public class ClassUnitMasterInfo {

    @Id
    private int id;
    @Column(length = 5)
    private String masterId;
    @Column(length = 30)
    private String masterName;
    @Column(length = 9)
    private String unitId;
    @Column(length = 50)
    private String unitName;
    @Column(length = 30)
    private String jobTitle;
    @Column(length = 8)
    private String createDt;
    @Column(length = 10)
    private String creatorId;
    @Column(length = 6)
    private String createTm;
    @Column(length = 40)
    private String creatorIp;
    @Column(length = 60)
    private String creatorName;
    @Column(length = 10)
    private String creatorUnitName;
    @Column(length = 8)
    private String updateDt;
    @Column(length = 10)
    private String updatorId;
    @Column(length = 40)
    private String updatorIp;
    @Column(length = 6)
    private String updateTm;
    @Column(length = 10)
    private String updatorUnitName;
    @Column(length = 60)
    private String updatorName;
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isAbbot;
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTreasurer;
    @Column(columnDefinition = "FLOAT")
    private Double jobOrder;
    @Column(length = 40)
    private String mobileNum;
    @Column(length = 5)
    private String preceptOrder;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreateTm() {
        return createTm;
    }

    public void setCreateTm(String createTm) {
        this.createTm = createTm;
    }

    public String getCreatorIp() {
        return creatorIp;
    }

    public void setCreatorIp(String creatorIp) {
        this.creatorIp = creatorIp;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreatorUnitName() {
        return creatorUnitName;
    }

    public void setCreatorUnitName(String creatorUnitName) {
        this.creatorUnitName = creatorUnitName;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId;
    }

    public String getUpdatorIp() {
        return updatorIp;
    }

    public void setUpdatorIp(String updatorIp) {
        this.updatorIp = updatorIp;
    }

    public String getUpdateTm() {
        return updateTm;
    }

    public void setUpdateTm(String updateTm) {
        this.updateTm = updateTm;
    }

    public String getUpdatorUnitName() {
        return updatorUnitName;
    }

    public void setUpdatorUnitName(String updatorUnitName) {
        this.updatorUnitName = updatorUnitName;
    }

    public String getUpdatorName() {
        return updatorName;
    }

    public void setUpdatorName(String updatorName) {
        this.updatorName = updatorName;
    }

    public Boolean getAbbot() {
        return isAbbot == null ? false : isAbbot;
    }

    public void setAbbot(Boolean abbot) {
        isAbbot = abbot;
    }

    public Boolean getTreasurer() {
        return isTreasurer == null ? false : isTreasurer;
    }

    public void setTreasurer(Boolean treasurer) {
        isTreasurer = treasurer;
    }

    public Double getJobOrder() {
        return jobOrder;
    }

    public void setJobOrder(Double jobOrder) {
        this.jobOrder = jobOrder;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getPreceptOrder() {
        return preceptOrder;
    }

    public void setPreceptOrder(String preceptOrder) {
        this.preceptOrder = preceptOrder;
    }
}
