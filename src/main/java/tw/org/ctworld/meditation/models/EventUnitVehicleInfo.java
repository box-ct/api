/*
* Auto Generated
* Based on name: event_unit_vehicle_info
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="event_unit_vehicle_info")
public class EventUnitVehicleInfo extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(columnDefinition = "Date")
    private Date arriveCtDate;

    @Column(columnDefinition = "Time")
    private Date arriveCtTime;

    @Column(length = 50)
    private String busDriverMobilePhone;

    @Column(length = 50)
    private String busDriverName;

    @Column(length = 50)
    private String busLeader;

    @Column(length = 50)
    private String busLeaderMemberId;

    @Column(length = 50)
    private String busLeaderPhone;

    @Column(length = 50)
    private String busLicenseNum;

    @Column(length = 50)
    private String carLeader;

    @Column(length = 50)
    private String carLeaderMemberId;

    @Column(length = 50)
    private String carLeaderPhone;

    @Column(length = 50)
    private String carLicenseNum1;

    @Column(length = 50)
    private String carOwnerMemberId;

    @Column(length = 50)
    private String carOwnerMemberName;

    @Column(length = 100)
    private String carOwnerMobilePhone;

    @Column(columnDefinition = "TEXT")
    private String enrollChangeHistory;

    @Column(columnDefinition = "TEXT")
    private String enrollLatestChange;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isUnitCar;

    @Column(columnDefinition = "Date")
    private Date leaveCtDate;

    @Column(columnDefinition = "Time")
    private Date leaveCtTime;

    @Column(columnDefinition = "Date")
    private Date leaveUnitDate;

    @Column(columnDefinition = "Time")
    private Date leaveUnitTime;

    private Integer lunchBoxQty;

    @Column(length = 50)
    private String masterId;

    @Column(length = 50)
    private String masterMobilePhone;

    @Column(length = 50)
    private String masterName;

    @Column(length = 50)
    private String noSnackLunchbox;

    private Integer passengerQty;

    private Integer seatQty;

    private Integer snackQty;

    @Column(length = 50)
    private String transId;

    @Column(columnDefinition = "TEXT")
    private String transNote;

    @Column(length = 50)
    private String transRouteType;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;

    private Integer vehicleAutoAddNum;

    @Column(length = 50)
    private String vehicleName;

    private Integer vehicleNum;

    @Column(length = 50)
    private String vehicleType;

    @Column(length = 50)
    private String viceBusLeader1;

    @Column(length = 50)
    private String viceBusLeader1MemberId;

    @Column(length = 50)
    private String viceBusLeader1Phone;

    @Column(length = 50)
    private String viceBusLeader2;

    @Column(length = 50)
    private String viceBusLeader2MemberId;

    @Column(length = 50)
    private String viceBusLeader2Phone;

    @Column(length = 50)
    private String viceBusLeader3;

    @Column(length = 50)
    private String viceBusLeader3MemberId;

    @Column(length = 50)
    private String viceBusLeader3Phone;

    @Column(length = 50)
    private String viceCarLeader1;

    @Column(length = 50)
    private String viceCarLeader1MemberId;

    @Column(length = 50)
    private String viceCarLeader1Phone;

    @Column(length = 50)
    private String viceCarLeader2;

    @Column(length = 50)
    private String viceCarLeader2MemberId;

    @Column(length = 50)
    private String viceCarLeader2Phone;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getArriveCtDate() {
        return arriveCtDate;
    }

    public void setArriveCtDate(Date arriveCtDate) {
        this.arriveCtDate = arriveCtDate;
    }

    public Date getArriveCtTime() {
        return arriveCtTime;
    }

    public void setArriveCtTime(Date arriveCtTime) {
        this.arriveCtTime = arriveCtTime;
    }

    public String getBusDriverMobilePhone() {
        return busDriverMobilePhone;
    }

    public void setBusDriverMobilePhone(String busDriverMobilePhone) {
        this.busDriverMobilePhone = busDriverMobilePhone;
    }

    public String getBusDriverName() {
        return busDriverName;
    }

    public void setBusDriverName(String busDriverName) {
        this.busDriverName = busDriverName;
    }

    public String getBusLeader() {
        return busLeader;
    }

    public void setBusLeader(String busLeader) {
        this.busLeader = busLeader;
    }

    public String getBusLeaderMemberId() {
        return busLeaderMemberId;
    }

    public void setBusLeaderMemberId(String busLeaderMemberId) {
        this.busLeaderMemberId = busLeaderMemberId;
    }

    public String getBusLeaderPhone() {
        return busLeaderPhone;
    }

    public void setBusLeaderPhone(String busLeaderPhone) {
        this.busLeaderPhone = busLeaderPhone;
    }

    public String getBusLicenseNum() {
        return busLicenseNum;
    }

    public void setBusLicenseNum(String busLicenseNum) {
        this.busLicenseNum = busLicenseNum;
    }

    public String getCarLeader() {
        return carLeader;
    }

    public void setCarLeader(String carLeader) {
        this.carLeader = carLeader;
    }

    public String getCarLeaderMemberId() {
        return carLeaderMemberId;
    }

    public void setCarLeaderMemberId(String carLeaderMemberId) {
        this.carLeaderMemberId = carLeaderMemberId;
    }

    public String getCarLeaderPhone() {
        return carLeaderPhone;
    }

    public void setCarLeaderPhone(String carLeaderPhone) {
        this.carLeaderPhone = carLeaderPhone;
    }

    public String getCarLicenseNum1() {
        return carLicenseNum1;
    }

    public void setCarLicenseNum1(String carLicenseNum1) {
        this.carLicenseNum1 = carLicenseNum1;
    }

    public String getCarOwnerMemberId() {
        return carOwnerMemberId;
    }

    public void setCarOwnerMemberId(String carOwnerMemberId) {
        this.carOwnerMemberId = carOwnerMemberId;
    }

    public String getCarOwnerMemberName() {
        return carOwnerMemberName;
    }

    public void setCarOwnerMemberName(String carOwnerMemberName) {
        this.carOwnerMemberName = carOwnerMemberName;
    }

    public String getCarOwnerMobilePhone() {
        return carOwnerMobilePhone;
    }

    public void setCarOwnerMobilePhone(String carOwnerMobilePhone) {
        this.carOwnerMobilePhone = carOwnerMobilePhone;
    }

    public String getEnrollChangeHistory() {
        return enrollChangeHistory;
    }

    public void setEnrollChangeHistory(String enrollChangeHistory) {
        this.enrollChangeHistory = enrollChangeHistory;
    }

    public String getEnrollLatestChange() {
        return enrollLatestChange;
    }

    public void setEnrollLatestChange(String enrollLatestChange) {
        this.enrollLatestChange = enrollLatestChange;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Boolean getIsUnitCar() {
        return isUnitCar == null ? false : isUnitCar;
    }

    public void setIsUnitCar(Boolean unitCar) {
        isUnitCar = unitCar;
    }

    public Date getLeaveCtDate() {
        return leaveCtDate;
    }

    public void setLeaveCtDate(Date leaveCtDate) {
        this.leaveCtDate = leaveCtDate;
    }

    public Date getLeaveCtTime() {
        return leaveCtTime;
    }

    public void setLeaveCtTime(Date leaveCtTime) {
        this.leaveCtTime = leaveCtTime;
    }

    public Date getLeaveUnitDate() {
        return leaveUnitDate;
    }

    public void setLeaveUnitDate(Date leaveUnitDate) {
        this.leaveUnitDate = leaveUnitDate;
    }

    public Date getLeaveUnitTime() {
        return leaveUnitTime;
    }

    public void setLeaveUnitTime(Date leaveUnitTime) {
        this.leaveUnitTime = leaveUnitTime;
    }

    public int getLunchBoxQty() {
        return lunchBoxQty;
    }

    public void setLunchBoxQty(int lunchBoxQty) {
        this.lunchBoxQty = lunchBoxQty;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterMobilePhone() {
        return masterMobilePhone;
    }

    public void setMasterMobilePhone(String masterMobilePhone) {
        this.masterMobilePhone = masterMobilePhone;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getNoSnackLunchbox() {
        return noSnackLunchbox;
    }

    public void setNoSnackLunchbox(String noSnackLunchbox) {
        this.noSnackLunchbox = noSnackLunchbox;
    }

    public int getPassengerQty() {
        return passengerQty;
    }

    public void setPassengerQty(int passengerQty) {
        this.passengerQty = passengerQty;
    }

    public int getSeatQty() {
        return seatQty;
    }

    public void setSeatQty(int seatQty) {
        this.seatQty = seatQty;
    }

    public int getSnackQty() {
        return snackQty;
    }

    public void setSnackQty(int snackQty) {
        this.snackQty = snackQty;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getTransNote() {
        return transNote;
    }

    public void setTransNote(String transNote) {
        this.transNote = transNote;
    }

    public String getTransRouteType() {
        return transRouteType;
    }

    public void setTransRouteType(String transRouteType) {
        this.transRouteType = transRouteType;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public int getVehicleAutoAddNum() {
        return vehicleAutoAddNum;
    }

    public void setVehicleAutoAddNum(int vehicleAutoAddNum) {
        this.vehicleAutoAddNum = vehicleAutoAddNum;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public int getVehicleNum() {
        return vehicleNum;
    }

    public void setVehicleNum(int vehicleNum) {
        this.vehicleNum = vehicleNum;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getViceBusLeader1() {
        return viceBusLeader1;
    }

    public void setViceBusLeader1(String viceBusLeader1) {
        this.viceBusLeader1 = viceBusLeader1;
    }

    public String getViceBusLeader1MemberId() {
        return viceBusLeader1MemberId;
    }

    public void setViceBusLeader1MemberId(String viceBusLeader1MemberId) {
        this.viceBusLeader1MemberId = viceBusLeader1MemberId;
    }

    public String getViceBusLeader1Phone() {
        return viceBusLeader1Phone;
    }

    public void setViceBusLeader1Phone(String viceBusLeader1Phone) {
        this.viceBusLeader1Phone = viceBusLeader1Phone;
    }

    public String getViceBusLeader2() {
        return viceBusLeader2;
    }

    public void setViceBusLeader2(String viceBusLeader2) {
        this.viceBusLeader2 = viceBusLeader2;
    }

    public String getViceBusLeader2MemberId() {
        return viceBusLeader2MemberId;
    }

    public void setViceBusLeader2MemberId(String viceBusLeader2MemberId) {
        this.viceBusLeader2MemberId = viceBusLeader2MemberId;
    }

    public String getViceBusLeader2Phone() {
        return viceBusLeader2Phone;
    }

    public void setViceBusLeader2Phone(String viceBusLeader2Phone) {
        this.viceBusLeader2Phone = viceBusLeader2Phone;
    }

    public String getViceBusLeader3() {
        return viceBusLeader3;
    }

    public void setViceBusLeader3(String viceBusLeader3) {
        this.viceBusLeader3 = viceBusLeader3;
    }

    public String getViceBusLeader3MemberId() {
        return viceBusLeader3MemberId;
    }

    public void setViceBusLeader3MemberId(String viceBusLeader3MemberId) {
        this.viceBusLeader3MemberId = viceBusLeader3MemberId;
    }

    public String getViceBusLeader3Phone() {
        return viceBusLeader3Phone;
    }

    public void setViceBusLeader3Phone(String viceBusLeader3Phone) {
        this.viceBusLeader3Phone = viceBusLeader3Phone;
    }

    public String getViceCarLeader1() {
        return viceCarLeader1;
    }

    public void setViceCarLeader1(String viceCarLeader1) {
        this.viceCarLeader1 = viceCarLeader1;
    }

    public String getViceCarLeader1MemberId() {
        return viceCarLeader1MemberId;
    }

    public void setViceCarLeader1MemberId(String viceCarLeader1MemberId) {
        this.viceCarLeader1MemberId = viceCarLeader1MemberId;
    }

    public String getViceCarLeader1Phone() {
        return viceCarLeader1Phone;
    }

    public void setViceCarLeader1Phone(String viceCarLeader1Phone) {
        this.viceCarLeader1Phone = viceCarLeader1Phone;
    }

    public String getViceCarLeader2() {
        return viceCarLeader2;
    }

    public void setViceCarLeader2(String viceCarLeader2) {
        this.viceCarLeader2 = viceCarLeader2;
    }

    public String getViceCarLeader2MemberId() {
        return viceCarLeader2MemberId;
    }

    public void setViceCarLeader2MemberId(String viceCarLeader2MemberId) {
        this.viceCarLeader2MemberId = viceCarLeader2MemberId;
    }

    public String getViceCarLeader2Phone() {
        return viceCarLeader2Phone;
    }

    public void setViceCarLeader2Phone(String viceCarLeader2Phone) {
        this.viceCarLeader2Phone = viceCarLeader2Phone;
    }
}
