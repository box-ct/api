/*
* Auto Generated
* Based on name: event_unit_secial_input
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp004Object;

@Entity
@Table(name="event_unit_special_input")
public class EventUnitSpecialInput extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isRequired;

    @Column(length = 50)
    private String sInputCategory;

    @Column(length = 200)
    private String sInputExample;

    @Column(length = 50)
    private String sInputId;

    @Column(length = 50)
    private String sInputName;

    private Integer sInputOrderNum;

    @Column(columnDefinition = "TEXT")
    private String sInputValue;

    @Column(length = 50)
    private String sInputWriteType;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Boolean getIsRequired() {
        return this.isRequired == null ? false : isRequired;
    }

    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public String getSInputCategory() {
        return this.sInputCategory;
    }

    public void setSInputCategory(String sInputCategory) {
        this.sInputCategory = sInputCategory;
    }

    public String getSInputExample() {
        return this.sInputExample;
    }

    public void setSInputExample(String sInputExample) {
        this.sInputExample = sInputExample;
    }

    public String getSInputId() {
        return this.sInputId;
    }

    public void setSInputId(String sInputId) {
        this.sInputId = sInputId;
    }

    public String getSInputName() {
        return this.sInputName;
    }

    public void setSInputName(String sInputName) {
        this.sInputName = sInputName;
    }

    public Integer getSInputOrderNum() {
        return this.sInputOrderNum;
    }

    public void setSInputOrderNum(Integer sInputOrderNum) {
        this.sInputOrderNum = sInputOrderNum;
    }

    public String getSInputValue() {
        return this.sInputValue;
    }

    public void setSInputValue(String sInputValue) {
        this.sInputValue = sInputValue;
    }

    public String getSInputWriteType() {
        return this.sInputWriteType;
    }

    public void setSInputWriteType(String sInputWriteType) {
        this.sInputWriteType = sInputWriteType;
    }

    public String getUnitId() {
        return this.unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return this.unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }


    public void copyUnitStartUp004Object(UnitStartUp004Object u) {
        //this.sInputId = u.getsInputId();
        this.sInputName = u.getsInputName();
        this.sInputValue = u.getsInputValue();
        this.isRequired = u.getIsRequired();
        this.sInputExample = u.getsInputExample();
    }
    
    public void copyCtSpecialInput(EventCtSpecialInput ctSpecialInput) {
        this.eventId = ctSpecialInput.getEventId();
        this.eventName = ctSpecialInput.getEventName();
        this.isRequired = ctSpecialInput.getIsRequired();
        this.sInputCategory = ctSpecialInput.getSInputCategory();
        this.sInputExample = ctSpecialInput.getSInputExample();
        this.sInputId = ctSpecialInput.getSInputId();
        this.sInputName = ctSpecialInput.getSInputName();
        this.sInputOrderNum = ctSpecialInput.getSInputOrderNum();
        this.sInputValue = null;
        this.sInputWriteType = ctSpecialInput.getSInputWriteType();
    }
}
