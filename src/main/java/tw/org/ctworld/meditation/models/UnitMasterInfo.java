/*
* Auto Generated
* Based on name: unit_master_info
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort;

@Entity
@Table(name="unit_master_info")
public class UnitMasterInfo extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isAbbot;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTreasurer;

    @Column(length = 50)
    private String jobOrder;

    @Column(length = 200)
    private String jobTitle;

    @Column(length = 50)
    private String masterId;

    @Column(length = 50)
    private String masterName;

    @Column(length = 50)
    private String masterPreceptTypeName;

    @Column(length = 50)
    private String mobileNum;

    @Column(length = 50)
    private String preceptOrder;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;

    public UnitMasterInfo() {
    }

    public UnitMasterInfo(CtMasterInfo ctMasterInfo) {
        this.masterId = ctMasterInfo.getMasterId();
        this.masterName = ctMasterInfo.getMasterName();
        this.masterPreceptTypeName = ctMasterInfo.getMasterPreceptTypeName();
        this.isAbbot = ctMasterInfo.getIsAbbot();
        this.isTreasurer = ctMasterInfo.getIsTreasurer();
        this.preceptOrder = ctMasterInfo.getPreceptOrder();
        this.jobOrder = ctMasterInfo.getJobOrder();
        this.jobTitle = ctMasterInfo.getJobTitle();
        this.mobileNum = ctMasterInfo.getMobileNum();

        this.unitId = ctMasterInfo.getUnitId();
        this.unitName = ctMasterInfo.getUnitName();
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getIsAbbot() {
        return this.isAbbot == null ? false : isAbbot;
    }

    public void setIsAbbot(Boolean isAbbot) {
        this.isAbbot = isAbbot;
    }

    public Boolean getIsTreasurer() {
        return this.isTreasurer == null ? false : isTreasurer;
    }

    public void setIsTreasurer(Boolean isTreasurer) {
        this.isTreasurer = isTreasurer;
    }

    public String getJobOrder() {
        return this.jobOrder;
    }

    public void setJobOrder(String jobOrder) {
        this.jobOrder = jobOrder;
    }

    public String getJobTitle() {
        return this.jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getMasterId() {
        return this.masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterName() {
        return this.masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getMasterPreceptTypeName() {
        return this.masterPreceptTypeName;
    }

    public void setMasterPreceptTypeName(String masterPreceptTypeName) {
        this.masterPreceptTypeName = masterPreceptTypeName;
    }

    public String getMobileNum() {
        return this.mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getPreceptOrder() {
        return this.preceptOrder;
    }

    public void setPreceptOrder(String preceptOrder) {
        this.preceptOrder = preceptOrder;
    }

    public String getUnitId() {
        return this.unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return this.unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public void copy(MasterInfoShort masterInfoShort) {
        setMasterId(masterInfoShort.getMasterId());
        setMasterName(masterInfoShort.getMasterName());
        setMasterPreceptTypeName(masterInfoShort.getMasterPreceptTypeName());
        setIsAbbot(masterInfoShort.getIsAbbot());
        setIsTreasurer(masterInfoShort.getIsTreasurer());
        setJobTitle(masterInfoShort.getJobTitle());
        setJobOrder(masterInfoShort.getJobOrder());
        setMobileNum(masterInfoShort.getMobileNum());
        setPreceptOrder(masterInfoShort.getPreceptOrder());
        setUnitId(masterInfoShort.getUnitId());
        setUnitName(masterInfoShort.getUnitName());
    }
}
