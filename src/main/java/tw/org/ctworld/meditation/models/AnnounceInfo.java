package tw.org.ctworld.meditation.models;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "announce_info")
public class AnnounceInfo extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    @Column(length = 9, unique = true)
    private String messageId;
    private Date announceDt;
    @Column(length = 50)
    private String announceSubject;
    @Column(length = 300)
    private String announceContent;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isEnabled;

    public AnnounceInfo() {
    }

    public AnnounceInfo(String messageId, Date announceDt, String announceSubject, String announceContent, Boolean isEnabled) {
        this.messageId = messageId;
        this.announceDt = announceDt;
        this.announceSubject = announceSubject;
        this.announceContent = announceContent;
        this.isEnabled = isEnabled;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Date getAnnounceDt() {
        return announceDt;
    }

    public void setAnnounceDt(Date announceDt) {
        this.announceDt = announceDt;
    }

    public String getAnnounceSubject() {
        return announceSubject;
    }

    public void setAnnounceSubject(String announceSubject) {
        this.announceSubject = announceSubject;
    }

    public String getAnnounceContent() {
        return announceContent;
    }

    public void setAnnounceContent(String announceContent) {
        this.announceContent = announceContent;
    }

    public Boolean isEnabled() {
        return isEnabled == null ? false : isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public void copy(AnnounceInfo announceInfo) {
        setEnabled(announceInfo.isEnabled);
        setAnnounceSubject(announceInfo.getAnnounceSubject());
        setAnnounceContent(announceInfo.getAnnounceContent());
    }
}
