/*
* Auto Generated
* Based on name: event_unit_template_pgm
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="event_unit_template_pgm")
public class EventUnitTemplatePgm extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(columnDefinition = "Date")
    private Date ctDormEndDate;

    @Column(columnDefinition = "Date")
    private Date ctDormStartDate;

    @Column(length = 50)
    private String eventCategoryId;

    @Column(length = 50)
    private String eventCategoryName;

    @Column(length = 50)
    private String eventCreatorUnitId;

    @Column(length = 50)
    private String eventCreatorUnitName;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @Column(length = 50)
    private String genPgmUserInputValue;

    @Column(columnDefinition = "Date")
    private Date goTransDate;

    @Column(length = 50)
    private String goTransId;

    @Column(length = 50)
    private String goTransNum;

    @Column(length = 50)
    private String goTransType;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isDisabledByOCA;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isGenPgm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isRequired;

    @Column(length = 50)
    private String pgmCategoryId;

    @Column(length = 50)
    private String pgmCategoryName;

    private Integer pgmCtOrderNum;

    @Column(columnDefinition = "Date")
    private Date pgmDate;

    @Column(length = 100)
    private String pgmDesc;

    @Column(columnDefinition = "Time")
    private Date pgmEndTime;

    @Column(length = 50)
    private String pgmId;

    @Column(length = 50)
    private String pgmName;

    @Column(columnDefinition = "TEXT")
    private String pgmNote;

    @Column(columnDefinition = "Time")
    private Date pgmStartTime;

    private Integer pgmUiOrderNum;

    @Column(length = 50)
    private String pgmUniqueId;

    @Column(columnDefinition = "Date")
    private Date returnTransDate;

    @Column(length = 50)
    private String returnTransId;

    @Column(length = 50)
    private String returnTransNum;

    @Column(length = 50)
    private String returnTransType;

    @Column(length = 150)
    private String sameGoTransMemberName;

    @Column(length = 150)
    private String sameReturnTransMemberName;

    @Column(length = 50)
    private String sponsorUnitId;

    @Column(length = 50)
    private String sponsorUnitName;

    @Column(length = 50)
    private String templateName;

    @Column(length = 50)
    private String templateUniqueId;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCtDormEndDate() {
        return this.ctDormEndDate;
    }

    public void setCtDormEndDate(Date ctDormEndDate) {
        this.ctDormEndDate = ctDormEndDate;
    }

    public Date getCtDormStartDate() {
        return this.ctDormStartDate;
    }

    public void setCtDormStartDate(Date ctDormStartDate) {
        this.ctDormStartDate = ctDormStartDate;
    }

    public String getEventCategoryId() {
        return this.eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return this.eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public String getEventCreatorUnitId() {
        return this.eventCreatorUnitId;
    }

    public void setEventCreatorUnitId(String eventCreatorUnitId) {
        this.eventCreatorUnitId = eventCreatorUnitId;
    }

    public String getEventCreatorUnitName() {
        return this.eventCreatorUnitName;
    }

    public void setEventCreatorUnitName(String eventCreatorUnitName) {
        this.eventCreatorUnitName = eventCreatorUnitName;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getGenPgmUserInputValue() {
        return this.genPgmUserInputValue;
    }

    public void setGenPgmUserInputValue(String genPgmUserInputValue) {
        this.genPgmUserInputValue = genPgmUserInputValue;
    }

    public Date getGoTransDate() {
        return this.goTransDate;
    }

    public void setGoTransDate(Date goTransDate) {
        this.goTransDate = goTransDate;
    }

    public String getGoTransId() {
        return this.goTransId;
    }

    public void setGoTransId(String goTransId) {
        this.goTransId = goTransId;
    }

    public String getGoTransNum() {
        return goTransNum;
    }

    public void setGoTransNum(String goTransNum) {
        this.goTransNum = goTransNum;
    }

    public String getGoTransType() {
        return this.goTransType;
    }

    public void setGoTransType(String goTransType) {
        this.goTransType = goTransType;
    }

    public Boolean getIsDisabledByOCA() {
        return this.isDisabledByOCA == null ? false : isDisabledByOCA;
    }

    public void setIsDisabledByOCA(Boolean isDisabledByOCA) {
        this.isDisabledByOCA = isDisabledByOCA;
    }

    public Boolean getIsGenPgm() {
        return this.isGenPgm == null ? false : isGenPgm;
    }

    public void setIsGenPgm(Boolean isGenPgm) {
        this.isGenPgm = isGenPgm;
    }

    public Boolean getIsRequired() {
        return this.isRequired == null ? false : isRequired;
    }

    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public String getPgmCategoryId() {
        return this.pgmCategoryId;
    }

    public void setPgmCategoryId(String pgmCategoryId) {
        this.pgmCategoryId = pgmCategoryId;
    }

    public String getPgmCategoryName() {
        return this.pgmCategoryName;
    }

    public void setPgmCategoryName(String pgmCategoryName) {
        this.pgmCategoryName = pgmCategoryName;
    }

    public Integer getPgmCtOrderNum() {
        return this.pgmCtOrderNum;
    }

    public void setPgmCtOrderNum(Integer pgmCtOrderNum) {
        this.pgmCtOrderNum = pgmCtOrderNum;
    }

    public Date getPgmDate() {
        return this.pgmDate;
    }

    public void setPgmDate(Date pgmDate) {
        this.pgmDate = pgmDate;
    }

    public String getPgmDesc() {
        return this.pgmDesc;
    }

    public void setPgmDesc(String pgmDesc) {
        this.pgmDesc = pgmDesc;
    }

    public Date getPgmEndTime() {
        return this.pgmEndTime;
    }

    public void setPgmEndTime(Date pgmEndTime) {
        this.pgmEndTime = pgmEndTime;
    }

    public String getPgmId() {
        return this.pgmId;
    }

    public void setPgmId(String pgmId) {
        this.pgmId = pgmId;
    }

    public String getPgmName() {
        return this.pgmName;
    }

    public void setPgmName(String pgmName) {
        this.pgmName = pgmName;
    }

    public String getPgmNote() {
        return this.pgmNote;
    }

    public void setPgmNote(String pgmNote) {
        this.pgmNote = pgmNote;
    }

    public Date getPgmStartTime() {
        return this.pgmStartTime;
    }

    public void setPgmStartTime(Date pgmStartTime) {
        this.pgmStartTime = pgmStartTime;
    }

    public Integer getPgmUiOrderNum() {
        return this.pgmUiOrderNum;
    }

    public void setPgmUiOrderNum(Integer pgmUiOrderNum) {
        this.pgmUiOrderNum = pgmUiOrderNum;
    }

    public String getPgmUniqueId() {
        return this.pgmUniqueId;
    }

    public void setPgmUniqueId(String pgmUniqueId) {
        this.pgmUniqueId = pgmUniqueId;
    }

    public Date getReturnTransDate() {
        return this.returnTransDate;
    }

    public void setReturnTransDate(Date returnTransDate) {
        this.returnTransDate = returnTransDate;
    }

    public String getReturnTransId() {
        return this.returnTransId;
    }

    public void setReturnTransId(String returnTransId) {
        this.returnTransId = returnTransId;
    }

    public String getReturnTransNum() {
        return returnTransNum;
    }

    public void setReturnTransNum(String returnTransNum) {
        this.returnTransNum = returnTransNum;
    }

    public String getReturnTransType() {
        return this.returnTransType;
    }

    public void setReturnTransType(String returnTransType) {
        this.returnTransType = returnTransType;
    }

    public String getSponsorUnitId() {
        return this.sponsorUnitId;
    }

    public void setSponsorUnitId(String sponsorUnitId) {
        this.sponsorUnitId = sponsorUnitId;
    }

    public String getSponsorUnitName() {
        return this.sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateUniqueId() {
        return this.templateUniqueId;
    }

    public void setTemplateUniqueId(String templateUniqueId) {
        this.templateUniqueId = templateUniqueId;
    }

    public String getUnitId() {
        return this.unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return this.unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getSameGoTransMemberName() {
        return sameGoTransMemberName;
    }

    public void setSameGoTransMemberName(String sameGoTransMemberName) {
        this.sameGoTransMemberName = sameGoTransMemberName;
    }

    public String getSameReturnTransMemberName() {
        return sameReturnTransMemberName;
    }

    public void setSameReturnTransMemberName(String sameReturnTransMemberName) {
        this.sameReturnTransMemberName = sameReturnTransMemberName;
    }
}
