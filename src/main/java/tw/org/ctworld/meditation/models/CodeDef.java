package tw.org.ctworld.meditation.models;

import javax.persistence.*;

@Entity
@Table(name="code_def")
public class CodeDef extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 20)
    private String codeCategory;
    @Column(length = 50)
    private String codeGroup;
    @Column(length = 50)
    private String codeGroupId;
    @Column(length = 50)
    private String codeName;
    @Column(length = 20)
    private String codeId;
    @Column(columnDefinition = "INT(5)")
    private int orderNum;
    @Column(length = 50)
    private String codeContentValue;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCodeCategory() {
        return codeCategory;
    }

    public void setCodeCategory(String codeCategory) {
        this.codeCategory = codeCategory;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeGroup() {
        return codeGroup;
    }

    public void setCodeGroup(String codeGroup) {
        this.codeGroup = codeGroup;
    }

    public String getCodeGroupId() {
        return codeGroupId;
    }

    public void setCodeGroupId(String codeGroupId) {
        this.codeGroupId = codeGroupId;
    }

    public String getCodeContentValue() {
        return codeContentValue;
    }

    public void setCodeContentValue(String codeContentValue) {
        this.codeContentValue = codeContentValue;
    }
}
