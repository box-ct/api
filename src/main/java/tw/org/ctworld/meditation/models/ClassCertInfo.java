package tw.org.ctworld.meditation.models;

import javax.persistence.*;

@Entity
@Table(name="class_cert_info")
public class ClassCertInfo extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 50)
    private String unitId;
    @Column(length = 50)
    private String unitName;
    @Column(length = 50, unique = true)
    private String certTemplateId;
    @Column(length = 50)
    private String certTypeName;
    @Column(length = 50)
    private String certTemplateName;
    @Column(length = 50)
    private String printerId;
    @Column(length = 50)
    private String printerName;
    @Column(columnDefinition = "TEXT")
    private String description;
    @Column(columnDefinition = "TEXT")
    private String note;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getCertTemplateId() {
        return certTemplateId;
    }

    public void setCertTemplateId(String certTemplateId) {
        this.certTemplateId = certTemplateId;
    }

    public String getCertTypeName() {
        return certTypeName;
    }

    public void setCertTypeName(String certTypeName) {
        this.certTypeName = certTypeName;
    }

    public String getCertTemplateName() {
        return certTemplateName;
    }

    public void setCertTemplateName(String certTemplateName) {
        this.certTemplateName = certTemplateName;
    }

    public String getPrinterId() {
        return printerId;
    }

    public void setPrinterId(String printerId) {
        this.printerId = printerId;
    }

    public String getPrinterName() {
        return printerName;
    }

    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    public String getDescription() {
        return description == null ? "" : description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
