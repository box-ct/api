package tw.org.ctworld.meditation.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="record_import_log")
public class RecordImportLog extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 9)
    private String unitId;
    @Column(length = 30)
    private String unitName;
    @Column(length = 60)
    private String aliasName;
    @Column(length = 30)
    private String ctDharmaName;
    private Date birthDate;
    @Column(length = 75)
    private String twIdNum;
    @Column(length = 13)
    private String classEnrollFormId;
    @Column(length = 9)
    private String memberId;
    @Column(columnDefinition = "TEXT ")
    private String importErrorContent;
    @Column(columnDefinition = "TEXT ")
    private String successUpdateContent;
    @Column(length = 30)
    private String importResultType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getImportErrorContent() {
        return importErrorContent;
    }

    public void setImportErrorContent(String importErrorContent) {
        this.importErrorContent = importErrorContent;
    }

    public String getSuccessUpdateContent() {
        return successUpdateContent;
    }

    public void setSuccessUpdateContent(String successUpdateContent) {
        this.successUpdateContent = successUpdateContent;
    }

    public String getImportResultType() {
        return importResultType;
    }

    public void setImportResultType(String importResultType) {
        this.importResultType = importResultType;
    }
}
