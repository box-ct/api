/*
* Auto Generated
* Based on name: event_main
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain004_1Object;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain006Request;
import tw.org.ctworld.meditation.beans.Makeup.Unit;

import static tw.org.ctworld.meditation.libs.CommonUtils.ParseDate;
import static tw.org.ctworld.meditation.libs.CommonUtils.ParseTime;

@Entity
@Table(name="event_main")
public class EventMain extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(columnDefinition = "TEXT")
    private String attendUnitList;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date changeEndDate;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date enrollEndDate;

    @Column(length = 50)
    private String eventCategoryId;

    @Column(length = 50)
    private String eventCategoryName;

    @Column(length = 50)
    private String eventCategoryColorCode;

    @Column(length = 50)
    private String eventCreatorUnitId;

    @Column(length = 50)
    private String eventCreatorUnitName;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date eventEndDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date eventEndTime;

    @Column(length = 50, unique = true)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @Column(columnDefinition = "TEXT")
    private String eventNote;

    @Column(length = 50)
    private String eventPlace;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date eventStartDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date eventStartTime;

    @Column(length = 50)
    private String eventYear;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date finalDate;

    @Column(columnDefinition = "TEXT")
    private String idType1List;

    @Column(columnDefinition = "TEXT")
    private String idType2List;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isAvailableForAllUnit;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCreatedByCt;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCreatedByUnit;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isHeldInUnit;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isMultipleEnroll;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedAbbotSigned;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedCtApproved;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isOnlyForMaster;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isOtherUnitCanApplyThisUnitEvent;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isShowBodhiPreceptTab;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isShowPrecept5Tab;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isShowPrecept8Tab;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isShowRefugeTab;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isShowSummerDonationButton;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isShowZen7Tab;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isSyncCtTable;

    @Column(columnDefinition = "TEXT")
    private String noTimeLimitUnitList;

    @Column(length = 50)
    private String sponsorUnitId;

    @Column(length = 50)
    private String sponsorUnitName;

    @Column(columnDefinition = "INT(6)")
    private int status;

    @Column(length = 50)
    private String summerDonationType;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAttendUnitList() {
        return this.attendUnitList;
    }

    public void setAttendUnitList(String attendUnitList) {
        this.attendUnitList = attendUnitList;
    }

    public Date getChangeEndDate() {
        return this.changeEndDate;
    }

    public void setChangeEndDate(Date changeEndDate) {
        this.changeEndDate = changeEndDate;
    }

    public Date getEnrollEndDate() {
        return this.enrollEndDate;
    }

    public void setEnrollEndDate(Date enrollEndDate) {
        this.enrollEndDate = enrollEndDate;
    }

    public String getEventCategoryId() {
        return this.eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return this.eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public String getEventCreatorUnitId() {
        return this.eventCreatorUnitId;
    }

    public void setEventCreatorUnitId(String eventCreatorUnitId) {
        this.eventCreatorUnitId = eventCreatorUnitId;
    }

    public String getEventCreatorUnitName() {
        return this.eventCreatorUnitName;
    }

    public void setEventCreatorUnitName(String eventCreatorUnitName) {
        this.eventCreatorUnitName = eventCreatorUnitName;
    }

    public Date getEventEndDate() {
        return this.eventEndDate;
    }

    public void setEventEndDate(Date eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public Date getEventEndTime() {
        return this.eventEndTime;
    }

    public void setEventEndTime(Date eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventNote() {
        return this.eventNote;
    }

    public void setEventNote(String eventNote) {
        this.eventNote = eventNote;
    }

    public String getEventPlace() {
        return this.eventPlace;
    }

    public void setEventPlace(String eventPlace) {
        this.eventPlace = eventPlace;
    }

    public Date getEventStartDate() {
        return this.eventStartDate;
    }

    public void setEventStartDate(Date eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public Date getEventStartTime() {
        return this.eventStartTime;
    }

    public void setEventStartTime(Date eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getEventYear() {
        return this.eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public Date getFinalDate() {
        return this.finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public String getIdType1List() {
        return this.idType1List;
    }

    public void setIdType1List(String idType1List) {
        this.idType1List = idType1List;
    }

    public String getIdType2List() {
        return this.idType2List;
    }

    public void setIdType2List(String idType2List) {
        this.idType2List = idType2List;
    }

    public Boolean getIsAvailableForAllUnit() {
        return this.isAvailableForAllUnit == null ? false : isAvailableForAllUnit;
    }

    public void setIsAvailableForAllUnit(Boolean isAvailableForAllUnit) {
        this.isAvailableForAllUnit = isAvailableForAllUnit;
    }

    public Boolean getIsCreatedByCt() {
        return this.isCreatedByCt == null ? false : isCreatedByCt;
    }

    public void setIsCreatedByCt(Boolean isCreatedByCt) {
        this.isCreatedByCt = isCreatedByCt;
    }

    public Boolean getIsCreatedByUnit() {
        return this.isCreatedByUnit == null ? false : isCreatedByUnit;
    }

    public void setIsCreatedByUnit(Boolean isCreatedByUnit) {
        this.isCreatedByUnit = isCreatedByUnit;
    }

    public Boolean getIsHeldInUnit() {
        return this.isHeldInUnit == null ? false : isHeldInUnit;
    }

    public void setIsHeldInUnit(Boolean isHeldInUnit) {
        this.isHeldInUnit = isHeldInUnit;
    }

    public Boolean getIsMultipleEnroll() {
        return this.isMultipleEnroll == null ? false : isMultipleEnroll;
    }

    public void setIsMultipleEnroll(Boolean isMultipleEnroll) {
        this.isMultipleEnroll = isMultipleEnroll;
    }

    public Boolean getIsNeedAbbotSigned() {
        return this.isNeedAbbotSigned == null ? false : isNeedAbbotSigned;
    }

    public void setIsNeedAbbotSigned(Boolean isNeedAbbotSigned) {
        this.isNeedAbbotSigned = isNeedAbbotSigned;
    }

    public Boolean getIsNeedCtApproved() {
        return this.isNeedCtApproved == null ? false : isNeedCtApproved;
    }

    public void setIsNeedCtApproved(Boolean isNeedCtApproved) {
        this.isNeedCtApproved = isNeedCtApproved;
    }

    public Boolean getIsOnlyForMaster() {
        return this.isOnlyForMaster == null ? false : isOnlyForMaster;
    }

    public void setIsOnlyForMaster(Boolean isOnlyForMaster) {
        this.isOnlyForMaster = isOnlyForMaster;
    }

    public Boolean getIsOtherUnitCanApplyThisUnitEvent() {
        return this.isOtherUnitCanApplyThisUnitEvent == null ? false : isOtherUnitCanApplyThisUnitEvent;
    }

    public void setIsOtherUnitCanApplyThisUnitEvent(Boolean isOtherUnitCanApplyThisUnitEvent) {
        this.isOtherUnitCanApplyThisUnitEvent = isOtherUnitCanApplyThisUnitEvent;
    }

    public Boolean getIsShowBodhiPreceptTab() {
        return this.isShowBodhiPreceptTab == null ? false : isShowBodhiPreceptTab;
    }

    public void setIsShowBodhiPreceptTab(Boolean isShowBodhiPreceptTab) {
        this.isShowBodhiPreceptTab = isShowBodhiPreceptTab;
    }

    public Boolean getIsShowPrecept5Tab() {
        return this.isShowPrecept5Tab == null ? false : isShowPrecept5Tab;
    }

    public void setIsShowPrecept5Tab(Boolean isShowPrecept5Tab) {
        this.isShowPrecept5Tab = isShowPrecept5Tab;
    }

    public Boolean getIsShowPrecept8Tab() {
        return this.isShowPrecept8Tab == null ? false : isShowPrecept8Tab;
    }

    public void setIsShowPrecept8Tab(Boolean isShowPrecept8Tab) {
        this.isShowPrecept8Tab = isShowPrecept8Tab;
    }

    public Boolean getIsShowRefugeTab() {
        return this.isShowRefugeTab == null ? false : isShowRefugeTab;
    }

    public void setIsShowRefugeTab(Boolean isShowRefugeTab) {
        this.isShowRefugeTab = isShowRefugeTab;
    }

    public Boolean getIsShowSummerDonationButton() {
        return this.isShowSummerDonationButton == null ? false : isShowSummerDonationButton;
    }

    public void setIsShowSummerDonationButton(Boolean isShowSummerDonationButton) {
        this.isShowSummerDonationButton = isShowSummerDonationButton;
    }

    public Boolean getIsShowZen7Tab() {
        return this.isShowZen7Tab == null ? false : isShowZen7Tab;
    }

    public void setIsShowZen7Tab(Boolean isShowZen7Tab) {
        this.isShowZen7Tab = isShowZen7Tab;
    }

    public Boolean getIsSyncCtTable() {
        return this.isSyncCtTable == null ? false : isSyncCtTable;
    }

    public void setIsSyncCtTable(Boolean isSyncCtTable) {
        this.isSyncCtTable = isSyncCtTable;
    }

    public String getNoTimeLimitUnitList() {
        return this.noTimeLimitUnitList;
    }

    public void setNoTimeLimitUnitList(String noTimeLimitUnitList) {
        this.noTimeLimitUnitList = noTimeLimitUnitList;
    }

    public String getSponsorUnitId() {
        return this.sponsorUnitId;
    }

    public void setSponsorUnitId(String sponsorUnitId) {
        this.sponsorUnitId = sponsorUnitId;
    }

    public String getSponsorUnitName() {
        return this.sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSummerDonationType() {
        return this.summerDonationType;
    }

    public void setSummerDonationType(String summerDonationType) {
        this.summerDonationType = summerDonationType;
    }

    public String getEventCategoryColorCode() {
        return eventCategoryColorCode;
    }

    public void setEventCategoryColorCode(String eventCategoryColorCode) {
        this.eventCategoryColorCode = eventCategoryColorCode;
    }

    public void copyEventMain(EventMain eventMain) {

        this.attendUnitList = eventMain.getAttendUnitList();
        this.changeEndDate = eventMain.getChangeEndDate();
        this.enrollEndDate = eventMain.getEnrollEndDate();
        this.eventCategoryId = eventMain.getEventCategoryId();
        this.eventCategoryName = eventMain.getEventCategoryName();
        this.eventCategoryColorCode = eventMain.getEventCategoryColorCode();
        this.eventCreatorUnitId = eventMain.getEventCreatorUnitId();
        this.eventCreatorUnitName = eventMain.getEventCreatorUnitName();
        this.eventEndDate = eventMain.getEventEndDate();
        this.eventEndTime = eventMain.getEventEndTime();
        //this.eventId = eventMain.eventId();
        this.eventName = eventMain.getEventName();
        this.eventNote = eventMain.getEventNote();
        this.eventPlace = eventMain.getEventPlace();
        this.eventStartDate = eventMain.getEventStartDate();
        this.eventStartTime = eventMain.getEventStartTime();
        this.eventYear = eventMain.getEventYear();
        this.finalDate = eventMain.getFinalDate();
        this.idType1List = eventMain.getIdType1List();
        this.idType2List = eventMain.getIdType2List();
        this.isAvailableForAllUnit = eventMain.getIsAvailableForAllUnit();
        this.isCreatedByCt = eventMain.getIsCreatedByCt();
        this.isCreatedByUnit = eventMain.getIsCreatedByUnit();
        this.isHeldInUnit = eventMain.getIsHeldInUnit();
        this.isMultipleEnroll = eventMain.getIsMultipleEnroll();
        this.isNeedAbbotSigned = eventMain.getIsNeedAbbotSigned();
        this.isNeedCtApproved = eventMain.getIsNeedCtApproved();
        this.isOnlyForMaster = eventMain.getIsOnlyForMaster();
        this.isOtherUnitCanApplyThisUnitEvent = eventMain.getIsOtherUnitCanApplyThisUnitEvent();
        this.isShowBodhiPreceptTab = eventMain.getIsShowBodhiPreceptTab();
        this.isShowPrecept5Tab = eventMain.getIsShowPrecept5Tab();
        this.isShowPrecept8Tab = eventMain.getIsShowPrecept8Tab();
        this.isShowRefugeTab = eventMain.getIsShowRefugeTab();
        this.isShowSummerDonationButton = eventMain.getIsShowSummerDonationButton();
        this.isShowZen7Tab = eventMain.getIsShowZen7Tab();
        this.isSyncCtTable = eventMain.getIsSyncCtTable();
        this.noTimeLimitUnitList = eventMain.getNoTimeLimitUnitList();
        this.sponsorUnitId = eventMain.getSponsorUnitId();
        this.sponsorUnitName = eventMain.getSponsorUnitName();
        this.status = eventMain.getStatus();
        this.summerDonationType = eventMain.getSummerDonationType();
    }

    public void copyEventMain(CtEventMaintain006Request ctEventMaintain006Request) {

        this.attendUnitList = ctEventMaintain006Request.getAttendUnitList();
        this.changeEndDate = ParseDate(ctEventMaintain006Request.getChangeEndDate());
        this.enrollEndDate = ParseDate(ctEventMaintain006Request.getEnrollEndDate());
        this.eventCategoryId = ctEventMaintain006Request.getEventCategoryId();
        this.eventCategoryName = ctEventMaintain006Request.getEventCategoryName();
        this.eventCreatorUnitId = ctEventMaintain006Request.getEventCreatorUnitId();
        this.eventCategoryColorCode = ctEventMaintain006Request.getEventCategoryColorCode();
        this.eventCreatorUnitName = ctEventMaintain006Request.getEventCreatorUnitName();
        this.eventEndDate = ParseDate(ctEventMaintain006Request.getEventEndDate());
        this.eventEndTime = ParseTime(ctEventMaintain006Request.getEventEndTime());
//        this.eventId = ctEventMaintain006Request.getEventId();
        this.eventName = ctEventMaintain006Request.getEventName();
        this.eventNote = ctEventMaintain006Request.getEventNote();
        this.eventPlace = ctEventMaintain006Request.getEventPlace();
        this.eventStartDate = ParseDate(ctEventMaintain006Request.getEventStartDate());
        this.eventStartTime = ParseTime(ctEventMaintain006Request.getEventStartTime());
        this.eventYear = ctEventMaintain006Request.getEventYear();
        this.finalDate = ParseDate(ctEventMaintain006Request.getFinalDate());
        this.idType1List = ctEventMaintain006Request.getIdType1List();
        this.idType2List = ctEventMaintain006Request.getIdType2List();
        this.isAvailableForAllUnit = ctEventMaintain006Request.getIsAvailableForAllUnit();
        this.isCreatedByCt = ctEventMaintain006Request.getIsCreatedByCt();
        this.isCreatedByUnit = ctEventMaintain006Request.getIsCreatedByUnit();
        this.isHeldInUnit = ctEventMaintain006Request.getIsHeldInUnit();
        this.isMultipleEnroll = ctEventMaintain006Request.getIsMultipleEnroll();
        this.isNeedAbbotSigned = ctEventMaintain006Request.getIsNeedAbbotSigned();
        this.isNeedCtApproved = ctEventMaintain006Request.getIsNeedAbbotSigned();
        this.isOnlyForMaster = ctEventMaintain006Request.getIsOnlyForMaster();
        this.isOtherUnitCanApplyThisUnitEvent = ctEventMaintain006Request.getIsOtherUnitCanApplyThisUnitEvent();
        this.isShowBodhiPreceptTab = ctEventMaintain006Request.getIsShowBodhiPreceptTab();
        this.isShowPrecept5Tab = ctEventMaintain006Request.getIsShowPrecept5Tab();
        this.isShowPrecept8Tab = ctEventMaintain006Request.getIsShowPrecept8Tab();
        this.isShowRefugeTab = ctEventMaintain006Request.getIsShowRefugeTab();
        this.isShowSummerDonationButton = ctEventMaintain006Request.getIsShowSummerDonationButton();
        this.isShowZen7Tab = ctEventMaintain006Request.getIsShowZen7Tab();
        this.isSyncCtTable = ctEventMaintain006Request.getIsSyncCtTable();
        this.noTimeLimitUnitList = ctEventMaintain006Request.getNoTimeLimitUnitList();
        this.sponsorUnitId = ctEventMaintain006Request.getSponsorUnitId();
        this.sponsorUnitName = ctEventMaintain006Request.getSponsorUnitName();
        this.status = ctEventMaintain006Request.getStatus();
        this.summerDonationType = ctEventMaintain006Request.getSummerDonationType();
    }

    public void copyData(CtEventMaintain004_1Object data) {
        setAttendUnitList(data.getAttendUnitList());
        setChangeEndDate(ParseDate(data.getChangeEndDate()));
        setEnrollEndDate(ParseDate(data.getEnrollEndDate()));
        setEventCategoryId(data.getEventCategoryId());
        setEventCategoryName(data.getEventCategoryName());
        setEventCreatorUnitId(data.getEventCreatorUnitId());
        setEventCreatorUnitName(data.getEventCreatorUnitName());
        setEventEndDate(ParseDate(data.getEventEndDate()));
        setEventEndTime(ParseTime(data.getEventEndTime()));
        setEventName(data.getEventName());
        setEventNote(data.getEventNote());
        setEventPlace(data.getEventPlace());
        setEventStartDate(ParseDate(data.getEventStartDate()));
        setEventStartTime(ParseTime(data.getEventStartTime()));
        setEventYear(data.getEventStartDate().substring(0, 4));
        setFinalDate(ParseDate(data.getFinalDate()));
        setIdType1List(data.getIdType1List());
        setIdType2List(data.getIdType2List());
        setIsAvailableForAllUnit(data.getIsAvailableForAllUnit());
        setIsMultipleEnroll(data.getIsMultipleEnroll());
        setIsNeedAbbotSigned(data.getIsNeedAbbotSigned());
        setIsNeedCtApproved(data.getIsNeedCtApproved());
        setIsOnlyForMaster(data.getIsOnlyForMaster());
        setIsShowBodhiPreceptTab(data.getIsShowBodhiPreceptTab());
        setIsShowPrecept5Tab(data.getIsShowPrecept5Tab());
        setIsShowPrecept8Tab(data.getIsShowPrecept8Tab());
        setIsShowRefugeTab(data.getIsShowRefugeTab());
        setIsShowSummerDonationButton(data.getIsShowSummerDonationButton());
        setIsShowZen7Tab(data.getIsShowZen7Tab());
        setIsSyncCtTable(data.getIsSyncCtTable());
        setNoTimeLimitUnitList(data.getNoTimeLimitUnitList());
        setSponsorUnitId(data.getSponsorUnitId());
        setSponsorUnitName(data.getSponsorUnitName());
        setStatus(data.getStatus());
        setSummerDonationType(data.getSummerDonationType());
        setIsOtherUnitCanApplyThisUnitEvent(data.getIsOtherUnitCanApplyThisUnitEvent());
    }

    public List<String> getAttendUnitIds() {
        List<String> units;

        if (this.attendUnitList != null) {
            units = Arrays.asList(this.attendUnitList.split("/"));
        } else {
            units = new ArrayList<>();
        }

        return units;
    }

    public List<Unit> getAttendUnits() {
        List<Unit> units = new ArrayList<>();

        if (this.attendUnitList != null) {
            List<String> unitStrList = Arrays.asList(this.attendUnitList.split("/"));

            if (unitStrList.size() > 0) {
                for (String unit : unitStrList) {
                    try {
                        String unitId = unit.substring(0, 9);
                        String unitName = unit.substring(10);

                        units.add(new Unit(unitId, unitName));
                    } catch (Exception e) {
                    }
                }
            }
        }

        return units;
    }
}
