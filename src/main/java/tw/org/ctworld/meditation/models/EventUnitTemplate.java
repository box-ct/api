/*
 * Auto Generated
 * Based on name: event_unit_template
 * Date: 2019-06-13 17:09:44
 */

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import tw.org.ctworld.meditation.beans.EventEnrollMember.EventEnrollRequest;
import tw.org.ctworld.meditation.libs.CommonUtils;

@Entity
@Table(name = "event_unit_template")
public class EventUnitTemplate extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date arriveCtDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date arriveCtTime;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date ctDormEndDate;

    @Column(length = 50)
    private String ctDormPurpose;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date ctDormStartDate;

    @Column(columnDefinition = "TEXT")
    private String dormNote;

    @Column(columnDefinition = "TEXT")
    private String enrollGroupName;

    @Column(columnDefinition = "TEXT")
    private String enrollNote;

    @Column(length = 50)
    private String eventCategoryId;

    @Column(length = 50)
    private String eventCreatorUnitId;

    @Column(length = 50)
    private String eventCreatorUnitName;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date eventEndDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date eventEndTime;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date eventStartDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date eventStartTime;

    @Column(columnDefinition = "TEXT")
    private String idType1;

    @Column(columnDefinition = "TEXT")
    private String idType2;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCancelPrecept5ThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCancelRefugeThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCtDorm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isEarlyBookCtDorm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isForMaster;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isGoCtBySelf;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedExtraPrecept5CertThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedPrecept5RobeThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedTranslator;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNotDorm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isRelativePreceptOrZen7Vlntr;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isRelativeTakePreceptOrZen7;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isShowInKiosk;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeExtraPrecept5ThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakePrecept5ThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeRefugeThisTime;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date leaveCtDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date leaveCtTime;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date leaveUnitDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date leaveUnitTime;

    @Column(length = 50)
    private String sponsorUnitId;

    @Column(length = 50)
    private String sponsorUnitName;

    @Column(length = 50)
    private String templateName;

    @Column(columnDefinition = "TEXT")
    private String templateNote;

    @Column(length = 50, unique = true)
    private String templateUniqueId;

    @Column(length = 50)
    private String translateLanguage;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date vlntrEndDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date vlntrEndTime;

    @Column(columnDefinition = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Taipei")
    private Date vlntrStartDate;

    @Column(columnDefinition = "Time")
    @JsonFormat(pattern = "HH:mm", timezone = "Asia/Taipei")
    private Date vlntrStartTime;


    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getArriveCtDate() {
        return this.arriveCtDate;
    }

    public void setArriveCtDate(Date arriveCtDate) {
        this.arriveCtDate = arriveCtDate;
    }

    public Date getArriveCtTime() {
        return this.arriveCtTime;
    }

    public void setArriveCtTime(Date arriveCtTime) {
        this.arriveCtTime = arriveCtTime;
    }

    public Date getCtDormEndDate() {
        return this.ctDormEndDate;
    }

    public void setCtDormEndDate(Date ctDormEndDate) {
        this.ctDormEndDate = ctDormEndDate;
    }

    public String getCtDormPurpose() {
        return ctDormPurpose;
    }

    public void setCtDormPurpose(String ctDormPurpose) {
        this.ctDormPurpose = ctDormPurpose;
    }

    public Date getCtDormStartDate() {
        return this.ctDormStartDate;
    }

    public void setCtDormStartDate(Date ctDormStartDate) {
        this.ctDormStartDate = ctDormStartDate;
    }

    public String getDormNote() {
        return this.dormNote;
    }

    public void setDormNote(String dormNote) {
        this.dormNote = dormNote;
    }

    public String getEnrollGroupName() {
        return this.enrollGroupName;
    }

    public void setEnrollGroupName(String enrollGroupName) {
        this.enrollGroupName = enrollGroupName;
    }

    public String getEnrollNote() {
        return this.enrollNote;
    }

    public void setEnrollNote(String enrollNote) {
        this.enrollNote = enrollNote;
    }

    public String getEventCategoryId() {
        return this.eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCreatorUnitId() {
        return this.eventCreatorUnitId;
    }

    public void setEventCreatorUnitId(String eventCreatorUnitId) {
        this.eventCreatorUnitId = eventCreatorUnitId;
    }

    public String getEventCreatorUnitName() {
        return this.eventCreatorUnitName;
    }

    public void setEventCreatorUnitName(String eventCreatorUnitName) {
        this.eventCreatorUnitName = eventCreatorUnitName;
    }

    public Date getEventEndDate() {
        return this.eventEndDate;
    }

    public void setEventEndDate(Date eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public Date getEventEndTime() {
        return this.eventEndTime;
    }

    public void setEventEndTime(Date eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getEventStartDate() {
        return this.eventStartDate;
    }

    public void setEventStartDate(Date eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public Date getEventStartTime() {
        return this.eventStartTime;
    }

    public void setEventStartTime(Date eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getIdType1() {
        return this.idType1;
    }

    public void setIdType1(String idType1) {
        this.idType1 = idType1;
    }

    public String getIdType2() {
        return this.idType2;
    }

    public void setIdType2(String idType2) {
        this.idType2 = idType2;
    }

    public Boolean getIsCancelPrecept5ThisTime() {
        return isCancelPrecept5ThisTime == null ? false : isCancelPrecept5ThisTime;
    }

    public void setIsCancelPrecept5ThisTime(Boolean cancelPrecept5ThisTime) {
        isCancelPrecept5ThisTime = cancelPrecept5ThisTime;
    }

    public Boolean getIsCancelRefugeThisTime() {
        return isCancelRefugeThisTime == null ? false : isCancelRefugeThisTime;
    }

    public void setIsCancelRefugeThisTime(Boolean cancelRefugeThisTime) {
        isCancelRefugeThisTime = cancelRefugeThisTime;
    }

    public Boolean getIsCtDorm() {
        return this.isCtDorm == null ? false : isCtDorm;
    }

    public void setIsCtDorm(Boolean isCtDorm) {
        this.isCtDorm = isCtDorm;
    }

    public Boolean getIsEarlyBookCtDorm() {
        return isEarlyBookCtDorm == null ? false : isEarlyBookCtDorm;
    }

    public void setIsEarlyBookCtDorm(Boolean earlyBookCtDorm) {
        isEarlyBookCtDorm = earlyBookCtDorm;
    }

    public Boolean getIsForMaster() {
        return this.isForMaster == null ? false : isForMaster;
    }

    public void setIsForMaster(Boolean isForMaster) {
        this.isForMaster = isForMaster;
    }

    public Boolean getIsGoCtBySelf() {
        return this.isGoCtBySelf == null ? false : isGoCtBySelf;
    }

    public void setIsGoCtBySelf(Boolean isGoCtBySelf) {
        this.isGoCtBySelf = isGoCtBySelf;
    }

    public Boolean getIsNeedExtraPrecept5CertThisTime() {
        return isNeedExtraPrecept5CertThisTime == null ? false : isNeedExtraPrecept5CertThisTime;
    }

    public void setIsNeedExtraPrecept5CertThisTime(Boolean needExtraPrecept5CertThisTime) {
        isNeedExtraPrecept5CertThisTime = needExtraPrecept5CertThisTime;
    }

    public Boolean getIsNeedPrecept5RobeThisTime() {
        return isNeedPrecept5RobeThisTime == null ? false : isNeedPrecept5RobeThisTime;
    }

    public void setIsNeedPrecept5RobeThisTime(Boolean needPrecept5RobeThisTime) {
        isNeedPrecept5RobeThisTime = needPrecept5RobeThisTime;
    }

    public Boolean getIsNeedTranslator() {
        return isNeedTranslator == null ? false : isNeedTranslator;
    }

    public void setIsNeedTranslator(Boolean needTranslator) {
        isNeedTranslator = needTranslator;
    }

    public Boolean getIsNotDorm() {
        return this.isNotDorm == null ? false : isNotDorm;
    }

    public void setIsNotDorm(Boolean isNotDorm) {
        this.isNotDorm = isNotDorm;
    }

    public Boolean getIsRelativeTakePreceptOrZen7() {
        return isRelativeTakePreceptOrZen7 == null ? false : isRelativeTakePreceptOrZen7;
    }

    public void setIsRelativeTakePreceptOrZen7(Boolean relativeTakePreceptOrZen7) {
        isRelativeTakePreceptOrZen7 = relativeTakePreceptOrZen7;
    }

    public Boolean getIsRelativePreceptOrZen7Vlntr() {
        return isRelativePreceptOrZen7Vlntr == null ? false : isRelativePreceptOrZen7Vlntr;
    }

    public void setIsRelativePreceptOrZen7Vlntr(Boolean relativePreceptOrZen7Vlntr) {
        isRelativePreceptOrZen7Vlntr = relativePreceptOrZen7Vlntr;
    }

    public Boolean getIsShowInKiosk() {
        return this.isShowInKiosk == null ? false : isShowInKiosk;
    }

    public void setIsShowInKiosk(Boolean isShowInKiosk) {
        this.isShowInKiosk = isShowInKiosk;
    }

    public Boolean getIsTakeExtraPrecept5ThisTime() {
        return isTakeExtraPrecept5ThisTime == null ? false : isTakeExtraPrecept5ThisTime;
    }

    public void setIsTakeExtraPrecept5ThisTime(Boolean takeExtraPrecept5ThisTime) {
        isTakeExtraPrecept5ThisTime = takeExtraPrecept5ThisTime;
    }

    public Boolean getIsTakePrecept5ThisTime() {
        return isTakePrecept5ThisTime == null ? false : isTakePrecept5ThisTime;
    }

    public void setIsTakePrecept5ThisTime(Boolean takePrecept5ThisTime) {
        isTakePrecept5ThisTime = takePrecept5ThisTime;
    }

    public Boolean getIsTakeRefugeThisTime() {
        return isTakeRefugeThisTime == null ? false : isTakeRefugeThisTime;
    }

    public void setIsTakeRefugeThisTime(Boolean takeRefugeThisTime) {
        isTakeRefugeThisTime = takeRefugeThisTime;
    }

    public Date getLeaveCtDate() {
        return this.leaveCtDate;
    }

    public void setLeaveCtDate(Date leaveCtDate) {
        this.leaveCtDate = leaveCtDate;
    }

    public Date getLeaveCtTime() {
        return this.leaveCtTime;
    }

    public void setLeaveCtTime(Date leaveCtTime) {
        this.leaveCtTime = leaveCtTime;
    }

    public Date getLeaveUnitDate() {
        return this.leaveUnitDate;
    }

    public void setLeaveUnitDate(Date leaveUnitDate) {
        this.leaveUnitDate = leaveUnitDate;
    }

    public Date getLeaveUnitTime() {
        return this.leaveUnitTime;
    }

    public void setLeaveUnitTime(Date leaveUnitTime) {
        this.leaveUnitTime = leaveUnitTime;
    }

    public String getSponsorUnitId() {
        return this.sponsorUnitId;
    }

    public void setSponsorUnitId(String sponsorUnitId) {
        this.sponsorUnitId = sponsorUnitId;
    }

    public String getSponsorUnitName() {
        return this.sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateNote() {
        return this.templateNote;
    }

    public void setTemplateNote(String templateNote) {
        this.templateNote = templateNote;
    }

    public String getTemplateUniqueId() {
        return this.templateUniqueId;
    }

    public void setTemplateUniqueId(String templateUniqueId) {
        this.templateUniqueId = templateUniqueId;
    }

    public String getTranslateLanguage() {
        return translateLanguage;
    }

    public void setTranslateLanguage(String translateLanguage) {
        this.translateLanguage = translateLanguage;
    }

    public String getUnitId() {
        return this.unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return this.unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Date getVlntrEndDate() {
        return this.vlntrEndDate;
    }

    public void setVlntrEndDate(Date vlntrEndDate) {
        this.vlntrEndDate = vlntrEndDate;
    }

    public Date getVlntrEndTime() {
        return this.vlntrEndTime;
    }

    public void setVlntrEndTime(Date vlntrEndTime) {
        this.vlntrEndTime = vlntrEndTime;
    }

    public Date getVlntrStartDate() {
        return this.vlntrStartDate;
    }

    public void setVlntrStartDate(Date vlntrStartDate) {
        this.vlntrStartDate = vlntrStartDate;
    }

    public Date getVlntrStartTime() {
        return this.vlntrStartTime;
    }

    public void setVlntrStartTime(Date vlntrStartTime) {
        this.vlntrStartTime = vlntrStartTime;
    }

    public void copyRequest(EventEnrollRequest request) {
        this.eventCategoryId = CommonUtils.Empty2Null(request.getEventCategoryId());
        this.eventId = request.getEventId();
        this.eventName = request.getEventName();
        this.eventCreatorUnitId = request.getEventCreatorUnitId();
        this.eventCreatorUnitName = request.getEventCreatorUnitName();
        this.sponsorUnitId = request.getSponsorUnitId();
        this.sponsorUnitName = request.getSponsorUnitName();
        this.eventStartDate = CommonUtils.ParseDate(request.getEventStartDate());
        this.eventStartTime = CommonUtils.ParseTime(request.getEventStartTime());
        this.eventEndDate = CommonUtils.ParseDate(request.getEventEndDate());
        this.eventEndTime = CommonUtils.ParseTime(request.getEventEndTime());
        this.templateUniqueId = request.getTemplateUniqueId();
        this.templateName = request.getTemplateName();
        this.templateNote = request.getTemplateNote();
        this.isShowInKiosk = request.getIsShowInKiosk();
        this.isForMaster = request.getIsForMaster();
        this.leaveUnitDate = CommonUtils.ParseDate(request.getLeaveUnitDate());
        this.leaveUnitTime = CommonUtils.ParseTime(request.getLeaveUnitTime());
        this.arriveCtDate = CommonUtils.ParseDate(request.getArriveCtDate());
        this.arriveCtTime = CommonUtils.ParseTime(request.getArriveCtTime());
        this.leaveCtDate = CommonUtils.ParseDate(request.getLeaveCtDate());
        this.leaveCtTime = CommonUtils.ParseTime(request.getLeaveCtTime());
        this.enrollGroupName = request.getEnrollGroupName();
        this.idType1 = request.getIdType1();
        this.idType2 = request.getIdType2();
        this.vlntrStartDate = CommonUtils.ParseDate(request.getVlntrStartDate());
        this.vlntrStartTime = CommonUtils.ParseTime(request.getVlntrStartTime());
        this.vlntrEndDate = CommonUtils.ParseDate(request.getVlntrEndDate());
        this.vlntrEndTime = CommonUtils.ParseTime(request.getVlntrEndTime());
        this.enrollNote = request.getEnrollNote();
        this.isCtDorm = request.getIsCtDorm();
        this.isNotDorm = request.getIsNotDorm();
        this.ctDormStartDate = CommonUtils.ParseDate(request.getCtDormStartDate());
        this.ctDormEndDate = CommonUtils.ParseDate(request.getCtDormEndDate());
        this.dormNote = request.getDormNote();
        this.isGoCtBySelf = request.getIsGoCtBySelf();
        this.isNeedTranslator = request.getIsNeedTranslator();
        this.translateLanguage = request.getTranslateLanguage();
        this.isTakeRefugeThisTime = request.getIsTakeRefugeThisTime();
        this.isCancelRefugeThisTime = request.getIsCancelRefugeThisTime();
        this.isTakePrecept5ThisTime = request.getIsTakePrecept5ThisTime();
        this.isCancelPrecept5ThisTime = request.getIsCancelPrecept5ThisTime();
        this.isTakeExtraPrecept5ThisTime = request.getIsTakeExtraPrecept5ThisTime();
        this.isNeedPrecept5RobeThisTime = request.getIsNeedPrecept5RobeThisTime();
        this.isNeedExtraPrecept5CertThisTime = request.getIsNeedExtraPrecept5CertThisTime();
        this.isEarlyBookCtDorm = request.getIsEarlyBookCtDorm();
        this.ctDormPurpose = request.getCtDormPurpose();
        this.isRelativeTakePreceptOrZen7 = request.getIsRelativeTakePreceptOrZen7();
        this.isRelativePreceptOrZen7Vlntr = request.getIsRelativePreceptOrZen7Vlntr();
    }
}
