/*
* Auto Generated
* Based on name: event_unit_vlntr_info
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="event_unit_vlntr_info")
public class EventUnitVlntrInfo extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 100)
    private String contactPersonName;

    @Column(length = 100)
    private String contactPersonPhoneNum;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCanceled;

    private Integer unitId;

    @Column(length = 50)
    private String unitName;

    @Column(length = 50)
    private String vlntrChildCount;

    @Column(columnDefinition = "Date")
    private Date vlntrDate;

    @Column(columnDefinition = "Time")
    private Date vlntrEndTime;

    @Column(length = 50)
    private String vlntrFemaleCount;

    @Column(length = 50)
    private String vlntrMaleCount;

    @Column(columnDefinition = "TEXT")
    private String vlntrNote;

    @Column(columnDefinition = "Time")
    private Date vlntrStartTime;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContactpersonname() {
        return this.contactPersonName;
    }

    public void setContactpersonname(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactpersonphonenum() {
        return this.contactPersonPhoneNum;
    }

    public void setContactpersonphonenum(String contactPersonPhoneNum) {
        this.contactPersonPhoneNum = contactPersonPhoneNum;
    }

    public String getEventid() {
        return this.eventId;
    }

    public void setEventid(String eventId) {
        this.eventId = eventId;
    }

    public String getEventname() {
        return this.eventName;
    }

    public void setEventname(String eventName) {
        this.eventName = eventName;
    }

    public Boolean getIscanceled() {
        return this.isCanceled == null ? false : isCanceled;
    }

    public void setIscanceled(Boolean isCanceled) {
        this.isCanceled = isCanceled;
    }

    public int getUnitid() {
        return this.unitId;
    }

    public void setUnitid(int unitId) {
        this.unitId = unitId;
    }

    public String getUnitname() {
        return this.unitName;
    }

    public void setUnitname(String unitName) {
        this.unitName = unitName;
    }

    public String getVlntrchildcount() {
        return this.vlntrChildCount;
    }

    public void setVlntrchildcount(String vlntrChildCount) {
        this.vlntrChildCount = vlntrChildCount;
    }

    public Date getVlntrdate() {
        return this.vlntrDate;
    }

    public void setVlntrdate(Date vlntrDate) {
        this.vlntrDate = vlntrDate;
    }

    public Date getVlntrendtime() {
        return this.vlntrEndTime;
    }

    public void setVlntrendtime(Date vlntrEndTime) {
        this.vlntrEndTime = vlntrEndTime;
    }

    public String getVlntrfemalecount() {
        return this.vlntrFemaleCount;
    }

    public void setVlntrfemalecount(String vlntrFemaleCount) {
        this.vlntrFemaleCount = vlntrFemaleCount;
    }

    public String getVlntrmalecount() {
        return this.vlntrMaleCount;
    }

    public void setVlntrmalecount(String vlntrMaleCount) {
        this.vlntrMaleCount = vlntrMaleCount;
    }

    public String getVlntrnote() {
        return this.vlntrNote;
    }

    public void setVlntrnote(String vlntrNote) {
        this.vlntrNote = vlntrNote;
    }

    public Date getVlntrstarttime() {
        return this.vlntrStartTime;
    }

    public void setVlntrstarttime(Date vlntrStartTime) {
        this.vlntrStartTime = vlntrStartTime;
    }

}
