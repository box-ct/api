/*
* Auto Generated
* Based on name: event_ct_special_input
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="event_ct_special_input")
public class EventCtSpecialInput extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isRequired;

    @Column(length = 50)
    private String sInputCategory;

    @Column(length = 200)
    private String sInputExample;

    @Column(length = 50, unique = true)
    private String sInputId;

    @Column(length = 50)
    private String sInputName;

    private Integer sInputOrderNum;

    @Column(length = 50)
    private String sInputWriteType;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Boolean getIsRequired() {
        return this.isRequired == null ? false : isRequired;
    }

    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public String getSInputCategory() {
        return this.sInputCategory;
    }

    public void setSInputCategory(String sInputCategory) {
        this.sInputCategory = sInputCategory;
    }

    public String getSInputExample() {
        return this.sInputExample;
    }

    public void setSInputExample(String sInputExample) {
        this.sInputExample = sInputExample;
    }

    public String getSInputId() {
        return this.sInputId;
    }

    public void setSInputId(String sInputId) {
        this.sInputId = sInputId;
    }

    public String getSInputName() {
        return this.sInputName;
    }

    public void setSInputName(String sInputName) {
        this.sInputName = sInputName;
    }

    public Integer getSInputOrderNum() {
        return this.sInputOrderNum;
    }

    public void setSInputOrderNum(Integer sInputOrderNum) {
        this.sInputOrderNum = sInputOrderNum;
    }

    public String getSInputWriteType() {
        return this.sInputWriteType;
    }

    public void setSInputWriteType(String sInputWriteType) {
        this.sInputWriteType = sInputWriteType;
    }

    public void copy(EventCtSpecialInput input, EventMain eventMain) {
        this.eventId = eventMain.getEventId();
        this.eventName = eventMain.getEventName();

        this.isRequired = input.isRequired;
        this.sInputCategory = input.sInputCategory;
        this.sInputExample = input.sInputExample;
        this.sInputName = input.sInputName;
        this.sInputOrderNum = input.sInputOrderNum;
        this.sInputWriteType = input.sInputWriteType;
    }
}
