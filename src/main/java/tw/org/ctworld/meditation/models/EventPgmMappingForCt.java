/*
* Auto Generated
* Based on name: event_pgm_mapping_for_ct
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.models;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="event_pgm_mapping_for_ct")
public class EventPgmMappingForCt extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String ctPgm01;

    @Column(length = 50)
    private String ctPgm02;

    @Column(length = 50)
    private String ctPgm03;

    @Column(length = 50)
    private String ctPgm04;

    @Column(length = 50)
    private String ctPgm05;

    @Column(length = 50)
    private String ctPgm06;

    @Column(length = 50)
    private String ctPgm07;

    @Column(length = 50)
    private String ctPgm08;

    @Column(length = 50)
    private String ctPgm09;

    @Column(length = 50)
    private String ctPgm10;

    @Column(length = 50)
    private String ctPgm11;

    @Column(length = 50)
    private String ctPgm12;

    @Column(length = 50)
    private String ctPgm13;

    @Column(length = 50)
    private String ctPgm14;

    @Column(length = 50)
    private String ctPgm15;

    @Column(length = 50)
    private String ctPgm16;

    @Column(length = 50)
    private String ctPgm17;

    @Column(length = 50)
    private String ctPgm18;

    @Column(length = 50)
    private String ctPgm19;

    @Column(length = 50)
    private String ctPgm20;

    @Column(length = 50)
    private String ctPgm21;

    @Column(length = 50)
    private String ctPgm22;

    @Column(length = 50)
    private String ctPgm23;

    @Column(length = 50)
    private String ctPgm24;

    @Column(length = 50)
    private String ctPgm25;

    @Column(length = 50)
    private String ctPgm26;

    @Column(length = 50)
    private String ctPgm27;

    @Column(length = 50)
    private String ctPgm28;

    @Column(length = 50)
    private String ctPgm29;

    @Column(length = 50)
    private String ctPgm30;

    @Column(length = 50)
    private String ctPgm31;

    @Column(length = 50)
    private String ctPgm32;

    @Column(length = 50)
    private String ctPgm33;

    @Column(length = 50)
    private String ctPgm34;

    @Column(length = 50)
    private String ctPgm35;

    @Column(length = 50)
    private String ctPgm36;

    @Column(length = 50)
    private String ctPgm37;

    @Column(length = 50)
    private String ctPgm38;

    @Column(length = 50)
    private String ctPgm39;

    @Column(length = 50)
    private String ctPgm40;

    @Column(length = 50)
    private String ctPgm41;

    @Column(length = 50)
    private String ctPgm42;

    @Column(length = 50)
    private String ctPgm43;

    @Column(length = 50)
    private String ctPgm44;

    @Column(length = 50)
    private String ctPgm45;

    @Column(length = 50)
    private String ctPgm46;

    @Column(length = 50)
    private String ctPgm47;

    @Column(length = 50)
    private String ctPgm48;

    @Column(length = 50)
    private String ctPgm49;

    @Column(length = 50)
    private String ctPgm50;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCtPgm01() {
        return this.ctPgm01;
    }

    public void setCtPgm01(String ctPgm01) {
        this.ctPgm01 = ctPgm01;
    }

    public String getCtPgm02() {
        return this.ctPgm02;
    }

    public void setCtPgm02(String ctPgm02) {
        this.ctPgm02 = ctPgm02;
    }

    public String getCtPgm03() {
        return this.ctPgm03;
    }

    public void setCtPgm03(String ctPgm03) {
        this.ctPgm03 = ctPgm03;
    }

    public String getCtPgm04() {
        return this.ctPgm04;
    }

    public void setCtPgm04(String ctPgm04) {
        this.ctPgm04 = ctPgm04;
    }

    public String getCtPgm05() {
        return this.ctPgm05;
    }

    public void setCtPgm05(String ctPgm05) {
        this.ctPgm05 = ctPgm05;
    }

    public String getCtPgm06() {
        return this.ctPgm06;
    }

    public void setCtPgm06(String ctPgm06) {
        this.ctPgm06 = ctPgm06;
    }

    public String getCtPgm07() {
        return this.ctPgm07;
    }

    public void setCtPgm07(String ctPgm07) {
        this.ctPgm07 = ctPgm07;
    }

    public String getCtPgm08() {
        return this.ctPgm08;
    }

    public void setCtPgm08(String ctPgm08) {
        this.ctPgm08 = ctPgm08;
    }

    public String getCtPgm09() {
        return this.ctPgm09;
    }

    public void setCtPgm09(String ctPgm09) {
        this.ctPgm09 = ctPgm09;
    }

    public String getCtPgm10() {
        return this.ctPgm10;
    }

    public void setCtPgm10(String ctPgm10) {
        this.ctPgm10 = ctPgm10;
    }

    public String getCtPgm11() {
        return this.ctPgm11;
    }

    public void setCtPgm11(String ctPgm11) {
        this.ctPgm11 = ctPgm11;
    }

    public String getCtPgm12() {
        return this.ctPgm12;
    }

    public void setCtPgm12(String ctPgm12) {
        this.ctPgm12 = ctPgm12;
    }

    public String getCtPgm13() {
        return this.ctPgm13;
    }

    public void setCtPgm13(String ctPgm13) {
        this.ctPgm13 = ctPgm13;
    }

    public String getCtPgm14() {
        return this.ctPgm14;
    }

    public void setCtPgm14(String ctPgm14) {
        this.ctPgm14 = ctPgm14;
    }

    public String getCtPgm15() {
        return this.ctPgm15;
    }

    public void setCtPgm15(String ctPgm15) {
        this.ctPgm15 = ctPgm15;
    }

    public String getCtPgm16() {
        return this.ctPgm16;
    }

    public void setCtPgm16(String ctPgm16) {
        this.ctPgm16 = ctPgm16;
    }

    public String getCtPgm17() {
        return this.ctPgm17;
    }

    public void setCtPgm17(String ctPgm17) {
        this.ctPgm17 = ctPgm17;
    }

    public String getCtPgm18() {
        return this.ctPgm18;
    }

    public void setCtPgm18(String ctPgm18) {
        this.ctPgm18 = ctPgm18;
    }

    public String getCtPgm19() {
        return this.ctPgm19;
    }

    public void setCtPgm19(String ctPgm19) {
        this.ctPgm19 = ctPgm19;
    }

    public String getCtPgm20() {
        return this.ctPgm20;
    }

    public void setCtPgm20(String ctPgm20) {
        this.ctPgm20 = ctPgm20;
    }

    public String getCtPgm21() {
        return this.ctPgm21;
    }

    public void setCtPgm21(String ctPgm21) {
        this.ctPgm21 = ctPgm21;
    }

    public String getCtPgm22() {
        return this.ctPgm22;
    }

    public void setCtPgm22(String ctPgm22) {
        this.ctPgm22 = ctPgm22;
    }

    public String getCtPgm23() {
        return this.ctPgm23;
    }

    public void setCtPgm23(String ctPgm23) {
        this.ctPgm23 = ctPgm23;
    }

    public String getCtPgm24() {
        return this.ctPgm24;
    }

    public void setCtPgm24(String ctPgm24) {
        this.ctPgm24 = ctPgm24;
    }

    public String getCtPgm25() {
        return this.ctPgm25;
    }

    public void setCtPgm25(String ctPgm25) {
        this.ctPgm25 = ctPgm25;
    }

    public String getCtPgm26() {
        return this.ctPgm26;
    }

    public void setCtPgm26(String ctPgm26) {
        this.ctPgm26 = ctPgm26;
    }

    public String getCtPgm27() {
        return this.ctPgm27;
    }

    public void setCtPgm27(String ctPgm27) {
        this.ctPgm27 = ctPgm27;
    }

    public String getCtPgm28() {
        return this.ctPgm28;
    }

    public void setCtPgm28(String ctPgm28) {
        this.ctPgm28 = ctPgm28;
    }

    public String getCtPgm29() {
        return this.ctPgm29;
    }

    public void setCtPgm29(String ctPgm29) {
        this.ctPgm29 = ctPgm29;
    }

    public String getCtPgm30() {
        return this.ctPgm30;
    }

    public void setCtPgm30(String ctPgm30) {
        this.ctPgm30 = ctPgm30;
    }

    public String getCtPgm31() {
        return this.ctPgm31;
    }

    public void setCtPgm31(String ctPgm31) {
        this.ctPgm31 = ctPgm31;
    }

    public String getCtPgm32() {
        return this.ctPgm32;
    }

    public void setCtPgm32(String ctPgm32) {
        this.ctPgm32 = ctPgm32;
    }

    public String getCtPgm33() {
        return this.ctPgm33;
    }

    public void setCtPgm33(String ctPgm33) {
        this.ctPgm33 = ctPgm33;
    }

    public String getCtPgm34() {
        return this.ctPgm34;
    }

    public void setCtPgm34(String ctPgm34) {
        this.ctPgm34 = ctPgm34;
    }

    public String getCtPgm35() {
        return this.ctPgm35;
    }

    public void setCtPgm35(String ctPgm35) {
        this.ctPgm35 = ctPgm35;
    }

    public String getCtPgm36() {
        return this.ctPgm36;
    }

    public void setCtPgm36(String ctPgm36) {
        this.ctPgm36 = ctPgm36;
    }

    public String getCtPgm37() {
        return this.ctPgm37;
    }

    public void setCtPgm37(String ctPgm37) {
        this.ctPgm37 = ctPgm37;
    }

    public String getCtPgm38() {
        return this.ctPgm38;
    }

    public void setCtPgm38(String ctPgm38) {
        this.ctPgm38 = ctPgm38;
    }

    public String getCtPgm39() {
        return this.ctPgm39;
    }

    public void setCtPgm39(String ctPgm39) {
        this.ctPgm39 = ctPgm39;
    }

    public String getCtPgm40() {
        return this.ctPgm40;
    }

    public void setCtPgm40(String ctPgm40) {
        this.ctPgm40 = ctPgm40;
    }

    public String getCtPgm41() {
        return this.ctPgm41;
    }

    public void setCtPgm41(String ctPgm41) {
        this.ctPgm41 = ctPgm41;
    }

    public String getCtPgm42() {
        return this.ctPgm42;
    }

    public void setCtPgm42(String ctPgm42) {
        this.ctPgm42 = ctPgm42;
    }

    public String getCtPgm43() {
        return this.ctPgm43;
    }

    public void setCtPgm43(String ctPgm43) {
        this.ctPgm43 = ctPgm43;
    }

    public String getCtPgm44() {
        return this.ctPgm44;
    }

    public void setCtPgm44(String ctPgm44) {
        this.ctPgm44 = ctPgm44;
    }

    public String getCtPgm45() {
        return this.ctPgm45;
    }

    public void setCtPgm45(String ctPgm45) {
        this.ctPgm45 = ctPgm45;
    }

    public String getCtPgm46() {
        return this.ctPgm46;
    }

    public void setCtPgm46(String ctPgm46) {
        this.ctPgm46 = ctPgm46;
    }

    public String getCtPgm47() {
        return this.ctPgm47;
    }

    public void setCtPgm47(String ctPgm47) {
        this.ctPgm47 = ctPgm47;
    }

    public String getCtPgm48() {
        return this.ctPgm48;
    }

    public void setCtPgm48(String ctPgm48) {
        this.ctPgm48 = ctPgm48;
    }

    public String getCtPgm49() {
        return this.ctPgm49;
    }

    public void setCtPgm49(String ctPgm49) {
        this.ctPgm49 = ctPgm49;
    }

    public String getCtPgm50() {
        return this.ctPgm50;
    }

    public void setCtPgm50(String ctPgm50) {
        this.ctPgm50 = ctPgm50;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }


    public void createEventPgmMappingForCt(String eventId, String eventName, List<EventPgmOca> eventPgmOcaList) {
        this.eventId = eventId;
        this.eventName = eventName;

        for (EventPgmOca p : eventPgmOcaList) {

            if (p.getPgmCtOrderNum() != null) {

                String value = null;

                if (p.getIsDisabledByOCA()) {

                    value = "(取消)" + p.getPgmDate() + p.getPgmName();
                }else {

                    value = p.getPgmDate() + p.getPgmName();
                }

                if (p.getPgmCtOrderNum() >= 1 && p.getPgmCtOrderNum() <= 50) {

                    try {
                        Method method = this.getClass().getMethod("setCtPgm" + String.format("%02d", p.getPgmCtOrderNum()), value.getClass());
                        method.invoke(this, value);
                    } catch (Exception e) {
//                        logger.debug(e.getMessage());
                    }
                }

//                switch (p.getPgmCtOrderNum()) {
//                    case 1:
//                        this.ctPgm01 = value;
//                        break;
//                    case 2:
//                        this.ctPgm02 = value;
//                        break;
//                    case 3:
//                        this.ctPgm03 = value;
//                        break;
//                    case 4:
//                        this.ctPgm04 = value;
//                        break;
//                    case 5:
//                        this.ctPgm05 = value;
//                        break;
//                    case 6:
//                        this.ctPgm06 = value;
//                        break;
//                    case 7:
//                        this.ctPgm07 = value;
//                        break;
//                    case 8:
//                        this.ctPgm08 = value;
//                        break;
//                    case 9:
//                        this.ctPgm09 = value;
//                        break;
//                    case 10:
//                        this.ctPgm10 = value;
//                        break;
//                    case 11:
//                        this.ctPgm11 = value;
//                        break;
//                    case 12:
//                        this.ctPgm12 = value;
//                        break;
//                    case 13:
//                        this.ctPgm13 = value;
//                        break;
//                    case 14:
//                        this.ctPgm14 = value;
//                        break;
//                    case 15:
//                        this.ctPgm15 = value;
//                        break;
//                    case 16:
//                        this.ctPgm16 = value;
//                        break;
//                    case 17:
//                        this.ctPgm17 = value;
//                        break;
//                    case 18:
//                        this.ctPgm18 = value;
//                        break;
//                    case 19:
//                        this.ctPgm19 = value;
//                        break;
//                    case 20:
//                        this.ctPgm20 = value;
//                        break;
//                    case 21:
//                        this.ctPgm21 = value;
//                        break;
//                    case 22:
//                        this.ctPgm22 = value;
//                        break;
//                    case 23:
//                        this.ctPgm23 = value;
//                        break;
//                    case 24:
//                        this.ctPgm24 = value;
//                        break;
//                    case 25:
//                        this.ctPgm25 = value;
//                        break;
//                    case 26:
//                        this.ctPgm26 = value;
//                        break;
//                    case 27:
//                        this.ctPgm27 = value;
//                        break;
//                    case 28:
//                        this.ctPgm28 = value;
//                        break;
//                    case 29:
//                        this.ctPgm29 = value;
//                        break;
//                    case 30:
//                        this.ctPgm30 = value;
//                        break;
//                    case 31:
//                        this.ctPgm31 = value;
//                        break;
//                    case 32:
//                        this.ctPgm32 = value;
//                        break;
//                    case 33:
//                        this.ctPgm33 = value;
//                        break;
//                    case 34:
//                        this.ctPgm34 = value;
//                        break;
//                    case 35:
//                        this.ctPgm35 = value;
//                        break;
//                    case 36:
//                        this.ctPgm36 = value;
//                        break;
//                    case 37:
//                        this.ctPgm37 = value;
//                        break;
//                    case 38:
//                        this.ctPgm38 = value;
//                        break;
//                    case 39:
//                        this.ctPgm39 = value;
//                        break;
//                    case 40:
//                        this.ctPgm40 = value;
//                        break;
//                    case 41:
//                        this.ctPgm41 = value;
//                        break;
//                    case 42:
//                        this.ctPgm42 = value;
//                        break;
//                    case 43:
//                        this.ctPgm43 = value;
//                        break;
//                    case 44:
//                        this.ctPgm44 = value;
//                        break;
//                    case 45:
//                        this.ctPgm45 = value;
//                        break;
//                    case 46:
//                        this.ctPgm46 = value;
//                        break;
//                    case 47:
//                        this.ctPgm47 = value;
//                        break;
//                    case 48:
//                        this.ctPgm48 = value;
//                        break;
//                    case 49:
//                        this.ctPgm49 = value;
//                        break;
//                    case 50:
//                        this.ctPgm50 = value;
//                        break;
//                }
            }
        }
    }
}
