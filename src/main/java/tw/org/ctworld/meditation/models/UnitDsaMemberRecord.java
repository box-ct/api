package tw.org.ctworld.meditation.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="unit_dsa_member_record")
public class UnitDsaMemberRecord extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 200)
    private String aliasName;

    @Column(length = 50)
    private String approvalNote;
    private Date birthDate;
    @Column(length = 50)
    private String centerDsaYear;
    @Column(columnDefinition = "TEXT")
    private String changeNote;
    @Column(length = 50)
    private String classList;
    @Column(length = 50)
    private String companyJobTitle;
    @Column(length = 150)
    private String companyName;
    @Column(length = 50)
    private String ctDharmaName;
    @Column(length = 50)
    private String ctDsaYear;
    @Column(length = 50)
    private String memberId;
    @Column(length = 100)
    private String dataCheckedResult;
    @Column(length = 200)
    private String donationNameList;
    @Column(length = 50)
    private String dsaJobIdNew;
    @Column(length = 50)
    private String dsaJobIdPrev;
    @Column(length = 50)
    private String dsaJobTitleNew;
    @Column(length = 50)
    private String dsaJobTitlePrev;
    @Column(length = 50)
    private String englishFirstName;
    @Column(length = 50)
    private String englishLastName;
    @Column(length = 50)
    private String gender;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isApproved;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCancelled;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedEnglishLetter;
    @Column(columnDefinition = "TEXT")
    private String note;
    @Column(length = 50)
    private String schoolDegree;
    @Column(length = 100)
    private String schoolMajor;
    @Column(length = 100)
    private String schoolName;
    @Column(length = 50)    
    private String twIdNum;
    @Column(length = 50)
    private String unitId;
    @Column(length = 50)
    private String unitName;
    @Column(columnDefinition = "TEXT")
    private String unitAppliedNote;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getApprovalNote() {
        return approvalNote;
    }

    public void setApprovalNote(String approvalNote) {
        this.approvalNote = approvalNote;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCenterDsaYear() {
        return centerDsaYear;
    }

    public void setCenterDsaYear(String centerDsaYear) {
        this.centerDsaYear = centerDsaYear;
    }

    public String getChangeNote() {
        return changeNote;
    }

    public void setChangeNote(String changeNote) {
        this.changeNote = changeNote;
    }

    public String getClassList() {
        return classList;
    }

    public void setClassList(String classList) {
        this.classList = classList;
    }

    public String getCompanyJobTitle() {
        return companyJobTitle;
    }

    public void setCompanyJobTitle(String companyJobTitle) {
        this.companyJobTitle = companyJobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getCtDsaYear() {
        return ctDsaYear;
    }

    public void setCtDsaYear(String ctDsaYear) {
        this.ctDsaYear = ctDsaYear;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getDataCheckedResult() {
        return dataCheckedResult;
    }

    public void setDataCheckedResult(String dataCheckedResult) {
        this.dataCheckedResult = dataCheckedResult;
    }

    public String getDonationNameList() {
        return donationNameList;
    }

    public void setDonationNameList(String donationNameList) {
        this.donationNameList = donationNameList;
    }

    public String getDsaJobIdNew() {
        return dsaJobIdNew;
    }

    public void setDsaJobIdNew(String dsaJobIdNew) {
        this.dsaJobIdNew = dsaJobIdNew;
    }

    public String getDsaJobIdPrev() {
        return dsaJobIdPrev;
    }

    public void setDsaJobIdPrev(String dsaJobIdPrev) {
        this.dsaJobIdPrev = dsaJobIdPrev;
    }

    public String getDsaJobTitleNew() {
        return dsaJobTitleNew;
    }

    public void setDsaJobTitleNew(String dsaJobTitleNew) {
        this.dsaJobTitleNew = dsaJobTitleNew;
    }

    public String getDsaJobTitlePrev() {
        return dsaJobTitlePrev;
    }

    public void setDsaJobTitlePrev(String dsaJobTitlePrev) {
        this.dsaJobTitlePrev = dsaJobTitlePrev;
    }

    public String getEnglishFirstName() {
        return englishFirstName;
    }

    public void setEnglishFirstName(String englishFirstName) {
        this.englishFirstName = englishFirstName;
    }

    public String getEnglishLastName() {
        return englishLastName;
    }

    public void setEnglishLastName(String englishLastName) {
        this.englishLastName = englishLastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getIsApproved() {
        return isApproved == null ? false : isApproved;
    }

    public void setIsApproved(Boolean approved) {
        isApproved = approved;
    }

    public Boolean getIsCancelled() {
        return isCancelled == null ? false : isCancelled;
    }

    public void setIsCancelled(Boolean cancelled) {
        isCancelled = cancelled;
    }

    public Boolean getIsNeedEnglishLetter() {
        return isNeedEnglishLetter == null ? false : isNeedEnglishLetter;
    }

    public void setIsNeedEnglishLetter(Boolean needEnglishLetter) {
        isNeedEnglishLetter = needEnglishLetter;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSchoolDegree() {
        return schoolDegree;
    }

    public void setSchoolDegree(String schoolDegree) {
        this.schoolDegree = schoolDegree;
    }

    public String getSchoolMajor() {
        return schoolMajor;
    }

    public void setSchoolMajor(String schoolMajor) {
        this.schoolMajor = schoolMajor;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitAppliedNote() {
        return unitAppliedNote;
    }

    public void setUnitAppliedNote(String unitAppliedNote) {
        this.unitAppliedNote = unitAppliedNote;
    }
}
