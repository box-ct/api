package tw.org.ctworld.meditation.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name="user_info")
public class UserInfo extends BaseEntity {

    public enum AccountCategory {
        ADMIN("系統管理員"), PERMANENT("本山管理單位"), ABODE("精舍使用者");

        private final String value;

        AccountCategory(String value) {
            this.value = value;
        }

        public String getValue() { return value; }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 9)
    private String unitId;
    @Column(length = 30)
    private String unitName;
    @Column(length = 20)
    private String roleType;
    @Column(length = 9)
    private String userId;
    @Column(length = 60)
    private String userName;
    @Column(length = 10)
    private String userJobTitle;
    @Column(length = 60)
    private String mobileNum;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isDisabled;
    @Column(length = 9)
    private String memberId;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeMemberData;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeCreateClass;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeBarcodeCheckIn;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeClassAttendRecord;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeClassGraduateStats;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeClassUpGrade;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeePrintReport;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeHomeClass;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeIDCheck;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeCurrentClassMembers;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeePreviousClassMembers;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeCurrentClassLeaders;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeMultipleClassMembersAndLeaders;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeEventRecord;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isClassLeaderSetting;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isClassMemberGroupSetting;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isMakeupFilesManage;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isCertPrint;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeClassStats;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeClassCurrentCheckInStats;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeClassPeriodCheckInStats;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeClassStatsByClassName;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeClassStatsWithLinechart;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeOnlineUser;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeNews;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeAuthorize;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeUnitInfo;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSeeImportClassRecord;

    @Column(columnDefinition = "TEXT ")
    private String managedClassList;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isMemberManageByCT;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isMakeupFileManageByCT;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private boolean isSysCode;

    @Column(length = 12)
    private String yubiKeyId;
    @Column(length = 5)
    private String yubiKeyTagNum;


    public UserInfo() {
    }

    public UserInfo(String unitId, String unitName, String roleType, String userId, String userName,
                    String userJobTitle, String mobileNum, boolean isDisabled, String memberId,
                    String yubiKeyId) {
        this.unitId = unitId;
        this.unitName = unitName;
        this.roleType = roleType;
        this.userId = userId;
        this.userName = userName;
        this.userJobTitle = userJobTitle;
        this.mobileNum = mobileNum;
        this.isDisabled = isDisabled;
        this.memberId = memberId;
        this.yubiKeyId = yubiKeyId;
    }
    
    public void setAllPermissions(boolean input) {
        isSeeMemberData = input;
        isSeeCreateClass = input;
        isSeeBarcodeCheckIn = input;
        isSeeClassAttendRecord = input;
        isSeeClassGraduateStats = input;
        isSeeClassUpGrade = input;
        isSeePrintReport = input;
        isSeeHomeClass = input;
        isSeeIDCheck = input;
        isSeeCurrentClassMembers = input;
        isSeePreviousClassMembers = input;
        isSeeCurrentClassLeaders = input;
        isSeeMultipleClassMembersAndLeaders = input;
        isSeeEventRecord = input;
        isSeeClassStats = input;
        isSeeClassCurrentCheckInStats = input;
        isSeeClassPeriodCheckInStats = input;
        isSeeClassStatsByClassName = input;
        isSeeClassStatsWithLinechart = input;
        isSeeOnlineUser = input;
        isSeeNews = input;
        isSeeAuthorize = input;
        isSeeUnitInfo = input;
        isSeeImportClassRecord = input;

        isClassLeaderSetting = input;
        isClassMemberGroupSetting = input;
        isMakeupFilesManage = input;
        isCertPrint = input;
        isMemberManageByCT = input;
        isMakeupFileManageByCT = input;

        isSysCode = input;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserJobTitle() {
        return userJobTitle;
    }

    public void setUserJobTitle(String userJobTitle) {
        this.userJobTitle = userJobTitle;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public boolean getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(boolean disabled) {
        isDisabled = disabled;
    }

    public boolean getIsSeeMemberData() {
        return this.isSeeMemberData;
    }

    public void setIsSeeMemberData(boolean seeMemberData) {
        isSeeMemberData = seeMemberData;
    }

    public boolean getIsSeeCreateClass() {
        return this.isSeeCreateClass;
    }

    public void setIsSeeCreateClass(boolean seeCreateClass) {
        isSeeCreateClass = seeCreateClass;
    }

    public boolean getIsSeeBarcodeCheckIn() {
        return this.isSeeBarcodeCheckIn;
    }

    public void setIsSeeBarcodeCheckIn(boolean seeBarcodeCheckIn) {
        isSeeBarcodeCheckIn = seeBarcodeCheckIn;
    }

    public boolean getIsSeeClassAttendRecord() {
        return this.isSeeClassAttendRecord;
    }

    public void setIsSeeClassAttendRecord(boolean seeClassAttendRecord) {
        isSeeClassAttendRecord = seeClassAttendRecord;
    }

    public boolean getIsSeeClassGraduateStats() {
        return this.isSeeClassGraduateStats;
    }

    public void setIsSeeClassGraduateStats(boolean seeClassGraduateStats) {
        isSeeClassGraduateStats = seeClassGraduateStats;
    }

    public boolean getIsSeeClassUpGrade() {
        return this.isSeeClassUpGrade;
    }

    public void setIsSeeClassUpGrade(boolean seeClassUpGrade) {
        isSeeClassUpGrade = seeClassUpGrade;
    }

    public boolean getIsSeePrintReport() {
        return this.isSeePrintReport;
    }

    public void setIsSeePrintReport(boolean seePrintReport) {
        isSeePrintReport = seePrintReport;
    }

    public boolean getIsSeeHomeClass() {
        return this.isSeeHomeClass;
    }

    public void setIsSeeHomeClass(boolean seeHomeClass) {
        isSeeHomeClass = seeHomeClass;
    }

    public boolean getIsSeeIDCheck() {
        return this.isSeeIDCheck;
    }

    public void setIsSeeIDCheck(boolean seeIDCheck) {
        isSeeIDCheck = seeIDCheck;
    }

    public boolean getIsSeeCurrentClassMembers() {
        return this.isSeeCurrentClassMembers;
    }

    public void setIsSeeCurrentClassMembers(boolean seeCurrentClassMembers) {
        isSeeCurrentClassMembers = seeCurrentClassMembers;
    }

    public boolean getIsSeePreviousClassMembers() {
        return this.isSeePreviousClassMembers;
    }

    public void setIsSeePreviousClassMembers(boolean seePreviousClassMembers) {
        isSeePreviousClassMembers = seePreviousClassMembers;
    }

    public boolean getIsSeeCurrentClassLeaders() {
        return this.isSeeCurrentClassLeaders;
    }

    public void setIsSeeCurrentClassLeaders(boolean seeCurrentClassLeaders) {
        isSeeCurrentClassLeaders = seeCurrentClassLeaders;
    }

    public boolean getIsSeeMultpleClassMembersAndLeaders() {
        return this.isSeeMultipleClassMembersAndLeaders;
    }

    public void setIsSeeMultpleClassMembersAndLeaders(boolean seeMultpleClassMembersAndLeaders) {
        isSeeMultipleClassMembersAndLeaders = seeMultpleClassMembersAndLeaders;
    }

    public boolean getIsSeeEventRecord() {
        return this.isSeeEventRecord;
    }

    public void setIsSeeEventRecord(boolean seeEventRecord) {
        isSeeEventRecord = seeEventRecord;
    }

    public boolean getIsSeeClassStats() {
        return this.isSeeClassStats;
    }

    public void setIsSeeClassStats(boolean seeClassStats) {
        isSeeClassStats = seeClassStats;
    }

    public boolean getIsSeeClassCurrentCheckInStats() {
        return this.isSeeClassCurrentCheckInStats;
    }

    public void setIsSeeClassCurrentCheckInStats(boolean seeClassCurrentCheckInStats) {
        isSeeClassCurrentCheckInStats = seeClassCurrentCheckInStats;
    }

    public boolean getIsSeeClassPeriodCheckInStats() {
        return this.isSeeClassPeriodCheckInStats;
    }

    public void setIsSeeClassPeriodCheckInStats(boolean seeClassPeriodCheckInStats) {
        isSeeClassPeriodCheckInStats = seeClassPeriodCheckInStats;
    }

    public boolean getIsSeeClassStatsByClassName() {
        return this.isSeeClassStatsByClassName;
    }

    public void setIsSeeClassStatsByClassName(boolean seeClassStatsByClassName) {
        isSeeClassStatsByClassName = seeClassStatsByClassName;
    }

    public boolean getIsSeeClassStatsWithLinechart() {
        return isSeeClassStatsWithLinechart;
    }

    public void setIsSeeClassStatsWithLinechart(boolean seeClassStatsWithLinechart) {
        isSeeClassStatsWithLinechart = seeClassStatsWithLinechart;
    }

    public boolean getIsSeeOnlineUser() {
        return this.isSeeOnlineUser;
    }

    public void setIsSeeOnlineUser(boolean seeOnlineUser) {
        isSeeOnlineUser = seeOnlineUser;
    }

    public boolean getIsSeeNews() {
        return this.isSeeNews;
    }

    public void setIsSeeNews(boolean seeNews) {
        isSeeNews = seeNews;
    }

    public boolean getIsSeeAuthorize() {
        return this.isSeeAuthorize;
    }

    public void setIsSeeAuthorize(boolean seeAuthorize) {
        isSeeAuthorize = seeAuthorize;
    }

    public boolean getIsSeeUnitInfo() {
        return isSeeUnitInfo;
    }

    public void setIsSeeUnitInfo(boolean seeUnitInfo) {
        isSeeUnitInfo = seeUnitInfo;
    }

    public boolean getIsSeeImportClassRecord() {
        return this.isSeeImportClassRecord;
    }

    public void setIsSeeImportClassRecord(boolean seeImportClassRecord) {
        isSeeImportClassRecord = seeImportClassRecord;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getYubiKeyId() {
        return yubiKeyId;
    }

    public void setYubiKeyId(String yubiKeyId) {
        this.yubiKeyId = yubiKeyId;
    }

    public String getYubiKeyTagNum() {
        return yubiKeyTagNum;
    }

    public void setYubiKeyTagNum(String yubiKeyTagNum) {
        this.yubiKeyTagNum = yubiKeyTagNum;
    }

    public boolean getIsClassLeaderSetting() {
        return isClassLeaderSetting;
    }

    public void setIsClassLeaderSetting(boolean classLeaderSetting) {
        isClassLeaderSetting = classLeaderSetting;
    }

    public boolean getIsClassMemberGroupSetting() {
        return isClassMemberGroupSetting;
    }

    public void setIsClassMemberGroupSetting(boolean classMemberGroupSetting) {
        isClassMemberGroupSetting = classMemberGroupSetting;
    }

    public boolean getIsMakeupFilesManage() {
        return isMakeupFilesManage;
    }

    public void setIsMakeupFilesManage(boolean makeupFilesManage) {
        isMakeupFilesManage = makeupFilesManage;
    }

    public boolean getIsCertPrint() {
        return isCertPrint;
    }

    public void setIsCertPrint(boolean certPrint) {
        isCertPrint = certPrint;
    }

    public String getManagedClassList() {
        return managedClassList;
    }

    public void setManagedClassList(String managedClassList) {
        this.managedClassList = managedClassList;
    }

    public boolean getIsMemberManageByCT() {
        return isMemberManageByCT;
    }

    public void setIsMemberManageByCT(boolean memberManageByCT) {
        isMemberManageByCT = memberManageByCT;
    }

    public boolean getIsMakeupFileManageByCT() {
        return isMakeupFileManageByCT;
    }

    public void setIsMakeupFileManageByCT(boolean makeupFileManageByCT) {
        isMakeupFileManageByCT = makeupFileManageByCT;
    }

    public boolean getIsSysCode() {
        return isSysCode;
    }

    public void setIsSysCode(boolean sysCode) {
        isSysCode = sysCode;
    }

    public void copy(UserInfo userInfo) {
        setMobileNum(userInfo.getMobileNum());
        setIsDisabled(userInfo.getIsDisabled());
        setUserJobTitle(userInfo.getUserJobTitle());
        setUnitName(userInfo.getUnitName());
        setUnitId(userInfo.getUnitId());

        setUserName(userInfo.getUserName());
        setUserId(userInfo.getUserId());

        setIsSeeMemberData(userInfo.getIsSeeMemberData());
        setIsSeeCreateClass(userInfo.getIsSeeCreateClass());
        setIsSeeBarcodeCheckIn(userInfo.getIsSeeBarcodeCheckIn());
        setIsSeeClassAttendRecord(userInfo.getIsSeeClassAttendRecord());
        setIsSeeClassGraduateStats(userInfo.getIsSeeClassGraduateStats());
        setIsSeeClassUpGrade(userInfo.getIsSeeClassUpGrade());
        setIsSeePrintReport(userInfo.getIsSeePrintReport());
        setIsSeeHomeClass(userInfo.getIsSeeHomeClass());
        setIsSeeIDCheck(userInfo.getIsSeeIDCheck());
        setIsSeeCurrentClassMembers(userInfo.getIsSeeCurrentClassMembers());
        setIsSeePreviousClassMembers(userInfo.getIsSeePreviousClassMembers());
        setIsSeeCurrentClassLeaders(userInfo.getIsSeeCurrentClassLeaders());
        setIsSeeMultpleClassMembersAndLeaders(userInfo.getIsSeeMultpleClassMembersAndLeaders());
        setIsSeeEventRecord(userInfo.getIsSeeEventRecord());
        setIsSeeClassStats(userInfo.getIsSeeClassStats());
        setIsSeeClassCurrentCheckInStats(userInfo.getIsSeeClassCurrentCheckInStats());
        setIsSeeClassPeriodCheckInStats(userInfo.getIsSeeClassPeriodCheckInStats());
        setIsSeeClassStatsByClassName(userInfo.getIsSeeClassStatsByClassName());
        setIsSeeClassStatsWithLinechart(userInfo.getIsSeeClassStatsWithLinechart());
        setIsSeeOnlineUser(userInfo.getIsSeeOnlineUser());
        setIsSeeNews(userInfo.getIsSeeNews());
        setIsSeeAuthorize(userInfo.getIsSeeAuthorize());
        setIsSeeUnitInfo(userInfo.getIsSeeUnitInfo());
        setIsSeeImportClassRecord(userInfo.getIsSeeImportClassRecord());

        setMemberId(userInfo.getMemberId());
        setYubiKeyId(userInfo.getYubiKeyId());
        setYubiKeyTagNum(userInfo.getYubiKeyTagNum());

        setRoleType(userInfo.getRoleType());

        setIsClassLeaderSetting(userInfo.getIsClassLeaderSetting());
        setIsClassMemberGroupSetting(userInfo.getIsClassMemberGroupSetting());
        setIsMakeupFilesManage(userInfo.getIsMakeupFilesManage());
        setIsCertPrint(userInfo.getIsCertPrint());
        setManagedClassList(userInfo.getManagedClassList());
        setIsMemberManageByCT(userInfo.getIsMemberManageByCT());
        setIsMakeupFileManageByCT(userInfo.getIsMakeupFileManageByCT());
        setIsSysCode(userInfo.getIsSysCode());
    }

}
