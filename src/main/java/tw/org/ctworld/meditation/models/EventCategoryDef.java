/*
* Auto Generated
* Based on name: event_category_def
* Date: 2019-06-13 17:09:44
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="event_category_def")
public class EventCategoryDef extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    private String eventCategoryId;

    @Column(length = 50)
    private String eventCategoryName;

    @Column(length = 50)
    private String eventCategoryColorCode;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCenterAvailable;


    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEventCategoryId() {
        return this.eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCategoryName() {
        return this.eventCategoryName;
    }

    public void setEventCategoryName(String eventCategoryName) {
        this.eventCategoryName = eventCategoryName;
    }

    public Boolean getIsCenterAvailable() {
        return this.isCenterAvailable == null ? false : isCenterAvailable;
    }

    public void setIsCenterAvailable(Boolean isCenterAvailable) {
        this.isCenterAvailable = isCenterAvailable;
    }

    public String getEventCategoryColorCode() {
        return eventCategoryColorCode;
    }

    public void setEventCategoryColorCode(String eventCategoryColorCode) {
        this.eventCategoryColorCode = eventCategoryColorCode;
    }
}
