package tw.org.ctworld.meditation.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="class_enroll_form")
public class ClassEnrollForm extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 9)
    private String unitId;
    @Column(length = 30)
    private String unitName;

    @Column(length = 13, unique = true)
    private String classEnrollFormId;
    @Column(length = 9)
    private String memberId;
    @Column(length = 75)
    private String twIdNum;
    @Column(length = 1)
    private String gender;
    @Column(length = 60)
    private String aliasName;
    @Column(length = 30)
    private String ctDharmaName;
    @Column(columnDefinition = "INT(11)")
    private int age;
    @Column(length = 60)
    private String homePhoneNum1;
    @Column(length = 60)
    private String mobileNum1;
    @Column(length = 100)
    private String dsaJobNameList;
    @Column(columnDefinition = "TEXT")
    private String address;
    @Column(columnDefinition = "TEXT")
    private String companyNameAndJobTitle;
    @Column(columnDefinition = "TEXT")
    private String schoolNameAndMajor;
    private Date birthDate;
    @Column(length = 12)
    private String classId;
    @Column(length = 4)
    private String year;
    private Date classStartDate;
    private Date classEndDate;
    @Column(columnDefinition = "INT(3)")
    private int classTypeNum;
    @Column(length = 20)
    private String classTypeName;
    @Column(length = 2)
    private String classPeriodNum;
    @Column(length = 30)
    private String className;
    @Column(length = 1)
    private String dayOfWeek;
    @Column(columnDefinition = "TEXT")
    private String classDesc;
    @Column(length = 60)
    private String classFullName;
    @Column(length = 2)
    private String classGroupId;
    @Column(length = 5)
    private String memberGroupNum;
    @Column(length = 30)
    private String sameGroupMemberName;
    @Column(length = 60)
    private String currentClassIntroducerName;
    @Column(length = 20)
    private String currentClassIntroducerRelationship;
    @Column(length = 60)
    private String childContactPersonName;
    @Column(length = 20)
    private String childContactPersonPhoneNum;
    @Column(length = 30)
    private String asistantMasterName;
    @Column(length = 2)
    private String classJobHighestNum;
    @Column(length = 100)
    private String classJobTitleList;
    @Column(length = 100)
    private String classJobContentList;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isLeadGroup;
    @Column(length = 30)
    private String beforeChangeClassName;
    @Column(length = 30)
    private String beforeChangeClassId;
    @Column(length = 30)
    private String homeClassNote;
    @Column(length = 30)
    private String homeClassName;
    @Column(length = 4)
    private String homeClassPeriodNum;
    @Column(length = 12)
    private String homeClassId;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isHomeClass;
    @Column(length = 10)
    private String newReturnChangeMark;
    private Date newReturnChangeDate;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean changeAndWaitPrintMark;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isDroppedClass;
    private Date classDroppedDate;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean userDefinedLabel1;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean userDefinedLabel2;
    @Column(columnDefinition = "TEXT")
    private String classEnrollFormNote;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isFullAttended;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isGraduated;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isFullAttendedPrinted;
    private Date isFullAttendedCertPrintedDtTm;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isGraduatedPrinted;
    private Date isGraduatedCertPrintedDtTm;
    @Column(length = 50)
    private String certPrintUserName;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isMakeupToFullAttended;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isDiligentAward;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isStayInOriginalClass;
    @Column(length = 5)
    private String classAttendedResult;
    @Column(length = 30)
    private String previousClassName;
    @Column(length = 2)
    private String previousClassGroupName;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isPreviousGraduated;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isPreviousFullAttend;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isEnrollToNewUpperClass;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNotComeBack;
    @Column(length = 12)
    private String nextNewClassId;
    @Column(columnDefinition = "INT(2)")
    private int nextNewClassTypeNum;
    @Column(length = 20)
    private String nextNewClassTypeName;
    @Column(length = 30)
    private String nextNewClassName;
    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCompleteNextNewClassEnroll;
    @Column(columnDefinition = "LONGTEXT ")
    private String nextNewClassNote;

    public ClassEnrollForm() {
    }

    public ClassEnrollForm(String classEnrollFormId, String memberId, String gender, String aliasName,
                           String ctDharmaName, String classId, String classPeriodNum, String className,
                           String classGroupId) {
        this.classEnrollFormId = classEnrollFormId;
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classId = classId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
    }

    public ClassEnrollForm(String memberId, String classEnrollFormId, String aliasName, String ctDharmaName, String mobileNum1) {
        this.memberId = memberId;
        this.classEnrollFormId = classEnrollFormId;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.mobileNum1 = mobileNum1;
    }

    public ClassEnrollForm(String classId, String className, String dayOfWeek) {
        this.classId = classId;
        this.className = className;
        this.dayOfWeek = dayOfWeek;
    }

    public ClassEnrollForm(String memberId, String classEnrollFormId, String gender, String aliasName, String ctDharmaName,
                           String classPeriodNum, String className, String classGroupId, String memberGroupNum,
                           String classJobTitleList, Boolean isFullAttended, Boolean isGraduated,
                           Boolean isFullAttendedPrinted, Date isFullAttendedCertPrintedDtTm, Boolean isGraduatedPrinted,
                           Date isGraduatedCertPrintedDtTm, String certPrintUserName) {
        this.memberId = memberId;
        this.classEnrollFormId = classEnrollFormId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.memberGroupNum = memberGroupNum;
        this.classJobTitleList = classJobTitleList;
        this.isFullAttended = isFullAttended;
        this.isGraduated = isGraduated;
        this.isFullAttendedPrinted = isFullAttendedPrinted;
        this.isFullAttendedCertPrintedDtTm = isFullAttendedCertPrintedDtTm;
        this.isGraduatedPrinted = isGraduatedPrinted;
        this.isGraduatedCertPrintedDtTm = isGraduatedCertPrintedDtTm;
        this.certPrintUserName = certPrintUserName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyNameAndJobTitle() {
        return companyNameAndJobTitle;
    }

    public void setCompanyNameAndJobTitle(String companyNameAndJobTitle) {
        this.companyNameAndJobTitle = companyNameAndJobTitle;
    }

    public String getSchoolNameAndMajor() {
        return schoolNameAndMajor;
    }

    public void setSchoolNameAndMajor(String schoolNameAndMajor) {
        this.schoolNameAndMajor = schoolNameAndMajor;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Date getClassStartDate() {
        return classStartDate;
    }

    public void setClassStartDate(Date classStartDate) {
        this.classStartDate = classStartDate;
    }

    public Date getClassEndDate() {
        return classEndDate;
    }

    public void setClassEndDate(Date classEndDate) {
        this.classEndDate = classEndDate;
    }

    public int getClassTypeNum() {
        return classTypeNum;
    }

    public void setClassTypeNum(int classTypeNum) {
        this.classTypeNum = classTypeNum;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getClassDesc() {
        return classDesc;
    }

    public void setClassDesc(String classDesc) {
        this.classDesc = classDesc;
    }

    public String getClassFullName() {
        return classFullName;
    }

    public void setClassFullName(String classFullName) {
        this.classFullName = classFullName;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getMemberGroupNum() {
        return memberGroupNum;
    }

    public void setMemberGroupNum(String memberGroupNum) {
        this.memberGroupNum = memberGroupNum;
    }

    public String getSameGroupMemberName() {
        return sameGroupMemberName;
    }

    public void setSameGroupMemberName(String sameGroupMemberName) {
        this.sameGroupMemberName = sameGroupMemberName;
    }

    public String getCurrentClassIntroducerName() {
        return currentClassIntroducerName;
    }

    public void setCurrentClassIntroducerName(String currentClassIntroducerName) {
        this.currentClassIntroducerName = currentClassIntroducerName;
    }

    public String getCurrentClassIntroducerRelationship() {
        return currentClassIntroducerRelationship;
    }

    public void setCurrentClassIntroducerRelationship(String currentClassIntroducerRelationship) {
        this.currentClassIntroducerRelationship = currentClassIntroducerRelationship;
    }

    public String getChildContactPersonName() {
        return childContactPersonName;
    }

    public void setChildContactPersonName(String childContactPersonName) {
        this.childContactPersonName = childContactPersonName;
    }

    public String getChildContactPersonPhoneNum() {
        return childContactPersonPhoneNum;
    }

    public void setChildContactPersonPhoneNum(String childContactPersonPhoneNum) {
        this.childContactPersonPhoneNum = childContactPersonPhoneNum;
    }

    public String getAsistantMasterName() {
        return asistantMasterName;
    }

    public void setAsistantMasterName(String asistantMasterName) {
        this.asistantMasterName = asistantMasterName;
    }

    public String getClassJobHighestNum() {
        return classJobHighestNum;
    }

    public void setClassJobHighestNum(String classJobHighestNum) {
        this.classJobHighestNum = classJobHighestNum;
    }

    public String getClassJobTitleList() {
        return classJobTitleList;
    }

    public void setClassJobTitleList(String classJobTitleList) {
        this.classJobTitleList = classJobTitleList;
    }

    public String getClassJobContentList() {
        return classJobContentList;
    }

    public void setClassJobContentList(String classJobContentList) {
        this.classJobContentList = classJobContentList;
    }

    public Boolean getIsLeadGroup() {
        return isLeadGroup == null ? false : isLeadGroup;
    }

    public void setIsLeadGroup(Boolean leadGroup) {
        isLeadGroup = leadGroup;
    }

    public String getBeforeChangeClassName() {
        return beforeChangeClassName;
    }

    public void setBeforeChangeClassName(String beforeChangeClassName) {
        this.beforeChangeClassName = beforeChangeClassName;
    }

    public String getBeforeChangeClassId() {
        return beforeChangeClassId;
    }

    public void setBeforeChangeClassId(String beforeChangeClassId) {
        this.beforeChangeClassId = beforeChangeClassId;
    }

    public String getHomeClassNote() {
        return homeClassNote;
    }

    public void setHomeClassNote(String homeClassNote) {
        this.homeClassNote = homeClassNote;
    }

    public String getHomeClassName() {
        return homeClassName;
    }

    public void setHomeClassName(String homeClassName) {
        this.homeClassName = homeClassName;
    }

    public String getHomeClassPeriodNum() {
        return homeClassPeriodNum;
    }

    public void setHomeClassPeriodNum(String homeClassPeriodNum) {
        this.homeClassPeriodNum = homeClassPeriodNum;
    }

    public String getHomeClassId() {
        return homeClassId;
    }

    public void setHomeClassId(String homeClassId) {
        this.homeClassId = homeClassId;
    }

    public Boolean getIsHomeClass() {
        return isHomeClass == null ? false : isHomeClass;
    }

    public void setIsHomeClass(Boolean homeClass) {
        isHomeClass = homeClass;
    }

    public String getNewReturnChangeMark() {
        return newReturnChangeMark;
    }

    public void setNewReturnChangeMark(String newReturnChangeMark) {
        this.newReturnChangeMark = newReturnChangeMark;
    }

    public Date getNewReturnChangeDate() {
        return newReturnChangeDate;
    }

    public void setNewReturnChangeDate(Date newReturnChangeDate) {
        this.newReturnChangeDate = newReturnChangeDate;
    }

    public Boolean getChangeAndWaitPrintMark() {
        return changeAndWaitPrintMark == null ? false : changeAndWaitPrintMark;
    }

    public void setChangeAndWaitPrintMark(Boolean changeAndWaitPrintMark) {
        this.changeAndWaitPrintMark = changeAndWaitPrintMark;
    }

    public Boolean getIsDroppedClass() {
        return isDroppedClass == null ? false : isDroppedClass;
    }

    public void setIsDroppedClass(Boolean droppedClass) {
        isDroppedClass = droppedClass;
    }

    public Date getClassDroppedDate() {
        return classDroppedDate;
    }

    public void setClassDroppedDate(Date classDroppedDate) {
        this.classDroppedDate = classDroppedDate;
    }

    public Boolean isUserDefinedLabel1() {
        return userDefinedLabel1;
    }

    public void setUserDefinedLabel1(Boolean userDefinedLabel1) {
        this.userDefinedLabel1 = userDefinedLabel1;
    }

    public Boolean isUserDefinedLabel2() {
        return userDefinedLabel2;
    }

    public void setUserDefinedLabel2(Boolean userDefinedLabel2) {
        this.userDefinedLabel2 = userDefinedLabel2;
    }

    public String getClassEnrollFormNote() {
        return classEnrollFormNote;
    }

    public void setClassEnrollFormNote(String classEnrollFormNote) {
        this.classEnrollFormNote = classEnrollFormNote;
    }

    public Boolean getIsFullAttended() {
        return isFullAttended == null ? false : isFullAttended;
    }

    public void setIsFullAttended(Boolean isFullAttended) {
        this.isFullAttended = isFullAttended;
    }

    public Boolean getIsGraduated() {
        return isGraduated == null ? false : isGraduated;
    }

    public void setIsGraduated(Boolean isGraduated) {
        this.isGraduated = isGraduated;
    }

    public Boolean getIsMakeupToFullAttended() {
        return isMakeupToFullAttended == null ? false : isMakeupToFullAttended;
    }

    public void setIsMakeupToFullAttended(Boolean isMakeupToFullAttended) {
        this.isMakeupToFullAttended = isMakeupToFullAttended;
    }

    public Boolean getIsDiligentAward() {
        return isDiligentAward == null ? false : isDiligentAward;
    }

    public void setIsDiligentAward(Boolean isDiligentAward) {
        this.isDiligentAward = isDiligentAward;
    }

    public Boolean getIsStayInOriginalClass() {
        return isStayInOriginalClass == null ? false : isStayInOriginalClass;
    }

    public void setIsStayInOriginalClass(Boolean stayInOriginalClass) {
        isStayInOriginalClass = stayInOriginalClass;
    }

    public String getClassAttendedResult() {
        return classAttendedResult == null ? "" : classAttendedResult;
    }

    public void setClassAttendedResult(String classAttendedResult) {
        this.classAttendedResult = classAttendedResult;
    }

    public String getPreviousClassName() {
        return previousClassName;
    }

    public void setPreviousClassName(String previousClassName) {
        this.previousClassName = previousClassName;
    }

    public String getPreviousClassGroupName() {
        return previousClassGroupName;
    }

    public void setPreviousClassGroupName(String previousClassGroupName) {
        this.previousClassGroupName = previousClassGroupName;
    }

    public Boolean getIsPreviousGraduated() {
        return isPreviousGraduated == null ? false : isPreviousGraduated;
    }

    public void setIsPreviousGraduated(Boolean previousGraduated) {
        isPreviousGraduated = previousGraduated;
    }

    public Boolean getIsPreviousFullAttend() {
        return isPreviousFullAttend == null ? false : isPreviousFullAttend;
    }

    public void setIsPreviousFullAttend(Boolean previousFullAttend) {
        isPreviousFullAttend = previousFullAttend;
    }

    public Boolean getIsEnrollToNewUpperClass() {
        return isEnrollToNewUpperClass == null ? false : isEnrollToNewUpperClass;
    }

    public void setIsEnrollToNewUpperClass(Boolean isEnrollToNewUpperClass) {
        this.isEnrollToNewUpperClass = isEnrollToNewUpperClass;
    }

    public Boolean getIsNotComeBack() {
        return isNotComeBack == null ? false : isNotComeBack;
    }

    public void setIsNotComeBack(Boolean isNotComeBack) {
        this.isNotComeBack = isNotComeBack;
    }

    public String getNextNewClassId() {
        return nextNewClassId;
    }

    public void setNextNewClassId(String nextNewClassId) {
        this.nextNewClassId = nextNewClassId;
    }

    public int getNextNewClassTypeNum() {
        return nextNewClassTypeNum;
    }

    public void setNextNewClassTypeNum(int nextNewClassTypeNum) {
        this.nextNewClassTypeNum = nextNewClassTypeNum;
    }

    public String getNextNewClassTypeName() {
        return nextNewClassTypeName;
    }

    public void setNextNewClassTypeName(String nextNewClassTypeName) {
        this.nextNewClassTypeName = nextNewClassTypeName;
    }

    public String getNextNewClassName() {
        return nextNewClassName;
    }

    public void setNextNewClassName(String nextNewClassName) {
        this.nextNewClassName = nextNewClassName;
    }

    public Boolean getIsCompleteNextNewClassEnroll() {
        return isCompleteNextNewClassEnroll == null ? false :isCompleteNextNewClassEnroll ;
    }

    public void setIsCompleteNextNewClassEnroll(Boolean completeNextNewClassEnroll) {
        isCompleteNextNewClassEnroll = completeNextNewClassEnroll;
    }

    public String getNextNewClassNote() {
        return nextNewClassNote;
    }

    public void setNextNewClassNote(String nextNewClassNote) {
        this.nextNewClassNote = nextNewClassNote;
    }

    public Boolean getIsFullAttendedPrinted() {
        return isFullAttendedPrinted == null ? false : isFullAttendedPrinted;
    }

    public void setIsFullAttendedPrinted(Boolean fullAttendedPrinted) {
        isFullAttendedPrinted = fullAttendedPrinted;
    }

    public Date getIsFullAttendedCertPrintedDtTm() {
        return isFullAttendedCertPrintedDtTm;
    }

    public void setIsFullAttendedCertPrintedDtTm(Date isFullAttendedCertPrintedDtTm) {
        this.isFullAttendedCertPrintedDtTm = isFullAttendedCertPrintedDtTm;
    }

    public Boolean getIsGraduatedPrinted() {
        return isGraduatedPrinted == null ? false : isGraduatedPrinted;
    }

    public void setIsGraduatedPrinted(Boolean graduatedPrinted) {
        isGraduatedPrinted = graduatedPrinted;
    }

    public Date getIsGraduatedCertPrintedDtTm() {
        return isGraduatedCertPrintedDtTm;
    }

    public void setIsGraduatedCertPrintedDtTm(Date isGraduatedCertPrintedDtTm) {
        this.isGraduatedCertPrintedDtTm = isGraduatedCertPrintedDtTm;
    }

    public String getCertPrintUserName() {
        return certPrintUserName;
    }

    public void setCertPrintUserName(String certPrintUserName) {
        this.certPrintUserName = certPrintUserName;
    }
}
