/*
* Auto Generated
* Based on name: event_form for_ct
* Date: 2019-06-13 16:57:01
*/

package tw.org.ctworld.meditation.models;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import tw.org.ctworld.meditation.beans.UserSession;

@Entity
@Table(name="event_form_for_ct")
public class EventFormForCt extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(columnDefinition = "TEXT")
    private String address;

    private Integer age;

    @Column(columnDefinition = "Date")
    private Date arriveCtDate;

    @Column(columnDefinition = "Time")
    private Date arriveCtTime;

    private Date birthDate;

    @Column(columnDefinition = "Date")
    private Date bodhiPreceptDate;

    @Column(columnDefinition = "Date")
    private Date cancelledDate;

    @Column(length = 50)
    private String carLicenseNum1;

    private Integer classGroupId;

    @Column(length = 50)
    private String classId;

    @Column(length = 50)
    private String classListName;

    @Column(length = 50)
    private String className;

    @Column(columnDefinition = "TEXT")
    private String classNameAndJobList;

    private Integer classPeriodNum;

    @Column(length = 50)
    private String classTypeName;

    @Column(length = 50)
    private String classTypeNum;

    @Column(length = 50)
    private String clubJobTitleNow;

    @Column(length = 50)
    private String clubJobTitlePrev;

    @Column(length = 50)
    private String clubName;

    @Column(length = 50)
    private String companyJobTitle;

    @Column(length = 150)
    private String companyName;

    @Column(columnDefinition = "TEXT")
    private String ctDormChangeHistory;

    @Column(columnDefinition = "Date")
    private Date ctDormEndDate;

    @Column(columnDefinition = "TEXT")
    private String ctDormLatestChange;

    @Column(length = 50)
    private String ctDormPurpose;

    @Column(columnDefinition = "Date")
    private Date ctDormStartDate;

    @Column(length = 200)
    private String ctNote1;

    @Column(length = 200)
    private String ctNote2;

    @Column(length = 200)
    private String ctNote3;

    @Column(length = 200)
    private String ctNote4;

    @Column(length = 200)
    private String ctNote5;

    @Column(length = 50)
    private String ctPgm01;

    @Column(length = 50)
    private String ctPgm02;

    @Column(length = 50)
    private String ctPgm03;

    @Column(length = 50)
    private String ctPgm04;

    @Column(length = 50)
    private String ctPgm05;

    @Column(length = 50)
    private String ctPgm06;

    @Column(length = 50)
    private String ctPgm07;

    @Column(length = 50)
    private String ctPgm08;

    @Column(length = 50)
    private String ctPgm09;

    @Column(length = 50)
    private String ctPgm10;

    @Column(length = 50)
    private String ctPgm11;

    @Column(length = 50)
    private String ctPgm12;

    @Column(length = 50)
    private String ctPgm13;

    @Column(length = 50)
    private String ctPgm14;

    @Column(length = 50)
    private String ctPgm15;

    @Column(length = 50)
    private String ctPgm16;

    @Column(length = 50)
    private String ctPgm17;

    @Column(length = 50)
    private String ctPgm18;

    @Column(length = 50)
    private String ctPgm19;

    @Column(length = 50)
    private String ctPgm20;

    @Column(length = 50)
    private String ctPgm21;

    @Column(length = 50)
    private String ctPgm22;

    @Column(length = 50)
    private String ctPgm23;

    @Column(length = 50)
    private String ctPgm24;

    @Column(length = 50)
    private String ctPgm25;

    @Column(length = 50)
    private String ctPgm26;

    @Column(length = 50)
    private String ctPgm27;

    @Column(length = 50)
    private String ctPgm28;

    @Column(length = 50)
    private String ctPgm29;

    @Column(length = 50)
    private String ctPgm30;

    @Column(length = 50)
    private String ctPgm31;

    @Column(length = 50)
    private String ctPgm32;

    @Column(length = 50)
    private String ctPgm33;

    @Column(length = 50)
    private String ctPgm34;

    @Column(length = 50)
    private String ctPgm35;

    @Column(length = 50)
    private String ctPgm36;

    @Column(length = 50)
    private String ctPgm37;

    @Column(length = 50)
    private String ctPgm38;

    @Column(length = 50)
    private String ctPgm39;

    @Column(length = 50)
    private String ctPgm40;

    @Column(length = 50)
    private String ctPgm41;

    @Column(length = 50)
    private String ctPgm42;

    @Column(length = 50)
    private String ctPgm43;

    @Column(length = 50)
    private String ctPgm44;

    @Column(length = 50)
    private String ctPgm45;

    @Column(length = 50)
    private String ctPgm46;

    @Column(length = 50)
    private String ctPgm47;

    @Column(length = 50)
    private String ctPgm48;

    @Column(length = 50)
    private String ctPgm49;

    @Column(length = 50)
    private String ctPgm50;

    @Column(length = 50)
    private String ctRefugeMaster;

    @Column(columnDefinition = "TEXT")
    private String degreeInfo;

    @Column(length = 200)
    private String donationNameList;

    @Column(length = 100)
    private String dormNote;

    @Column(length = 100)
    private String dsaJobNameList;

    @Column(length = 50)
    private String email1;

    @Column(columnDefinition = "TEXT")
    private String enrollChangeHistory;

    @Column(length = 100)
    private String enrollGroupName;

    @Column(columnDefinition = "TEXT")
    private String enrollLatestChange;

    @Column(columnDefinition = "TEXT")
    private String enrollNote;

    private Integer enrollOrderNum;

    @Column(length = 50)
    private String enrollUnitId;

    @Column(length = 50)
    private String enrollUnitName;

    @Column(length = 50)
    private String enrollUserCtDharmaName;

    @Column(length = 50)
    private String enrollUserId;

    @Column(length = 150)
    private String enrollUserName;

    @Column(length = 50)
    private String eventCategoryId;

    @Column(length = 50)
    private String eventCreatorUnitId;

    @Column(length = 50)
    private String eventCreatorUnitName;

    private Integer eventEnrollId;

    private Date eventFormForCtCreateDtTm;

    private Date eventFormForCtUpdateDtTm;

    @Column(length = 50)
    private String eventFormUid;

    @Column(length = 50)
    private String eventId;

    @Column(length = 100)
    private String eventName;

    @Column(length = 50)
    private String familyLeaderMemberId;

    @Column(length = 150)
    private String familyLeaderName;

    @Column(length = 50)
    private String formAutoAddNum;

    @Column(length = 50)
    private String gender;

    @Column(columnDefinition = "TEXT")
    private String healthList;

    @Column(columnDefinition = "DOUBLE(5,2)")
    private Double height;

    @Column(length = 50)
    private String highestPreceptName;

    @Column(length = 50)
    private String homeClassId;

    @Column(length = 100)
    private String homeClassNote;

    @Column(length = 50)
    private String homePhoneNum1;

    @Column(length = 50)
    private String idType1;

    @Column(length = 50)
    private String idType2;

    @Column(length = 150)
    private String introducerName;

    @Column(length = 50)
    private String introducerPhoneNum;

    @Column(length = 50)
    private String introducerRelationship;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isAbbot;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCancelPrecept5ThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCancelRefugeThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCanceled;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCtDorm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isEarlyBookCtDorm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isGoCtBySelf;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isMaster;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedExtraPrecept5CertThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedParkingPass;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedPrecept5RobeThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNeedTranslator;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isNotDorm;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isRelativePreceptOrZen7Vlntr;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isRelativeTakePreceptOrZen7;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isSentAfterEnrollEndDate;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeBodhiPrecept;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeExtraPrecept5ThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakePrecept5;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakePrecept5ThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeRefuge;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeRefugeThisTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTreasurer;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isWaitForCtSync;

    @Column(length = 50)
    private String jobOrder;

    @Column(length = 200)
    private String jobTitle;

    @Column(columnDefinition = "Date")
    private Date leaveCtDate;

    @Column(columnDefinition = "Time")
    private Date leaveCtTime;

    @Column(columnDefinition = "Date")
    private Date leaveUnitDate;

    @Column(columnDefinition = "Time")
    private Date leaveUnitTime;

    @Column(length = 50)
    private String mailingCity;

    @Column(length = 50)
    private String mailingCountry;

    @Column(length = 50)
    private String mailingState;

    @Column(columnDefinition = "TEXT")
    private String mailingStreet;

    @Column(length = 50)
    private String mailingZip;

    @Column(length = 100)
    private String masterDormNote;

    @Column(length = 50)
    private String masterPreceptTypeName;

    @Column(length = 50)
    private String mobileNum1;

    @Column(length = 50)
    private String officePhoneNum1;

    @Column(length = 100)
    private String onThatDayDonation;

    @Column(length = 50)
    private String parentsJobTitle;

    @Column(length = 150)
    private String parentsName;

    @Column(length = 50)
    private String parentsPhoneNum;

    @Column(length = 100)
    private String passportNum;

    @Column(columnDefinition = "Date")
    private Date precept5Date;

    @Column(length = 50)
    private String preceptOrder;

    private Integer preceptVlntrCount;

    @Column(columnDefinition = "Date")
    private Date refugeDate;

    private Integer relativeAge;

    @Column(columnDefinition = "TEXT")
    private String relativeMeritList;

    @Column(length = 150)
    private String relativeName;

    @Column(length = 50)
    private String relativeRelationship;

    @Column(length = 100)
    private String relativeTwId;

    @Column(length = 50)
    private String schoolDegree;

    @Column(length = 50)
    private String schoolMajor;

    @Column(length = 50)
    private String schoolName;

    @Column(columnDefinition = "TEXT")
    private String skillList;

    @Column(columnDefinition = "TEXT")
    private String socialTitle;

    @Column(length = 100)
    private String specialDonation;

    @Column(length = 100)
    private String specialDonationCompleteNote;

    private Integer specialDonationSum;

    @Column(length = 50)
    private String sponsorUnitId;

    @Column(length = 50)
    private String sponsorUnitName;

    @Column(length = 50)
    private String summerDonation1;

    private Integer summerDonation1Count;

    private Integer summerDonation1and2Sum;

    @Column(length = 100)
    private String summerDonation2;

    @Column(length = 50)
    private String summerDonationType;

    @Column(length = 100)
    private String summerRelativeMeritList;

    private Integer summerVlntrCount;

    @Column(length = 50)
    private String templateName;

    @Column(length = 50)
    private String templateUniqueId;

    @Column(length = 50)
    private String translateLanguage;

    @Column(length = 100)
    private String twIdNum;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;

    @Column(length = 150)
    private String urgentContactPersonName1;

    @Column(length = 50)
    private String urgentContactPersonPhoneNum1;

    @Column(length = 50)
    private String urgentContactPersonRelationship1;

    @Column(columnDefinition = "TEXT")
    private String vehicleNumJobTitleList;

    @Column(columnDefinition = "Date")
    private Date vlntrEndDate;

    @Column(columnDefinition = "Time")
    private Date vlntrEndTime;

    @Column(length = 100)
    private String vlntrGroupNameIdList;

    @Column(columnDefinition = "Date")
    private Date vlntrStartDate;

    @Column(columnDefinition = "Time")
    private Date vlntrStartTime;

    @Column(columnDefinition = "DOUBLE(5,2)")
    private Double weight;

    private Integer zen7Count;

    private Integer zen7VlntrCount;



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getBodhiPreceptDate() {
        return bodhiPreceptDate;
    }

    public void setBodhiPreceptDate(Date bodhiPreceptDate) {
        this.bodhiPreceptDate = bodhiPreceptDate;
    }

    public Date getPrecept5Date() {
        return precept5Date;
    }

    public void setPrecept5Date(Date precept5Date) {
        this.precept5Date = precept5Date;
    }

    public Date getRefugeDate() {
        return refugeDate;
    }

    public void setRefugeDate(Date refugeDate) {
        this.refugeDate = refugeDate;
    }

    public String getAddress () {
        return this.address ;
    }

    public void setAddress (String address ) {
        this.address  = address ;
    }

    public Integer getAge() {
        return this.age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getArriveCtDate() {
        return this.arriveCtDate;
    }

    public void setArriveCtDate(Date arriveCtDate) {
        this.arriveCtDate = arriveCtDate;
    }

    public Date getArriveCtTime() {
        return this.arriveCtTime;
    }

    public void setArriveCtTime(Date arriveCtTime) {
        this.arriveCtTime = arriveCtTime;
    }

    public Date getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getCancelledDate() {
        return this.cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getCarLicenseNum1() {
        return this.carLicenseNum1;
    }

    public void setCarLicenseNum1(String carLicenseNum1) {
        this.carLicenseNum1 = carLicenseNum1;
    }

    public int getClassGroupId() {
        return this.classGroupId;
    }

    public void setClassGroupId(int classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getClassId() {
        return this.classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassListName() {
        return this.classListName;
    }

    public void setClassListName(String classListName) {
        this.classListName = classListName;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassNameAndJobList () {
        return this.classNameAndJobList ;
    }

    public void setClassNameAndJobList (String classNameAndJobList ) {
        this.classNameAndJobList  = classNameAndJobList ;
    }

    public int getClassPeriodNum() {
        return this.classPeriodNum;
    }

    public void setClassPeriodNum(int classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassTypeName() {
        return this.classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getClassTypeNum() {
        return this.classTypeNum;
    }

    public void setClassTypeNum(String classTypeNum) {
        this.classTypeNum = classTypeNum;
    }

    public String getClubJobTitleNow() {
        return this.clubJobTitleNow;
    }

    public void setClubJobTitleNow(String clubJobTitleNow) {
        this.clubJobTitleNow = clubJobTitleNow;
    }

    public String getClubJobTitlePrev() {
        return this.clubJobTitlePrev;
    }

    public void setClubJobTitlePrev(String clubJobTitlePrev) {
        this.clubJobTitlePrev = clubJobTitlePrev;
    }

    public String getClubName() {
        return this.clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getCompanyJobTitle() {
        return this.companyJobTitle;
    }

    public void setCompanyJobTitle(String companyJobTitle) {
        this.companyJobTitle = companyJobTitle;
    }

    public String getCompanyName() {
        return this.companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCtDormChangeHistory() {
        return this.ctDormChangeHistory;
    }

    public void setCtDormChangeHistory(String ctDormChangeHistory) {
        this.ctDormChangeHistory = ctDormChangeHistory;
    }

    public Date getCtDormEndDate() {
        return this.ctDormEndDate;
    }

    public void setCtDormEndDate(Date ctDormEndDate) {
        this.ctDormEndDate = ctDormEndDate;
    }

    public String getCtDormLatestChange() {
        return this.ctDormLatestChange;
    }

    public void setCtDormLatestChange(String ctDormLatestChange) {
        this.ctDormLatestChange = ctDormLatestChange;
    }

    public String getCtDormPurpose() {
        return this.ctDormPurpose;
    }

    public void setCtDormPurpose(String ctDormPurpose) {
        this.ctDormPurpose = ctDormPurpose;
    }

    public Date getCtDormStartDate() {
        return this.ctDormStartDate;
    }

    public void setCtDormStartDate(Date ctDormStartDate) {
        this.ctDormStartDate = ctDormStartDate;
    }

    public String getCtNote1() {
        return this.ctNote1;
    }

    public void setCtNote1(String ctNote1) {
        this.ctNote1 = ctNote1;
    }

    public String getCtNote2() {
        return this.ctNote2;
    }

    public void setCtNote2(String ctNote2) {
        this.ctNote2 = ctNote2;
    }

    public String getCtNote3() {
        return this.ctNote3;
    }

    public void setCtNote3(String ctNote3) {
        this.ctNote3 = ctNote3;
    }

    public String getCtNote4() {
        return this.ctNote4;
    }

    public void setCtNote4(String ctNote4) {
        this.ctNote4 = ctNote4;
    }

    public String getCtNote5() {
        return this.ctNote5;
    }

    public void setCtNote5(String ctNote5) {
        this.ctNote5 = ctNote5;
    }

    public String getCtPgm01() {
        return this.ctPgm01;
    }

    public void setCtPgm01(String ctPgm01) {
        this.ctPgm01 = ctPgm01;
    }

    public String getCtPgm02() {
        return this.ctPgm02;
    }

    public void setCtPgm02(String ctPgm02) {
        this.ctPgm02 = ctPgm02;
    }

    public String getCtPgm03() {
        return this.ctPgm03;
    }

    public void setCtPgm03(String ctPgm03) {
        this.ctPgm03 = ctPgm03;
    }

    public String getCtPgm04() {
        return this.ctPgm04;
    }

    public void setCtPgm04(String ctPgm04) {
        this.ctPgm04 = ctPgm04;
    }

    public String getCtPgm05() {
        return this.ctPgm05;
    }

    public void setCtPgm05(String ctPgm05) {
        this.ctPgm05 = ctPgm05;
    }

    public String getCtPgm06() {
        return this.ctPgm06;
    }

    public void setCtPgm06(String ctPgm06) {
        this.ctPgm06 = ctPgm06;
    }

    public String getCtPgm07() {
        return this.ctPgm07;
    }

    public void setCtPgm07(String ctPgm07) {
        this.ctPgm07 = ctPgm07;
    }

    public String getCtPgm08() {
        return this.ctPgm08;
    }

    public void setCtPgm08(String ctPgm08) {
        this.ctPgm08 = ctPgm08;
    }

    public String getCtPgm09() {
        return this.ctPgm09;
    }

    public void setCtPgm09(String ctPgm09) {
        this.ctPgm09 = ctPgm09;
    }

    public String getCtPgm10() {
        return this.ctPgm10;
    }

    public void setCtPgm10(String ctPgm10) {
        this.ctPgm10 = ctPgm10;
    }

    public String getCtPgm11() {
        return this.ctPgm11;
    }

    public void setCtPgm11(String ctPgm11) {
        this.ctPgm11 = ctPgm11;
    }

    public String getCtPgm12() {
        return this.ctPgm12;
    }

    public void setCtPgm12(String ctPgm12) {
        this.ctPgm12 = ctPgm12;
    }

    public String getCtPgm13() {
        return this.ctPgm13;
    }

    public void setCtPgm13(String ctPgm13) {
        this.ctPgm13 = ctPgm13;
    }

    public String getCtPgm14() {
        return this.ctPgm14;
    }

    public void setCtPgm14(String ctPgm14) {
        this.ctPgm14 = ctPgm14;
    }

    public String getCtPgm15() {
        return this.ctPgm15;
    }

    public void setCtPgm15(String ctPgm15) {
        this.ctPgm15 = ctPgm15;
    }

    public String getCtPgm16() {
        return this.ctPgm16;
    }

    public void setCtPgm16(String ctPgm16) {
        this.ctPgm16 = ctPgm16;
    }

    public String getCtPgm17() {
        return this.ctPgm17;
    }

    public void setCtPgm17(String ctPgm17) {
        this.ctPgm17 = ctPgm17;
    }

    public String getCtPgm18() {
        return this.ctPgm18;
    }

    public void setCtPgm18(String ctPgm18) {
        this.ctPgm18 = ctPgm18;
    }

    public String getCtPgm19() {
        return this.ctPgm19;
    }

    public void setCtPgm19(String ctPgm19) {
        this.ctPgm19 = ctPgm19;
    }

    public String getCtPgm20() {
        return this.ctPgm20;
    }

    public void setCtPgm20(String ctPgm20) {
        this.ctPgm20 = ctPgm20;
    }

    public String getCtPgm21() {
        return this.ctPgm21;
    }

    public void setCtPgm21(String ctPgm21) {
        this.ctPgm21 = ctPgm21;
    }

    public String getCtPgm22() {
        return this.ctPgm22;
    }

    public void setCtPgm22(String ctPgm22) {
        this.ctPgm22 = ctPgm22;
    }

    public String getCtPgm23() {
        return this.ctPgm23;
    }

    public void setCtPgm23(String ctPgm23) {
        this.ctPgm23 = ctPgm23;
    }

    public String getCtPgm24() {
        return this.ctPgm24;
    }

    public void setCtPgm24(String ctPgm24) {
        this.ctPgm24 = ctPgm24;
    }

    public String getCtPgm25() {
        return this.ctPgm25;
    }

    public void setCtPgm25(String ctPgm25) {
        this.ctPgm25 = ctPgm25;
    }

    public String getCtPgm26() {
        return this.ctPgm26;
    }

    public void setCtPgm26(String ctPgm26) {
        this.ctPgm26 = ctPgm26;
    }

    public String getCtPgm27() {
        return this.ctPgm27;
    }

    public void setCtPgm27(String ctPgm27) {
        this.ctPgm27 = ctPgm27;
    }

    public String getCtPgm28() {
        return this.ctPgm28;
    }

    public void setCtPgm28(String ctPgm28) {
        this.ctPgm28 = ctPgm28;
    }

    public String getCtPgm29() {
        return this.ctPgm29;
    }

    public void setCtPgm29(String ctPgm29) {
        this.ctPgm29 = ctPgm29;
    }

    public String getCtPgm30() {
        return this.ctPgm30;
    }

    public void setCtPgm30(String ctPgm30) {
        this.ctPgm30 = ctPgm30;
    }

    public String getCtPgm31() {
        return this.ctPgm31;
    }

    public void setCtPgm31(String ctPgm31) {
        this.ctPgm31 = ctPgm31;
    }

    public String getCtPgm32() {
        return this.ctPgm32;
    }

    public void setCtPgm32(String ctPgm32) {
        this.ctPgm32 = ctPgm32;
    }

    public String getCtPgm33() {
        return this.ctPgm33;
    }

    public void setCtPgm33(String ctPgm33) {
        this.ctPgm33 = ctPgm33;
    }

    public String getCtPgm34() {
        return this.ctPgm34;
    }

    public void setCtPgm34(String ctPgm34) {
        this.ctPgm34 = ctPgm34;
    }

    public String getCtPgm35() {
        return this.ctPgm35;
    }

    public void setCtPgm35(String ctPgm35) {
        this.ctPgm35 = ctPgm35;
    }

    public String getCtPgm36() {
        return this.ctPgm36;
    }

    public void setCtPgm36(String ctPgm36) {
        this.ctPgm36 = ctPgm36;
    }

    public String getCtPgm37() {
        return this.ctPgm37;
    }

    public void setCtPgm37(String ctPgm37) {
        this.ctPgm37 = ctPgm37;
    }

    public String getCtPgm38() {
        return this.ctPgm38;
    }

    public void setCtPgm38(String ctPgm38) {
        this.ctPgm38 = ctPgm38;
    }

    public String getCtPgm39() {
        return this.ctPgm39;
    }

    public void setCtPgm39(String ctPgm39) {
        this.ctPgm39 = ctPgm39;
    }

    public String getCtPgm40() {
        return this.ctPgm40;
    }

    public void setCtPgm40(String ctPgm40) {
        this.ctPgm40 = ctPgm40;
    }

    public String getCtPgm41() {
        return this.ctPgm41;
    }

    public void setCtPgm41(String ctPgm41) {
        this.ctPgm41 = ctPgm41;
    }

    public String getCtPgm42() {
        return this.ctPgm42;
    }

    public void setCtPgm42(String ctPgm42) {
        this.ctPgm42 = ctPgm42;
    }

    public String getCtPgm43() {
        return this.ctPgm43;
    }

    public void setCtPgm43(String ctPgm43) {
        this.ctPgm43 = ctPgm43;
    }

    public String getCtPgm44() {
        return this.ctPgm44;
    }

    public void setCtPgm44(String ctPgm44) {
        this.ctPgm44 = ctPgm44;
    }

    public String getCtPgm45() {
        return this.ctPgm45;
    }

    public void setCtPgm45(String ctPgm45) {
        this.ctPgm45 = ctPgm45;
    }

    public String getCtPgm46() {
        return this.ctPgm46;
    }

    public void setCtPgm46(String ctPgm46) {
        this.ctPgm46 = ctPgm46;
    }

    public String getCtPgm47() {
        return this.ctPgm47;
    }

    public void setCtPgm47(String ctPgm47) {
        this.ctPgm47 = ctPgm47;
    }

    public String getCtPgm48() {
        return this.ctPgm48;
    }

    public void setCtPgm48(String ctPgm48) {
        this.ctPgm48 = ctPgm48;
    }

    public String getCtPgm49() {
        return this.ctPgm49;
    }

    public void setCtPgm49(String ctPgm49) {
        this.ctPgm49 = ctPgm49;
    }

    public String getCtPgm50() {
        return this.ctPgm50;
    }

    public void setCtPgm50(String ctPgm50) {
        this.ctPgm50 = ctPgm50;
    }

    public String getCtRefugeMaster() {
        return this.ctRefugeMaster;
    }

    public void setCtRefugeMaster(String ctRefugeMaster) {
        this.ctRefugeMaster = ctRefugeMaster;
    }

    public String getDegreeInfo() {
        return this.degreeInfo;
    }

    public void setDegreeInfo(String degreeInfo) {
        this.degreeInfo = degreeInfo;
    }

    public String getDonationNameList() {
        return this.donationNameList;
    }

    public void setDonationNameList(String donationNameList) {
        this.donationNameList = donationNameList;
    }

    public String getDormNote() {
        return this.dormNote;
    }

    public void setDormNote(String dormNote) {
        this.dormNote = dormNote;
    }

    public String getDsaJobNameList() {
        return this.dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getEmail1() {
        return this.email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEnrollChangeHistory() {
        return this.enrollChangeHistory;
    }

    public void setEnrollChangeHistory(String enrollChangeHistory) {
        this.enrollChangeHistory = enrollChangeHistory;
    }

    public String getEnrollGroupName() {
        return this.enrollGroupName;
    }

    public void setEnrollGroupName(String enrollGroupName) {
        this.enrollGroupName = enrollGroupName;
    }

    public String getEnrollLatestChange() {
        return this.enrollLatestChange;
    }

    public void setEnrollLatestChange(String enrollLatestChange) {
        this.enrollLatestChange = enrollLatestChange;
    }

    public String getEnrollNote() {
        return this.enrollNote;
    }

    public void setEnrollNote(String enrollNote) {
        this.enrollNote = enrollNote;
    }

    public Integer getEnrollOrderNum() {
        return this.enrollOrderNum;
    }

    public void setEnrollOrderNum(Integer enrollOrderNum) {
        this.enrollOrderNum = enrollOrderNum;
    }

    public String getEnrollUnitId() {
        return this.enrollUnitId;
    }

    public void setEnrollUnitId(String enrollUnitId) {
        this.enrollUnitId = enrollUnitId;
    }

    public String getEnrollUnitName() {
        return this.enrollUnitName;
    }

    public void setEnrollUnitName(String enrollUnitName) {
        this.enrollUnitName = enrollUnitName;
    }

    public String getEnrollUserCtDharmaName() {
        return this.enrollUserCtDharmaName;
    }

    public void setEnrollUserCtDharmaName(String enrollUserCtDharmaName) {
        this.enrollUserCtDharmaName = enrollUserCtDharmaName;
    }

    public String getEnrollUserId() {
        return this.enrollUserId;
    }

    public void setEnrollUserId(String enrollUserId) {
        this.enrollUserId = enrollUserId;
    }

    public String getEnrollUserName() {
        return this.enrollUserName;
    }

    public void setEnrollUserName(String enrollUserName) {
        this.enrollUserName = enrollUserName;
    }

    public String getEventCategoryId() {
        return this.eventCategoryId;
    }

    public void setEventCategoryId(String eventCategoryId) {
        this.eventCategoryId = eventCategoryId;
    }

    public String getEventCreatorUnitId() {
        return this.eventCreatorUnitId;
    }

    public void setEventCreatorUnitId(String eventCreatorUnitId) {
        this.eventCreatorUnitId = eventCreatorUnitId;
    }

    public String getEventCreatorUnitName() {
        return this.eventCreatorUnitName;
    }

    public void setEventCreatorUnitName(String eventCreatorUnitName) {
        this.eventCreatorUnitName = eventCreatorUnitName;
    }

    public int getEventEnrollId() {
        return this.eventEnrollId;
    }

    public void setEventEnrollId(int eventEnrollId) {
        this.eventEnrollId = eventEnrollId;
    }

    public Date getEventFormForCtCreateDtTm () {
        return this.eventFormForCtCreateDtTm ;
    }

    public void setEventFormForCtCreateDtTm (Date eventFormForCtCreateDtTm ) {
        this.eventFormForCtCreateDtTm  = eventFormForCtCreateDtTm ;
    }

    public Date getEventFormForCtUpdateDtTm () {
        return this.eventFormForCtUpdateDtTm ;
    }

    public void setEventFormForCtUpdateDtTm (Date eventFormForCtUpdateDtTm ) {
        this.eventFormForCtUpdateDtTm  = eventFormForCtUpdateDtTm ;
    }

    public String getEventFormUid () {
        return this.eventFormUid ;
    }

    public void setEventFormUid (String eventFormUid ) {
        this.eventFormUid  = eventFormUid ;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getFamilyLeaderMemberId() {
        return this.familyLeaderMemberId;
    }

    public void setFamilyLeaderMemberId(String familyLeaderMemberId) {
        this.familyLeaderMemberId = familyLeaderMemberId;
    }

    public String getFamilyLeaderName() {
        return this.familyLeaderName;
    }

    public void setFamilyLeaderName(String familyLeaderName) {
        this.familyLeaderName = familyLeaderName;
    }

    public String getFormAutoAddNum() {
        return this.formAutoAddNum;
    }

    public void setFormAutoAddNum(String formAutoAddNum) {
        this.formAutoAddNum = formAutoAddNum;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHealthList() {
        return this.healthList;
    }

    public void setHealthList(String healthList) {
        this.healthList = healthList;
    }

    public Double getHeight() {
        return this.height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getHighestPreceptName() {
        return this.highestPreceptName;
    }

    public void setHighestPreceptName(String highestPreceptName) {
        this.highestPreceptName = highestPreceptName;
    }

    public String getHomeClassId () {
        return this.homeClassId ;
    }

    public void setHomeClassId (String homeClassId ) {
        this.homeClassId  = homeClassId ;
    }

    public String getHomeClassNote () {
        return this.homeClassNote ;
    }

    public void setHomeClassNote (String homeClassNote ) {
        this.homeClassNote  = homeClassNote ;
    }

    public String getHomePhoneNum1() {
        return this.homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getIdType1() {
        return this.idType1;
    }

    public void setIdType1(String idType1) {
        this.idType1 = idType1;
    }

    public String getIdType2() {
        return this.idType2;
    }

    public void setIdType2(String idType2) {
        this.idType2 = idType2;
    }

    public String getIntroducerName() {
        return this.introducerName;
    }

    public void setIntroducerName(String introducerName) {
        this.introducerName = introducerName;
    }

    public String getIntroducerPhoneNum() {
        return this.introducerPhoneNum;
    }

    public void setIntroducerPhoneNum(String introducerPhoneNum) {
        this.introducerPhoneNum = introducerPhoneNum;
    }

    public String getIntroducerRelationship() {
        return this.introducerRelationship;
    }

    public void setIntroducerRelationship(String introducerRelationship) {
        this.introducerRelationship = introducerRelationship;
    }

    public Boolean getIsAbbot() {
        return this.isAbbot == null ? false : isAbbot;
    }

    public void setIsAbbot(Boolean isAbbot) {
        this.isAbbot = isAbbot;
    }

    public Boolean getIsCancelPrecept5ThisTime() {
        return this.isCancelPrecept5ThisTime == null ? false : isCancelPrecept5ThisTime;
    }

    public void setIsCancelPrecept5ThisTime(Boolean isCancelPrecept5ThisTime) {
        this.isCancelPrecept5ThisTime = isCancelPrecept5ThisTime;
    }

    public Boolean getIsCancelRefugeThisTime() {
        return this.isCancelRefugeThisTime == null ? false : isCancelRefugeThisTime;
    }

    public void setIsCancelRefugeThisTime(Boolean isCancelRefugeThisTime) {
        this.isCancelRefugeThisTime = isCancelRefugeThisTime;
    }

    public Boolean getIsCanceled() {
        return this.isCanceled == null ? false : isCanceled;
    }

    public void setIsCanceled(Boolean isCanceled) {
        this.isCanceled = isCanceled;
    }

    public Boolean getIsCtDorm() {
        return this.isCtDorm == null ? false : isCtDorm;
    }

    public void setIsCtDorm(Boolean isCtDorm) {
        this.isCtDorm = isCtDorm;
    }

    public Boolean getIsEarlyBookCtDorm() {
        return this.isEarlyBookCtDorm == null ? false : isEarlyBookCtDorm;
    }

    public void setIsEarlyBookCtDorm(Boolean isEarlyBookCtDorm) {
        this.isEarlyBookCtDorm = isEarlyBookCtDorm;
    }

    public Boolean getIsGoCtBySelf() {
        return this.isGoCtBySelf == null ? false : isGoCtBySelf;
    }

    public void setIsGoCtBySelf(Boolean isGoCtBySelf) {
        this.isGoCtBySelf = isGoCtBySelf;
    }

    public Boolean getIsMaster() {
        return this.isMaster == null ? false : isMaster;
    }

    public void setIsMaster(Boolean isMaster) {
        this.isMaster = isMaster;
    }

    public Boolean getIsNeedExtraPrecept5CertThisTime() {
        return this.isNeedExtraPrecept5CertThisTime == null ? false : isNeedExtraPrecept5CertThisTime;
    }

    public void setIsNeedExtraPrecept5CertThisTime(Boolean isNeedExtraPrecept5CertThisTime) {
        this.isNeedExtraPrecept5CertThisTime = isNeedExtraPrecept5CertThisTime;
    }

    public Boolean getIsNeedParkingPass() {
        return this.isNeedParkingPass == null ? false : isNeedParkingPass;
    }

    public void setIsNeedParkingPass(Boolean isNeedParkingPass) {
        this.isNeedParkingPass = isNeedParkingPass;
    }

    public Boolean getIsNeedPrecept5RobeThisTime() {
        return this.isNeedPrecept5RobeThisTime == null ? false : isNeedPrecept5RobeThisTime;
    }

    public void setIsNeedPrecept5RobeThisTime(Boolean isNeedPrecept5RobeThisTime) {
        this.isNeedPrecept5RobeThisTime = isNeedPrecept5RobeThisTime;
    }

    public Boolean getIsNeedTranslator() {
        return this.isNeedTranslator == null ? false : isNeedTranslator;
    }

    public void setIsNeedTranslator(Boolean isNeedTranslator) {
        this.isNeedTranslator = isNeedTranslator;
    }

    public Boolean getIsNotDorm() {
        return this.isNotDorm == null ? false : isNotDorm;
    }

    public void setIsNotDorm(Boolean isNotDorm) {
        this.isNotDorm = isNotDorm;
    }

    public Boolean getIsRelativePreceptOrZen7Vlntr() {
        return this.isRelativePreceptOrZen7Vlntr == null ? false : isRelativePreceptOrZen7Vlntr;
    }

    public void setIsRelativePreceptOrZen7Vlntr(Boolean isRelativePreceptOrZen7Vlntr) {
        this.isRelativePreceptOrZen7Vlntr = isRelativePreceptOrZen7Vlntr;
    }

    public Boolean getIsRelativeTakePreceptOrZen7() {
        return this.isRelativeTakePreceptOrZen7 == null ? false : isRelativeTakePreceptOrZen7;
    }

    public void setIsRelativeTakePreceptOrZen7(Boolean isRelativeTakePreceptOrZen7) {
        this.isRelativeTakePreceptOrZen7 = isRelativeTakePreceptOrZen7;
    }

    public Boolean getIsSentAfterEnrollEndDate() {
        return this.isSentAfterEnrollEndDate == null ? false : isSentAfterEnrollEndDate;
    }

    public void setIsSentAfterEnrollEndDate(Boolean isSentAfterEnrollEndDate) {
        this.isSentAfterEnrollEndDate = isSentAfterEnrollEndDate;
    }

    public Boolean getIsTakeBodhiPrecept () {
        return this.isTakeBodhiPrecept == null ? false : isTakeBodhiPrecept;
    }

    public void setIsTakeBodhiPrecept (Boolean isTakeBodhiPrecept ) {
        this.isTakeBodhiPrecept  = isTakeBodhiPrecept ;
    }

    public Boolean getIsTakeExtraPrecept5ThisTime() {
        return this.isTakeExtraPrecept5ThisTime == null ? false : isTakeExtraPrecept5ThisTime;
    }

    public void setIsTakeExtraPrecept5ThisTime(Boolean isTakeExtraPrecept5ThisTime) {
        this.isTakeExtraPrecept5ThisTime = isTakeExtraPrecept5ThisTime;
    }

    public Boolean getIsTakePrecept5 () {
        return this.isTakePrecept5 == null ? false : isTakePrecept5;
    }

    public void setIsTakePrecept5 (Boolean isTakePrecept5 ) {
        this.isTakePrecept5  = isTakePrecept5 ;
    }

    public Boolean getIsTakePrecept5ThisTime() {
        return this.isTakePrecept5ThisTime == null ? false : isTakePrecept5ThisTime;
    }

    public void setIsTakePrecept5ThisTime(Boolean isTakePrecept5ThisTime) {
        this.isTakePrecept5ThisTime = isTakePrecept5ThisTime;
    }

    public Boolean getIsTakeRefuge() {
        return this.isTakeRefuge == null ? false : isTakeRefuge;
    }

    public void setIsTakeRefuge(Boolean isTakeRefuge) {
        this.isTakeRefuge = isTakeRefuge;
    }

    public Boolean getIsTakeRefugeThisTime() {
        return this.isTakeRefugeThisTime == null ? false : isTakeRefugeThisTime;
    }

    public void setIsTakeRefugeThisTime(Boolean isTakeRefugeThisTime) {
        this.isTakeRefugeThisTime = isTakeRefugeThisTime;
    }

    public Boolean getIsTreasurer() {
        return this.isTreasurer == null ? false : isTreasurer;
    }

    public void setIsTreasurer(Boolean isTreasurer) {
        this.isTreasurer = isTreasurer;
    }

    public Boolean getIsWaitForCtSync() {
        return this.isWaitForCtSync == null ? false : isWaitForCtSync;
    }

    public void setIsWaitForCtSync(Boolean isWaitForCtSync) {
        this.isWaitForCtSync = isWaitForCtSync;
    }

    public String getJobOrder() {
        return this.jobOrder;
    }

    public void setJobOrder(String jobOrder) {
        this.jobOrder = jobOrder;
    }

    public String getJobTitle() {
        return this.jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Date getLeaveCtDate() {
        return this.leaveCtDate;
    }

    public void setLeaveCtDate(Date leaveCtDate) {
        this.leaveCtDate = leaveCtDate;
    }

    public Date getLeaveCtTime() {
        return this.leaveCtTime;
    }

    public void setLeaveCtTime(Date leaveCtTime) {
        this.leaveCtTime = leaveCtTime;
    }

    public Date getLeaveUnitDate() {
        return this.leaveUnitDate;
    }

    public void setLeaveUnitDate(Date leaveUnitDate) {
        this.leaveUnitDate = leaveUnitDate;
    }

    public Date getLeaveUnitTime() {
        return this.leaveUnitTime;
    }

    public void setLeaveUnitTime(Date leaveUnitTime) {
        this.leaveUnitTime = leaveUnitTime;
    }

    public String getMailingCity() {
        return this.mailingCity;
    }

    public void setMailingCity(String mailingCity) {
        this.mailingCity = mailingCity;
    }

    public String getMailingCountry() {
        return this.mailingCountry;
    }

    public void setMailingCountry(String mailingCountry) {
        this.mailingCountry = mailingCountry;
    }

    public String getMailingState() {
        return this.mailingState;
    }

    public void setMailingState(String mailingState) {
        this.mailingState = mailingState;
    }

    public String getMailingStreet() {
        return this.mailingStreet;
    }

    public void setMailingStreet(String mailingStreet) {
        this.mailingStreet = mailingStreet;
    }

    public String getMailingZip() {
        return this.mailingZip;
    }

    public void setMailingZip(String mailingZip) {
        this.mailingZip = mailingZip;
    }

    public String getMasterDormNote() {
        return this.masterDormNote;
    }

    public void setMasterDormNote(String masterDormNote) {
        this.masterDormNote = masterDormNote;
    }

    public String getMasterPreceptTypeName() {
        return this.masterPreceptTypeName;
    }

    public void setMasterPreceptTypeName(String masterPreceptTypeName) {
        this.masterPreceptTypeName = masterPreceptTypeName;
    }

    public String getMobileNum1() {
        return this.mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getOfficePhoneNum1() {
        return this.officePhoneNum1;
    }

    public void setOfficePhoneNum1(String officePhoneNum1) {
        this.officePhoneNum1 = officePhoneNum1;
    }

    public String getOnThatDayDonation() {
        return this.onThatDayDonation;
    }

    public void setOnThatDayDonation(String onThatDayDonation) {
        this.onThatDayDonation = onThatDayDonation;
    }

    public String getParentsJobTitle() {
        return this.parentsJobTitle;
    }

    public void setParentsJobTitle(String parentsJobTitle) {
        this.parentsJobTitle = parentsJobTitle;
    }

    public String getParentsName() {
        return this.parentsName;
    }

    public void setParentsName(String parentsName) {
        this.parentsName = parentsName;
    }

    public String getParentsPhoneNum() {
        return this.parentsPhoneNum;
    }

    public void setParentsPhoneNum(String parentsPhoneNum) {
        this.parentsPhoneNum = parentsPhoneNum;
    }

    public String getPassportNum() {
        return this.passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public String getPreceptOrder() {
        return this.preceptOrder;
    }

    public void setPreceptOrder(String preceptOrder) {
        this.preceptOrder = preceptOrder;
    }

    public int getPreceptVlntrCount() {
        return this.preceptVlntrCount;
    }

    public void setPreceptVlntrCount(int preceptVlntrCount) {
        this.preceptVlntrCount = preceptVlntrCount;
    }

    public Integer getRelativeAge() {
        return this.relativeAge;
    }

    public void setRelativeAge(Integer relativeAge) {
        this.relativeAge = relativeAge;
    }

    public String getRelativeMeritList() {
        return this.relativeMeritList;
    }

    public void setRelativeMeritList(String relativeMeritList) {
        this.relativeMeritList = relativeMeritList;
    }

    public String getRelativeName() {
        return this.relativeName;
    }

    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName;
    }

    public String getRelativeRelationship() {
        return this.relativeRelationship;
    }

    public void setRelativeRelationship(String relativeRelationship) {
        this.relativeRelationship = relativeRelationship;
    }

    public String getRelativeTwId() {
        return this.relativeTwId;
    }

    public void setRelativeTwId(String relativeTwId) {
        this.relativeTwId = relativeTwId;
    }

    public String getSchoolDegree() {
        return this.schoolDegree;
    }

    public void setSchoolDegree(String schoolDegree) {
        this.schoolDegree = schoolDegree;
    }

    public String getSchoolMajor() {
        return this.schoolMajor;
    }

    public void setSchoolMajor(String schoolMajor) {
        this.schoolMajor = schoolMajor;
    }

    public String getSchoolName() {
        return this.schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSkillList() {
        return this.skillList;
    }

    public void setSkillList(String skillList) {
        this.skillList = skillList;
    }

    public String getSocialTitle() {
        return this.socialTitle;
    }

    public void setSocialTitle(String socialTitle) {
        this.socialTitle = socialTitle;
    }

    public String getSpecialDonation() {
        return this.specialDonation;
    }

    public void setSpecialDonation(String specialDonation) {
        this.specialDonation = specialDonation;
    }

    public String getSpecialDonationCompleteNote() {
        return this.specialDonationCompleteNote;
    }

    public void setSpecialDonationCompleteNote(String specialDonationCompleteNote) {
        this.specialDonationCompleteNote = specialDonationCompleteNote;
    }

    public int getSpecialDonationSum() {
        return this.specialDonationSum;
    }

    public void setSpecialDonationSum(int specialDonationSum) {
        this.specialDonationSum = specialDonationSum;
    }

    public String getSponsorUnitId() {
        return this.sponsorUnitId;
    }

    public void setSponsorUnitId(String sponsorUnitId) {
        this.sponsorUnitId = sponsorUnitId;
    }

    public String getSponsorUnitName() {
        return this.sponsorUnitName;
    }

    public void setSponsorUnitName(String sponsorUnitName) {
        this.sponsorUnitName = sponsorUnitName;
    }

    public String getSummerDonation1() {
        return this.summerDonation1;
    }

    public void setSummerDonation1(String summerDonation1) {
        this.summerDonation1 = summerDonation1;
    }

    public int getSummerDonation1Count() {
        return this.summerDonation1Count;
    }

    public void setSummerDonation1Count(int summerDonation1Count) {
        this.summerDonation1Count = summerDonation1Count;
    }

    public int getSummerDonation1and2Sum() {
        return this.summerDonation1and2Sum;
    }

    public void setSummerDonation1and2Sum(int summerDonation1and2Sum) {
        this.summerDonation1and2Sum = summerDonation1and2Sum;
    }

    public String getSummerDonation2() {
        return this.summerDonation2;
    }

    public void setSummerDonation2(String summerDonation2) {
        this.summerDonation2 = summerDonation2;
    }

    public String getSummerDonationType() {
        return this.summerDonationType;
    }

    public void setSummerDonationType(String summerDonationType) {
        this.summerDonationType = summerDonationType;
    }

    public String getSummerRelativeMeritList() {
        return this.summerRelativeMeritList;
    }

    public void setSummerRelativeMeritList(String summerRelativeMeritList) {
        this.summerRelativeMeritList = summerRelativeMeritList;
    }

    public int getSummerVlntrCount() {
        return this.summerVlntrCount;
    }

    public void setSummerVlntrCount(int summerVlntrCount) {
        this.summerVlntrCount = summerVlntrCount;
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateUniqueId() {
        return this.templateUniqueId;
    }

    public void setTemplateUniqueId(String templateUniqueId) {
        this.templateUniqueId = templateUniqueId;
    }

    public String getTranslateLanguage() {
        return this.translateLanguage;
    }

    public void setTranslateLanguage(String translateLanguage) {
        this.translateLanguage = translateLanguage;
    }

    public String getTwIdNum() {
        return this.twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getUnitId() {
        return this.unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return this.unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUrgentContactPersonName1() {
        return this.urgentContactPersonName1;
    }

    public void setUrgentContactPersonName1(String urgentContactPersonName1) {
        this.urgentContactPersonName1 = urgentContactPersonName1;
    }

    public String getUrgentContactPersonPhoneNum1() {
        return this.urgentContactPersonPhoneNum1;
    }

    public void setUrgentContactPersonPhoneNum1(String urgentContactPersonPhoneNum1) {
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
    }

    public String getUrgentContactPersonRelationship1() {
        return this.urgentContactPersonRelationship1;
    }

    public void setUrgentContactPersonRelationship1(String urgentContactPersonRelationship1) {
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
    }

    public String getVehicleNumJobTitleList() {
        return this.vehicleNumJobTitleList;
    }

    public void setVehicleNumJobTitleList(String vehicleNumJobTitleList) {
        this.vehicleNumJobTitleList = vehicleNumJobTitleList;
    }

    public Date getVlntrEndDate() {
        return this.vlntrEndDate;
    }

    public void setVlntrEndDate(Date vlntrEndDate) {
        this.vlntrEndDate = vlntrEndDate;
    }

    public Date getVlntrEndTime() {
        return this.vlntrEndTime;
    }

    public void setVlntrEndTime(Date vlntrEndTime) {
        this.vlntrEndTime = vlntrEndTime;
    }

    public String getVlntrGroupNameIdList() {
        return this.vlntrGroupNameIdList;
    }

    public void setVlntrGroupNameIdList(String vlntrGroupNameIdList) {
        this.vlntrGroupNameIdList = vlntrGroupNameIdList;
    }

    public Date getVlntrStartDate() {
        return this.vlntrStartDate;
    }

    public void setVlntrStartDate(Date vlntrStartDate) {
        this.vlntrStartDate = vlntrStartDate;
    }

    public Date getVlntrStartTime() {
        return this.vlntrStartTime;
    }

    public void setVlntrStartTime(Date vlntrStartTime) {
        this.vlntrStartTime = vlntrStartTime;
    }

    public Double getWeight() {
        return this.weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public int getZen7Count() {
        return this.zen7Count;
    }

    public void setZen7Count(int zen7Count) {
        this.zen7Count = zen7Count;
    }

    public int getZen7VlntrCount() {
        return this.zen7VlntrCount;
    }

    public void setZen7VlntrCount(int zen7VlntrCount) {
        this.zen7VlntrCount = zen7VlntrCount;
    }


    public void copyEventEnrollMainAndEventEnrollPgm(EventEnrollMain m, List<EventEnrollPgm> eventEnrollPgmList) {
        this.address = m.getAddress();//*
        this.age = m.getAge();//*
        this.arriveCtDate = m.getArriveCtDate();//*
        this.arriveCtTime = m.getArriveCtTime();//*
        this.birthDate = m.getBirthDate();//*
        this.bodhiPreceptDate = m.getBodhiPreceptDate();//*
        this.cancelledDate = m.getCancelledDate();//*
        this.carLicenseNum1 = m.getCarLicenseNum1();//*
        this.classGroupId = m.getClassGroupId();
        this.classId = m.getClassId();
        this.classListName = m.getClassList();//*
        this.className = m.getClassName();
        this.classNameAndJobList = m.getClassNameAndJobList();//*
        this.classPeriodNum = m.getClassPeriodNum();
        this.classTypeName = m.getClassTypeName();
        this.classTypeNum = m.getClassTypeNum();
        this.clubJobTitleNow = m.getClubJobTitleNow();//*
        this.clubJobTitlePrev = m.getClubJobTitlePrev();//*
        this.clubName = m.getClubName();//*
        this.companyJobTitle = m.getCompanyJobTitle();//*
        this.companyName = m.getCompanyName();//*
        this.ctDormChangeHistory = m.getCtDormChangeHistory();//*
        this.ctDormEndDate = m.getCtDormEndDate();//*
        this.ctDormLatestChange = m.getCtDormLatestChange();//*
        this.ctDormPurpose = m.getCtDormPurpose();//*
        this.ctDormStartDate = m.getCtDormStartDate();//*
        this.ctNote1 = m.getCtNote1();//*
        this.ctNote2 = m.getCtNote2();//*
        this.ctNote3 = m.getCtNote3();//*
        this.ctNote4 = m.getCtNote4();//*
        this.ctNote5 = m.getCtNote5();//*
        this.ctRefugeMaster = m.getCtRefugeMaster();//*
        this.degreeInfo = m.getDegreeInfo();//*
        this.donationNameList = m.getDonationNameList();
        this.dormNote = m.getDormNote();//*
        this.dsaJobNameList = m.getDsaJobNameList();
        this.email1 = m.getEmail1();//*
        this.enrollChangeHistory = m.getEnrollChangeHistory();//*
        this.enrollGroupName = m.getEnrollGroupName();//*
        this.enrollLatestChange = m.getEnrollLatestChange();//*
        this.enrollNote = m.getEnrollNote();//*
        this.enrollOrderNum = m.getEnrollOrderNum();//*
        this.enrollUnitId = m.getEnrollUnitId();//*
        this.enrollUnitName = m.getEnrollUnitName();//*
        this.enrollUserCtDharmaName = m.getEnrollUserCtDharmaName();//*
        this.enrollUserId = m.getEnrollUserId();//*
        this.enrollUserName = m.getEnrollUserName();//*
        this.eventCategoryId = m.getEventCategoryId();
        this.eventCreatorUnitId = m.getEventCreatorUnitId();//*
        this.eventCreatorUnitName = m.getEventCreatorUnitName();//*
        this.eventEnrollId = (int)m.getId();
        this.eventFormUid = m.getEventFormUid();//*
        this.eventId = m.getEventId();//*
        this.eventName = m.getEventName();//*
        this.familyLeaderMemberId = m.getFamilyLeaderMemberId();//*
        this.familyLeaderName = m.getFamilyLeaderName();//*
        this.formAutoAddNum = m.getFormAutoAddNum();//*
        this.gender = m.getGender();//*
        this.healthList = m.getHealthList();//*
        this.height = m.getHeight();//*
        this.highestPreceptName = m.getHighestPreceptName();//*
        this.homeClassId = m.getHomeClassId();//*
        this.homeClassNote = m.getHomeClassNote();//*
        this.homePhoneNum1 = m.getHomePhoneNum1();//*
        this.idType1 = m.getIdType1();//*
        this.idType2 = m.getIdType2();//*
        this.introducerName = m.getIntroducerName();//*
        this.introducerPhoneNum = m.getIntroducerPhoneNum();//*
        this.introducerRelationship = m.getIntroducerRelationship();//*
        this.isAbbot = m.getIsAbbot();//*
        this.isCanceled = m.getIsCanceled();//*
        this.isCancelPrecept5ThisTime = m.getIsCancelPrecept5ThisTime();//*
        this.isCancelRefugeThisTime = m.getIsCancelRefugeThisTime();//*
        this.isCtDorm = m.getIsCtDorm();//*
        this.isEarlyBookCtDorm = m.getIsEarlyBookCtDorm();//*
        this.isGoCtBySelf = m.getIsGoCtBySelf();//*
        this.isMaster = m.getIsMaster();//*
        this.isNeedExtraPrecept5CertThisTime = m.getIsNeedExtraPrecept5CertThisTime();//*
        this.isNeedParkingPass = m.getIsNeedParkingPass();//*
        this.isNeedPrecept5RobeThisTime = m.getIsNeedPrecept5RobeThisTime();//*
        this.isNeedTranslator = m.getIsNeedTranslator();//*
        this.isNotDorm = m.getIsNotDorm();//*
        this.isRelativePreceptOrZen7Vlntr = m.getIsRelativePreceptOrZen7Vlntr();//*
        this.isRelativeTakePreceptOrZen7 = m.getIsRelativeTakePreceptOrZen7();//*
        this.isSentAfterEnrollEndDate = m.getIsSentAfterEnrollEndDate();//*
        this.isTakeBodhiPrecept = m.getIsTakeBodhiPrecept();//*
        this.isTakeExtraPrecept5ThisTime = m.getIsTakeExtraPrecept5ThisTime();//*
        this.isTakePrecept5 = m.getIsTakePrecept5();//*
        this.isTakePrecept5ThisTime = m.getIsTakePrecept5ThisTime();//*
        this.isTakeRefuge = m.getIsTakeRefuge();//*
        this.isTakeRefugeThisTime = m.getIsTakeRefugeThisTime();//*
        this.isTreasurer = m.getIsTreasurer();//*
        this.isWaitForCtSync = m.getIsWaitForCtSync();
        this.jobOrder = m.getJobOrder();//*
        this.jobTitle = m.getJobTitle();//*
        this.leaveCtDate = m.getLeaveCtDate();//*
        this.leaveCtTime = m.getLeaveCtTime();//*
        this.leaveUnitDate = m.getLeaveUnitDate();//*
        this.leaveUnitTime = m.getLeaveUnitTime();//*
        this.mailingCity = m.getMailingCity();//*
        this.mailingCountry = m.getMailingCountry();//*
        this.mailingState = m.getMailingState();//*
        this.mailingStreet = m.getMailingStreet();//*
        this.mailingZip = m.getMailingZip();//*
        this.masterDormNote = m.getMasterDormNote();//*
        this.masterPreceptTypeName = m.getMasterPreceptTypeName();//*
        this.mobileNum1 = m.getMobileNum1();//*
        this.officePhoneNum1 = m.getOfficePhoneNum1();//*
        this.onThatDayDonation = m.getOnThatDayDonation();//*
        this.parentsJobTitle = m.getParentsJobTitle();
        this.parentsName = m.getParentsName();//*
        this.parentsPhoneNum = m.getParentsPhoneNum();//*
        this.passportNum = m.getPassportNum();//*
        this.precept5Date = m.getPrecept5Date();//*
        this.preceptOrder = m.getPreceptOrder();//*
        this.preceptVlntrCount = m.getPreceptVlntrCount();//*
        this.refugeDate = m.getRefugeDate();//*
        this.relativeAge = m.getRelativeAge();//*
        this.relativeMeritList = m.getRelativeMeritList();//*
        this.relativeName = m.getRelativeName();//*
        this.relativeRelationship = m.getRelativeRelationship();//*
        this.relativeTwId = m.getRelativeTwId();//*
        this.schoolDegree = m.getSchoolDegree();//*
        this.schoolMajor = m.getSchoolMajor();//*
        this.schoolName = m.getSchoolName();//*
        this.skillList = m.getSkillList();//*
        this.socialTitle = m.getSocialTitle();//*
        this.specialDonation = m.getSpecialDonation();//*
        this.specialDonationCompleteNote = m.getSpecialDonationCompleteNote();//*
        this.specialDonationSum = m.getSpecialDonationSum();//*
        this.sponsorUnitId = m.getSponsorUnitId();//*
        this.sponsorUnitName = m.getSponsorUnitName();//*
        this.summerDonation1 = m.getSummerDonation1();//*
        this.summerDonation1and2Sum = m.getSummerDonation1and2Sum();//*
        this.summerDonation1Count = m.getSummerDonation1Count();//*
        this.summerDonation2 = m.getSummerDonation2();//*
        this.summerDonationType = m.getSummerDonationType();//*
        this.summerRelativeMeritList = m.getSummerRelativeMeritList();//*
        this.summerVlntrCount = m.getSummerVlntrCount();//*
        this.templateName = m.getTemplateName();//*
        this.templateUniqueId = m.getTemplateUniqueId();//*
        this.translateLanguage = m.getTranslateLanguage();//*
        this.twIdNum = m.getTwIdNum();
        this.unitId = m.getUnitId();//*
        this.unitName = m.getUnitName();//*
        this.urgentContactPersonName1 = m.getUrgentContactPersonName1();//*
        this.urgentContactPersonPhoneNum1 = m.getUrgentContactPersonPhoneNum1();//*
        this.urgentContactPersonRelationship1 = m.getUrgentContactPersonRelationship1();//*
        this.vehicleNumJobTitleList = m.getVehicleNumJobTitleList();//*
        this.vlntrEndDate = m.getVlntrEndDate();//*
        this.vlntrEndTime = m.getVlntrEndTime();//*
        this.vlntrGroupNameIdList = m.getVlntrGroupNameIdList();//*
        this.vlntrStartDate = m.getVlntrStartDate();//*
        this.vlntrStartTime = m.getVlntrStartTime();//*
        this.weight = m.getWeight();//*
        this.zen7Count = m.getZen7Count();//*
        this.zen7VlntrCount = m.getZen7VlntrCount();//*

        this.setCreateDtTm(m.getCreateDtTm());
        this.setCreatorId(m.getCreatorId());
        this.setCreatorIp(m.getCreatorIp());
        this.setCreatorName(m.getCreatorName());
        this.setCreatorUnitName(m.getCreatorUnitName());
        this.setCreatorUnitId(m.getCreatorUnitId());
        this.setUpdateDtTm(m.getUpdateDtTm());
        this.setUpdatorId(m.getUpdatorId());
        this.setUpdatorIp(m.getUpdatorIp());
        this.setUpdatorName(m.getUpdatorName());
        this.setUpdatorUnitName(m.getUpdatorUnitName());
        this.setUpdatorUnitId(m.getUpdatorUnitId());
        
        for (EventEnrollPgm p : eventEnrollPgmList) {

            if (p.getPgmCtOrderNum() != null) {

                String value = null;

                if (p.getIsGenPgm()) {

                    value = p.getGenPgmUserInputValue();
                }else {

                    value = "1";
                }

                if (p.getPgmCtOrderNum() >= 1 && p.getPgmCtOrderNum() <= 50) {

                    try {
                        Method method = this.getClass().getMethod("setCtPgm" + String.format("%02d", p.getPgmCtOrderNum()), value.getClass());
                        method.invoke(this, value);
                    } catch (Exception e) {
//                        logger.debug(e.getMessage());
                    }
                }
            }
        }
    }
}
