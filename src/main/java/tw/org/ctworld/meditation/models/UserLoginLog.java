package tw.org.ctworld.meditation.models;

import tw.org.ctworld.meditation.beans.UserSession;

import javax.persistence.*;
import javax.servlet.http.HttpSession;
import java.util.Date;

@Entity
@Table(name="user_login_log")
public class UserLoginLog extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 20)
    private String roleType;
    @Column(length = 9)
    private String userID;
    @Column(length = 60)
    private String userName;
    @Column(length = 9)
    private String unitId;
    @Column(length = 30)
    private String unitName;
    @Column(length = 20)
    private String userIP;
    private Date loginTime;
    private Date logoutTime;
    @Column(length = 64, unique = true)
    private String sessionId;

    public UserLoginLog() {
    }

    public UserLoginLog(HttpSession session) {
        this.roleType = (String) session.getAttribute(UserSession.Keys.RoleType.getValue());
        this.userID = (String) session.getAttribute(UserSession.Keys.UserId.getValue());
        this.userName = (String) session.getAttribute(UserSession.Keys.UserName.getValue());
        this.unitId = (String) session.getAttribute(UserSession.Keys.UnitId.getValue());
        this.unitName = (String) session.getAttribute(UserSession.Keys.UnitName.getValue());
        this.userIP = (String) session.getAttribute(UserSession.Keys.UserIP.getValue());
        this.sessionId = session.getId();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUserIP() {
        return userIP;
    }

    public void setUserIP(String userIP) {
        this.userIP = userIP;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Date getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(Date logoutTime) {
        this.logoutTime = logoutTime;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    
}
