package tw.org.ctworld.meditation.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import tw.org.ctworld.meditation.libs.CommonUtils;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ct_member_info")
public class CtMemberInfo extends BaseEntity {

    public enum Gender {
        Male("M", "男"), Female("F", "女");

        private final String value;
        private final String code;

        Gender(String code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public String getCode() {
            return code;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean after2012Oct;

    private Integer age;

    @Column(length = 150)
    private String aliasName;

    @Column(length = 50)
    private String birthCity;

    @Column(length = 50)
    private String birthCountry;

    private Date birthDate;

    @Column(length = 50)
    private String birthProvince;

    @Column(length = 50)
    private String bodhiPreceptDate;

    @Column(length = 50)
    private String carLicenseNum1;

    @Column(length = 50)
    private String carLicenseNum2;

    @Column(length = 50)
    private String classList;

    @Column(length = 50)
    private String clubJobTitleNow;

    @Column(length = 50)
    private String clubJobTitlePrev;

    @Column(length = 50)
    private String clubName;

    @Column(length = 50)
    private String comingReason;

    @Column(length = 50)
    private String companyJobTitle;

    @Column(length = 150)
    private String companyName;

    @Column(length = 50)
    private String ctDharmaName;

    @Column(length = 50)
    private String ctRefugeMaster;

    private Date dataSentToCTDate;

    @Column(length = 50)
    private String dataUnitMemberId;

    @Column(length = 200)
    private String donationNameList;

    @Column(length = 100)
    private String dsaJobNameList;

    @Column(length = 100)
    private String email1;

    @Column(length = 100)
    private String email2;

    @Column(length = 150)
    private String engFirstName;

    @Column(length = 150)
    private String engLastName;

    @Column(length = 50)
    private String familyCity;

    @Column(length = 50)
    private String familyCountry;

    @Column(length = 50)
    private String familyLeaderMemberId;

    @Column(length = 50)
    private String familyLeaderName;

    @Column(length = 50)
    private String familyLeaderRelationship;

    @Column(length = 50)
    private String familyProvince;

    @Column(length = 100)
    private String firstEnrolledClass;

    @Column(length = 150)
    private String firstName;

    @Column(length = 150)
    private String fullName;

    @Column(length = 50)
    private String gender;

    @Column(columnDefinition = "TEXT")
    private String healthList;

    @Column(columnDefinition = "DOUBLE(5,2)")
    private Double height;

    @Column(length = 50)
    private String homePhoneNum1;

    @Column(length = 50)
    private String homePhoneNum2;

    @Column(length = 50)
    private String introducerName;

    @Column(length = 50)
    private String introducerPhoneNum;

    @Column(length = 50)
    private String introducerRelationship;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isBodyHealthy;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isDeleteByUnit;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isMentalHealthy;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isPhotoCompleted;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isPiSigned;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeBodhiPrecept;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakePrecept5;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTakeRefuge;

    @Column(length = 50)
    private String lastClassEnrolledDesc;

    @Column(length = 50)
    private String lastEventEnrolledDesc;

    @Column(length = 150)
    private String lastName;

    @Column(length = 50)
    private String mailingCity;

    @Column(length = 50)
    private String mailingCountry;

    @Column(length = 50)
    private String mailingState;

    @Column(columnDefinition = "TEXT")
    private String mailingStreet;

    @Column(length = 50)
    private String mailingZip;

    @Column(length = 50)
    private String marriageStatus;

    @Column(length = 50, unique = true)
    private String memberId;

    @Column(length = 150)
    private String middleName;

    @Column(length = 50)
    private String mobileNum1;

    @Column(length = 50)
    private String mobileNum2;

    @Column(length = 50)
    private String motoLicenseNum1;

    @Column(length = 50)
    private String nameTail;

    @Column(length = 50)
    private String nationality;

    @Column(columnDefinition = "TEXT")
    private String note;

    @Column(length = 50)
    private String officePhoneNum1;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean okSendEletter;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean okSendEmail;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean okSendMagazine;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean okSendMessage;

    @Column(length = 50)
    private String okToCallType;

    @Column(length = 50)
    private String otherDharmaName;

    @Column(length = 50)
    private String otherRefugeMaster;

    @Column(length = 100)
    private String parentsEmployer;

    @Column(length = 50)
    private String parentsJobTitle;

    @Column(length = 50)
    private String parentsName;

    @Column(length = 50)
    private String parentsPhoneNum;

    @Column(length = 100)
    private String passportNum;

    @Column(length = 50)
    private String permanentCity;

    @Column(length = 50)
    private String permanentCountry;

    @Column(length = 50)
    private String permanentState;

    @Column(columnDefinition = "TEXT")
    private String permanentStreet;

    @Column(length = 50)
    private String permanentZip;

    @Column(length = 50)
    private String piDocId;

    @Column(length = 50)
    private String precept5Date;

    private Integer preceptVlntrCount;

    @Column(length = 50)
    private String refugeDate;

    @Column(columnDefinition = "TEXT")
    private String relativeMeritList;

    @Column(length = 50)
    private String schoolDegree;

    @Column(length = 50)
    private String schoolMajor;

    @Column(length = 50)
    private String schoolName;

    @Column(columnDefinition = "TEXT")
    private String skillList;

    @Column(length = 50)
    private String sourceMemberId;

    private Integer summerVlntrCount;

    @Column(length = 50)
    private String thisPersonStatus;

    @Column(length = 100)
    private String twIdNum;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;

    @Column(length = 50)
    private String urgentContactPersonDharmaName1;

    @Column(length = 50)
    private String urgentContactPersonDharmaName2;

    @Column(length = 50)
    private String urgentContactPersonName1;

    @Column(length = 50)
    private String urgentContactPersonName2;

    @Column(length = 50)
    private String urgentContactPersonPhoneNum1;

    @Column(length = 50)
    private String urgentContactPersonPhoneNum2;

    @Column(length = 50)
    private String urgentContactPersonRelationship1;

    @Column(length = 50)
    private String urgentContactPersonRelationship2;

    @Column(columnDefinition = "TEXT")
    private String vlntrGroupNameIdList;

    @Column(columnDefinition = "DOUBLE(5,2)")
    private Double weight;

    private Integer zen7Count;

    private Integer zen7VlntrCount;


    @Transient
    private String photo;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getSourceMemberId() {
        return sourceMemberId;
    }

    public void setSourceMemberId(String sourceMemberId) {
        this.sourceMemberId = sourceMemberId;
    }

    public String getFamilyLeaderName() {
        return familyLeaderName;
    }

    public void setFamilyLeaderName(String familyLeaderName) {
        this.familyLeaderName = familyLeaderName;
    }

    public String getFamilyLeaderMemberId() {
        return familyLeaderMemberId;
    }

    public void setFamilyLeaderMemberId(String familyLeaderMemberId) {
        this.familyLeaderMemberId = familyLeaderMemberId;
    }

    public String getFamilyLeaderRelationship() {
        return familyLeaderRelationship;
    }

    public void setFamilyLeaderRelationship(String familyLeaderRelationship) {
        this.familyLeaderRelationship = familyLeaderRelationship;
    }

    public String getIntroducerName() {
        return introducerName;
    }

    public void setIntroducerName(String introducerName) {
        this.introducerName = introducerName;
    }

    public String getIntroducerRelationship() {
        return introducerRelationship;
    }

    public void setIntroducerRelationship(String introducerRelationship) {
        this.introducerRelationship = introducerRelationship;
    }

    public String getIntroducerPhoneNum() {
        return introducerPhoneNum;
    }

    public void setIntroducerPhoneNum(String introducerPhoneNum) {
        this.introducerPhoneNum = introducerPhoneNum;
    }

    public String getNameTail() {
        return nameTail;
    }

    public void setNameTail(String nameTail) {
        this.nameTail = nameTail;
    }

    public Boolean getPhotoCompleted() {
        return isPhotoCompleted == null ? false : isPhotoCompleted;
    }

    public void setPhotoCompleted(Boolean photoCompleted) {
        isPhotoCompleted = photoCompleted;
    }

    public String getDonationNameList() {
        return donationNameList;
    }

    public void setDonationNameList(String donationNameList) {
        this.donationNameList = donationNameList;
    }

    public String getDsaJobNameList() {
        return dsaJobNameList;
    }

    public void setDsaJobNameList(String dsaJobNameList) {
        this.dsaJobNameList = dsaJobNameList;
    }

    public String getRelativeMeritList() {
        return relativeMeritList;
    }

    public void setRelativeMeritList(String relativeMeritList) {
        this.relativeMeritList = relativeMeritList;
    }

    public String getHealthList() {
        return healthList;
    }

    public void setHealthList(String healthList) {
        this.healthList = healthList;
    }

    public String getSkillList() {
        return skillList;
    }

    public void setSkillList(String skillList) {
        this.skillList = skillList;
    }

    public Integer getPreceptVlntrCount() {
        return preceptVlntrCount != null ? preceptVlntrCount : 0;
    }

    public void setPreceptVlntrCount(Integer preceptVlntrCount) {
        this.preceptVlntrCount = preceptVlntrCount;
    }

    public String getMotoLicenseNum1() {
        return motoLicenseNum1;
    }

    public void setMotoLicenseNum1(String motoLicenseNum1) {
        this.motoLicenseNum1 = motoLicenseNum1;
    }

    public String getDataUnitMemberId() {
        return dataUnitMemberId;
    }

    public void setDataUnitMemberId(String dataUnitMemberId) {
        this.dataUnitMemberId = dataUnitMemberId;
    }

    public Date getDataSentToCTDate() {
        return dataSentToCTDate;
    }

    public void setDataSentToCTDate(Date dataSentToCTDate) {
        this.dataSentToCTDate = dataSentToCTDate;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getClubJobTitleNow() {
        return clubJobTitleNow;
    }

    public void setClubJobTitleNow(String clubJobTitleNow) {
        this.clubJobTitleNow = clubJobTitleNow;
    }

    public String getClubJobTitlePrev() {
        return clubJobTitlePrev;
    }

    public void setClubJobTitlePrev(String clubJobTitlePrev) {
        this.clubJobTitlePrev = clubJobTitlePrev;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getCtRefugeMaster() {
        return ctRefugeMaster;
    }

    public void setCtRefugeMaster(String ctRefugeMaster) {
        this.ctRefugeMaster = ctRefugeMaster;
    }

    public String getOtherDharmaName() {
        return otherDharmaName;
    }

    public void setOtherDharmaName(String otherDharmaName) {
        this.otherDharmaName = otherDharmaName;
    }

    public String getOtherRefugeMaster() {
        return otherRefugeMaster;
    }

    public void setOtherRefugeMaster(String otherRefugeMaster) {
        this.otherRefugeMaster = otherRefugeMaster;
    }

    public String getEngFirstName() {
        return engFirstName;
    }

    public void setEngFirstName(String engFirstName) {
        this.engFirstName = engFirstName;
    }

    public String getEngLastName() {
        return engLastName;
    }

    public void setEngLastName(String engLastName) {
        this.engLastName = engLastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTwIdNum() {
        return twIdNum;
    }

    public void setTwIdNum(String twIdNum) {
        this.twIdNum = twIdNum;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getMobileNum1() {
        return mobileNum1;
    }

    public void setMobileNum1(String mobileNum1) {
        this.mobileNum1 = mobileNum1;
    }

    public String getMobileNum2() {
        return mobileNum2;
    }

    public void setMobileNum2(String mobileNum2) {
        this.mobileNum2 = mobileNum2;
    }

    public String getHomePhoneNum1() {
        return homePhoneNum1;
    }

    public void setHomePhoneNum1(String homePhoneNum1) {
        this.homePhoneNum1 = homePhoneNum1;
    }

    public String getHomePhoneNum2() {
        return homePhoneNum2;
    }

    public void setHomePhoneNum2(String homePhoneNum2) {
        this.homePhoneNum2 = homePhoneNum2;
    }

    public String getUrgentContactPersonName1() {
        return urgentContactPersonName1;
    }

    public void setUrgentContactPersonName1(String urgentContactPersonName1) {
        this.urgentContactPersonName1 = urgentContactPersonName1;
    }

    public String getUrgentContactPersonDharmaName1() {
        return urgentContactPersonDharmaName1;
    }

    public void setUrgentContactPersonDharmaName1(String urgentContactPersonDharmaName1) {
        this.urgentContactPersonDharmaName1 = urgentContactPersonDharmaName1;
    }

    public String getUrgentContactPersonRelationship1() {
        return urgentContactPersonRelationship1;
    }

    public void setUrgentContactPersonRelationship1(String urgentContactPersonRelationship1) {
        this.urgentContactPersonRelationship1 = urgentContactPersonRelationship1;
    }

    public String getUrgentContactPersonPhoneNum1() {
        return urgentContactPersonPhoneNum1;
    }

    public void setUrgentContactPersonPhoneNum1(String urgentContactPersonPhoneNum1) {
        this.urgentContactPersonPhoneNum1 = urgentContactPersonPhoneNum1;
    }

    public String getUrgentContactPersonName2() {
        return urgentContactPersonName2;
    }

    public void setUrgentContactPersonName2(String urgentContactPersonName2) {
        this.urgentContactPersonName2 = urgentContactPersonName2;
    }

    public String getUrgentContactPersonDharmaName2() {
        return urgentContactPersonDharmaName2;
    }

    public void setUrgentContactPersonDharmaName2(String urgentContactPersonDharmaName2) {
        this.urgentContactPersonDharmaName2 = urgentContactPersonDharmaName2;
    }

    public String getUrgentContactPersonRelationship2() {
        return urgentContactPersonRelationship2;
    }

    public void setUrgentContactPersonRelationship2(String urgentContactPersonRelationship2) {
        this.urgentContactPersonRelationship2 = urgentContactPersonRelationship2;
    }

    public String getUrgentContactPersonPhoneNum2() {
        return urgentContactPersonPhoneNum2;
    }

    public void setUrgentContactPersonPhoneNum2(String urgentContactPersonPhoneNum2) {
        this.urgentContactPersonPhoneNum2 = urgentContactPersonPhoneNum2;
    }

    public String getClassList() {
        return classList;
    }

    public void setClassList(String classList) {
        this.classList = classList;
    }

    public String getFirstEnrolledClass() {
        return firstEnrolledClass;
    }

    public void setFirstEnrolledClass(String firstEnrolledClass) {
        this.firstEnrolledClass = firstEnrolledClass;
    }

    public String getThisPersonStatus() {
        return thisPersonStatus;
    }

    public void setThisPersonStatus(String thisPersonStatus) {
        this.thisPersonStatus = thisPersonStatus;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getFamilyCountry() {
        return familyCountry;
    }

    public void setFamilyCountry(String familyCountry) {
        this.familyCountry = familyCountry;
    }

    public String getFamilyProvince() {
        return familyProvince;
    }

    public void setFamilyProvince(String familyProvince) {
        this.familyProvince = familyProvince;
    }

    public String getFamilyCity() {
        return familyCity;
    }

    public void setFamilyCity(String familyCity) {
        this.familyCity = familyCity;
    }

    public String getBirthCountry() {
        return birthCountry;
    }

    public void setBirthCountry(String birthCountry) {
        this.birthCountry = birthCountry;
    }

    public String getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(String birthProvince) {
        this.birthProvince = birthProvince;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getMarriageStatus() {
        return marriageStatus;
    }

    public void setMarriageStatus(String marriageStatus) {
        this.marriageStatus = marriageStatus;
    }

    public String getMailingStreet() {
        return mailingStreet;
    }

    public void setMailingStreet(String mailingStreet) {
        this.mailingStreet = mailingStreet;
    }

    public String getMailingCity() {
        return mailingCity;
    }

    public void setMailingCity(String mailingCity) {
        this.mailingCity = mailingCity;
    }

    public String getMailingState() {
        return mailingState;
    }

    public void setMailingState(String mailingState) {
        this.mailingState = mailingState;
    }

    public String getMailingZip() {
        return mailingZip;
    }

    public void setMailingZip(String mailingZip) {
        this.mailingZip = mailingZip;
    }

    public String getMailingCountry() {
        return mailingCountry;
    }

    public void setMailingCountry(String mailingCountry) {
        this.mailingCountry = mailingCountry;
    }

    public String getPermanentStreet() {
        return permanentStreet;
    }

    public void setPermanentStreet(String permanentStreet) {
        this.permanentStreet = permanentStreet;
    }

    public String getPermanentCity() {
        return permanentCity;
    }

    public void setPermanentCity(String permanentCity) {
        this.permanentCity = permanentCity;
    }

    public String getPermanentState() {
        return permanentState;
    }

    public void setPermanentState(String permanentState) {
        this.permanentState = permanentState;
    }

    public String getPermanentZip() {
        return permanentZip;
    }

    public void setPermanentZip(String permanentZip) {
        this.permanentZip = permanentZip;
    }

    public String getPermanentCountry() {
        return permanentCountry;
    }

    public void setPermanentCountry(String permanentCountry) {
        this.permanentCountry = permanentCountry;
    }

    public Boolean getOkSendEmail() {
        return okSendEmail == null ? false : okSendEmail;
    }

    public void setOkSendEmail(Boolean okSendEmail) {
        this.okSendEmail = okSendEmail;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolMajor() {
        return schoolMajor;
    }

    public void setSchoolMajor(String schoolMajor) {
        this.schoolMajor = schoolMajor;
    }

    public String getSchoolDegree() {
        return schoolDegree;
    }

    public void setSchoolDegree(String schoolDegree) {
        this.schoolDegree = schoolDegree;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyJobTitle() {
        return companyJobTitle;
    }

    public void setCompanyJobTitle(String companyJobTitle) {
        this.companyJobTitle = companyJobTitle;
    }

    public String getOfficePhoneNum1() {
        return officePhoneNum1;
    }

    public void setOfficePhoneNum1(String officePhoneNum1) {
        this.officePhoneNum1 = officePhoneNum1;
    }

    public String getComingReason() {
        return comingReason;
    }

    public void setComingReason(String comingReason) {
        this.comingReason = comingReason;
    }

    public Boolean getOkSendMagazine() {
        return okSendMagazine == null ? false : okSendMagazine;
    }

    public void setOkSendMagazine(Boolean okSendMagazine) {
        this.okSendMagazine = okSendMagazine;
    }

    public Boolean getOkSendEletter() {
        return okSendEletter == null ? false : okSendEletter;
    }

    public void setOkSendEletter(Boolean okSendEletter) {
        this.okSendEletter = okSendEletter;
    }

    public Boolean getOkSendMessage() {
        return okSendMessage == null ? false : okSendMessage;
    }

    public void setOkSendMessage(Boolean okSendMessage) {
        this.okSendMessage = okSendMessage;
    }

    public String getOkToCallType() {
        return okToCallType;
    }

    public void setOkToCallType(String okToCallType) {
        this.okToCallType = okToCallType;
    }

    public Integer getZen7Count() {
        return zen7Count != null ? zen7Count : 0;
    }

    public void setZen7Count(Integer zen7Count) {
        this.zen7Count = zen7Count;
    }

    public Integer getZen7VlntrCount() {
        return zen7VlntrCount != null ? zen7VlntrCount : 0;
    }

    public void setZen7VlntrCount(Integer zen7VlntrCount) {
        this.zen7VlntrCount = zen7VlntrCount;
    }

    public Integer getSummerVlntrCount() {
        return summerVlntrCount != null ? summerVlntrCount : 0;
    }

    public void setSummerVlntrCount(Integer summerVlntrCount) {
        this.summerVlntrCount = summerVlntrCount;
    }

    public Boolean getIsTakeRefuge() {
        return isTakeRefuge == null ? false : isTakeRefuge;
    }

    public void setIsTakeRefuge(Boolean takeRefuge) {
        isTakeRefuge = takeRefuge;
    }

    public String getRefugeDate() {
        return refugeDate;
    }

    public void setRefugeDate(String refugeDate) {
        this.refugeDate = refugeDate;
    }

    public Boolean getIsTakePrecept5() {
        return isTakePrecept5 == null ? false : isTakePrecept5;
    }

    public void setIsTakePrecept5(Boolean takePrecept5) {
        isTakePrecept5 = takePrecept5;
    }

    public String getPrecept5Date() {
        return precept5Date;
    }

    public void setPrecept5Date(String precept5Date) {
        this.precept5Date = precept5Date;
    }

    public Boolean getIsTakeBodhiPrecept() {
        return isTakeBodhiPrecept == null ? false : isTakeBodhiPrecept;
    }

    public void setIsTakeBodhiPrecept(Boolean takeBodhiPrecept) {
        isTakeBodhiPrecept = takeBodhiPrecept;
    }

    public String getBodhiPreceptDate() {
        return bodhiPreceptDate;
    }

    public void setBodhiPreceptDate(String bodhiPreceptDate) {
        this.bodhiPreceptDate = bodhiPreceptDate;
    }

    public String getCarLicenseNum1() {
        return carLicenseNum1;
    }

    public void setCarLicenseNum1(String carLicenseNum1) {
        this.carLicenseNum1 = carLicenseNum1;
    }

    public String getCarLicenseNum2() {
        return carLicenseNum2;
    }

    public void setCarLicenseNum2(String carLicenseNum2) {
        this.carLicenseNum2 = carLicenseNum2;
    }

    public Boolean getAfter2012Oct() {
        return after2012Oct == null ? false : after2012Oct;
    }

    public void setAfter2012Oct(Boolean after2012Oct) {
        this.after2012Oct = after2012Oct;
    }

    public Boolean getIsPiSigned() {
        return isPiSigned == null ? false : isPiSigned;
    }

    public void setIsPiSigned(Boolean piSigned) {
        isPiSigned = piSigned;
    }

    public String getPiDocId() {
        return piDocId;
    }

    public void setPiDocId(String piDocId) {
        this.piDocId = piDocId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getAge() {
        return age != null ? age : 0;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getLastClassEnrolledDesc() {
        return lastClassEnrolledDesc;
    }

    public void setLastClassEnrolledDesc(String lastClassEnrolledDesc) {
        this.lastClassEnrolledDesc = lastClassEnrolledDesc;
    }

    public String getLastEventEnrolledDesc() {
        return lastEventEnrolledDesc;
    }

    public void setLastEventEnrolledDesc(String lastEventEnrolledDesc) {
        this.lastEventEnrolledDesc = lastEventEnrolledDesc;
    }

    public Boolean getDeleteByUnit() {
        return isDeleteByUnit == null ? false : isDeleteByUnit;
    }

    public void setDeleteByUnit(Boolean deleteByUnit) {
        isDeleteByUnit = deleteByUnit;
    }

    public String getVlntrGroupNameIdList() {
        return vlntrGroupNameIdList;
    }

    public void setVlntrGroupNameIdList(String vlntrGroupNameIdList) {
        this.vlntrGroupNameIdList = vlntrGroupNameIdList;
    }

    public String getParentsEmployer() {
        return parentsEmployer;
    }

    public void setParentsEmployer(String parentsEmployer) {
        this.parentsEmployer = parentsEmployer;
    }

    public String getParentsName() {
        return parentsName;
    }

    public void setParentsName(String parentsName) {
        this.parentsName = parentsName;
    }

    public String getParentsPhoneNum() {
        return parentsPhoneNum;
    }

    public void setParentsPhoneNum(String parentsPhoneNum) {
        this.parentsPhoneNum = parentsPhoneNum;
    }

    public String getParentsJobTitle() {
        return parentsJobTitle;
    }

    public void setParentsJobTitle(String parentsJobTitle) {
        this.parentsJobTitle = parentsJobTitle;
    }

    public Boolean getIsMentalHealthy() {
        return isMentalHealthy == null ? false : isMentalHealthy;
    }

    public void setIsMentalHealthy(Boolean mentalHealthy) {
        isMentalHealthy = mentalHealthy;
    }

    public Boolean getIsBodyHealthy() {
        return isBodyHealthy == null ? false : isBodyHealthy;
    }

    public void setIsBodyHealthy(Boolean bodyHealthy) {
        isBodyHealthy = bodyHealthy;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void updateAge() {
        if (this.birthDate != null) {
            Date now = new Date();
            long timeBetween = now.getTime() - this.birthDate.getTime();
            double yearsBetween = timeBetween / 3.15576e+10;
            this.age = (int) Math.floor(yearsBetween);
        }
    }

    public void copy(CtMemberInfo input, boolean isMaster) {
        this.fullName = input.getFullName();
        this.nameTail = input.getNameTail();
        this.aliasName = input.getAliasName();
        this.ctDharmaName = input.getCtDharmaName();
        this.gender = input.getGender();
        this.twIdNum = input.getTwIdNum();
        this.email1 = input.getEmail1();
        this.schoolDegree = input.getSchoolDegree();
        this.schoolName = input.getSchoolName();
        this.schoolMajor = input.getSchoolMajor();
        this.okSendEletter = input.getOkSendEletter();
        this.okSendMagazine = input.getOkSendMagazine();
        this.mailingCountry = input.getMailingCountry();
        this.mailingState = input.getMailingState();
        this.mailingCity = input.getMailingCity();
        this.mailingStreet = input.getMailingStreet();
        this.companyName = input.getCompanyName();
        this.companyJobTitle = input.getCompanyJobTitle();
        this.familyLeaderName = input.getFamilyLeaderName();
        this.familyLeaderMemberId = input.getFamilyLeaderMemberId();
        this.familyLeaderRelationship = input.getFamilyLeaderRelationship();
        this.urgentContactPersonName1 = input.getUrgentContactPersonName1();
        this.urgentContactPersonDharmaName1 = input.getUrgentContactPersonDharmaName1();
        this.urgentContactPersonPhoneNum1 = input.getUrgentContactPersonPhoneNum1();
        this.urgentContactPersonRelationship1 = input.getUrgentContactPersonRelationship1();
        this.introducerName = input.getIntroducerName();
        this.introducerPhoneNum = input.getIntroducerPhoneNum();
        this.introducerRelationship = input.getIntroducerRelationship();
        this.refugeDate = input.getRefugeDate();
        this.precept5Date = input.getPrecept5Date();
        this.bodhiPreceptDate = input.getBodhiPreceptDate();
        this.dataUnitMemberId = input.getDataUnitMemberId();
        this.dataSentToCTDate = input.getDataSentToCTDate();
        this.skillList = input.getSkillList();
        this.thisPersonStatus = input.getThisPersonStatus();
        this.birthDate = input.getBirthDate();
        this.birthCountry = input.getBirthCountry();
        this.birthCity = input.getBirthCity();
        this.homePhoneNum1 = input.getHomePhoneNum1();
        this.mobileNum1 = input.getMobileNum1();
        this.passportNum = input.getPassportNum();
        this.ctRefugeMaster = input.getCtRefugeMaster();
        this.otherDharmaName = input.getOtherDharmaName();
        this.otherRefugeMaster = input.getOtherRefugeMaster();
        this.okToCallType = input.getOkToCallType();
        this.classList = input.getClassList();
        this.email2 = input.getEmail2();
        this.okSendEmail = input.getOkSendEmail();
        this.lastName = input.getLastName();
        this.middleName = input.getMiddleName();
        this.firstName = input.getFirstName();
        this.okSendMessage = input.getOkSendMessage();
        this.engLastName = input.getEngLastName();
        this.engFirstName = input.getEngFirstName();
        this.height = input.getHeight();
        this.weight = input.getWeight();
        this.isPiSigned = input.getIsPiSigned();
        this.piDocId = input.getPiDocId();
        this.relativeMeritList = input.getRelativeMeritList();
        this.marriageStatus = input.getMarriageStatus();
        this.mobileNum2 = input.getMobileNum2();
        this.homePhoneNum2 = input.getHomePhoneNum2();
        this.urgentContactPersonName2 = input.getUrgentContactPersonName2();
        this.urgentContactPersonDharmaName2 = input.getUrgentContactPersonDharmaName2();
        this.urgentContactPersonRelationship2 = input.getUrgentContactPersonRelationship2();
        this.urgentContactPersonPhoneNum2 = input.getUrgentContactPersonPhoneNum2();
        this.zen7Count = input.getZen7Count();
        this.zen7VlntrCount = input.getZen7VlntrCount();
        this.summerVlntrCount = input.getSummerVlntrCount();
        this.preceptVlntrCount = input.getPreceptVlntrCount();
        this.mailingZip = input.getMailingZip();
        this.birthProvince = input.getBirthProvince();
        this.familyCountry = input.getFamilyCountry();
        this.familyProvince = input.getFamilyProvince();
        this.familyCity = input.getFamilyCity();
        this.nationality = input.getNationality();
        this.permanentCountry = input.getPermanentCountry();
        this.permanentZip = input.getPermanentZip();
        this.permanentState = input.getPermanentState();
        this.permanentCity = input.getPermanentCity();
        this.permanentStreet = input.getPermanentStreet();
        this.clubName = input.getClubName();
        this.clubJobTitleNow = input.getClubJobTitleNow();
        this.clubJobTitlePrev = input.getClubJobTitlePrev();
        this.motoLicenseNum1 = input.getMotoLicenseNum1();
        this.carLicenseNum1 = input.getCarLicenseNum1();
        this.carLicenseNum2 = input.getCarLicenseNum2();

        if (isMaster) {
            this.note = input.getNote();
            this.donationNameList = input.getDonationNameList();
            this.healthList = input.getHealthList();
        }

        this.isMentalHealthy = input.getIsMentalHealthy();
        this.isBodyHealthy = input.getIsBodyHealthy();

        this.officePhoneNum1 = input.getOfficePhoneNum1();
        this.comingReason = input.getComingReason();
        this.isTakeRefuge = input.getIsTakeRefuge();
        this.isTakePrecept5 = input.getIsTakePrecept5();
        this.isTakeBodhiPrecept = input.getIsTakeBodhiPrecept();
    }

    public void updateFromEnroll(EventEnrollMain enrollMain) {
        this.aliasName = enrollMain.getEnrollUserName();
        this.ctDharmaName = enrollMain.getEnrollUserCtDharmaName();
        this.gender = enrollMain.getGender();
        this.birthDate = enrollMain.getBirthDate();
        this.age = enrollMain.getAge();
        this.classList = enrollMain.getClassList();
        this.twIdNum = enrollMain.getTwIdNum();
        this.donationNameList = enrollMain.getDonationNameList();
        this.healthList = enrollMain.getHealthList();
        this.isBodyHealthy = enrollMain.getIsBodyHealthy();
        this.isMentalHealthy = enrollMain.getIsMentalHealthy();
        this.relativeMeritList = enrollMain.getRelativeMeritList();
        this.urgentContactPersonName1 = enrollMain.getUrgentContactPersonName1();
        this.urgentContactPersonRelationship1 = enrollMain.getUrgentContactPersonRelationship1();
        this.urgentContactPersonPhoneNum1 = enrollMain.getUrgentContactPersonPhoneNum1();
        this.carLicenseNum1 = enrollMain.getCarLicenseNum1();
        this.vlntrGroupNameIdList = enrollMain.getVlntrGroupNameIdList();
        this.introducerName = enrollMain.getIntroducerName();
        this.introducerRelationship = enrollMain.getIntroducerRelationship();
        this.introducerPhoneNum = enrollMain.getIntroducerPhoneNum();
        this.ctRefugeMaster = enrollMain.getCtRefugeMaster();
        this.isTakeRefuge = enrollMain.getIsTakeRefuge();
        this.refugeDate = CommonUtils.DateTime2DateString(enrollMain.getRefugeDate());
        this.isTakePrecept5 = enrollMain.getIsTakePrecept5();
        this.precept5Date = CommonUtils.DateTime2DateString(enrollMain.getPrecept5Date());
        this.isTakeBodhiPrecept = enrollMain.getIsTakeBodhiPrecept();
        this.bodhiPreceptDate = CommonUtils.DateTime2DateString(enrollMain.getBodhiPreceptDate());
        this.zen7Count = enrollMain.getZen7Count();
        this.zen7VlntrCount = enrollMain.getZen7VlntrCount();
        this.summerVlntrCount = enrollMain.getSummerVlntrCount();
        this.preceptVlntrCount = enrollMain.getPreceptVlntrCount();
        this.homePhoneNum1 = enrollMain.getHomePhoneNum1();
        this.mobileNum1 = enrollMain.getMobileNum1();
        this.passportNum = enrollMain.getPassportNum();
        this.mailingZip = enrollMain.getMailingZip();
        this.mailingCountry = enrollMain.getMailingCountry();
        this.mailingState = enrollMain.getMailingState();
        this.mailingCity = enrollMain.getMailingCity();
        this.mailingStreet = enrollMain.getMailingStreet();
        this.companyName = enrollMain.getCompanyName();
        this.companyJobTitle = enrollMain.getCompanyJobTitle();
        this.officePhoneNum1 = enrollMain.getOfficePhoneNum1();
        this.schoolDegree = enrollMain.getSchoolDegree();
        this.schoolName = enrollMain.getSchoolName();
        this.schoolMajor = enrollMain.getSchoolMajor();
        this.clubName = enrollMain.getClubName();
        this.clubJobTitleNow = enrollMain.getClubJobTitleNow();
        this.clubJobTitlePrev = enrollMain.getClubJobTitlePrev();
        this.email1 = enrollMain.getEmail1();
        this.skillList = enrollMain.getSkillList();
        this.height = enrollMain.getHeight();
        this.weight = enrollMain.getWeight();
        this.parentsName = enrollMain.getParentsName();
        this.parentsPhoneNum = enrollMain.getParentsPhoneNum();
        this.parentsJobTitle = enrollMain.getParentsJobTitle();

//        updateAge();
    }
}
