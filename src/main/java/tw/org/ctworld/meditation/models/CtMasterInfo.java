/*
* Auto Generated
* Based on name: ct_master_info
* Date: 2019-06-18 15:46:19
*/

package tw.org.ctworld.meditation.models;

import java.util.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="ct_master_info")
public class CtMasterInfo extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isAbbot;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isTreasurer;

    @Column(length = 50)
    private String jobOrder;

    @Column(length = 200)
    private String jobTitle;

    @Column(length = 50)
    private String masterId;

    @Column(length = 50)
    private String masterName;

    @Column(length = 50)
    private String masterPreceptTypeName;

    @Column(length = 50)
    private String mobileNum;

    @Column(length = 50)
    private String preceptOrder;

    @Column(length = 50)
    private String unitId;

    @Column(length = 50)
    private String unitName;


    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getIsAbbot() {
        return this.isAbbot == null ? false : isAbbot;
    }

    public void setIsAbbot(Boolean isAbbot) {
        this.isAbbot = isAbbot;
    }

    public Boolean getIsTreasurer() {
        return this.isTreasurer == null ? false : isTreasurer;
    }

    public void setIsTreasurer(Boolean isTreasurer) {
        this.isTreasurer = isTreasurer;
    }

    public String getJobOrder() {
        return this.jobOrder;
    }

    public void setJobOrder(String jobOrder) {
        this.jobOrder = jobOrder;
    }

    public String getJobTitle() {
        return this.jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getMasterId() {
        return this.masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterName() {
        return this.masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getMasterPreceptTypeName() {
        return masterPreceptTypeName;
    }

    public void setMasterPreceptTypeName(String masterPreceptTypeName) {
        this.masterPreceptTypeName = masterPreceptTypeName;
    }

    public String getMobileNum() {
        return this.mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getPreceptOrder() {
        return this.preceptOrder;
    }

    public void setPreceptOrder(String preceptOrder) {
        this.preceptOrder = preceptOrder;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
