package tw.org.ctworld.meditation.models;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name="class_attend_record")
public class ClassAttendRecord extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 9)
    private String unitId;
    @Column(length = 30)
    private String unitName;
    @Column(length = 13)
    private String classEnrollFormId;
    @Column(length = 9)
    private String memberId;
    @Column(length = 1)
    private String gender;
    @Column(length = 60)
    private String aliasName;
    @Column(length = 30)
    private String ctDharmaName;
    @Column(length = 12)
    private String classId;
    @Column(length = 2)
    private String classPeriodNum;
    @Column(length = 30)
    private String className;
    @Column(length = 2)
    private String classGroupId;
    @Column(columnDefinition = "INT(3)")
    private int classWeeksNum;
    private Date classDate;
    private Date classStartDtTm;
    @Column(length = 10)
    private String attendMark;
    private Date attendCheckinDtTm;
    @Column(length = 300)
    private String note;

    public ClassAttendRecord() {
    }

    public ClassAttendRecord(String unitId, String unitName, String classEnrollFormId,
                             String memberId, String gender, String aliasName, String ctDharmaName, String classId,
                             String classPeriodNum, String className, String classGroupId, int classWeeksNum,
                             Date classDate, Date classStartDtTm, String attendMark, Date attendCheckinDtTm,
                             String note) {
        this.unitId = unitId;
        this.unitName = unitName;
        this.classEnrollFormId = classEnrollFormId;
        this.memberId = memberId;
        this.gender = gender;
        this.aliasName = aliasName;
        this.ctDharmaName = ctDharmaName;
        this.classId = classId;
        this.classPeriodNum = classPeriodNum;
        this.className = className;
        this.classGroupId = classGroupId;
        this.classWeeksNum = classWeeksNum;
        this.classDate = classDate;
        this.classStartDtTm = classStartDtTm;
        this.attendMark = attendMark;
        this.attendCheckinDtTm = attendCheckinDtTm;
        this.note = note;
    }

    public ClassAttendRecord(String classEnrollFormId, String attendMark) {
        this.classEnrollFormId = classEnrollFormId;
        this.attendMark = attendMark;
    }

    public ClassAttendRecord(String classId, int classWeeksNum, Date classDate, String attendMark, String note) {
        this.classId = classId;
        this.classWeeksNum = classWeeksNum;
        this.classDate = classDate;
        this.attendMark = attendMark;
        this.note = note;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getClassEnrollFormId() {
        return classEnrollFormId;
    }

    public void setClassEnrollFormId(String classEnrollFormId) {
        this.classEnrollFormId = classEnrollFormId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getCtDharmaName() {
        return ctDharmaName;
    }

    public void setCtDharmaName(String ctDharmaName) {
        this.ctDharmaName = ctDharmaName;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassPeriodNum() {
        return classPeriodNum;
    }

    public void setClassPeriodNum(String classPeriodNum) {
        this.classPeriodNum = classPeriodNum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(String classGroupId) {
        this.classGroupId = classGroupId;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }

    public Date getClassStartDtTm() {
        return classStartDtTm;
    }

    public void setClassStartDtTm(Date classStartDtTm) {
        this.classStartDtTm = classStartDtTm;
    }

    public String getAttendMark() {
        return attendMark;
    }

    public void setAttendMark(String attendMark) {
        this.attendMark = attendMark;
    }

    public Date getAttendCheckinDtTm() {
        return attendCheckinDtTm;
    }

    public void setAttendCheckinDtTm(Date attendCheckinDtTm) {
        this.attendCheckinDtTm = attendCheckinDtTm;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
