package tw.org.ctworld.meditation.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name="class_date_info")
public class ClassDateInfo extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 9)
    private String unitId;
    @Column(length = 30)
    private String unitName;

    @Column(length = 12)
    private String classId;
    @Column(columnDefinition = "INT(3)")
    private int classWeeksNum;
    private Date classDate;
    @Column(length = 1)
    private String classDayOfWeek;
    @Column(length = 200)
    private String classContent;
    @Column(columnDefinition = "LONGTEXT")
    private String actualClassContent;

    private Time classStartTime;
    private Time classEndTime;

    @JsonProperty
    @Column(columnDefinition = "TINYINT(1)")
    private Boolean isCancelled;

    @Column(length = 100)
    private String makeupFileName;
//    @Column(length = 50)
//    private String makeupFileId;
    @Column(length = 50)
    private String makeupFileType;
    private int fileTimeTotalLength;
    @Column(columnDefinition = "TEXT")
    private String originFileName;
    @Column(columnDefinition = "TEXT")
    private String userDefinedFileDesc;
    private int makeupFileSizeMB;
    private Date uploadDtTm;
    @Column(length = 50)
    private String makeupFileProcessStatus;
    @Column(columnDefinition = "TEXT")
    private String fileProcessedLog;
    @Transient
    private String className;

    public ClassDateInfo() {
    }

    public ClassDateInfo(String classId, int classWeeksNum, Date classDate, Time classStartTime) {
        this.classId = classId;
        this.classWeeksNum = classWeeksNum;
        this.classDate = classDate;
        this.classStartTime = classStartTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public int getClassWeeksNum() {
        return classWeeksNum;
    }

    public void setClassWeeksNum(int classWeeksNum) {
        this.classWeeksNum = classWeeksNum;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }

    public String getClassDayOfWeek() {
        return classDayOfWeek;
    }

    public void setClassDayOfWeek(String classDayOfWeek) {
        this.classDayOfWeek = classDayOfWeek;
    }

    public String getClassContent() {
        return classContent;
    }

    public void setClassContent(String classContent) {
        this.classContent = classContent;
    }

    public String getActualClassContent() {
        return actualClassContent;
    }

    public void setActualClassContent(String actualClassContent) {
        this.actualClassContent = actualClassContent;
    }

    public Time getClassStartTime() {
        return classStartTime;
    }

    public void setClassStartTime(Time classStartTime) {
        this.classStartTime = classStartTime;
    }

    public Time getClassEndTime() {
        return classEndTime;
    }

    public void setClassEndTime(Time classEndTime) {
        this.classEndTime = classEndTime;
    }

    public Boolean getCancelled() {
        return isCancelled == null ? false : isCancelled;
    }

    public void setCancelled(Boolean cancelled) {
        isCancelled = cancelled;
    }

    public String getMakeupFileName() {
        return makeupFileName;
    }

    public void setMakeupFileName(String makeupFileName) {
        this.makeupFileName = makeupFileName;
    }

//    public String getMakeupFileId() {
//        return makeupFileId;
//    }
//
//    public void setMakeupFileId(String makeupFileId) {
//        this.makeupFileId = makeupFileId;
//    }

    public String getMakeupFileType() {
        return makeupFileType;
    }

    public void setMakeupFileType(String makeupFileType) {
        this.makeupFileType = makeupFileType;
    }

    public int getFileTimeTotalLength() {
        return fileTimeTotalLength;
    }

    public void setFileTimeTotalLength(int fileTimeTotalLength) {
        this.fileTimeTotalLength = fileTimeTotalLength;
    }

    public String getOriginFileName() {
        return originFileName;
    }

    public void setOriginFileName(String originFileName) {
        this.originFileName = originFileName;
    }

    public String getUserDefinedFileDesc() {
        return userDefinedFileDesc;
    }

    public void setUserDefinedFileDesc(String userDefinedFileDesc) {
        this.userDefinedFileDesc = userDefinedFileDesc;
    }

    public int getMakeupFileSizeMB() {
        return makeupFileSizeMB;
    }

    public void setMakeupFileSizeMB(int makeupFileSizeMB) {
        this.makeupFileSizeMB = makeupFileSizeMB;
    }

    public Date getUploadDtTm() {
        return uploadDtTm;
    }

    public void setUploadDtTm(Date uploadDtTm) {
        this.uploadDtTm = uploadDtTm;
    }

    public String getMakeupFileProcessStatus() {
        return makeupFileProcessStatus;
    }

    public void setMakeupFileProcessStatus(String makeupFileProcessStatus) {
        this.makeupFileProcessStatus = makeupFileProcessStatus;
    }

    public String getFileProcessedLog() {
        return fileProcessedLog;
    }

    public void setFileProcessedLog(String fileProcessedLog) {
        this.fileProcessedLog = fileProcessedLog;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
