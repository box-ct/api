package tw.org.ctworld.meditation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tw.org.ctworld.meditation.services.*;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

@Component
public class ScheduledTasks {

    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;

    @Autowired
    private ClassDateInfoService classDateInfoService;

    @Autowired
    private SysCodeService sysCodeService;

    @Autowired
    private EventMainService eventMainService;

    @Autowired
    private CtTableSyncService ctTableSyncService;


    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    // parameters: second, minute, hour, day of month, month, day(s) of week
    @Scheduled(cron = "0 0 1 * * *")
    public void am0100() {
        logger.info("The time is now {}, delete temp files", new Date());

        fileStorageService.cleanTempFiles(true);
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void midnight() {
        logger.info("The time is now {}, update class/event status", new Date());

        classInfoService.updateClassStatus();

        // event task #3
        eventMainService.updateEventStatus();
    }


    // check update age hr setting every hr, if it's time do update, ok
    @Scheduled(cron = "0 0 * * * *")
    public void updateMemberAge() {
        int hour = sysCodeService.getUpdateAgeHour();

        if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == hour) {
            logger.info("The time is now {}, update age now", new Date());
            ctMemberInfoService.updateMemberAge();
        }
    }

    // delete makeup file over a year every month 3rd day at 3am, ok
    @Scheduled(cron = "0 0 3 3 * *")
    public void deleteMakeFiles() {
        logger.info("The time is now {}, delete makeup files over a year", new Date());

        classDateInfoService.DeleteMakeupFilesOverYear();
    }


    @Scheduled(cron = "0 0 * * * *")
    @Scheduled(cron = "0 30 * * * *")
    public void event2Ct30min() {
        LocalDateTime now = LocalDateTime.now();
        logger.info("The time is now {}", now);

//        if ((now.getHour() >= 8 && now.getHour() <= 10) || (now.getHour() >= 14 && now.getHour() <= 16) || (now.getHour() >= 20 && now.getHour() <= 22)) {
//            if (!((now.getHour() == 10 || now.getHour() == 16 || now.getHour() == 22) && now.getMinute() >= 30)) {
//
//                ctTableSyncService.updateEventFormForCt(false);
//            }
//        }

        if (now.getHour() % 6 == 0 && now.getMinute() <= 5) {
            // event task #2
            logger.debug("update with updateDtTm");
            ctTableSyncService.updateEventFormForCt(true);
        } else {
            // event task #1
            logger.debug("update with waitForSync");
            ctTableSyncService.updateEventFormForCt(false);
        }
    }


//    @Scheduled(cron = "0 0 0 5 * *")
//    @Scheduled(cron = "0 0 6 * * *")
//    @Scheduled(cron = "0 0 12 * * *")
//    @Scheduled(cron = "0 0 18 * * *")
//    public void event2Ct6h() {
//        LocalDateTime now = LocalDateTime.now();
//        logger.info("The time is now {}", now);
//
//        ctTableSyncService.updateEventFormForCt(true);
//    }
}
