package tw.org.ctworld.meditation.controllers;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.Makeup.*;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.models.ClassDateInfo;
import tw.org.ctworld.meditation.services.ClassDateInfoService;
import tw.org.ctworld.meditation.services.ClassInfoService;
import tw.org.ctworld.meditation.services.FileStorageService;
import tw.org.ctworld.meditation.services.VideoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(path = "/api/makeup")
public class MakeupController {

    private static final Logger logger = LoggerFactory.getLogger(MakeupController.class);

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private ClassDateInfoService classDateInfoService;

    @Autowired
    private VideoService videoService;

    @Autowired
    private FileStorageService fileStorageService;

    @RequestMapping(value = "/units", method = RequestMethod.GET)
    public ResponseEntity units(HttpServletRequest request,
                                @RequestParam("year") String year) {

        logger.debug("api/makeup/units " + year);

        return ResponseEntity.ok(new UnitsResponse(0, "success", classInfoService.getDistinctUnitByYear(year)));
    }

    @RequestMapping(value = "/class_status", method = RequestMethod.GET)
    public ResponseEntity class_status(HttpServletRequest request,
                                       @RequestParam("unitId") String unitId,
                                       @RequestParam("year") String year) {

        logger.debug("api/makeup/class_status " + unitId + "/" + year);

        return ResponseEntity.ok(new StatusResponse(0, "success", classInfoService.getDistinctStatusByYearUnit(year, unitId)));
    }

    @RequestMapping(value = "/classes", method = RequestMethod.GET)
    public ResponseEntity classes(HttpServletRequest request,
                                  @RequestParam(value = "unitId", required = false) String unitId,
                                  @RequestParam(value = "classStatus", required = false) String classStatus,
                                  @RequestParam(value = "year", required = false) String year) {

        logger.debug("api/makeup/classes " + unitId + "/" + classStatus + "/" + year);

        return ResponseEntity.ok(new ClassesResponse(0, "success", classInfoService.getClassByClassId(unitId, year, classStatus, null)));
    }

    @RequestMapping(value = "/classes_by_unit", method = RequestMethod.GET)
    public ResponseEntity classes_by_unit(HttpServletRequest request,
                                          @RequestParam(value = "classId", required = false) String classId,
                                          @RequestParam(value = "classStatus", required = false) String classStatus,
                                          @RequestParam(value = "year", required = false) String year) {

        String unitId = String.valueOf(request.getSession().getAttribute(UserSession.Keys.UnitId.getValue()));

        logger.debug("api/makeup/classes_by_unit " + unitId + "/" + classId + "/" + classStatus + "/" + year);

        return ResponseEntity.ok(new ClassesResponse(0, "success", classInfoService.getClassByClassId(unitId, year, classStatus, classId)));
    }

    @RequestMapping(value = "/class_date_infos", method = RequestMethod.GET)
    public ResponseEntity class_date_infos(HttpServletRequest request,
                                           @RequestParam("classId") String classId) {

        logger.debug("api/makeup/class_date_infos post " + classId);

        return ResponseEntity.ok(new ClassDateInfoResponse(0, "success", classDateInfoService.getClassDateByClassId(classId)));
    }

    @RequestMapping(value = "/class_date_infos/{id}", method = RequestMethod.POST)
    public ResponseEntity class_date_infos_post(HttpServletRequest request,
                                                @PathVariable(value = "id") long id,
                                                @RequestParam("files") MultipartFile[] files,
                                                @RequestParam("filesCombineNums") int[] filesCombineNums,
                                                @RequestParam("makeupFileType") String makeupFileType,
                                                @RequestParam("userDefinedFileDesc") String userDefinedFileDesc) {

        logger.debug("api/makeup/class_date_infos post " + id);

        try {

            List<String> formats = new ArrayList<>();
            List<String> tempFilenames = new ArrayList<>();

            // check format
            for (int i = 0; i < files.length; i++) {
                logger.debug("file name " + files[i].getOriginalFilename());

                String oriFilename = files[i].getOriginalFilename();
                String[] temp = oriFilename.split("\\.");
                String fileFormat = temp[temp.length - 1];

                // if format supported
                if (!videoService.isSupported(fileFormat)) {
                    return ResponseEntity.ok(new BaseResponse(404, "#" + i + " file format not supported"));
                } else {
                    formats.add(fileFormat);
                }

                // if all files are audio or video (not mixed)
                if (formats.size() > 1) {
                    if (videoService.isVideo(formats.get(i-1)) != videoService.isVideo(formats.get(i))) {
                        return ResponseEntity.ok(new BaseResponse(405, "mixed formats"));
                    }
                }
            }

            boolean isVideo = videoService.isVideo(formats.get(0));

            // get file name for class
            ClassDateInfo classDateInfo = classDateInfoService.getClassDateInfoById(id);
            String fileName = classDateInfoService.genMakeupFileNameById(classDateInfo);

            if (fileName == null) {
                return ResponseEntity.ok(new BaseResponse(406, "id or class not found"));
            }

            StringBuilder originalFileName = new StringBuilder();

            // save file to temp folder
            for (int i = 0; i < filesCombineNums.length; i++) {
                String tempFilename = fileName + "." + i;
                logger.debug(tempFilename);
                tempFilenames.add(tempFilename);
                fileStorageService.saveVideo(files[filesCombineNums[i]], tempFilename);

                originalFileName.append(files[filesCombineNums[i]].getOriginalFilename());

                if (i < (filesCombineNums.length - 1)) {
                    originalFileName.append("/");
                }
            }

            // update classDateInfo
            classDateInfo.setOriginFileName(originalFileName.toString());
            Date now = new Date();
            classDateInfo.setUploadDtTm(now);
            classDateInfo.setUserDefinedFileDesc(userDefinedFileDesc);
            classDateInfo.setMakeupFileType(isVideo ? "影音檔" : "聲音檔");
            classDateInfo.setMakeupFileName(fileName + (isVideo ? ".mp4" : ".mp3"));
            classDateInfo.setMakeupFileProcessStatus("上傳成功");
            String username = CommonUtils.Null2Empty((String)request.getSession().getAttribute(UserSession.Keys.UserName.getValue()));
            classDateInfo.setFileProcessedLog(classDateInfo.getFileProcessedLog() == null ? "" : classDateInfo.getFileProcessedLog() + username + CommonUtils.DateTime2DateTimeString(now) + " 上傳完成|");
            classDateInfoService.saveClassDateInfo(classDateInfo, request.getSession());

            // convert format and join
            new Thread(new Runnable(){
                @Override
                public void run(){
                    try {
                        int result = 0;

                        if (isVideo) {
                            result = videoService.joinVideos(tempFilenames, fileName + ".mp4");
                        } else {
                            result = videoService.joinAudios(tempFilenames, fileName + ".mp3");
                        }

                        Date now = new Date();

                        if (result == 0) {
                            classDateInfo.setMakeupFileProcessStatus("轉檔成功");
                            classDateInfo.setFileProcessedLog(classDateInfo.getFileProcessedLog() == null ? "" : classDateInfo.getFileProcessedLog() + username + CommonUtils.DateTime2DateTimeString(now) + " 轉檔完成|");
                            classDateInfo.setMakeupFileSizeMB(videoService.getFileSizeMB(classDateInfo.getMakeupFileName()));
                            classDateInfo.setFileTimeTotalLength(videoService.getMediaDuration(classDateInfo.getMakeupFileName()));

                        } else {
                            classDateInfo.setMakeupFileProcessStatus("轉檔失敗");
                            classDateInfo.setFileProcessedLog(classDateInfo.getFileProcessedLog() == null ? "" : classDateInfo.getFileProcessedLog() + username + CommonUtils.DateTime2DateTimeString(now) + " 轉檔失敗|");

                            classDateInfo.setOriginFileName(null);
                            classDateInfo.setUploadDtTm(null);
                            classDateInfo.setUserDefinedFileDesc(null);
                            classDateInfo.setMakeupFileType(null);
                            classDateInfo.setMakeupFileName(null);
                        }

                        classDateInfoService.saveClassDateInfo(classDateInfo, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.error(e.getMessage());
                    }
                }
            }).start();

            return ResponseEntity.ok(new FilenameResponse(0, "success", fileName));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return ResponseEntity.ok(new BaseResponse(400, e.getMessage()));
        }
    }

    @RequestMapping(value = "/class_date_infos/{id}", method = RequestMethod.DELETE)
    public ResponseEntity class_date_infos_delete(HttpServletRequest request,
                                                  @PathVariable(value = "id") long id) {

        logger.debug("api/makeup/class_date_infos delete " + id);

        boolean result = classDateInfoService.deleteMakeupFileById(id, request.getSession());

        return ResponseEntity.ok(new BaseResponse(result ? 0 : 400, result ? "success" : "id not found"));
    }

    @RequestMapping(value = "/remove_class_video", method = RequestMethod.POST)
    public ResponseEntity remove_class_video(HttpServletRequest request,
                                             @RequestParam("classIds") List<String> classIds) {

        logger.debug("api/makeup/remove_class_video " + classIds.size());

        boolean result = classDateInfoService.deleteMakeupFileByClassIds(classIds, request.getSession());

        return ResponseEntity.ok(new BaseResponse(result ? 0 : 400, result ? "success" : "classIds not found"));
    }
}
