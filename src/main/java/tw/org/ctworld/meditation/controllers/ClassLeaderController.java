package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.ClassLeader.ClassLeader003Response;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord000Response;
import tw.org.ctworld.meditation.beans.Classes.Classes007Response;
import tw.org.ctworld.meditation.beans.Classes.Classes008Request;
import tw.org.ctworld.meditation.beans.StringListResponse;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.services.ClassInfoService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/class_leader")
public class ClassLeaderController {

    private static final Logger logger = LoggerFactory.getLogger(ClassLeaderController.class);

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private ClassEnrollFormService classEnrollFormService;


    // CR3 get year list of classes
    @RequestMapping(value = "/years", method = RequestMethod.GET)
    public ResponseEntity classLeader001(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new StringListResponse(200, "success",
                classInfoService.getYears(unitId)));
    }

    // 取得上課記錄的狀態列表 ( 需卡法師 / 居士 / 學員長職事 身份) ( 開課中/待結業/已結業) OK
    // CR3
    @RequestMapping(value = "/class_status", method = RequestMethod.GET)
    public ResponseEntity classLeader002(HttpServletRequest request,
                                         @RequestParam(value = "year", required = false) String year) {

        return ResponseEntity.ok(new ClassRecord000Response(200, "success",
                classInfoService.getClassStatus(request.getSession(), year)));
    }

    // 取得「年度、狀態」取得課程列表，如果條件皆為空值，預設傳出「本期 "開課中"、"待結業" 所有課程"」
    // CR3
    @RequestMapping(value = "/class_infos", method = RequestMethod.GET)
    public ResponseEntity classLeader003(HttpServletRequest request,
                                         @RequestParam(value = "year", required = false) String year,
                                         @RequestParam(value = "classStatus", required = false) String classStatus) {

        return ResponseEntity.ok(new ClassLeader003Response(200, "success",
                classInfoService.getClassInfoListBySession(request.getSession(), year, classStatus)));
    }

    // 取得學員長職事資料
    // CR3
    @RequestMapping(value = "/leaders", method = RequestMethod.GET)
    public ResponseEntity classLeader004(HttpServletRequest request,
                                         @RequestParam(value = "classId") String classId) {

        return ResponseEntity.ok(new Classes007Response(200,"success",
                classEnrollFormService.getClassLeaderList(classId)));
    }

    // 新增學員長職事資料
    // CR3
    @RequestMapping(value = "/leaders", method = RequestMethod.POST)
    public ResponseEntity classLeader005(HttpServletRequest request,
                                         @RequestBody Classes008Request classes008Request) {

        return ResponseEntity.ok(
                classEnrollFormService.addClassEnrollFormJobTitle(request.getSession(), classes008Request));
    }

    // 取得學員長職事詳細資料
    // CR3
    @RequestMapping(value = "/leaders/{enrollFormId}", method = RequestMethod.GET)
    public ResponseEntity classLeader006(HttpServletRequest request,
                                         @PathVariable("enrollFormId") String classEnrollFormId) {

        return ResponseEntity.ok(classEnrollFormService.getClassEnrollForm(classEnrollFormId));
    }

    // 更新學員長職事詳細資料
    // CR3
    @RequestMapping(value = "/leaders/{enrollFormId}", method = RequestMethod.PUT)
    public ResponseEntity classLeader007(HttpServletRequest request,
                                         @PathVariable("enrollFormId") String classEnrollFormId,
                                         @RequestBody Classes008Request classes008Request) {

        return ResponseEntity.ok(new BaseResponse(200,
                classEnrollFormService.editClassEnrollFormJobTitle(
                        request.getSession(), classEnrollFormId, classes008Request)));
    }

    // 刪除學員長職事詳細資料
    // CR3
    @RequestMapping(value = "/leaders/{enrollFormId}", method = RequestMethod.DELETE)
    public ResponseEntity classLeader008(HttpServletRequest request,
                                         @PathVariable("enrollFormId") String classEnrollFormId) {

        return ResponseEntity.ok(new BaseResponse(200,
                classEnrollFormService.deleteClassEnrollFormJobTitle(request.getSession(), classEnrollFormId)));
    }

    // 取得課程學員列表
    // CR3
    @RequestMapping(value = "/leaders/enroll_forms", method = RequestMethod.GET)
    public ResponseEntity classLeader009(HttpServletRequest request,
                                         @RequestParam(value = "classId") String classId) {

        return ResponseEntity.ok(new Classes007Response(200, "success",
                classEnrollFormService.getClassMemberList(classId)));
    }
}
