package tw.org.ctworld.meditation.controllers;

import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.ListResponse;
import tw.org.ctworld.meditation.beans.Member.Members002ImagesObject;
import tw.org.ctworld.meditation.beans.MemberInfos.*;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.models.CtMemberInfo;
import tw.org.ctworld.meditation.services.CtMemberInfoService;
import tw.org.ctworld.meditation.services.ClassCtMemberNoteService;
import tw.org.ctworld.meditation.services.FileStorageService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(path = "/api/member_infos")
public class MemberInfosController {

    private static final Logger logger = LoggerFactory.getLogger(MemberInfosController.class);

    @Autowired
    private CtMemberInfoService ctMemberInfoService;

    @Autowired
    private ClassCtMemberNoteService classCtMemberNoteService;

    @Autowired
    private FileStorageService fileStorageService;

    // 取得學員資料列表(className & classPeriodNum帶上本期的資料)
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity members009(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(ctMemberInfoService.getAllMemberInfoListByUnitId(unitId));
    }

    @RequestMapping(value = "/check_same_name", method = RequestMethod.GET)
    public ResponseEntity check_same_name(HttpServletRequest request, @RequestParam("name") String name) {

        return ResponseEntity.ok(new ListResponse<>(200, "success", ctMemberInfoService.findSameName(name)));
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity add_member(HttpServletRequest request, @RequestBody NewMemberRequest newMemberRequest) {

        CtMemberInfo memberInfo = ctMemberInfoService.newMember(newMemberRequest, request.getSession(), false);

        return ResponseEntity.ok(new NewMemberResponse(200, "success", memberInfo));
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity get_list(HttpServletRequest request,
                                   @RequestParam(value = "name", required = false) String name,
                                   @RequestParam(value = "ctDharmaName", required = false) String ctDharmaName,
                                   @RequestParam(value = "twIdNum", required = false) String twIdNum,
                                   @RequestParam(value = "memberId", required = false) String memberId,
                                   @RequestParam(value = "birthDate", required = false) String birthDate,
                                   @RequestParam(value = "dsaJobNameList", required = false) String dsaJobNameList,
                                   @RequestParam(value = "email", required = false) String email,
                                   @RequestParam(value = "phone", required = false) String phone,
                                   @RequestParam(value = "address", required = false) String address,
                                   @RequestParam(value = "skill", required = false) String skill) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        String unitName = (String) request.getSession().getAttribute(UserSession.Keys.UnitName.getValue());
        logger.debug(unitId + ":" + unitName);

        List<SearchMemberInfo> memberInfoList =
                ctMemberInfoService.searchMember(
                        name, ctDharmaName, twIdNum, memberId, birthDate, dsaJobNameList, email, phone, address, skill, unitId);

        return ResponseEntity.ok(new SearchMemberInfoResponse(200, "success", memberInfoList));
    }

    @RequestMapping(value = "/ct_list", method = RequestMethod.GET)
    public ResponseEntity get_ct_list(HttpServletRequest request,
                                      @RequestParam(value = "name", required = false) String name,
                                      @RequestParam(value = "ctDharmaName", required = false) String ctDharmaName,
                                      @RequestParam(value = "twIdNum", required = false) String twIdNum,
                                      @RequestParam(value = "memberId", required = false) String memberId,
                                      @RequestParam(value = "birthDate", required = false) String birthDate,
                                      @RequestParam(value = "dsaJobNameList", required = false) String dsaJobNameList,
                                      @RequestParam(value = "email", required = false) String email,
                                      @RequestParam(value = "phone", required = false) String phone,
                                      @RequestParam(value = "address", required = false) String address,
                                      @RequestParam(value = "skill", required = false) String skill,
                                      @RequestParam(value = "unitId", required = false) String unitId) {

        List<SearchMemberInfo> memberInfoList =
                ctMemberInfoService.searchMember(
                        name, ctDharmaName, twIdNum, memberId, birthDate, dsaJobNameList, email, phone, address, skill, unitId);

        return ResponseEntity.ok(new SearchMemberInfoResponse(200, "success", memberInfoList));
    }

    @RequestMapping(value = "/list/{memberId}/profile", method = RequestMethod.GET)
    public ResponseEntity getProfile(HttpServletRequest request, @PathVariable("memberId") String memberId) {

        logger.debug("get profile " + memberId);

        CtMemberInfo member = ctMemberInfoService.getProfile(request.getSession(), memberId);

        return ResponseEntity.ok(new ClassCtMemberInfoResponse(member != null ? 200 : 400, member != null ? "success" : "member not found", member));
    }

    @RequestMapping(value = "/list/{memberId}/profile", method = RequestMethod.POST)
    public ResponseEntity saveProfile(HttpServletRequest request, @PathVariable("memberId") String memberId,
                                      @RequestBody CtMemberInfo memberInfo) {

        logger.debug("post profile " + memberId + "");

        boolean result = ctMemberInfoService.saveProfile(request.getSession(), memberId, memberInfo);

        return ResponseEntity.ok(new BaseResponse(result ? 200 : 400, result ? "success" : "member not found"));
    }

    @RequestMapping(value = "/list/{memberId}/other", method = RequestMethod.GET)
    public ResponseEntity getOther(HttpServletRequest request, @PathVariable("memberId") String memberId) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(ctMemberInfoService.getOther(unitId, memberId));
    }

    @RequestMapping(value = "/list/{memberId}/ct_other", method = RequestMethod.GET)
    public ResponseEntity getCtOther(HttpServletRequest request, @PathVariable("memberId") String memberId) {

        return ResponseEntity.ok(ctMemberInfoService.getOther(null, memberId));
    }

    @RequestMapping(value = "/list/{memberId}/notes", method = RequestMethod.GET)
    public ResponseEntity getNotes(HttpServletRequest request, @PathVariable("memberId") String memberId) {

        return ResponseEntity.ok(classCtMemberNoteService.getNote(memberId));
    }

    @RequestMapping(value = "/list/{memberId}/notes", method = RequestMethod.POST)
    public ResponseEntity getNotes(HttpServletRequest request, @PathVariable("memberId") String memberId,
                                   @RequestBody NewMemberNoteRequest noteRequest) {

        classCtMemberNoteService.createNote(memberId, noteRequest, request.getSession());

        return ResponseEntity.ok(new BaseResponse(200, "success"));
    }

    @RequestMapping(value = "/list/{memberId}/notes/{noteId}", method = RequestMethod.POST)
    public ResponseEntity deleteNotes(HttpServletRequest request,
                                      @PathVariable("memberId") String memberId,
                                      @PathVariable("noteId") int noteId) {

        boolean result = classCtMemberNoteService.deleteNote(noteId);

        return ResponseEntity.ok(new BaseResponse(result ? 200 : 400, result ? "success" : "failed"));
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public ResponseEntity deleteMember(HttpServletRequest request,
                                      @RequestBody MemberIds memberIds) {

        boolean result = ctMemberInfoService.deleteMembersById(memberIds, request.getSession());

        return ResponseEntity.ok(new BaseResponse(result ? 200 : 400, result ? "success" : "failed"));
    }


    @RequestMapping(value = "/list/{memberId}/upload_photo", method = RequestMethod.POST)
    public ResponseEntity uploadImage(
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file,
            @RequestParam("field") String field,
            @PathVariable("memberId") String memberId) {

        StringBuilder sb = new StringBuilder();
        sb.append(memberId);
        sb.append("_");

        switch (field) {
            case "photo":
                sb.append("Photo");
                break;
            case "id1":
                sb.append("IdFront");
                break;
            case "id2":
                sb.append("IdBack");
                break;
            case "agree":
                sb.append("ParentAgreeForm");
                break;
            case "data":
                sb.append("PersonalForm");
                break;
            default:
                sb.append("Unknown");
        }

        String fileExt = Files.getFileExtension(file.getOriginalFilename());

        sb.append(".");
        sb.append(fileExt);

        try {
            fileStorageService.saveImage(file, sb.toString(), memberId);

            Members002ImagesObject images = ctMemberInfoService.getPhotosByMemberId(memberId);

            String errMsg = "";

            switch (field) {
                case "photo":
                    errMsg = images.getPhoto();
                    break;
                case "id1":
                    errMsg = images.getId1();
                    break;
                case "id2":
                    errMsg = images.getId2();
                    break;
                case "agree":
                    errMsg = images.getAgree();
                    break;
                case "data":
                    errMsg = images.getData();
                    break;
            }

            return ResponseEntity.ok(new BaseResponse(200, errMsg));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());

            return ResponseEntity.ok(new BaseResponse(400, "failed"));
        }
    }

    @RequestMapping(value = "/list/{memberId}/remove_photo", method = RequestMethod.POST)
    public ResponseEntity removeImage(
            HttpServletRequest request,
            @RequestParam("field") String field,
            @PathVariable("memberId") String memberId) {

        Members002ImagesObject images = ctMemberInfoService.getPhotosByMemberId(memberId);

        String filename = "";

        switch (field) {
            case "photo":
                filename = images.getPhoto();
                break;
            case "id1":
                filename = images.getId1();
                break;
            case "id2":
                filename = images.getId2();
                break;
            case "agree":
                filename = images.getAgree();
                break;
            case "data":
                filename = images.getData();
                break;
        }

        try {

            if (!filename.equals("")) {
                fileStorageService.removeImage(filename, memberId);
            }

            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());

            return ResponseEntity.ok(new BaseResponse(400, "failed"));
        }
    }

    @RequestMapping(value = "/search_by_name", method = RequestMethod.GET)
    public ResponseEntity search_by_name(@RequestParam("name") String name,
                                         @RequestParam(value = "unitId", required = false) String unitId) {

        return ResponseEntity.ok(new ListResponse<>(200, "success", ctMemberInfoService.searchName(name, unitId)));
    }
}
