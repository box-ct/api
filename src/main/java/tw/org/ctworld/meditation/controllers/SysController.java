package tw.org.ctworld.meditation.controllers;

import com.google.gson.Gson;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.Member.Members002ImagesObject;
import tw.org.ctworld.meditation.beans.Sys.*;
import tw.org.ctworld.meditation.beans.Unit.Unit003_2Response;
import tw.org.ctworld.meditation.beans.Unit.Unit003Response;
import tw.org.ctworld.meditation.beans.Unit.Unit001Response;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.libs.RedundantException;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.services.*;
import tw.org.ctworld.meditation.views.DocumentHorizontallyWordView;
import tw.org.ctworld.meditation.views.DocumentVerticallyWordView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Controller
@RequestMapping(path = "/api/sys")
public class SysController {

    private static final Logger logger = LoggerFactory.getLogger(SysController.class);

    @Autowired
    private UnitInfoService unitInfoService;

    @Autowired
    private AnnounceInfoService announceInfoService;

    @Autowired
    private AnnounceIsReadRecordService announceIsReadRecordService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserLoginLogService userLoginLogService;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ExcelProcessService excelProcessService;

    @Autowired
    private ExcelWriterService excelWriterService;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;

    @Autowired
    private OldClassMonthStatsService oldClassMonthStatsService;

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private DocumentHorizontallyWordView documentHorizontallyWordView;

    @Autowired
    private DocumentVerticallyWordView documentVerticallyWordView;


    /**
     * 401 Online Users
     */
    //取得在線使用者列表
    @RequestMapping(path = "/online_users", method = RequestMethod.GET)
    public ResponseEntity online_users(HttpServletRequest request) {
        List<UserLoginLog> logs = userLoginLogService.getCurrentlyLogin();

        return ResponseEntity.ok(new OnlineUserResponse(200, "success", logs));
    }

    /**
     * 402 Messages
     */
    // 取得未讀公告訊息 + 已讀確認
    @RequestMapping(path = "/unread_message_badge")
    public ResponseEntity unread_message_badge(HttpServletRequest request, @RequestBody(required = false) ReadMessagesRequest readIds) {

        String userId = (String) request.getSession().getAttribute(UserSession.Keys.UserId.getValue());

        switch (request.getMethod()) {

            case "GET": // 取得未讀公告訊息

                List<AnnounceInfo> result = announceInfoService.getAnnouncements(userId);

                return ResponseEntity.ok(new MessagesResponse(200, "success", result));

            case "POST": // 已讀確認
                if (readIds != null) {
                    announceIsReadRecordService.add(request.getSession(), readIds.getMessageIds());
                }

                return ResponseEntity.ok(new BaseResponse(200, "success"));
        }

        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(path = "/messages")
    public ResponseEntity messages(HttpServletRequest request,
                                   @RequestBody(required = false) MessageRequest message) {

        switch (request.getMethod()) {

            case "GET":
                return  ResponseEntity.ok(new MessagesResponse(200, "success", announceInfoService.query()));
            case "POST": // 新增公告
                AnnounceInfo info = new AnnounceInfo();
                info.setAnnounceSubject(message.getAnnounceSubject());
                info.setAnnounceContent(message.getAnnounceContent());
                info.setEnabled(message.isEnabled());

                announceInfoService.create(request.getSession(), info);

                return ResponseEntity.ok(new BaseResponse(200, "success"));
        }

        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(path = "/messages/{messageId}")
    public ResponseEntity messages(HttpServletRequest request,
                                   @PathVariable(value = "messageId") String messageId,
                                   @RequestBody(required = false) MessageRequest message) {

        if (messageId != null) {
            AnnounceInfo info = announceInfoService.getInfoByMessageId(messageId);

            if (info != null) {
                switch (request.getMethod()) {
                    case "PUT":  // 編輯公告
                        if (message != null) {
                            info.setAnnounceSubject(message.getAnnounceSubject());
                            info.setAnnounceContent(message.getAnnounceContent());
                            info.setEnabled(message.isEnabled());

                            announceInfoService.update(request.getSession(), info);
                        }
                        break;
                    case "DELETE":  //刪除公告
                        announceInfoService.delete(info.getId());
                        break;
                }

                return ResponseEntity.ok(new BaseResponse(200, "success"));
            }

            return ResponseEntity.status(404).body(new BaseResponse(404, "not found"));
        }

        return ResponseEntity.badRequest().build();
    }

    /**
     * 403 Users
     */
    // users
    @RequestMapping(path = "/users")
    public ResponseEntity users(HttpServletRequest request,
                                @RequestBody(required = false) UserInfo newUser) {

        switch (request.getMethod()) {
            case "GET": // 取得使用者列表
                List<UserInfo> result = userInfoService.getAllUserInfo(request.getSession());
                return ResponseEntity.ok(new ClassLoginUsersResponse(200, "success", result));
            case "POST": // 新增使用者
                if (newUser != null) {
                    if (userInfoService.createUser(request.getSession(), newUser)) {
                        return ResponseEntity.ok(new BaseResponse(200, "success"));
                    } else {
                        return ResponseEntity.status(409).body(new BaseResponse(409, "exists"));
                    }
                }
        }

        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(path = "/users/{userId}")
    public ResponseEntity users(HttpServletRequest request,
                                @PathVariable(value = "userId") String userId,
                                @RequestBody(required = false) UserInfo user) {

        if (userId != null) {
            switch (request.getMethod()) {

                case "PUT":  // 修改使用者資訊
                    if (user != null && userId != null) {
                        user.setId(Long.parseLong(userId));
                        if (userInfoService.updateUser(request.getSession(), user)) {
                            return ResponseEntity.ok(new BaseResponse(200, "success"));
                        } else {
                            return ResponseEntity.status(404).body(new BaseResponse(404, "not exists"));
                        }
                    } else {
                        return ResponseEntity.badRequest().build();
                    }
                case "DELETE":  //刪除使用者資訊
                    UserInfo iUser = new UserInfo();
                    iUser.setId(Long.parseLong(userId));

                    if (userInfoService.deleteUser(iUser)) {
                        return ResponseEntity.ok(new BaseResponse(200, "success"));
                    } else {
                        return ResponseEntity.status(404).body(new BaseResponse(404, "not exists"));
                    }
            }
        }

        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(path = "/users/members", method = RequestMethod.GET)
    public ResponseEntity users_members(HttpServletRequest request, @RequestParam("aliasName") String aliasName) {

        List<SearchMember> result = new ArrayList<>();

        if (aliasName != null && !aliasName.isEmpty()) {
            result = ctMemberInfoService.searchAliasName(aliasName);
        }

        return ResponseEntity.ok(
                new SearchMemberResponse(200, "success", result));
    }


    /**
     * 404 Unit
     */
    // 取得單位/區域列表
    @RequestMapping(path = "/unit_district", method = RequestMethod.GET)
    public ResponseEntity unit_district(HttpServletRequest request, Model model) {

        return ResponseEntity.ok(new Unit001Response(200, "success", unitInfoService.getUnitDistrict()));
    }

    // 取得單位/分類列表
    @RequestMapping(path = "/unit_group", method = RequestMethod.GET)
    public ResponseEntity unit_group(HttpServletRequest request, Model model) {

        return ResponseEntity.ok(new Unit001Response(200, "success", unitInfoService.getUnitGroupName()));
    }

    @RequestMapping(path = "/units")
    public ResponseEntity units(HttpServletRequest request,
                                @RequestBody(required = false) ClassUnitRequest unit) {

        logger.debug("api/sys/units " + request.getMethod());

        switch (request.getMethod()) {
            case "GET": // 取得單位列表(登入及權限管理都使用這支API)
                return ResponseEntity.ok(
                        new Unit003Response(200, "success", unitInfoService.getLoginUnitList()));
            case "POST": // 新增單位
                if (unit != null) {
                    try {
                        UnitInfo newUnit = new UnitInfo(unit);
                        unitInfoService.createUnitInfo(request.getSession(), newUnit);
                        return ResponseEntity.ok(new BaseResponse(200, "success"));
                    } catch (RedundantException e) {
                        return ResponseEntity.status(409).body(new BaseResponse(409, "exists"));
                    } catch (Exception e) {
                        return ResponseEntity.status(408).body(new BaseResponse(408, e.getMessage()));
                    }
                }
        }

        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(path = "/units/{unitId}")
    public ResponseEntity units(HttpServletRequest request,
                                @PathVariable(value = "unitId") String unitId,
                                @RequestBody(required = false) ClassUnitRequest unit) {

        if (unitId != null) {
            switch (request.getMethod()) {
                case "PUT": // 編輯單位
                    try {
                        UnitInfo newUnit = new UnitInfo(unit);
                        newUnit.setUnitId(unitId);
                        unitInfoService.updateUnitInfo(request.getSession(), newUnit);
                        return ResponseEntity.ok(new BaseResponse(200, "success"));
                    } catch (NotFoundException e) {
                        String json = new Gson().toJson(new BaseResponse(404, "not exists"));
                        return ResponseEntity.status(404).contentType(MediaType.APPLICATION_JSON_UTF8).body(json);
                    }

                case "DELETE": // 刪除單位
                    UnitInfo info = new UnitInfo();
                    info.setUnitId(unitId);
                    if (unitInfoService.deleteUnit(info)) {
                        return ResponseEntity.ok(new BaseResponse(200, "success"));
                    } else {
                        return ResponseEntity.status(404).body(new BaseResponse(404, "not exists"));
                    }
            }
        }

        return ResponseEntity.badRequest().build();
    }

    // units structure
    @RequestMapping(path = "/units2", method = RequestMethod.GET)
    public ResponseEntity units2(HttpServletRequest request, Model model) {
        return ResponseEntity.ok(new Unit003_2Response(200, "success", unitInfoService.getLoginUnits().getSubUnit()));
    }


    /**
     * 405 Data Import
     */
    // import data from excel
    @RequestMapping(path = "/data_import/import", method = RequestMethod.POST)
    public ResponseEntity importData(HttpServletRequest request,
                                     @RequestParam("file") MultipartFile file,
                                     @RequestParam("classId") String classId) {

        try {
            // process file
            ImportResult result = excelProcessService.processClassDataImport(request.getSession(), classId, file);
            ClassRecordImportResponse response = new ClassRecordImportResponse(200, "success", result.getLogs());

            // write excels
            if (result.getEnrolls().size() > 0) {
                String filename = String.valueOf(Calendar.getInstance().getTime().getTime());
                logger.debug("has new members, filename " + filename);

                response.setEnrollFile(filename + "E.xls");
                response.setMemberFile(filename + "M.xls");

                excelWriterService.buildImportDataExcel(result.getEnrolls(), response.getEnrollFile());
                excelWriterService.buildNewMemberExcel(result.getEnrolls(), response.getMemberFile());

                response.setEnrollFile("/temp/" + response.getEnrollFile());
                response.setMemberFile("/temp/" + response.getMemberFile());

                ClassInfo classInfo = result.getClassInfo();
                String tempName = classInfo.getUnitName() + "_禪修紀錄匯入_" + classInfo.getClassPeriodNum() + "期" + classInfo.getClassName() + "_";
                response.setEnrollFilename(tempName + "待新增學員後匯入禪修紀錄.xls");
                response.setMemberFilename(tempName + "新增學員.xls");
            }

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());

            String json = new Gson().toJson(new BaseResponse(401, "file process error"));
            return ResponseEntity.status(400).contentType(MediaType.APPLICATION_JSON_UTF8).body(json);
        }
    }

    // download sample excel
    @RequestMapping(path = "/data_import/empty_file", method = RequestMethod.GET)
    public ResponseEntity emptyFile(HttpServletRequest request) {

        try {
            // Try to determine file's content type
            String contentType = null;

            Resource resource = fileStorageService.loadFileAsResource("ClassImportRecords.xlsx");
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());

            // Fallback to the default content type if type could not be determined
            if(contentType == null) {
                contentType = "application/octet-stream";
            }

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);

        } catch (Exception e) {
            logger.info("Could not determine file type.");

            String json = new Gson().toJson(new BaseResponse(404, "not found"));
            return ResponseEntity.status(404).contentType(MediaType.APPLICATION_JSON_UTF8).body(json);
        }
    }

    // check class
    @RequestMapping(path = "/data_import/classes", method = RequestMethod.GET)
    public ResponseEntity classes(HttpServletRequest request,
                                  @RequestParam("unitId") String unitId,
                                  @RequestParam("classDayOrNight") String classDayOrNight,
                                  @RequestParam("classTypeName") String classTypeName,
                                  @RequestParam("classPeriodNum") String classPeriodNum,
                                  @RequestParam(value = "className" , required = false) String className) {

        try {

            List<String> results = classInfoService.findClasses(unitId, classDayOrNight, classTypeName, classPeriodNum, className);

            if (className != null && results.size() > 0) {
                String firstResult = results.get(0);
                results.clear();
                results.add(firstResult);
            }

            return ResponseEntity.ok(new SearchClassResponse(200, "success", results));

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(404).body(new BaseResponse(404, "not found"));
        }
    }

    /**
     * serving empty image
     */
    @RequestMapping(path = "/image/", method = RequestMethod.GET)
    public ResponseEntity emptyImage(HttpServletRequest request, HttpServletResponse response) {

        Resource resource = new ClassPathResource("empty.png");

        response.setContentType(MediaType.IMAGE_JPEG_VALUE);

        try {
            StreamUtils.copy(resource.getInputStream(), response.getOutputStream());
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        return null;
    }

    /**
     * serving images
     */
    @RequestMapping(path = "/image/**", method = RequestMethod.GET)
    public ResponseEntity image(HttpServletRequest request, HttpServletResponse response) {

        Resource resource = null;

        try {
            String requestURL = request.getRequestURL().toString();

            String imageName = requestURL.split("/image/")[1];

            resource = fileStorageService.loadFile(0, imageName);
            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
            StreamUtils.copy(resource.getInputStream(), response.getOutputStream());
            return null;
        } catch (Exception e) {

            resource = new ClassPathResource("empty.png");
            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
            try {
                StreamUtils.copy(resource.getInputStream(), response.getOutputStream());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return null;
        }
    }

    /**
     * testing
     */
    @RequestMapping(path = "/test/{marginTop}/{marginLeft}/{type}", method = RequestMethod.GET)
    public ResponseEntity test(HttpServletRequest request, HttpServletResponse response,
                               @PathVariable(value="marginTop") int marginTop,
                               @PathVariable(value="marginLeft") int marginLeft,
                               @PathVariable(value = "type") int type) {
        logger.debug(marginTop + "/" + marginLeft);

//        Date date = Calendar.getInstance().getTime();
//
//        logger.debug(date.toString());
//
//        TimeZone tz = Calendar.getInstance().getTimeZone();
//
//        logger.debug(tz.getDisplayName() + "/" + tz.getRawOffset() + tz.getID());

//        ZonedDateTime zonedDateTime = LocalDateTime.parse( "2018-01-23T01:23:45" )
//                .atZone( ZoneId.of( "Africa/Tunis" ) );
//
//        logger.debug(zonedDateTime.toLocalDate().toString());

//        SimpleDateFormat formatter8 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        formatter8.setTimeZone(TimeZone.getTimeZone("GMT+8"));
//
//        SimpleDateFormat formatter7= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        formatter7.setTimeZone(TimeZone.getTimeZone("GMT+9"));
//
//        try {
//            Date target = formatter8.parse("2018-01-01 14:30:15");
//            logger.debug(target.toString());
//
//            Date target2 = formatter7.parse("2018-01-01 14:30:15");
//            logger.debug(target2.toString());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

//        logger.debug("lastYearMonth: " + oldClassMonthStatsService.getOldClassStatsYearMonth());
//        oldClassMonthStatsService.avg_by_range("2017/01", "2017/10");

        try {

            String fileName = null;

            switch (type) {
                case 1:
                    // 中文-結業證書
                    fileName = "中文-結業證書.doc";
                    fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                    response.setContentType("application/msword; charset=UTF-8");
                    response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

                    documentHorizontallyWordView.generateDocument(marginTop, marginLeft, 1).write(response.getOutputStream());
                    break;
                case 2:
                    // 中文-全勤證書
                    fileName = "中文-全勤證書.doc";
                    fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                    response.setContentType("application/msword; charset=UTF-8");
                    response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

                    documentHorizontallyWordView.generateDocument(marginTop, marginLeft, 2).write(response.getOutputStream());
                    break;
                case 3:
                    // 英文-結業證書
                    fileName = "英文-結業證書.doc";
                    fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                    response.setContentType("application/msword; charset=UTF-8");
                    response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

                    documentVerticallyWordView.generateDocument(marginTop, marginLeft, 3).write(response.getOutputStream());
                    break;
                case 4:
                    // 英文-全勤證書
                    fileName = "英文-全勤證書.doc";
                    fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                    response.setContentType("application/msword; charset=UTF-8");
                    response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

                    documentVerticallyWordView.generateDocument(marginTop, marginLeft, 4).write(response.getOutputStream());
                    break;
                case 5:
                    // 中英文-結業證書
                    fileName = "中英文-結業證書.doc";
                    fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                    response.setContentType("application/msword; charset=UTF-8");
                    response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

                    documentVerticallyWordView.generateDocument(marginTop, marginLeft, 5).write(response.getOutputStream());
                    break;
                case 6:
                    // 中英文-全勤證書
                    fileName = "中英文-全勤證書.doc";
                    fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                    response.setContentType("application/msword; charset=UTF-8");
                    response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

                    documentVerticallyWordView.generateDocument(marginTop, marginLeft, 6).write(response.getOutputStream());
                    break;
                case 7:
                    // 日文-結業證書
                    fileName = "日文-結業證書.doc";
                    fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                    response.setContentType("application/msword; charset=UTF-8");
                    response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

                    documentVerticallyWordView.generateDocument(marginTop, marginLeft, 7).write(response.getOutputStream());
                    break;
                case 8:
                    // 日文-全勤證書
                    fileName = "日文-全勤證書.doc";
                    fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                    response.setContentType("application/msword; charset=UTF-8");
                    response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

                    documentVerticallyWordView.generateDocument(marginTop, marginLeft, 8).write(response.getOutputStream());
                    break;
            }

            return null;
        } catch ( Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new BaseResponse(400, "error"));
        }

//        try {
//            String fileName = "Document(H).doc";
//            fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
//            response.setContentType("application/msword; charset=UTF-8");
//            response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);
//
//            documentHorizontally.generateDocument(marginTop, marginLeft).write(response.getOutputStream());
//
//            return null;
//        } catch ( Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.ok(new BaseResponse(400, "error"));
//        }
    }

    /**
     * download images by memberId
     */
    @RequestMapping(value="/zipImages", produces="application/zip")
    public void zipImages(HttpServletResponse response,
                         @RequestParam("memberId") String memberId) throws IOException {
        logger.debug("memberId " + memberId);

        Members002ImagesObject images = ctMemberInfoService.getPhotosByMemberId(memberId);

        //setting headers
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-Disposition", "attachment; filename=\"" + memberId + "_images.zip\"");

        ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());

        // create a list to add files to be zipped
        ArrayList<File> files = new ArrayList<>();

        if (!images.getId1().isEmpty())
            files.add(fileStorageService.loadFile(images.getId1()));

        if (!images.getId2().isEmpty())
            files.add(fileStorageService.loadFile(images.getId2()));

        if (!images.getAgree().isEmpty())
            files.add(fileStorageService.loadFile(images.getAgree()));

        if (!images.getData().isEmpty())
            files.add(fileStorageService.loadFile(images.getData()));

        // package files
        for (File file : files) {
            //new zip entry and copying inputstream with file to zipOutputStream, after all closing streams
            zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
            FileInputStream fileInputStream = new FileInputStream(file);

            IOUtils.copy(fileInputStream, zipOutputStream);

            fileInputStream.close();
            zipOutputStream.closeEntry();
        }

        zipOutputStream.close();
    }

    /**
     * member images transfer
     */
    @RequestMapping(value="/images_transfer", method = RequestMethod.GET)
    public ResponseEntity test() {

        return ResponseEntity.ok(ctMemberInfoService.imagesTransfer2());
    }
}
