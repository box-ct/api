package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain004Request;
import tw.org.ctworld.meditation.beans.CtEventMaintain.CtEventMaintain004Response;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.*;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.models.EventMain;
import tw.org.ctworld.meditation.services.CtEventsMaintainService;
import tw.org.ctworld.meditation.services.UnitEventsMaintainService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/unit_events")
public class UnitEventsMaintainController {

    private static final Logger logger = LoggerFactory.getLogger(UnitEventsMaintainController.class);

    @Autowired
    private UnitEventsMaintainService unitEventsMaintainService;
    @Autowired
    private CtEventsMaintainService ctEventsMaintainService;


    // 取得 event_category_def 列表
    @RequestMapping(value = "/event_category_defs", method = RequestMethod.GET)
    public ResponseEntity unitEventsMaintain001(HttpServletRequest request) {

        logger.debug("api/unit_events/event_category_defs(GET)");

        return ResponseEntity.ok(unitEventsMaintainService.getCategoryList(request.getSession()));
    }

    // 取得精舍 event_main
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity unitEventsMaintain002(HttpServletRequest request) {

        logger.debug("api/unit_events/list(GET)");

        return ResponseEntity.ok(unitEventsMaintainService.getEventMainList(request.getSession()));
    }

    // 新增/複製活動 (如果copyId有值，為複製模式)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity unitEventsMaintain003(HttpServletRequest request,
                                                @RequestBody CtEventMaintain004Request ctEventMaintain004Request) {

        logger.debug("api/unit_events/list(POST)");

        try {
            return ResponseEntity.ok(ctEventsMaintainService
                    .addMainEvent(request.getSession(), ctEventMaintain004Request, "U"));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new CtEventMaintain004Response(400, "找不到event main id : " + ctEventMaintain004Request.getCopyId(), null));
        }
    }

    // 取得單取活動資料
    @RequestMapping(value = "/list/{eventId}", method = RequestMethod.GET)
    public ResponseEntity unitEventsMaintain004(HttpServletRequest request, @PathVariable("eventId") String eventId) {

        logger.debug("api/unit_events/list/" + eventId + "(GET)");

        return ResponseEntity.ok(unitEventsMaintainService.getMainEventByEventId(request.getSession(), eventId));
    }

    // 修改單筆活動資料
    @RequestMapping(value = "/list/{eventId}", method = RequestMethod.PUT)
    public ResponseEntity unitEventsMaintain005(HttpServletRequest request,
                                                @PathVariable("eventId") String eventId,
                                                @RequestBody UnitEventsMaintain005Request unitEventsMaintain005Request) {

        logger.debug("api/unit_events/list/" + eventId + "(PUT)");

        return ResponseEntity.ok(ctEventsMaintainService.editMainEventByEventId(
                request.getSession(), eventId, unitEventsMaintain005Request, "U"));
    }

    // 刪除單筆活動資料
    @RequestMapping(value = "/list/{eventId}", method = RequestMethod.DELETE)
    public ResponseEntity unitEventsMaintain006(HttpServletRequest request,
                                                @PathVariable("eventId") String eventId) {

        logger.debug("api/unit_events/list/" + eventId + "(DELETE)");

        return ResponseEntity.ok(ctEventsMaintainService.deleteMainEventByEventId(request.getSession(), eventId));
    }

    // 取得活動pgm_oca列表
    @RequestMapping(value = "/list/{eventId}/event_pgm_ocas", method = RequestMethod.GET)
    public ResponseEntity unitEventsMaintain007(@PathVariable("eventId") String eventId) {

        logger.debug("api/unit_events/list/" + eventId + "/event_pgm_ocas(GET)");

        return ResponseEntity.ok(unitEventsMaintainService.getEventPgmOcaList(eventId));
    }

    // 單筆新增pgm_oca
    @RequestMapping(value = "/list/{eventId}/event_pgm_ocas", method = RequestMethod.POST)
    public ResponseEntity unitEventsMaintain008(HttpServletRequest request,@PathVariable("eventId") String eventId,
                                                @RequestBody UnitEventsMaintain008Request unitEventsMaintain008Request) {

        logger.debug("api/unit_events/list/" + eventId + "/event_pgm_ocas(POST)");

        return ResponseEntity.ok(unitEventsMaintainService
                .addEventPgmOca(request.getSession(), eventId, unitEventsMaintain008Request));
    }

    // 修改單筆event_pgm_oca
    @RequestMapping(value = "/list/{eventId}/event_pgm_ocas/{pgmUniqueId}", method = RequestMethod.PUT)
    public ResponseEntity unitEventsMaintain009(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @PathVariable("pgmUniqueId") String pgmUniqueId,
                                             @RequestBody CtEventMaintain009Request ctEventMaintain009Request) {

        logger.debug("api/unit_events/list/" + eventId + "/event_pgm_ocas/" + pgmUniqueId + "(POST)");

        return ResponseEntity.ok(ctEventsMaintainService.editEventPgmOca(
                request.getSession(), eventId, pgmUniqueId, ctEventMaintain009Request));
    }

    // 刪除單筆event_pgm_oca
    @RequestMapping(value = "/list/{eventId}/event_pgm_ocas/{pgmUniqueId}", method = RequestMethod.DELETE)
    public ResponseEntity unitEventsMaintain010(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @PathVariable("pgmUniqueId") String pgmUniqueId) {

        logger.debug("api/unit_events/list/" + eventId + "/event_pgm_ocas/" + pgmUniqueId + "(DELETE)");

        return ResponseEntity.ok(ctEventsMaintainService.deleteEventPgmOca(request.getSession(), eventId, pgmUniqueId));
    }

    // 取得活動特殊輸入項目列表
    @RequestMapping(value = "/list/{eventId}/special_inputs", method = RequestMethod.GET)
    public ResponseEntity unitEventsMaintain011(HttpServletRequest request, @PathVariable("eventId") String eventId) {

        logger.debug("api/unit_events/list/" + eventId + "/special_inputs(GET)");

        return ResponseEntity.ok(ctEventsMaintainService.getSpecialInputs(request.getSession(), eventId));
    }

    // 新增特殊輸入項目
    @RequestMapping(value = "/list/{eventId}/special_inputs", method = RequestMethod.POST)
    public ResponseEntity unitEventsMaintain012(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @RequestBody UnitEventsMaintain012Request unitEventsMaintain012Request) {

        logger.debug("api/unit_events/list/" + eventId + "/special_inputs(POST)");

        return ResponseEntity.ok(
                ctEventsMaintainService.addSpecialInput(request.getSession(), eventId, unitEventsMaintain012Request));
    }

    // 修改單筆特殊輸入項目
    @RequestMapping(value = "/list/{eventId}/special_inputs/{sInputId}", method = RequestMethod.PUT)
    public ResponseEntity unitEventsMaintain013(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @PathVariable("sInputId") String sInputId,
                                             @RequestBody UnitEventsMaintain012Request unitEventsMaintain012Request) {

        logger.debug("api/unit_events/list/" + eventId + "/special_inputs/" + sInputId + "(PUT)");

        return ResponseEntity.ok(ctEventsMaintainService.editSpecialInput(
                request.getSession(), eventId, sInputId, unitEventsMaintain012Request));
    }

    // 刪除單筆特殊輸入項目
    @RequestMapping(value = "/list/{eventId}/special_inputs/{sInputId}", method = RequestMethod.DELETE)
    public ResponseEntity unitEventsMaintain014(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @PathVariable("sInputId") String sInputId) {

        logger.debug("api/unit_events/list/" + eventId + "/special_inputs/" + sInputId + "(DELETE)");

        return ResponseEntity.ok(ctEventsMaintainService.deleteSpecialInput(request.getSession(), eventId, sInputId));
    }
}
