package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.ClassBarCode.ClassBarCode001Response;
import tw.org.ctworld.meditation.services.ClassAttendMarkService;
import tw.org.ctworld.meditation.services.ClassAttendRecordService;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

@Controller
@RequestMapping(path = "/api/class_barcode")
public class ClassBarCodeController {

    private static final Logger logger = LoggerFactory.getLogger(ClassBarCodeController.class);

    @Autowired
    private ClassAttendRecordService classAttendRecordService;


    // 取得指定日期的報到記錄 OK
    @RequestMapping(value = "/{classId}/attend_record", method = RequestMethod.GET)
    public ResponseEntity classBarCode001(HttpServletRequest request,
                                          @PathVariable("classId") String classId,
                                          @RequestParam("date") String date) {

        return ResponseEntity.ok(new ClassBarCode001Response(200, "success",
                classAttendRecordService.getClassAttendRecordList(request.getSession(), classId, date)));
    }

    // 學員報到 OK
    @RequestMapping(value = "/{classId}/attend_record/{attendRecordId}", method = RequestMethod.POST)
    public ResponseEntity classBarCode002(HttpServletRequest request,
                                     @PathVariable("classId") String classId,
                                     @PathVariable("attendRecordId") String attendRecordId) {

        return ResponseEntity.ok(classAttendRecordService.enrollment(request.getSession(), classId, attendRecordId));
    }

    // 學員取消報到 OK
    @RequestMapping(value = "/{classId}/attend_record/{attendRecordId}", method = RequestMethod.DELETE)
    public ResponseEntity classBarCode003(HttpServletRequest request,
                                     @PathVariable("classId") String classId,
                                     @PathVariable("attendRecordId") String attendRecordId) {

        return ResponseEntity.ok(classAttendRecordService.cancelEnrollment(request.getSession(), classId, attendRecordId));
    }
}
