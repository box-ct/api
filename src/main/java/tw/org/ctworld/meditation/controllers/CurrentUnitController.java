package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.beans.UnitMaster.*;
import tw.org.ctworld.meditation.models.UnitInfo;
import tw.org.ctworld.meditation.repositories.UnitInfoRepo;
import tw.org.ctworld.meditation.services.ExcelUnitMasterService;
import tw.org.ctworld.meditation.services.UnitMasterService;
import tw.org.ctworld.meditation.views.MasterTemplateExcelView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/current_unit")
public class CurrentUnitController {

    private static final Logger logger = LoggerFactory.getLogger(CurrentUnitController.class);

    @Autowired
    private UnitMasterService unitMasterService;

    @Autowired
    private ExcelUnitMasterService excelUnitMasterService;

    @Autowired
    private UnitInfoRepo unitInfoRepo;

    //下載匯入格式檔
    @RequestMapping(value = "/download_template", method = RequestMethod.GET)
    public ModelAndView getCurrent_Unit_Template(HttpServletRequest request){
        return new ModelAndView(new MasterTemplateExcelView(request.getSession()));
    }

    //取得單位資訊
    @RequestMapping(value = "/data", method = RequestMethod.GET)
    public ResponseEntity getCurrent_Unit_Data(HttpServletRequest request){
        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        UnitInfo cf= unitInfoRepo.GetClassUnitInfoByUnitId(unitId);
        if (cf!=null)
            return ResponseEntity.ok( new UnitMaster002Response(200,"success",cf.getUnitName(),cf.getCertUnitName(),cf.getUnitId(),cf.getFundUnitId(),cf.getCertUnitEngName(),
                    cf.getUnitPhoneNum(),cf.getUnitFaxNum(),cf.getTimezone(),cf.getUnitPhoneNumCode(),cf.getUnitMobileNum(),cf.getUnitEmail(),cf.getUnitMailingAddress(),
                    cf.getStateName(),cf.getAbbotId(),cf.getAbbotName(),cf.getAbbotEngName(),cf.getIsOverseasUnit(),cf.getDistrict(),cf.getGroupName(),unitMasterService.getUnitMasterByUnitid(unitId)));
        else
            return ResponseEntity.ok(new BaseResponse(400, "data not found"));
    }

    //修改單位資訊及單位法師
    @RequestMapping(value = "/data", method = RequestMethod.PUT)
    public ResponseEntity putCurrent_Unit_Data(HttpServletRequest request,@RequestBody UnitMaster002Data data){
        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        //log data to json string
//        Object o=Object.class.cast(data);   unitMasterService.LogObjJsonstr(o);
        unitMasterService.UpdateUnitMasterInfos(request.getSession(), unitId,data);
        return ResponseEntity.ok(new BaseResponse(200, "success"));
    }

    //搜尋法師(僅取一筆)
    @RequestMapping(value = "/search_master", method = RequestMethod.GET)
    public ResponseEntity getCurrent_Unit_SearchMaster(HttpServletRequest request,@RequestParam("aliasName") String aliasName){
        return ResponseEntity.ok( new UnitMaster003Response(200,"success",unitMasterService.getCtMasterByAliasname(aliasName)));
    }

    //匯入檔案比對法師名單
    @RequestMapping(value = "/mapping_master", method = RequestMethod.POST)
    public ResponseEntity getCurrent_Unit_MapMaster(HttpServletRequest request, MultipartFile file){
        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        MasterImportResultObj result=excelUnitMasterService.processUnitMasterImport(unitId,file);
        return ResponseEntity.ok( new MasterImportResult(200,"success",result.getMasterInfoList(),result.getFailed()));
    }
}
