package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.DataSearch.*;
import tw.org.ctworld.meditation.beans.Member.MembersObject;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.services.EventEnrollMainService;
import tw.org.ctworld.meditation.services.CtEventsMaintainService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/api/data_search")
public class DataSearchController {

    private static final Logger logger = LoggerFactory.getLogger(DataSearchController.class);

    @Autowired
    ClassEnrollFormService classEnrollFormService;

    @Autowired
    CtEventsMaintainService ctEventsMaintainService;

    @Autowired
    EventEnrollMainService eventEnrollMainService;

    // 取得證件檢核未通過的學員列表
    @RequestMapping(path = "/not_valid_members", method = RequestMethod.GET)
    public ResponseEntity not_valid_members(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<MembersObject> result = classEnrollFormService.getMembersByUnitIdNewestClassPeriod(unitId);

        return ResponseEntity.ok(new SpanMembersResponse(0, "success", result));
    }

    // 取得當期人數統計(班別)
    @RequestMapping(path = "/now_number_by_class", method = RequestMethod.GET)
    public ResponseEntity now_number_by_class(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<DataSearchClassSubTotalResponse.ClassSubTotal> result = classEnrollFormService.getClassSubTotal(unitId);

        return ResponseEntity.ok(new DataSearchClassSubTotalResponse(200,"success", result));
    }

    // 取得當期人數統計(組別)
    @RequestMapping(path = "/now_number_by_group", method = RequestMethod.GET)
    public ResponseEntity now_number_by_group(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<DataSearchGroupSubTotalResponse.ClassSubTotal> result = classEnrollFormService.getGroupSubTotal(unitId);

        return ResponseEntity.ok(new DataSearchGroupSubTotalResponse(200,"success", result));
    }

    // 取得歷史人數統計
    @RequestMapping(path = "/history_number", method = RequestMethod.GET)
    public ResponseEntity history_number(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<DataSearchHistoryNumber.HistoryPeriod> result = classEnrollFormService.getCountByPeriod(unitId);

        return ResponseEntity.ok(new DataSearchHistoryNumber(200,"success", result));
    }

    // 取得學員長列表
    @RequestMapping(path = "/now_leader", method = RequestMethod.GET)
    public ResponseEntity now_leader(HttpServletRequest request,
                                        @RequestParam(value = "class_status") String classStatus,
                                        @RequestParam(value = "class_period_num", required = false) String classPeriodNum,
                                        @RequestParam(value = "class_name", required = false) String className,
                                        @RequestParam(value = "class_group_id", required = false) String classGroupId) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        logger.debug(classStatus + "/" + classPeriodNum + "/" + className + "/" + classGroupId + "/" + unitId);

        List<MembersObject> result = classEnrollFormService.getLeaders(unitId, classStatus, classPeriodNum, className, classGroupId);

        logger.debug("# of leaders: " + result.size());

        return ResponseEntity.ok(new SpanMembersResponse(200,"success", result));
    }

    // 取得跨班學員/長數量
    @RequestMapping(path = "/span_numbers", method = RequestMethod.GET)
    public ResponseEntity span_numbers(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<DataSearchSpanNumberResponse.SpanNumbers> result = classEnrollFormService.getSpanMemberCounts(unitId);

        return ResponseEntity.ok(new DataSearchSpanNumberResponse(200,"success", result));
    }

    // 取得學員活動記錄表
    @RequestMapping(path = "/events", method = RequestMethod.GET)
    public ResponseEntity events(HttpServletRequest request,
                                 @RequestParam(value = "startDate", required = false) String startDate,
                                 @RequestParam(value = "endDate", required = false) String endDate) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<String> eventIds = ctEventsMaintainService.getEventsByUnitDates(startDate, endDate);

        logger.debug("event count: " + eventIds.size());

        List<DataSearchEventEnrollObject> result;

        if (eventIds.size() > 0) {
            result = eventEnrollMainService.getClassEventEnrollsByUnitEventIds(unitId, eventIds);
        } else {
            result = new ArrayList<>();
        }

        return ResponseEntity.ok(new DataSearchEventEnrollResponse(200,"success", result));
    }
}
