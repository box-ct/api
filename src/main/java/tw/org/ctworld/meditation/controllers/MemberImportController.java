package tw.org.ctworld.meditation.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.MemberImport.*;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.services.ExcelUnitMasterService;
import tw.org.ctworld.meditation.services.FileStorageService;
import tw.org.ctworld.meditation.services.MemberImportService;
import tw.org.ctworld.meditation.services.SysCodeService;
import tw.org.ctworld.meditation.views.MemberTemplateExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(path = "/api/member_import")
public class MemberImportController {
    private static final Logger logger = LoggerFactory.getLogger(CurrentUnitController.class);

    @Autowired
    private MemberImportService memberImportService;

    @Autowired
    private ExcelUnitMasterService excelUnitMasterService;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private SysCodeService sysCodeService;

    //下載匯入格式檔
    @RequestMapping(value = "/download_template", method = RequestMethod.GET)
    public ResponseEntity load_file(HttpServletRequest request,
                                    HttpServletResponse response){
        try {

            String docPath = sysCodeService.getTrainingDocsPath()+"/資料批次匯入_學員基本資料格式.xls";
//            Resource resource = fileStorageService.loadFile2Resource("doc/信眾基本資料格式檔.xls");
            Resource resource = fileStorageService.loadFile2Resource(docPath);
            response.setContentType("application/excel");
            String  filename = "資料批次匯入_學員基本資料格式.xls";
            filename=java.net.URLEncoder.encode(filename, StandardCharsets.UTF_8.name());
            response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + filename);
            StreamUtils.copy(resource.getInputStream(), response.getOutputStream());
        } catch (Exception e) {
            logger.debug("read template excel file error");
            e.printStackTrace();
        }

        return ResponseEntity.ok().build();
    }

    //取得匯出欄位清單
    @RequestMapping(value = "/fieldlist", method = RequestMethod.GET)
    public ResponseEntity getExportFieldList(HttpServletRequest request){
        String userId= (String) request.getSession().getAttribute(UserSession.Keys.UserId.getValue());
        List<String> res=new ArrayList<>();
        for (String ele:memberImportService.fieldNames){
            if (ele.equals("本山發心")){
                if (userId.substring(0,1).equals("A")||userId.substring(0,1).equals("B")){
                    res.add(ele);
                }
            }
            else {
                res.add(ele);
            }
        }
        return ResponseEntity.ok( new MemberExportFieldlistResponse(200,"success",res));
    }

    //取得當前精舍學員列表
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity getMemberImportList(HttpServletRequest request){
        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        return ResponseEntity.ok( new MemberImport001Response(200,"success",memberImportService.GetMemberListItem(unitId)));
    }

    //檢查匯入檔
    @RequestMapping(value = "/check_import", method = RequestMethod.POST)
    public ResponseEntity CheckImport(HttpServletRequest request, MultipartFile file){

        List<MemberTemplateItem> memberTemplateItems=new ArrayList<>();
        String fn= file.getOriginalFilename();
        String[] tmp=fn.split("\\.");
        String ext=tmp[tmp.length-1];
        if (ext.toLowerCase().equals("xls")){
            memberTemplateItems=excelUnitMasterService.readExcelMemberImport_XLS(file);
        }
        else {
            memberTemplateItems=excelUnitMasterService.readExcelMemberImport(file);
        }

        if (memberTemplateItems.size()==0){

            return ResponseEntity.ok(new BaseResponse(400,"error 無匯入資料"));
        }
        else {

            return ResponseEntity.ok(new MemberImportResultResponse(
                    200,"success",memberImportService.CheckMemberImport(memberTemplateItems)) );
        }
    }

    //匯出學員資料
    @RequestMapping(value = "/export", method = RequestMethod.POST)
    public ModelAndView getMemberExport(HttpServletRequest request, Model model, HttpServletResponse response,
                                        @RequestBody(required = false) MemberExportRequest body){
        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        String unitName = (String) request.getSession().getAttribute(UserSession.Keys.UnitName.getValue());
        List<MemberTemplateItem> memberTemplateItems=memberImportService.GetExportDataByMemberIds(Arrays.asList(body.getMemberIds()));
        MemberExportInfo results=new MemberExportInfo(unitId,unitName,memberTemplateItems,Arrays.asList(body.getSelectedField()));
        model.addAttribute("results", results);
        return new ModelAndView(new MemberTemplateExcelView(request.getSession()));
    }

    //匯入學員資料
    @RequestMapping(value = "/import", method = RequestMethod.POST) //, @RequestBody(required = false) MemberImportRequest body
    public ResponseEntity MemberImport(HttpServletRequest request,
                                       @RequestParam("file") MultipartFile file,
                                       @RequestParam("updateType") String updateType,
                                       @RequestParam("items") String items){
        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        String unitName = (String) request.getSession().getAttribute(UserSession.Keys.UnitName.getValue());
        List<MemberTemplateItem> memberTemplateItems=new ArrayList<>();
        String fn= file.getOriginalFilename();
        String[] tmp=fn.split("\\.");
        String ext=tmp[tmp.length-1];
        if (ext.toLowerCase().equals("xls")){
            memberTemplateItems=excelUnitMasterService.readExcelMemberImport_XLS(file);
        }
        else {
            memberTemplateItems=excelUnitMasterService.readExcelMemberImport(file);
        }
        ObjectMapper mapper = new ObjectMapper();
        List<MemberImportResult> jitems =new ArrayList<>();
        try{
            jitems=mapper.readValue(items,new TypeReference<List<MemberImportResult>>(){});
        }
        catch (Exception ex){
            logger.debug(ex.getMessage());

        }
        if (jitems.size()==0){
            return ResponseEntity.ok(new BaseResponse(400,"error 無匯入資料"));
        }
        else{
            MemberImportRequest body=new  MemberImportRequest(updateType,jitems);
            memberImportService.MemberImport(request.getSession(), unitId,unitName, body,memberTemplateItems);
            return ResponseEntity.ok(new BaseResponse(200,"success"));
        }
    }

    //
    @RequestMapping(value = "/logs", method = RequestMethod.GET)
    public ResponseEntity getImportlogs(HttpServletRequest request,
                                      @RequestParam("startDate") String startDate,
                                      @RequestParam("endDate") String endDate,
                                      @RequestParam("name") String name){
        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        return ResponseEntity.ok(new MemberImportLogResponse(200,"success",memberImportService.GetMemberImportLog(unitId,startDate,endDate,name)));
    }
}
