package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.LoginRequest;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/api/sample")
public class SampleController {

    private static final Logger logger = LoggerFactory.getLogger(SampleController.class);

    @RequestMapping(path = "/test", method = RequestMethod.POST)
    public ResponseEntity login(HttpServletRequest request) {

        logger.debug("/api/sample/test");

        return ResponseEntity.ok(new BaseResponse(0, "success"));
    }

}
