package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tw.org.ctworld.meditation.beans.BatchRegister.BatchRegister001Request;
import tw.org.ctworld.meditation.services.BatchRegisterService;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/batch_register")
public class BatchRegisterController {

    private static final Logger logger = LoggerFactory.getLogger(BatchRegisterController.class);

    @Autowired
    private BatchRegisterService batchRegisterService;


    // 批次註冊報名
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity batchRegister001(HttpServletRequest request,
                                           @RequestBody BatchRegister001Request batchRegister001Request) {

        return ResponseEntity.ok(batchRegisterService.batchRegister(request.getSession(), batchRegister001Request));
    }
}
