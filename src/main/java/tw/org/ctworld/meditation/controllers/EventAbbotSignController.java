package tw.org.ctworld.meditation.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.EventAbbotSign.*;
import tw.org.ctworld.meditation.beans.EventEnrollMember.EnrollMain;
import tw.org.ctworld.meditation.beans.ListResponse;
import tw.org.ctworld.meditation.beans.ResultResponse;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain002Object;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.services.EventEnrollMainService;
import tw.org.ctworld.meditation.services.EventMainService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(path = "/api/event_abbot_sign")
@Api(description = "18-住持簽核報名表&19-審核活動報名表(本山)")
public class EventAbbotSignController {

    private static final Logger logger = LoggerFactory.getLogger(EventAbbotSignController.class);

    @Autowired
    private EventMainService eventMainService;

    @Autowired
    private EventEnrollMainService eventEnrollMainService;


    @RequestMapping(value = "/event_mains", method = RequestMethod.GET)
    @ApiOperation(value = "取得活動列表*")
    public ResponseEntity event_mains(HttpServletRequest request, @RequestParam String type) {
        String unitId = null;

        if (type.equals("UNIT")) {
            unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        }

        List<UnitEventsMaintain002Object> items = eventMainService.getEventsByAttendUnit(unitId);

        return ResponseEntity.ok(new ListResponse<>(200, "success", items));
    }

    @RequestMapping(value = "/event_unit_infos", method = RequestMethod.GET)
    @ApiOperation(value = "取得 event_unit_info 列表*")
    public ResponseEntity event_unit_info(HttpServletRequest request, @RequestParam String eventId) {

        try {
            List<SignUnit> units = eventMainService.getUnitsByEvent(eventId);

            return ResponseEntity.ok().body(new ListResponse<>(200, "success", units));
        } catch (NotFoundException n) {
            return ResponseEntity.ok().body(new BaseResponse(400, "活動不存在"));
        }
    }

    @RequestMapping(value = "/event_unit_infos", method = RequestMethod.PUT)
    @ApiOperation(value = "批次更新報名截止日&異動截止日*")
    public ResponseEntity edit_event_unit_info(HttpServletRequest request, @RequestBody UpdateEventUnitDateRequest payload) {

        try {
            eventMainService.updateEventUnitDate(request.getSession(), payload);

            return ResponseEntity.ok().body(new BaseResponse(200, "success"));
        } catch (NotFoundException n) {
            return ResponseEntity.ok().body(new BaseResponse(400, "活動不存在"));
        }
    }

    @RequestMapping(value = "/event_unit_info", method = RequestMethod.GET)
    @ApiOperation(value = "取得單筆 event_unit_info*")
    public ResponseEntity event_unit_info_single(HttpServletRequest request, @RequestParam String eventId, @RequestParam String unitId) {

        try {
            SignUnit unit = eventMainService.getUnitByEvent(eventId, unitId);
            return ResponseEntity.ok().body(new ResultResponse<>(200, "success", unit));
        } catch (NotFoundException n) {
            return ResponseEntity.ok().body(new BaseResponse(400, n.getMessage()));
        }
    }

    @RequestMapping(value = "/master_enroll_mains", method = RequestMethod.GET)
    @ApiOperation(value = "取得法師報名表*")
    public ResponseEntity master_enroll_mains(HttpServletRequest request, @RequestParam String eventId, @RequestParam String unitId) {

        List<EnrollMain> enrollMainList = eventEnrollMainService.getEnrolledMasterByEventId("UNIT", unitId, eventId);

        return ResponseEntity.ok().body(new ListResponse<>(200, "success", enrollMainList));
    }

    @RequestMapping(value = "/member_enroll_mains", method = RequestMethod.GET)
    @ApiOperation(value = "取得學員報名表*")
    public ResponseEntity member_enroll_mains(HttpServletRequest request, @RequestParam String eventId, @RequestParam String unitId) {

        List<EnrollMain> enrollMainList = eventEnrollMainService.getEnrolledMembersByEventId(unitId, eventId);

        return ResponseEntity.ok().body(new ListResponse<>(200, "success", enrollMainList));
    }

    @RequestMapping(value = "/event_enroll_mains/is_abbot_signed", method = RequestMethod.POST)
    @ApiOperation(value = "批次設定 isAbbotSigned (學員 & 法師共用)*")
    public ResponseEntity abbot_signed(HttpServletRequest request, @RequestBody AbbotSignRequest payload) {

        eventEnrollMainService.batchSign(request.getSession(), payload);

        return ResponseEntity.ok().body(new BaseResponse(200, "success"));
    }

    @RequestMapping(value = "/event_enroll_mains/is_ct_approved", method = RequestMethod.POST)
    @ApiOperation(value = "批次設定 isCtApproved (學員 & 法師共用)*")
    public ResponseEntity abbot_approved(HttpServletRequest request, @RequestBody CtApprovedRequest payload) {

        eventEnrollMainService.batchCtApproved(request.getSession(), payload);

        return ResponseEntity.ok().body(new BaseResponse(200, "success"));
    }

    @RequestMapping(value = "/event_enroll_mains", method = RequestMethod.POST)
    @ApiOperation(value = "表格即時編輯*")
    public ResponseEntity update_enroll_note(HttpServletRequest request, @RequestBody CtApprovedNoteRequest payload) {

        try {
            eventEnrollMainService.updateCtApprovedNote(request.getSession(), payload);

            return ResponseEntity.ok().body(new BaseResponse(200, "success"));
        } catch (NotFoundException n) {
            return ResponseEntity.ok().body(new BaseResponse(400, "活動報名表不存在"));
        }
    }

    @RequestMapping(value = "/export_enroll_mains", method = RequestMethod.POST)
    @ApiOperation(value = "匯出活動報表(預留)")
    public ResponseEntity export_enroll_mains(HttpServletRequest request) {

        return ResponseEntity.status(400).body(new BaseResponse(400, "not implemented"));
    }

    @RequestMapping(value = "/import_enroll_mains", method = RequestMethod.POST)
    @ApiOperation(value = "匯入審核結果(預留)")
    public ResponseEntity import_enroll_mains(HttpServletRequest request) {

        return ResponseEntity.status(400).body(new BaseResponse(400, "not implemented"));
    }
}
