package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.ClassReport.ClassMemberRecord;
import tw.org.ctworld.meditation.beans.Member.*;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.repositories.CtMemberInfoRepo;
import tw.org.ctworld.meditation.services.CtMemberInfoService;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.services.ClassInfoService;
import tw.org.ctworld.meditation.views.StudentNameCardWordView1;
import tw.org.ctworld.meditation.views.StudentNameCardWordView2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Controller
@RequestMapping(path = "/api/members")
public class MemberController {

    private static final Logger logger = LoggerFactory.getLogger(MemberController.class);

    @Autowired
    private ClassEnrollFormService classEnrollFormService;

    @Autowired
    private CtMemberInfoRepo ctMemberInfoRepo;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    StudentNameCardWordView1 studentNameCardWordView1;

    @Autowired
    StudentNameCardWordView2 studentNameCardWordView2;


    // 取得學員資料列表 OK (2018/12/03 舊版暫時關閉)
//    @RequestMapping(method = RequestMethod.GET)
//    public ResponseEntity members001(HttpServletRequest request,
//                                     @RequestParam("classStatus") String classStatus,
//                                     @RequestParam("classPeriodNum") String classPeriodNum,
//                                     @RequestParam("className") String className,
//                                     @RequestParam("classGroupId") String classGroupId,
//                                     @RequestParam("aliasName") String aliasName) {
//
//        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
//
//        List<Members001Response.Members001Object> MembersObjectList = new ArrayList<>();
//
//        if (aliasName.equals("")) {
//            // 姓名為空值，有連動選單的條件
//            MembersObjectList = classEnrollFormService
//                    .getClassEnrollFormListByRequestParam(classStatus, classPeriodNum, className, classGroupId, unitId);
//        }else {
//            // 姓名為有值
//            if (classStatus.equals("") &&
//                    classPeriodNum.equals("") &&
//                    className.equals("") &&
//                    classGroupId.equals("") &&
//                    !aliasName.equals("")) {
//
//                // 姓名為有值，無連動選單的條件
//                MembersObjectList = classEnrollFormService.getClassEnrollFormListByRequestParam(unitId, aliasName);
//            }else {
//                // 姓名為有值，有連動選單的條件
//                MembersObjectList = classEnrollFormService
//                        .getClassEnrollFormListByRequestParam(classStatus, classPeriodNum, className, classGroupId, unitId, aliasName);
//            }
//        }
//
//        return ResponseEntity.ok(new Members001Response(200, "success", MembersObjectList));
//    }

    // 取得學員資料列表 (2018/12/03 CR新版)
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity members001(HttpServletRequest request,
                                     @RequestParam("classStatus") String classStatus,
                                     @RequestParam("classPeriodNum") String classPeriodNum,
                                     @RequestParam("className") String className) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<Members001Object> MembersObjectList = classEnrollFormService
                .getClassEnrollFormListByRequestParam(unitId, classStatus, classPeriodNum, className);

        return ResponseEntity.ok(new Members001Response(200, "success", MembersObjectList));
    }

    // 依照MemberId取得學員資料 OK
    @RequestMapping(value = "/{memberId}", method = RequestMethod.GET)
    public ResponseEntity members002(HttpServletRequest request, @PathVariable("memberId") String memberId) {

        return ResponseEntity.ok(ctMemberInfoService.getClassCtMemberInfoByMemberId(memberId, request.getSession()));
    }

    // 報名/更新資料 OK
    @RequestMapping(value = "/{memberId}/register", method = RequestMethod.POST)
    public ResponseEntity members003(HttpServletRequest request,
                                     @PathVariable("memberId") String memberId,
                                     @RequestBody Members003Request members003Request) {

        return ResponseEntity.ok(classEnrollFormService.checkClassEnrollForm(request.getSession(), memberId, members003Request));
    }

    // 輸出名牌 zh-tw or en OK
    @RequestMapping(value = "/{classEnrollFormId}/print_card", method = RequestMethod.GET)
    public ModelAndView members004(HttpServletRequest request,
                                   HttpServletResponse response,
                                   @PathVariable("classEnrollFormId") String classEnrollFormId,
                                   @RequestParam("lang") String lang) {

        List<ClassMemberRecord> results = classEnrollFormService.printCard(classEnrollFormId, lang);

        String unitName = (String) request.getSession().getAttribute(UserSession.Keys.UnitName.getValue());
        String userName = (String) request.getSession().getAttribute(UserSession.Keys.UserName.getValue());
        String className =  results.get(0).getClassName();

        switch (lang) {
            case "en":

                try {
                    String fileName =  unitName + "_" + className + "_英文名牌_" + userName + "_" + CommonUtils.GetCurrentTimeString() + ".doc";
                    fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                    response.setContentType("application/msword; charset=UTF-8");
                    response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

//                    studentNameCard1.generateDocument(results, 1).write(response.getOutputStream());
                    studentNameCardWordView2.generateDocument(results, 1, unitName).write(response.getOutputStream());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "zh-tw":

                try {
                    String fileName =  unitName + "_" + className + "_中文名牌_" + userName + "_" + CommonUtils.GetCurrentTimeString() + ".doc";
                    fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                    response.setContentType("application/msword; charset=UTF-8");
                    response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

//                    studentNameCard1.generateDocument(results, 0).write(response.getOutputStream());
                    studentNameCardWordView2.generateDocument(results, 0, unitName).write(response.getOutputStream());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }

        return null;
    }

    // 變更班/組別 OK
    @RequestMapping(value = "/{memberId}/change_class", method = RequestMethod.POST)
    public ResponseEntity members005(HttpServletRequest request,
                                     @PathVariable("memberId") String memberId,
                                     @RequestBody Members005Request members005Request) {

        return ResponseEntity.ok(new BaseResponse(200,
                classEnrollFormService.changeClass(request.getSession(), memberId, members005Request)));
    }

    // 取得學員的報名表列表 OK
    @RequestMapping(value = "/{memberId}/class_enroll_form", method = RequestMethod.GET)
    public ResponseEntity members006(HttpServletRequest request,
                                     @PathVariable("memberId") String memberId,
                                     @PathParam("classStatus") String classStatus) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new Members006Response(200,"success",
                classEnrollFormService.getMemberClassName(memberId, classStatus, unitId)));
    }

    // 取得學員的報名表資訊, 如無報名表，errCode回傳錯誤即可
    @RequestMapping(value = "/{memberId}/class_enroll_form/{classId}", method = RequestMethod.GET)
    public ResponseEntity members007(HttpServletRequest request,
                                     @PathVariable("memberId") String memberId,
                                     @PathVariable("classId") String classId) {

        return ResponseEntity.ok(classEnrollFormService.getClassEnrollForm(memberId, classId));
    }

    // 暫定 報名-取得報名連動資料combobox
    @RequestMapping(value = "/class_data", method = RequestMethod.GET)
    public ResponseEntity members007(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new Members008Response(200,"success",
                classInfoService.getClassDataList(unitId)));
    }

    // 暫定 報名-報名狀態
    @RequestMapping(value = "/class_register_status", method = RequestMethod.GET)
    public ResponseEntity members009(HttpServletRequest request,
                                     @RequestParam("memberId") String memberId,
                                     @RequestParam("classId") String classId) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new Members009Response(200,"success",
                classEnrollFormService.getClassRegisterStatus(memberId, classId, unitId).split(";")[0]));
    }

    // TEST 自動報名系統
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity members010(HttpServletRequest request) {

        return ResponseEntity.ok(classEnrollFormService.register(request.getSession()));
    }
}
