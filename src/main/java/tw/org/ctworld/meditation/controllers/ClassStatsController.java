package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord000Response;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002Response;
import tw.org.ctworld.meditation.beans.ClassStats.*;
import tw.org.ctworld.meditation.beans.StringListResponse;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.services.ClassInfoService;
import tw.org.ctworld.meditation.views.*;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

import static tw.org.ctworld.meditation.libs.CommonUtils.print2JSON;

@Controller
@RequestMapping(path = "/api/class_stats")
public class ClassStatsController {

    private static final Logger logger = LoggerFactory.getLogger(ClassStatsController.class);

    @Autowired
    private ClassEnrollFormService classEnrollFormService;

    @Autowired
    private ClassInfoService classInfoService;

    // CR3 get year list of classes
    @RequestMapping(value = "/years", method = RequestMethod.GET)
    public ResponseEntity years(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new StringListResponse(200, "success",
                classInfoService.getYears(unitId)));
    }

    // 取得修業統計的狀態列表 ( 需卡法師 / 居士 / 學員長職事 身份) ( 開課中/待結業/已結業) OK
    // CR3
    @RequestMapping(value = "/class_status", method = RequestMethod.GET)
    public ResponseEntity class_status(HttpServletRequest request,
                                        @RequestParam(value = "year", required = false) String year) {

        return ResponseEntity.ok(new ClassRecord000Response(200, "success",
                classInfoService.getClassStatus(request.getSession(), year)));
    }

    // 取得修業統計的期別列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK
    @RequestMapping(value = "/class_period_num", method = RequestMethod.GET)
    public ResponseEntity classStats001(HttpServletRequest request,
                                        @RequestParam("classStatus") String classStatus) {

        return ResponseEntity.ok(new ClassStats001Response(200, "success",
                classInfoService.getClassStatsClassPeriodNumList(request.getSession(), classStatus)));
    }

    // 取得修業統計的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK (2018/12/03 舊版暫時關閉)
//    @RequestMapping(value = "/classes", method = RequestMethod.GET)
//    public ResponseEntity classStats002(HttpServletRequest request,
//                                        @RequestParam("classStatus") String classStatus,
//                                        @RequestParam("classPeriodNum") String classPeriodNum) {
//
//        return ResponseEntity.ok(new ClassRecord002Response(200, "success",
//                classInfoService.getClassStatsClassNameList(request.getSession(), classStatus, classPeriodNum)));
//    }

    // 取得修業統計的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK (2018/12/03 CR新版)
    // CR3
    @RequestMapping(value = "/classes", method = RequestMethod.GET)
    public ResponseEntity classes(HttpServletRequest request,
                                  @RequestParam(value = "year", required = false) String year,
                                  @RequestParam(value = "classStatus", required = false) String classStatus) {

        return ResponseEntity.ok(new ClassRecord002Response(200, "success",
                classInfoService.getClassRecordClassNameList(request.getSession(), year, classStatus)));
    }

    // 依取得學員的修業統計 OK
    @RequestMapping(value = "/{classId}/members", method = RequestMethod.GET)
    public ResponseEntity classStats003(HttpServletRequest request,
                                        @PathVariable("classId") String classId,
                                        @RequestParam("startDate") String startDate,
                                        @RequestParam("endDate") String endDate) {

        return ResponseEntity.ok(new ClassStats003Response(200, "success",
                classEnrollFormService.getClassStats003List(request.getSession(), classId, startDate, endDate)));
    }

    // 更新學員的修業狀態 OK
    @RequestMapping(value = "/{classId}/members", method = RequestMethod.POST)
    public ResponseEntity classStats004(HttpServletRequest request,
                                        @PathVariable("classId") String classId,
                                        @RequestBody ClassStats004Request classStats004Request) {

        return ResponseEntity.ok(new BaseResponse(200,
                classEnrollFormService.updateClassEnrollForm(request.getSession(), classId, classStats004Request)));
    }

    // 匯出證書套印資料，勾選以及全勤結業由前端傳送id列表過去 OK
    @RequestMapping(value = "/{classId}/excel/cert", method = RequestMethod.GET)
    public ModelAndView classStats005(HttpServletRequest request,
                                        Model model,
                                        @PathVariable("classId") String classId,
                                        @RequestParam("ids") String ids) {

        List<ClassStats005Object> results = classEnrollFormService.excelCert(ids);
        model.addAttribute("results", results);

        return new ModelAndView(new CertExcelView(request.getSession()));
    }

    // 匯出統計表 OK
    @RequestMapping(value = "/{classId}/excel/stats", method = RequestMethod.GET)
    public ModelAndView classStats006(HttpServletRequest request,
                                        Model model,
                                        @PathVariable("classId") String classId,
                                        @RequestParam("startDate") String startDate,
                                        @RequestParam("endDate") String endDate) {

        List<ClassStats006Object> results =
                classEnrollFormService.excelStats(request.getSession(), classId, startDate, endDate);
        model.addAttribute("results", results);

        return new ModelAndView(new StatsExcelView(request.getSession()));
    }

    // 匯出資料組 OK
    @RequestMapping(value = "/{classId}/excel/data", method = RequestMethod.GET)
    public ModelAndView classStats007(HttpServletRequest request,
                                      Model model,
                                      @PathVariable("classId") String classId) {

        List<ClassStats007Object> results = classEnrollFormService.excelData(classId);
        model.addAttribute("results", results);

        return new ModelAndView(new DataExcelView(request.getSession()));
    }

    // 匯出補課名單 OK
    @RequestMapping(value = "/{classId}/excel/make_up_classes", method = RequestMethod.GET)
    public ModelAndView classStats008(HttpServletRequest request,
                                        Model model,
                                        @PathVariable("classId") String classId,
                                        @RequestParam("startDate") String startDate,
                                        @RequestParam("endDate") String endDate) {

        List<ClassStats008Response.ClassStats008_1Object> results =
                classEnrollFormService.excelMakeUpClasses(classId, startDate, endDate);
        model.addAttribute("results", results);

        return new ModelAndView(new MakeUpClassesExcelView(request.getSession()));
    }
}
