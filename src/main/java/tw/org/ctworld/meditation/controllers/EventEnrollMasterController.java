package tw.org.ctworld.meditation.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.EventEnrollMaster.MasterInfoShort;
import tw.org.ctworld.meditation.beans.EventEnrollMember.*;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain002Object;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.libs.RedundantException;
import tw.org.ctworld.meditation.models.*;
import tw.org.ctworld.meditation.services.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/api/event_enroll_master")
@Api(description = "13&16-活動報名(法師)(精舍/本山)")
public class EventEnrollMasterController {

    private static final Logger logger = LoggerFactory.getLogger(EventEnrollMasterController.class);

    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    ClassInfoService classInfoService;

    @Autowired
    ClassEnrollFormService classEnrollFormService;

    @Autowired
    EventMainService eventMainService;

    @Autowired
    EventUnitInfoService eventUnitInfoService;

    @Autowired
    EventEnrollMainService eventEnrollMainService;

    @Autowired
    EventUnitTemplateService eventUnitTemplateService;

    @Autowired
    EventSummerDonationService eventSummerDonationService;

    @Autowired
    CtMemberInfoService ctMemberInfoService;

    @Autowired
    ExcelProcessService excelProcessService;

    @Autowired
    UnitMasterService unitMasterService;


    @RequestMapping(value = "/download_filter_excel", method = RequestMethod.GET)
    @ApiOperation(value = "下載篩選格式檔*")
    public ResponseEntity download_filter_excel(HttpServletRequest request, HttpServletResponse response) {
        try {
            String filename = "篩選法師格式檔.xlsx";

            Resource resource = fileStorageService.loadFileAsResource(filename);

            filename = URLEncoder.encode(filename, StandardCharsets.UTF_8.name());

            response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + filename);

            StreamUtils.copy(resource.getInputStream(), response.getOutputStream());
        } catch (Exception e) {
            logger.debug("篩選法師格式檔.xlsx not found");
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }


    @RequestMapping(value = "/event_mains", method = RequestMethod.GET)
    @ApiOperation(value = "取得活動列表*")
    public ResponseEntity event_mains(HttpServletRequest request, @RequestParam String type) {
        String unitId = null;

        if (type.equals("UNIT")) {
            unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        }

        List<UnitEventsMaintain002Object> items = eventMainService.getEventsByAttendUnit(unitId);

        return ResponseEntity.ok(new ListResponse<>(200, "success", items));
    }


    @RequestMapping(value = "/search_master", method = RequestMethod.GET)
    @ApiOperation(value = "取得法師列表*")
    public ResponseEntity not_enrolls(HttpServletRequest request,
                                      @RequestParam String type,
                                      @RequestParam(required = false) String unitId,
                                      @RequestParam(required = false) String masterIdPrefix,
                                      @RequestParam(required = false) String keyword) {

        List<MasterInfoShort> notEnrolledList = eventEnrollMainService.getNotEnrolledMasters(type, unitId, masterIdPrefix, keyword);

        return ResponseEntity.ok(new ListResponse<>(200, "success", notEnrolledList));
    }


    @RequestMapping(value = "/search_master", method = RequestMethod.POST)
    @ApiOperation(value = "透過excel取得法師列表*")
    public ResponseEntity search_master(HttpServletRequest request,
                                            @RequestParam("file") MultipartFile file,
                                            @RequestParam String type,
                                            @RequestParam boolean isDeleteOriginalData) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<ExcelMember> masters = excelProcessService.processMasterFilterExcel(file);

        return ResponseEntity.ok(ctMemberInfoService.searchMasters(type, isDeleteOriginalData, unitId, masters, request.getSession()));

    }


    @RequestMapping(value = "unit_master_infos/{masterId}", method = RequestMethod.GET)
    @ApiOperation(value = "取得單筆法師資料*")
    public ResponseEntity get_unit_master(HttpServletRequest request, @PathVariable String masterId, @RequestParam(required = false) String unitId) {

        if (unitId == null || unitId.isEmpty()) {
            unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
        }

        List<MasterInfoShort> masterInfoShorts = unitMasterService.getUnitMasterByMasterId(unitId, masterId);

        ResultResponse<MasterInfoShort> response;

        if (masterInfoShorts.size() > 0) {
            response = new ResultResponse<>(200, "success", masterInfoShorts.get(0));
        } else {
            response = new ResultResponse<>(400, "法師不存在", null);
        }

        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "unit_master_infos/{masterId}", method = RequestMethod.POST)
    @ApiOperation(value = "新增單位法師*")
    public ResponseEntity add_unit_master(HttpServletRequest request,
                                           @PathVariable String masterId,
                                           @RequestBody MasterInfoShort masterInfo) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        try {
            unitMasterService.addUnitMasterByMasterId(unitId, masterId, masterInfo, request.getSession());

            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (RedundantException n) {

            return ResponseEntity.ok(new BaseResponse(400, "法師 Id 重複"));
        }
    }

    @RequestMapping(value = "unit_master_infos/{masterId}", method = RequestMethod.PUT)
    @ApiOperation(value = "編輯單位法師*")
    public ResponseEntity edit_unit_master(HttpServletRequest request,
                                           @PathVariable String masterId,
                                           @RequestBody MasterInfoShort masterInfo) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        try {
            unitMasterService.editUnitMasterByMasterId(unitId, masterId, masterInfo, request.getSession());

            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (NotFoundException n) {

            return ResponseEntity.ok(new BaseResponse(400, "法師不存在"));
        }
    }

    @RequestMapping(value = "unit_master_infos/{masterId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "刪除單位法師*")
    public ResponseEntity del_unit_master(HttpServletRequest request, @PathVariable String masterId) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        unitMasterService.delUnitMasterByMasterId(unitId, masterId);

        return ResponseEntity.ok(new BaseResponse(200, "success"));
    }

    @RequestMapping(value = "/event_mains/{eventId}/templates", method = RequestMethod.GET)
    @ApiOperation(value = "取得範本列表*")
    public ResponseEntity get_templates(HttpServletRequest request, @PathVariable String eventId) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<TemplateShort> items = eventUnitTemplateService.getTemplatesByEventIdUnitId(eventId, unitId, true);

        ListResponse<TemplateShort> response = new ListResponse<>(200, "success", items);

        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "/event_mains/{eventId}/once_enroll", method = RequestMethod.POST)
    @ApiOperation(value = "單筆報名*")
    public ResponseEntity event_mains_enroll(HttpServletRequest request,
                                             @PathVariable String eventId,
                                             @RequestBody  EventEnrollRequest eventEnrollRequest) {

        eventEnrollRequest.setEventId(eventId);

        try {
            EventEnrollMain result = eventEnrollMainService.enroll(request.getSession(), eventEnrollRequest, true);
            return ResponseEntity.ok(new EventEnrollResponse(200, "success", result.getFormAutoAddNum()));
        } catch (RedundantException r) {
            return ResponseEntity.ok(new EventEnrollResponse(400, "該活動不可重複報名", null));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new EventEnrollResponse(400, "活動不存在", null));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/once_enroll/{formAutoAddNum}", method = RequestMethod.GET)
    @ApiOperation(value = "取得單筆報名*")
    public ResponseEntity get_enroll(@PathVariable String eventId,
                                     @PathVariable String formAutoAddNum) {

        EventEnrollMainPgmResponse response = eventEnrollMainService.getEnroll(eventId, formAutoAddNum, true);

        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "/event_mains/{eventId}/once_enroll/{formAutoAddNum}", method = RequestMethod.PUT)
    @ApiOperation(value = "修改單筆報名*")
    public ResponseEntity update_enroll(HttpServletRequest request,
                                        @PathVariable String eventId,
                                        @PathVariable String formAutoAddNum,
                                        @RequestBody  EventEnrollRequest eventEnrollRequest) {

        eventEnrollRequest.setEventId(eventId);

        try {
            eventEnrollMainService.updateEnroll(request.getSession(), eventId, formAutoAddNum, eventEnrollRequest, true);
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "活動報名表不存在"));
        }

        return ResponseEntity.ok(new BaseResponse(200, "success"));
    }

    @RequestMapping(value = "/event_mains/{eventId}/many_enroll", method = RequestMethod.POST)
    @ApiOperation(value = "批次報名*")
    public ResponseEntity event_mains_batch_enroll(HttpServletRequest request,
                                                   @PathVariable String eventId,
                                                   @RequestBody  BatchEventEnrollRequest batchEventEnrollRequest) {

        batchEventEnrollRequest.getEnrollData().setEventId(eventId);

        try {
            List<EventEnrollResult> results = eventEnrollMainService.batchEnroll(request.getSession(), batchEventEnrollRequest, true);

            return ResponseEntity.ok(new BatchEventEnrollResponse(200, "success", results));
        } catch (RedundantException r) {
            return ResponseEntity.ok(new BatchEventEnrollResponse(400, "該活動不可重複報名", new ArrayList<>()));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BatchEventEnrollResponse(400, "活動不存在", new ArrayList<>()));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/many_enroll", method = RequestMethod.PUT)
    @ApiOperation(value = "批次異動*")
    public ResponseEntity edit_enroll_batch(HttpServletRequest request,
                                            @PathVariable String eventId,
                                            @RequestBody  BatchEventEnrollRequest batchEventEnrollRequest) {

        batchEventEnrollRequest.getEnrollData().setEventId(eventId);

        try {
            eventEnrollMainService.batchUpdateEnroll(request.getSession(), batchEventEnrollRequest);
            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "活動報名表不存在"));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/is_enrolls", method = RequestMethod.GET)
    @ApiOperation(value = "取得已報名法師*")
    public ResponseEntity event_mains_enrolled(HttpServletRequest request, @RequestParam String type, @PathVariable String eventId) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<EnrollMain> enrolls = eventEnrollMainService.getEnrolledMasterByEventId(type, unitId, eventId);

        return ResponseEntity.ok(new ListResponse<>(200, "success", enrolls));
    }


}
