package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.ListResponse;
import tw.org.ctworld.meditation.services.CtMemberInfoService;
import tw.org.ctworld.meditation.services.CtTableSyncService;
import tw.org.ctworld.meditation.services.EventMainService;

@Controller
@RequestMapping(path = "/")
public class BaseController {

    private static final Logger logger = LoggerFactory.getLogger(BaseController.class);

    @Autowired
    private EventMainService eventMainService;

    @Autowired
    private CtTableSyncService ctTableSyncService;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;

    @RequestMapping(path = "/changelog", method = RequestMethod.GET)
    public ResponseEntity changelog(Model model) {
        Resource resource = new ClassPathResource("changelog.md");
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "text/markdown; charset=utf-8");
            byte[] bytes = StreamUtils.copyToByteArray(resource.getInputStream());
            return ResponseEntity.ok().headers(headers).body(bytes);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok("change log not found!");
        }
    }

    @RequestMapping(path = "/updateEvent", method = RequestMethod.GET)
    public ResponseEntity updateEvent() {
        eventMainService.updateEventStatus();
        return ResponseEntity.ok(new BaseResponse(200, "success"));
    }

    // todo test only
    @RequestMapping(path = "/test", method = RequestMethod.GET)
    public ResponseEntity test() {

        return ResponseEntity.ok(new ListResponse<>(200, "success", ctMemberInfoService.getMemberIds("UNIT01056")));
    }

}
