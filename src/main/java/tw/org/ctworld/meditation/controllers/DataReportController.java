package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import tw.org.ctworld.meditation.beans.DataReport.*;
import tw.org.ctworld.meditation.services.ClassAttendRecordService;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.services.OldClassMonthStatsService;
import tw.org.ctworld.meditation.views.CheckinCountByRangeExcelView;
import tw.org.ctworld.meditation.views.MemberCountExcelView;
import tw.org.ctworld.meditation.views.RealtimeCountExcelView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(path = "/api/data_report")
public class DataReportController {

    private static final Logger logger = LoggerFactory.getLogger(DataReportController.class);

    @Autowired
    private ClassEnrollFormService classEnrollFormService;

    @Autowired
    private ClassAttendRecordService classAttendRecordService;

    @Autowired
    private OldClassMonthStatsService oldClassMonthStatsService;

    @Value("${meditation.oldClassMonthStats.lastYearMonth}")
    private String LAST_YEAR_MONTH;


    // 人數統計 OK
    @RequestMapping(value = "/member_count", method = RequestMethod.GET)
    public ResponseEntity dataReport001(HttpServletRequest request,
                                     @RequestParam("reportType") String reportType,
                                     @RequestParam("year") String year,
                                     @RequestParam("isMeditation") boolean isMeditation) {

        return ResponseEntity.ok(new DataReport001Response(200, "success",
                classEnrollFormService.getMemberCount(reportType, year, isMeditation)));
    }

    // 人數統計(EXCEL) OK
    @RequestMapping(value = "/member_count_excel", method = RequestMethod.GET)
    public ModelAndView dataReport002(HttpServletRequest request,
                                      Model model,
                                      @RequestParam("reportType") String reportType,
                                      @RequestParam("year") String year,
                                      @RequestParam("isMeditation") boolean isMeditation) {

        List<DataReport001Response.DataReport001Object> results =
                classEnrollFormService.getMemberCount(reportType, year, isMeditation);
        model.addAttribute("results", results);

        return new ModelAndView(new MemberCountExcelView(request.getSession()));
    }

    // 即時統計 OK
    @RequestMapping(value = "/realtime_count", method = RequestMethod.GET)
    public ResponseEntity dataReport003(HttpServletRequest request,
                                        @RequestParam("reportType") String reportType) {

        return ResponseEntity.ok(new DataReport003Response(200, "success",
                classEnrollFormService.getRealTimeCount(reportType)));
    }

    // 輸出excel OK
    @RequestMapping(value = "/realtime_count_excel", method = RequestMethod.GET)
    public ModelAndView dataReport004(HttpServletRequest request,
                                      Model model,
                                      @RequestParam("reportType") String reportType) {

        List<DataReport003Response.DataReport003_1Object> results = classEnrollFormService.getRealTimeCount(reportType);
        model.addAttribute("results", results);

        return new ModelAndView(new RealtimeCountExcelView(request.getSession()));
    }

    // 禪修班報到統計表(區間統計) OK
    @RequestMapping(value = "/checkin_count_by_range", method = RequestMethod.GET)
    public ResponseEntity dataReport005(HttpServletRequest request,
                                        @RequestParam("startDate") String startDate,
                                        @RequestParam("endDate") String endDate,
                                        @RequestParam("isMeditation") boolean isMeditation,
                                        @RequestParam("reportType") String reportType) {

        return ResponseEntity.ok(new DataReport005Response(200, "success",
                classEnrollFormService.getCheckinCountByRange(startDate, endDate, isMeditation, reportType)));
    }

    // 禪修班報到統計表(區間統計)(EXCEL) OK
    @RequestMapping(value = "/checkin_count_by_range_excel", method = RequestMethod.GET)
    public ModelAndView dataReport006(HttpServletRequest request,
                                      Model model,
                                      @RequestParam("startDate") String startDate,
                                      @RequestParam("endDate") String endDate,
                                      @RequestParam("isMeditation") boolean isMeditation,
                                      @RequestParam("reportType") String reportType) {

        List<DataReport005Response.DataReport005_1Object> results =
                classEnrollFormService.getCheckinCountByRange(startDate, endDate, isMeditation, reportType);
        model.addAttribute("results", results);

        return new ModelAndView(new CheckinCountByRangeExcelView(request.getSession()));
    }

    // 海內外精舍週平均(直接輸出Google Chart格式)
    @RequestMapping(value = "/avg_by_range", method = RequestMethod.GET)
    public ResponseEntity dataReport007(HttpServletRequest request,
                                        @RequestParam("startDate") String startDate,
                                        @RequestParam("endDate") String endDate) {

        int lastYear = Integer.parseInt(LAST_YEAR_MONTH.substring(0,4));
        int lastMonth = Integer.parseInt(LAST_YEAR_MONTH.substring(5,7));

        List<String>[] oldItems = new List[0];
        List<String>[] newItems = new List[0];
        List<String>[] items = new List[0];

        long mStartDate = 0, mEndDate = 0, mLastDate = 0;
        int type = 0;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mStartDate = dateFormat.parse(startDate).getTime();
            mEndDate = dateFormat.parse(endDate).getTime();
            mLastDate = dateFormat.parse(LAST_YEAR_MONTH + "-01").getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (mStartDate >= mLastDate) {

            newItems = classAttendRecordService.getAvgByRange(startDate, endDate);

            type = 2;
        }

        if (mStartDate < mLastDate && mEndDate >= mLastDate) {

            String newEndDate = lastYear + "-" + ((lastMonth - 1) < 10 ? "0" + (lastMonth - 1) : (lastMonth - 1)) + "-01";
            oldItems = oldClassMonthStatsService.getAvgByRange(startDate, newEndDate);
            newItems = classAttendRecordService.getAvgByRange(LAST_YEAR_MONTH + "-01", endDate);

            type = 1;
        }

        if (mEndDate < mLastDate) {

            oldItems = oldClassMonthStatsService.getAvgByRange(startDate, endDate);

            type = 0;
        }

        switch (type) {
            case 0:
                items = new List[oldItems.length];

                for (int x = 0 ; x < items.length ; x++) {

                    items[x] = oldItems[x];
                }
                break;
            case 1:
                items = new List[oldItems.length + newItems.length - 1];

                for (int x = 0 ; x < items.length ; x++) {

                    if (x < oldItems.length) {

                        items[x] = oldItems[x];
                    }else {

                        items[x] = newItems[x - oldItems.length + 1];
                    }
                }
                break;
            case 2:

                items = new List[newItems.length];

                for (int x = 0 ; x < items.length ; x++) {

                    items[x] = newItems[x];
                }
                break;
        }

        return ResponseEntity.ok(new DataReport007Response(items));
    }

    // 海內外精舍週平均 pdf
    //@RequestMapping(value = "/avg_by_range_pdf", method = RequestMethod.GET)
    //public ResponseEntity dataReport008(HttpServletRequest request,
    //                                    @RequestParam("startDate") String startDate,
    //                                    @RequestParam("endDate") String endDate) {
    //
    //    return ResponseEntity.ok(new BaseResponse(200, "success"));
    //}

    // 海內外精舍依班別(直接輸出Google Chart格式)
    @RequestMapping(value = "/avg_by_class", method = RequestMethod.GET)
    public ResponseEntity dataReport009(HttpServletRequest request,
                                        @RequestParam("unitId") String unitId,
                                        @RequestParam("startDate") String startDate,
                                        @RequestParam("endDate") String endDate,
                                        @RequestParam("language") String language,
                                        @RequestParam("classes") String classes) {

//        List<String>[] items = new List[0];
//
//        switch (getDataType(startDate, endDate)) {
//
//            case 400: // 跨區
//
//                logger.error("跨區");
//                break;
//            case 201: // 新資料
//
//                items = classAttendRecordService.getAvgByClass(unitId, startDate, endDate, language, classes);
//                break;
//            case 202: // 舊資料
//
//                items = oldClassMonthStatsService.getAvgByClass(unitId, startDate, endDate, language, classes);
//                break;
//        }

        int lastYear = Integer.parseInt(LAST_YEAR_MONTH.substring(0,4));
        int lastMonth = Integer.parseInt(LAST_YEAR_MONTH.substring(5,7));

        List<String>[] oldItems = new List[0];
        List<String>[] newItems = new List[0];
        List<String>[] items = new List[0];

        long mStartDate = 0, mEndDate = 0, mLastDate = 0;
        int type = 0;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mStartDate = dateFormat.parse(startDate).getTime();
            mEndDate = dateFormat.parse(endDate).getTime();
            mLastDate = dateFormat.parse(LAST_YEAR_MONTH + "-01").getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (mStartDate >= mLastDate) {

            newItems = classAttendRecordService.getAvgByClass(unitId, startDate, endDate, language, classes);

            type = 2;
        }

        if (mStartDate < mLastDate && mEndDate >= mLastDate) {

            String newEndDate = lastYear + "-" + ((lastMonth - 1) < 10 ? "0" + (lastMonth - 1) : (lastMonth - 1)) + "-01";
            oldItems = oldClassMonthStatsService.getAvgByClass(unitId, startDate, newEndDate, language, classes);
            newItems = classAttendRecordService.getAvgByClass(unitId, LAST_YEAR_MONTH + "-01", endDate, language, classes);

            type = 1;
        }

        if (mEndDate < mLastDate) {

            oldItems = oldClassMonthStatsService.getAvgByClass(unitId, startDate, endDate, language, classes);

            type = 0;
        }

        switch (type) {
            case 0:
                items = new List[oldItems.length];

                for (int x = 0 ; x < items.length ; x++) {

                    items[x] = oldItems[x];
                }
                break;
            case 1:
                items = new List[oldItems.length + newItems.length - 1];

                for (int x = 0 ; x < items.length ; x++) {

                    List<String> contentList = new ArrayList<>();

                    if (x == 0) {

                        for (int s = 0 ; s < (oldItems[x].size() + newItems[x].size()) - 4 ; s++) {

                            if (s < oldItems[x].size() - 3) {

                                contentList.add(oldItems[x].get(s));
                            }else {

                                contentList.add(newItems[x].get(s - (oldItems[x].size() - 3) + 1));
                            }

                        }
                    }else if (x < (items.length - (newItems.length - 1))){

                        for (int s = 0 ; s < (oldItems[0].size() + newItems[0].size()) - 4 ; s++) {

                            if (s < oldItems[0].size() - 3) {

                                contentList.add(oldItems[x].get(s));
                            }else if (s >= ((oldItems[0].size() + newItems[0].size()) - 4) - 3){

                                contentList.add(oldItems[x].get(s - (newItems[0].size() - 4)));
                            }else {

                                contentList.add("0");
                            }

                        }
                    }else {

                        for (int s = 0 ; s < (oldItems[0].size() + newItems[0].size()) - 4 ; s++) {

                            if (s == 0) {

                                contentList.add(newItems[x - oldItems.length + 1].get(s));
                            }else if (s > 0 && s < oldItems[0].size() - 3) {

                                contentList.add("0");
                            }else if (s >= ((oldItems[0].size() + newItems[0].size()) - 4) - 3){

                                contentList.add(newItems[x - oldItems.length + 1].get(s - (oldItems[0].size() - 3) + 1));
                            }else {

                                contentList.add(newItems[x - oldItems.length + 1].get(s - (oldItems[0].size() - 3) + 1));
                            }
                        }
                    }

                    items[x] = contentList;
                }
                break;
            case 2:

                items = new List[newItems.length];

                for (int x = 0 ; x < items.length ; x++) {

                    items[x] = newItems[x];
                }
                break;
        }

        return ResponseEntity.ok(new DataReport009Response(items));
    }

    // 海內外精舍依班別 pdf
    //@RequestMapping(value = "/avg_by_class_pdf", method = RequestMethod.GET)
    //public ResponseEntity dataReport010(HttpServletRequest request,
    //                                    @RequestParam("unitId") String unitId,
    //                                    @RequestParam("startDate") String startDate,
    //                                    @RequestParam("endDate") String endDate,
    //                                    @RequestParam("language") String language,
    //                                    @RequestParam("classes") String classes) {
    //
    //    return ResponseEntity.ok(new BaseResponse(200, "success"));
    //}

    // 海內外精舍依班別(直接輸出Google Chart格式)
    @RequestMapping(value = "/avg_by_week", method = RequestMethod.GET)
    public ResponseEntity dataReport011(HttpServletRequest request,
                                        @RequestParam("unitId") String unitId,
                                        @RequestParam("startDate") String startDate,
                                        @RequestParam("endDate") String endDate,
                                        @RequestParam("weeks") String weeks) {

        int errCode = 200;
        String errMsg = "success";

//        List<String>[] items = new List[0];
//
//        switch (getDataType(startDate, endDate)) {
//
//            case 400: // 跨區
//
//                errCode = 400;
//                errMsg = "不得跨越" + LAST_YEAR_MONTH.replace("-","/");
//                break;
//            case 201: // 新資料
//
//                items = classAttendRecordService.getAvgByWeek(unitId, startDate, endDate, weeks);
//                break;
//            case 202: // 舊資料
//
//                errMsg = "舊資料未完成";
//                break;
//        }

        List<String>[] items = classAttendRecordService.getAvgByWeek(unitId, startDate, endDate, weeks);

        return ResponseEntity.ok(new DataReport011Response(errCode, errMsg, items));
    }

    // 海內外精舍依班別 pdf
    //@RequestMapping(value = "/avg_by_week_pdf", method = RequestMethod.GET)
    //public ResponseEntity dataReport012(HttpServletRequest request,
    //                                    @RequestParam("unitId") String unitId,
    //                                    @RequestParam("startDate") String startDate,
    //                                    @RequestParam("endDate") String endDate,
    //                                    @RequestParam("weeks") String weeks) {
    //
    //    return ResponseEntity.ok(new BaseResponse(200, "success"));
    //}
    
    private int getDataType(String startDate, String endDate) {

        Date date = java.sql.Date.valueOf(LAST_YEAR_MONTH + "-1");
        long seconds = date.getTime() / 1000;

        date = java.sql.Date.valueOf(startDate);
        long startDateSeconds = date.getTime() / 1000;

        date = java.sql.Date.valueOf(endDate);
        long endDateSeconds = date.getTime() / 1000;
        
        int dataType = 400;

        if (startDateSeconds >= seconds && endDateSeconds > seconds) {

            // 新資料
            dataType = 201;
        }else if (startDateSeconds < seconds && endDateSeconds < seconds) {

            // 舊資料
            dataType = 202;
        }else {

            // 跨區 錯誤
            dataType = 400; 
        }
        
        return dataType;
    }
}
