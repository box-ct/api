package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.services.FileStorageService;
import tw.org.ctworld.meditation.services.VideoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(path = "/api/video")
public class VideoController {

    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    VideoService videoService;

    private static final Logger logger = LoggerFactory.getLogger(VideoController.class);

    /**
     * serving video & audio
     */
//    @RequestMapping(path = "/stream/{name}", method = RequestMethod.GET)
//    public void video(HttpServletRequest request, HttpServletResponse response,
//                                 @PathVariable(value = "name") String name) {
//
//        logger.debug("/api/video/stream/" + name);
//
//        try {
//            Resource resource = fileStorageService.loadFile(1, name);
//            MultipartFileSender.fromPath(resource.getFile().toPath())
//                    .with(request)
//                    .with(response)
//                    .serveResource();
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//        }
//    }

    /**
     * upload multiple videos or audios
     */
//    @RequestMapping(value = "/uploads", method = RequestMethod.POST)
//    public ResponseEntity uploadMultiple(
//            @RequestParam("uploadFiles") MultipartFile[] uploadFiles,
//            @RequestParam("fileName") String fileName) {
//
//        logger.debug("/api/video/uploads " + fileName);
//
//        try {
//
//            List<String> formats = new ArrayList<>();
//            List<String> tempFilenames = new ArrayList<>();
//
//            // check format
//            for (int i = 0; i < uploadFiles.length; i++) {
//                logger.debug("file name " + uploadFiles[i].getOriginalFilename());
//
//                String oriFilename = uploadFiles[i].getOriginalFilename();
//                String[] temp = oriFilename.split("\\.");
//                String fileFormat = temp[temp.length - 1];
//
//                // if format supported
//                if (!videoService.isSupported(fileFormat)) {
//                    String json = new Gson().toJson(new BaseResponse(404, "#" + i + " file format not supported"));
//                    return ResponseEntity.status(404).contentType(MediaType.APPLICATION_JSON_UTF8).body(json);
//                } else {
//                    formats.add(fileFormat);
//                }
//
//                // if all files are audio or video (not mixed)
//                if (formats.size() > 1) {
//                    if (videoService.isVideo(formats.get(i-1)) != videoService.isVideo(formats.get(i))) {
//                        String json = new Gson().toJson(new BaseResponse(405, "mixed formats"));
//                        return ResponseEntity.status(405).contentType(MediaType.APPLICATION_JSON_UTF8).body(json);
//                    }
//                }
//            }
//
//            // save file to temp folder
//            for (int i = 0; i < uploadFiles.length; i++) {
//                String tempFilename = fileName + "." + i;
//                tempFilenames.add(tempFilename);
//                fileStorageService.saveVideo(uploadFiles[i], tempFilename);
//            }
//
//            // convert format and join
//            new Thread(new Runnable(){
//                @Override
//                public void run(){
//                    try {
//                        boolean isVideo = videoService.isVideo(formats.get(0));
//
//                        if (isVideo) {
//                            videoService.joinVideos(tempFilenames, fileName + ".mp4");
//                        } else {
//                            videoService.joinAudios(tempFilenames, fileName + ".mp3");
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        logger.error(e.getMessage());
//                    }
//                }
//            }).start();
//
//            return ResponseEntity.ok(new BaseResponse(0, "success"));
//        } catch (Exception e) {
//            e.printStackTrace();
//            logger.error(e.getMessage());
//            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
//        }
//    }

    /**
     * delete video & audio
     */
//    @RequestMapping(path = "/delete/{name}", method = RequestMethod.GET)
//    public ResponseEntity delete(HttpServletRequest request, HttpServletResponse response,
//                      @PathVariable(value = "name") String name) {
//
//        logger.debug("/api/video/delete/" + name);
//
//        videoService.deleteFile(name);
//
//        return ResponseEntity.ok().build();
//    }

    /**
     * get free space
     */
    @RequestMapping(path = "/space", method = RequestMethod.GET)
    public ResponseEntity space(HttpServletRequest request, HttpServletResponse response) {

        logger.debug("/api/video/space");

        return ResponseEntity.ok(videoService.getFreeSpace());
    }


    /**
     * get files
     */
//    @RequestMapping(path = "/files", method = RequestMethod.GET)
//    public ResponseEntity files(HttpServletRequest request, HttpServletResponse response) {
//
//        logger.debug("/api/video/files");
//
//        File[] files = videoService.getFiles();
//
//        List<AVFile> avFiles = new ArrayList<>();
//
//        if (files != null && files.length > 0) {
//            for (int i = 0; i < files.length; i++) {
//                File file = files[i];
//                if (file.isFile()) {
//                    avFiles.add(new AVFile(file.getName(), file.length(), file.lastModified(), videoService.displayFileSize(file.length()), sdf.format(file.lastModified())));
//                }
//            }
//        }
//
//        return ResponseEntity.ok(avFiles);
//    }

    /**
     * get free space
     */
    @RequestMapping(path = "/rearrangeMemberImages", method = RequestMethod.GET)
    public ResponseEntity rearrangeMemberImages(HttpServletRequest request) {

        logger.debug("/api/video/rearrangeMemberImages");

        fileStorageService.rearrangeMemberImage();

        return ResponseEntity.ok(new BaseResponse(0, "success"));
    }
}
