package tw.org.ctworld.meditation.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord000Response;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord002Response;
import tw.org.ctworld.meditation.beans.ClassUpgrade.*;
import tw.org.ctworld.meditation.beans.StringListResponse;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.services.ClassInfoService;

import javax.servlet.http.HttpServletRequest;

import static tw.org.ctworld.meditation.libs.CommonUtils.print2JSON;

@Controller
@RequestMapping(path = "/api/class_upgrade")
public class ClassUpgradeController {

    private static final Logger logger = LoggerFactory.getLogger(ClassUpgradeController.class);

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private ClassEnrollFormService classEnrollFormService;

    // CR3 get year list of classes
    @RequestMapping(value = "/years", method = RequestMethod.GET)
    public ResponseEntity years(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new StringListResponse(200, "success",
                classInfoService.getYears(unitId)));
    }

    // 取得上課記錄的狀態列表 ( 需卡法師 / 居士 / 學員長職事 身份) (可報名/開課中/待結業) OK
    // CR3
    @RequestMapping(value = "/class_status", method = RequestMethod.GET)
    public ResponseEntity classUpgrade000(HttpServletRequest request,
                                          @RequestParam(value = "year", required = false) String year) {

        return ResponseEntity.ok(new ClassRecord000Response(200, "success",
                classInfoService.getClassStatus(request.getSession(), year)));
    }

    // 取得班級續升的期別列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK
    @RequestMapping(value = "/class_period_num", method = RequestMethod.GET)
    public ResponseEntity classUpgrade001(HttpServletRequest request,
                                          @RequestParam("classStatus") String classStatus) {

        return ResponseEntity.ok(new ClassUpgrade001Response(200,"success",
                classInfoService.getClassUpgradeClassPeriodNumList(request.getSession(), classStatus)));
    }

    // 取得班級續升的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK (2018/12/03 舊版暫時關閉)
//    @RequestMapping(value = "/classes", method = RequestMethod.GET)
//    public ResponseEntity classUpgrade002(HttpServletRequest request,
//                                          @RequestParam("classStatus") String classStatus,
//                                          @RequestParam("classPeriodNum") String classPeriodNum) {
//
//        return ResponseEntity.ok(new ClassRecord002Response(200,"success",
//                classInfoService.getClassUpgradeClassNameList(request.getSession(), classStatus, classPeriodNum)));
//    }

    // 取得班級續升的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK (2018/12/03 CR新版)
    // CR3
    @RequestMapping(value = "/classes", method = RequestMethod.GET)
    public ResponseEntity classes(HttpServletRequest request,
                                  @RequestParam(value = "year", required = false) String year,
                                  @RequestParam(value = "classStatus", required = false) String classStatus) {

        return ResponseEntity.ok(new ClassRecord002Response(200, "success",
                classInfoService.getClassRecordClassNameList(request.getSession(), year, classStatus)));
    }

    // 取得班級續升的學員列表 (左右表格皆取這API), isAttend欄位用來判斷該學員是否有上課記錄 OK
    @RequestMapping(value = "/{classId}/members", method = RequestMethod.GET)
    public ResponseEntity classUpgrade003(HttpServletRequest request, @PathVariable("classId") String classId) {

        return ResponseEntity.ok(new ClassUpgrade003Response(200,"success",
                classEnrollFormService.getClassEnrollFormListByClassId(request.getSession(), classId)));
    }

    // 左Grid，更新升班備註 OK
    @RequestMapping(value = "/{classId}/members/{memberId}/nextNewClassNote", method = RequestMethod.PUT)
    public ResponseEntity classUpgrade004(HttpServletRequest request,
                                          @PathVariable("classId") String classId,
                                          @PathVariable("memberId") String memberId,
                                          @RequestBody ClassUpgrade004Request classUpgrade004Request) {

        return ResponseEntity.ok(new BaseResponse(200, classEnrollFormService
                .updateNextClassNote(request.getSession(), classId, memberId, classUpgrade004Request.getText())));
    }

    // 右Grid、修改 OK
    @RequestMapping(value = "/{classId}/members/{memberId}", method = RequestMethod.PUT)
    public ResponseEntity classUpgrade005(HttpServletRequest request,
                                          @PathVariable("classId") String classId,
                                          @PathVariable("memberId") String memberId,
                                          @RequestBody ClassUpgrade005Request classUpgrade005Request) {

        return ResponseEntity.ok(classEnrollFormService.updateClassEnrollFormJobTitle(
                request.getSession(), classId, memberId, classUpgrade005Request));
    }

    // 設定下期不再上襌修班 OK
    @RequestMapping(value = "/{classId}/members_set_not_back", method = RequestMethod.POST)
    public ResponseEntity classUpgrade006(HttpServletRequest request,
                                          @PathVariable("classId") String classId,
                                          @RequestBody ClassUpgrade006Request classUpgrade006Request) {

        return ResponseEntity.ok(new BaseResponse(200, classEnrollFormService.setMemberSetNotBack(
                request.getSession(), classId, classUpgrade006Request)));
    }

    // 班級續升，左邊表格續升
    @RequestMapping(value = "/{classId}/members_upgrade", method = RequestMethod.POST)
    public ResponseEntity classUpgrade007(HttpServletRequest request,
                                          @PathVariable("classId") String classId,
                                          @RequestBody ClassUpgrade007Request classUpgrade007Request) {

        return ResponseEntity.ok(new BaseResponse(200, classEnrollFormService.membersUpgrade(
                request.getSession(), classId, classUpgrade007Request)));
    }

    // 班級續升，右邊表格移除
    @RequestMapping(value = "/{classId}/members_remove", method = RequestMethod.POST)
    public ResponseEntity classUpgrade008(HttpServletRequest request,
                                          @PathVariable("classId") String classId,
                                          @RequestBody ClassUpgrade008Request classUpgrade008Request) {

        return ResponseEntity.ok(new BaseResponse(200, classEnrollFormService.membersRemove(
                request.getSession(), classId, classUpgrade008Request)));
    }

    // CR3
    @RequestMapping(value = "/next_class_info", method = RequestMethod.GET)
    public ResponseEntity next_class_info(HttpServletRequest request,
                                  @RequestParam(value = "classId", required = false) String classId) {

        return ResponseEntity.ok(new ClassRecord002Response(200, "success",
                classInfoService.getClassRecordClassNameList(request.getSession(), classId)));
    }
}
