package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.Classes.*;
import tw.org.ctworld.meditation.services.CodeDefService;
import tw.org.ctworld.meditation.services.ClassDateInfoService;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.services.ClassInfoService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/classes")
public class ClassesController {

    private static final Logger logger = LoggerFactory.getLogger(ClassesController.class);

    @Autowired
    private CodeDefService codeDefService;

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private ClassEnrollFormService classEnrollFormService;

    @Autowired
    private ClassDateInfoService classDateInfoService;


    // 依年度、狀態、期別、班別取得課程資料 OK
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity classes001(HttpServletRequest request,
                                     @RequestParam("year") String year,
                                     @RequestParam("classStatus") String classStatus,
                                     @RequestParam("classPeriodNum") String classPeriodNum,
                                     @RequestParam("className") String className) {

        logger.debug("/api/classes" + year + "/" + classStatus + "/" + classPeriodNum + "/" + className);

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new Classes001Response(200, "success",
                classInfoService.getClassInfoList(year, classStatus, classPeriodNum, className, unitId)));
    }

    // 新增課程 OK
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity classes002(HttpServletRequest request, @RequestBody Classes002Request classes002Request) {

        String[] callback = classInfoService.addClassInfo(classes002Request, request.getSession()).split(";");

        int errCode = Integer.parseInt(callback[0]);

        String errMsg = callback[1];

        // after class info created successfully, add class dates
        if (errCode == 200) {
            classDateInfoService.createClassDates(errMsg, request.getSession());
        }

        return ResponseEntity.ok(new BaseResponse(errCode, errMsg));
    }

    // 取得課程資料 OK
    @RequestMapping(value = "/{classId}", method = RequestMethod.GET)
    public ResponseEntity classes003(HttpServletRequest request, @PathVariable("classId") String classId) {

        return ResponseEntity.ok(classInfoService.getClassInfoByClassId(classId));
    }

    // 依classId刪除課程 OK
    @RequestMapping(value = "/{classId}", method = RequestMethod.DELETE)
    public ResponseEntity classes004(HttpServletRequest request, @PathVariable("classId") String classId) {

        String[] callback = classInfoService.deleteClassInfoByClassId(classId).split(";");
        int errCode = Integer.parseInt(callback[0]);
        String errMsg = callback[1];

        return ResponseEntity.ok(new BaseResponse(errCode, errMsg));
    }

    // 修改課程資料 OK
    @RequestMapping(value = "/{classId}", method = RequestMethod.PUT)
    public ResponseEntity classes005(HttpServletRequest request, @PathVariable("classId") String classId,
                                     @RequestBody Classes005Request classes005Request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        String[] callback = classInfoService.editClassInfoByClassId(unitId, classId, classes005Request, request.getSession()).split(";");
        int errCode = Integer.parseInt(callback[0]);
        String errMsg = callback[1];

        return ResponseEntity.ok(new BaseResponse(errCode, errMsg));
    }

    // 取得已報名該課程的學員列表 OK
    @RequestMapping(value = "/{classId}/members", method = RequestMethod.GET)
    public ResponseEntity classes006(HttpServletRequest request,
                                     @PathVariable("classId") String classId) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new Classes006Response(200,"success",
                classEnrollFormService.getClassRegisterList(classId, unitId)));
    }

    // 取得該課程的學員長職事列表 OK
//    @RequestMapping(value = "/{classId}/leaders", method = RequestMethod.GET)
//    public ResponseEntity classes007(HttpServletRequest request, @PathVariable("classId") String classId) {
//
//        return ResponseEntity.ok(new Classes007Response(200,"success", classEnrollFormService.getClassLeaderList(classId)));
//    }

    // 新增學員長執事 OK
//    @RequestMapping(value = "/{classId}/leaders", method = RequestMethod.POST)
//    public ResponseEntity classes008(HttpServletRequest request,
//                                     @PathVariable("classId") String classId,
//                                     @RequestBody Classes008Request classes008Request) {
//
//        return ResponseEntity.ok(classEnrollFormService.addClassEnrollFormJobTitle(request.getSession(), classes008Request));
//    }

    // 依id取得學員長執事資料 OK
//    @RequestMapping(value = "/{classId}/leaders/{classEnrollFormId}", method = RequestMethod.GET)
//    public ResponseEntity classes009(HttpServletRequest request,
//                                     @PathVariable("classId") String classId,
//                                     @PathVariable("classEnrollFormId") String classEnrollFormId) {
//
//        return ResponseEntity.ok(classEnrollFormService.getClassEnrollForm(classEnrollFormId));
//    }

    // 更新學員長職事資料 OK
//    @RequestMapping(value = "/{classId}/leaders/{classEnrollFormId}", method = RequestMethod.PUT)
//    public ResponseEntity classes010(HttpServletRequest request,
//                                     @PathVariable("classId") String classId,
//                                     @PathVariable("classEnrollFormId") String classEnrollFormId,
//                                     @RequestBody Classes010Request classes010Request) {
//
//        return ResponseEntity.ok(new BaseResponse(200,
//                classEnrollFormService.editClassEnrollFormJobTitle(request.getSession(), classEnrollFormId, classes010Request)));
//    }

    // 依id刪除學員長職事資料 OK
//    @RequestMapping(value = "/{classId}/leaders/{classEnrollFormId}", method = RequestMethod.DELETE)
//    public ResponseEntity classes011(HttpServletRequest request,
//                                     @PathVariable("classId") String classId,
//                                     @PathVariable("classEnrollFormId") String classEnrollFormId) {
//
//
//        return ResponseEntity.ok(new BaseResponse(200,
//                classEnrollFormService.deleteClassEnrollFormJobTitle(request.getSession(), classEnrollFormId)));
//    }

    // TEST 更新課程狀態
    @RequestMapping(value = "/updateClassStatus", method = RequestMethod.GET)
    public ResponseEntity classes012(HttpServletRequest request) {

        classInfoService.updateClassStatus();

        return ResponseEntity.ok(new BaseResponse(200,"success"));
    }

    // TEST 自動新增課程
    @RequestMapping(value = "/allClass1", method = RequestMethod.POST)
    public ResponseEntity classes013(HttpServletRequest request) {

        String[] callback = classInfoService.addClass(request.getSession()).split(";");
        int errCode = Integer.parseInt(callback[0]);
        String errMsg = callback[1];

        return ResponseEntity.ok(new BaseResponse(errCode, errMsg));
    }
}
