package tw.org.ctworld.meditation.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup001Object;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup001Response;
import tw.org.ctworld.meditation.beans.EventEnrollMember.*;
import tw.org.ctworld.meditation.beans.MemberInfos.MemberIds;
import tw.org.ctworld.meditation.beans.MemberInfos.MemberInfoItem;
import tw.org.ctworld.meditation.beans.MemberInfos.NewMemberRequest;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain002Object;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.libs.RedundantException;
import tw.org.ctworld.meditation.libs.UnacceptableException;
import tw.org.ctworld.meditation.models.CtMemberInfo;
import tw.org.ctworld.meditation.models.EventEnrollMain;
import tw.org.ctworld.meditation.models.EventSummerDonationRecord;
import tw.org.ctworld.meditation.services.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/api/event_enroll_member")
@Api(description = "12-活動報名(學員)")
public class EventEnrollMemberController {

    private static final Logger logger = LoggerFactory.getLogger(EventEnrollMemberController.class);

    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    ClassInfoService classInfoService;

    @Autowired
    ClassEnrollFormService classEnrollFormService;

    @Autowired
    EventMainService eventMainService;

    @Autowired
    EventUnitInfoService eventUnitInfoService;

    @Autowired
    EventEnrollMainService eventEnrollMainService;

    @Autowired
    EventUnitTemplateService eventUnitTemplateService;

    @Autowired
    EventSummerDonationService eventSummerDonationService;

    @Autowired
    CtMemberInfoService ctMemberInfoService;

    @Autowired
    ExcelProcessService excelProcessService;


    @RequestMapping(value = "/download_filter_excel", method = RequestMethod.GET)
    @ApiOperation(value = "下載篩選格式檔*")
    public ResponseEntity download_filter_excel(HttpServletRequest request, HttpServletResponse response) {
        try {
            String filename = "匯入學員名單格式檔.xlsx";

            Resource resource = fileStorageService.loadFileAsResource(filename);

            filename = URLEncoder.encode(filename, StandardCharsets.UTF_8.name());

            response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + filename);

            StreamUtils.copy(resource.getInputStream(), response.getOutputStream());
        } catch (Exception e) {
            logger.debug("匯入學員名單格式檔.xlsx not found");
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/class_infos", method = RequestMethod.GET)
    @ApiOperation(value = "取得開班中班別列表*")
    public ResponseEntity class_infos(HttpServletRequest request, @RequestParam(required = false) String memberId) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<ClassGroup001Object> classes;

        if (memberId != null && !memberId.isEmpty()) {
            classes = classEnrollFormService.getOpeningClassByMemberId(unitId, memberId);
        } else {
            classes = classInfoService.getClassByUnitId(unitId);
        }

        return ResponseEntity.ok(new ClassGroup001Response(200, "success", classes));
    }

    @RequestMapping(value = "/event_mains", method = RequestMethod.GET)
    @ApiOperation(value = "取得活動列表*")
    public ResponseEntity event_mains(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<UnitEventsMaintain002Object> items = eventMainService.getEventsByAttendUnit(unitId);

        return ResponseEntity.ok(new ListResponse<>(200, "success", items));
    }

    @RequestMapping(value = "/event_mains/{eventId}", method = RequestMethod.GET)
    @ApiOperation(value = "取得單筆活動*")
    public ResponseEntity event_mains(HttpServletRequest request, @PathVariable String eventId) {

        tw.org.ctworld.meditation.beans.EventEnrollMember.EventMain eventMain = eventMainService.getEventMain(eventId);

        if (eventMain != null) {
            return ResponseEntity.ok(new EventMainResponse(200, "success", eventMain));
        } else {
            return ResponseEntity.ok(new EventMainResponse(400, "event not found", null));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/leader", method = RequestMethod.PUT)
    @ApiOperation(value = "更新活動職事*")
    public ResponseEntity event_mains_leader(HttpServletRequest request, @PathVariable String eventId, @RequestBody ModLeaderRequest modLeaderRequest) {

        boolean found = eventUnitInfoService.modifyLeader(request.getSession(), eventId, modLeaderRequest);

        if (found) {
            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } else {
            return ResponseEntity.ok(new BaseResponse(400, "event unit not found"));
        }

    }

    @RequestMapping(value = "/event_mains/{eventId}/is_enrolls", method = RequestMethod.GET)
    @ApiOperation(value = "取得已報名學員*")
    public ResponseEntity event_mains_enrolled(HttpServletRequest request, @PathVariable String eventId, @RequestParam(required = false) String unitId) {

        List<EnrollMain> enrolls = eventEnrollMainService.getEnrolledMembersByEventId(unitId, eventId);

        return ResponseEntity.ok(new ListResponse<>(200, "success", enrolls));
    }

    @RequestMapping(value = "/event_mains/{eventId}/once_enroll", method = RequestMethod.POST)
    @ApiOperation(value = "單筆報名*")
    public ResponseEntity event_mains_enroll(HttpServletRequest request,
                                             @PathVariable String eventId,
                                             @RequestBody  EventEnrollRequest eventEnrollRequest) {

        eventEnrollRequest.setEventId(eventId);

        try {
            EventEnrollMain result = eventEnrollMainService.enroll(request.getSession(), eventEnrollRequest, false);
            return ResponseEntity.ok(new EventEnrollResponse(200, "success", result.getFormAutoAddNum()));
        } catch (RedundantException r) {
            return ResponseEntity.ok(new EventEnrollResponse(400, "該活動不可重複報名", null));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new EventEnrollResponse(400, "活動不存在", null));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/once_enroll/{formAutoAddNum}", method = RequestMethod.GET)
    @ApiOperation(value = "取得單筆報名資料*")
    public ResponseEntity get_enroll(@PathVariable String eventId,
                                     @PathVariable String formAutoAddNum) {

        EventEnrollMainPgmResponse response = eventEnrollMainService.getEnroll(eventId, formAutoAddNum, false);

        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "/event_mains/{eventId}/once_enroll/{formAutoAddNum}", method = RequestMethod.PUT)
    @ApiOperation(value = "異動單筆報名資料*")
    public ResponseEntity update_enroll(HttpServletRequest request,
                                        @PathVariable String eventId,
                                        @PathVariable String formAutoAddNum,
                                        @RequestBody  EventEnrollRequest eventEnrollRequest) {

        eventEnrollRequest.setEventId(eventId);

        try {
            eventEnrollMainService.updateEnroll(request.getSession(), eventId, formAutoAddNum, eventEnrollRequest, false);
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "活動報名表不存在"));
        }

        return ResponseEntity.ok(new BaseResponse(200, "success"));
    }

    @RequestMapping(value = "/event_mains/{eventId}/once_enroll/{formAutoAddNum}/enroll_order_num", method = RequestMethod.PUT)
    @ApiOperation(value = "inline edit enroll_order_num*")
    public ResponseEntity edit_enroll_order_num (HttpServletRequest request, @PathVariable String eventId, @PathVariable String formAutoAddNum, @RequestBody ValueRequest<Integer> value) {

        try {
            eventEnrollMainService.updateEnrollOrderNum(request.getSession(), eventId, formAutoAddNum, value.getValue());
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "活動報名表不存在"));
        }

        return ResponseEntity.ok(new BaseResponse(200, "success"));
    }

    @RequestMapping(value = "/event_mains/{eventId}/many_enroll", method = RequestMethod.POST)
    @ApiOperation(value = "批次報名*")
    public ResponseEntity event_mains_batch_enroll(HttpServletRequest request,
                                                   @PathVariable String eventId,
                                                   @RequestBody  BatchEventEnrollRequest batchEventEnrollRequest) {


        batchEventEnrollRequest.getEnrollData().setEventId(eventId);

        try {
            List<EventEnrollResult> results = eventEnrollMainService.batchEnroll(request.getSession(), batchEventEnrollRequest, false);

            return ResponseEntity.ok(new BatchEventEnrollResponse(200, "success", results));
        } catch (RedundantException r) {
            return ResponseEntity.ok(new BatchEventEnrollResponse(400, "該活動不可重複報名", new ArrayList<>()));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BatchEventEnrollResponse(400, "活動不存在", new ArrayList<>()));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/many_enroll", method = RequestMethod.PUT)
    @ApiOperation(value = "異動多筆報名資料*")
    public ResponseEntity edit_enroll_batch(HttpServletRequest request,
                                            @PathVariable String eventId,
                                            @RequestBody  BatchEventEnrollRequest batchEventEnrollRequest) {

        batchEventEnrollRequest.getEnrollData().setEventId(eventId);

        try {
            eventEnrollMainService.batchUpdateEnroll(request.getSession(), batchEventEnrollRequest);
            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "活動報名表不存在"));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/batch_cancel", method = RequestMethod.POST)
    @ApiOperation(value = "取消報名*")
    public ResponseEntity batch_cancel(HttpServletRequest request, @PathVariable String eventId, @RequestBody CancelEnrollMainRequest cancelRequest) {

        try {
            eventEnrollMainService.batchCancel(request.getSession(), eventId, cancelRequest.getFormAutoAddNums());
            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (NotFoundException e) {
            return ResponseEntity.ok(new BaseResponse(400, "活動報名表不存在"));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/recover_cancel", method = RequestMethod.POST)
    @ApiOperation(value = "恢復報名*")
    public ResponseEntity recover_cancel(HttpServletRequest request, @PathVariable String eventId, @RequestBody CancelEnrollMainRequest cancelRequest) {

        try {
            eventEnrollMainService.batchRecover(request.getSession(), eventId, cancelRequest.getFormAutoAddNums());
            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (NotFoundException e) {
            return ResponseEntity.ok(new BaseResponse(400, "活動報名表不存在"));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/templates", method = RequestMethod.GET)
    @ApiOperation(value = "取得範本列表*")
    public ResponseEntity get_templates(HttpServletRequest request, @PathVariable String eventId) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<TemplateShort> items = eventUnitTemplateService.getTemplatesByEventIdUnitId(eventId, unitId, false);

        ListResponse<TemplateShort> response = new ListResponse<>(200, "success", items);

        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "/event_mains/{eventId}/templates", method = RequestMethod.POST)
    @ApiOperation(value = "新增範本*")
    public ResponseEntity new_template(HttpServletRequest request, @PathVariable String eventId, @RequestBody  EventEnrollRequest eventEnrollRequest) {
        eventEnrollRequest.setEventId(eventId);

        try {
            eventEnrollRequest.setEventId(eventId);
            eventUnitTemplateService.newTemplate(request.getSession(), eventEnrollRequest);
            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (RedundantException r) {
            return ResponseEntity.ok(new BaseResponse(400, "範本名稱不可重複:{" + eventEnrollRequest.getTemplateName() + "}"));
        } catch (UnacceptableException u) {
            return ResponseEntity.ok(new BaseResponse(400, "範本名稱空白。"));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/templates/{templateUniqueId}", method = RequestMethod.GET)
    @ApiOperation(value = "取得單筆範本*")
    public ResponseEntity get_template(@PathVariable String eventId, @PathVariable String templateUniqueId) {

        EventUnitTemplatePgmResponse response = eventUnitTemplateService.getTemplate(eventId, templateUniqueId);

        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "/event_mains/{eventId}/templates/{templateUniqueId}", method = RequestMethod.PUT)
    @ApiOperation(value = "更新範本*")
    public ResponseEntity edit_template(HttpServletRequest request, @PathVariable String eventId, @PathVariable String templateUniqueId, @RequestBody  EventEnrollRequest eventEnrollRequest) {

        eventEnrollRequest.setEventId(eventId);
        eventEnrollRequest.setTemplateUniqueId(templateUniqueId);

        try {
            eventUnitTemplateService.updateTemplate(request.getSession(), eventEnrollRequest);
            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "範本不存在。"));
        } catch (RedundantException r) {
            return ResponseEntity.ok(new BaseResponse(400, "範本名稱不可重複:{" + eventEnrollRequest.getTemplateName() + "}"));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/templates/{templateUniqueId}/apply_all", method = RequestMethod.POST)
    @ApiOperation(value = "重新套用範本*")
    public ResponseEntity apply_all(HttpServletRequest request, @PathVariable String eventId, @PathVariable String templateUniqueId) {


        try {
            int count = eventUnitTemplateService.reApplyTemplate(request.getSession(), eventId, templateUniqueId);
            return ResponseEntity.ok(new BaseResponse(200, "成功重新套用{" + count + "}張報名表"));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, n.getMessage()));
        }
    }

    @RequestMapping(value = "/get_members_class_list", method = RequestMethod.POST)
    @ApiOperation(value = "取得學員目前的班級列表*")
    public ResponseEntity get_home_class(HttpServletRequest request, @RequestBody MemberIds requestBody) {

        List<MemberShort> memberShortList = ctMemberInfoService.getCurrentClassList(requestBody.getMemberIds());

        return ResponseEntity.ok(new ListResponse<>(200, "success", memberShortList));
    }

    @RequestMapping(value = "/event_mains/{eventId}/not_enrolls", method = RequestMethod.GET)
    @ApiOperation(value = "取得未報名學員清單*")
    public ResponseEntity not_enrolls(HttpServletRequest request,
                                      @PathVariable String eventId,
                                      @RequestParam(required = false) String aliasName,
                                      @RequestParam(required = false) String classId) {

        try {
            List<MemberInfoNotEnrolled> notEnrolledList = eventEnrollMainService.getNotEnrolledMembers(request.getSession(), eventId, aliasName, classId);
            return ResponseEntity.ok(new ListResponse<>(200, "success", notEnrolledList));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "活動不存在"));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/not_enrolls", method = RequestMethod.POST)
    @ApiOperation(value = "透過excel篩選學員*")
    public ResponseEntity not_enrolls_excel(HttpServletRequest request, @PathVariable String eventId, @RequestParam("file") MultipartFile file) {

        try {
            eventEnrollMainService.checkEvent(eventId);

            String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

            List<ExcelMember> members = excelProcessService.processMemberFilterExcel(file);

            return ResponseEntity.ok(ctMemberInfoService.searchMembers(unitId, members));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "活動不存在"));
        }
    }

    @RequestMapping(value = "/member_infos", method = RequestMethod.GET)
    @ApiOperation(value = "取得同姓名學員列表*")
    public ResponseEntity member_infos(HttpServletRequest request, @RequestParam String name) {
        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<MemberInfoItem> items = ctMemberInfoService.findSameNameByUnit(unitId, name);

        return ResponseEntity.ok(new ListResponse<>(200, "success", items));
    }

    @RequestMapping(value = "/member_infos", method = RequestMethod.POST)
    @ApiOperation(value = "新增學員*")
    public ResponseEntity new_member(HttpServletRequest request, @RequestBody NewMemberRequest newMemberRequest) {

        CtMemberInfo memberInfo = ctMemberInfoService.newMember(newMemberRequest, request.getSession(), false);

        return ResponseEntity.ok(new ResultResponse<>(200, "success", memberInfo));
    }

    @RequestMapping(value = "/member_infos/{memberId}/summer_donation_records", method = RequestMethod.GET)
    @ApiOperation(value = "取得結夏發心列表*")
    public ResponseEntity get_summer_donation(@PathVariable String memberId) {

        List<EventSummerDonationRecord> items = eventSummerDonationService.getByMemberId(memberId);

        return ResponseEntity.ok(new ListResponse<>(200, "success", items));
    }

    @RequestMapping(value = "/member_infos/{memberId}/summer_donation_records", method = RequestMethod.PUT)
    @ApiOperation(value = "儲存本年結夏發心*")
    public ResponseEntity edit_summer_donation(HttpServletRequest request, @PathVariable String memberId, @RequestBody SummerDonationRequest summerDonationRequest) {
        summerDonationRequest.setEnrollUserId(memberId);

        eventSummerDonationService.addOrUpdateCurrentYear(request.getSession(), summerDonationRequest);

        return ResponseEntity.ok(new BaseResponse(200, "success"));
    }

    @RequestMapping(value = "/member_infos/{memberId}/summer_donation_records", method = RequestMethod.DELETE)
    @ApiOperation(value = "刪除本年度結夏發心*")
    public ResponseEntity del_summer_donation(@PathVariable String memberId) {

        eventSummerDonationService.delCurrentYear(memberId);

        return ResponseEntity.ok(new BaseResponse(200, "success"));
    }

    @RequestMapping(value = "/member_infos/{memberId}/basic", method = RequestMethod.GET)
    @ApiOperation(value = "取得學員基本資料*")
    public ResponseEntity get_member(HttpServletRequest request, @PathVariable String memberId) {

        MemberInfoNotEnrolled result = ctMemberInfoService.getMemberInfo4EventEnroll(memberId);

        return ResponseEntity.ok(new ResultResponse<>(200, "success", result));
    }

    @RequestMapping(value = "/member_infos/{memberId}/basic", method = RequestMethod.PUT)
    @ApiOperation(value = "修改學員基本資料*")
    public ResponseEntity edit_member(HttpServletRequest request, @PathVariable String memberId, @RequestBody MemberInfoNotEnrolled member) {

        member.setMemberId(memberId);

        try {
            ctMemberInfoService.updateMemberInfo4EventEnroll(request.getSession(), member);
            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "無學員資料"));
        } catch (UnacceptableException u) {
            return ResponseEntity.ok(new BaseResponse(400, "不是有效的中華名國身分證字號"));
        }

    }

    @RequestMapping(value = "/member_infos/{memberId}/urgent_contact_person", method = RequestMethod.PUT)
    @ApiOperation(value = "補緊急聯絡人資料*")
    public ResponseEntity edit_emg_contact(HttpServletRequest request, @PathVariable String memberId, @RequestBody UrgentContactRequest requestBody) {

        try {
            ctMemberInfoService.updateUrgentContact(request.getSession(), memberId, requestBody);
            return ResponseEntity.ok(new BaseResponse(200, "success"));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "member id 不存在"));
        }
    }
}
