package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import tw.org.ctworld.meditation.beans.Public.TrainingFileResponse;
import tw.org.ctworld.meditation.beans.SysCode.LoginContactResponse;
import tw.org.ctworld.meditation.libs.MultipartFileSender;
import tw.org.ctworld.meditation.models.SysCode;
import tw.org.ctworld.meditation.services.FileStorageService;
import tw.org.ctworld.meditation.services.SysCodeService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(path = "/api/public")
public class PublicController {
    private static final Logger logger = LoggerFactory.getLogger(PublicController.class);

    @Autowired
    private SysCodeService sysCodeService;

    @Autowired
    private FileStorageService fileStorageService;

    @RequestMapping(path = "/contact", method = RequestMethod.GET)
    public ResponseEntity contact(HttpServletRequest request) {

        logger.debug("/api/public/contact");

        SysCode sysCode = sysCodeService.getSysCodeByKey("class_contact_info_on_login_page");

        String text = "";

        if (sysCode != null) {
            text = sysCode.getCode_value();
        }

        return ResponseEntity.ok(new LoginContactResponse(0, "success", text));
    }

    @RequestMapping(path = "/files", method = RequestMethod.GET)
    public ResponseEntity files(HttpServletRequest request) {

        logger.debug("/api/public/files");

        return ResponseEntity.ok(new TrainingFileResponse(0, "success", fileStorageService.getTrainingFiles()));
    }

    @RequestMapping(path = "/load_video", method = RequestMethod.GET)
    public ResponseEntity load_video(HttpServletRequest request,
                                     HttpServletResponse response,
                                     @RequestParam("path") String path) {

        logger.debug("/api/public/load_video");

        try {
            Resource resource = fileStorageService.loadFile2Resource(path);
            MultipartFileSender.fromPath(resource.getFile().toPath())
                    .with(request)
                    .with(response)
                    .serveResource();
        } catch (Exception e) {
            logger.debug("read training video file error");
            e.printStackTrace();
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/load_file", method = RequestMethod.GET)
    public ResponseEntity load_file(HttpServletRequest request,
                                    HttpServletResponse response,
                                    @RequestParam("path") String path) {

        logger.debug("/api/public/load_file");

        try {
            Resource resource = fileStorageService.loadFile2Resource(path);
            StreamUtils.copy(resource.getInputStream(), response.getOutputStream());
        } catch (Exception e) {
            logger.debug("read training doc file error");
            e.printStackTrace();
        }

        return ResponseEntity.ok().build();
    }
}
