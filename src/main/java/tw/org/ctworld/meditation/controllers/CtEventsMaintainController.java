package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tw.org.ctworld.meditation.beans.CtEventMaintain.*;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.models.EventMain;
import tw.org.ctworld.meditation.services.CtEventsMaintainService;
import tw.org.ctworld.meditation.services.EventCategoryDefService;
import tw.org.ctworld.meditation.services.EventPgmDefService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/ct_events")
public class CtEventsMaintainController {

    private static final Logger logger = LoggerFactory.getLogger(CtEventsMaintainController.class);

    @Autowired
    private CtEventsMaintainService ctEventsMaintainService;


    // 取得 event_category_def 列表
    @RequestMapping(value = "/event_category_defs", method = RequestMethod.GET)
    public ResponseEntity ctEventMaintain001(HttpServletRequest request) {

        logger.debug("api/ct_events/event_category_defs(GET)");

        return ResponseEntity.ok(ctEventsMaintainService.getCategoryList(request.getSession()));
    }

    // 取得 event_pgm_def 列表
    @RequestMapping(value = "/event_pgm_defs", method = RequestMethod.GET)
    public ResponseEntity ctEventMaintain002(HttpServletRequest request) {

        logger.debug("api/ct_events/event_pgm_defs(GET)");

        return ResponseEntity.ok(ctEventsMaintainService.getPGMList(request.getSession()));
    }

    // 所有活動列表
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity ctEventMaintain003(HttpServletRequest request) {

        logger.debug("api/ct_events/list(GET)");

        return ResponseEntity.ok(ctEventsMaintainService.getCtEventMain(request.getSession()));
    }

    // 新增/複製活動 (如果 copyId有值，為複製模式)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity ctEventMaintain004(HttpServletRequest request,
                                             @RequestBody CtEventMaintain004Request ctEventMaintain004Request) {

        logger.debug("api/ct_events/list(POST)");

        try {
            return ResponseEntity.ok(ctEventsMaintainService
                    .addMainEvent(request.getSession(), ctEventMaintain004Request, "C"));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new CtEventMaintain004Response(400, "找不到event main id : " + ctEventMaintain004Request.getCopyId(), null));
        }
    }

    // 取得單取活動資料
    @RequestMapping(value = "/list/{eventId}", method = RequestMethod.GET)
    public ResponseEntity ctEventMaintain005(HttpServletRequest request, @PathVariable("eventId") String eventId) {

        logger.debug("api/ct_events/list/" + eventId + "(GET)");

        return ResponseEntity.ok(ctEventsMaintainService.getMainEventByEventId(request.getSession(), eventId));
    }

    // 修改單筆活動資料
    @RequestMapping(value = "/list/{eventId}", method = RequestMethod.PUT)
    public ResponseEntity ctEventMaintain006(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @RequestBody CtEventMaintain006Request ctEventMaintain006Request) {

        logger.debug("api/ct_events/list/" + eventId + "(PUT)");

        return ResponseEntity.ok(ctEventsMaintainService.editMainEventByEventId(
                request.getSession(), eventId, ctEventMaintain006Request, "C"));
    }

    // 刪除單筆活動資料
    @RequestMapping(value = "/list/{eventId}", method = RequestMethod.DELETE)
    public ResponseEntity ctEventMaintain007(HttpServletRequest request, @PathVariable("eventId") String eventId) {

        logger.debug("api/ct_events/list/" + eventId + "(DELETE)");

        return ResponseEntity.ok(ctEventsMaintainService.deleteMainEventByEventId(request.getSession(), eventId));
    }

    // 同步報名表到中台資料表btn
    @RequestMapping(value = "/list/{eventId}/sync_ct_table", method = RequestMethod.POST)
    public ResponseEntity ctEventMaintain008(HttpServletRequest request, @PathVariable("eventId") String eventId) {

        logger.debug("api/ct_events/list/" + eventId + "/sync_ct_table(POST)");

        return ResponseEntity.ok(ctEventsMaintainService.syncCtTable(request.getSession(), eventId));
    }

    // 刪除中台資料表的報名表btn
    @RequestMapping(value = "/list/{eventId}/sync_ct_table", method = RequestMethod.DELETE)
    public ResponseEntity ctEventMaintain009(HttpServletRequest request, @PathVariable("eventId") String eventId) {

        logger.debug("api/ct_events/list/" + eventId + "/sync_ct_table(DELETE)");

        return ResponseEntity.ok(ctEventsMaintainService.deleteSyncCtTable(request.getSession(), eventId));
    }

    // 匯入活動類別預定項目
    @RequestMapping(value = "/list/{eventId}/event_pgm_oca_import", method = RequestMethod.POST)
    public ResponseEntity ctEventMaintain010(HttpServletRequest request, @PathVariable("eventId") String eventId) {

        logger.debug("api/ct_events/list/" + eventId + "/event_pgm_oca_import(POST)");

        return ResponseEntity.ok(ctEventsMaintainService.importEventCategoryItem(request.getSession(), eventId));
    }

    // 取得活動pgm_oca列表
    @RequestMapping(value = "/list/{eventId}/event_pgm_ocas", method = RequestMethod.GET)
    public ResponseEntity ctEventMaintain011(HttpServletRequest request, @PathVariable("eventId") String eventId) {

        logger.debug("api/ct_events/list/" + eventId + "/event_pgm_ocas(GET)");

        return ResponseEntity.ok(ctEventsMaintainService.getEventPgmOcaList(request.getSession(), eventId));
    }

    // 批次新增pgm_ocas
    @RequestMapping(value = "/list/{eventId}/event_pgm_ocas", method = RequestMethod.POST)
    public ResponseEntity ctEventMaintain012(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @RequestBody CtEventMaintain012Request ctEventMaintain012Request) {

        logger.debug("api/ct_events/list/" + eventId + "/event_pgm_ocas(POST)");

        return ResponseEntity.ok(
                ctEventsMaintainService.addEventPgmOcaList(request.getSession(), eventId, ctEventMaintain012Request));
    }

    // 修改單筆event_pgm_oca
    @RequestMapping(value = "/list/{eventId}/event_pgm_ocas/{pgmUniqueId}", method = RequestMethod.PUT)
    public ResponseEntity ctEventMaintain013(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @PathVariable("pgmUniqueId") String pgmUniqueId,
                                             @RequestBody CtEventMaintain013Request ctEventMaintain013Request) {

        logger.debug("api/ct_events/list/" + eventId + "/event_pgm_ocas/" + pgmUniqueId + "(POST)");

        return ResponseEntity.ok(ctEventsMaintainService.editEventPgmOca(
                request.getSession(), eventId, pgmUniqueId, ctEventMaintain013Request));
    }

    // 刪除單筆event_pgm_oca
    @RequestMapping(value = "/list/{eventId}/event_pgm_ocas/{pgmUniqueId}", method = RequestMethod.DELETE)
    public ResponseEntity ctEventMaintain014(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @PathVariable("pgmUniqueId") String pgmUniqueId) {

        logger.debug("api/ct_events/list/" + eventId + "/event_pgm_ocas/" + pgmUniqueId + "(DELETE)");

        return ResponseEntity.ok(ctEventsMaintainService.deleteEventPgmOca(request.getSession(), eventId, pgmUniqueId));
    }

    // 取得特殊輸入項目下一筆排序碼
//    @RequestMapping(value = "/list/{eventId}/special_inputs_next_order_num", method = RequestMethod.POST)
//    public ResponseEntity ctEventMaintain015(HttpServletRequest request, @PathVariable("eventId") String eventId) {
//
//        logger.debug("api/ct_events/list/" + eventId + "/special_inputs_next_order_num(POST)");
//
//        return null;
//    }

    // 取得活動特殊輸入項目列表
    @RequestMapping(value = "/list/{eventId}/special_inputs", method = RequestMethod.GET)
    public ResponseEntity ctEventMaintain016(HttpServletRequest request, @PathVariable("eventId") String eventId) {

        logger.debug("api/ct_events/list/" + eventId + "/special_inputs(GET)");

        return ResponseEntity.ok(ctEventsMaintainService.getSpecialInputs(request.getSession(), eventId));
    }

    // 新增特殊輸入項目
    @RequestMapping(value = "/list/{eventId}/special_inputs", method = RequestMethod.POST)
    public ResponseEntity ctEventMaintain017(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @RequestBody CtEventMaintain017Request ctEventMaintain017Request) {

        logger.debug("api/ct_events/list/" + eventId + "/special_inputs(POST)");

        return ResponseEntity.ok(
                ctEventsMaintainService.addSpecialInput(request.getSession(), eventId, ctEventMaintain017Request));
    }

    // 修改單筆特殊輸入項目
    @RequestMapping(value = "/list/{eventId}/special_inputs/{sInputId}", method = RequestMethod.PUT)
    public ResponseEntity ctEventMaintain018(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @PathVariable("sInputId") String sInputId,
                                             @RequestBody CtEventMaintain017Request ctEventMaintain017Request) {

        logger.debug("api/ct_events/list/" + eventId + "/special_inputs/" + sInputId + "(PUT)");

        return ResponseEntity.ok(ctEventsMaintainService.editSpecialInput(
                request.getSession(), eventId, sInputId, ctEventMaintain017Request));
    }

    // 刪除單筆特殊輸入項目
    @RequestMapping(value = "/list/{eventId}/special_inputs/{sInputId}", method = RequestMethod.DELETE)
    public ResponseEntity ctEventMaintain019(HttpServletRequest request,
                                             @PathVariable("eventId") String eventId,
                                             @PathVariable("sInputId") String sInputId) {

        logger.debug("api/ct_events/list/" + eventId + "/special_inputs/" + sInputId + "(DELETE)");

        return ResponseEntity.ok(ctEventsMaintainService.deleteSpecialInput(request.getSession(), eventId, sInputId));
    }
}
