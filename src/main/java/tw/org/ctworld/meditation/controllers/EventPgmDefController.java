package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tw.org.ctworld.meditation.beans.EventPgmDef.EventPgmDef002Request;
import tw.org.ctworld.meditation.services.EventPgmDefService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/event_pgm_def")
public class EventPgmDefController {

    private static final Logger logger = LoggerFactory.getLogger(EventPgmDefController.class);

    @Autowired
    private EventPgmDefService eventPgmDefService;


    // 取得pgm_def列表
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity eventPgmDef001(HttpServletRequest request) {

        logger.debug("api/event_pgm_def/list(Get)");

        return ResponseEntity.ok(eventPgmDefService.getPGMList(request.getSession()));
    }

    // 新增pgm
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity eventPgmDef002(HttpServletRequest request, @RequestBody EventPgmDef002Request eventPgmDef002Request) {

        logger.debug("api/event_pgm_def/list(POST)");

        return ResponseEntity.ok(eventPgmDefService.addPGM(request.getSession(), eventPgmDef002Request));
    }

    // 編輯pgm
    @RequestMapping(value = "/list/{id}", method = RequestMethod.PUT)
    public ResponseEntity eventPgmDef003(HttpServletRequest request, @PathVariable("id") String id,
                                  @RequestBody EventPgmDef002Request eventPgmDef002Request) {

        logger.debug("api/event_pgm_def/list(PUT)");

        return ResponseEntity.ok(eventPgmDefService.editPGM(request.getSession(), id, eventPgmDef002Request));
    }

    // 刪除pgm
    @RequestMapping(value = "/list/{id}", method = RequestMethod.DELETE)
    public ResponseEntity eventPgmDef004(HttpServletRequest request, @PathVariable("id") String id) {

        logger.debug("api/event_pgm_def/list(DELETE)");

        return ResponseEntity.ok(eventPgmDefService.deletePGM(id));
    }
}
