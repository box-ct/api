package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.DebugResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Controller
public class CustomerErrorController implements ErrorController {

    private static final Logger logger = LoggerFactory.getLogger(CustomerErrorController.class);

    @Autowired
    private ErrorAttributes errorAttributes;

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @Value("${server.servlet.context-path}")
    private String ServerContext;

    @RequestMapping(value = "/error")
    public ResponseEntity error(HttpServletRequest request, WebRequest webRequest, HttpServletResponse response) {

        Map<String, Object> errorAtt = getErrorAttributes(webRequest, true);
        int status = response.getStatus();

        logger.debug("path: " + request.getPathTranslated() + " / error " + status + ": " + errorAtt.get("error") + ": " + errorAtt.get("message"));

        return ResponseEntity.ok(new DebugResponse(status, (String)errorAtt.get("error"), errorAtt.toString()));
    }

    private Map<String, Object> getErrorAttributes(WebRequest request, boolean includeStackTrace) {
        return errorAttributes.getErrorAttributes(request, includeStackTrace);
    }

}
