package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup001Response;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup002Response;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup003Request;
import tw.org.ctworld.meditation.beans.ClassGroup.ClassGroup004Request;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.services.ClassInfoService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/class_group")
public class ClassGroupController {

    private static final Logger logger = LoggerFactory.getLogger(ClassGroupController.class);

    @Autowired
    private ClassEnrollFormService classEnrollFormService;

    @Autowired
    private ClassInfoService classInfoService;


    // 取得開課中的班別
    @RequestMapping(value = "/class_infos", method = RequestMethod.GET)
    public ResponseEntity classGroup001(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new ClassGroup001Response(200, "success",
                classInfoService.getClassByUnitId(unitId)));
    }

    // 依classId取得學員報名表
    @RequestMapping(value = "/enroll_forms", method = RequestMethod.GET)
    public ResponseEntity classGroup002(HttpServletRequest request,
                                        @RequestParam(value = "classId") String classId) {

        return ResponseEntity.ok(new ClassGroup002Response(200, "success",
                classEnrollFormService.getMemberListByClassId(classId)));
    }

    // 更新組別
    @RequestMapping(value = "/change_group", method = RequestMethod.POST)
    public ResponseEntity classGroup003(HttpServletRequest request,
                                        @RequestBody ClassGroup003Request classGroup003Request) {

        return ResponseEntity.ok(classEnrollFormService.changeClassGroupId(classGroup003Request));
    }

    // 移出組別, 將classGroupId清空
    @RequestMapping(value = "/clear_group", method = RequestMethod.POST)
    public ResponseEntity classGroup004(HttpServletRequest request,
                                        @RequestBody ClassGroup004Request classGroup004Request) {

        return ResponseEntity.ok(classEnrollFormService.clearClassGroupId(classGroup004Request));
    }
}
