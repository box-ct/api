package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tw.org.ctworld.meditation.beans.Anonymous.*;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.MultipartFileSender;
import tw.org.ctworld.meditation.models.UserInfo;
import tw.org.ctworld.meditation.models.UnitInfo;
import tw.org.ctworld.meditation.repositories.UnitInfoRepo;
import tw.org.ctworld.meditation.services.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/api/anonymous")
public class AnonymousController {

    private static final Logger logger = LoggerFactory.getLogger(AnonymousController.class);

    @Autowired
    private UnitInfoService unitInfoService;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;

    @Autowired
    private ClassDateInfoService classDateInfoService;

    @Autowired
    private ClassAttendRecordService classAttendRecordService;

    @Autowired
    private ClassEnrollFormService classEnrollFormService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UnitInfoRepo unitInfoRepo;

    @Autowired
    private ClassMakeupRecordService classMakeupRecordService;

    @Autowired
    private FileStorageService fileStorageService;


    // 依ip取得當前精舍
    @RequestMapping(value = "/unit", method = RequestMethod.GET)
    public ResponseEntity anonymous001(HttpServletRequest request) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));
        String unitName = String.valueOf(session.getAttribute(UserSession.Keys.UnitName2.getValue()));

        int errCode = 200;
        String errMsg = "success";

        if (unitId.equals("null")) {
            errCode = 400;
            errMsg = "ip無對應單位";
        }

        return ResponseEntity.ok(new Anonymous001Response(errCode, errMsg, unitId, unitName));
    }

    // AD驗證
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity anonymous002(HttpServletRequest request,
                                       @RequestBody Anonymous002Request anonymous002Request) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        int errCode = 200;
        String errMsg = "success";
        List<Anonymous002Object> loginInfo = new ArrayList<>();
        UserInfo loginUserInfo = null;

        if (unitId.equals("null")) {
            errCode = 400;
            errMsg = "ip無對應單位";
        }else {

            String userId = anonymous002Request.getUsername();
            String password = anonymous002Request.getPassword();

            UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(userId, password);

            try {

                authenticationManager.authenticate(authReq);

                loginUserInfo = userInfoService.findLoginUserInfo(userId, unitId);
            }catch (Exception e) {
                errCode = 401;
                errMsg = "username and/or password not matched";
            }

            if (loginUserInfo != null) {

                if (loginUserInfo.getIsDisabled()) {
                    errCode = 402;
                    errMsg = "account disabled";
                }

                UnitInfo unitInfo = unitInfoRepo.GetClassUnitInfoByUnitId(unitId);

                if (unitInfo == null || !loginUserInfo.getUnitId().equals(unitId)) {
                    errCode = 404;
                    errMsg = "unit not found";
                }
            }else {
                errCode = 400;
                errMsg = "no privilege found";
            }
        }

        return ResponseEntity.ok(new BaseResponse(errCode, errMsg));

//        if (errCode == 200) {
//
//            String updatorId = loginUserInfo.getUserId();
//            String updatorName = loginUserInfo.getUserName();
//            String updatorUnitName = String.valueOf(session.getAttribute(UserSession.Keys.UnitName2.getValue()));
//
//            loginInfo.add(new Anonymous002Object(
//                    updatorId,
//                    updatorName,
//                    updatorUnitName
//            ));
//        }
//        return ResponseEntity.ok(new Anonymous002Response(errCode, errMsg, loginInfo));
    }

    // 二重驗證學員資料
    @RequestMapping(value = "/member_verify", method = RequestMethod.POST)
    public ResponseEntity anonymous003(HttpServletRequest request,
                                       @RequestBody Anonymous003Request anonymous003Request) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        int errCode = 200;
        String errMsg = "success";

        BaseResponse response;

        if (unitId.equals("null")) {
            errCode = 400;
            errMsg = "ip無對應單位";
        }else {
            response = ctMemberInfoService.getMemberCtInfoDoubleCheck(anonymous003Request);
            errCode = response.getErrCode();
            errMsg = response.getErrMsg();
        }

        return ResponseEntity.ok(new BaseResponse(errCode, errMsg));
    }

    // 依卡號取得學員資料
    @RequestMapping(value = "/members/{memberId}", method = RequestMethod.GET)
    public ResponseEntity anonymous004(HttpServletRequest request, @PathVariable("memberId") String memberId) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        int errCode = 200;
        String errMsg = "success";
        String photo = "";
        String aliasName = "";
        String ctDharmaName = "";

        if (unitId.equals("null")) {
            errCode = 400;
            errMsg = "ip無對應單位";
        }else {
            Anonymous004Response anonymous004Response = ctMemberInfoService.getMemberCtInfoByMemberId(unitId, memberId);

            if (anonymous004Response.getErrCode() == 200) {
                photo = anonymous004Response.getPhoto();
                aliasName = anonymous004Response.getAliasName();
                ctDharmaName = anonymous004Response.getCtDharmaName();
            }else {
                errCode = anonymous004Response.getErrCode();
                errMsg = anonymous004Response.getErrMsg();
            }
        }

        return ResponseEntity.ok(new Anonymous004Response(errCode, errMsg, photo, aliasName, ctDharmaName));
    }

    // 取得學員列表
    @RequestMapping(value = "/members", method = RequestMethod.GET)
    public ResponseEntity anonymous005(HttpServletRequest request, @RequestParam("q") String keyword) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        int errCode = 200;
        String errMsg = "success";

        List<Anonymous005_2Object> items = new ArrayList<>();

        if (unitId.equals("null")) {
            errCode = 400;
            errMsg = "ip無對應單位";
        }else {
            items = ctMemberInfoService.getMemberInfoLikeKeyword(unitId, keyword);
        }

        return ResponseEntity.ok(new Anonymous005Response(errCode, errMsg, items));
    }

    // 取得報到班別列表(依「開課時間最接近」排序)
    @RequestMapping(value = "/check_in/classes", method = RequestMethod.GET)
    public ResponseEntity anonymous006(HttpServletRequest request) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        int errCode = 200;
        String errMsg = "success";

        List<Anonymous006_1Object> items = new ArrayList<>();

        if (unitId.equals("null")) {
            errCode = 400;
            errMsg = "ip無對應單位";
        }else {
            items = classDateInfoService.getClassByNow(session);
        }

        return ResponseEntity.ok(new Anonymous006Response(errCode, errMsg, items));
    }

    // 取得已報到 & 未報到學員列表
    @RequestMapping(value = "/check_in/members", method = RequestMethod.GET)
    public ResponseEntity anonymous007(HttpServletRequest request,
                                       @RequestParam("classId") String classId,
                                       @RequestParam("classWeeksNum") int classWeeksNum) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        int errCode = 200;
        String errMsg = "success";

        List<Anonymous007Object> items = new ArrayList<>();

        if (unitId.equals("null")) {
            errCode = 400;
            errMsg = "ip無對應單位";
        }else {
            items = classAttendRecordService.getMemberAttendByClassIdAndClassWeeksNum(classId, classWeeksNum);
        }

        return ResponseEntity.ok(new Anonymous007Response(errCode, errMsg, items));
    }

    // 學員報到(不經過登入)
    @RequestMapping(value = "/check_in/attend", method = RequestMethod.POST)
    public ResponseEntity anonymous008(HttpServletRequest request,
                                       @RequestBody Anonymous008Request anonymous008Request) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        int errCode = 200;
        String errMsg = "success";

        Anonymous008Response anonymous008Response;

        if (unitId.equals("null")) {

            errCode = 400;
            errMsg = "ip無對應單位";

            anonymous008Response = new Anonymous008Response(errCode, errMsg, 0, "");
        }else {
            anonymous008Response = classAttendRecordService.getAttendRecordIdAndEnrollment(session, anonymous008Request);
        }

        return ResponseEntity.ok(anonymous008Response);
    }

    // 取得學員班級列表
    @RequestMapping(value = "/take_off/classes", method = RequestMethod.GET)
    public ResponseEntity anonymous009(HttpServletRequest request, @RequestParam("memberId") String memberId) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        int errCode = 200;
        String errMsg = "success";

        List<Anonymous009_1Object> items = new ArrayList<>();

        if (unitId.equals("null")) {
            errCode = 400;
            errMsg = "ip無對應單位";
        }else {
            items = classEnrollFormService.getMemberClassListByMemberId(session, memberId);
        }

        return ResponseEntity.ok(new Anonymous009Response(errCode, errMsg, items));
    }

    // 請假
    @RequestMapping(value = "/take_off/add", method = RequestMethod.POST)
    public ResponseEntity anonymous010(HttpServletRequest request,
                                       @RequestBody Anonymous010Request anonymous010Request) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        int errCode = 200;
        String errMsg = "success";

        if (unitId.equals("null")) {
            errCode = 400;
            errMsg = "ip無對應單位";
        }else {

            String result = classAttendRecordService.takeOff(session, anonymous010Request);

            if (!result.equals("success")) {

                errCode = 400;
                errMsg = result;
            }
        }

        return ResponseEntity.ok(new BaseResponse(errCode, errMsg));
    }

    // 取得學員班級列表，詳細點開
    @RequestMapping(value = "/makeup/classes", method = RequestMethod.GET)
    public ResponseEntity anonymous011(HttpServletRequest request, @RequestParam("memberId") String memberId) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        int errCode = 200;
        String errMsg = "success";

        List<AnonymousMakeupClass> items = new ArrayList<>();

        if (unitId.equals("null")) {
            errCode = 400;
            errMsg = "ip無對應單位";
        } else {
            items = classEnrollFormService.getClassEnrollFormListByUnitIdAndId(unitId, memberId);

            if (items.size() == 0) {
                errCode = 400;
                errMsg = "查無資料";
            }
        }

        return ResponseEntity.ok(new Anonymous011Response(errCode, errMsg, items));
    }


    // serving makeup video & audio
    @RequestMapping(path = "/makeup/video/{makeupFileName}")
    public void serveMakeupFile(HttpServletRequest request, HttpServletResponse response,
                      @PathVariable(value = "makeupFileName") String makeupFileName) {

        logger.debug(makeupFileName);

        try {
            Resource resource = fileStorageService.loadFile(1, makeupFileName);
            MultipartFileSender.fromPath(resource.getFile().toPath())
                    .with(request)
                    .with(response)
                    .serveResource();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    // 設定影片時間軸的時間
    @RequestMapping(value = "/makeup/video", method = RequestMethod.POST)
    public ResponseEntity anonymous012(HttpServletRequest request, @RequestBody AnonymousMakeupRequest requestData) {

        String result = classMakeupRecordService.CreateOrUpdateMakeupRecord(requestData, request.getSession());

        return ResponseEntity.ok(new BaseResponse(result == null ? 0 : 400, result == null ? "success" : result));
    }

    // 取消報到
    @RequestMapping(value = "/check_in/cancel", method = RequestMethod.POST)
    public ResponseEntity anonymous013(HttpServletRequest request, @RequestBody Anonymous013Request anonymous013Request) {

        HttpSession session = unitInfoService.getCurrentlyIp(request);

        String unitId = String.valueOf(session.getAttribute(UserSession.Keys.UnitId2.getValue()));

        BaseResponse baseResponse;

        int errCode = 200;
        String errMsg = "success";

        if (unitId.equals("null")) {

            errCode = 400;
            errMsg = "ip無對應單位";

            baseResponse = new BaseResponse(errCode, errMsg);
        }else {
            baseResponse = classAttendRecordService.deleteEnrollment(session, anonymous013Request);
        }

        return ResponseEntity.ok(baseResponse);
    }
}
