package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp001Response;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp004Request;
import tw.org.ctworld.meditation.beans.UnitStartUp.UnitStartUp006Request;
import tw.org.ctworld.meditation.services.UnitStartUpService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/unit_startup")
public class UnitStartUpController {

    private static final Logger logger = LoggerFactory.getLogger(UnitStartUpController.class);

    @Autowired
    private UnitStartUpService unitStartUpService;


    // 取得「複製最後一次有值的群組資料」
    @RequestMapping(value = "/list/{eventId}/get_last_group_name_list", method = RequestMethod.GET)
    public ResponseEntity unitStartUp001(HttpServletRequest request, @PathVariable String eventId) {

        logger.debug("api/unit_startup/get_last_group_name_list(GET)");

        return ResponseEntity.ok(unitStartUpService.getLastEventEnrollGroupList(request.getSession(), eventId));
    }

    // 取得精舍啟動活動設定列表
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity unitStartUp002(HttpServletRequest request) {

        logger.debug("api/unit_startup/list(GET)");

        return ResponseEntity.ok(unitStartUpService.getList(request.getSession()));
    }

    // 取得精舍啟動活動單筆資料
    @RequestMapping(value = "/list/{eventId}", method = RequestMethod.GET)
    public ResponseEntity unitStartUp003(HttpServletRequest request, @PathVariable String eventId) {

        logger.debug("api/unit_startup/list/" + eventId + "(GET)");

        return ResponseEntity.ok(unitStartUpService.getDetail(request.getSession(), eventId));
    }

    // 更新單筆資料
    @RequestMapping(value = "/list/{eventId}", method = RequestMethod.PUT)
    public ResponseEntity unitStartUp004(HttpServletRequest request, @PathVariable String eventId,
                                         @RequestBody UnitStartUp004Request unitStartUp004Request) {

        logger.debug("api/unit_startup/list/" + eventId + "(PUT)");

        return ResponseEntity.ok(unitStartUpService.putDetail(request.getSession(), eventId, unitStartUp004Request));
    }

    // 取得法師資訊
    @RequestMapping(value = "/list/{eventId}/unit_master_infos", method = RequestMethod.GET)
    public ResponseEntity unitStartUp005(HttpServletRequest request, @PathVariable String eventId) {

        logger.debug("api/unit_startup/list/" + eventId + "/unit_master_infos(GET)");

        return ResponseEntity.ok(unitStartUpService.getUnitMasterInfo(request.getSession(), eventId));
    }

    // 新增單位法師 未完成
    @RequestMapping(value = "/list/{eventId}/unit_master_infos", method = RequestMethod.POST)
    public ResponseEntity unitStartUp006(HttpServletRequest request, @PathVariable String eventId,
                                         @RequestBody UnitStartUp006Request unitStartUp006Request) {

        logger.debug("api/unit_startup/list/" + eventId + "/unit_master_infos(POST)");

        return ResponseEntity.ok(
                unitStartUpService.addUnitMasterInfo(request.getSession(), eventId, unitStartUp006Request));
    }

    // 取得已報名的學員資料
    @RequestMapping(value = "/list/{eventId}/event_enroll_main", method = RequestMethod.GET)
    public ResponseEntity unitStartUp007(HttpServletRequest request, @PathVariable String eventId) {

        logger.debug("api/unit_startup/list/" + eventId + "/event_enroll_main(GET)");

        return ResponseEntity.ok(unitStartUpService.getEventEnrollMain(request.getSession(), eventId));
    }
}
