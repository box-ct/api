package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.SysCode.SysCodeRequest;
import tw.org.ctworld.meditation.beans.SysCode.SysCodeResponse;
import tw.org.ctworld.meditation.models.SysCode;
import tw.org.ctworld.meditation.services.SysCodeService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/sys_code")
public class SysCodeController {
    private static final Logger logger = LoggerFactory.getLogger(SysCodeController.class);

    @Autowired
    private SysCodeService sysCodeService;

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public ResponseEntity get(HttpServletRequest request) {

        logger.debug("/api/sys_code/list get");

        return ResponseEntity.ok(new SysCodeResponse(0, "success", sysCodeService.getAllSysCode()));
    }

    @RequestMapping(path = "/list", method = RequestMethod.POST)
    public ResponseEntity post(HttpServletRequest request, @RequestBody SysCodeRequest code) {

        logger.debug("/api/sys_code/list post");

        if (sysCodeService.getSysCodeByKey(code.getCode_key()) == null) {

            sysCodeService.addSysCode(code.getCode_desc(), code.getCode_key(), code.getCode_value(), code.getCode_category(), request.getSession());

            return ResponseEntity.ok(new BaseResponse(0, "success"));
        } else {
            return ResponseEntity.ok(new BaseResponse(400, "key exists"));
        }
    }

    @RequestMapping(path = "/list/{code_key}", method = RequestMethod.PUT)
    public ResponseEntity put(HttpServletRequest request,
                              @PathVariable("code_key") String code_key,
                              @RequestBody SysCodeRequest code) {

        logger.debug("/api/sys_code/list put");

        SysCode sysCode = sysCodeService.getSysCodeByKey(code_key);

        if (sysCode != null) {
            sysCode.setCode_desc(code.getCode_desc());
            sysCode.setCode_value(code.getCode_value());
            sysCode.setCode_category(code.getCode_category());

            sysCode.setUpdater(request.getSession());

            sysCodeService.saveSysCode(sysCode);

            return ResponseEntity.ok(new BaseResponse(0, "success"));
        } else {
            return ResponseEntity.ok(new BaseResponse(400, "key not found"));
        }
    }

    @RequestMapping(path = "/list/{code_key}", method = RequestMethod.DELETE)
    public ResponseEntity delete(HttpServletRequest request,
                                 @PathVariable("code_key") String code_key) {

        logger.debug("/api/sys_code/list delete");

        SysCode sysCode = sysCodeService.getSysCodeByKey(code_key);

        if (sysCode != null) {
            sysCodeService.deleteSysCode(sysCode);

            return ResponseEntity.ok(new BaseResponse(0, "success"));
        } else {
            return ResponseEntity.ok(new BaseResponse(400, "key not found"));
        }
    }
}
