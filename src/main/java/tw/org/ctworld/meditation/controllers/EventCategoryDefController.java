package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef003Request;
import tw.org.ctworld.meditation.beans.EventCategoryDef.EventCategoryDef008Request;
import tw.org.ctworld.meditation.services.EventCategoryDefService;
import tw.org.ctworld.meditation.services.EventPgmDefService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/event_category_def")
public class EventCategoryDefController {

    private static final Logger logger = LoggerFactory.getLogger(EventCategoryDefController.class);

    @Autowired
    private EventCategoryDefService eventCategoryDefService;

    @Autowired
    private EventPgmDefService eventPgmDefService;


    // 取得 event_pgm_def 列表，同 /event_pgm_def/list
    @RequestMapping(value = "/event_pgm_defs", method = RequestMethod.GET)
    public ResponseEntity eventCategoryDef001(HttpServletRequest request) {

        logger.debug("api/event_category_def/event_pgm_defs");

        return ResponseEntity.ok(eventPgmDefService.getPGMList(request.getSession()));
    }

    // 取得category_def列表
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity eventCategoryDef002(HttpServletRequest request) {

        logger.debug("api/event_category_def/list(GET)");

        return ResponseEntity.ok(eventCategoryDefService.getCategoryList(request.getSession()));
    }

    // 新增category_def
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity eventCategoryDef003(HttpServletRequest request, @RequestBody EventCategoryDef003Request eventCategoryDef003Request) {

        logger.debug("api/event_category_def/list(POST)");

        return ResponseEntity.ok(eventCategoryDefService.addCategory(request.getSession(), eventCategoryDef003Request));
    }

    // 取得 event_category_def 的詳細資料
    @RequestMapping(value = "/list/{eventCategoryId}", method = RequestMethod.GET)
    public ResponseEntity eventCategoryDef004(HttpServletRequest request,
                                              @PathVariable("eventCategoryId") String eventCategoryId) {

        logger.debug("api/event_category_def/list/" + eventCategoryId + "(GET)");

        return ResponseEntity.ok(eventCategoryDefService.getCategory(request.getSession(), eventCategoryId));
    }

    // 編輯
    @RequestMapping(value = "/list/{eventCategoryId}", method = RequestMethod.PUT)
    public ResponseEntity eventCategoryDef005(HttpServletRequest request,
                                              @PathVariable("eventCategoryId") String eventCategoryId,
                                              @RequestBody EventCategoryDef003Request eventCategoryDef003Request) {

        logger.debug("api/event_category_def/list/" + eventCategoryId + "(PUT)");

        return ResponseEntity.ok(eventCategoryDefService.editCategory(
                request.getSession(), eventCategoryId, eventCategoryDef003Request));
    }

    // 刪除
    @RequestMapping(value = "/list/{eventCategoryId}", method = RequestMethod.DELETE)
    public ResponseEntity eventCategoryDef006(HttpServletRequest request,
                                              @PathVariable("eventCategoryId") String eventCategoryId) {

        logger.debug("api/event_category_def/list/" + eventCategoryId + "(DELETE)");

        return ResponseEntity.ok(eventCategoryDefService.deleteCategory(request.getSession(), eventCategoryId));
    }

    // 依eventCategoryId取得event_category_specified_pgm_record中的pgm_def
    @RequestMapping(value = "/list/{eventCategoryId}/event_pgm_defs", method = RequestMethod.GET)
    public ResponseEntity eventCategoryDef007(HttpServletRequest request,
                                         @PathVariable("eventCategoryId") String eventCategoryId) {

        logger.debug("api/event_category_def/list/" + eventCategoryId + "/event_pgm_defs(GET)");

        return ResponseEntity.ok(eventCategoryDefService.getCategorySpecified(request.getSession(), eventCategoryId));
    }

    // 新增pgm_def到pgm_category中 ( api 只取pgmId 應該就夠了)
    @RequestMapping(value = "/list/{eventCategoryId}/event_pgm_defs", method = RequestMethod.POST)
    public ResponseEntity eventCategoryDef008(HttpServletRequest request,
                                         @PathVariable("eventCategoryId") String eventCategoryId,
                                         @RequestBody EventCategoryDef008Request eventCategoryDef008Request) {

        logger.debug("api/event_category_def/list/" + eventCategoryId + "/event_pgm_defs(POST)");

        return ResponseEntity.ok(eventCategoryDefService.editCategorySpecified(
                request.getSession(), eventCategoryId, eventCategoryDef008Request));
    }

    // 依eventCategoryId取得event_category_specified_pgm_record中的pgm_def
    @RequestMapping(value = "/list/{eventCategoryId}/event_pgm_defs/{id}", method = RequestMethod.DELETE)
    public ResponseEntity eventCategoryDef009(HttpServletRequest request,
                                              @PathVariable("eventCategoryId") String eventCategoryId,
                                              @PathVariable("id") String id) {

        logger.debug("api/event_category_def/list/" + eventCategoryId + "/event_pgm_defs/" + id + "(DELETE)");

        return ResponseEntity.ok(eventCategoryDefService.deleteCategorySpecified(
                request.getSession(), eventCategoryId, id));
    }
}
