package tw.org.ctworld.meditation.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.EventEnrollMember.ExcelMember;
import tw.org.ctworld.meditation.beans.EventEnrollMember.MemberInfoNotEnrolled;
import tw.org.ctworld.meditation.beans.MemberInfos.MemberInfoItem;
import tw.org.ctworld.meditation.beans.MemberInfos.NewMemberRequest;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain002Object;
import tw.org.ctworld.meditation.beans.UnitEventMaintain.UnitEventsMaintain002Response;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.libs.UnacceptableException;
import tw.org.ctworld.meditation.models.CtMemberInfo;
import tw.org.ctworld.meditation.services.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(path = "/api/event_ct_enroll_member")
@Api(description = "15-活動報名(學員本山用)")
public class EventCtEnrollMemberController {

    private static final Logger logger = LoggerFactory.getLogger(EventCtEnrollMemberController.class);


    @Autowired
    private EventSummerDonationService eventSummerDonationService;

    @Autowired
    private EventMainService eventMainService;

    @Autowired
    private EventEnrollMainService eventEnrollMainService;

    @Autowired
    private CtMemberInfoService ctMemberInfoService;

    @Autowired
    private ExcelProcessService excelProcessService;


    @RequestMapping(value = "/event_mains", method = RequestMethod.GET)
    @ApiOperation(value = "取得活動列表*")
    public ResponseEntity get_events() {

        List<UnitEventsMaintain002Object> items = eventMainService.getEventsByAttendUnit(null);

        return ResponseEntity.ok(new UnitEventsMaintain002Response(200, "success", items));
    }

    @RequestMapping(value = "/event_mains/{eventId}/not_enrolls", method = RequestMethod.GET)
    @ApiOperation(value = "取得未報名學員清單*")
    public ResponseEntity get_not_enrolled(
            @PathVariable String eventId,
            @RequestParam(required = false) String unitId,
            @RequestParam(required = false) String name,
            @RequestParam int page,
            @RequestParam int pageSize,
            @RequestParam(required = false) String keyword,
            @RequestParam(required = false) String sortKey,
            @RequestParam(required = false) Integer isDesc) {

        try {
            PageResponse<MemberInfoNotEnrolled> response = eventEnrollMainService.getNotEnrolledMembers4Ct(
                    eventId, unitId, name, page, pageSize, keyword, sortKey, isDesc);

            return ResponseEntity.ok(response);
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "活動不存在"));
        } catch (UnacceptableException u) {
            return ResponseEntity.ok(new BaseResponse(400, "搜尋參數有誤"));
        }
    }

    @RequestMapping(value = "/event_mains/{eventId}/not_enrolls", method = RequestMethod.POST)
    @ApiOperation(value = "透過 excel 歸選學員")
    public ResponseEntity get_not_enrolled_excel(@PathVariable String eventId,
                                                 @RequestParam("file") MultipartFile file) {

        try {
            eventEnrollMainService.checkEvent(eventId);

            List<ExcelMember> members = excelProcessService.processMemberFilterExcel(file);

            return ResponseEntity.ok(ctMemberInfoService.searchMembers(null, members));
        } catch (NotFoundException n) {
            return ResponseEntity.ok(new BaseResponse(400, "活動不存在"));
        }
    }

    @RequestMapping(value = "/event_mains/member_infos", method = RequestMethod.GET)
    @ApiOperation(value = "取得同姓名學員列表*")
    public ResponseEntity get_same_name(@RequestParam(required = false) String unitId,
                                        @RequestParam String name) {

        List<MemberInfoItem> items = ctMemberInfoService.findSameNameByUnit(unitId, name);

        return ResponseEntity.ok(new ListResponse<>(200, "success", items));
    }

    @RequestMapping(value = "/event_mains/member_infos", method = RequestMethod.POST)
    @ApiOperation(value = "新增學員*")
    public ResponseEntity add_member(HttpServletRequest request, @RequestBody NewMemberRequest newMemberRequest) {

        CtMemberInfo memberInfo = ctMemberInfoService.newMember(newMemberRequest, request.getSession(), true);

        return ResponseEntity.ok(new ResultResponse<>(200, "success", memberInfo));
    }

    @RequestMapping(value = "/member_infos/{memberId}/summer_donation_records", method = RequestMethod.DELETE)
    @ApiOperation(value = "刪除本年度結夏發心*")
    public ResponseEntity del_summer_donation(@PathVariable String memberId) {

        eventSummerDonationService.delCurrentYear(memberId);

        return ResponseEntity.ok(new BaseResponse(200, "success"));
    }

}
