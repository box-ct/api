package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.Printer.*;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.services.ClassPrinterService;
import tw.org.ctworld.meditation.views.DocumentHorizontallyWordViewNew;
import tw.org.ctworld.meditation.views.DocumentVerticallyWordViewNew;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Controller
@RequestMapping(path = "/api/printer")
public class PrinterController {

    private static final Logger logger = LoggerFactory.getLogger(PrinterController.class);

    @Autowired
    private ClassPrinterService classPrinterService;

    @Autowired
    private ClassEnrollFormService classEnrollFormService;

    @Autowired
    private DocumentHorizontallyWordViewNew documentHorizontallyWordViewNew;

    @Autowired
    private DocumentVerticallyWordViewNew documentVerticallyWordViewNew;


    // 取得印表機列表
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity printer001(HttpServletRequest request) {

        return ResponseEntity.ok(classPrinterService.getPrinterList(request.getSession()));
    }

    // 新增印表機資料
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity printer002(HttpServletRequest request, @RequestBody Printer002Request printer002Request) {

        return ResponseEntity.ok(classPrinterService.addPrinter(request.getSession(), printer002Request));
    }

    // 修改單筆印表機資料
    @RequestMapping(value = "/list/{printerId}", method = RequestMethod.PUT)
    public ResponseEntity printer003(HttpServletRequest request,
                                     @PathVariable("printerId") String printerId,
                                     @RequestBody Printer002Request printer002Request) {

        return ResponseEntity.ok(classPrinterService.editPrinter(request.getSession(), printer002Request, printerId));
    }

    // 取得單筆印表機資料
    @RequestMapping(value = "/list/{printerId}", method = RequestMethod.GET)
    public ResponseEntity printer004(@PathVariable("printerId") String printerId) {

        return ResponseEntity.ok(classPrinterService.getPrinterInfo(printerId));
    }

    // 取得印表機, 某個證書的版型列表
    @RequestMapping(value = "/cert_infos", method = RequestMethod.GET)
    public ResponseEntity printer005(@RequestParam("certTypeName") String certTypeName,
                                     @RequestParam("printerId") String printerId) {

        return ResponseEntity.ok(classPrinterService.getCertInfoList(certTypeName, printerId));
    }

    // 新增版型
    @RequestMapping(value = "/cert_infos", method = RequestMethod.POST)
    public ResponseEntity printer006(HttpServletRequest request, @RequestBody Printer006Request printer006Request) {

        return ResponseEntity.ok(classPrinterService.addCertType(request.getSession(), printer006Request));
    }

    // 取得單筆板型資料
    @RequestMapping(value = "/cert_infos/{certTemplateId}", method = RequestMethod.GET)
    public ResponseEntity printer007(@PathVariable("certTemplateId") String certTemplateId) {

        return ResponseEntity.ok(classPrinterService.getCertFieldSetting(certTemplateId));
    }

    // 刪除樣版
    @RequestMapping(value = "/cert_infos/{certTemplateId}", method = RequestMethod.DELETE)
    public ResponseEntity printer008(@PathVariable("certTemplateId") String certTemplateId) {

        return ResponseEntity.ok(classPrinterService.deleteCertFieldSetting(certTemplateId));
    }

    // 更新板型
    @RequestMapping(value = "/cert_infos/{certTemplateId}", method = RequestMethod.PUT)
    public ResponseEntity printer009(HttpServletRequest request,
                                     @PathVariable("certTemplateId") String certTemplateId,
                                     @RequestBody Printer009Request printer009Request) {

        return ResponseEntity.ok(
                classPrinterService.editCertFieldSetting(request.getSession(), certTemplateId, printer009Request));
    }

    // 下載列印檔
    @RequestMapping(value = "/cert_infos/{certTemplateId}/export", method = RequestMethod.POST)
    public ResponseEntity printer010(HttpServletRequest request,
                                     HttpServletResponse response,
                                     @PathVariable("certTemplateId") String certTemplateId,
                                     @RequestBody Printer010Request printer010Request) {

        Printer010Response printer010Response =
                classPrinterService.exportCert(request.getSession(), certTemplateId, printer010Request);

        int errCode = 200;
        String errMsg = "success";

        if (printer010Response.getErrCode() != 200) {

            errCode = printer010Response.getErrCode();
            errMsg = printer010Response.getErrMsg();
            return ResponseEntity.ok(new BaseResponse(errCode, errMsg));
        }else {

            List<Printer007Object> classCertFieldSettingList = printer010Response.getClassCertFieldSettingList();
            List<Printer011Object> memberNameAndAgeList = printer010Response.getMemberNameAndAgeList();
            String certContent = printer010Response.getCertContent();
            String certMaster = printer010Response.getCertMaster();
            String certDate = printer010Response.getCertDate();
            String certFileName = printer010Response.getCertFileName();
            String certTypeName = printer010Response.getCertTypeName();

            try {
                certFileName = URLEncoder.encode(certFileName, StandardCharsets.UTF_8.name()).replace("+", "%20");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            response.setContentType("application/msword; charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + certFileName);

            try {
                switch (certTypeName) {
                    case "中文結業證書":
                        documentHorizontallyWordViewNew.generateDocument(
                                1, classCertFieldSettingList, memberNameAndAgeList, certContent, certMaster, certDate)
                                .write(response.getOutputStream());
                        break;
                    case "中文全勤證書":
                        documentHorizontallyWordViewNew.generateDocument(
                                2, classCertFieldSettingList, memberNameAndAgeList, certContent, certMaster, certDate)
                                .write(response.getOutputStream());
                        break;
                    case "英文結業證書":
                        documentVerticallyWordViewNew.generateDocument(
                                3, classCertFieldSettingList, memberNameAndAgeList, certContent, certMaster, certDate, null)
                                .write(response.getOutputStream());
                        break;
                    case "英文全勤證書":
                        documentVerticallyWordViewNew.generateDocument(
                                4, classCertFieldSettingList, memberNameAndAgeList, certContent, certMaster, certDate, null)
                                .write(response.getOutputStream());
                        break;
                    case "日文結業證書":
                        documentVerticallyWordViewNew.generateDocument(
                                7, classCertFieldSettingList, memberNameAndAgeList, certContent, certMaster, certDate, null)
                                .write(response.getOutputStream());
                        break;
                    case "日文全勤證書":
                        documentVerticallyWordViewNew.generateDocument(
                                8, classCertFieldSettingList, memberNameAndAgeList, certContent, certMaster, certDate, null)
                                .write(response.getOutputStream());
                        break;
                    case "中英文結業證書":
                        documentVerticallyWordViewNew.generateDocument(
                                5, classCertFieldSettingList, memberNameAndAgeList, certContent, certMaster, certDate, printer010Request.getPrintType())
                                .write(response.getOutputStream());
                        break;
                    case "中英文全勤證書":
                        documentVerticallyWordViewNew.generateDocument(
                                6, classCertFieldSettingList, memberNameAndAgeList, certContent, certMaster, certDate, printer010Request.getPrintType())
                                .write(response.getOutputStream());
                        break;
                }

                return null;
            }catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok(new BaseResponse(400, "error"));
            }
        }
    }

    // 取得報名表資料
    @RequestMapping(value = "/enroll_forms", method = RequestMethod.GET)
    public ResponseEntity printer011(@RequestParam("classId") String classId) {

        return ResponseEntity.ok(classEnrollFormService.getClassEnrollFormList(classId));
    }

    // 刪除印表機資料
    @RequestMapping(value = "/list/{printerId}", method = RequestMethod.DELETE)
    public ResponseEntity printer012(HttpServletRequest request,
                                     @PathVariable("printerId") String printerId) {

        return ResponseEntity.ok(classPrinterService.deletePinter(printerId));
    }
}
