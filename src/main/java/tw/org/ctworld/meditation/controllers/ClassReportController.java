package tw.org.ctworld.meditation.controllers;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.ClassReport.ClassMemberRecord;
import tw.org.ctworld.meditation.beans.ClassReport.ClassReportRequest;
import tw.org.ctworld.meditation.beans.ClassReport.ClassReportResponse;
import tw.org.ctworld.meditation.beans.ClassReport.ClearPrintMarkerRequest;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.libs.CommonUtils;
import tw.org.ctworld.meditation.services.ClassAttendRecordService;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.views.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/api/class_report")
public class ClassReportController {

    private static final Logger logger = LoggerFactory.getLogger(ClassReportController.class);

    @Autowired
    ClassEnrollFormService classEnrollFormService;

    @Autowired
    ClassAttendRecordService classAttendRecordService;

    @Autowired
    StudentInfoCardWordView studentInfoCardWordView;

    @Autowired
    StudentNameCardWordView1 studentNameCardWordView1;

    @Autowired
    StudentNameCardWordView2 studentNameCardWordView2;


    /**
     * 205 Class Report
     */
    // 報表列印
    @RequestMapping(value = "/{classId}/members", method = RequestMethod.GET)
    public ResponseEntity members(HttpServletRequest request,
                                         @PathVariable("classId") String classId) {

        List<ClassMemberRecord> results =  classEnrollFormService.getClassEnrollFormsAttendRecordsByClassId(classId, false);

        return ResponseEntity.ok(new ClassReportResponse(200,"success", results));
    }

    // 異動列印清除
    @RequestMapping(value = "/{classId}/clear_mark", method = RequestMethod.POST)
    public ResponseEntity clear_mark(HttpServletRequest request,
                                         @PathVariable("classId") String classId,
                                         @RequestBody(required = true) ClearPrintMarkerRequest input) {

        classEnrollFormService.setPrintMark(classId, input.getMemberIdList(), false);

        return ResponseEntity.ok(new BaseResponse(200,"success"));
    }

    // 匯出資料 category由0到4，分別對應到規格書中的5個項目
    @RequestMapping(value = "/{classId}/excel", method = RequestMethod.POST)
    public ModelAndView excel(HttpServletRequest request, Model model, HttpServletResponse response,
                                       @PathVariable("classId") String classId,
                                       @RequestBody(required = false) ClassReportRequest input) {

        logger.debug(classId + "/" + input.getCategory() + "/" + input.getMemberIdList());

        List<ClassMemberRecord> results;

        if (input.getCategory() == 4) {
            results = classEnrollFormService.getClassEnrollFormsAttendRecordsByClassId(classId, true);
        } else {
            results = classEnrollFormService.getClassEnrollFormsAttendRecordsByClassId(classId, false);
        }

        logger.debug("member count: " + results.size());

        for (ClassMemberRecord record: results) {
            logger.debug(record.getMemberId());
        }

        // only print select member
        results = results.stream().filter(m -> input.getMemberIdList().contains(m.getMemberId())).collect(Collectors.toList());

        BaseResponse error = new BaseResponse(400, "input error category 0~4");

        if (results.size() > 0) {
            model.addAttribute("results", results);

            String unitName = (String) request.getSession().getAttribute(UserSession.Keys.UnitName.getValue());
            String userName = (String) request.getSession().getAttribute(UserSession.Keys.UserName.getValue());
            String className =  results.get(0).getClassName();

            switch (input.getCategory()) {
                case 0: //簽到表
                    return new ModelAndView(new AttendanceExcelView(request.getSession()));
                case 1: //學員資料卡
                    try {
                        String fileName =  unitName + "_" + className + "_學員資料卡_" + userName + "_" + CommonUtils.GetCurrentTimeString() + ".doc";
                        fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                        response.setContentType("application/msword; charset=UTF-8");
                        response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

                        studentInfoCardWordView.generateDocument(results).write(response.getOutputStream());
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                        e.printStackTrace();
                    }
                    return null;
                case 2: //中文名牌
                    try {
                        String fileName =  unitName + "_" + className + "_中文名牌_" + userName + "_" + CommonUtils.GetCurrentTimeString() + ".doc";
                        fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                        response.setContentType("application/msword; charset=UTF-8");
                        response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

//                        studentNameCard1.generateDocument(results, 0).write(response.getOutputStream());
                        studentNameCardWordView2.generateDocument(results, 0, unitName).write(response.getOutputStream());
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                        e.printStackTrace();
                    }
                    return null;
                case 3: //英文名牌
                    try {
                        String fileName =  unitName + "_" + className + "_英文名牌_" + userName + "_" + CommonUtils.GetCurrentTimeString() + ".doc";
                        fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());
                        response.setContentType("application/msword; charset=UTF-8");
                        response.setHeader("Content-Disposition", "attachment; filename*=utf-8''" + fileName);

//                        studentNameCard1.generateDocument(results, 1).write(response.getOutputStream());
                        studentNameCardWordView2.generateDocument(results, 1, unitName).write(response.getOutputStream());
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                        e.printStackTrace();
                    }
                    return null;
                case 4: //學員基本資料
                    return new ModelAndView(new StudentInfoExcelView(request.getSession()));
            }
        } else {
            error.setErrCode(401);
            error.setErrMsg("empty result");
        }

        String json = new Gson().toJson(error);
        response.setContentType("application/json");
        try {
            response.getWriter().write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        response.setStatus(error.getErrCode());
        return null;
    }
}
