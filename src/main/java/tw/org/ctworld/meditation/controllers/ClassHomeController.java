package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.Member.MembersObject;
import tw.org.ctworld.meditation.libs.NotFoundException;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(path = "/api/class_home")
public class ClassHomeController {

    private static final Logger logger = LoggerFactory.getLogger(ClassHomeController.class);

    @Autowired
    ClassEnrollFormService classEnrollFormService;

    /**
     * 206 Class Home
     */
    // 取得有跨班的學員列表
    @RequestMapping(value = "/members", method = RequestMethod.GET)
    public ResponseEntity span_members001(HttpServletRequest request,
                                     @RequestParam("class_status") String classStatus) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<MembersObject> result = classEnrollFormService.getSpanMembersByClassStatusUnitId(unitId, classStatus);

        return ResponseEntity.ok(new SpanMembersResponse(0, "success", result));
    }

    // 設定本班
    @RequestMapping(value = "/members", method = RequestMethod.POST)
    public ResponseEntity span_members002(HttpServletRequest request, @RequestBody SpanMembersPostRequest postRequest) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        try {
            classEnrollFormService.updateSpanMember(unitId, postRequest);
        } catch (NotFoundException e) {
            return ResponseEntity.ok(new BaseResponse(400, e.getMessage()));
        }

        return ResponseEntity.ok(new BaseResponse(200,"success"));
    }
}
