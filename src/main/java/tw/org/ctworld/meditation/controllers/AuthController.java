package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tw.org.ctworld.meditation.beans.Auth.Auth004Request;
import tw.org.ctworld.meditation.services.UserInfoService;

import javax.servlet.http.HttpServletRequest;
/*
    Authentication Controller
    Used by Security
    Not for UI
 */
@Controller
@RequestMapping(path = "/api/auth")
public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);


    @Autowired
    private UserInfoService userInfoService;


    // 取得當前使用者資訊
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity auth001(HttpServletRequest request) {

        return ResponseEntity.ok(userInfoService.auth001(request.getSession()));
    }

    // 代登功能
    @RequestMapping(value = "/proxy", method = RequestMethod.POST)
    public ResponseEntity auth004(HttpServletRequest request, @RequestBody Auth004Request auth004Request) {

        return ResponseEntity.ok(userInfoService.auth004(request.getSession(), auth004Request.getUserId()));
    }
}
