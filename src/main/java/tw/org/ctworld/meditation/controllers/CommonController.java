package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import tw.org.ctworld.meditation.beans.*;
import tw.org.ctworld.meditation.beans.Common.*;
import tw.org.ctworld.meditation.models.CodeDef;
import tw.org.ctworld.meditation.repositories.CodeDefRepo;
import tw.org.ctworld.meditation.repositories.ClassInfoRepo;
import tw.org.ctworld.meditation.services.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(path = "/api/common")
public class CommonController {

    private static final Logger logger = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private AnnounceInfoService announceInfoService;

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private CodeDefService codeDefService;

    @Autowired
    private CodeDefRepo codeDefRepo;

    @Autowired
    private ClassInfoRepo classInfoRepo;

    @Autowired
    private ClassEnrollFormService classEnrollFormService;


    // 取得登入者左方選單列表 OK 移除
//    @RequestMapping(path = "/navs", method = RequestMethod.GET)
//    public ResponseEntity common001(HttpServletRequest request, Model model) {
//
//        String userId = (String) request.getSession().getAttribute(UserSession.Keys.UserId.getValue());
//
//        ClassLoginUserInfo classLoginUserInfo = classLoginUserInfoService.findLoginUserInfo(userId);
//
//        return ResponseEntity.ok(new Common001Response(200, "success", classLoginUserInfo));
//    }

    // 取得報到資訊 OK
    @RequestMapping(path = "/check_in_info", method = RequestMethod.GET)
    public ResponseEntity common002(HttpServletRequest request, Model model) {

        String roleType = (String) request.getSession().getAttribute(UserSession.Keys.RoleType.getValue());

        logger.debug("/api/common/check_in_info " + roleType);

        Common002Response common002Response = classInfoService.getClassCounts(request.getSession(), roleType);

        return ResponseEntity.ok(new Common002Response(200, "success",
                common002Response.getTotal(), common002Response.getAttendance(), common002Response.getItems()));
    }

    // 取得未讀取公告訊息數量 OK 移除
//    @RequestMapping(path = "/message_badge", method = RequestMethod.GET)
//    public ResponseEntity common003(HttpServletRequest request, Model model) {
//
//        String userId = (String) request.getSession().getAttribute(UserSession.Keys.UserId.getValue());
//
//        int unreadMsgCount = classAnnounceInfoService.getAnnouncements(userId).size();
//
//        return ResponseEntity.ok(new Common003Response(200, "success", unreadMsgCount));
//    }

    // 取得Year Combobox
    @RequestMapping(path = "/class_year", method = RequestMethod.GET)
    public ResponseEntity common004(HttpServletRequest request, Model model) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

//        List<String> classYearList = classInfoService.getClassYearComboBox(unitId);

        List<String> classYearList = classInfoService.getClassYearComboBox();

        return ResponseEntity.ok(new Common004Response(200, "success", classYearList));
    }

    // 取得狀態ComboBox OK
//    @RequestMapping(path = "/class_status", method = RequestMethod.GET)
//    public ResponseEntity common005(HttpServletRequest request, Model model) {
//
//        List<String> classStatusList = classCodeDefService.getCodeNameComboBox("課程狀態");
//
//        return ResponseEntity.ok(new Common005Response(200, "success", classStatusList));
//    }

    // 取得期別ComboBox OK
    @RequestMapping(path = "/class_period_num", method = RequestMethod.GET)
    public ResponseEntity common006(HttpServletRequest request, @RequestParam("classStatus") String classStatus) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<String> classInfoList = classInfoService.getClassInfoComboBox(classStatus, unitId);

        return ResponseEntity.ok(new Common006Response(200, "success", classInfoList));
    }

    // 取得班別ComboBox OK
    @RequestMapping(path = "/class_name", method = RequestMethod.GET)
    public ResponseEntity common007(HttpServletRequest request,
                                            @RequestParam("classStatus") String classStatus,
                                            @RequestParam("classPeriodNum") String classPeriodNum) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        List<String> classNameList = classInfoService
                .getClassNameComboBox(classStatus, classPeriodNum, unitId);

        return ResponseEntity.ok(new Common006Response(200, "success", classNameList));
    }

    // 取得組別ComboBox OK (2018/12/06 舊版暫時關閉)
//    @RequestMapping(path = "/class_group", method = RequestMethod.GET)
//    public ResponseEntity common008(HttpServletRequest request,
//                                             @RequestParam("classStatus") String classStatus,
//                                             @RequestParam("classPeriodNum") String classPeriodNum,
//                                             @RequestParam("className") String className) {
//
//        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());
//
//        List<String> classNameList = classInfoService
//                .getClassGroupComboBox(classStatus, classPeriodNum, className, unitId);
//
//        return ResponseEntity.ok(new Common006Response(200, "success", classNameList));
//    }

    // 取得class_code_def list OK
    @RequestMapping(path = "/class_code_def", method = RequestMethod.GET)
    public ResponseEntity common009(HttpServletRequest request, Model model) {

        List<CodeDef> codeDefList = codeDefRepo.GetClassCodeDefList();

        return ResponseEntity.ok(new Common009Response(200, "success", codeDefList));
    }

    // 依時段 & 類別取得當前最大期別 OK 原版本
//    @RequestMapping(path = "/max_class_period_num", method = RequestMethod.GET)
//    public ResponseEntity common010(HttpServletRequest request,
//                                    @RequestParam("classDayOrNight") String classDayOrNight,
//                                    @RequestParam("classTypeNum") int classTypeNum) {
//
//        int classPeriodNum = 1;
//
//        if (classInfoRepo.GetClassInfoNumByClassDayOrNight(classDayOrNight) != 0 &&
//                classInfoRepo.GetClassInfoNumByClassTypeNum(classTypeNum) != 0) {
//
//            if (classInfoRepo.GetMaxClassPeriodNum(classDayOrNight, classTypeNum) != null) {
//                classPeriodNum = classInfoRepo.GetMaxClassPeriodNum(classDayOrNight, classTypeNum);
//            }
//        }
//
//        return ResponseEntity.ok(new Common010Response(200, "success", String.valueOf(classPeriodNum)));
//    }

    // 依時段 & 類別取得當前最大期別 OK 9/19新版本
    @RequestMapping(path = "/max_class_period_num", method = RequestMethod.GET)
    public ResponseEntity common010(HttpServletRequest request,
                                    @RequestParam("classDayOrNight") String classDayOrNight,
                                    @RequestParam("classTypeNum") int classTypeNum) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        Integer classPeriodNum = classInfoRepo.GetMaxClassPeriodNum(classDayOrNight, classTypeNum, unitId);

        if (classPeriodNum == null) {
            classPeriodNum = 1;
        }

        return ResponseEntity.ok(new Common010Response(200, "success", String.valueOf(classPeriodNum)));
    }

    // 取得課程列表
    @RequestMapping(path = "/classes", method = RequestMethod.GET)
    public ResponseEntity common011(HttpServletRequest request,
                                    @RequestParam("year") String year,
                                    @RequestParam("classStatus") String classStatus) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(classInfoService.getClassList(year, classStatus, unitId));
    }

    // 取得組別ComboBox OK (2018/12/06 CR新版)
    @RequestMapping(path = "/class_group", method = RequestMethod.GET)
    public ResponseEntity common012(HttpServletRequest request, @RequestParam("classId") String classId) {

        return ResponseEntity.ok(classEnrollFormService.getClassGroupListByClassId(classId));
    }
}
