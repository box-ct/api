package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import tw.org.ctworld.meditation.beans.ClassRecord.ClassRecord000Response;
import tw.org.ctworld.meditation.beans.Makeup.ClassesResponse;
import tw.org.ctworld.meditation.beans.StringListResponse;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.services.ClassInfoService;
import tw.org.ctworld.meditation.services.UserInfoService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(path = "/api/managed_class")
public class ManagedClassController {

    private static final Logger logger = LoggerFactory.getLogger(ManagedClassController.class);

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private UserInfoService userInfoService;

    // CR3 get year list of classes
    @RequestMapping(value = "/years", method = RequestMethod.GET)
    public ResponseEntity years(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new StringListResponse(200, "success",
                classInfoService.getYears(unitId)));
    }

    // 取得上課記錄的狀態列表 ( 需卡法師 / 居士 / 學員長職事 身份) ( 開課中/待結業/已結業) OK
    // CR3
    @RequestMapping(value = "/class_status", method = RequestMethod.GET)
    public ResponseEntity class_status(HttpServletRequest request,
                                         @RequestParam(value = "year", required = false) String year) {

        return ResponseEntity.ok(new ClassRecord000Response(200, "success",
                classInfoService.getClassStatus(request.getSession(), year)));
    }

    @RequestMapping(value = "/class_infos", method = RequestMethod.GET)
    public ResponseEntity class_infos(HttpServletRequest request,
                                         @RequestParam(value = "year", required = false) String year,
                                         @RequestParam(value = "classStatus", required = false) String classStatus,
                                         @RequestParam(value = "classIds", required = false) List<String> classIds) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        if (classIds != null && classIds.size() > 0) {
            year = null;
            classStatus = null;
        } else {
            classIds = null;
        }

        return ResponseEntity.ok(new ClassesResponse(0, "success", classInfoService.getClassByClassIdList(unitId, year, classStatus, classIds)));
    }

//    @RequestMapping(value = "/user_info", method = RequestMethod.POST)
//    public ResponseEntity user_info(HttpServletRequest request,
//                                    @RequestBody ManagedClassRequest managedClassRequest) {
//
//        boolean result = classLoginUserInfoService.updateManagedClasses(managedClassRequest, request.getSession());
//
//        return ResponseEntity.ok(new BaseResponse(result ? 200 : 400, result ? "success" : "user not found"));
//    }
}
