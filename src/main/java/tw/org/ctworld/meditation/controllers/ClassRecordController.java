package tw.org.ctworld.meditation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tw.org.ctworld.meditation.beans.BaseResponse;
import tw.org.ctworld.meditation.beans.ClassRecord.*;
import tw.org.ctworld.meditation.beans.StringListResponse;
import tw.org.ctworld.meditation.beans.UserSession;
import tw.org.ctworld.meditation.services.ClassAttendMarkService;
import tw.org.ctworld.meditation.services.ClassAttendRecordService;
import tw.org.ctworld.meditation.services.ClassEnrollFormService;
import tw.org.ctworld.meditation.services.ClassInfoService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/api/class_record")
public class ClassRecordController {

    private static final Logger logger = LoggerFactory.getLogger(ClassRecordController.class);

    @Autowired
    private ClassEnrollFormService classEnrollFormService;

    @Autowired
    private ClassAttendMarkService classAttendMarkService;

    @Autowired
    private ClassAttendRecordService classAttendRecordService;

    @Autowired
    private ClassInfoService classInfoService;

    // CR3 - get year list of classes
    @RequestMapping(value = "/years", method = RequestMethod.GET)
    public ResponseEntity years(HttpServletRequest request) {

        String unitId = (String) request.getSession().getAttribute(UserSession.Keys.UnitId.getValue());

        return ResponseEntity.ok(new StringListResponse(200, "success",
                classInfoService.getYears(unitId)));
    }

    // 取得上課記錄的狀態列表 ( 需卡法師 / 居士 / 學員長職事 身份) ( 開課中/待結業/已結業) OK
    // CR3
    @RequestMapping(value = "/class_status", method = RequestMethod.GET)
    public ResponseEntity class_status(HttpServletRequest request,
                                         @RequestParam(value = "year", required = false) String year) {

        return ResponseEntity.ok(new ClassRecord000Response(200, "success",
                classInfoService.getClassStatus(request.getSession(), year)));
    }

    // 取得上課記錄的期別列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK
    @RequestMapping(value = "/class_period_num", method = RequestMethod.GET)
    public ResponseEntity classRecord001(HttpServletRequest request,
                                         @RequestParam("year") String year,
                                         @RequestParam("classStatus") String classStatus) {

        return ResponseEntity.ok(new ClassRecord001Response(200, "success",
                classInfoService.getClassRecordClassPeriodNumList(request.getSession(), year, classStatus)));
    }

    // 取得上課記錄的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) OK (2018/12/03 舊版暫時關閉)
//    @RequestMapping(value = "/classes", method = RequestMethod.GET)
//    public ResponseEntity classRecord002(HttpServletRequest request,
//                                         @RequestParam("year") String year,
//                                         @RequestParam("classStatus") String classStatus,
//                                         @RequestParam("classPeriodNum") String classPeriodNum) {
//
//        return ResponseEntity.ok(new ClassRecord002Response(200, "success",
//                classInfoService.getClassRecordClassNameList(request.getSession(), year, classStatus, classPeriodNum)));
//    }

    // 取得上課記錄的班級列表 ( 需卡法師 / 居士 / 學員長職事 身份) (2018/12/03 CR新版)
    // CR3
    @RequestMapping(value = "/classes", method = RequestMethod.GET)
    public ResponseEntity classes(HttpServletRequest request,
                                         @RequestParam(value = "year", required = false) String year,
                                         @RequestParam(value = "classStatus", required = false) String classStatus) {

        return ResponseEntity.ok(new ClassRecord002Response(200, "success",
                classInfoService.getClassRecordClassNameList(request.getSession(), year, classStatus)));
    }

    // 依取得學員的上課記錄 OK
    @RequestMapping(value = "/{classId}/members", method = RequestMethod.GET)
    public ResponseEntity classRecord003(HttpServletRequest request, @PathVariable("classId") String classId) {

        return ResponseEntity.ok(classEnrollFormService.classRecord003(classId));
    }

    // 儲存上課記錄 OK
    @RequestMapping(value = "/{classId}/members", method = RequestMethod.POST)
    public ResponseEntity classRecord004(HttpServletRequest request,
                                         @PathVariable("classId") String classId,
                                         @RequestBody ClassRecord004Request classRecord004Request) {

        return ResponseEntity.ok(
                classAttendRecordService.saveClassAttendRecordList(request.getSession(), classId, classRecord004Request));
    }

    // 刪除學員的上課記錄 OK
    @RequestMapping(value = "/{classId}/members/{memberId}", method = RequestMethod.DELETE)
    public ResponseEntity classRecord005(HttpServletRequest request,
                                         @PathVariable("classId") String classId,
                                         @PathVariable("memberId") String memberId) {

        return ResponseEntity.ok(new BaseResponse(200,
                classAttendRecordService.deleteClassAttendRecord(classId, memberId)));
    }

    // 單筆編輯
    @RequestMapping(value = "/{classId}/members/{memberId}", method = RequestMethod.PUT)
    public ResponseEntity classRecord006(HttpServletRequest request,
                                         @PathVariable("classId") String classId,
                                         @PathVariable("memberId") String memberId,
                                         @RequestBody ClassRecord006Request classRecord006Request) {

        return ResponseEntity.ok(new BaseResponse(200,
                classAttendRecordService.editClassAttendRecord(classId, memberId, classRecord006Request)));
    }

    // 取得上課記錄標記 OK
    @RequestMapping(value = "/{classId}/attend_mark", method = RequestMethod.GET)
    public ResponseEntity classRecord007(HttpServletRequest request, @PathVariable("classId") String classId) {

        return ResponseEntity.ok(new ClassRecord007Response(200,"success",
                classAttendMarkService.getClassAttendMark(request.getSession(), classId)));
    }

    // 更新上課標記 OK
    @RequestMapping(value = "/{classId}/attend_mark", method = RequestMethod.POST)
    public ResponseEntity classRecord008(HttpServletRequest request,
                                         @PathVariable("classId") String classId,
                                         @RequestBody ClassRecord008Request classRecord008Request) {

        return ResponseEntity.ok(new BaseResponse(200,
                classAttendMarkService.editClassAttendMark(request.getSession(), classId, classRecord008Request)));
    }
}
