
const uName = window.navigator.userAgent
// console.log(
//   uName.indexOf('Trident'),
//   uName.indexOf('rv:11'),
//   uName.indexOf('FireFox')
// )
if ( 
  uName.indexOf('Trident') !== -1 &&
  uName.indexOf('rv:11') !== -1 &&
  uName.indexOf('FireFox') === -1
) {
  window.addEventListener('load', function() {
    document.body.addEventListener('click', function(e) {
      console.log('click')
      const formId = e.target.getAttribute('form')
      let event;
      if(typeof(Event) === 'function') {
          event = new Event('submit');
      }else{
          event = document.createEvent('Event');
          event.initEvent('submit', true, true);
      }

      if ( formId !== null ) {
        document.querySelector('#' + formId).dispatchEvent(event)
      }
    })
  })
}
