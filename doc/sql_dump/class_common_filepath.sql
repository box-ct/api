/*
Navicat MariaDB Data Transfer

Source Server         : 10.1.4.29
Source Server Version : 50544
Source Host           : 10.1.4.29:3306
Source Database       : tcgx

Target Server Type    : MariaDB
Target Server Version : 50544
File Encoding         : 65001

Date: 2018-06-04 16:32:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for class_common_filepath
-- ----------------------------
DROP TABLE IF EXISTS `class_common_filepath`;
CREATE TABLE `class_common_filepath` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filePathKind` varchar(10) DEFAULT NULL,
  `filePathRoot` varchar(100) DEFAULT NULL,
  `filePath` varchar(100) DEFAULT NULL,
  `fileName` varchar(100) DEFAULT NULL,
  `isEncryption` tinyint(1) DEFAULT NULL,
  `creatorId` varchar(50) DEFAULT NULL,
  `createDt` varchar(8) DEFAULT NULL,
  `createTm` varchar(6) DEFAULT NULL,
  `creatorIp` varchar(40) DEFAULT NULL,
  `creatorName` varchar(60) DEFAULT NULL,
  `creatorUnitName` varchar(60) DEFAULT NULL,
  `updateDt` varchar(8) DEFAULT NULL,
  `updatorId` varchar(50) DEFAULT NULL,
  `updatorIp` varchar(40) DEFAULT NULL,
  `updateTm` varchar(6) DEFAULT NULL,
  `updatorUnitName` varchar(10) DEFAULT NULL,
  `updatorName` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class_common_filepath
-- ----------------------------
INSERT INTO `class_common_filepath` VALUES ('1', 'SYSTEMIMG', '/image_nfs/SYSTEMIMG/', 'LOGINPAGE', '103-1280.jpg', '0', 'admin', '1060531', '163010', null, null, null, '1060531', 'admin', '192.168.66.31', '163010', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('4', 'PHOTO', '/image_nfs/IMAGES/', '106/106000001/PHOTO', '106000001.M.jpg', '1', 'B1182', '1060605', '194838', null, null, null, '1060605', 'B1182', '192.168.1.54', '194838', '靈泉', null);
INSERT INTO `class_common_filepath` VALUES ('7', 'PHOTO', '/image_nfs/IMAGES/', '106/106000002/PHOTO', '106000002.M.png', '1', 'B1182', '1060605', '194959', null, null, null, '1060605', 'B1182', '192.168.1.54', '194959', '靈泉', null);
INSERT INTO `class_common_filepath` VALUES ('10', 'PHOTO', '/image_nfs/IMAGES/', '106/106000037/PHOTO', '106000037.M.jpg', '1', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `class_common_filepath` VALUES ('13', 'PHOTO', '/image_nfs/IMAGES/', '106/106000038/PHOTO', '106000038.M.png', '1', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `class_common_filepath` VALUES ('16', 'SYSTEMIMG', '/image_nfs/SYSTEMIMG/', 'LOGINPAGE', '46-1680.jpg', '0', 'admin', '1060627', '151419', null, null, null, '1060627', 'admin', '192.168.66.31', '151419', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('19', 'SYSTEMIMG', '/image_nfs/SYSTEMIMG/', 'LOGINPAGE', '50-1680.jpg', '0', 'admin', '1060627', '162001', null, null, null, '1060627', 'admin', '192.168.66.31', '162001', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('22', 'PHOTO', '/image_nfs/IMAGES/', '106/106002384/PHOTO', '106002384.M.jpg', '1', 'A0469', '1060815', '082252', null, null, null, '1060815', 'A0469', '192.168.66.31', '082252', '普同', null);
INSERT INTO `class_common_filepath` VALUES ('25', 'PHOTO', '/image_nfs/IMAGES/', '106/106001950/PHOTO', '106001950.F.jpg', '1', 'A0469', '1060902', '161502', null, null, null, '1060902', 'A0469', '192.168.66.31', '161502', '普同', null);
INSERT INTO `class_common_filepath` VALUES ('28', 'TWID', '/image_nfs/IMAGES/', '107/107000001/TWID', '107000001.D1.jpg', '0', 'admin', '1070210', '170741', null, null, null, '1070210', 'admin', '192.168.66.138', '170741', '中台', 'SYSIMG');
INSERT INTO `class_common_filepath` VALUES ('31', 'TWID', '/image_nfs/IMAGES/', '107/107000001/TWID', '107000001.D2.jpg', '0', 'admin', '1070210', '170758', null, null, null, '1070210', 'admin', '192.168.66.138', '170758', '中台', 'SYSIMG');
INSERT INTO `class_common_filepath` VALUES ('34', 'PHOTO', '/image_nfs/IMAGES/', '107/107000001/PHOTO', '107000001.M.jpg', '0', 'admin', '1070210', '171324', null, null, null, '1070210', 'admin', '192.168.66.138', '171324', '中台', 'SYSIMG');
INSERT INTO `class_common_filepath` VALUES ('37', 'PHOTO', '/image_nfs/IMAGES/', '107/107000001/PHOTO', '107000001.M.jpg', '0', 'admin', '1070223', '162859', null, null, null, '1070223', 'admin', '10.1.1.14', '162859', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('40', 'PHOTO', '/image_nfs/IMAGES/', '107/107007001/PHOTO', '107007001.M.jpg', '0', 'admin', '1070223', '163015', null, null, null, '1070223', 'admin', '10.1.1.14', '163015', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('43', 'PID', '/image_nfs/IMAGES/', '107/107003001/PID', '107003001.P1.png', '0', 'B0590', '1070309', '165207', null, null, null, '1070309', 'B0590', '192.168.66.31', '165207', '普天', null);
INSERT INTO `class_common_filepath` VALUES ('46', 'P_CONSENT', '/image_nfs/IMAGES/', '107/107003001/P_CONSENT', '107003001.P2.png', '0', 'B0590', '1070309', '165518', null, null, null, '1070309', 'B0590', '192.168.66.31', '165518', '普天', null);
INSERT INTO `class_common_filepath` VALUES ('49', 'PID', '/image_nfs/IMAGES/', '107/107000001/PID', '107000001.P1.jpg', '0', 'admin', '1070310', '145444', null, null, null, '1070310', 'admin', '192.168.1.54', '145444', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('52', 'P_CONSENT', '/image_nfs/IMAGES/', '107/107000001/P_CONSENT', '107000001.P2.jpg', '0', 'admin', '1070310', '145457', null, null, null, '1070310', 'admin', '192.168.1.54', '145457', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('55', 'PHOTO', '/image_nfs/IMAGES/', '107/107000001/PHOTO', '107000001.M.png', '0', 'admin', '1070310', '174651', null, null, null, '1070310', 'admin', '192.168.66.31', '174651', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('58', 'PHOTO', '/image_nfs/IMAGES/', '107/107000001/PHOTO', '107000001.M.png', '0', 'admin', '1070310', '174732', null, null, null, '1070310', 'admin', '192.168.66.31', '174732', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('61', 'PHOTO', '/image_nfs/IMAGES/', '107/107000002/PHOTO', '107000002.M.jpg', '0', 'admin', '1070310', '174834', null, null, null, '1070310', 'admin', '192.168.66.31', '174834', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('65', 'SYSTEMIMG', '/image_nfs/SYSTEMIMG/', 'LOGINPAGE', '50-1920.jpg', '0', 'admin', '1070504', '165803', null, null, null, '1070504', 'admin', '192.168.1.54', '165803', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('68', 'PHOTO', '/image_nfs/IMAGES/', '107/107023804/PHOTO', '107023804.M.jpg', '0', 'B0590', '1070505', '094025', null, null, null, '1070505', 'B0590', '192.168.66.138', '094025', '普天', null);
INSERT INTO `class_common_filepath` VALUES ('71', 'PHOTO', '/image_nfs/IMAGES/', '107/107007001/PHOTO', '107007001.M.jpg', '0', 'admin', '1070512', '105244', null, null, null, '1070512', 'admin', '192.168.1.54', '105244', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('74', 'TWID', '/image_nfs/IMAGES/', '107/107007001/TWID', '107007001.D1.jpg', '0', 'admin', '1070512', '105253', null, null, null, '1070512', 'admin', '192.168.1.54', '105253', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('77', 'TWID', '/image_nfs/IMAGES/', '107/107007001/TWID', '107007001.D2.jpg', '0', 'admin', '1070512', '105301', null, null, null, '1070512', 'admin', '192.168.1.54', '105301', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('80', 'PID', '/image_nfs/IMAGES/', '107/107007001/PID', '107007001.P1.jpg', '0', 'admin', '1070512', '105309', null, null, null, '1070512', 'admin', '192.168.1.54', '105309', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('83', 'P_CONSENT', '/image_nfs/IMAGES/', '107/107007001/P_CONSENT', '107007001.P2.jpg', '0', 'admin', '1070512', '105318', null, null, null, '1070512', 'admin', '192.168.1.54', '105318', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('86', 'PID', '/image_nfs/IMAGES/', '107/107007001/PID', '107007001.P1.png', '0', 'admin', '1070512', '111055', null, null, null, '1070512', 'admin', '192.168.1.54', '111055', '中台', null);
INSERT INTO `class_common_filepath` VALUES ('89', 'PID', '/image_nfs/IMAGES/', '107/107007001/PID', '107007001.P1.png', '0', 'admin', '1070512', '111931', null, null, null, '1070512', 'admin', '192.168.1.54', '111931', '中台', null);
SET FOREIGN_KEY_CHECKS=1;
