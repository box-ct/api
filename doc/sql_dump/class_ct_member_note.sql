/*
Navicat MariaDB Data Transfer

Source Server         : 10.1.4.29
Source Server Version : 50544
Source Host           : 10.1.4.29:3306
Source Database       : tcgx

Target Server Type    : MariaDB
Target Server Version : 50544
File Encoding         : 65001

Date: 2018-06-04 16:33:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for class_ct_member_note
-- ----------------------------
DROP TABLE IF EXISTS `class_ct_member_note`;
CREATE TABLE `class_ct_member_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createDt` varchar(8) DEFAULT NULL,
  `creatorId` varchar(10) DEFAULT NULL,
  `createTm` varchar(6) DEFAULT NULL,
  `creatorIp` varchar(40) DEFAULT NULL,
  `creatorName` varchar(60) DEFAULT NULL,
  `creatorUnitName` varchar(10) DEFAULT NULL,
  `updateDt` varchar(8) DEFAULT NULL,
  `updatorId` varchar(10) DEFAULT NULL,
  `updatorIp` varchar(40) DEFAULT NULL,
  `updateTm` varchar(6) DEFAULT NULL,
  `updatorUnitName` varchar(10) DEFAULT NULL,
  `updatorName` varchar(60) DEFAULT NULL,
  `aliasName` varchar(180) DEFAULT NULL,
  `memberId` varchar(50) DEFAULT NULL,
  `note` text,
  `noteDate` varchar(50) DEFAULT NULL,
  `noteType` varchar(30) DEFAULT NULL,
  `unitId` varchar(10) DEFAULT NULL,
  `unitName` varchar(10) DEFAULT NULL,
  `writePerson` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class_ct_member_note
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
