/*
Navicat MariaDB Data Transfer

Source Server         : 10.1.4.29
Source Server Version : 50544
Source Host           : 10.1.4.29:3306
Source Database       : tcgx

Target Server Type    : MariaDB
Target Server Version : 50544
File Encoding         : 65001

Date: 2018-06-04 16:33:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for class_event_main
-- ----------------------------
DROP TABLE IF EXISTS `class_event_main`;
CREATE TABLE `class_event_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` varchar(17) DEFAULT NULL,
  `eventName` varchar(100) DEFAULT NULL,
  `eventFullName` varchar(100) DEFAULT NULL,
  `eventCategoryId` varchar(6) DEFAULT NULL,
  `eventStartDate` varchar(8) DEFAULT NULL,
  `eventEndDate` varchar(8) DEFAULT NULL,
  `eventStartTime` varchar(7) DEFAULT NULL,
  `eventEndTime` varchar(7) DEFAULT NULL,
  `vlntrDaysAfterEvent` smallint(6) DEFAULT NULL,
  `vlntrDaysBeforeEvent` smallint(6) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `note` varchar(300) DEFAULT NULL,
  `sponsorUnitId` varchar(9) DEFAULT NULL,
  `sponsorUnitName` varchar(10) DEFAULT NULL,
  `peopleForecast` int(11) DEFAULT NULL,
  `peopleEnroll` int(11) DEFAULT NULL,
  `eventYear` varchar(10) DEFAULT NULL,
  `creatorId` varchar(50) DEFAULT NULL,
  `creatorName` varchar(60) DEFAULT NULL,
  `createDt` varchar(8) DEFAULT NULL,
  `createTm` varchar(6) DEFAULT NULL,
  `creatorIp` varchar(40) DEFAULT NULL,
  `creatorUnitName` varchar(60) DEFAULT NULL,
  `updatorId` varchar(50) DEFAULT NULL,
  `updatorName` varchar(60) DEFAULT NULL,
  `updateDt` varchar(8) DEFAULT NULL,
  `updateTm` varchar(6) DEFAULT NULL,
  `updatorIp` varchar(40) DEFAULT NULL,
  `updatorUnitName` varchar(60) DEFAULT NULL,
  `attendUnitList` text,
  `changeEndDate` varchar(8) DEFAULT NULL,
  `eventCreatorUnitId` varchar(9) DEFAULT NULL,
  `enrollEndDate` varchar(8) DEFAULT NULL,
  `eventCategoryName` varchar(30) DEFAULT NULL,
  `finalDate` varchar(8) DEFAULT NULL,
  `idType1List` varchar(50) DEFAULT NULL,
  `idType2List` varchar(50) DEFAULT NULL,
  `isAvailableForAllUnit` tinyint(1) DEFAULT NULL,
  `isCreatedByCt` tinyint(1) DEFAULT NULL,
  `isCreatedByUnit` tinyint(1) DEFAULT NULL,
  `isHeldInUnit` tinyint(1) DEFAULT NULL,
  `isShowRefugePrecept5Tab` tinyint(1) DEFAULT NULL,
  `isShowSummerDonationButton` tinyint(1) DEFAULT NULL,
  `isShowTourTab` tinyint(1) DEFAULT NULL,
  `isShowUnitVlntrTab` tinyint(1) DEFAULT NULL,
  `isShowZen7Precept8Tab` tinyint(1) DEFAULT NULL,
  `isSyncCtTable` tinyint(1) DEFAULT NULL,
  `noTimeLimitUnitList` text,
  `summerDonationType` varchar(50) DEFAULT NULL,
  `eventCreatorUnitName` varchar(10) DEFAULT NULL,
  `ctEnrolledSum` int(11) DEFAULT NULL,
  `isMutipleEnroll` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=691 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class_event_main
-- ----------------------------
INSERT INTO `class_event_main` VALUES ('688', 'EVNT2210706020001', '1070629_法務組_福慧出坡(2天)_111', '1070629_法務組_福慧出坡(2天)_111', 'EVNT22', '20180629', '20180630', '0800', '1700', '0', '0', '1', '活動備註', 'UNIT02002', '法務組', '200', '0', '2018', 'admin', '系統管理者', '1070602', '133248', '192.168.1.54', '中台', 'B0590', '釋見驚(系統管理者代)', '1070602', '140231', '192.168.1.54', '普天', 'UNIT01001:靈泉,UNIT01002:彌陀寺,UNIT01003:天祥,UNIT01004:蓮社,UNIT01005:慈明寺,UNIT01006:普光,UNIT01007:普眼,UNIT01008:普照,UNIT01009:普覺,UNIT01010:普賢,UNIT01011:普印,UNIT01013:普泉,UNIT01014:普觀,UNIT01015:普行,UNIT01016:普皇,UNIT01017:普得,UNIT01018:普榮,UNIT01019:普圓,UNIT01020:普明,UNIT01021:普因,UNIT01022:普濟,UNIT01024:普雨,UNIT01025:普新,UNIT01026:普善,UNIT01027:印月,UNIT01028:普燈,UNIT01029:普學,UNIT01030:普正,UNIT01031:普頓,UNIT01032:普中,UNIT01033:普耕,UNIT01034:普親,UNIT01035:興山寺,UNIT01036:普宏,UNIT01037:普平,UNIT01038:普慈,UNIT01039:普雲,UNIT01040:古峰寺,UNIT01041:普真,UNIT01042:普民,UNIT01043:普安,UNIT01044:普豐,UNIT01045:普梵,UNIT01046:普成,UNIT01047:普恩,UNIT01048:普方,UNIT01049:普竺,UNIT01050:普天,UNIT01051:奉天,UNIT01052:地藏,UNIT01053:普彰,UNIT01054:普林,UNIT01055:普田,UNIT01056:普嘉,UNIT01057:普佛,UNIT01058:普思,UNIT01059:普憲,UNIT01060:普門,UNIT01061:普南,UNIT01062:普高,UNIT01063:普化,UNIT01064:普信,UNIT01065:普上,UNIT01066:普糧,UNIT01067:普剛,UNIT01068:志成,UNIT01069:普蓮寺,UNIT01070:瑞光,UNIT01071:普宜,UNIT01072:普台,UNIT01073:天池,UNIT01074:太谷,UNIT01075:佛門寺,UNIT01076:中洲,UNIT01077:佛心,UNIT01078:普德,UNIT01079:佛寶,UNIT01080:寶林,UNIT01081:法寶,UNIT01082:普東,UNIT01083:海天,UNIT01084:普廣,UNIT01085:泰佛,UNIT01086:普法,UNIT01088:德州禪寺,UNIT01089:普同,UNIT01090:華義寺,UNIT01091:普和,UNIT01092:妙光,UNIT01093:普開,UNIT01094:普慶', '20180630', 'UNIT02000', '20180629', '福慧出坡', '20180630', '貴賓,信眾,義工,學員,親眷', '打七,護七,戒子,護戒', '1', '1', null, null, '1', '1', '1', '1', '1', '1', 'UNIT01050:普天', null, '中台', '4', '1');
SET FOREIGN_KEY_CHECKS=1;
